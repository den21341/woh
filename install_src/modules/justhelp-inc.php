<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

/*
//======================= BUYER CLIENTS TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_SHOP_BUYERS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_SHOP_BUYERS."(
  id integer NOT NULL auto_increment,
  account_type integer NOT NULL DEFAULT '1',
  login varchar(128) NOT NULL,
  passwd varchar(128) NOT NULL,
  isactive tinyint NOT NULL DEFAULT '1',
  isactive_web tinyint NOT NULL DEFAULT '0',
  add_date datetime NOT NULL,
  name varchar(255) NOT NULL DEFAULT '',
  fname varchar(255) NOT NULL DEFAULT '',
  city_id integer NOT NULL DEFAULT '0',
  city varchar(255) NOT NULL DEFAULT '',
  profession varchar(128) NOT NULL DEFAULT '',
  address varchar(255) NOT NULL DEFAULT '',
  phone varchar(64) NOT NULL DEFAULT '',
  email varchar(128) DEFAULT '',
  fb_uid varchar(255) DEFAULT '',
  vk_uid varchar(255) DEFAULT '',
  od_uid varchar(255) DEFAULT '',
  guid_act varchar(255) DEFAULT '',
  guid_deact varchar(255) DEFAULT '',
  comments text,
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_SHOP_BUYERS." created<br>";

//==================== BUYER AUTHORIZATION TEMP DATA ========================
$this->db->exec("DROP TABLE ".TABLE_SHOP_BUYER_AUTH." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_SHOP_BUYER_AUTH." (
  id integer NOT NULL PRIMARY KEY auto_increment,
  ses_id varchar(64) NOT NULL UNIQUE,
  user_login varchar(64) NOT NULL,
  user_passwd varchar(64) NOT NULL,
  time_added datetime,
  sc_uid varchar(64) NOT NULL DEFAULT '',
  sc_token text
) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_SHOP_BUYER_AUTH." created<br>";

//mysql_query("CREATE INDEX photo_gal_ind ON $TABLE_PAGE_PHOTO (item_id)");
//$this->db->exec("CREATE INDEX photo_gal_ind ON ".TABLE_PAGE_PHOTO." (item_id)");


//============================ PRODUCT CATALOG TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_CATALOG." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_CATALOG." (
  id integer NOT NULL auto_increment,
  parent_id integer NOT NULL DEFAULT '0',
  visible integer NOT NULL DEFAULT '1',
  sort_num integer NOT NULL DEFAULT '0',
  product_layout integer NOT NULL DEFAULT '0',
  sect_rate integer NOT NULL DEFAULT '0',
  show_first integer NOT NULL DEFAULT '0',
  is_link integer NOT NULL DEFAULT '0',
  url varchar(255) DEFAULT '',
  url_old varchar(255) DEFAULT '',
  filename varchar(255) NOT NULL DEFAULT '',
  filename_thumb varchar(255) NOT NULL DEFAULT '',
  banner_file varchar(255) NOT NULL DEFAULT '',
  banner_link varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_CATALOG." created<br>";

//============================ PRODUCT CATALOG LANG TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_CATALOG_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_CATALOG_LANGS." (
  id integer NOT NULL auto_increment,
  sect_id integer NOT NULL,
  lang_id integer NOT NULL,
  name varchar(255) DEFAULT '',
  descr text,
  descr0 text,
  meta_title text,
  meta_keyw text,
  meta_descr text,
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_CATALOG_LANGS." created<br>";

$this->db->exec("CREATE INDEX sect_lang_ind ON ".TABLE_CAT_CATALOG_LANGS." (sect_id, lang_id)");
//echo mysql_error();

//=========================== ITEM TO CATALOG SECT TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_CAT_CATITEMS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_CATITEMS."(
  id integer NOT NULL auto_increment,
  sect_id integer NOT NULL,
  item_id integer NOT NULL,
  sort_num integer NOT NULL DEFAULT '0',
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_CATITEMS." created<br>";

$this->db->exec("CREATE INDEX sect_item_ind ON ".TABLE_CAT_CATITEMS." (sect_id, item_id)");
//echo mysql_error();

//============================ PRODUCT ITEMS TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS."(
id integer NOT NULL auto_increment,
profile_id integer NOT NULL DEFAULT '1',
author_id integer NOT NULL,
obl_id integer NOT NULL DEFAULT '0',
item_rate integer NOT NULL DEFAULT '0',
status tinyint DEFAULT '0',
archive tinyint DEFAULT '0',
is_series tinyint NOT NULL DEFAULT '0',
amount_type integer NOT NULL DEFAULT '0',
amount_val integer NOT NULL DEFAULT '0',
amount decimal(8,2) NOT NULL DEFAULT '0.00',
articul varchar(64) NOT NULL DEFAULT '',
title varchar(128) NOT NULL,
url varchar(255) DEFAULT '',		
add_date datetime NOT NULL,
modify_date datetime NOT NULL,
start_date datetime NOT NULL,
end_date datetime NOT NULL,
PRIMARY KEY(id) )");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS." created<br>";

$this->db->exec("CREATE INDEX obl_ind ON ".TABLE_CAT_ITEMS." (obl_id)");
$this->db->exec("CREATE INDEX publisher_ind ON ".TABLE_CAT_ITEMS." (author_id)");
//echo mysql_error();
//$this->db->exec("CREATE INDEX section_ind ON ".TABLE_CAT_ITEMS." (sect_id)");
//echo mysql_error();
//$this->db->exec("CREATE INDEX make_ind ON ".TABLE_CAT_ITEMS." (make_id)");
//echo mysql_error();
//$this->db->exec("CREATE INDEX profile_ind ON ".TABLE_CAT_ITEMS." (profile_id)");
//echo mysql_error();

//========================== VEHICLE ITEMS LANGS TABLE =======================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_LANGS."(
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
lang_id integer NOT NULL,
title2 varchar(128) NOT NULL DEFAULT '',
descr text,
descr0 text,
page_title VARCHAR(255) NOT NULL DEFAULT '',
page_keywords TEXT NOT NULL,
page_descr TEXT NOT NULL,
PRIMARY KEY(id) )");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_LANGS." created<br>";

$this->db->exec("CREATE INDEX item_lang_ind ON ".TABLE_CAT_ITEMS_LANGS." (item_id, lang_id)");
//echo mysql_error();

//======================= VEHICLE ITEM'S PICTURES TABLE ======================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_PICS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_PICS." ( 
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
filename varchar(255) NOT NULL DEFAULT '',
filename_big varchar(255) NOT NULL DEFAULT '',
filename_thumb varchar(255) NOT NULL DEFAULT '',
filename_ico varchar(255) NOT NULL DEFAULT '',
title varchar(64) NOT NULL DEFAULT '',
sort_num integer NOT NULL,
ico_w integer NOT NULL DEFAULT '0',
ico_h integer NOT NULL DEFAULT '0',
thumb_w integer NOT NULL DEFAULT '0',
thumb_h integer NOT NULL DEFAULT '0',
big_w integer NOT NULL DEFAULT '0',
big_h integer NOT NULL DEFAULT '0',
src_w integer NOT NULL DEFAULT '0',
src_h integer NOT NULL DEFAULT '0',
PRIMARY KEY(id) )");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_PICS." created<br>";

$this->db->exec("CREATE INDEX item_file_ind ON ".TABLE_CAT_ITEMS_PICS." (item_id)");
//echo mysql_error();

//========================== ITEM's VIDEO TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_VIDEO." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_VIDEO."(
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
add_date datetime,
filename varchar(255) NOT NULL DEFAULT '',
filename_ico varchar(255) NOT NULL DEFAULT '',
sort_num integer NOT NULL,
ico_w integer NOT NULL DEFAULT '0',
ico_h integer NOT NULL DEFAULT '0',
src_w integer NOT NULL DEFAULT '0',
src_h integer NOT NULL DEFAULT '0',
tube_code text,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_VIDEO." created<br>";

$this->db->exec("CREATE INDEX video_gal_ind ON ".TABLE_CAT_ITEMS_VIDEO." (item_id)");

//========================== ITEM's VIDEO LANG TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_VIDEO_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_VIDEO_LANGS." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
lang_id integer NOT NULL,
title varchar(255) NOT NULL DEFAULT '',
descr text,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_VIDEO_LANGS." created<br>";

$this->db->exec("CREATE INDEX item_lang_ind ON ".TABLE_CAT_ITEMS_VIDEO_LANGS." (item_id, lang_id)");
//echo mysql_error();

//========================== ITEM's FILES TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_FILES." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_FILES." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
file_type integer NOT NULL DEFAULT '0',
add_date datetime,
filename varchar(255) NOT NULL DEFAULT '',
sort_num integer NOT NULL,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_FILES." created<br>";

$this->db->exec("CREATE INDEX prod_ind ON ".TABLE_CAT_ITEMS_FILES." (item_id)");

//========================== ITEM's FILES LANG TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_FILES_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_FILES_LANGS." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
lang_id integer NOT NULL,
title varchar(255) NOT NULL DEFAULT '',
descr text,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_FILES_LANGS." created<br>";

$this->db->exec("CREATE INDEX item_lang_ind ON ".TABLE_CAT_ITEMS_FILES_LANGS." (item_id, lang_id)");
//echo mysql_error();

//======================= COMMENTS TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_COMMENT." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_COMMENT." (
id integer NOT NULL AUTO_INCREMENT,
item_id integer NOT NULL,
sort_num int DEFAULT '0',
visible tinyint DEFAULT '0',
rate tinyint DEFAULT '0',
add_date datetime DEFAULT NULL,
author varchar(255) NOT NULL,
author_email varchar(255) NOT NULL,
PRIMARY KEY (`id`),
KEY product_id (`item_id`))");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_COMMENT." created<br>";

//======================= COMMENTS LOCAL LANG ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_COMMENT_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_COMMENT_LANGS." (
id integer NOT NULL AUTO_INCREMENT,
item_id integer NOT NULL,
lang_id integer NOT NULL,
content text,
PRIMARY KEY (`id`),
KEY item_id (`item_id`),
KEY lang_id (`lang_id`))");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_COMMENT_LANGS." created<br>";

//=========================== ITEM RATE TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_RATE." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_RATE." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
dt date NOT NULL,
amount integer NOT NULL DEFAULT '0',
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_CAT_ITEMS_RATE." created<br>";

$this->db->exec("CREATE INDEX rate_item_ind ON ".TABLE_CAT_ITEMS_RATE." (item_id)");
//echo mysql_error();
$this->db->exec("CREATE INDEX rate_dt_ind ON ".TABLE_CAT_ITEMS_RATE." (dt)");
//echo mysql_error();

//======================= ITEM MODIFICATIONS TABLE =========================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_RELATED." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_RELATED." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
rel_id integer NOT NULL,
reltype integer NOT NULL DEFAULT '0',
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_CAT_ITEMS_RELATED." created<br>";

$this->db->exec("CREATE INDEX item_ind ON ".TABLE_CAT_ITEMS_RELATED." (item_id)");
//echo mysql_error();
$this->db->exec("CREATE INDEX rel_ind ON ".TABLE_CAT_ITEMS_RELATED." (rel_id)");
//echo mysql_error();


//========================== ITEM's HELP REQ TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_HELPREQ." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_HELPREQ." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
sender_id integer NOT NULL,
req_type integer NOT NULL DEFAULT '0',
req_status integer NOT NULL DEFAULT '0',
req_amount decimal(8,2) NOT NULL DEFAULT '0.00',
add_date datetime,
modify_date datetime,
comments text,		
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_HELPREQ." created<br>";

$this->db->exec("CREATE INDEX prod_ind ON ".TABLE_CAT_ITEMS_HELPREQ." (item_id)");
$this->db->exec("CREATE INDEX user_ind ON ".TABLE_CAT_ITEMS_HELPREQ." (sender_id)");

//========================== ITEM's HELP REQ RATES TABLE ============================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_HELPREQ_RATE." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_HELPREQ_RATE." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
req_id integer NOT NULL, 
author_id integer NOT NULL,
user_id integer NOT NULL,
rate integer NOT NULL DEFAULT '0',		
add_date datetime,
rate_comments text,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_HELPREQ_RATE." created<br>";

$this->db->exec("CREATE INDEX proj_ind ON ".TABLE_CAT_ITEMS_HELPREQ_RATE." (item_id)");
$this->db->exec("CREATE INDEX req_ind ON ".TABLE_CAT_ITEMS_HELPREQ_RATE." (req_id)");
$this->db->exec("CREATE INDEX author_ind ON ".TABLE_CAT_ITEMS_HELPREQ_RATE." (author_id)");
$this->db->exec("CREATE INDEX user_ind ON ".TABLE_CAT_ITEMS_HELPREQ_RATE." (user_id)");

*/

//========================== MESSAGE BOARD TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_MSG." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_MSG." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
from_id integer NOT NULL,
to_id integer NOT NULL,
reply_msg_id integer NOT NULL,
msg_status integer NOT NULL DEFAULT '0',
add_date datetime,
modify_date datetime,
message text,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_MSG." created<br>";

$this->db->exec("CREATE INDEX prod_ind ON ".TABLE_CAT_MSG." (item_id)");
$this->db->exec("CREATE INDEX from_ind ON ".TABLE_CAT_MSG." (from_id)");
$this->db->exec("CREATE INDEX to_ind ON ".TABLE_CAT_MSG." (to_id)");
$this->db->exec("CREATE INDEX reply_ind ON ".TABLE_CAT_MSG." (reply_msg_id)");

//===================== MESSAGE BOARD ASSIGNES TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_MSG_P2P." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_MSG_P2P." (
id integer NOT NULL auto_increment,
msg_id integer NOT NULL,
user_id integer NOT NULL,
is_read integer NOT NULL DEFAULT '0',
status integer NOT NULL DEFAULT '0',
add_date datetime,
modify_date datetime,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_MSG_P2P." created<br>";

$this->db->exec("CREATE INDEX msg_ind ON ".TABLE_CAT_MSG_P2P." (msg_id)");
$this->db->exec("CREATE INDEX user_ind ON ".TABLE_CAT_MSG_P2P." (user_id)");


//========================== MASSAGEs' FILES TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_MSG_FILES." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_MSG_FILES." (
id integer NOT NULL auto_increment,
msg_id integer NOT NULL,
file_type integer NOT NULL DEFAULT '0',
sort_num integer NOT NULL,
add_date datetime,
filename varchar(255) NOT NULL DEFAULT '',
title varchar(255) NOT NULL DEFAULT '',
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_MSG_FILES." created<br>";

$this->db->exec("CREATE INDEX msg_ind ON ".TABLE_CAT_MSG_FILES." (msg_id)");


?>