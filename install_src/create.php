<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Create database structure
	//include "../inc/db-inc.php";
	//include "../inc/connect-inc.php";
	//include "../inc/utils-inc.php";
	
	$this->db->setDebug(true);

	// Include files with SQL scripts to create database tables for each module
	/*
	include "modules/langs-inc.php";

	include "modules/pages-inc.php";
	include "modules/user-inc.php";	
	include "modules/slides-inc.php";	
	include "modules/news-inc.php";
	include "modules/faq-inc.php";

	//include "modules/photo-inc.php";
	//include "modules/vacancy-inc.php";
	//include "modules/links-inc.php";
	//include "modules/staff-inc.php";
	//include "modules/awards-inc.php";
	//include "modules/feedbacklists-inc.php";
	//include "modules/docs-inc.php";
	//include "modules/questlist-inc.php";
	//include "modules/offices-inc.php";

	include "modules/country-inc.php";
	include "modules/countryinit-inc.php";
	include "modules/init-inc.php";

	//include "modules/banners-inc.php";
	*/
	
	//include "modules/justhelp-inc.php";
	
	/*
	include "modules/catalog2-inc.php";
	include "modules/catalog2_init-inc.php";
	include "modules/catalog2filters-inc.php";
	include "modules/catalog2markets-inc.php";

	include "modules/carcatalog-inc.php";	
	*/

//=========================== PAYS DUMP TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_PAYS_DUMP." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_PAYS_DUMP." (
id integer NOT NULL auto_increment,
paygate_id integer NOT NULL,
callback_id integer NOT NULL,		
add_date datetime NOT NULL,
opid varchar(64) NOT NULL DEFAULT '',
info text,		
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_PAYS_DUMP." created<br>";

$this->db->exec("CREATE INDEX pgate_ind ON ".TABLE_PAYS_DUMP." (paygate_id)");
$this->db->exec("CREATE INDEX op_ind ON ".TABLE_PAYS_DUMP." (opid)");


//=========================== PAYS OPERATIONS TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_PAYS_OPERATIONS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_PAYS_OPERATIONS." (
id integer NOT NULL auto_increment,
payer_id integer NOT NULL DEFAULT '0',
reciever_id integer NOT NULL DEFAULT '0',
proj_id integer NOT NULL DEFAULT '0',
proj_prof integer NOT NULL DEFAULT '0',
paygate_id integer NOT NULL,
callback_id integer NOT NULL,
status integer NOT NULL DEFAULT '0',
add_date datetime NOT NULL,
modify_date datetime NOT NULL,
amount decimal(8,2) NOT NULL DEFAULT '0.00',
currency varchar(32) NOT NULL DEFAULT '',
op_id varchar(128) NOT NULL DEFAULT '',		
op_status varchar(64) NOT NULL DEFAULT '',
pay_via varchar(64) NOT NULL DEFAULT '',
info text,
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_PAYS_OPERATIONS." created<br>";

$this->db->exec("CREATE INDEX payer_ind ON ".TABLE_PAYS_OPERATIONS." (payer_id)");
$this->db->exec("CREATE INDEX status_ind ON ".TABLE_PAYS_OPERATIONS." (status)");
$this->db->exec("CREATE INDEX reciever_ind ON ".TABLE_PAYS_OPERATIONS." (reciever_id)");
$this->db->exec("CREATE INDEX proj_ind ON ".TABLE_PAYS_OPERATIONS." (proj_id)");
$this->db->exec("CREATE INDEX pgate_ind ON ".TABLE_PAYS_OPERATIONS." (paygate_id)");
$this->db->exec("CREATE INDEX op_ind ON ".TABLE_PAYS_OPERATIONS." (op_id)");


/*
$query = "SELECT id FROM ".TABLE_SHOP_BUYERS." ";
$res = $this->db->query($query);
if( count($res) > 0 )
{
	for( $i=0; $i<count($res); $i++ )
	{
		$uhash = UhCmsUtils::makeUuid();
		
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET guid_uhash='".addslashes($uhash)."' WHERE id=".$res[$i]['id'];
		$this->db->exec($query); 
	}
}
*/
	
/*
//=========================== ITEM RATE TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_SC_RATE." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_SC_RATE." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
user_referer_id integer NOT NULL,
dt date NOT NULL,
add_date datetime NOT NULL,
ip varchar(32) NOT NULL DEFAULT '',
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_CAT_ITEMS_SC_RATE." created<br>";

$this->db->exec("CREATE INDEX rate_item_ind ON ".TABLE_CAT_ITEMS_SC_RATE." (item_id)");
//echo mysql_error();
$this->db->exec("CREATE INDEX rate_dt_ind ON ".TABLE_CAT_ITEMS_SC_RATE." (dt)");
//echo mysql_error();
$this->db->exec("CREATE INDEX ip_ind ON ".TABLE_CAT_ITEMS_SC_RATE." (ip)");


//=========================== ITEM RATE TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_CAT_ITEMS_SC_RATESUM." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_SC_RATESUM." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
dt date NOT NULL,
amount integer NOT NULL DEFAULT '0',
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_CAT_ITEMS_SC_RATESUM." created<br>";

$this->db->exec("CREATE INDEX rate_item_ind ON ".TABLE_CAT_ITEMS_SC_RATESUM." (item_id)");
//echo mysql_error();
$this->db->exec("CREATE INDEX rate_dt_ind ON ".TABLE_CAT_ITEMS_SC_RATESUM." (dt)");
*/

////////////////////////////////////////////////////////////////////////////////
//
// ---------------------------    SEO SECTION    -------------------------------
//
////////////////////////////////////////////////////////////////////////////////

/*
//============================ PRODUCT ITEMS TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_SEO_TITLES." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_SEO_TITLES."(
id integer NOT NULL auto_increment,
lang_id integer NOT NULL,
url varchar(255) NOT NULL DEFAULT '',
add_date datetime,
modify_date datetime,
page_title varchar(255) NOT NULL DEFAULT '',
page_keywords text,
page_descr text,
content_text text,
content_words text,
PRIMARY KEY(id) )");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_SEO_TITLES." created<br>";

$this->db->exec("CREATE INDEX lang_ind ON ".TABLE_SEO_TITLES." (lang_id)");
$this->db->exec("CREATE INDEX url_ind ON ".TABLE_SEO_TITLES." (url)");
*/

/*
//============================ PRODUCT ITEMS TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS."(
id integer NOT NULL auto_increment,
author_id integer NOT NULL,
obl_id integer NOT NULL DEFAULT '0',
item_rate integer NOT NULL DEFAULT '0',
status tinyint DEFAULT '0',
archive tinyint DEFAULT '0',
amount_val integer NOT NULL DEFAULT '0',
title varchar(128) NOT NULL,
url varchar(255) DEFAULT '',
add_date datetime NOT NULL,
modify_date datetime NOT NULL,
start_date datetime NOT NULL,
end_date datetime NOT NULL,
PRIMARY KEY(id) )");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_BONUS." created<br>";

$this->db->exec("CREATE INDEX obl_ind ON ".TABLE_CAT_BONUS." (obl_id)");
$this->db->exec("CREATE INDEX publisher_ind ON ".TABLE_CAT_BONUS." (author_id)");

//========================== VEHICLE ITEMS LANGS TABLE =======================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_LANGS."(
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
lang_id integer NOT NULL,
title2 varchar(128) NOT NULL DEFAULT '',
descr text,
descr0 text,
page_title VARCHAR(255) NOT NULL DEFAULT '',
page_keywords TEXT NOT NULL,
page_descr TEXT NOT NULL,
PRIMARY KEY(id) )");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_BONUS_LANGS." created<br>";

$this->db->exec("CREATE INDEX item_lang_ind ON ".TABLE_CAT_BONUS_LANGS." (item_id, lang_id)");
//echo mysql_error();

//======================= VEHICLE ITEM'S PICTURES TABLE ======================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_PICS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_PICS." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
filename varchar(255) NOT NULL DEFAULT '',
filename_big varchar(255) NOT NULL DEFAULT '',
filename_thumb varchar(255) NOT NULL DEFAULT '',
filename_ico varchar(255) NOT NULL DEFAULT '',
title varchar(64) NOT NULL DEFAULT '',
sort_num integer NOT NULL,
ico_w integer NOT NULL DEFAULT '0',
ico_h integer NOT NULL DEFAULT '0',
thumb_w integer NOT NULL DEFAULT '0',
thumb_h integer NOT NULL DEFAULT '0',
big_w integer NOT NULL DEFAULT '0',
big_h integer NOT NULL DEFAULT '0',
src_w integer NOT NULL DEFAULT '0',
src_h integer NOT NULL DEFAULT '0',
PRIMARY KEY(id) )");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_BONUS_PICS." created<br>";

$this->db->exec("CREATE INDEX item_file_ind ON ".TABLE_CAT_BONUS_PICS." (item_id)");
//echo mysql_error();

//========================== ITEM's VIDEO TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_VIDEO." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_VIDEO."(
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
add_date datetime,
filename varchar(255) NOT NULL DEFAULT '',
filename_ico varchar(255) NOT NULL DEFAULT '',
sort_num integer NOT NULL,
ico_w integer NOT NULL DEFAULT '0',
ico_h integer NOT NULL DEFAULT '0',
src_w integer NOT NULL DEFAULT '0',
src_h integer NOT NULL DEFAULT '0',
tube_code text,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_BONUS_VIDEO." created<br>";

$this->db->exec("CREATE INDEX video_gal_ind ON ".TABLE_CAT_BONUS_VIDEO." (item_id)");

//========================== ITEM's VIDEO LANG TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_VIDEO_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_VIDEO_LANGS." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
lang_id integer NOT NULL,
title varchar(255) NOT NULL DEFAULT '',
descr text,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_BONUS_VIDEO_LANGS." created<br>";

$this->db->exec("CREATE INDEX item_lang_ind ON ".TABLE_CAT_BONUS_VIDEO_LANGS." (item_id, lang_id)");
//echo mysql_error();
 
*/

/*
//========================== ITEM's FILES TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_FILES." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_FILES." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
file_type integer NOT NULL DEFAULT '0',
add_date datetime,
filename varchar(255) NOT NULL DEFAULT '',
sort_num integer NOT NULL,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_BONUS_FILES." created<br>";

$this->db->exec("CREATE INDEX prod_ind ON ".TABLE_CAT_BONUS_FILES." (item_id)");

//========================== ITEM's FILES LANG TABLE ===========================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_FILES_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_ITEMS_FILES_LANGS." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
lang_id integer NOT NULL,
title varchar(255) NOT NULL DEFAULT '',
descr text,
PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_ITEMS_FILES_LANGS." created<br>";

$this->db->exec("CREATE INDEX item_lang_ind ON ".TABLE_CAT_ITEMS_FILES_LANGS." (item_id, lang_id)");
//echo mysql_error();
*/

/*
//======================= COMMENTS TABLE ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_COMMENT." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_COMMENT." (
id integer NOT NULL AUTO_INCREMENT,
item_id integer NOT NULL,
sort_num int DEFAULT '0',
visible tinyint DEFAULT '0',
rate tinyint DEFAULT '0',
add_date datetime DEFAULT NULL,
author varchar(255) NOT NULL,
author_email varchar(255) NOT NULL,
PRIMARY KEY (`id`),
KEY product_id (`item_id`))");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_BONUS_COMMENT." created<br>";

//======================= COMMENTS LOCAL LANG ================================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_COMMENT_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_COMMENT_LANGS." (
id integer NOT NULL AUTO_INCREMENT,
item_id integer NOT NULL,
lang_id integer NOT NULL,
content text,
PRIMARY KEY (`id`),
KEY item_id (`item_id`),
KEY lang_id (`lang_id`))");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_BONUS_COMMENT_LANGS." created<br>";

//=========================== ITEM RATE TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_RATE." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_RATE." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
dt date NOT NULL,
amount integer NOT NULL DEFAULT '0',
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_CAT_BONUS_RATE." created<br>";

$this->db->exec("CREATE INDEX rate_item_ind ON ".TABLE_CAT_BONUS_RATE." (item_id)");
//echo mysql_error();
$this->db->exec("CREATE INDEX rate_dt_ind ON ".TABLE_CAT_BONUS_RATE." (dt)");
//echo mysql_error();

*/

/*
//=========================== ITEM BONUS WINS TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_WINS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_WINS." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
user_id integer NOT NULL,
point_ind integer NOT NULL,
add_date datetime NOT NULL,
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_CAT_BONUS_WINS." created<br>";

$this->db->exec("CREATE INDEX item_ind ON ".TABLE_CAT_BONUS_WINS." (item_id)");
//echo mysql_error();
$this->db->exec("CREATE INDEX user_ind ON ".TABLE_CAT_BONUS_WINS." (user_id)");

//=========================== ITEM BONUS QUIZ TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_QUIZ." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_QUIZ." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
user_id integer NOT NULL,
point_total integer NOT NULL,
add_date datetime NOT NULL,
status tinyint NOT NULL DEFAULT '0',
ses_id varchar(128) NOT NULL DEFAULT '',
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_CAT_BONUS_QUIZ." created<br>";

$this->db->exec("CREATE INDEX item_ind ON ".TABLE_CAT_BONUS_QUIZ." (item_id)");
$this->db->exec("CREATE INDEX user_ind ON ".TABLE_CAT_BONUS_QUIZ." (user_id)");
//echo mysql_error();
$this->db->exec("CREATE INDEX ses_ind ON ".TABLE_CAT_BONUS_QUIZ." (ses_id)");
//echo mysql_error();

//=========================== ITEM BONUS QUIZ TABLE ====================
$this->db->exec("DROP TABLE ".TABLE_CAT_BONUS_QUIZ_POINTS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_BONUS_QUIZ_POINTS." (
id integer NOT NULL auto_increment,
item_id integer NOT NULL,
quiz_id integer NOT NULL,
point_ind integer NOT NULL,
point_total integer NOT NULL,		
add_date datetime NOT NULL,
value tinyint NOT NULL DEFAULT '0',
opened tinyint NOT NULL DEFAULT '0',
ses_id varchar(128) NOT NULL DEFAULT '',
PRIMARY KEY(id)  ) ");

//echo mysql_error()."<br>";
echo TABLE_CAT_BONUS_QUIZ_POINTS." created<br>";

$this->db->exec("CREATE INDEX item_ind ON ".TABLE_CAT_BONUS_QUIZ_POINTS." (item_id)");
//echo mysql_error();
$this->db->exec("CREATE INDEX ses_ind ON ".TABLE_CAT_BONUS_QUIZ_POINTS." (ses_id)");
//echo mysql_error();

*/


/*
//================================ CURRENCY =================================
$this->db->exec("DROP TABLE ".TABLE_SHOP_BUYER_SECTS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_SHOP_BUYER_SECTS."(
  id integer NOT NULL auto_increment,
  buyer_id integer NOT NULL DEFAULT '0',
  sect_id integer NOT NULL DEFAULT '0',
  add_date datetime,
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_SHOP_BUYER_SECTS." created<br>";

// Create indexes
$this->db->exec("CREATE INDEX buyer_index ON ".TABLE_SHOP_BUYER_SECTS." (buyer_id)");
$this->db->exec("CREATE INDEX sect_index ON ".TABLE_SHOP_BUYER_SECTS." (sect_id)");
//echo mysql_error(); 
*/

/*
//================================ CURRENCY =================================
$this->db->exec("DROP TABLE ".TABLE_CAT_CURRENCY." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_CURRENCY."(
  id integer NOT NULL auto_increment,
  sort_num integer NOT NULL DEFAULT '0',
  cur_symb varchar(12) NOT NULL DEFAULT '',
  cur_code varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_CURRENCY." created<br>";

//================================ CITIES LANG ===============================
$this->db->exec("DROP TABLE ".TABLE_CAT_CURRENCY_LANGS." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_CAT_CURRENCY_LANGS."(
  id integer NOT NULL auto_increment,
  item_id integer NOT NULL,
  lang_id integer NOT NULL,
  name varchar(128) NOT NULL,
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_CAT_CURRENCY_LANGS." created<br>";

// Create indexes
$this->db->exec("CREATE INDEX cur_index ON ".TABLE_CAT_CURRENCY_LANGS." (item_id, lang_id)");
//echo mysql_error(); 
*/

/*
//================================ OBLAST =================================
$this->db->exec("DROP TABLE ".TABLE_REGION." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_REGION."(
  id integer NOT NULL auto_increment,
  sort_num integer NOT NULL DEFAULT '0',
  country_id integer NOT NULL,
  country_code varchar(3) NOT NULL DEFAULT 'US',  
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_REGION." created<br>";

//================================ CITIES LANG ===============================
$this->db->exec("DROP TABLE ".TABLE_REGION_LANG." ");
//echo mysql_error()."<br>";

$this->db->exec("CREATE TABLE ".TABLE_REGION_LANG."(
  id integer NOT NULL auto_increment,
  region_id integer NOT NULL,
  lang_id integer NOT NULL,
  name varchar(128) NOT NULL,
  PRIMARY KEY(id)  ) ");

//echo "<h1>".mysql_error()."</h1><br>";
echo TABLE_REGION_LANG." created<br>";

// Create indexes
$this->db->exec("CREATE INDEX country_index ON ".TABLE_REGION." (country_id)");
$this->db->exec("CREATE INDEX region_index ON ".TABLE_REGION_LANG." (region_id, lang_id)");
//echo mysql_error();
*/	
	
/*
//=========================== SUBSCRIBE TABLE ====================
mysql_query("DROP TABLE $TABLE_SUBSCRIBE");
echo mysql_error()."<br>";

mysql_query("CREATE TABLE $TABLE_SUBSCRIBE(
  id integer NOT NULL auto_increment,
  isactive integer NOT NULL DEFAULT '1',
  add_date datetime NOT NULL,
  email varchar(128) NOT NULL,
  PRIMARY KEY(id)  ) ");

echo mysql_error()."<br>";
echo "$TABLE_SUBSCRIBE created<br>";

mysql_query("CREATE INDEX email_ind ON $TABLE_SUBSCRIBE (email)");
echo mysql_error();
*/

/*
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN regdoc1 varchar(255) DEFAULT ''");
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN regdoc2 varchar(255) DEFAULT ''");
	
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN pic varchar(255) DEFAULT ''");
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN pic_sm varchar(255) DEFAULT ''");

	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN orgname varchar(255) DEFAULT ''");	
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN orgsphere varchar(255) DEFAULT ''");	
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN phone varchar(64) DEFAULT ''");	
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN employee_num varchar(20) DEFAULT ''");
  
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN fb_uid varchar(255) DEFAULT ''");	
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN vk_uid varchar(255) DEFAULT ''");	
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYERS." ADD COLUMN od_uid varchar(255) DEFAULT ''");	
	
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYER_AUTH." ADD COLUMN sc_uid varchar(64) NOT NULL DEFAULT ''");
	$this->db->exec("ALTER TABLE ".TABLE_SHOP_BUYER_AUTH." ADD COLUMN sc_token text");
	
	$this->db->exec("ALTER TABLE ".TABLE_CAT_CATALOG." ADD COLUMN filename_thumb_sel varchar(255) NOT NULL DEFAULT ''");

	////////////////////////////////////////////////////////////////////////////
	// Make other initialization
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (1, 'footcopy', 'Копирайт в футере')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (1, 1, 1, '&copy; wayofhelp.com.ua - Все права защищены')");	
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (2, 'poplog', 'Окно входа')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (2, 2, 1, 'Вы можете осуществить вход воспользовавшись вашим логино и паролем, или используя свои учетные записи в соц.сетях')");	
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (3, 'popreg', 'Окно регистрация')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (3, 3, 1, 'Регистрация позволит вам размещать объявления о получении или подаче помощи; принимать предложения и заявки помощи от других участников системы; делать к заявкам пометки. Приглашаем вас получить тестовый доступ для размещения ваших заявок и предложений!')");	
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (4, 'popregpers', 'Окно регистрация - физ.лицо')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (4, 4, 1, '<ul><li>Автоматически ежедневно получать свежие заявки и предложения;</li><li>Автоматически еженедельно рассылать уведомления.</li></ul')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (5, 'popregcomp', 'Окно регистрация - компания')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (5, 5, 1, '<ul><li>Автоматически ежедневно получать свежие заявки и предложения;</li><li>Автоматически еженедельно рассылать уведомления.</li></ul')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (6, 'popregorg', 'Окно регистрация - организация')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (6, 6, 1, '<ul><li>Автоматически ежедневно получать свежие заявки и предложения;</li><li>Автоматически еженедельно рассылать уведомления.</li></ul')");		
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (7, 'regformpers', 'Форма регистрации - физ.лицо.')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (7, 7, 1, 'Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей')");	
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (8, 'regformcomp', 'Форма регистрации - компания')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (8, 8, 1, 'Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей')");	
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (9, 'regformorg', 'Форма регистрации - организация')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (9, 9, 1, 'Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей: Здравствуйте! Вы можете войти на наш сайт с помощью аккаунтов соцсетей')");	
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (10, 'regdone', 'Форма регистрации - завершение')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (10, 10, 1, 'На ваш почтовый ящик было отправлено письмо с подтверждением регистрации. Пройдите по ссылке в письме, чтобы активировать учетную запись.')");	
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (11, 'regactivate', 'Форма активации регистрации')");	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (11, 11, 1, 'Вы только что активировали вашу учетную запись и теперь вы можете войти на сайт под свои логином и паролем.')");	
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (12, 'pophelpmoney', 'Окно оказания помощи (деньги)')");
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (12, 12, 1, 'Укажите сумму, которую вы готовы пожертвовать на этот проект.')");
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (13, 'pophelphuman', 'Окно оказания помощи (люди)')");
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (13, 13, 1, 'Если вы компания или организация, то задайте кол-во человек, которые вы можете выделить для оказания помощи.')");
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (14, 'pophelpsome', 'Окно оказания помощи (услуги)')");
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (14, 14, 1, 'Укажите, чем конкретно вы можете помочь и на каких условиях.')");
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (15, 'mainslogan', 'Главная - слоган')");
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (15, 15, 1, 'ПОМОГАЙ,<br>ПОЛУЧАЙ,<br>ДЕЛИСЬ!')");
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (16, 'mainslogantxt', 'Главная - под слоганом')");
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (16, 16, 1, 'Хочешь получить медицинскую, юридическую или любую другую консультацию или помощь? Все возможно. Присоединяйся!')");
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (17, 'popvideo', 'Главная - видео всплывающее')");
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (17, 17, 1, '')");
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (18, 'popstarrev', 'Форма оценки помощи')");
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (18, 18, 1, 'Вы можете поставить оценку тому, кто оказал вам помощь. Отнеситесь серьезно к этой задаче, так как оценка повлияет на рейтинг этого пользователя, а ваш комментарий увидят другие посетители сайта.')");
	
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE." (id, name, title) VALUES (19, 'prodreqsend', 'Ваше предложение помощи отправлено')");
	$this->db->exec("INSERT INTO ".TABLE_RESOURCE_LANGS." (id, item_id, lang_id, content) VALUES (20, 20, 1, 'Спасибо, что вы не остались равнодушним, и отправили предложение о помощи нуждавшемуся в ней.<br>')");
*/
	
	
/*
	mysql_query("INSERT INTO $TABLE_RESOURCE (id, name, title) VALUES (1, 'worktime', 'Время работы')");
	echo mysql_error()."<br />";
    mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (1, 1, 1, 'Пн-Пт. 10:00  — 18:00, Сб. 10:00 — 17:00')");
    echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (2, 1, 2, 'Рабочее время:<br/> Пн.- Пт.: с 9:00 до 18:00')");
    //echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (3, 1, 3, 'Рабочее время:<br/> Пн.- Пт.: с 9:00 до 18:00')");
    //echo mysql_error()."<br />";

    mysql_query("INSERT INTO $TABLE_RESOURCE (id, name, title) VALUES (2, 'ordermade', 'Заказ принят')");
    echo mysql_error()."<br />";
    mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (2, 2, 1, 'Ваш заказ будет обработан в течении часа и с вами свяжется на менеджер по продажам для уточнения способа оплаты и времени доставки.')");
    echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (5, 2, 2, 'Ваш заказ будет обработан в течении часа и с вами свяжется на менеджер по продажам для уточнения способа оплаты и времени доставки.')");
    //echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (6, 2, 3, 'Ваш заказ будет обработан в течении часа и с вами свяжется на менеджер по продажам для уточнения способа оплаты и времени доставки.')");
    //echo mysql_error()."<br />";

    mysql_query("INSERT INTO $TABLE_RESOURCE (id, name, title) VALUES (3, 'paywire', 'Оплата заказа через банковский перевод')");
    echo mysql_error()."<br />";
    mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (3, 3, 1, 'Для оплаты заказа с помощью банковского перевода Вы должны воспользоваться счетом, представленным ниже. Счет содержит реквизиты получателя, по которым должна быть произведена оплата. Оплата может быть произведена в любом отделении любого банка Украины. Взять счет для оплаты можно по этой ссылке: ')");
    echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (8, 3, 2, 'Для оплаты заказа с помощью банковского перевода Вы должны воспользоваться счетом, представленным ниже. Счет содержит реквизиты получателя, по которым должна быть произведена оплата. Оплата может быть произведена в любом отделении любого банка Украины. Взять счет для оплаты можно по этой ссылке: ')");
    //echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (9, 3, 3, 'Для оплаты заказа с помощью банковского перевода Вы должны воспользоваться счетом, представленным ниже. Счет содержит реквизиты получателя, по которым должна быть произведена оплата. Оплата может быть произведена в любом отделении любого банка Украины. Взять счет для оплаты можно по этой ссылке: ')");
    //echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE (id, name, title) VALUES (4, 'paywebmoney', 'Оплата заказа с помощью WebMoney')");
    //echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (4, 4, 1, 'Для оплаты заказа через интернет-систему электронных платежей WebMoney Вы должны быть владельцем электронного кошелька. Если у вас есть электронный кошелек WebMoney, и у вас достаточно средств для оплаты, то вы можете перейти на форму оплаты заказа нажав на кнопку, указанную ниже.')");
    //echo mysql_error()."<br />";
    mysql_query("INSERT INTO $TABLE_RESOURCE (id, name, title) VALUES (4, 'paydeliv', 'Оплата при доставке')");
    echo mysql_error()."<br />";
    mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (4, 4, 1, 'Так же можете выполнить оплату в момент доставки Вам товара. Для этого Вам следует связаться с нашим менеджером по телефону для согласования времент и места доставки товара. С менеджером можно связаться по телефону: ')");
    echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (11, 4, 2, 'Так же можете выполнить оплату в момент доставки Вам товара. Для этого Вам следует связаться с нашим менеджером по телефону для согласования времент и места доставки товара. С менеджером можно связаться по телефону: ')");
    //echo mysql_error()."<br />";
    //mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (12, 4, 3, 'Так же можете выполнить оплату в момент доставки Вам товара. Для этого Вам следует связаться с нашим менеджером по телефону для согласования времент и места доставки товара. С менеджером можно связаться по телефону: ')");
    //echo mysql_error()."<br />";

    mysql_query("INSERT INTO $TABLE_RESOURCE (id, name, title) VALUES (5, 'rekvis', 'Реквизиты')");
    echo mysql_error()."<br />";
    mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (5, 5, 1, 'СПДФЛ Иванов Иван Иванович
ИНН 1111111111
Код ОКПО: 2222222222
Р/счет №: 26000000000000
в ХОФ АКБ \"Укрсоцбанк\"
МФО: 351016')");
    echo mysql_error()."<br />";
*/

    /*
    mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (14, 5, 2, 'СПДФЛ Иванов Иван Иванович
ИНН 1111111111
Код ОКПО: 2222222222
Р/счет №: 26000000000000
в ХОФ АКБ \"Укрсоцбанк\"
МФО: 351016')");
    echo mysql_error()."<br />";
    mysql_query("INSERT INTO $TABLE_RESOURCE_LANGS (id, item_id, lang_id, content) VALUES (15, 5, 3, 'СПДФЛ Иванов Иван Иванович
ИНН 1111111111
Код ОКПО: 2222222222
Р/счет №: 26000000000000
в ХОФ АКБ \"Укрсоцбанк\"
МФО: 351016')");
    echo mysql_error()."<br />";
*/

	/*
	////////////////////////////////////////////////////////////////////////////
    // Add new parameters for all products
    $query = "INSERT INTO $TABLE_CAT_PARAMS ( min_val, max_val, isbasic, param_display_type_id )
    			VALUES ('', '', '1', '$FIELD_TYPE_FILE')";
	if(mysql_query($query))
	{
		$newparid = mysql_insert_id();

		for( $i=0; $i<count($langs); $i++ )
		{
			if( !mysql_query( "INSERT INTO $TABLE_CAT_PARAMS_LANGS ( param_id, lang_id, name, izm, sample )
                   VALUES ('$newparid', '".$langs[$i]."', 'Прайс-лист (у.е.)', '', '')" ) )
			{
				echo mysql_error();
			}
		}

		// Assign parameter to profile
		if( !mysql_query( "INSERT INTO $TABLE_CAT_PROFILE_PARAMS (profile_id, param_id, sort_ind) VALUES
                	('1', '$newparid', '4')" ) )
		{
			echo mysql_error();
		}

		// Now we shoould add param value records to all products
  		$query = "SELECT * FROM $TABLE_CAT_ITEMS WHERE profile_id='1'";
	    if( $res = mysql_query( $query ) )
	    {
	    	while( $row = mysql_fetch_object( $res ) )
	    	{
	    		$item_id = $row->id;

	    		// Insert value for all langs
	    		for( $i=0; $i<count($langs); $i++ )
				{
					if( !mysql_query( "INSERT INTO $TABLE_CAT_PARAM_VALUES ( item_id, param_id, lang_id, value )
		                   VALUES ( $item_id, $newparid, '".$langs[$i]."', '')" ) )
					{
						echo mysql_error();
					}
				}
	    	}
	    	mysql_free_result( $res );
	    }
	}
	else
	{
		echo "<b>".mysql_error()."</b>";
	}
	*/

	/*
	$query = "SELECT c1.id as sectid, c2.* FROM $TABLE_CAT_CATALOG c1
		INNER JOIN $TABLE_CAT_CATALOG_LANGS c2 ON c1.id=c2.sect_id AND c2.lang_id='$LangId'
		ORDER BY c1.id";
	if( $res = mysql_query( $query ) )
	{
		while( $row = mysql_fetch_object( $res ) )
		{
			$query1 = "INSERT INTO $TABLE_CAT_CATTITLES (sect_id, make_id, lang_id, sortmode_id, filter_id, filter_val, add_date, page_title, page_keywords, page_descr, content_text, content_words)
			VALUES (".$row->sectid.",0,".$LangId.",0,0,0,NOW(), '".$row->meta_title."', '".$row->meta_keyw."', '".$row->meta_descr."', '".$row->descr."', '')";
			echo $query1."<br /><br />";
			//if( !mysql_query( $query1 ) )
			//{
			//	echo mysql_error();
			//}
		}
		mysql_free_result( $res );
	}
	*/

	/*
	$query = "SELECT c1.id as sectid, c2.* FROM $TABLE_CAT_CATALOG c1
		INNER JOIN $TABLE_CAT_CATALOG_LANGS c2 ON c1.id=c2.sect_id AND c2.lang_id='$LangId'
		ORDER BY c1.id";
	if( $res = mysql_query( $query ) )
	{
		while( $row = mysql_fetch_object( $res ) )
		{
			$page_url = strtolower(TranslitEncode(stripslashes($row->name)));

			$query1 = "UPDATE $TABLE_CAT_CATALOG SET url='".addslashes($page_url)."' WHERE id=".$row->sectid;
			if( !mysql_query( $query1 ) )
			{
				echo mysql_error();
			}
			//$query1 = "INSERT INTO $TABLE_CAT_CATTITLES (sect_id, make_id, lang_id, sortmode_id, filter_id, filter_val, add_date, page_title, page_keywords, page_descr, content_text, content_words)
			//VALUES (".$row->sectid.",0,".$LangId.",0,0,0,NOW(), '".$row->meta_title."', '".$row->meta_keyw."', '".$row->meta_descr."', '".$row->descr."', '')";
			//echo $query1."<br /><br />";
			//if( !mysql_query( $query1 ) )
			//{
			//	echo mysql_error();
			//}
		}
		mysql_free_result( $res );
	}
	*/
	
	$this->db->setDebug(false);

	//include	"../inc/close-inc.php";
?>