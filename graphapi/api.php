﻿<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 04.06.15
 * Time: 3:08
 */
 
error_reporting(0);
 
$STATIC_DATA_PATH = 'graphapi/';



$js = file_get_contents($STATIC_DATA_PATH."names.json");
$nms = json_decode($js)->names;

$js = file_get_contents($STATIC_DATA_PATH."avas.json");
$avs = json_decode($js)->avas;


/*
 * file_put_contents("out.json", json_encode($nms)."\n".count($nms));
*/



$prjTop = 5;
$ndsTop = 5;
$usrIdTop = 10000;


if (isset($_REQUEST["cnt_prj"]) )
{
	$prjTop = $_REQUEST["cnt_prj"];
}
if (isset($_REQUEST["cnt_usr"]) )
{
	$ndsTop = $_REQUEST["cnt_usr"];
}
if (isset($_REQUEST["max_uid"]) )
{
	$usrIdTop = $_REQUEST["max_uid"];
}

$answ = array();
$arTp = array();

for ($i = 1; $i< 13; $i++)
	$arTp[] = 't'.$i;

$ansData = array();



if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "settings_save")

{

	$sets = array();
	$sets["graph_layout"] = $_REQUEST["graph_layout"];
	$sets["show_avas"] = $_REQUEST["show_avas"];





	$json = json_encode($sets);

	file_put_contents("sets.json",$json);

	$answ = array("status"=>"ok");
	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "node_mainuser_info")

{
	$answ = array("status"=>"ok");

	$ansData ["request"] = "node_mainuser_info";

	$ansData["gid"] = $_REQUEST["gid"];
	$ansData["content"] = "some content about <br> main user - ".$_REQUEST['gid'];

	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "node_project_info")

{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "node_project_info";

	$ansData["gid"] = $_REQUEST["gid"];
	$ansData["content"] = "some content about <br> project - ".$_REQUEST['gid'];

	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "node_user_info")

{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "node_user_info";

	$ansData["gid"] = $_REQUEST["gid"];
	$ansData["content"] = "some content about <br> user - ".$_REQUEST['gid'];

	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}



if (isset($_REQUEST["request"]) && 	$_REQUEST["request"]  == "tree_main_users"){

		$answ = array("status"=>"ok");

		$ansData ["request"] = $_REQUEST["request"];

		// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
		$ansData ["source_node"] = array();

		// data for vis.js
		$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;

		$ansNodes = array();
		$ansEdges = array();


		// build tree for user's help acceptors

		$l1 = mt_rand ( 1 ,$ndsTop );

		for($i = 0; $i<$l1; $i++){

			$uid_1 = mt_rand ( 1 , $usrIdTop );

			$acp = createUser($uid_1, 1, true, false);
			$ansNodes[] = $acp;

			// создаем связь
			// data for vis.js

			$ansEdges[] = createEdge($_REQUEST["gid"], "u".$uid_1, $acp["p_type"]);
		}

		$ansData["nodes"] = $ansNodes ;
		$ansData["edges"] = $ansEdges;
		$answ["data"] = $ansData;


		$json = json_encode($answ);
		echo $json;
		exit;


}

if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "tree_main_projects" )
{
	$answ["status"] = "ok";



	$ansData ["request"] = $_REQUEST["request"] ;

	// main start node
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;

	$lprj = mt_rand ( 1 , $prjTop );

	$ansNodes = array();
	$ansEdges = array();



	// create projects

	$ecnt = 1;

	for($i = 0; $i<$lprj; $i++){

		$prj = array();

		$up_1 = mt_rand ( 1 , $usrIdTop );

		// data for vis.js
		$prj["gid"] = "p".$up_1;

		// data for graph showing
		$prj["p_type"] =  $arTp[ mt_rand ( 0 , 2 )];
		$prj["nd_type"] = "prj";
		$prj["lvl"] = -1;


		// data for user interaction and any other stuff
		$prj["info"] = array();
		$prj["info"]["title"] = "some title - ".$up_1;
		$prj["info"]["content"] = "Lorem Ipsum content";
		$prj["info"]["url_view"] = "http://wayofhelp.com/proj/view/". mt_rand ( 100 , 1000 ) . "/";

		$ansNodes[] = $prj;

		$ed = array();

		// add connection between MAIN_USER and MY_PROJECT

		// data for vis.js





		$ansEdges[] = createEdge($prj["gid"], $ansData ["source_node"]["gid"], $prj["p_type"]);


		$lhlp = mt_rand ( 1 , $ndsTop );

		for($h = 0; $h<$lhlp; $h++){

			$hlp = createUser(mt_rand ( 1 , $usrIdTop ), -2, false, false  );

			$ansNodes[] = $hlp;

			// data for vis.js

			$ansEdges[] = createEdge($hlp["gid"], $prj["gid"], $prj["p_type"]);
		}


	}


	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}







// load full data about user connections


function createProject($gid, $mrShow){

	global $arTp ;

	// data for vis.js
	$prj["gid"] = $gid;

	// data for graph showing
	$prj["p_type"] =  $arTp[ rand ( 0 , count($arTp) -1 ) ];
	$prj["nd_type"] = "prj";
	$prj["lvl"] = -1;


	// data for user interaction and any other stuff
	$prj["info"] = array();
	$prj["info"]["title"] = "some title - ".$gid;
	$prj["info"]["content"] = "Lorem Ipsum content";
	$prj["info"]["url_view"] = "http://wayofhelp.com/proj/view/". mt_rand ( 100 , 1000 ) . "/";

	return $prj;

}

function createUser($gid, $lvl, $mrShow, $mrLoad, $p_type = null){

	global $arTp ;
	global $nms;
	global $avs;

	$acp = array();
	$acp["nd_type"] = "usr";
	if ($p_type != null)
		$acp["p_type"] = $p_type;
	else
		$acp["p_type"] = $arTp[ mt_rand ( 0 , count($arTp) -1 )];
	$acp["gid"] = "u".$gid;
	if ($lvl < 100)
		$acp["lvl"] = $lvl;;

	$acp["size"] = mt_rand ( 10, 30 );

	$acp["info"] = array("uname" => $nms[mt_rand(0, 59)]->name,
		"title" => "accceptor",
		"url_ava" => $avs[rand ( 1 , count($avs))-1],
		"url_view" => "http://wayofhelp.com/users/viewinfo/". mt_rand ( 10 , 100 ) . "/",

		// дублирование dhtvtyb явно не нужно, но с УТЦ удобно, а с форматироваием - читаемо в дебаге

		// есть ли что показать еще, т.е. скрытые пользователи?
		// т.е. надо ли показать узел "Еще"?
		"more_show" => $mrShow,
		// возможно ли загрузить следующий уровень для этого пользователя?
		// т.е. надо ли показать показать пользователю, что можно развернуть дерево дальше?
		"more_load" => $mrLoad
	);

	return $acp;

}

function createEdge($from, $to, $p_type){

	global $arTp ;
	global $nms;
	global $avs;

	$ed = array();
	$ed['from'] = $from ;
	$ed['to'] = $to;

	$utc = mt_rand(1060070279, time());
	$ed["info"] = array(
		"time_date" => date("Y-m-d\TH:i:s", $utc),
		"title" => "some info about doing help"
	);
	$ed["p_type"] = $p_type;
	return $ed;

}

if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "tree_user_show")

{
	$answ = array("status"=>"ok");




	$ansData ["request"] = "tree_user_show";

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;
	$ansData ["source_node"]["more_show"] = (rand(1,10) <4);
	$ansData ["source_node"]["more_load"] = (rand(1,10) <4);





	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors

	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		$acp = createUser($uid_1, 1, true, false);
		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge($_REQUEST["gid"], "u".$uid_1, $acp["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}

// load next level from this user (not full)
if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "tree_user_load")
{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "tree_user_load";

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;
	$ansData ["source_node"]["more_show"] = (rand(1,10) <4);
	$ansData ["source_node"]["more_load"] = (rand(1,10) <4);



	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors

	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );
		$acp = createUser($uid_1, 99999999, true, false );
		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge($_REQUEST["gid"], "u".$uid_1, $acp["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;
		
}



if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "tree_main")
{

	$musrId = $_REQUEST["user_id"];
	$answ["status"] = "ok";



	$ansData["settings"] = array();


	if (file_exists ($STATIC_DATA_PATH."sets.json"))
	{
		$st = file_get_contents($STATIC_DATA_PATH."sets.json");
		$star = json_decode($st);
	}
	else
	{
		$star = array();
		$star["graph_layout"] = "binary_tree";
		$star["show_avas"] = true;
	}


	$star =  (array) $star;


	/*
	if (isset($star["graph_layout"]))
	{
		$ansData["settings"]["graph_layout"] = $star["graph_layout"];
	}
	else
		$ansData["settings"]["graph_layout"] = "binary_tree";

	if (isset($star["show_avas"]))
	{
		$ansData["settings"]["show_avas"] = $star["show_avas"];
	}
	else
		$ansData["settings"]["show_avas"] = false;

	$ansData["settings"]["debug_config"] = array(
		"cnt_prj" => $prjTop,
		"cnt_usr" =>$ndsTop,
		"max_uid" => $usrIdTop);



	$ansData ["request"] = "tree_main";

	// main start node
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] = "mnu" . $musrId;
	// data for graph showing


	// data for user interaction and any other stuff

	$ansData ["source_node"]["info"] = array();

	$lprj = mt_rand ( 1 , $prjTop );

	$ansData ["source_node"]["info"]["more_prj"] = (rand(1,10) <5);
	$ansData ["source_node"]["info"]["more_usr"] = (rand(1,10) <5);
	$ansData ["source_node"]["info"]["uname"]	= "Лариса Попова";
	$ansData ["source_node"]["info"]["url_ava"] = "http://wayofhelp.com/pics/avatar/375_686.jpg";


	$ansNodes = array();
	$ansEdges = array();



	// create projects

	$ecnt = 1;

	for($i = 0; $i<$lprj; $i++){

		$prj = array();

		$up_1 = mt_rand ( 1 , $usrIdTop );

		// data for vis.js
		$prj["gid"] = "p".$up_1;

		// data for graph showing
		$prj["p_type"] =  $arTp[ mt_rand ( 0 , 2 )];
		$prj["nd_type"] = "prj";
		$prj["lvl"] = -1;


		// data for user interaction and any other stuff
		$prj["info"] = array();
		$prj["info"]["title"] = "some title - ".$up_1;
		$prj["info"]["content"] = "Lorem Ipsum content";
		$prj["info"]["url_view"] = "http://wayofhelp.com/proj/view/". mt_rand ( 100 , 1000 ) . "/";

		$ansNodes[] = $prj;

		$ed = array();

		// add connection between MAIN_USER and MY_PROJECT

		// data for vis.js





		$ansEdges[] = createEdge($prj["gid"], $ansData ["source_node"]["gid"], $prj["p_type"]);


		$lhlp = mt_rand ( 1 , $ndsTop );

		for($h = 0; $h<$lhlp; $h++){

			$hlp = createUser("u".mt_rand ( 1 , $usrIdTop ), -2, false, false  );

			$ansNodes[] = $hlp;

			// data for vis.js

			$ansEdges[] = createEdge($hlp["gid"], $prj["gid"], $prj["p_type"]);
		}


	}



	// build tree for user's help acceptors

	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		if ($uid_1 == $musrId)
			continue;

		$acp = createUser($uid_1, 1, true, false);

		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge($ansData ["source_node"]["gid"], "u".$uid_1, $acp["p_type"]);


		$l2 = mt_rand ( 1 ,$ndsTop );

		for($y = 0; $y<$l2; $y++){
			$uid_2 = mt_rand ( 1 , $usrIdTop );
			if ($uid_2 == $musrId)
				continue;
			if ($uid_2 == $uid_1)
				continue;


			$acp = createUser($uid_2, 2, true, false);

			$ansNodes[] = $acp;

			// создаем связь
			// data for vis.js

			$ansEdges[] = createEdge("u".$uid_1, "u".$uid_2, $acp["p_type"]);;

			$l3= mt_rand ( 1 ,$ndsTop );

			for($n = 0; $n<$l3; $n++){
				$uid_3 = mt_rand ( 1 , $usrIdTop );


				if ($musrId == $uid_3)
					continue;
				if ($uid_1 == $uid_3)
					continue;

				$acp = createUser($uid_3, 2, true, false);

				$ansNodes[] = $acp;

				// создаем связь
				// data for vis.js

				$ansEdges[] = createEdge("u".$uid_2, "u".$uid_3, $acp["p_type"]);;


				$l4= mt_rand ( 1 , $ndsTop );

				for($n = 0; $n<$l4; $n++){
					$uid_4 = mt_rand ( 1 , $usrIdTop );


					if ($musrId == $uid_4)
						continue;
					if ($uid_1 == $uid_4)
						continue;

					$acp = createUser($uid_4, 4, true, false);

					$ansNodes[] = $acp;

					// создаем связь
					// data for vis.js

					$ansEdges[] = createEdge("u".$uid_3, "u".$uid_4, $acp["p_type"]);;

				}

			}
		}

	}

	//print_r($answ);
	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;
	*/
	
	////////////////////////////////////////
	// New tree 
	$answ = Array();
	$answ["status"] = "ok";

	$ansData = Array();
	$ansData["settings"] = array();
	
	if (isset($star["graph_layout"]))
	{
		$ansData["settings"]["graph_layout"] = $star["graph_layout"];
	}
	else
		$ansData["settings"]["graph_layout"] = "binary_tree";

	if (isset($star["show_avas"]))
	{
		$ansData["settings"]["show_avas"] = $star["show_avas"];
	}
	else
		$ansData["settings"]["show_avas"] = false;

	$ansData["settings"]["debug_config"] = array(
		"cnt_prj" => $prjTop,
		"cnt_usr" =>$ndsTop,
		"max_uid" => $usrIdTop);


	$ansData ["request"] = "tree_main";
	
	// main start node
	$ansData ["source_node"] = array();
	
	
	// data for user interaction and any other stuff
	$ansData ["source_node"]["info"] = array();

	//$lprj = mt_rand ( 1 , $prjTop );	
	
	$ansData["nodes"] = Array();
	$ansData["edges"] = Array();	
	
	$resp = file_get_contents("http://wayofhelp.com/ajx/graphsrv/?uid=".$musrId);
	//echo $resp;
	
	$resobj = json_decode($resp, true);
	
	//var_dump($resobj['data']['nodes']);
	
	// data for vis.js
	$ansData ["source_node"]["gid"] = "u" . $musrId;
	// data for graph showing
	$ansData ["source_node"]["info"]["more_prj"] = (rand(1,10) <5);
	$ansData ["source_node"]["info"]["more_usr"] = (rand(1,10) <5);
	
	$ansData ["source_node"]["info"]["uname"] = $resobj['data']['nodes'][0]['info']['uname'];
	$ansData ["source_node"]["info"]["url_ava"] = $resobj['data']['nodes'][0]['info']['url_ava'];
	
	//$ansData ["source_node"]["info"]["uname"]	= "Лариса Попова";
	//$ansData ["source_node"]["info"]["url_ava"] = "http://wayofhelp.com/pics/avatar/375_686.jpg";
	

	$ansData["nodes"] = $resobj['data']['nodes'];
	$ansData["edges"] = $resobj['data']['edges'];

	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}
else
{
	$answ["status"]="some error explanation";

	$json = json_encode($answ);
	echo $json;

	exit;
}
