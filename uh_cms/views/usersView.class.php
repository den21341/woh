<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UsersView extends PageView
{
	protected $reqdata;
	protected $months;

	function __construct($config, $pModel)
	{
		parent::__construct($config, $pModel);


		//$sc_tags = '<meta property="og:image" content="http://wayofhelp.com/img/tree_img/1100_1100_scr.png" >';

		//$this->setSeo(SEO_SOCIAL_TAGS, $sc_tags);

		$this->months = Array("января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря");
	}

	public function render_default() {
		//print_r($this->userinfo);
		//$this->renderOpenGraph($this->userinfo, $this->helpcount);

		$this->viewMode = "";
		$this->renderPage("user.php");
	}

	public function render_reviews() {
		//print_r($this->userinfo);
		$this->renderOpenGraph($this->userinfo, $this->helpcount);

		$this->viewMode = "reviews";
		$this->renderPage("user.php");
	}

	public function render_empty()
	{

	}

	protected function build_DateStr($dy, $dm, $dd, $dh, $dmin, $sep=", ")
	{

		$str = sprintf("%d %s %04d%s %02d:%02d", $dd, $this->build_MonthStr($dm), $dy, $sep, $dh, $dmin);

		return $str;
	}

	protected function build_MonthStr($monind, $sklon = 0)
	{
		return $this->months[$monind-1];
	}

	protected function num_2_ending($num)
	{
		$ret = 5;

		$num0 = $num % 100;

		if( $num0 > 20 )
			$num0 = $num0 % 10;

		if( $num0 == 0 )
			$ret = 5;
		else if( $num0 == 1 )
			$ret = 1;
		else if( ($num0 >= 2) && ($num0 <= 4) )
			$ret = 2;

		return $ret;
	}

	protected function DaysStr($daysnum) {
		$day_ind_str = "days_".$this->num_2_ending($daysnum);

		return $this->localize->day_ind_str;
	}

	protected function renderOpenGraph($uinfo, $hlpco = 0) {
		if(isset($uinfo) && $hlpco > 0) {
			$title = $this->localize->get("tree", "shareogtit");
			$share_descr = $this->localize->get("tree","shareogdescr");

			$title = str_replace('_name_', 'Пользователь '.$uinfo['name'].' '.$uinfo['fname'], $title);
			$title = str_replace('_co_', $hlpco, $title);

			$share_descr = str_replace('_co_', ($hlpco < 11) ? rand($hlpco, $hlpco + 10) : rand($hlpco - 10, $hlpco + 10), $share_descr);

			$sc_tags = '<meta property="og:type" content="article">
						<meta property="og:url" content="'.$this->page_BuildUrl('users','viewrev').$uinfo['id'].'/'.'">
						<meta property="og:title" content="'.$title.'">
						<meta property="og:description" content="'.$share_descr.'" >
						<meta property="og:image" content="'.WWWHOST.'img/user_share/ok_'.$uinfo['id'].'.png'.'"/>';

			$this->setSeo(SEO_SOCIAL_TAGS, $sc_tags);
		}
	}

}