<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class RatingView extends PageView
{
	protected $reqdata;
	protected $months;
	
	function __construct($config, $pModel)
	{
		parent::__construct($config, $pModel);
		
		$this->months = Array("января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря");
	}
	
	public function render_default()
	{
		//$this->addBreadcrumbs($this->Page_BuildUrl("cat",$spath[$i]['url']), $spath[$i]['name']);
		$this->addBreadcrumbs($this->Page_BuildUrl("info", "ratrules"), "Правила рейтинга");
		
		//$this->with_fancybox = true;
		//$this->with_magnific_popup = true;
		$this->viewMode = "toptable";		
		//$this->reqdata = $reqd;
				
		$this->renderPage("rating.php");
	}		
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	// Utils
	protected function build_Yes($flag)
	{
		return ( $flag ? '<span class="yes">Да</span>' : ' - ' );
	}
	
	protected function build_RateTable()
	{
		$html = '<table class="rateTbl">
		<tr>
			<th>Место</th>
			<th>Название</th>			
			<th>Рейтинг</th>			
			<th>VIP</th>
			<th>Аватарка</th>
			<th>Фото</th>
			<th>Просмотров</th>
			<th>СЦ</th>
			<th>Оценка</th>
			<th>Кол-во помощи</th>
		</tr>';
		
		for($i=0; $i<count($this->tlist); $i++)
		{
			$it = $this->tlist[$i];
			
			$html .= '<tr>
				<td>'.($i+1).'</td>
				<td class="ta_left"><a href="'.$this->page_BuildUrl("proj", "view/".$it['id']).'" target="_blank">'.$it['title2'].'</a></td>
				<td>'.$it['item_priority_rate'].'</td>				
				<td>'.$this->build_Yes($it['isvip']).'</td>
				<td>'.$this->build_Yes( ($it['pic_sm'] != "") ).'</td>
				<td>'.$this->build_Yes( $it['it_picnum'] ).'</td>
				<td>'.$it['item_rate'].'</td>
				<td>'.$it['sc_share_rate'].'</td>
				<td>'.$it['help_rate']['avgrate'].'</td>						
				<td>'.$it['help_num'].'</td>				
			</tr>';
		}
		
		$html .= '</table>';
		
		return $html;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	protected function build_DateStr($dy, $dm, $dd, $dh, $dmin, $sep=", ")
	{		
		
		$str = sprintf("%d %s %04d%s %02d:%02d", $dd, $this->build_MonthStr($dm), $dy, $sep, $dh, $dmin);
		
		return $str;
	}
	
	protected function build_MonthStr($monind, $sklon = 0)
	{
		return $this->months[$monind-1];
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	protected function num_2_ending($num)
	{
		$ret = 5;
		
		$num0 = $num % 100;
		
		if( $num0 > 20 )
			$num0 = $num0 % 10;
		
		if( $num0 == 0 )
			$ret = 5;
		else if( $num0 == 1 )
			$ret = 1;
		else if( ($num0 >= 2) && ($num0 <= 4) )
			$ret = 2;
		
		return $ret;
	}
	
	protected function DaysStr($daysnum)
	{
		$day_ind_str = "days_".$this->num_2_ending($daysnum);
		
		return $this->localize->$day_ind_str;
	}
}
?>