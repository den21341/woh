<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class RegistrationView extends PageView
{
	protected $regmode;
	protected $reqdata;
	
	public function render_default()
	{
		$this->renderPage("main.php");
	}	
	
	public function render_regform($regmode = "person", $reqdata)
	{		
		$this->reqdata = $reqdata;
		$this->regmode = $regmode;		
		$this->renderPage("registration.php");
	}
	
	public function render_regdone($regmode = "person")
	{
		$this->regmode = "done_".$regmode;
		$this->renderPage("registration.php");
	}
	
	public function render_attachform($reqdata)
	{
		$this->regmode = "attach";
		$this->reqdata = $reqdata;
		$this->renderPage("registration.php");
	}
	
	public function render_activate($ok = false)
	{
		$this->regmode = "activate";
		$this->renderPage("registration.php");
	}
}
?>