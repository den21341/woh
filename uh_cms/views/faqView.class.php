<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class FaqView extends PageView
{
	protected $reqdata;
	protected $months;
	
	function __construct($config, $pModel)
	{
		parent::__construct($config, $pModel);
		
		$this->months = Array("января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря");
	}
	
	public function render_default()
	{
		//$this->addBreadcrumbs($this->Page_BuildUrl("cat",$spath[$i]['url']), $spath[$i]['name']);
		//$this->addBreadcrumbs($this->Page_BuildUrl("info", "ratingrules"), "Правила рейтинга");
		
		//$this->with_fancybox = true;
		//$this->with_magnific_popup = true;
		//$this->viewMode = "toptable";		
		//$this->reqdata = $reqd;
				
		$this->renderPage("faq.php");
	}		
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	// Utils
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	protected function build_DateStr($dy, $dm, $dd, $dh, $dmin, $sep=", ")
	{		
		
		$str = sprintf("%d %s %04d%s %02d:%02d", $dd, $this->build_MonthStr($dm), $dy, $sep, $dh, $dmin);
		
		return $str;
	}
	
	protected function build_MonthStr($monind, $sklon = 0)
	{
		return $this->months[$monind-1];
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	protected function num_2_ending($num)
	{
		$ret = 5;
		
		$num0 = $num % 100;
		
		if( $num0 > 20 )
			$num0 = $num0 % 10;
		
		if( $num0 == 0 )
			$ret = 5;
		else if( $num0 == 1 )
			$ret = 1;
		else if( ($num0 >= 2) && ($num0 <= 4) )
			$ret = 2;
		
		return $ret;
	}
	
	protected function DaysStr($daysnum)
	{
		$day_ind_str = "days_".$this->num_2_ending($daysnum);
		
		return $this->localize->$day_ind_str;
	}
}
?>