<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CatdoView extends PageView
{
	protected $reqdata;
	
	public function render_default()
	{
		$this->viewMode = "";
		
		if( isset($this->sinf) )
		{
			$this->setSeo(SEO_TITLE, $this->sinf['name']." оказание помощи - ".$this->cfg['NAME_RU']);
			$this->setSeo(SEO_KEYW, $this->sinf['name'].", помощь");
			$this->setSeo(SEO_DESCR, "Просьбы о помощи обычных людей по теме ".$this->sinf['name'].". Смотрите все запросы на сайте ".$this->cfg['NAME_RU']);
		}
		
		$this->renderPage("catalogdo.php");
	}	
	
	/*
	public function render_messages()
	{
		$this->viewMode = "messages";
		
		$this->renderPage("messages.php");
	}
	*/
	
	public function render_combo_country($cits, $cselid=0)
	{
		$out = '';
		for($i=0; $i<count($cits); $i++)
		{
			$out .= '<option value="'.$cits[$i]['id'].'"'.($cits[$i]['id'] == $cselid ? ' selected' : '').'>'.$cits[$i]['name'].'</option>';
		}
		
		return $out;
	}
	
	public function Catalog_BuildUrl($caturl)
	{
		//$url = $this->Page_BuildUrl("cat", "view");
		
		$curl = $this->Page_BuildUrl("catdo", $caturl);
		//$curl = $url."/".$caturl;
		
		return $curl;
	}
	public function render_successfull()
	{
		$this->renderPage('successfull.php');
	}
}
?>