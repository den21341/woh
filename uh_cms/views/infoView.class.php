<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class InfoView extends PageView
{
	protected $reqdata;
	
	public function render_default()
	{
		$this->renderPage("login.php");
	}		
	
	public function render_infopage()
	{
		$this->renderPage("info.php");
	}

	public function render_siterules() {
		$this->renderPage('siterules.php');
	}

	public function render_otherpage() {
		$this->renderPage('infoother.php');
	}

	public function render_rules() {
		$this->renderPage('info_rules.php');
	}

	public function render_tree() {
		$this->renderPage('info_tree.php');
	}

	public function render_about() {
		$this->renderPage('info_about.php');
	}

	public function render_work() {
		$this->renderPage('info_work.php');
	}

	public function render_angels() {
		$this->renderPage('info_angels.php');
	}
	
	public function render_project() {
		$this->renderPage('info_project.php');
	}
	public function render_riseproject() {
		$this->renderPage('info_riseproject.php');
	}
	public function render_projectpract() {
		$this->renderPage('info_projectpract.php');
	}
	public function render_agreement() {
		$this->renderPage('info_agreement.php');
	}
}