<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class PromoView extends PageView
{
	protected $reqdata;
	
	public function render_default()
	{
		//$this->renderPage("login.php");
	}		
	
	public function render_promopage($infile)
	{
		//$this->renderPage("info.php");
		$this->renderPage("promo/".$infile);
	}
}
?>