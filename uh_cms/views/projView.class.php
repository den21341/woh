<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ProjView extends PageView
{
	protected $reqdata;
	protected $months;
	
	function __construct($config, $pModel)
	{
		parent::__construct($config, $pModel);
		
		$this->months = Array("января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря");
	}
	
	public function render_default($reqd)
	{
		$spath = $this->spath;
		for( $i=(count($spath)-1); $i>=0; $i-- )
		{
			//$this->pageView->addBreadcrumbs($this->Catalog_BuildUrl($spath[$i]['url']), $spath[$i]['name']);
			$this->addBreadcrumbs($this->Page_BuildUrl("cat",$spath[$i]['url']), $spath[$i]['name']);
		}
		
		//$this->with_fancybox = true;
		$this->with_magnific_popup = true;
		$this->viewMode = "";		
		$this->reqdata = $reqd;

		if( isset($this->projinfo) )
		{
			$this->setSeo(SEO_TITLE, $this->projinfo['title']." - ".$this->cfg['NAME_RU']);
			$this->setSeo(SEO_KEYW, " ");
			
			$pdescr = strip_tags($this->projinfo['descr0']);

			if(strlen($pdescr) > 160) {
				$pos = strpos($pdescr, ' ', 160);
				$pdescr = substr($pdescr, 0, $pos);
			}

			$this->setSeo(SEO_DESCR, str_replace("\"", "&quot;", $pdescr.". Помогите на сайте ".$this->cfg['NAME_RU']));
			$sc_tags = "";
			
			$sc_pic_tag = '';
			$pics = $this->projinfo['photos'];
			if( count($pics)>0 )
			{
				$sc_pic_tag = '<meta property="og:image" content="'.WWWHOST.$pics[0]['filename_thumb'].'" >';
			}
			
			$sc_tags = '<meta property="fb:app_id" content="803813303006196" >
<meta property="og:type" content="article">
<meta property="og:site_name" content="'.$this->cfg['NAME_RU'].'">
<meta property="og:title" content="'.str_replace("\"", "&quot;", $this->seo[SEO_TITLE]).'">
<meta property="og:url" content="'.$this->page_BuildUrl("proj","view/".$this->projinfo['id']).'">
'.$sc_pic_tag.'
<meta property="og:description" content="'.str_replace('"', "&quot;", $this->seo[SEO_DESCR]).'" >';
			/*
			<meta property="fb:app_id" content="205429962821175" >
			<meta property="fb:admins" content="100001909172605, 100001909618416, 1414700856" >
			<meta property="og:type" content="product" >
			<meta property="og:site_name" content="rozetka" >
			<meta property="og:title" content="Samsung Galaxy J7 J700H/DS Gold" >
			<meta property="og:url" content="http://rozetka.com.ua/samsung_galaxy_j7_ds_gold/p3818771/" >
			<meta property="og:image" content="http://i2.rozetka.ua/goods/1112690/samsung_galaxy_j7_ds_gold_1112690584.jpg" >
			<meta property="og:description" content="Экран 5.5&quot; Super AMOLED+ (1280x720, емкостный, Multi-Touch) / моноблок / процессор (1.5 ГГц) / ОЗУ 1.5 ГБ / камера 13 Мп + фронтальная 5 Мп / Bluetooth 4.1 / Wi-Fi b/g/n / 16 ГБ встроенной памяти + поддержка microSD / разъем 3.5 мм / поддержка 2-х SIM-карт / 3G / GPS / ГЛОНАСС / OC Android 5.1 (Lollipop) / 152.2 x 78.6 x 7.5 мм, 171 г / золотистый" >
			*/
			
			$this->setSeo(SEO_SOCIAL_TAGS, $sc_tags);
		}
		
		$this->renderPage("project.php");
	}

	public function render_addnotallowed($type = 'notallowed') {
		$this->viewMode = $type;
		$this->renderPage("project.php");
	}
	
	public function render_formedit($reqd)
	{
		$this->reqdata = $reqd;
		$this->viewMode = "edit";
		$this->title = $this->localize->get("projdone", "h1-edit");	// "Редактирование проекта";
		
		$this->addBreadcrumbs($this->page_BuildUrl("cabinet","needhelp"), $this->localize->get("projdone", "breadcr-myproj"));
		$this->addBreadcrumbs($this->page_BuildUrl("proj","view/".$reqd->id), $reqd->tit);
		
		$this->renderPage("project.php");
	}
	
	public function render_savedone()
	{
		$this->viewMode = "saved";
		$this->title = $this->localize->get("projdone", "h1-saved");	//"Информация о проекте сохранена";
	
		$this->addBreadcrumbs($this->page_BuildUrl("cabinet","needhelp"), $this->localize->get("projdone", "breadcr-myproj"));
	
		$this->renderPage("project.php");
	}
	
	public function render_formnew($reqd)
	{
		$this->reqdata = $reqd;
		$this->viewMode = "create";
		$this->title = $this->localize->get("projdone", "h1-new");			//"Создание нового проекта";		
		if( $this->dream_mode )
			$this->title = $this->localize->get("projdone", "h1-new-dream");//"Реализуй свою мечту";
		if( $this->event_mode )
			$this->title = $this->localize->get("projdone", "h1-new-event");//"Создание нового события";
		if( $this->thing_mode )
			$this->title = $this->localize->get("projdone", "h1-new-thing");//"Поделись вещами";
		if( $this->myhelp_mode )
			$this->title = $this->localize->get("projdone", "h1-new-myhelp");//"Окажу помощь желающим";
				
		$this->addBreadcrumbs($this->page_BuildUrl("cabinet","needhelp"), $this->localize->get("projdone", "breadcr-myproj"));
		
		$this->renderPage("project.php");
	}
	
	public function render_newdone()
	{
		$this->viewMode = "created";
		$this->title = $this->localize->get("projdone", "h1-created");			//"Вы создали проект на получение помощи";
		
		$this->addBreadcrumbs($this->page_BuildUrl("cabinet","needhelp"), $this->localize->get("projdone", "breadcr-myproj"));
		
		$this->renderPage("project.php");
	}
	
	public function render_newfinish()
	{
		$this->viewMode = "finished";
		$this->title = $this->localize->get("projdone", "h1-created");			//"Вы создали проект на получение помощи";
		
		$this->addBreadcrumbs($this->page_BuildUrl("cabinet","needhelp"), $this->localize->get("projdone", "breadcr-myproj"));
		
		$this->renderPage("project.php");
	}
	
	public function render_helpdone()
	{
		$this->viewMode = "helpdone";
		$this->title = $this->localize->get("projdone", "h1-helpdone");			//"Вы отправили заявку на оказание помощи";
	
		$this->addBreadcrumbs($this->page_BuildUrl("cabinet","needhelp"), $this->localize->get("projdone", "breadcr-myproj"));
		$this->addBreadcrumbs($this->page_BuildUrl("proj","view/".$this->projinfo['id']), $this->projinfo['title2']);
	
		$this->renderPage("project.php");
	}
	
	public function render_thingdone()
	{
		//echo "things<br>";
		
		$this->viewMode = "thingdone";
		$this->title = $this->localize->get("projdone", "h1-thingdone");			//"Вы отправили заявку на получение вещи";
	
		$this->addBreadcrumbs($this->page_BuildUrl("cabinet","needhelp"), $this->localize->get("projdone", "breadcr-myproj"));
		$this->addBreadcrumbs($this->page_BuildUrl("proj","view/".$this->projinfo['id']), $this->projinfo['title2']);
	
		$this->renderPage("project.php");
	}
	
	public function render_myhelpdone()
	{
		//echo "things<br>";
		
		$this->viewMode = "myhelpdone";
		$this->title = $this->localize->get("projdone", "h1-myhelpdone");			//"Вы отправили заявку на получение помощи";
	
		$this->addBreadcrumbs($this->page_BuildUrl("cabinet","needhelp"), $this->localize->get("projdone", "breadcr-myproj"));
		$this->addBreadcrumbs($this->page_BuildUrl("proj","view/".$this->projinfo['id']), $this->projinfo['title2']);
	
		$this->renderPage("project.php");
	}
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	// Utils
	protected function render_SectCombo_Help($selid=0)
	{			
		return $this->_combo_SectLevel(CAT_HELPGIVE_MODE, 0, 0, $selid); 			
	} 	
	
	protected function render_SectCombo($selid=0)
	{			
		return $this->_combo_SectLevel(CAT_HELPGET_MODE, 0, 0, $selid, RSECT_ID_THING); 			
	} 	
	
	protected function render_SubSectCombo($sectgroupid, $selid=0)
	{
		return $this->_combo_SectLevel(CAT_HELPGET_MODE, $sectgroupid, 1, $selid);
	}
	
	private function _combo_SectLevel($sgroup, $sid, $level=0, $selid=0, $skipsect=0)
	{
		$out = '';
		
		$catLib = $this->catmodel;
		
		if( ($skipsect != 0) && ($skipsect == $sid) )
			return '';
		
		$sect0 = $catLib->Catalog_SectLevel($sgroup, $sid);
		for( $i=0; $i<count($sect0); $i++ )
		{
			if( $skipsect == $sect0[$i]['id'] )
				continue;
			
			$spaces = '';
			for($j=0; $j<$level; $j++)
				$spaces .= ' &nbsp;&nbsp;&nbsp; ';
			$out .= '<option value="'.( $level == 0 ? 0 : $sect0[$i]['id'] ).'"'.( $sect0[$i]['id'] == $selid ? ' selected' : '' ).'>'.$spaces.$sect0[$i]['name'].'</option>';
			$out .= $this->_combo_SectLevel($sgroup, $sect0[$i]['id'], $level+1, $selid, $skipsect);
		}
		
		return $out;	
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	protected function build_DateStr($dy, $dm, $dd, $dh, $dmin, $sep=", ")
	{		
		
		$str = sprintf("%d %s %04d%s %02d:%02d", $dd, $this->build_MonthStr($dm), $dy, $sep, $dh, $dmin);
		
		return $str;
	}
	
	protected function build_MonthStr($monind, $sklon = 0)
	{
		return $this->months[$monind-1];
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	public function render_reviews()
	{
		$this->renderPage("reviews.php");
	}

	public function render_prepay() {
		$this->renderPage("project_prepay.php");
	}

	public function render_promise() {
		$this->renderPage("project_promise.php");
	}


	protected function num_2_ending($num)
	{
		$ret = 5;
		
		$num0 = $num % 100;
		
		if( $num0 > 20 )
			$num0 = $num0 % 10;
		
		if( $num0 == 0 )
			$ret = 5;
		else if( $num0 == 1 )
			$ret = 1;
		else if( ($num0 >= 2) && ($num0 <= 4) )
			$ret = 2;
		
		return $ret;
	}
	
	protected function DaysStr($daysnum)
	{
		$day_ind_str = "days_".$this->num_2_ending($daysnum);
		
		return $this->localize->$day_ind_str;
	}
}
?>