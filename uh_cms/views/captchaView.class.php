<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CaptchaView extends View
{
	protected $captchaModel;
	
	function __construct($config, $pModel)
	{
		parent::__construct($config);
		
		$this->captchaModel = $pModel;
	}
	
	public function render_default()
	{
		echo "";
	}

	public function render_image($txt = "AAAAAA")
	{
		$number = $txt;
		
		//echo "!!!";
		
		// Send header through HTTP to client, to determine that it will be image but not HTML page
		header("Content-type: image/png");

		$conv_from = "CP1251";
		$conv_to = "UTF-8";

		$font_file_name = "font/verdana.ttf";
		$font_size = "12";

		// Create image with specified width and height
		$im = imagecreate(100,30);

		// Fill background with color
		$bg_color = imagecolorallocate($im, 0xFF,0xFF,0xFF);
		imagefilledrectangle($im,0,0,imagesx($im),imagesy($im),$bg_color);

		$txt_color = imagecolorallocate($im, 0,0,0);

		for( $i=0; $i<4; $i++ )
		{
			$curletter = $number[$i];
			//$curnumber = $decode_table[$curletter];
			//$curdigit = $curnumber % 10;
			$curdigit = $this->captchaModel->decode_letter($curletter);

			$txt_x = 5 + 25*$i;
			$txt_y = 22;

			imagettftext($im,$font_size,0,$txt_x,$txt_y,$txt_color,$font_file_name, $curdigit);
		}

		// Send Image To Output Stream In PNG Format
		imagepng($im);
		// Destroy Image Object
		imagedestroy($im);
	}	
}
?>