<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define('REQ_PER_PAGE', 50);
define('ITEMS_PER_PAGE', 20);

class CabinetView extends PageView
{
	protected $reqdata;
	
	public function render_default()
	{
		//$this->pn = ITEMS_PER_PAGE;
		$this->viewMode = "";
		$this->renderPage("cabinet.php");
	}	
	
	public function render_messages()
	{
		$this->viewMode = "messages";
		
		$this->renderPage("messages.php");
	}
	
	public function render_loginbox()
	{
		$this->renderPage("cabinet.php");
	}
	
	public function render_restorepass()
	{
		$this->renderPage("cabinet.php");
	}
	
	public function render_proj_needhelp()
	{
		//$this->pn = ITEMS_PER_PAGE;
		$this->viewMode = "needlist";
		
		$this->renderPage("myprojects.php");
	}
	
	public function render_proj_myhelp()
	{
		//$this->pn = ITEMS_PER_PAGE;
		$this->viewMode = "sendlist";
		
		$this->renderPage("myprojects.php");
	}
	
	public function render_proj_myhelpget()
	{
		//$this->pn = ITEMS_PER_PAGE;
		$this->viewMode = "getlist";
		
		$this->renderPage("myprojects.php");
	}
	
	public function render_proj_dohelp()
	{
		//$this->pn = ITEMS_PER_PAGE;
		$this->viewMode = "dolist";
		
		$this->renderPage("myprojects.php");
	}
	
	public function render_proj_givehelp()
	{
		//$this->pn = ITEMS_PER_PAGE;
		$this->viewMode = "givelist";
	
		$this->renderPage("myprojects.php");
	}
	
	public function render_proj_reqlist()
	{
		//$this->pn = REQ_PER_PAGE;
		$this->viewMode = "reqlist";
		
		$this->renderPage("myprojects.php");
	}
	
	public function render_edituserfrm($utype, $req)
	{
		$this->regmode = "edit_".$utype;
		$this->reqdata = $req;
		
		//$this->pageModel->get_page()->title = "Редактирование профиля";
		//$this->pageModel->get_page()->h1 = "Редактирование профиля";
		
		$this->renderPage("registration.php");
	}
	
	public function render_edittopicfrm($utype)
	{
		//$this->regmode = "edittopic_".$utype;
		$this->regmode = "edittopics";
		//$this->reqdata = $req;
	
		$this->renderPage("registration.php");
	}
	
	public function render_mybonuslist()
	{
		$this->viewMode = "bonuslist";
		
		$this->renderPage("mybonus.php");
	}
	public function render_globus()
	{
		$this->viewMode = "globus";

		$this->renderPage("globus.php");
	}
}
?>