<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class AjxView extends View
{
	protected $ajxModel;
	protected $localize;

	function __construct($config, $pModel)
	{
		parent::__construct($config);

		$this->localize = UhCmsApp::getLocalizerInstance();

		$this->ajxModel = $pModel;
	}

	public function render_default()
	{
		// do nothing, Ajx controller can't find appropriate action
	}

	public function render_json($res)
	{
		echo json_encode($res);
	}

	public function render_html($html)
	{
		header('Content-Type: text/plain; charset=utf-8');
		echo $html;
	}

	public function render_result($txt)
	{
		//header("Content-Type: charset=utf-8");
		header('Content-Type: text/plain; charset=utf-8');
		echo $txt;
	}

	public function load_popDialog($dlg_name)
	{
		$load_file = "";
		switch ($dlg_name) {
			case "sharedlg":
				$load_file = "wnd_share-inc.php";
				break;

			case "showtreedlg":
				//$load_file = "wnd_tree_dialog-inc.php";
				$load_file = "wnd_tree_dialog_new-inc.php";
				break;

			case "sendmsgdlg":
				$load_file = "wnd_sendmsg_dialog-inc.php";
				break;

			case "starratedlg":
				$load_file = "wnd_starreview_dialog-inc.php";
				break;

			case "promovideodlg":
				$load_file = "wnd_promovid_dialog-inc.php";
				break;

			case "promohelpdlg2":
				$load_file = "wnd_promohelp_dialog-inc.php";
				break;

			case "promohelpdlg3":
				$load_file = "wnd_promograph_dialog-inc.php";
				break;

			case "restorepdlg":
				$load_file = "wnd_restpass_dialog-inc.php";
				break;

			case "regdlg":
				$load_file = "wnd_reg_dialog-inc.php";
				break;

			case "logdlg":
				$load_file = "wnd_log_dialog-inc.php";
				break;

			case "addmoneydlg":
				$this->helpType = PROJ_TYPE_MONEY;
				$this->projtype = PROJ_NEEDHELP;
				$load_file = "wnd_addmoney_dialog-inc.php";
				break;

			case "addmandlg":
				$this->helpType = PROJ_TYPE_HUMAN;
				$this->projtype = PROJ_NEEDHELP;
				$load_file = "wnd_addmoney_dialog-inc.php";
				break;

			case "addomandlg":
				$this->helpType = PROJ_TYPE_CONSULT;
				$this->projtype = PROJ_NEEDHELP;
				$load_file = "wnd_addmoney_dialog-inc.php";
				break;

			case "getthingdlg":
				$this->helpType = PROJ_TYPE_CONSULT;
				$this->projtype = PROJ_THINGS;
				$load_file = "wnd_addmoney_dialog-inc.php";
				break;

			case "getmyhelpdlg":
				$this->helpType = PROJ_TYPE_CONSULT;
				$this->projtype = PROJ_SENDHELP;
				$load_file = "wnd_addmoney_dialog-inc.php";
				break;

			case "getbonusdlg":
				$load_file = "wnd_getbonus_dialog-inc.php";
				break;

			case 'getsocial':
				$load_file = 'wnd_getsocial_dialog_inc.php';
				break;

			case 'projview':
				$load_file = 'wnd_projview_dialog-inc.php';
				break;
			case 'projcreate':
				$load_file = 'wnd_projcreate_dialog-inc.php';
				break;
			case 'infopopup':
				$load_file = 'wnd_infopopup_dialog-inc.php';
				break;
			case 'payproj':
				$load_file = 'wnd_payproj_dialog-inc.php';
				break;
			case 'socialconn':
				$load_file = 'wnd_socialconn_dialog-inc.php';
				break;
			case 'postregistr':
				$load_file = 'wnd_postregistr_dialog-inc.php';
				break;
			case 'projpayment':
				$load_file = 'wnd-getpaymnt_dialog-inc.php';
				break;
			case "getbonusresdlg":
				$load_file = "wnd_getbonus_result_dialog-inc.php";
				break;
			case "cabpopup":
				$load_file = "wnd_cabpopup_dialog-inc.php";
				break;
			case "gretitude":
				$load_file = "wnd_reqgretir_dialog-inc.php";
				break;
			case "projpopup":
				$load_file = "wnd_projpopup_dialog-inc.php";
				break;
			case "newreg":
				$load_file = "wnd_newreg_dialog-inc.php";
				break;
			case "reqpopup":
				$load_file = "wnd_newreq_dialog-inc.php";
				break;
			case "rateinfo":
				$load_file = "wnd_rateinfo_dialog-inc.php";
				break;
		}

		if ($load_file != "") {
			include $this->cfg['MVC_PATH'] . "templates/" . $load_file;
		} else {
			// do nothing, dialog is unknown
		}
	}

	public function page_BuildUrl($contr, $action, $add = "")
	{
		//return WWWHOST.$contr."/".$action;
		return UhCmsUtils::Page_BuildUrl($contr, $action, $add);
	}

	/////////////////////////////////////////////////////////////////
	// Layout dinamic
	protected function num_2_ending($num)
	{
		$ret = 5;

		$num0 = $num % 100;

		if ($num0 > 20)
			$num0 = $num0 % 10;

		if ($num0 == 0)
			$ret = 5;
		else if ($num0 == 1)
			$ret = 1;
		else if (($num0 >= 2) && ($num0 <= 4))
			$ret = 2;

		return $ret;
	}

	protected function DaysStr($daysnum)
	{
		$day_ind_str = "days_" . $this->num_2_ending($daysnum);

		return $this->localize->$day_ind_str;
	}

	public function reqlist_tpl($reqits, $currency = '')
	{
		$OUT_HTML = "";
		for ($i = 0; $i < count($reqits); $i++) {
			$uname = ($reqits[$i]['account_type'] == USR_TYPE_PERS ? $reqits[$i]['name'] . ' ' . $reqits[$i]['fname'] : $reqits[$i]['orgname']);

			$help_amount = '';
			if ($reqits[$i]['req_type'] == REQ_TYPE_MONEY) {
				$help_amount = number_format($reqits[$i]['req_amount'], ($reqits[$i]['req_amount'] > 999 ? 0 : 2), ",", "") . "<span> " . $currency . "</span>";
			} else if ($reqits[$i]['req_type'] == REQ_TYPE_HUMAN) {
				$help_amount = number_format($reqits[$i]['req_amount'], 0, "", "") . " <span>" . $this->localize->human . "</span>";
			} else {
				$help_amount = number_format($reqits[$i]['req_amount'], 0, "", "") . " <span>" . $this->localize->human . "</span>";
			}

			($reqits[$i]['pic_sm']) ? $uprevImg = WWWHOST . $reqits[$i]['pic_sm'] : $uprevImg = WWWHOST . 'img/no-pic.png';

			$OUT_HTML .= '<div class="pview-rel-it pview-rel-uit">
						<table>
						<tr>
							<td>
								<div class="pview-rel-uprev">' . ($reqits[$i]['author_id'] != ''
					? '<a target="_blank" onclick="yaCounter34223620.reachGoal(\'PushButtonAVA_Right\'); return true;" href="' . $this->Page_BuildUrl("users", "viewrev/" . $reqits[$i]['author_id']) . '"><img class="uprev-img" alt="' . $this->localize->get("successfull", "user") . ' - ' . $uname . '" title="' . $this->localize->get("successfull", "user") . ' - ' . $uname . '" src=' . $uprevImg . ' height="78px" width="78px"></a>'
					: '<img class="uprev-img" alt="' . $this->localize->get("proj", "anon-username") . '" title="' . $this->localize->get("proj", "anon-username") . '" src=' . WWWHOST . 'img/no-usr.png' . ' height="78px" width="78px">') . '
								</div>
							</td>
							<td class="rel-td-w">
								<div class="pview-rel-ucomm">' . ($reqits[$i]['author_id'] != ''
					? '<a target="_blank" onclick="yaCounter34223620.reachGoal(\'PushButtonSsilka_Right\'); return true;" href="' . $this->Page_BuildUrl("users", "viewrev/" . $reqits[$i]['author_id']) . '"><div class="pview-rel-upic pview-rel-uname">' . $uname . '</div></a>'
					: '<div class="pview-rel-upic pview-rel-uname">' . $this->localize->get("proj", "anon-username") . '</div>') . '
									<p class="proj-size-data">' . ($reqits[$i]['daysbefore'] == 0 ? $this->localize->get("projhelp", "today") : $reqits[$i]['daysbefore'] . ' ' . $this->DaysStr($reqits[$i]['daysbefore']) . ' ' . $this->localize->get("projhelp", "ago")) . '</p>
									<div class="pview-rel-ucost">' . $help_amount . '</div>
								</div>
							</td>
						</tr>
						</table>
					</div>';
		}

		return $OUT_HTML;
	}

	public function reqlist_comments($coms)
	{

		for ($i = 0; $i < count($coms); ++$i) {
			echo '<div class="pview-comment-it pview-comm">
								<div class="row">
									<div class="col-md-3 col-xs-3 col-lg-3 pview-comm-border">
										<div class="pview-comm-usr">';
			($coms[$i]['pic']) ? $psrc = (WWWHOST . $coms[$i]['pic']) : $psrc = (WWWHOST . 'img/no-pic.png');
			?><a href="<?= $this->Page_BuildUrl("users", "viewrev/" . $coms[$i]['author_id']) ?>"><img
				class="usr-comm-pic" src='<?= $psrc ?>'
				alt="<?= ($coms[$i]['account_type'] == USR_TYPE_PERS ? $coms[$i]['name'] . ' ' . $coms[$i]['fname'] : $coms[$i]['orgname']) ?>"
				title="<?= ($coms[$i]['account_type'] == USR_TYPE_PERS ? $coms[$i]['name'] . ' ' . $coms[$i]['fname'] : $coms[$i]['orgname']) ?>"
				height="50px" width="50px">
			<span
				class="pview-usr-ico pview-com-usr"><?= ($coms[$i]['account_type'] == USR_TYPE_PERS ? $coms[$i]['name'] . ' ' . $coms[$i]['fname'] : $coms[$i]['orgname']) ?></span>
			</a>
			</div>
			</div>
			<div class="col-md-9 col-xs-7 col-lg-10">
				<div class="pview-comm-txt">
					<div class="pview-comment-msg proj-size-fix"><?= htmlspecialchars($coms[$i]['content']) ?></div>
					<span
						class="pview-comment-dt proj-size-data"><?= $this->localize->get("projview", "dt-comment") . ': ' . $coms[$i]['add_dt'] ?></span>
				</div>
			</div>

			</div>
			</div>
			<?php if (UhCmsApp::getSesInstance()->UserId != 0 && $coms[$i]['author_id'] != UhCmsApp::getSesInstance()->UserId) { ?>
				<a href="#answer" class="proj-answer" uname="<?= $coms[$i]['name'] . '_' . $coms[$i]['fname'] ?>"
				   onclick='$("#comment").val("<?= $coms[$i]['name'] . ' ' . $coms[$i]['fname'] . ', ' ?>"); $("#addcomment").attr("uid", <?= $coms[$i]['author_id'] ?>)'>Ответить</a>

				<?php
			}
		}
	}

	public function newslist_tpl($its, $pi, $pn)
	{
		echo '<div class="row">';
		for ($i = 0; $i < count($its); $i++) {
			if ($i % 4 == 0) { ?>
				<div class="hlpw-row col-md-4 col-lg-4 col-xs-4">
			<?php } ?>
			<div class="row">
				<?php if ($i == 0 || $i == 5 || $i == 8) { ?>
					<a href="<?= WWWHOST . 'publ/helpwall/' . $its[$i]['id'] . '.html' ?>">
						<div class="hlpw-big-news">
							<img
								src="<?= WWWHOST . ($its[$i]['filename_ico'] != '' ? substr($its[$i]['filename_ico'], 3) : 'img/no-pic.png') ?>">
							<p><?= $its[$i]['title'] ?></p>
						</div>
					</a>
				<?php } else { ?>
					<a href="<?= WWWHOST . 'publ/helpwall/' . $its[$i]['id'] . '.html' ?>">
						<div class="hlpw-sm-news">
							<table>
								<tr>
									<td><img
											src="<?= WWWHOST . ($its[$i]['filename_ico'] != '' ? substr($its[$i]['filename_ico'], 3) : 'img/no-pic.png') ?>">
									</td>
									<td><p><?= $its[$i]['title'] ?></p></td>
								</tr>
							</table>
						</div>
					</a>
				<?php } ?>
			</div>
			<?php if ($i == 3 || $i == 7 || $i == 12) { ?>
				</div>
			<?php } ?>
		<?php }
		echo '</div>'; ?>
		</div>
		</div>
	<?php }

	public function gameproj_tpl($its, $num, $date = 0)
	{
		$its_count = count($its);
		$to = $num * 5;
		$from = $to - 5;
		?>
		<div class="wnd-projcreate-tbl">
			<table class="table table-striped">
				<tr class="pre-top-th">
					<th>Имя</th>
					<th>Проект</th>
					<th>Внутреняя валюта за день</th>
					<th>Курс</th>
					<th>Дата</th>
					<th>Переведено на проект</th>
					<th>Всего переведено на проект</th>
				</tr>
				<tr>
					<?php for ($i = $from;
					$i < ($to > $its_count ? $its_count : $to);
					++$i) {
					if ($date === 'now') {
						$its[$i]['pays_coef'] = $its[$i]['pays_send'] = '<span class="dat-upd">Данные обновятся в конце суток</span>';
						$its[$i]['date'] = "Сегодня";
						$its[$i]['local_currency'] = $its[$i]['lc_count'];
					} else if ($date === '2016-06-02') {
						$its[$i]['pays_coef'] = '-';
					}
					?>
					<td class="">
						<div class="t-usr-img"><img
								src=<?= WWWHOST . ($its[$i]['user_pic'] ? $its[$i]['user_pic'] : 'img/no-pic.png') ?> heigh="60px"
								width="60px" alt=""></div>
						<div class="t-usr-txt">
							<div>
								<a href="<?= $this->Page_BuildUrl("users", "viewrev/" . $its[$i]['author_id']) ?>"><?= ($its[$i]['account_type'] == USR_TYPE_PERS ? $its[$i]['name'] : $its[$i]['orgname']) ?></a>
							</div>
						</div>
					</td>
					<td>
						<div class="p-txt">
							<div class="p-tit"><a
									href="<?= $this->Page_BuildUrl('proj', 'view') . $its[$i]['item_id'] ?>"><?= $its[$i]['title2'] ?></a>
							</div>
							<div class="p-date"><?= $its[$i]['add_date'] ?></div>
						</div>
					</td>
					<td>
						<div class="r-rate">
							<span><?= $its[$i]['local_currency'] ?></span>
						</div>
					</td>
					<td class="<?= ($date === 'now' ? 'wnd-sm-td' : '') ?>">
						<div class="r-rate">
							<span><?= ($its[$i]['pays_coef'] ? $its[$i]['pays_coef'] : 0) ?></span>
						</div>
					</td>
					<td>
						<div class="r-rate-date">
							<span><?= ($date !== 'now' ? substr($its[$i]['date'], 0, 10) : $its[$i]['date']) ?></span>
						</div>
					</td>
					<td class="<?= ($date === 'now' ? 'wnd-sm-td' : '') ?>">
						<div class="r-rate">
							<span><?= ($its[$i]['pays_send'] ? $its[$i]['pays_send'] : 0) ?></span>
						</div>
					</td>
					<td>
						<div class="r-rate">
							<span><?= ($its[$i]['pays_all'] ? $its[$i]['pays_all'] : 0) ?></span>
						</div>
					</td>
				</tr>
				<?php } ?>
			</table>
		</div>
	<?php }

	public function poptable_tpl($table_arr, $urate)
	{
		echo ' <table class="rateinfo restruct">
            <thead class="thead rate-thead">
            <tr class="rate-heigh-mid rate-heigh-top">
                <th class="rate-th-pos rate-td text-aligndd">Позиция</th>
                <th class="rate-th-usr rate-td text-aligndd">Информация о пользователе</th>
                <th class="rate-th-rtg text-aligndd">Рейтинг</th>
            </tr>
            </thead>
        </table>
       <div id="tfocus" class="scrollbar res-tfocus">
            <div class="force-overflow">
            <table>
                <tbody class="tbody rate-tbody">';

		foreach ($table_arr as $item) {
			echo '<tr id="' . ($item['user_rank'] == $urate && $item['buyer_id'] == UhCmsApp::getSesInstance()->UserId ? 'target' : '') . '" class="repos rate-heigh-top rate-heigh-mid rate-tr ' . ($item['user_rank'] == ($urate && $item['buyer_id'] == UhCmsApp::getSesInstance()->UserId) ? 'target-color' : '') . '">
                               <td class="rate-th-pos rate-td">' . $item['user_rank'] . '</td>
                               <td class="rate-th-usr rate-td rate-reqnum res-pic-padding"><img class="res-pic-around" src="' . ($item['pic_sm'] != "" ? (file_exists($item['pic_sm']) ? WWWHOST . $item['pic_sm'] : WWWHOST . 'img/no-pic.png') : WWWHOST . 'img/no-pic.png') . '">' . (strlen($item['fname'] . ' ' . $item['name'] . ' / Добрых дел: ' . '<b>' . $item['hlpco']) < 60 ? $item['fname'] . ' ' . $item['name'] . ' / Добрых дел: ' . '<b>' . $item['hlpco'] : $item['fname'] . ' ' . $item['name'] . ' / <br><span style="margin-left: 30px;">Добрых дел: ' . '<b>' . $item['hlpco']) . '</b></span>' . '</td>
								<td class="rate-th-rtg rate-td rate-td-value">' . $item['user_rate'] . ($item['buyer_id'] == UhCmsApp::getSesInstance()->UserId ? '<div onclick="popWndsm(\'rateinfo\', \'type=1\')" class="rate-arrow blink"><img src="' . WWWHOST . 'img/white-arrow.png" class="rate-arrow-img"></div>' : '') . '</td>

						   </tr>';
		}
		if ($urate == 0) {
			echo '<tr class="repos rate-heigh-top rate-heigh-mid rate-tr">
                                       <td class="rate-th-pos rate-td">--</td>
                                       <td class="rate-th-usr rate-td rate-reqnum res-pic-padding">--</td>
                                       <td class="rate-th-rtg rate-td rate-td-value"><img src="' . WWWHOST . 'img/white-arrow.png" class="rate-arrow-img"></div></td>
                                   </tr>';
			echo '<tr id="target" class="repos rate-heigh-top rate-heigh-mid rate-tr target-color">
                                       <td class="rate-th-pos rate-td">' . UhCmsApp::getSesInstance()->UserId . '</td>
                                       <td class="rate-th-usr rate-td rate-reqnum res-pic-padding">Вы находитесь на последнем месте в рейтинге</td>
                                       <td class="rate-th-rtg rate-td rate-td-value"><div onclick="popWndsm(\'rateinfo\', \'type=1\')" class="rate-arrow blink"><img src="' . WWWHOST . 'img/white-arrow.png" class="rate-arrow-img"></div></td>
                                   </tr>';
		}

		echo '</tbody>
            </table>
        </div>
        </div>
    </div>';
	}

	public function html_list_msg($msg_list, $uinfo = '') {
		
		$html = '';

		for($i = 0; $i < count($msg_list); ++$i) {

			$html .= '<div class="col-md-12 col-xs-12 col-lg-12 messages-right-div" style="margin: 5px 0;padding: 12px 0px 0px 10px;">
			<div class="col-md-1 col-xs-1 col-lg-1">
				<a href="'.WWWHOST.'users/viewrev/'.$msg_list[$i]['from_id'].'/"><img src="'.WWWHOST.($msg_list[$i]['pic_sm'] != "" ? (file_exists($msg_list[$i]['pic_sm']) ? $msg_list[$i]['pic_sm'] : 'img/no-pic.png') : 'img/no-pic.png').'" class="messages-img-radius" width="60" height="60"></a>
			</div>
			<div class="col-md-9 col-xs-9 col-lg-9 in-msg" style="padding-left: 22px;">
				<p class="messages-username"><a class="messages-username" href="'.WWWHOST.'users/viewrev/'.$msg_list[$i]['from_id'].'/" style="text-decoration:none">' . ($msg_list[$i]['from_id'] == UhCmsApp::getSesInstance()->UserId? $this->localize->get("cabinet", "you") : $msg_list[$i]['name'].' '.$msg_list[$i]['fname']).'</a></p>
				<p style="padding: 0px 0px 4px 0px; margin-top: -5px;">' . $msg_list[$i]['message'] . '</p>
			</div>
			 <div class="col-md-2 col-xs-2 col-lg-2">
			 <p class="msg-last-dt">' . date("d.m.y H:i", strtotime($msg_list[$i]['add_date'])) . '</p>
				 </div>
				
			 </div>';
		}

			echo json_encode([$html, $uinfo]);

	}


	public function html_mainlist_msg($its, $cat) {

		for( $i=0; $i<count($its); ++$i ) {

			$user_id = ($its[$i]['user_id'] != UhCmsAdminApp::getSesInstance()->UserId ? $its[$i]['user_id'] : 0);

			if(!$user_id) {
				continue;

			} else {

				$user_mess_info = $cat->Buyer_Info($user_id);

				$its[$i]['last_mess'] = preg_replace('/<[^>]*>/', ' ', $its[$i]['last_mess']);

				//print_r($user_mess_info['name'].' '.$user_mess_info['fname']);
				echo '
			<div class="col-md-12 col-xs-12 col-lg-12 messages-right-div messages-choise-chat" onclick="openMsgBord('.$user_id.')">
			<div class="col-md-1 col-xs-1 col-lg-1">
				<a target="_blank" href="'.WWWHOST.'users/viewrev/'.$user_mess_info['id'].'/"><img src="'.WWWHOST.($user_mess_info['pic_sm'] != "" ? (file_exists($user_mess_info['pic_sm']) ? $user_mess_info['pic_sm'] : 'img/no-pic.png') : 'img/no-pic.png').'" class="messages-img-radius" width="60" height="60"></a>
			</div>
				<div class="col-md-9 col-xs-9 col-lg-9 messages-last-mess">
					<p class="messages-username badgefix">' . $user_mess_info['name'].' '.$user_mess_info['fname'] . ' '.( $its[$i]['new_msg'] > 0 ? '<span class="badge-red badge-green">'.$its[$i]['new_msg'].'</span>' : '' ).'</p>
					<p class="message-lastcomm">'.($its[$i]['to_id'] == UhCmsAdminApp::getSesInstance()->UserId ? '' : $this->localize->get("cabinet", "you").': ').' ' .(strlen($its[$i]['last_mess']) > 50 ? substr($its[$i]['last_mess'], 0, 50).'...' : $its[$i]['last_mess']). ' </p>
					
					<br/>
				 </div>
				 <div class="col-md-2 col-xs-2 col-lg-2">
				 <p class="msg-last-dt">' . date("d.m.y H:i", strtotime($its[$i]['latestdt'])) . '</p>
				 </div>
			</div><hr/>';
			}
		}
	}
	
	public function globView_userinfo($its) {
		return "<div style=''>
						<div class='col-lg-12 col-md-12 col-xs-12' style='padding: 17px 10px 4px 10px;background: #05181b;border-radius: 10px;'>
							<div class='col-lg-3 col-md-3 col-xs-3'>
								<a href='".WWWHOST.'users/viewrev/'.$its['id']."/' style='text-decoration: none'><img style='border-radius: 45px;' src='".WWWHOST.$its['u_pic']."' width='40' height='40'></a>
							</div>
							<div class='col-lg-9 col-md-9 col-xs-9'>
								<p style='font-size:11px;margin-top: 2px;margin-left: 5px;font-weight:bold;'><a href='#' style='color:white;text-decoration: none'>".$its['name']."</a></p>
								<img style='width: 11px;margin-left: 5px; margin-top: -17px;' src='".WWWHOST."img/country-g.png'>
								<span style='font-size:10px;color:#2a8e3c;position:relative;top:-9px;margin-left:2px;'>".(mb_strlen($its['city']) > 15 ? $its['city'] : $its['city'].' ('.$its['country'].')' )."</span>
							</div>
							<div class='col-lg-12 col-md-12 col-xs-12'>
								<a href='".WWWHOST.'proj/view/'.$its['item_id']."/' style='text-decoration: none'><img src='".WWWHOST.$its['p_pic']."' width='180' height='125'></a>
								<p style='margin-top: 2px;font-weight: bold;'><a href='".WWWHOST.'proj/view/'.$its['item_id']."/' style='text-decoration: none;color: white;'>".$its['title']."</a></p>
								<a href='".WWWHOST.'proj/view/'.$its['item_id']."/' style='text-decoration: none'><img style='float:right;margin-top: -30px;width: 23px;' src='".WWWHOST."img/cab-pop.png'></a>
								
								<p align='justify' style='color: white;font-size:10px;'>".$its['fulltxt']."</p>
							</div>
						</div>
					</div>";
	}

	public function globView_mainUserInfo($its) {
		return "<div style=''>
					<div class='col-lg-12 col-md-12 col-xs-12' style='padding: 17px 10px 4px 10px;background: #05181b;border-radius: 10px;'>
						<div class='col-lg-3 col-md-3 col-xs-3'>
							<a href='".WWWHOST.'users/viewrev/'.$its['uid']."/' style='text-decoration: none'><img style='border-radius: 45px;' src='".WWWHOST.$its['u_pic']."' width='40' height='40'></a>
						</div>
						<div class='col-lg-9 col-md-9 col-xs-9'>
							<p style='font-size:11px;margin-top: 2px;margin-left: 5px;font-weight:bold;'><a href='".$its['uid']."' style='color:white;text-decoration: none'>".$its['name']."</a></p>
							<img style='width: 11px;margin-left: 5px; margin-top: -17px;' src='".WWWHOST."img/country-g.png'>
							<span style='font-size:10px;color:#2a8e3c;position:relative;top:-9px;margin-left:2px;'>".(mb_strlen($its['city']) > 15 ? $its['city'] : $its['city'].' ('.$its['country'].')' )."</span>
						</div>
						<div>
										<div style='width: 180px;' class='cabinet-line'></div>
			
							<p style='font-size:11px;margin-top: 20px;color:white;'>Оказал помощь в <span style='color:#2a8e3c;'>".$its['numx']."</span> ".($its['numx'] % 10 == 1 && $its['numy'] != 11 ? 'стране' : 'странах')."  из <span style='color:#2a8e3c;'>".$its['numy']."</span></p>
							<p style='font-size:11px;color:white;'>Кол-во помощи деньгами: <span style='color:#2a8e3c;'>".$its['money_help_co']."</span></p>
							<p style='font-size:11px;color:white;'>Кол-во помощи делом: <span style='color:#2a8e3c;'>".$its['work_help_co']."</span></p>
						</div>
					</div>
				</div>";
	}

	public function globView_countryinfo($its) {
		return "<div style=''>
					<div class='col-lg-12 col-md-12 col-xs-12' style='padding: 17px 10px 4px 10px;border-radius: 10px;'>
						
						<div class='col-lg-12 col-md-12 col-xs-12'>
							<img style='float: left;width: 14px; margin-top: 25px;' src='".WWWHOST."img/country-g.png'>
							<p style='color: white;font-size:12px;margin-left: 18px;font-weight: bold;margin-top: 25px;'>Страна <span style='color:#2a8e3c;'>".$its['name']."</span></p>
							<div style='width: 180px;' class='cabinet-line'></div>
							<p>Кол-во помощи: <span style='color:#2a8e3c;'>".$its['co_help']."</span></p>
							<div style='width: 180px;' class='cabinet-line'></div>
							<p>Кол-во вашей помощи: <span style='color:#2a8e3c;'>".$its['co_u_help']."</span></p>
						</div>
						
					</div>
				</div>";
	}
}
