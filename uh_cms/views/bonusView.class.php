<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class BonusView extends PageView
{
	protected $reqdata;
	protected $months;
	
	function __construct($config, $pModel)
	{
		parent::__construct($config, $pModel);
	
		$this->months = Array("января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября","декабря");
	}
	
	public function render_default()
	{
		$this->viewMode = "";
		$this->renderPage("bonus.php");
	}

	public function render_add_bonus($reqd)
	{
		$this->reqdata = $reqd;
		$this->viewMode = "create";
		
		$this->title = $this->localize->get("addbonus", "h1");
		
		$this->renderPage("bonusfrm.php");
	}
	
	public function render_edit_bonus($reqd)
	{
		$this->reqdata = $reqd;
		$this->viewMode = "edit";
		
		$this->title = $this->localize->get("addbonus", "h1-edit");
		
		$this->renderPage("bonusfrm.php");
	}
	
	public function render_done_bonus()
	{
		//$this->reqdata = $reqd;
		$this->viewMode = "created";
	
		$this->title = $this->localize->get("addbonus", "h1-created");
	
		$this->renderPage("bonusfrm.php");
	}
	
	public function render_view_bonus($reqd)
	{
		$this->with_magnific_popup = true;
		$this->reqdata = $reqd;
		$this->viewMode = "view";
		
		if( $this->projinfo['id'] != 0 )
		{
			$this->setSeo(SEO_TITLE, $this->projinfo['title']." - ".$this->cfg['NAME_RU']);
			$this->setSeo(SEO_KEYW, " ");
				
			$pdescr = strip_tags($this->projinfo['descr0']);
			if( strlen($pdescr)>250 )
				$pdescr = substr($pdescr, 0, 240);
			$this->setSeo(SEO_DESCR, str_replace("\"", "&quot;", $pdescr.". Получите бесплатный бонус на сайте ".$this->cfg['NAME_RU']));
				
			$sc_tags = "";
				
			$sc_pic_tag = '';
			$pics = $this->projinfo['photos'];
			if( count($pics)>0 )
			{
				$sc_pic_tag = '<meta property="og:image" content="'.WWWHOST.$pics[0]['filename_thumb'].'" >';
			}
				
			$sc_tags = '<meta property="fb:app_id" content="803813303006196" >
<meta property="og:type" content="article">
<meta property="og:site_name" content="'.$this->cfg['NAME_RU'].'">
<meta property="og:title" content="'.str_replace("\"", "&quot;", $this->projinfo['title'])." - Бесплатные бонусы за добрые дела на сайте ".$this->cfg['NAME_RU'].'">
<meta property="og:url" content="'.$this->page_BuildUrl("bonus","view/".$this->projinfo['id']).'">
'.$sc_pic_tag.'
<meta property="og:description" content="'.str_replace('"', "&quot;", $this->seo[SEO_DESCR]).'" >';
		
			
			$this->setSeo(SEO_SOCIAL_TAGS, $sc_tags);
		}
		
		
		$this->renderPage("bonusview.php");
	}
	
	public function render_addnotallowed()
	{
		$this->viewMode = "notallowed";
		$this->renderPage("bonusfrm.php");
	}
	
	public function Catalog_BuildUrl($caturl)
	{
		$curl = $this->Page_BuildUrl("cat", $caturl);
		//$curl = $url."/".$caturl;
		
		return $curl;
	}
	
	protected function build_DateStr($dy, $dm, $dd, $dh, $dmin, $sep=", ")
	{
	
		$str = sprintf("%d %s %04d%s %02d:%02d", $dd, $this->build_MonthStr($dm), $dy, $sep, $dh, $dmin);
	
		return $str;
	}
	
	protected function build_MonthStr($monind, $sklon = 0)
	{
		return $this->months[$monind-1];
	}
}
?>