<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class LoginView extends PageView
{
	protected $reqdata;
	
	public function render_default()
	{
		$this->hide_login_btn_top = true;
		$this->viewMode = "";
		$this->renderPage("login.php");
	}	
	
	public function render_loginbox()
	{
		$this->hide_login_btn_top = true;
		$this->viewMode = "";
		$this->renderPage("login.php");
	}
	
	public function render_restorenotify()
	{
		$this->viewMode = "restorepassnotify";
		$this->renderPage("login.php");
	}
	
	public function render_restorepass()
	{
		$this->viewMode = "restorepass";
		$this->renderPage("login.php");
	}
	
	public function render_passchanged()
	{
		$this->viewMode = "passchanged";
		$this->renderPage("login.php");
	}
}
?>