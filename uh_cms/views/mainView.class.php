<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class MainView extends PageView
{
	public function render_default()
	{		
		$this->renderPage("main.php");
	}	
	
	/*
	public function page_BuildUrl($contr, $action, $add="")
	{
		return WWWHOST.$contr."/".$action;
	}
	*/
}
?>