<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class PayscallbackView extends PageView {

    function __construct($config, $pModel) {
        parent::__construct($config, $pModel);
    }

    public function render_default() {
        include $this->cfg['MVC_PATH']."templates/payscallback.php";
    }

}