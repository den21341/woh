<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class cronsView extends View
{
	protected $cronModel;
	
	function __construct($config, $pModel)
	{
		parent::__construct($config);
		
		$this->cronModel = $pModel;
	}
	
	public function render_default()
	{
		echo "";
	}

	public function render_html($html)
	{
		echo $html;
	}
	
	public function render_result($txt)
	{
		//header("Content-Type: charset=utf-8");
		header('Content-Type: text/plain; charset=utf-8');
		echo $txt;
	}
	
	public function page_BuildUrl($contr, $action, $add="")
	{
		//return WWWHOST.$contr."/".$action;
		return UhCmsUtils::Page_BuildUrl($contr, $action, $add);
	}
}
?>