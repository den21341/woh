<!DOCTYPE HTML>
<html lang="ru">

<head>
    <title>Осуществить мечту</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=0, maximum-scale=1" />
    <meta name="keywords" content="мечта, осуществить мечту, зделать мечту реальной" />
    <meta name="description" content="Осуществить свою мечту. Мечта на WayOfHelp" />
    <meta name="webmoney" content="0D303893-71E4-4B81-881E-4C3124D4E5EC">
    <link media="all" rel="stylesheet" type="text/css" href="<?=WWWHOST?>uh_cms/landOpt/landDream/css/fonts.css" />
    <link media="all" rel="stylesheet" type="text/css" href="<?=WWWHOST?>uh_cms/landOpt/landDream/css/style.css" />
    <link media="all" rel="stylesheet" type="text/css" href="<?=WWWHOST?>/css/bootstrap.min.css" />
    <link media="all" rel="stylesheet" type="text/css" href="<?=WWWHOST?>/css/all.css" />
    <script type="text/javascript" src="<?=WWWHOST?>uh_cms/landOpt/landDream/js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="<?=WWWHOST?>uh_cms/landOpt/landDream/js/control-page.js"></script>
    <script type="text/javascript" src="<?=WWWHOST?>js/basic.js"></script>
    <link href="<?=WWWHOST?>favicon.ico" rel="icon" type="image/x-icon"/>
    <link href="<?=WWWHOST?>favicon.ico" rel="shortcut icon" type="image/x-icon"/>

</head>
<body>

<div class="total-container">

    <!-- .hat -->
    <div class="hat">

        <div class="hat__container">

            <div class="hat-block">

                <div class="logo"><a href="http://wayofhelp.com/"><img src="<?=WWWHOST?>uh_cms/landOpt/images/logo.png" alt="img" /></a></div>

                <ul class="hat-menu">
                    <li><a class="hat-menu__give" href="http://wayofhelp.com/cat/fulfill-dream/">Осуществить мечту</a></li>
                </ul>

                <ul class="identification">
                    <li><a class="identification__entrance" href="http://wayofhelp.com/registration/person/">Вход</a></li>
                    <li><a class="identification__registration" href="http://wayofhelp.com/registration/person/">Регистрация</a></li>
                </ul>

            </div>

        </div>

    </div>
    <!-- END .hat -->
<div class="container-fluid">
    <div class="row">
        <div class="peopleNeed">
            <div class="peopleNeed__container">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="th-row col-lg-12 col-md-12">
            <!--<div class="goodDeeds__container"></div>-->
            <div class="row">
            <div class="bl bl-1 col-md-3 col-sm-5 col-xs-12 col-lg-3">
                <div class="ic1">
                </div>
                <div class="txt ">
                <p>Делись своими и чужими</p><p>проектами в социальных сетях</p> <p class="yell"><b>+0,1</b> к рейтингу</p>
                </div>
                </div>
            <div class="bl bl-2 col-md-3 col-sm-5 col-xs-12 col-lg-3">
                <div class="ic2">
                </div>
                <div class="txt ">
                <p>Расскажи о своей мечте!</p>
                    <p>Красиво оформи проект</p>
                    <p>(фото+ текст)</p>
                    <p class="yell"><b>+10</b> к рейтингу </p>
                </div>
            </div>
            <div class="bl bl-3 col-xs-12 col-sm-5 col-md-3 col-lg-3">
                <div class="ic3">
                </div>
                <div class="txt txt-def">
                <p class="xsmt">Помогай, чем можешь
                другим проектам:</p>
                <p>- Реши задачу по математике</p>
                <p>- Переведи текст на английский</p>
                <p>- Отдай нуждающимся б\у или</p> не нужную тебе вещь
                <p>- Отправь 5 рублей
                на понравившийся проект</p>
                <p class="yell bz-xs"><b>+1</b> к рейтингу</p>
                    </div>
            </div>
            <div class="bl bl-4 col-md-3 col-sm-5 col-xs-12 col-lg-3">
                <div class="ic4">
                </div>
                <div class="txt txt-def">
                <p class="yell bz-xs"><b>База всех проектов</b></p>
                <p>- Тысячи проектов на получение
                    и для оказания помощи</p>
                <p> - Более 70 различных категорий</p>
                <p> - Помощь делом</p>
                <p> - Помощь деньгами  </p>
                    </div>
            </div>
        </div>
            </div>
    </div>
        <div class="row">
            <div class="fo-row">
                <div class="ipad col-md-6 col-sm-6 col-xs-6 col-lg-6"></div>
                <div class="text col-md-6 col-sm-6 col-xs-6 col-lg-6">
                    <p class="p1">И тогда:</p>
                    <p class="p1-sl">Получай свое дерево добра</p>
                    <div id="btn-tree" style="cursor: pointer" onclick="yaCounter34223620.reachGoal('PushButtonLookFromLandDream'); return true;" class="btn-watch dream-msg-btn">
                        <div class="col-md-3">
                            <img src="<?=WWWHOST?>img/dream-tree.png" class="dream-msg-img" alt="Смотри" title="Смотри">
                        </div>
                        <div class="col-md-9">
                            <p>Смотри</p>
                        </div>
                    </div>
                    <img src="<?=WWWHOST?>img/arrow-dreams.png" href="130" width="80" class="arrow">
                    <p class="p1-sl">Попадай на главную</p>
                    <p class="p1-sl">страницу и грарантированно</p>
                    <p class="p1-sl">получай помощь!</p>
                </div>
            </div>
        </div>
    </div>


    <div class="daydream">
        <div class="daydream__container">
            <div class="butt-play"></div>
        </div>
    </div>


    <div class="blockInfo">

        <div class="blockInfo__container">

            <ul class="blockInfo__buttCont">
                <li><a class="blockInfo__give" href="http://wayofhelp.com/cat/fulfill-dream/">Осуществить мечту</a></li>
            </ul>

            <div class="socNet">
                <span class="socNet__text">Подписывайся на нас в социальных сетях и ежедневно заряжайся позитивом!</span>
                <ul class="socNet__list">
                    <li><a class="socNet-ok" href="http://ok.ru/group/53895621509237" target="_blank"></a></li>
                    <li><a class="socNet-fb" href="https://www.facebook.com/WayofHelp" target="_blank"></a></li>
                    <li><a class="socNet-vk" href="https://vk.com/wayofhelp" target="_blank"></a></li>
                </ul>
            </div>

        </div>

    </div>



    <div class="hFooter"></div>
<style>
    footer{
        background: none!important;
        padding: 40px 0 4px 0!important;
    }
</style>
    <footer>
        <div class="container-fluid footer-container">
            <div class="container foot-xs-cont">
                <!--noindex--><div class="col-md-2 col-lg-2 col-xs-2 foot-block foot-block-f">
                    <p>Начать</p>
                    <ul>
                        <li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","siterules");?>">Правила участия</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","howitwork");?>">Как это работает</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('cat','only_success')?>">Удачные проекты</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','specrules')?>">Правила безопасности</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("registration","person")?>">Регистрация</a></li>
                    </ul>
                </div><!--/noindex-->
                <div class="col-md-2 col-lg-2 col-xs-2 foot-block">
                    <p>Узнать больше</p>
                    <ul>
                        <li><a class="foo-a lnkviewpromo" href="#">Смотреть видео</a></li>
                        <li class="foot-txt-gr"><a class="foo-a" href="<?=$this->Page_BuildUrl('info','tree')?>">Дерево добрых дел</a></li>
                        <li><a class="foo-a" href="<?=$this->Page_BuildUrl("faq","");?>">FAQ</a></li>
                        <li class="foot-txt-gr"><a class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules");?>">Рейтинг и Статусы</a></li>
                        <li><a class="foo-a" href="<?=$this->Page_BuildUrl('info','rules').'#cathelp'?>">Категории помощи</a></li>
                    </ul>
                </div>
                <!--noindex--><div class="col-md-2 col-lg-2 col-xs-2 foot-block">
                    <p>Связь с нами</p>
                    <ul>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","about");?>">О нас</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>">Контакты</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>">Центр поддержки</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>">Задать вопрос</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#ourteam'?>">Команда</a></li>
                    </ul>
                </div><!--/noindex-->
                <!--noindex--><div class="col-md-3 col-lg-3 col-xs-3 foot-block footbl-sm">
                    <p>Другое</p>
                    <ul>
                        <li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("publ","helpwall");?>">Добрая стена</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','rules').'#copl'?>">Жалобы</a></li>
                        <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','sanction')?>">Штрафные санкции</a></li>
                        <li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules").'#ratproj';?>">Как повысить рейтинг проекта</a></li>
                        <li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules").'#ratuser';?>">Как повысить рейтинг пользователя</a></li>
                    </ul>
                </div><!--/noindex-->
                <!--noindex--><div class="col-md-3 col-lg-3 col-xs-3 foot-block footbl-bg">
                    <div class="footer-logo-block">
                        <a rel="nofollow" href="<?=WWWHOST?>"><div class="footer-logo"></div></a>
                        <div class="all-right-res">© wayofhelp.com - Все права защищены</div>
                        <div class="webmon-2 col-lg-6"></div>
                        <div class="webmon-1 col-lg-6"></div>
                        <a rel="nofollow" class="foot-at-a" href="https://passport.webmoney.ru/asp/certview.asp?wmid=235515103904" target="_blank">Проверить аттестат</a>
                    </div>
                </div><!--/noindex-->
            </div>
        </div>
    </footer>

</div>


<div class="wnd-bg" id="wnd-bg"></div>
<div class="wnd-wnd" id="wnd-wnd">
    <div class="wnd-in">
        <div class="wnd-close"><a href="#" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a></div>
        <div class="wnd-cont" id="wnd-cont">
            <div class="wnd-loading"></div>
        </div>
    </div>
</div>

<style type="text/css">
    @import url("<?=WWWHOST?>css/graph.css");
    @import url("<?=WWWHOST?>css/vis.css");
</style>
<script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/my_vis.js"></script>
<script type="text/javascript">
    isMine = true;
</script>
<script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/obj_graph_baloon.js"></script>

<div class="popUp">

    <div class="popUp__wind">

        <div class="popUp__close"></div>

        <iframe  id="video" width="100%" height="100%" src="https://www.youtube.com/embed/z3b0H6H4H6k" frameborder="0" allowfullscreen></iframe>

    </div>

</div>
</div>
<script>
    $('#btn-tree').bind('click', function () {
        req_ajx_host = '<?=WWWHOST?>';
        adapt_top = 'false';

        popWnd('showtreedlg', 'uid=375');
    });

        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter34223620 = new Ya.Metrika({
                        id:34223620,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");

</script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=SS7Mq4nJYr*Btbji8/ILPVasdTyEYZB9QMnH4D4hTUxC*3WB*CQLR*W4pQaegAEFXddm8DlRMD0ozgb4be4C2AYDA0U93lF2BfxgfvFgY5lotTecYvZJ0kDGanxmJ6tU2emMe5e5im/UVdG7QZivI/HkbNnwddG4RlG2pTuwB5o-';</script>
</body>
</html>








