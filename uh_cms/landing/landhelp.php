<!DOCTYPE HTML>
<html lang="ru">
<head>
    <title>Поделиться своими вещами</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=0, maximum-scale=1" />
    <meta name="keywords" content="раздать вещи, отдать вещи, отдать бесплатно вещи" />
    <meta name="description" content="Отдать безвоздмездно вещи нуждающимся. Создайте проект и опишите что Вы хотите раздать. Оказание помощи на WayOfHelp" />
    <meta name="webmoney" content="0D303893-71E4-4B81-881E-4C3124D4E5EC">
    <link media="all" rel="stylesheet" type="text/css" href="<?=WWWHOST;?>uh_cms/landOpt/css/fonts.css" />
    <link media="all" rel="stylesheet" type="text/css" href="<?=WWWHOST;?>uh_cms/landOpt/css/style.css" />
    <link media="all" rel="stylesheet" type="text/css" href="<?=WWWHOST;?>/css/bootstrap.min.css" />
    <link media="all" rel="stylesheet" type="text/css" href="<?=WWWHOST;?>/css/all.css" />
    <script type="text/javascript" src="<?=WWWHOST;?>uh_cms/landOpt/js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="<?=WWWHOST;?>uh_cms/landOpt/js/control-page.js"></script>
    <script type="text/javascript" src="<?=WWWHOST?>js/basic.js"></script>
    <link href="<?=WWWHOST;?>favicon.ico" rel="icon" type="image/x-icon"/>
    <link href="<?=WWWHOST;?>favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    <script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
    <script type="text/javascript" src="<?=WWWHOST?>js/jquery.progress.js"></script>

</head>
<body>

<div class="total-container">



    <!-- .hat -->
    <div class="hat">

        <div class="hat__container">

            <div class="hat-block">

                <div class="logo"><a href="http://wayofhelp.com/"><img src="<?=WWWHOST;?>uh_cms/landOpt/images/logo.png" alt="img" /></a></div>

                <ul class="hat-menu">
                    <li><a class="hat-menu__give" href="http://wayofhelp.com/cat/thing-market/sort_new/">Отдам вещи</a></li>
                    <li><a class="hat-menu__necessary" href="http://wayofhelp.com/cat/goods/sort_new/">Нужны вещи</a></li>
                </ul>

                <ul class="identification">
                    <li><a class="identification__entrance" href="http://wayofhelp.com/registration/person/">Вход</a></li>
                    <li><a class="identification__registration" href="http://wayofhelp.com/registration/person/">Регистрация</a></li>
                </ul>

            </div>

        </div>

    </div>
    <!-- END .hat -->



    <div class="peopleNeed">

        <div class="peopleNeed__container"></div>

    </div>



    <div class="goodDeeds">

        <div class="goodDeeds__container">
            <div id="btn-tree" class="whiteball"></div>
            <img class="arrowlan" src="<?=WWWHOST?>uh_cms/landOpt/images/arrowz.png">
        </div>

    </div>



    <div class="daydream">

        <div class="daydream__container">

            <div class="butt-play"></div>

        </div>

    </div>



    <div class="blockInfo">

        <div class="blockInfo__container">

            <ul class="blockInfo__buttCont">
                <li><a class="blockInfo__give" href="http://wayofhelp.com/cat/thing-market/sort_new/">Отдам вещи</a></li>
                <li><a class="blockInfo__necessary" href="http://wayofhelp.com/cat/goods/sort_new/">Нужны вещи</a></li>
            </ul>

            <div class="socNet">
                <span class="socNet__text">Подписывайся на нас в социальных сетях и ежедневно заряжайся позитивом!</span>
                <ul class="socNet__list">
                    <li><a class="socNet-ok" onclick="Share.odnoklassniki('http://wayofhelp.com/land/give_and_take_things/?utm_source=ok&utm_medium=social&utm_campaign=ShareProject&utm_content=landhelp')"></a></li>
                    <li><a class="socNet-fb" onclick="Share.facebook('http://wayofhelp.com/land/give_and_take_things/?utm_source=facebook&utm_medium=social&utm_campaign=ShareProject&utm_content=landhelp')"></a></li>
                    <li><a class="socNet-vk" onclick="Share.vkontakte('http://wayofhelp.com/land/give_and_take_things/?utm_source=vk&utm_medium=social&utm_campaign=ShareProject&utm_content=landhelp')"></a></li>
                </ul>
            </div>

        </div>

    </div>



    <div class="hFooter"></div>

</div>

<style>
    footer{
        background: none!important;
        padding: 40px 0 4px 0!important;
    }
</style>
<footer>
    <div class="container-fluid footer-container">
        <div class="container foot-xs-cont">
            <!--noindex--><div class="col-md-2 col-lg-2 col-xs-2 foot-block foot-block-f">
                <p>Начать</p>
                <ul>
                    <li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","siterules");?>">Правила участия</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","howitwork");?>">Как это работает</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('cat','only_success')?>">Удачные проекты</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','specrules')?>">Правила безопасности</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("registration","person")?>">Регистрация</a></li>
                </ul>
            </div><!--/noindex-->
            <div class="col-md-2 col-lg-2 col-xs-2 foot-block">
                <p>Узнать больше</p>
                <ul>
                    <li><a class="foo-a lnkviewpromo" href="#">Смотреть видео</a></li>
                    <li class="foot-txt-gr"><a class="foo-a" href="<?=$this->Page_BuildUrl('info','tree')?>">Дерево добрых дел</a></li>
                    <li><a class="foo-a" href="<?=$this->Page_BuildUrl("faq","");?>">FAQ</a></li>
                    <li class="foot-txt-gr"><a class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules");?>">Рейтинг и Статусы</a></li>
                    <li><a class="foo-a" href="<?=$this->Page_BuildUrl('info','rules').'#cathelp'?>">Категории помощи</a></li>
                </ul>
            </div>
            <!--noindex--><div class="col-md-2 col-lg-2 col-xs-2 foot-block">
                <p>Связь с нами</p>
                <ul>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","about");?>">О нас</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>">Контакты</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>">Центр поддержки</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>">Задать вопрос</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#ourteam'?>">Команда</a></li>
                </ul>
            </div><!--/noindex-->
            <!--noindex--><div class="col-md-3 col-lg-3 col-xs-3 foot-block footbl-sm">
                <p>Другое</p>
                <ul>
                    <li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("publ","helpwall");?>">Добрая стена</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','rules').'#copl'?>">Жалобы</a></li>
                    <li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','sanction')?>">Штрафные санкции</a></li>
                    <li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules").'#ratproj';?>">Как повысить рейтинг проекта</a></li>
                    <li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules").'#ratuser';?>">Как повысить рейтинг пользователя</a></li>
                </ul>
            </div><!--/noindex-->
            <!--noindex--><div class="col-md-3 col-lg-3 col-xs-3 foot-block footbl-bg">
                <div class="footer-logo-block">
                    <a rel="nofollow" href="<?=WWWHOST?>"><div class="footer-logo"></div></a>
                    <div class="all-right-res">© wayofhelp.com - Все права защищены</div>
                    <div class="webmon-2 col-lg-6"></div>
                    <div class="webmon-1 col-lg-6"></div>
                    <a rel="nofollow" class="foot-at-a" href="https://passport.webmoney.ru/asp/certview.asp?wmid=235515103904" target="_blank">Проверить аттестат</a>
                </div>
            </div><!--/noindex-->
        </div>
    </div>
</footer>

<div class="wnd-bg" id="wnd-bg"></div>
<div class="wnd-wnd" id="wnd-wnd">
    <div class="wnd-in">
        <div class="wnd-close"><a href="#" class="btn btn-default"><span class="glyphicon glyphicon-remove"></span></a></div>
        <div class="wnd-cont" id="wnd-cont">
            <div class="wnd-loading"></div>
        </div>
    </div>
</div>

<style type="text/css">
    @import url("<?=WWWHOST?>css/graph.css");
    @import url("<?=WWWHOST?>css/vis.css");
</style>
<script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/my_vis.js"></script>
<script type="text/javascript">
    isMine = true;
</script>
<script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/obj_graph_baloon.js"></script>

<div class="popUp">

    <div class="popUp__wind">

        <div class="popUp__close"></div>

        <iframe  id="video" width="100%" height="100%" src="https://www.youtube.com/embed/z3b0H6H4H6k" frameborder="0" allowfullscreen></iframe>

    </div>

</div>

<script>
    $(document).ready(function() {

        Share = {
            vkontakte: function(purl) {
                url  = 'http://vkontakte.ru/share.php?';
                url += 'url='          + encodeURIComponent(purl);
                Share.popup(url);
                yaCounter34223620.reachGoal('PushButton_ShareVK'); return true;
            },
            odnoklassniki: function(purl) {
                url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
                url += '&st._surl='    + encodeURIComponent(purl);
                Share.popup(url);
                yaCounter34223620.reachGoal('PushButton_ShareOK'); return true;
            },
            facebook: function(purl) {
                url  = 'http://www.facebook.com/sharer.php?s=100';
                url += '&p[url]='       + encodeURIComponent(purl);
                Share.popup(url);
                yaCounter34223620.reachGoal('PushButton_ShareFB'); return true;
            },
            popup: function(url, soc) {
                window.open(url,'','toolbar=0,status=0,width=626,height=436');
            }
        };
    });

    $('#btn-tree').bind('click', function () {
        req_ajx_host = '<?=WWWHOST?>';
        adapt_top = 'false';

        popWnd('showtreedlg', 'uid=375');
    });
</script>

</body>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=BIEJMIEBmnnXBvSwfBDm51*Bhb1ZqhTOTgOSTbA9P04TyfOQfLKVert3nBFc*5S8f00SrK5biTellCtoqwatExPs2RnpR1Y8jON1Tj57s3D8r*feFiTtINn2J7qcbdFwiQ1TDNpFccTuA5OeYcmTCYgi2gMwAVL/w9w6HzYffzk-';</script>

</html>








