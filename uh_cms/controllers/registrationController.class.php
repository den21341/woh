<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////


class Registration extends PageController
{
	protected $captchaModel;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);

		$this->captchaModel = $this->loadCaptchaModel();
		$this->pageView->catmodel = new Catalog($this->db, $this->LangId);
		$this->pageView->captcha = $this->captchaModel;
		
	}	
	
	////////////////////////////////////////////////////////////////////////////
	// Protected methods - local utils
	
	protected function loadCaptchaModel()
	{
		$fileModel = $this->cfg['MVC_PATH']."models/captchaModel.class.php";		
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Controller');
		}			
		include_once $fileModel;
			
		$cmodel = new CaptchaModel($this->getCfg(), $this->db, $this->LangId);
		
		return $cmodel;
	}
	
	////////////////////////////////////////////////////////////////////////////
	// Action's handlers
	
	public function action_default()
	{
		$this->pageView->render_default();
	}
	
	////////////////////////////////////////////////////////////////////////////
	// Return URLS for social login
	public function action_fbreg() {
		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		// Init POST vars to assoc array
		$reg = $this->_initRegvars();
		
		// Check if session has facebook token
		$fb_access_token = UhCmsApp::getSesInstance()->GetSessionToken();
		
		if( $fb_access_token == "" ) {
			// No facebook access token, so show basic registration
			
			// Build object to pass data to form view and model
			$formreq = new UhCmsFormData($reg);
			
			$this->pageView->render_regform('person', $formreq);
			
			return;
		}
		
		// Load user data from facebook
		$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);
		
		$FbApi->setSessionToken($fb_access_token);
		
		$uInf = $FbApi->getUserInfo();				
		$uAvat = $FbApi->getUserAvatar();
		
		$reg['rname'] = $uInf['first_name'];
		$reg['rfname'] = $uInf['last_name'];
		if( isset($uInf['email']) && $uInf['email']!="" )
			$reg['rlogin'] = $uInf['email'];

		//var_dump($uAvat);
		//echo $uAvat['data']['url']."<br>";
		//$reg['pic'] = ( isset($uAvat['picture']['data']['url']) ? $uAvat['picture']['data']['url'] : '' );
		if( isset($uAvat['data']['url']) && $uAvat['data']['url'] != "" )
		{
			//echo $uAvat['url']."<br>";
			$reg['pic'] = $uAvat['data']['url'];
		}
		
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
		
		$this->pageView->render_regform('personfb', $formreq);
	}
	
	public function action_vkreg()
	{
		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		// Init POST vars to assoc array
		$reg = $this->_initRegvars();
	
		// Check if session has vkontakte token
		$vk_access_token = UhCmsApp::getSesInstance()->GetSessionToken();
		
		echo $vk_access_token."<br>";
	
		if( $vk_access_token == "" )
		{
			// No VK access token, so show basic registration
				
			// Build object to pass data to form view and model
			$formreq = new UhCmsFormData($reg);
				
			$this->pageView->render_regform('person', $formreq);
				
			return;
		}
		
		$vk_uid = UhCmsApp::getSesInstance()->GetSessionScuserid();
	
		// Load user data from VK
		$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);
	
		$VkApi->setSessionToken($vk_access_token);
	
		$uInf = $VkApi->getUserInfo('',$vk_uid);
		
		//var_dump($uInf);
		//echo "<br>";
	
		if( isset($uInf['id']) )
		{
			//$uAvat = $VkApi->getUserAvatar();
		
			$reg['rname'] = $uInf['first_name'];
			$reg['rfname'] = $uInf['last_name'];
			if( isset($uInf["city"]) )
				$reg['robl'] = $uInf['city']['title'];
		
			if( isset($uInf['photo_200']) && ($uInf['photo_200'] != ""))
				$reg['pic'] = $uInf['photo_200']; //( isset($uAvat['picture']['data']['url']) ? $uAvat['picture']['data']['url'] : '' );
		}
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
	
		$this->pageView->render_regform('personvk', $formreq);
	}
	
	// Процесс регистрации пользователя (создания профиля) после первого входа через одноклассники
	public function action_okreg()
	{
		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		// Init POST vars to assoc array
		$reg = $this->_initRegvars();
	
		// Check if session has Odnoklassniki token
		$ok_access_token = UhCmsApp::getSesInstance()->GetSessionToken();
	
		if( $ok_access_token == "" )
		{
			// No Odnoklassniki access token, so show basic registration
	
			// Build object to pass data to form view and model
			$formreq = new UhCmsFormData($reg);
	
			$this->pageView->render_regform('person', $formreq);
	
			return;
		}
	
		//$ok_uid = UhCmsApp::getSesInstance()->GetSessionScuserid();
	
		// Load user data from Odnoklassniki
		$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);
	
		$OkApi->setSessionToken($ok_access_token);
	
		$uInf = $OkApi->getCurUserInfo($ok_access_token);
	
		//var_dump($uInf);
		//echo "<br>";

		if( isset($uInf['uid']) )
		{
			//$uAvat = $VkApi->getUserAvatar();
				
			$reg['rname'] = $uInf['first_name'];
			$reg['rfname'] = $uInf['last_name'];
				
			$reg['pic'] = $uInf['pic_2']; //( isset($uAvat['picture']['data']['url']) ? $uAvat['picture']['data']['url'] : '' );
		}
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
	
		$this->pageView->render_regform('personok', $formreq);
	}
	
	//////////////////////////////////////////////////////////////////////////
	// STEP 1 - Reg Forms
	
	public function action_person()
	{
		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		// Init POST vars to assoc array
		$reg = $this->_initRegvars();
		$reg['uhash'] = $this->getReqParam('uhash', '');
		
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
				
		$this->pageView->render_regform('person', $formreq);
	}
	
	public function action_company()
	{
		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		// Init POST vars to assoc array
		$reg = $this->_initRegvars();
		
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
		
		$this->pageView->render_regform('company', $formreq);
	}
	
	public function action_organization()
	{
		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		// Init POST vars to assoc array
		$reg = $this->_initRegvars();
		
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
		
		$this->pageView->render_regform('organization', $formreq);
	}
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['rname'] = $this->getReqParam("rname", "");
		$reg['rfname'] = $this->getReqParam("rfname", "");
		$reg['rlogin'] = $this->getReqParam("rlogin", "");
		$reg['rpasswd1'] = $this->getReqParam("rpasswd1", "");
		$reg['rpasswd2'] = $this->getReqParam("rpasswd2", "");
		$reg['rtel'] = $this->getReqParam("rtel", "");
		$reg['rprof'] = $this->getReqParam("rprof", "");
		$reg['rcountry'] = $this->getReqParam("rcountry", 1);
		$reg['robl'] = $this->getReqParam("robl", 0);
		$reg['rcity'] = $this->getReqParam("rcity", 0);
		$reg['rrule'] = $this->getReqParam("rrule", 0);
		$reg['rdescr'] = $this->getReqParam("rdescr", "");
		
		$reg['rusepic'] = $this->getReqParam("rusepic", 0);
		$reg['pic'] = "";
		
		$reg['rorg'] = $this->getReqParam("rorg", "");
		$reg['rorgsphere'] = $this->getReqParam("rorgsphere", "");
		$reg['remplnum'] = $this->getReqParam("remplnum", 5);
		
		$reg['rseccode'] = $this->getReqParam("rseccode", "");
		$reg['controlcode'] = $this->getReqParam("controlcode", "");
		$reg['uhash'] = $this->getReqParam("uhash", '');
		
		return $reg;
	}
	
	private function _initRegvars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['rname'] = "";
		$reg['rfname'] = "";
		$reg['rlogin'] = "";
		$reg['rpasswd1'] = "";
		$reg['rpasswd2'] = "";
		$reg['rtel'] = "";
		$reg['rprof'] = "";
		$reg['rcountry'] = 1;
		$reg['robl'] = 0;
		$reg['rcity'] = 0;
		$reg['rrule'] = 0;
		$reg['rdescr'] = "";
		
		$reg['rusepic'] = 0;
		$reg['pic'] = "";
		
		$reg['rorg'] = "";
		$reg['rorgsphere'] = "";
		$reg['remplnum'] = 5;
		
		$reg['rseccode'] = "";
		$reg['controlcode'] = "";
		
		return $reg;
	}
	
	/////////////////////////////////////////////////////////////////////
	// STEP 3 - for orgs. Attach document
	public function action_doattach()
	{
		$reg = Array();
		$req['rsvid1'] = "";
		$reg['rsvid2'] = "";
		
		
		$uid = 0;
		if( isset($_SESSION['neworgid']) )
		{
			$uid = $_SESSION['neworgid'];
		}
		
		$ind = 1;
		if( isset($_FILES['rsvid1']) && ($_FILES['rsvid1']['tmp_name'] != "") )
		{
			$this->pageModel->saveRegDoc($uid, $_FILES['rsvid1'], $ind);
			$ind++;
			
			$req['rsvid1'] = $_FILES['rsvid1']['name'];
		}
		
		if( isset($_FILES['rsvid2']) && ($_FILES['rsvid2']['tmp_name'] != "") )
		{
			$this->pageModel->saveRegDoc($uid, $_FILES['rsvid2'], $ind);
			
			$req['rsvid2'] = $_FILES['rsvid2']['name'];
		}
		
		$formreq = new UhCmsFormData($reg);
		
		unset($_SESSION['neworgid']);
		$this->pageView->render_regdone("organization");
	}
	
	/////////////////////////////////////////////////////////////////////
	// STEP 2 - Form check and add account
	
	public function action_doorganization()
	{
		// Get POST vars to assoc array
		$reg = $this->_getRegVars();
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
	
		// Make check for all fields
		if( trim($formreq->rorg) == "" )
		{
			$formreq->setok("rorg", false);
			$formreq->setmsg("rorg", "Вы не указали название организации");
		}
		if( trim($formreq->rname) == "" )
		{
			$formreq->setok("rname", false);
			$formreq->setmsg("rname", "Вы не указали свое имя");
		}
		if( trim($formreq->rfname) == "" )
		{
			$formreq->setok("rfname", false);
			$formreq->setmsg("rfname", "Вы не указали свою фамилию");
		}
	
		if( trim($formreq->rlogin) == "" )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Вы не указали свой E-mail/Логин");
		}		
		else if( preg_match(EMAIL_REGEXP, trim($formreq->rlogin)) == 0 )
		//else if( preg_match("/^[a-zA-Z0-9\._]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/", trim($formreq->rlogin)) == 0 )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "E-mail/Логин указан с ошибками");
		}
		else if( $this->pageModel->checkLoginExists(trim($formreq->rlogin)) )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Такой E-mail/Логин уже зарегистрирован. Укажите другой адрес электронной почты.");
		}
		
		if( trim($formreq->rtel) == "" )
		{
			
		}
		else if( preg_match(PHONE_REGEXP, trim($formreq->rtel)) == 0 )		
		{
			$formreq->setok("rtel", false);
			$formreq->setmsg("rtel", "Телефон указан с ошибками");
		}
	
		if( $formreq->rpasswd1 != $formreq->rpasswd2 )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Введенные пароли не совпадают");
		}
		else if( $formreq->rpasswd1 == "" )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Вы не ввели пароль для вашей учетной записи");
		}
		else if( mb_strlen($formreq->rpasswd1) < 6 )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Пароль должен быть минимум 6 символов");
		}
		
		if( $formreq->robl == 0 )
		{
			$formreq->setok("robl", false);
			$formreq->setmsg("robl", "Укажите ваш регион");
		}
		
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Укажите ваш основной город");
		}
		
		if( $formreq->rrule != 1 )
		{
			$formreq->setok("rrule", false);
			$formreq->setmsg("rrule", "Подтвердите что вы принимаете правила пользования сайтом");
		}
		
		// check for control code
		$decodecode = "";
		for( $i=0; $i<4; $i++ )
		{
			//$curletter = $controlcode[$i];
			//$curnumber = decode_table[$curletter];
			//$curdigit = $curnumber % 10;
			$curdigit = $this->captchaModel->decode_letter($formreq->controlcode[$i]);
			$decodecode .= $curdigit;
		}
		
		if( $decodecode != $formreq->rseccode )
		{
			$formreq->setok("rseccode", false);
			$formreq->setmsg("rseccode", "Контрольный код указан неправильно");
		}
	
		if( $formreq->has_errors() )
		{
			$this->pageView->render_regform("organization", $formreq);
		}
		else
		{
			$neworgid = $this->pageModel->addOrganization($formreq);
			
			$_SESSION['neworgid'] = $neworgid; 
			
			//$this->pageView->render_regdone("organization");
			
			$reg = Array();
			$req['rsvid1'] = "";
			$reg['rsvid2'] = "";						
			$formreq = new UhCmsFormData($reg);
			
			$this->pageView->render_attachform($formreq);
		}
	}
	
	public function action_docompany()
	{
		// Get POST vars to assoc array
		$reg = $this->_getRegVars();
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
	
		// Make check for all fields
		if( trim($formreq->rorg) == "" )
		{
			$formreq->setok("rorg", false);
			$formreq->setmsg("rorg", "Вы не указали название компании");
		}
		if( trim($formreq->rname) == "" )
		{
			$formreq->setok("rname", false);
			$formreq->setmsg("rname", "Вы не указали свое имя");
		}
		if( trim($formreq->rfname) == "" )
		{
			$formreq->setok("rfname", false);
			$formreq->setmsg("rfname", "Вы не указали свою фамилию");
		}
	
		if( trim($formreq->rlogin) == "" )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Вы не указали свой E-mail/Логин");
		}
		else if( preg_match(EMAIL_REGEXP, trim($formreq->rlogin)) == 0 )
		//else if( preg_match("/^[a-zA-Z0-9\._]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/", trim($formreq->rlogin)) == 0 )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "E-mail/Логин указан с ошибками");
		}
		else if( $this->pageModel->checkLoginExists(trim($formreq->rlogin)) )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Такой E-mail/Логин уже зарегистрирован. Укажите другой адрес электронной почты.");
		}
		
		if( trim($formreq->rtel) == "" )
		{
				
		}
		else if( preg_match(PHONE_REGEXP, trim($formreq->rtel)) == 0 )
		{
			$formreq->setok("rtel", false);
			$formreq->setmsg("rtel", "Телефон указан с ошибками");
		}
	
		if( $formreq->rpasswd1 != $formreq->rpasswd2 )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Введенные пароли не совпадают");
		}
		else if( $formreq->rpasswd1 == "" )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Вы не ввели пароль для вашей учетной записи");
		}
		else if( mb_strlen($formreq->rpasswd1) < 6 )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Пароль должен быть минимум 6 символов");
		}
		
		if( $formreq->robl == 0 )
		{
			$formreq->setok("robl", false);
			$formreq->setmsg("robl", "Укажите ваш регион");
		}
		
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Укажите ваш основной город");
		}
		
		if( $formreq->rrule != 1 )
		{
			$formreq->setok("rrule", false);
			$formreq->setmsg("rrule", "Подтвердите что вы принимаете правила пользования сайтом");
		}
		
		// check for control code
		$decodecode = "";
		for( $i=0; $i<4; $i++ )
		{
			//$curletter = $controlcode[$i];
			//$curnumber = decode_table[$curletter];
			//$curdigit = $curnumber % 10;
			$curdigit = $this->captchaModel->decode_letter($formreq->controlcode[$i]);
			$decodecode .= $curdigit;
		}
		
		if( $decodecode != $formreq->rseccode )
		{
			$formreq->setok("rseccode", false);
			$formreq->setmsg("rseccode", "Контрольный код указан неправильно");
		}
	
		if( $formreq->has_errors() )
		{
			$this->pageView->render_regform("company", $formreq);
		}
		else
		{
			if( $this->pageModel->addCompany($formreq) )
			{
				$this->pageView->render_regdone("company");
			}
			else 
			{
				echo "Ошибка регистрации";
				$this->pageView->render_regform("company", $formreq);
			}
		}
	}
	
	public function action_doperson()
	{		
		// Get POST vars to assoc array
		$reg = $this->_getRegVars();
				
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
		
		// Make check for all fields
		if( trim($formreq->rname) == "" )
		{
			$formreq->setok("rname", false);
			$formreq->setmsg("rname", "Вы не указали свое имя");
		}
		if( trim($formreq->rfname) == "" )
		{
			$formreq->setok("rfname", false);
			$formreq->setmsg("rfname", "Вы не указали свою фамилию");
		}
		
		if( trim($formreq->rlogin) == "" )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Вы не указали свой E-mail/Логин");
		}
		else if( (preg_match(EMAIL_REGEXP, trim($formreq->rlogin)) == 0) && (preg_match(PHONE_REGEXP, trim($formreq->rlogin)) == 0) )
		//else if( preg_match(PHONE_REGEXP, trim($formreq->rtel)) == 0 )
		//else if( preg_match("/^[a-zA-Z0-9\._]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/", trim($formreq->rlogin)) == 0 )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "E-mail/Логин указан с ошибками");
		}
		else if( $this->pageModel->checkLoginExists(trim($formreq->rlogin)) )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Такой E-mail/Логин уже зарегистрирован. Укажите другой адрес электронной почты.");
		}
		
		if( $formreq->rpasswd1 != $formreq->rpasswd2 )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Введенные пароли не совпадают");
		}
		else if( $formreq->rpasswd1 == "" )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Вы не ввели пароль для вашей учетной записи");
		}
		else if( mb_strlen($formreq->rpasswd1) < 6 )
		{
			$formreq->setok("rpasswd1", false);
			$formreq->setmsg("rpasswd1", "Пароль должен быть минимум 6 символов");
		}
		
		if( $formreq->robl == 0 )
		{
			$formreq->setok("robl", false);
			$formreq->setmsg("robl", "Укажите ваш регион");
		}
		
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Укажите город, в котором вы живете");
		}
		
		if( $formreq->rrule != 1 )
		{
			$formreq->setok("rrule", false);
			$formreq->setmsg("rrule", "Подтвердите что вы принимаете правила пользования сайтом");
		}
		
		// check for control code
		$decodecode = "";
		for( $i=0; $i<4; $i++ )
		{
			//$curletter = $controlcode[$i];
			//$curnumber = decode_table[$curletter];
			//$curdigit = $curnumber % 10;
			$curdigit = $this->captchaModel->decode_letter($formreq->controlcode[$i]);
			$decodecode .= $curdigit;
		}
						
		if( $decodecode != $formreq->rseccode )
		{
			$formreq->setok("rseccode", false);
			$formreq->setmsg("rseccode", "Контрольный код указан неправильно");
		}
		
		// Show once again if errors or make reg
		if( $formreq->has_errors() ) {
			$this->pageView->render_regform("person", $formreq);
		}
		else {
			$this->pageModel->addPerson($formreq);
			$this->pageView->render_regdone("person");
		}
	}
	
	public function action_dopersonfb() {
		// Get POST vars to assoc array
		$reg = $this->_getRegVars();
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
	
		// Make check for all fields
		if( trim($formreq->rname) == "" )
		{
			$formreq->setok("rname", false);
			$formreq->setmsg("rname", "Вы не указали свое имя");
		}
		if( trim($formreq->rfname) == "" )
		{
			$formreq->setok("rfname", false);
			$formreq->setmsg("rfname", "Вы не указали свою фамилию");
		}
	
		if( trim($formreq->rlogin) == "" )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Вы не указали свой E-mail");
		}
		else if( preg_match(EMAIL_REGEXP, trim($formreq->rlogin)) == 0 )
		//else if( preg_match("/^[a-zA-Z0-9\._]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/", trim($formreq->rlogin)) == 0 )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "E-mail указан с ошибками");
		}
		else if( $this->pageModel->checkLoginExists(trim($formreq->rlogin)) )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Такой E-mail/Логин уже зарегистрирован. Укажите другой адрес электронной почты.");
		}		
		
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Укажите город, в котором вы живете");
		}
		
		// Check if session has facebook token
		$fb_access_token = UhCmsApp::getSesInstance()->GetSessionToken();
			
		if( $fb_access_token == "" )
		{
			// No facebook access token, so show basic registration
			$this->pageView->render_regform('person', $formreq);			
			return;
		}
			
		// Load user data from facebook
		$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);
		$FbApi->setSessionToken($fb_access_token);
		$uInf = $FbApi->getUserInfo();
		$uAvat = $FbApi->getUserAvatar();
		
		$formreq->pic = ( isset($uAvat['data']['url']) ? $uAvat['data']['url'] : '' );
		
		if( empty($uInf['id']) || ($uInf['id'] == '') )
		{
			// No facebook access token, so show basic registration
			$this->pageView->render_regform('person', $formreq);			
			return;
		}
	
		if( $formreq->has_errors() )
		{					
			$this->pageView->render_regform("personfb", $formreq);
		}
		else
		{
			$this->pageModel->addPersonFb($uInf['id'], $formreq, $fb_access_token);
			$this->pageView->render_regdone("personfb");
		}
	}
	
	public function action_dopersonvk()
	{
		// Get POST vars to assoc array
		$reg = $this->_getRegVars();
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
	
		// Make check for all fields
		if( trim($formreq->rname) == "" )
		{
			$formreq->setok("rname", false);
			$formreq->setmsg("rname", "Вы не указали свое имя");
		}
		if( trim($formreq->rfname) == "" )
		{
			$formreq->setok("rfname", false);
			$formreq->setmsg("rfname", "Вы не указали свою фамилию");
		}
	
		if( trim($formreq->rlogin) == "" )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Вы не указали свой E-mail");
		}
		else if( preg_match(EMAIL_REGEXP, trim($formreq->rlogin)) == 0 )
		//else if( preg_match("/^[a-zA-Z0-9\._]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/", trim($formreq->rlogin)) == 0 )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "E-mail указан с ошибками");
		}
		else if( $this->pageModel->checkLoginExists(trim($formreq->rlogin)) )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Такой E-mail/Логин уже зарегистрирован. Укажите другой адрес электронной почты.");
		}
		
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Укажите город, в котором вы живете");
		}
	
		// Check if session has facebook token
		$vk_access_token = UhCmsApp::getSesInstance()->GetSessionToken();
			
		if( $vk_access_token == "" )
		{
			// No facebook access token, so show basic registration
			$this->pageView->render_regform('person', $formreq);
			return;
		}
		
		$vk_uid = UhCmsApp::getSesInstance()->GetSessionScuserid();
			
		// Load user data from facebook
		$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);
		$VkApi->setSessionToken($vk_access_token);
		$uInf = $VkApi->getUserInfo('', $vk_uid);
		
		
		if( empty($uInf['id']) || ($uInf['id'] == '') )
		{
			// No facebook access token, so show basic registration
			$this->pageView->render_regform('person', $formreq);
			return;
		}
		
		if( isset($uInf['photo_200']) && ($uInf['photo_200'] != ""))
			$formreq->pic = $uInf['photo_200'];
		
		//$reg['pic'] = $uInf['photo_200'];// isset($uAvat['picture']['data']['url']) ? $uAvat['picture']['data']['url'] : '' );
	
		if( $formreq->has_errors() )
		{
			$this->pageView->render_regform("personvk", $formreq);
		}
		else
		{
			$this->pageModel->addPersonVk($uInf['id'], $formreq, $vk_access_token);
			$this->pageView->render_regdone("personvk");
		}
	}
	
	public function action_dopersonok()
	{
		// Get POST vars to assoc array
		$reg = $this->_getRegVars();
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
	
		// Make check for all fields
		if( trim($formreq->rname) == "" )
		{
			$formreq->setok("rname", false);
			$formreq->setmsg("rname", "Вы не указали свое имя");
		}
		if( trim($formreq->rfname) == "" )
		{
			$formreq->setok("rfname", false);
			$formreq->setmsg("rfname", "Вы не указали свою фамилию");
		}
	
		if( trim($formreq->rlogin) == "" )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Вы не указали свой E-mail");
		}
		else if( preg_match(EMAIL_REGEXP, trim($formreq->rlogin)) == 0 )
		//else if( preg_match("/^[a-zA-Z0-9\._]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/", trim($formreq->rlogin)) == 0 )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "E-mail указан с ошибками");
		}
		else if( $this->pageModel->checkLoginExists(trim($formreq->rlogin)) )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Такой E-mail/Логин уже зарегистрирован. Укажите другой адрес электронной почты.");
		}
		
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Укажите город, в котором вы живете");
		}
	
		// Check if session has Odnoklassniki token
		$ok_access_token = UhCmsApp::getSesInstance()->GetSessionToken();
			
		if( $ok_access_token == "" )
		{
			// No Odnoklassniki access token, so show basic registration
			$this->pageView->render_regform('person', $formreq);
			return;
		}
	
		$ok_uid = UhCmsApp::getSesInstance()->GetSessionScuserid();
			
		// Load user data from Odnoklassniki
		$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);
		$OkApi->setSessionToken($ok_access_token);
		$uInf = $OkApi->getCurUserInfo($ok_access_token);
		if( empty($uInf['uid']) || ($uInf['uid'] == '') )
		{
			// No Odnoklassniki access token, so show basic registration
			$this->pageView->render_regform('person', $formreq);
			return;
		}
	
		$reg['pic'] = $uInf['pic_2'];// isset($uAvat['picture']['data']['url']) ? $uAvat['picture']['data']['url'] : '' );
	
		if( $formreq->has_errors() )
		{
			$this->pageView->render_regform("personok", $formreq);
		}
		else
		{
			$this->pageModel->addPersonOk($uInf['uid'], $formreq, $ok_access_token);
			$this->pageView->render_regdone("personok");
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////
	// Activate user account by link in email
	
	public function action_activate() {
		$redirto = $this->getReqParam('redirto', '');

		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		$guid = $this->getReqParam("guid", "", true, true);

		if( $guid != "" ) {
			$actok = $this->pageModel->activateAccount($guid);
			
			if( $actok ) {
				if( UhCmsApp::getSesInstance()->AuthMakeAutoByActGuid($guid) ) {
					if($redirto)
						$this->goToUrl($redirto);
					else
						$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
				}
			}
			
			$this->pageView->render_activate($actok);
		}
	}

}
?>