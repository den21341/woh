<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Proj extends PageController
{		
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		//$this->self_contolled = true;
	
		//$this->catLib = new Catalog($this->db, $this->LangId);
	}
	
	private function authAllow()
	{
		$uid = UhCmsApp::getSesInstance()->UserId;
		if( $uid == 0 )
		{
			header("Location: ".WWWHOST);
			exit();
		}
		
		return $uid;
	}
	
	public function action_view()
	{
		$proj_id = 0;
		
		if( count($this->urlparts)>0 )
		{
			$proj_url = $this->urlparts[0];
			
			// Check if this project exists
			$projinfo = $this->catLib->Item_Info($proj_url);
			if( $projinfo != null )
				$proj_id = $projinfo['id'];
		}
		
		if( $proj_id == 0 )
			$this->go404();
		
		$this->catLib->Item_UpdateRate($proj_id);
		
		// Find path to section and add it to breadcrumbs
		$spath = $this->catLib->Catalog_SectPath($projinfo['sectid']);					
		
		// Fill data to pageView object
		$this->pageView->spath = $spath;
		$this->pageView->sid = $projinfo['sectid'];
		$this->pageView->projinfo = $projinfo;
		$this->pageView->reqlist = $this->catLib->Item_ReqList($projinfo['id'], REQ_STATUS_CONFIRM, 0, 3);
		$this->pageView->needhelpsects = $this->catLib->Buyer_ItemSects($projinfo['author_id'], PROJ_NEEDHELP);
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		
		//$this->pageView->catmodel = $this->catLib;
		
		// Render page
		$this->pageView->render_default();
	}
	
	public function action_default()
	{
		//$this->catLib = new Catalog($this->db, 1);
		//$uid = $this->authAllow();
		$uid = UhCmsApp::getSesInstance()->UserId;		
		
		$uinfo = $this->catLib->Buyer_Info($uid);
		$uinfo['group_name'] = $this->catLib->Buyer_GroupInfo($uinfo['account_type']);		
		$this->pageView->userinfo = $uinfo;
		//$this->pageView->catmodel = $this->catLib;
		$this->pageView->curl = '';
		$this->pageView->render_default();
	}
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['tit'] = $this->getReqParam("tit", "");
		$reg['descr'] = $this->getReqParam("descr", "");
		$reg['descrfull'] = $this->getReqParam("descrfull", "");
		$reg['stdt'] = $this->getReqParam("stdt", date("d.m.Y", time()));
		$reg['withendt'] = $this->getReqParam("withendt", 1);
		$reg['endt'] = $this->getReqParam("endt", date("d.m.Y", time()+30*24*3600));
		$reg['ptype'] = $this->getReqParam("ptype", 0);
		$reg['sect'] = $this->getReqParam("sect", 0);
		$reg['pamount'] = $this->getReqParam("pamount", 0);
		$reg['robl'] = $this->getReqParam("robl", 1);
		$reg['rcity'] = $this->getReqParam("rcity", 0);
	
		return $reg;
	}
	
	private function _initRegvars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['tit'] = "";
		$reg['descr'] = "";
		$reg['descrfull'] = "";
		$reg['stdt'] = date("d.m.Y", time());
		$reg['withendt'] = 1;
		$reg['endt'] = date("d.m.Y", time()+30*24*3600);
		$reg['ptype'] = 0;
		$reg['sect'] = 0;
		$reg['pamount'] = "0";
		$reg['robl'] = 1;
		$reg['rcity'] = 0;
	
		return $reg;
	}
	
	public function action_add()
	{
		$uid = $this->authAllow();
		
		$uinf = $this->catLib->Buyer_Info($uid);
		
		$reqd = $this->_initRegvars();
		$reqd['robl'] = $uinf['obl_id'];
		$reqd['rcity'] = $uinf['city_id'];
		
		$formreq = new UhCmsFormData($reqd);
		
		//$this->pageView->catmodel = $this->catLib;
		$this->pageView->userinfo = $uinf;
				
		$this->pageView->render_formnew($formreq);		
	}
	
	public function action_publish()
	{
		$uid = $this->authAllow();
		
		// Get POST vars to assoc array
		$reqd = $this->_getRegVars();
		
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reqd);
		
		// Make check for all fields
		if( trim($formreq->tit) == "" )
		{
			$formreq->setok("tit", false);
			$formreq->setmsg("tit", "Вы не указали заголовок вашего проекта");
		}

		if( $formreq->sect == 0 )
		{
			$formreq->setok("sect", false);
			$formreq->setmsg("sect", "Вы не указали в какой раздел размещать проект");
		}
		
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Вы не указали город для проекта");
		}
		
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->stdt)) == 0 )
		{
			$formreq->setok("stdt", false);
			$formreq->setmsg("stdt", "Дата старта проекта указана с ошибками");
		}
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->endt)) == 0 )
		{
			$formreq->setok("endt", false);
			$formreq->setmsg("endt", "Дата старта проекта указана с ошибками");
		}
				
		//$this->pageView->catmodel = $this->catLib;
		
		if( $formreq->has_errors() )
		{
			$this->pageView->render_formnew($formreq);
		}
		else
		{			
			$newpid = $this->pageModel->addProject($uid, $formreq);
			$this->pageView->projid = $newpid;
			$this->pageView->projpics = $this->catLib->Item_Pics($newpid);
			$this->pageView->render_newdone();
		}
	}
	
	public function action_addmedia()
	{
		$projid = $this->getReqParam("projid", 0);		
		$uid = UhCmsApp::getSesInstance()->UserId;
		
		$addpicbtn = $this->getReqParam("addpicbtn", "");
		
		//$this->pageView->catmodel = $this->catLib;
		
		if( $addpicbtn != "" )
		{
			// Load photo and show add photo again		
			if( isset($_FILES['pfile1']) && ($_FILES['pfile1']['name'] != "") && ($_FILES['pfile1']['tmp_name'] != "") )
			{
				$this->pageModel->addProjectPhoto($uid, $projid, $_FILES['pfile1']);
			}
						
			$this->pageView->projid = $projid;
			$this->pageView->projpics = $this->catLib->Item_Pics($projid);
			$this->pageView->render_newdone();
		}
		else
		{		
			//$this->pageView->render_newfinish();
			$this->goToUrl( $this->pageView->Page_BuildUrl("proj", "view/".$projid) );
			return;
		}
	}
	
	public function action_delmedia()
	{
		$uid = UhCmsApp::getSesInstance()->UserId;
		$projid = $this->getReqParam("projid", 0);
		$picid = $this->getReqParam("picid", 0);
		$projinfo = $this->catLib->Item_Info($projid);
	
		if( $projinfo['author_id'] != $uid )
		{
			$this->goToUrl(WWWHOST);
			return;
		}
	
		$this->pageModel->delProjectPhoto($picid, $projid);
		
		//$this->pageView->catmodel = $this->catLib;				
		$this->pageView->projid = $projid;
		$this->pageView->projpics = $this->catLib->Item_Pics($projid);
		$this->pageView->render_newdone();
	}
	
	public function action_save()
	{
		$uid = $this->authAllow();
	
		// Get POST vars to assoc array
		$reqd = $this->_getRegVars();
		$projid = $this->getReqParam("projid", 0);
		
		$btnaddphoto = $this->getReqParam("btnaddphoto", "");
		if( $btnaddphoto != "" )
		{					
			$projinfo = $this->catLib->Item_Info($projid);
			
			if( $uid != $projinfo['author_id'] )
			{
				$this->goToUrl(WWWHOST);
				return;
			}
			
			if( isset($_FILES['file1']) && ($_FILES['file1']['tmp_name'] != "") )
			{
				$this->pageModel->addProjectPhoto($uid, $projid, $_FILES['file1'], 0);
			}
			
			$formreq = $this->_prepare_projedit($uid, $projinfo);
			$this->pageView->render_formedit($formreq);
			
			return;
		}
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reqd);
	
		// Make check for all fields
		if( trim($formreq->tit) == "" )
		{
			$formreq->setok("tit", false);
			$formreq->setmsg("tit", "Вы не указали заголовок вашего проекта");
		}
	
		if( $formreq->sect == 0 )
		{
			$formreq->setok("sect", false);
			$formreq->setmsg("sect", "Вы не указали в какой раздел размещать проект");
		}
	
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Вы не указали город для проекта");
		}
	
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->stdt)) == 0 )
		{
			$formreq->setok("stdt", false);
			$formreq->setmsg("stdt", "Дата старта проекта указана с ошибками");
		}
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->endt)) == 0 )
		{
			$formreq->setok("endt", false);
			$formreq->setmsg("endt", "Дата старта проекта указана с ошибками");
		}
	
		//$this->pageView->catmodel = $this->catLib;
	
		if( $formreq->has_errors() )
		{
			$this->pageView->render_formedit($formreq);
		}
		else
		{
			$this->pageModel->saveProject($uid, $projid, $formreq);
			$this->pageView->projid = $projid;
			$this->pageView->render_savedone();
		}
	}
	
	protected function _prepare_projedit($uid, $projinfo)
	{
		$uinf = $this->catLib->Buyer_Info($uid);
		
		$reqd = $this->_initRegvars();
		$reqd['id'] = $projinfo['id'];
		$reqd['tit'] = $projinfo['title'];
		$reqd['robl'] = $projinfo['obl_id'];
		$reqd['rcity'] = $projinfo['city_id'];
		$reqd['descr'] = $projinfo['descr0'];
		$reqd['descrfull'] = $projinfo['descr'];
		$reqd['ptype'] = $projinfo['amount_type'];
		$reqd['pamount'] = round($projinfo['amount']);
		$reqd['sect'] = $projinfo['sectid'];
		$reqd['stdt'] = $projinfo['stdt'];
		$reqd['withendt'] = 1;
		$reqd['endt'] = $projinfo['endt'];
		
		$formreq = new UhCmsFormData($reqd);
		
		//$this->pageView->catmodel = $this->catLib;
		$this->pageView->userinfo = $uinf;
		
		return $formreq; 
	}
	
	public function action_edit()
	{
		// Allow access only for authorized users
		$uid = $this->authAllow(); 
		
		$proj_id = 0;
		
		if( count($this->urlparts)>0 )
		{
			$proj_url = $this->urlparts[0];
			
			// Check if this project exists
			$projinfo = $this->catLib->Item_Info($proj_url);
			if( $projinfo != null )
				$proj_id = $projinfo['id'];
		}
		
		if( $proj_id == 0 )
			$this->go404();
		
		// User requests not his project
		if( $projinfo['author_id'] != $uid )
		{
			$this->goToUrl(WWWHOST);
			return;
		}		
		
		$formreq = $this->_prepare_projedit($uid, $projinfo);
		
		$this->pageView->render_formedit($formreq);
	}
	
	public function action_picdel()
	{
		$uid = $this->authAllow();
		$projid = $this->getReqParam("projid", 0);
		$picid = $this->getReqParam("picid", 0);
		$projinfo = $this->catLib->Item_Info($projid);
		
		if( $projinfo['author_id'] != $uid )
		{
			$this->goToUrl(WWWHOST);
			return;
		}
		
		$this->pageModel->delProjectPhoto($picid, $projid);
		
		$formreq = $this->_prepare_projedit($uid, $projinfo);
		
		$this->pageView->render_formedit($formreq);
	}
	
	public function action_addhelpmoney()
	{
		$projid = $this->getReqParam("selprojid", 0);
		$helpsum = $this->getReqParam("helpsum", 0);
		$helptxt = $this->getReqParam("helptxt", '');
		$uid = UhCmsApp::getSesInstance()->UserId;
		
		if( ($projid == 0) || ($uid == 0) )
		{
			//echo $projid."<br>";
			$this->goToUrl(WWWHOST);
			return;
		}
		
		$projinfo = $this->catLib->Item_Info($projid);
		
		if( ($projinfo['amount_type'] == PROJ_TYPE_HUMAN) && ($helpsum == 0) )
			$helpsum = 1;
		if( $projinfo['amount_type'] == PROJ_TYPE_SOME )
			$helpsum = 1;
		
		$isReqSent = ( ($reqMy = $this->catLib->Buyer_ReqIsSend($uid, $projinfo['id'])) !== false );
		
		if( $isReqSent )
			$this->goToUrl($this->pageView->Page_BuildUrl("proj", "view/".$projinfo['id']));
		
		if( $helpsum > 0 )
		{					
			$this->pageModel->addHelpSum($projid, $uid, $helpsum, $helptxt);
			$this->pageView->projinfo = $projinfo;
			$this->pageView->render_helpdone();
		}
	}
	
	public function action_reviews()
	{
		$this->pageView->revlist = $this->catLib->Req_List(8, "", 0, false, -1, 0, 21);
		$this->pageView->render_reviews();
	}
}
?>