<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Login extends PageController
{		
	public function action_default() 
	{
		UhCmsApp::getSesInstance()->AuthClear();
		
		$this->pageView->render_default();
	}
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['rlogin'] = $this->getReqParam("rlogin", "");
		
		return $reg;
	}
	
	private function _initRegvars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['rlogin'] = "";
		
		return $reg;
	}
	
	public function action_logout()
	{
		UhCmsApp::getSesInstance()->AuthClear();
		
		$this->pageView->render_default();
	}		
	
	public function action_setpass()
	{
		$guid = $this->getReqParam("guid", "");
		
		if( trim($guid) == "" )
		{
			$this->goToUrl(WWWHOST);
			return;
		}
		
		$uInf = $this->pageModel->findUserByMakeGuid($guid);
		if( isset($uInf['id']) )
		{
			$this->pageView->guid = $guid;
			$this->pageView->render_restorepass();
		}
		else
		{
			$this->goToUrl(WWWHOST);
			return;
		}
	}
	
	public function action_savepass()
	{
		$guid = $this->getReqParam("guid", "");
		$rpass1 = $this->getReqParam("rpass1", "");
		$rpass2 = $this->getReqParam("rpass2", "");
		
		if( trim($guid) == "" )
		{
			$this->goToUrl(WWWHOST);
			return;
		}
		
		$uInf = $this->pageModel->findUserByMakeGuid($guid);
		if( isset($uInf['id']) )
		{
			$this->pageView->guid = $guid;
			
			if( ($rpass1 != $rpass2) || (trim($rpass1) == "") )
			{
				$this->pageView->msg = "Пароли отличаются или пустые. Введите корректные пароли.";
				$this->pageView->render_restorepass();
			}			
			else
			{
				$this->pageModel->setNewPass($guid, $rpass1);
				$this->pageView->render_passchanged();
			}
		}
		else
		{
			$this->goToUrl(WWWHOST);		
			return;
		}
	}
	
	public function action_restorepass()
	{		
		$ulogin = $this->getReqParam("ulogin", "");
		
		// Send email with code for changing password
		$uInf = $this->pageModel->findUserByLogin($ulogin);
		
		if( isset($uInf['id']) )
		{
			$guid = $this->pageModel->setRestorePassGuid($uInf['id']);
			
			$this->pageModel->sendRestorePassEmail($ulogin, $guid);
		}
		
		$this->pageView->render_restorenotify();
		
		/*
		// Get POST vars to assoc array
		$reg = $this->_getRegVars();
				
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reg);
				
		// Make check for all fields
		if( trim($formreq->rlogin) == "" )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "Вы не указали свой E-mail/Логин");
		}
		else if( preg_match("/^[a-zA-Z0-9\._]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/", trim($formreq->rlogin)) == 0 )
		{
			$formreq->setok("rlogin", false);
			$formreq->setmsg("rlogin", "E-mail/Логин указан с ошибками");
		}
		
		if( $formreq->has_errors() )
		{
			$this->pageView->render_restorepass($formreq);
		}
		else
		{
			$this->pageModel->addPerson($formreq);
			$this->pageView->render_regdone("person");
		}
		*/
	}
	
	public function action_tokenfb()
	{
		echo "token<br>";
						
		
		$this->pageView->render_default();
	}
	
	// Called when redirected from facebook oauth form
	public function action_redirfb() {
		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);

		// if 'code' GET param was recieved, then FB auth ok
		if (isset($_GET['code'])) {
			// Get Facebook session token
			$FB_REDIRECT_URL = WWWHOST.'login/redirfb';
			$tokenInfo = $FbApi->getSessionToken($FB_REDIRECT_URL, $_GET['code']);
			
			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) 			
			{							
				$uInf = $FbApi->getUserInfo();
				//print_r($uInf);
				//echo "<br>";
				//print_r($FbApi->getLoginUrl());
				
				if( isset($uInf['id']) )
				{
					// User was in database, make auth only
					$buyer = $this->pageModel->findUserByFbId($uInf['id']);
					if( $buyer !== false )
					{
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'],$buyer['login'],$buyer['passwd'],$tokenInfo['access_token']);

						$query = "SELECT * FROM ".TABLE_BUYER_SOCIAL." WHERE buyer_id = ".$buyer['id'];
						$ch = $this->db->query($query);

						if(empty($ch)) {
							$query = "INSERT INTO ".TABLE_BUYER_SOCIAL." (buyer_id, fb_id) VALUES(".$buyer['id'].",".addslashes($uInf['id']).") ";
						} else {
							$query = "UPDATE ".TABLE_BUYER_SOCIAL." SET fb_id = ".addslashes($uInf['id'])." WHERE buyer_id = ".$buyer['id'];
						}
						$this->db->exec($query);

						header("Location: ".$this->pageView->Page_BuildUrl("cabinet", ""));
						exit();
					}
					else
					{
						echo "added token to session<br>";
						// No user with such facebook id in database, add token to session record and go to registration page					
						// Add session record
						UhCmsApp::getSesInstance()->AddSocialSession($tokenInfo['access_token']);
						
						// Redirect to registration
						header("Location: ".$this->pageView->Page_BuildUrl("registration", "fbreg"));
						exit();
					}
				}				
				
				
				//$uAvat = $FbApi->getUserAvatar();				
				//echo "<br>";
				//var_dump($uAvat);
				
				//$uPics = $FbApi->getUserPhotos();				
				//echo "<br>";
				//var_dump($uPics);
				
				//$uFriends = $FbApi->getUserFriends();
				
				//echo "<br>";
				//var_dump($uPics);						
			}			
			
			
		}
		
		$this->pageView->render_default();				
	}

	public function action_redirlinkFB() {
		if( UhCmsApp::getSesInstance()->UserId == 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));

		$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);

		// if 'code' GET param was recieved, then FB auth ok
		if (isset($_GET['code'])) {
			// Get Facebook session token
			$FB_REDIRECT_URL = WWWHOST.'login/redirlinkFB';
			$tokenInfo = $FbApi->getSessionToken($FB_REDIRECT_URL, $_GET['code']);

			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
				$uInf = $FbApi->getUserInfo();
				//echo "<br>";
				//print_r($FbApi->getLoginUrl());

				if( isset($uInf['id']) ) {
					// User was in database, make auth only
					$buyer = $this->pageModel->findUserByFbId($uInf['id']);
					if( $buyer !== false ) {
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'],$buyer['login'],$buyer['passwd'],$tokenInfo['access_token']);

						header("Location: ".$this->pageView->Page_BuildUrl("cabinet", ""));
						exit();
					}
					else {
						$query = "UPDATE ".TABLE_SHOP_BUYERS." SET fb_uid = ".$uInf['id']." WHERE id = ".UhCmsApp::getSesInstance()->UserId;
						$this->db->exec($query);

						$query = "SELECT * FROM ".TABLE_BUYER_SOCIAL." WHERE buyer_id = ".UhCmsApp::getSesInstance()->UserId;
						$ch = $this->db->query($query);
						if(empty($ch)) {
							$query = "INSERT INTO ".TABLE_BUYER_SOCIAL." (buyer_id, fb_id) VALUES(".UhCmsApp::getSesInstance()->UserId.",".addslashes($uInf['id']).") ";
						} else {
							$query = "UPDATE ".TABLE_BUYER_SOCIAL." SET fb_id = ".$uInf['id']." WHERE buyer_id = ".UhCmsApp::getSesInstance()->UserId;
						}
						$this->db->exec($query);
					}
				}
			}


		}

		$this->pageView->render_default();
	}
	
	// Called when redirected from facebook oauth form
	public function action_redirvk()
	{
		if( UhCmsApp::getSesInstance()->UserId != 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);
	
		// if 'code' GET param was recieved, then FB auth ok
		if (isset($_GET['code']))
		{
			// Get Facebook session token
			$VK_REDIRECT_URL = WWWHOST.'login/redirvk';
			$tokenInfo = $VkApi->getSessionToken($VK_REDIRECT_URL, $_GET['code']);
				
			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token']))
			{
				$uInf = $VkApi->getUserInfo("", $tokenInfo['user_id']);

				//var_dump($uInf);
				//echo "<br>";
					
				if( isset($uInf['id']) )
				{
					//echo "ok";
					// User was in database, make auth only
					$buyer = $this->pageModel->findUserByVkId($uInf['id']);
					if( $buyer !== false )
					{
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'],$buyer['login'],$buyer['passwd'],$tokenInfo['access_token'],$uInf['id']);

						$query = "SELECT * FROM ".TABLE_BUYER_SOCIAL." WHERE buyer_id = ".$buyer['id'];
						$ch = $this->db->query($query);

						if(empty($ch)) {
							$query = "INSERT INTO ".TABLE_BUYER_SOCIAL." (buyer_id, vk_id) VALUES(".$buyer['id'].",".addslashes($uInf['id']).") ";
						} else {
							$query = "UPDATE ".TABLE_BUYER_SOCIAL." SET vk_id = ".addslashes($uInf['id'])." WHERE buyer_id = ".$buyer['id'];
						}
						$this->db->exec($query);

						header("Location: ".$this->pageView->Page_BuildUrl("cabinet", ""));
						exit();
					}
					else
					{
						//echo "added token to session<br>";
						// No user with such facebook id in database, add token to session record and go to registration page
						// Add session record
						UhCmsApp::getSesInstance()->AddSocialSession($tokenInfo['access_token'],$uInf['id']);
	
						// Redirect to registration
						header("Location: ".$this->pageView->Page_BuildUrl("registration", "vkreg"));
						exit();
					}
					
				}		
			}			
		}
	
		$this->pageView->render_default();
	}

	public function action_redirlinkVK() {
		if( UhCmsApp::getSesInstance()->UserId == 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));

		$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);

		// if 'code' GET param was recieved, then FB auth ok
		if (isset($_GET['code'])) {
			// Get Facebook session token
			$VK_REDIRECT_URL = WWWHOST.'login/redirlinkVK';
			$tokenInfo = $VkApi->getSessionToken($VK_REDIRECT_URL, $_GET['code']);

			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
				$uInf = $VkApi->getUserInfo("", $tokenInfo['user_id']);

				//var_dump($uInf);
				//echo "<br>";

				if( isset($uInf['id']) ) {
					//echo "ok";
					// User was in database, make auth only
					$buyer = $this->pageModel->findUserByVkId($uInf['id']);
					if( $buyer !== false ) {
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'],$buyer['login'],$buyer['passwd'],$tokenInfo['access_token'],$uInf['id']);

						header("Location: ". $this->pageView->Page_BuildUrl("cabinet", ""));
						exit();
					}
					else {
						$query = "UPDATE ".TABLE_SHOP_BUYERS." SET vk_uid = ".$uInf['id']." WHERE id = ".UhCmsApp::getSesInstance()->UserId;
						$this->db->exec($query);

						$query = "SELECT * FROM ".TABLE_BUYER_SOCIAL." WHERE buyer_id = ".UhCmsApp::getSesInstance()->UserId;
						$ch = $this->db->query($query);
						if(empty($ch)) {
							$query = "INSERT INTO ".TABLE_BUYER_SOCIAL." (buyer_id, vk_id) VALUES(".UhCmsApp::getSesInstance()->UserId.",".addslashes($uInf['id']).") ";
						} else {
							$query = "UPDATE ".TABLE_BUYER_SOCIAL." SET vk_id = ".$uInf['id']." WHERE buyer_id = ".UhCmsApp::getSesInstance()->UserId;
						}
						$this->db->exec($query);
					}

				}
			}
		}

		$this->pageView->render_default();
	}
	
	// Called when redirected from Odnoklassniki oauth form
	public function action_redirok() {
		$redir = $this->getReqParam('to', '');

		if( UhCmsApp::getSesInstance()->UserId != 0 ) {
			$type = 1;
		} else {
			$type = 2;
		}
			//$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);
	
		// if 'code' GET param was recieved, then FB auth ok
		if (isset($_GET['code'])) {

			$OK_REDIRECT_URL = WWWHOST.'login/redirok'.($redir != '' ? '?to='.urlencode($redir) : '');

			$tokenInfo = $OkApi->getSessionToken($OK_REDIRECT_URL, $_GET['code']);

			//echo "<Br>";//print_r($tokenInfo); die();
	
			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
				$uInf = $OkApi->getCurUserInfo($tokenInfo['access_token']);

				if( isset($uInf['uid']) ) {
					// User was in database, make auth only
					
					$buyer = $this->pageModel->findUserByOkId($uInf['uid']);

					$redir = str_replace('_', '/', $redir);

					if($buyer === false && $redir != '' && $type == 2) {
						//add utm_source
						$redir .= '?utm_campaign=regfromprojectOK';
						$redir .= '&px=1';

						if($this->pageModel->smplAddPersonOK($uInf, $tokenInfo['access_token']))
							header('Location: ' . ($redir != '' ? $redir : WWWHOST.'cabinet/' ));
						else
							header('Location: ' . WWWHOST.'registration/person/');

					} else if( $buyer !== false && $type == 2) {
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'],$buyer['login'],$buyer['passwd'],$tokenInfo['access_token'],$uInf['uid']);

						$query = "SELECT * FROM ".TABLE_BUYER_SOCIAL." WHERE buyer_id = ".$buyer['id'];
						$ch = $this->db->query($query);

						if(empty($ch)) {
							$query = "INSERT INTO ".TABLE_BUYER_SOCIAL." (buyer_id, ok_id) VALUES(".$buyer['id'].",".addslashes($uInf['id']).") ";
						} else {
							$query = "UPDATE ".TABLE_BUYER_SOCIAL." SET ok_id = ".addslashes($uInf['id'])." WHERE buyer_id = ".$buyer['id'];
						}
						$this->db->exec($query);

						header("Location: ".($redir != '' ? $redir : $this->pageView->Page_BuildUrl("cabinet", "")));
						exit();

					} else if($buyer !== false && $type == 1) {
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'],$buyer['login'],$buyer['passwd'],$tokenInfo['access_token'],$uInf['uid']);

						header("Location: ".($redir != '' ? $redir : $this->pageView->Page_BuildUrl("cabinet", "")));
						exit();
					}
					else if($buyer === false && $type == 2){
						//echo "added token to session<br>";
						// No user with such facebook id in database, add token to session record and go to registration page
						// Add session record
						UhCmsApp::getSesInstance()->AddSocialSession($tokenInfo['access_token'], $uInf['uid']);

						// Redirect to registration
						header("Location: ".$this->pageView->Page_BuildUrl("registration", "okreg"));
						exit();
					} else if($buyer === false && $type == 1) {
						$query = "UPDATE ".TABLE_SHOP_BUYERS." SET od_uid = ".$uInf['uid']." WHERE id = ".UhCmsApp::getSesInstance()->UserId;
						$this->db->exec($query);

						$query = "SELECT * FROM ".TABLE_BUYER_SOCIAL." WHERE buyer_id = ".UhCmsApp::getSesInstance()->UserId;
						$ch = $this->db->query($query);
						//echo $query.'<br>';
						if(empty($ch)) {
							$query = "INSERT INTO ".TABLE_BUYER_SOCIAL." (buyer_id, ok_id) VALUES(".UhCmsApp::getSesInstance()->UserId.",".addslashes($uInf['uid']).") ";
						} else {
							$query = "UPDATE ".TABLE_BUYER_SOCIAL." SET ok_id = ".$uInf['uid']." WHERE buyer_id = ".UhCmsApp::getSesInstance()->UserId;
						}
						//echo $query; die();
						$this->db->exec($query);
					}
				}	
			}		
		}
		$this->pageView->render_default();
	}

	public function action_redirlinkOK() {
		if( UhCmsApp::getSesInstance()->UserId == 0 )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));

		$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);

		// if 'code' GET param was recieved, then FB auth ok
		if (isset($_GET['code'])) {
			// Get Facebook session token
			$OK_REDIRECT_URL = WWWHOST.'login/redirlinkOK';
			$tokenInfo = $OkApi->getSessionToken($OK_REDIRECT_URL, $_GET['code']);

			//var_dump($tokenInfo);
			//echo "<Br>";

			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
				$uInf = $OkApi->getCurUserInfo($tokenInfo['access_token']);
				//echo "<br>";
				//var_dump($uInf);
				//echo "<br>";
				if( isset($uInf['uid']) ) {
					// User was in database, make auth only
					$buyer = $this->pageModel->findUserByOkId($uInf['uid']);
					if( $buyer !== false ) {
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'],$buyer['login'],$buyer['passwd'],$tokenInfo['access_token'],$uInf['uid']);

						header("Location: ".$this->pageView->Page_BuildUrl("cabinet", ""));
						exit();
					}
					else {
						$query = "UPDATE ".TABLE_SHOP_BUYERS." SET od_uid = ".$uInf['id']." WHERE id = ".UhCmsApp::getSesInstance()->UserId;
						$this->db->exec($query);

						$query = "SELECT * FROM ".TABLE_BUYER_SOCIAL." WHERE buyer_id = ".UhCmsApp::getSesInstance()->UserId;
						$ch = $this->db->query($query);
						if(empty($ch)) {
							$query = "INSERT INTO ".TABLE_BUYER_SOCIAL." (buyer_id, ok_id) VALUES(".UhCmsApp::getSesInstance()->UserId.",".addslashes($uInf['id']).") ";
						} else {
							$query = "UPDATE ".TABLE_BUYER_SOCIAL." SET ok_id = ".$uInf['id']." WHERE buyer_id = ".UhCmsApp::getSesInstance()->UserId;
						}
						$this->db->exec($query);
					}
				}
			}
		}

		$this->pageView->render_default();
	}


	// REGISTRATION WITH FB VIA AJAX

	public function action_ajxregFB() {
		$redir = $this->getReqParam('to', '');
		$code = $this->getReqParam('code', '');

		if (UhCmsApp::getSesInstance()->UserId != 0)
			header('location: ' . $this->pageView->Page_BuildUrl("cabinet", ""));


		if ($code) {
			// Get Facebook session token
			$FB_REDIRECT_URL = WWWHOST . 'login/ajxregFB?to='.urlencode($redir);

			$tokenInfo = null;
			$url = 'https://graph.facebook.com/oauth/access_token';
			$params = array(
				'client_id' => FB_APP_ID,
				'redirect_uri' => $FB_REDIRECT_URL,
				'client_secret' => FB_APP_SECRET,
				'code' => $code
			);

			parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);

			// UhCmsFacebook methods not working with ERROR: failed to open strem

			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
				$params = array('access_token' => $tokenInfo['access_token'], 'fields' => 'id,birthday,email,first_name,last_name,name,gender,website,cover,link');
				$uInf = json_decode(file_get_contents('https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params))), true);

				if (isset($uInf['id'])) {

					$email = $this->checkEmail($uInf['email']);

					if($email != false) {
						$query = "UPDATE ".TABLE_SHOP_BUYERS." SET vk_uid = '".$uInf['uid']."' WHERE login = '".$email['login']."'";
						UhCmsApp::getSesInstance()->AuthMakeSocial($email['id'], $email['login'], $email['passwd'], $tokenInfo['access_token'], $uInf['uid']);
						$this->db->exec($query);
						header("Location: " . ($redir != '' ? $redir : WWWHOST . 'cabinet/'));
						exit();
					}

					$buyer = $this->pageModel->findUserByFbId($uInf['id']);

					if ($buyer !== false) {
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'], $buyer['login'], $buyer['passwd'], $tokenInfo['access_token'], $uInf['id']);
						header("Location: " . ($redir ? $redir : WWWHOST . 'cabinet/'));
						exit();
					} else {
						$params = array(
							'type' => 'large',
							'redirect' => 'false',
							'access_token' => $tokenInfo['access_token']
						);

						$FB_ME_URL = 'https://graph.facebook.com/me/picture';

						$url_req = $FB_ME_URL . '?' . urldecode(http_build_query($params));

						$picInfo = json_decode(file_get_contents($url_req), true);

						$uInf['pic'] = (isset($picInfo['data']['url']) ? $picInfo['data']['url'] : '');

						UhCmsApp::getSesInstance()->AddSocialSession($tokenInfo['access_token']);

						//add utm_source
						$redir .= '?utm_campaign=regfromprojectFB';
						$redir .= '&px=1';

						if ($this->pageModel->smplAddPersonFB($uInf))
							header('Location: ' . $redir);
						else
							header('Location: ' . WWWHOST.'registration/person/');
					}
				}

			}
		}
	}

	public function action_ajxregVK() {
		$redir = $this->getReqParam('to', '');
		$code = $this->getReqParam('code', '');

		if (UhCmsApp::getSesInstance()->UserId != 0)
			header('location: ' . $this->pageView->Page_BuildUrl("cabinet", ""));

		if ($code) {
			$VK_REDIRECT_URL = WWWHOST . 'login/ajxregVK?to='.urlencode($redir);

			$params = array(
				'client_id' => VK_APP_ID,
				'client_secret' => VK_APP_SECRET,
				'code' => $code,
				'redirect_uri' => $VK_REDIRECT_URL
			);

			$tokenInfo = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);

			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
				$params = array(
					'uids'         => $tokenInfo['user_id'],
					'fields'       => 'uid,first_name,last_name,screen_name,sex,bdate,photo_200,country',
					'access_token' => $tokenInfo['access_token']
				);

				$uInf = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);

				if (isset($uInf['response'][0]['uid'])) {

					$uInf = $uInf['response'][0];
					$uInf['email'] = (isset($tokenInfo['email']) ? $tokenInfo['email'] : '');

					$email = $this->checkEmail($uInf['email']);

					if($email != false) {
						$query = "UPDATE ".TABLE_SHOP_BUYERS." SET vk_uid = '".$uInf['uid']."' WHERE login = '".$email['login']."'";
						UhCmsApp::getSesInstance()->AuthMakeSocial($email['id'], $email['login'], $email['passwd'], $tokenInfo['access_token'], $uInf['uid']);
						$this->db->exec($query);
						header("Location: " . ($redir != '' ? $redir : WWWHOST . 'cabinet/'));
						exit();
					}

					$buyer = $this->pageModel->findUserByVkId($uInf['uid']);

					if ($buyer !== false) {
						UhCmsApp::getSesInstance()->AuthMakeSocial($buyer['id'], $buyer['login'], $buyer['passwd'], $tokenInfo['access_token'], $uInf['uid']);

						header("Location: " . ($redir != '' ? $redir : WWWHOST . 'cabinet/'));
						exit();
					} else {
						UhCmsApp::getSesInstance()->AddSocialSession($tokenInfo['access_token'], $uInf['uid']);

						//add utm_source
						$redir .= '?utm_campaign=regfromprojectVK';
						$redir .= '&px=1';

						if($this->pageModel->smplAddPersonVK($uInf))
							header('Location: ' . $redir);
						else
							header('Location: ' . WWWHOST.'registration/person/');
					}
				}
			}
		}
	}
	
	protected function checkEmail($email) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE login = '".addslashes($email)."'";
		$res = $this->db->query($query);

		if(empty($res))
			return false;
		else
			return $res[0];
	}
	
}