<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Info extends PageController
{		
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$this->self_contolled = true;
		$this->model = new InfoModel($this->cfg, $this->db, $this->LangId);
	}
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['rlogin'] = $this->getReqParam("rlogin", "");
	
		return $reg;
	}
	
	private function _initRegvars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['rlogin'] = "";
	
		return $reg;
	}
	
	public function handle_action($urlparts)
	{
		if( isset($urlparts[0]) && ($urlparts[0] != "") )
		{
			switch ($urlparts[0]) {
				case 'siterules' : $this->action_siterules(); break;

				case 'project' : $this->action_otherpage($urlparts[0], 2); break;
				case 'concepts' : $this->action_otherpage($urlparts[0], 4); break;
				case 'users' : $this->action_otherpage($urlparts[0], 5); break;
				case 'stat' : $this->action_otherpage($urlparts[0], 6); break;
				case 'power' : $this->action_otherpage($urlparts[0], 7); break;
				case 'fees' : $this->action_otherpage($urlparts[0], 8); break;
				case 'conf' : $this->action_otherpage($urlparts[0], 9); break;
				case 'donors' : $this->action_otherpage($urlparts[0], 10); break;
				case 'report' : $this->action_otherpage($urlparts[0], 11); break;
				case 'ratrules' : $this->action_otherpage($urlparts[0], 13); break;
				case 'specrules' : $this->action_otherpage($urlparts[0], 14); break;
				case 'sanction' : $this->action_otherpage($urlparts[0], 15); break;
				case 'makehlp' : $this->action_otherpage($urlparts[0], 18); break;
				case 'active' : $this->action_otherpage($urlparts[0], 19); break;
				case 'treeup' : $this->action_otherpage($urlparts[0], 20); break;
				case 'angelsinfo' : $this->action_angels(); break;
				case 'projectinfo' : $this->action_project(); break;
				case 'projectrise' : $this->action_riseproject(); break;
				case 'projexample' : $this->action_projectpract(); break;
				case 'agreement' : $this->action_agreement(); break;

				
				case 'howitwork' : $this->action_work(); break;
				case 'rules' : $this->action_rules(3); break;
				case 'tree' : $this->action_tree(12); break;
				case 'err404' : $this->action_otherpage(); break;
				case 'about' :
					if($urlparts[1] == 'send') {
						$this->pageModel->setUserMsg($this->getMsgParams());
						$this->goToUrl(WWWHOST.'info/'.$urlparts[0].'/');
					}
						else
					$this->action_about(17);
					break;
				default : $this->action_otherpage(); break;
			}
		}		
		else
		{
			$this->action_default();			
		}
	}

	public function getMsgParams() {
		$par['name'] = addslashes(htmlspecialchars($this->getReqParam("name", "")));
		$par['email'] = addslashes(htmlspecialchars($this->getReqParam("email", "")));
		$par['phone'] = addslashes(htmlspecialchars($this->getReqParam("phone", "")));
		$par['msg'] = addslashes(htmlspecialchars($this->getReqParam('umsg', '')));
		return $par;
	}
	
	public function action_default()
	{
		//$this->pageView->render_default();
		header("Location: ".WWWHOST);
		exit();
	}	
	
	public function action_runpage($pageurl)
	{
		$this->pageModel = new InfoModel($this->cfg, $this->db, $this->LangId, $pageurl);
		$this->pageView->setPageModel($this->pageModel);		
		$this->pageView->render_infopage();
	}

	protected function action_siterules() {
		$this->pageView->pageInfo = $this->model->getPageData(1);

		$this->pageView->render_siterules();
	}

	protected function action_otherpage($view='', $lid='') {
		if(!$view || !$lid) {
			$this->pageView->render404();
			return;
		}

		$this->pageView->viewMode = $view;
		$this->pageView->pageInfo = $this->model->getPageData($lid);
		
		$this->pageView->render_otherpage();
	}

	protected function action_rules($lid) {
		$sects = $this->catLib->Catalog_SectLevel(CAT_HELPGET_MODE, 0, false,"firstpage");

		for( $i=0; $i<count($sects); $i++ ) {
			$sects[$i]['its_need_num'] = $this->catLib->Catalog_ItemsNum($sects[$i]['id'], 2, "active");
			$sects[$i]['its_do_num'] = $this->catLib->Catalog_ItemsReqNum($sects[$i]['id'], 2, "active", REQ_STATUS_CONFIRM);
		}

		$this->pageView->sects = $sects;
		$this->pageView->pageInfo = $this->model->getPageData($lid);
		$this->pageView->render_rules();
	}

	protected function action_tree($lid) {
		$this->pageView->pageInfo = $this->model->getPageData($lid);
		$this->pageView->render_tree();
	}

	protected function action_about($lid) {
		$this->pageView->pageInfo = $this->model->getPageData($lid);
		$this->pageView->render_about();
	}

	protected function action_work() {
		$this->pageView->render_work();
	}

	protected function action_angels() {
		$this->pageView->render_angels();
	}
	
	protected function action_project() {
		$this->pageView->render_project();
	}
	
	protected function action_riseproject() {
		$this->pageView->render_riseproject();
	}
	
	protected function action_projectpract() {
		$this->pageView->render_projectpract();
	}
	protected function action_agreement() {
		$this->pageView->render_agreement();
	}
	/*
	public function action_logout()
	{
		// Init POST vars to assoc array
		//$reg = $this->_initRegvars();
		
		// Build object to pass data to form view and model
		//$formreq = new UhCmsFormData($reg);
		
		UhCmsApp::getSesInstance()->AuthClear();
		
		$this->pageView->render_default();
	}		
	*/
}
?>