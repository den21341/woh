<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Install extends Controller{
	
	public function index()
	{
		echo "UhCms is installed";
	}
	
	public function action_default()
	{
		$this->index();
	}
	
	public function action_create()
	{
		if( $this->cfg['RUN_MODE'] == UH_RUNMODE_INSTALL )
		{
			echo "Make install<br />";
			
			include_once "install_src/create.php";
		}
		else
		{
			echo "Install is forbidden";
		}
	}
	
	public function action_addcities()
	{
		$CSV_FILE = "city_ua.csv";
		
		$carr = file($CSV_FILE);
		for( $i=0; $i<count($carr); $i++ )
		{
			$city = explode(";", iconv("cp1251", "UTF-8", $carr[$i]));
			
			echo $city[1].". ".trim($city[2], "\" ");
			echo "<br>";
			
			$this->db->setDebug(true);
			
			$query = "INSERT INTO ".TABLE_CITY." (obl_id, country_id, country_code) VALUES ('".$city[1]."', '1', 'UA')";
			$this->db->exec($query);
			$cityid = $this->db->insert_id();
			$query = "INSERT INTO ".TABLE_CITY_LANG." (city_id, lang_id, name, name_ua) VALUES ('$cityid', '1', '".addslashes(trim($city[2], "\" "))."', '".addslashes(trim($city[3], "\" "))."')";
			$this->db->exec($query);
		}
	}
	
	public function action_addobl()
	{
		global $REGIONS;
		
		$this->db->setDebug(true);
		
		foreach($REGIONS as $id => $obl)
		{
			if( $id == 0 )
				continue;			
				
			$query = "INSERT INTO ".TABLE_REGION." (id, country_id, country_code) VALUES ('$id', '1', 'UA')";
			$this->db->exec($query);
			$oblid = $this->db->insert_id();
			$query = "INSERT INTO ".TABLE_REGION_LANG." (region_id, lang_id, name) VALUES ('$oblid', '1', '".addslashes($REGIONS[$id])."')";
			$this->db->exec($query);
		}
	}
	
	public function action_addlocations()
	{
		$COUNTRY_FILE = "cities/country.csv";
		$REGION_FILE = "cities/region.csv";
		$CITY_FILE = "cities/city.csv";
		
		$c2ind = Array();
		$r2ind = Array();
		$g2ind = Array();
		
		$c2id = Array();
		$r2id = Array();
		$g2id = Array();
		
		$this->db->setDebug(true);
		
		$this->db->exec("TRUNCATE ".TABLE_COUNTRY);
		$this->db->exec("TRUNCATE ".TABLE_COUNTRY_LANG);
		$this->db->exec("TRUNCATE ".TABLE_CITY);
		$this->db->exec("TRUNCATE ".TABLE_CITY_LANG);
		$this->db->exec("TRUNCATE ".TABLE_REGION);
		$this->db->exec("TRUNCATE ".TABLE_REGION_LANG);
		
		$countries = UhCmsUtils::Load_CsvFileAsArray($COUNTRY_FILE, "cp1251");
						
		for($i=1; $i<count($countries); $i++)
		{
			echo $countries[$i][0]." ".$countries[$i][2]."<br>";
			$c2ind[$countries[$i][0]] = $i;
			
			// Add country to list
			$query = "INSERT INTO ".TABLE_COUNTRY." (continent_id, code) VALUES (1, '')";
			$this->db->exec($query);
			$country_id = $this->db->insert_id();
			$query = "INSERT INTO ".TABLE_COUNTRY_LANG." (country_id, lang_id, name) VALUES ('".$country_id."', 1, '".addslashes($countries[$i][2])."')";
			$this->db->exec($query);
			
			$c2id[$countries[$i][0]] = $country_id;
		}
		
		echo "<br><b>Всего стран: ".(count($countries) - 1)."</b><br><br>";
		
		$regions = UhCmsUtils::Load_CsvFileAsArray($REGION_FILE, "cp1251");
		
		for($i=1; $i<count($regions); $i++)
		{
			echo $regions[$i][0]." ".$regions[$i][3]."  === ".$countries[$c2ind[$regions[$i][1]]][2]."<br>";
			$r2ind[$regions[$i][0]] = $i;
			
			$country_id = $c2id[$regions[$i][1]];  // Get id of country in new DB
			
			$query = "INSERT INTO ".TABLE_REGION." (country_id, country_code) VALUES ('".$country_id."', '')";
			$this->db->exec($query);
			$oblid = $this->db->insert_id();
			$query = "INSERT INTO ".TABLE_REGION_LANG." (region_id, lang_id, name) VALUES ('$oblid', '1', '".addslashes($regions[$i][3])."')";
			$this->db->exec($query);
			
			$r2id[$regions[$i][0]] = $oblid;
		}
		
		echo "<br><b>Всего регионов/областей: ".(count($regions) - 1)."</b><br><br>";
		
		
		$cities = UhCmsUtils::Load_CsvFileAsArray($CITY_FILE, "cp1251");
		
		for($i=1; $i<count($cities); $i++)
		{
			echo $cities[$i][0]." ".$cities[$i][3]."  === ".$countries[$c2ind[$cities[$i][1]]][2]."<br>";
			
			$country_id = $c2id[$cities[$i][1]];	// Get id of country in new DB
			$obl_id = $r2id[$cities[$i][2]];		// Get id of the region in new DB
			
			$query = "INSERT INTO ".TABLE_CITY." (obl_id, country_id, country_code) VALUES ('".$obl_id."', '".$country_id."', '')";
			$this->db->exec($query);
			$cityid = $this->db->insert_id();
			$query = "INSERT INTO ".TABLE_CITY_LANG." (city_id, lang_id, name, name_ua) VALUES ('$cityid', '1', '".addslashes($cities[$i][3])."', '')";
			$this->db->exec($query);
		}
		
		echo "<br><b>Всего городов: ".(count($cities) - 1)."</b><br><br>";
		
		// 1 - Киевская, 2 - Харьковская, 4 - Винницкая, 6 - Днепр, 14 - Львовская, 16 - Одесская, 17 - Полтавск, 20 - Тернопольск., 21 - Херсонская, 22 - Хмельницкая
		$translate_lo = Array(1 => 709, 2 => 721, 4 => 701, 6 => 703, 14 => 713, 16 => 715, 17 => 716, 20 => 719, 21 => 722, 22 => 723);
		// 19 - Барышевка, 69 - Бородянка, 79 - Брусилов, 139 - Винница, 131 - Вишневое, 251 - Запорожье, 587 - Полтава, 720 - Тернополь, 736 - Трускавец
		// 30 - Березань, 39 - Белая церковь, 66 - Борисполь, 76 - Бровари, 129 - Вышгород, 290 - Ирпень,  
		// 314 - Киев, 420 - Львов, 530 - Одесса, 564 - Переяслав-хмельницк, 752 - Харьков, 753 -Херсон, 755 - Хмельницкий
		$translate_lc = Array(19 => 9885, 30 => 9887, 39 => 9886, 66 => 9889, 69 => 9891, 76 => 9893, 79 => 9789, 129 => 9897, 
				131 => 9905, 139 => 9640, 216 => 9699, 251 => 9839, 290 => 9902, 314 => 9905, 420 => 10057, 530 => 10117, 
				564 => 9912, 587 => 10150, 720 => 10218, 736 => 10070, 752 => 10268, 753 => 10291, 755 => 10314);
		
		foreach($translate_lo as $k => $v)
		{
			$query = "UPDATE ".TABLE_CAT_ITEMS." SET obl_id=".$v." WHERE obl_id='".$k."'";
			$this->db->exec($query);
			
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET obl_id=".$v." WHERE obl_id='".$k."'";
			$this->db->exec($query);
		}
		
		foreach($translate_lc as $k => $v)
		{
			$query = "UPDATE ".TABLE_CAT_ITEMS." SET city_id=".$v." WHERE city_id='".$k."'";
			$this->db->exec($query);
				
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET city_id=".$v." WHERE city_id='".$k."'";
			$this->db->exec($query);
		}
	}
	
	public function action_addlang()
	{
		$langcode = $this->getReqParam("langcode", "");
		$langname = $this->getReqParam("langname", "");
		$langshort = $this->getReqParam("langshort", "");
		
		if( $langcode == "" )
		{
			echo "Не передан код языка";
			return;
		}
		
		// Create table records for new language
		
		echo "<b>Sections</b><br>";
		// Add sections 
		$query = "SELECT c1.*, c2.name, c2.descr FROM ".TABLE_CAT_CATALOG." c1 
			INNER JOIN ".TABLE_CAT_CATALOG_LANGS." c2 ON c1.id=c2.sect_id AND c2.lang_id=1 
			ORDER BY c1.id";
		$sects = $this->db->query($query);
				
		for( $i=0; $i<count($sects); $i++ )
		{
			echo $sects[$i]['id']." - ".$sects[$i]['name']."<br>";
		}
		
		echo "<b>Projects</b><br>";
		// Add projects
		$query = "SELECT c1.*, c2.title2, c2.descr FROM ".TABLE_CAT_ITEMS." c1
			INNER JOIN ".TABLE_CAT_ITEMS_LANGS." c2 ON c1.id=c2.item_id AND c2.lang_id=1
			ORDER BY c1.id";
		$its = $this->db->query($query);
		
		for( $i=0; $i<count($its); $i++ )
		{
			echo $its[$i]['id']." - ".$its[$i]['title2']."<br>";
		}
		
		echo "<b>Pages</b><br>";
		// Add projects
		$query = "SELECT c1.*, c2.page_mean, c2.page_title FROM ".TABLE_PAGES." c1
			INNER JOIN ".TABLE_PAGES_LANGS." c2 ON c1.id=c2.item_id AND c2.lang_id=1
			ORDER BY c1.parent_id, c1.id";
		$its = $this->db->query($query);
		
		for( $i=0; $i<count($its); $i++ )
		{
			echo $its[$i]['id']." - ".$its[$i]['page_mean']."<br>";
		}
		
		echo "<b>Resources</b><br>";
		// Add projects
		$query = "SELECT c1.*, c2.content FROM ".TABLE_RESOURCE." c1
			INNER JOIN ".TABLE_RESOURCE_LANGS." c2 ON c1.id=c2.item_id AND c2.lang_id=1
			ORDER BY c1.id";
		$its = $this->db->query($query);
		
		for( $i=0; $i<count($its); $i++ )
		{
			echo $its[$i]['id']." - ".$its[$i]['title']."<br>";
		}
	}
}
?>