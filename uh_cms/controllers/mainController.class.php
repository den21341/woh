<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Main extends PageController {
	protected $main;

	public function __construct($config, $db) {
		parent::__construct($config, $db);
		$this->main = new MainModel($this->cfg, $this->db, $this->LangId);
	}

	public function action_default()
	{			
		//$this->model = new MainModel($this->getCfg(), $this->getPageModel(), 1);		
		//$this->model = $this->getPageModel();
		//$this->pageView = new MainView($this->getCfg(), $this->pageModel);
		
		// If the user first time on site - show video popup
		$first_time_on_site = $this->pageModel->isFirstVisit();

		//$cat = new Catalog( $this->db, 1 );
				
		$sects = $this->catLib->Catalog_SectLevel(CAT_HELPGET_MODE, 0, false,"firstpage");
		
		for( $i=0; $i<count($sects); $i++ )
		{
			$sects[$i]['its_need_num'] = $this->catLib->Catalog_ItemsNum($sects[$i]['id'], 2, "active");			
			$sects[$i]['its_do_num'] = $this->catLib->Catalog_ItemsReqNum($sects[$i]['id'], 2, "active", REQ_STATUS_CONFIRM);
		}
			
		$this->pageView->sects = $sects;
		$this->pageView->revlist = $this->catLib->Req_List(8, "index", 0, false, -1, 0, 3);
		$this->pageView->first_visit = $first_time_on_site;
		$this->pageView->treeCount = $this->main->getUsersTree()['count'];
		$this->pageView->actUser = $this->catLib->getUserWithAngStat();
		$this->pageView->actProj = $this->catLib->getPopularProject();

		$only_by = 'success';
		$this->pageView->endCount = $this->pageView->its_total = $this->catLib->Item_Num(PROJ_ANY, 0, 0, -1, "", ( $only_by != "" ? PROJ_STATUS_ENDED : PROJ_STATUS_RUN ), "", "", $only_by, ($only_by != "" ? false : true) );
		
		$this->pageView->setAdaptiveTop(true);
		$this->pageView->render_default();
						
		//echo "Slides num: ".$this->pageModel->get_slides()->length()."<br />";		
		//var_dump($this->pageModel->get_continfo());		
		//var_dump($this->pageModel->get_txtres());
	}
		
}
?>