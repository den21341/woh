<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Crons extends Controller {
	protected $model;
	protected $view;
	protected $LangId;
	
	function __construct($config, $db) {
		parent::__construct($config, $db);
		
		$this->LangId = UhCmsApp::getLangId();
						
		/////////////////////////////////////////////////////////////////////////
		// Load page model source code
		$fileModel = $config['MVC_PATH']."models/cronsModel.class.php";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Controller');
		}
		
		include_once $fileModel;
		
		// Load page view class source code
		$fileView = $config['MVC_PATH']."views/cronsView.class.php";
	
		if( !file_exists($fileView) )
		{
			die('404 Not Found - Controller');
		}
		
		include_once $fileView;		

		//////////////////////////////////////////////////////////////////////////
		// Create Model and View
		$this->model = new CronsModel($this->getCfg(), $this->db, $this->LangId);
		$this->view = new CronsView($this->getCfg(), $this->model);

		// ADD SENDPULSE API
		require_once( 'sendpulse/sendpulseInterface.php' );
		require_once( 'sendpulse/sendpulse.php' );

	}
	
	public function action_default()
	{
		// Do nothing
		$this->view->render_default();		
	}
	
	public function action_dailyrate()
	{
		$this->model->Item_DailyRateUpdate();
	}	

	public function action_stopclosed()
	{
		$this->model->Item_CloseEnded();
	}
	
	// Calculate visit rating from published posts for each user
	public function action_calcscrateday()
	{
		$this->model->User_ScSharedPostsDayRatingUpdate();
	}
	
	public function action_calcscrateall()
	{
		$this->model->User_ScSharedPostsTotalRatingUpdate();
	}
	
	// Calculate total project rating
	public function action_calcrating() {
		$catLib = new Catalog($this->db, $this->LangId);
		
		$this->model->new_Item_PriorityRatingUpdate($catLib);
	}
	
	// Sum all help to ended projectes
	public function action_amountsuccess() {
		//$catLib = new Catalog($this->db, $this->LangId);
	
		//$this->model->Item_AmountSum($catLib);
		$this->model->checkSuccessItems();
	}

	public function action_calcDiffMoney() {
		$this->model->userDailyDiffMoney();
	}

	public function action_currencySend() {
		$this->model->currencySendPerDay();
	}

	public function action_stopuserproj() {
		$this->model->stopUserProject();
	}

	public function action_checksocial() {
		$this->model->checkUserSocial();
	}

	public function action_checkprom() {
		$this->model->checkPromisePays();
	}

	public function action_checkProjData() {
		$this->model->checkDoneData();
	}

	public function action_dailymail() {
		$this->model->dailyUserMail();
	}

	public function action_sendPulseTest() {
		$this->model->sendPulseTest();
	}

	public function action_cronsQuery() {
		$catLib = new Catalog($this->db, $this->LangId);

		$cq = $catLib->getCronsQuery();

		if(!empty($cq)) {
			$next = $cq[0]['crons_next'];

			//echo $next.'<br>';

			if($next == 1) {
				echo 'sendUserNewBon';
				$this->model->sendUserNewBon();
			}
			if($next == 2) {
				echo 'sendTopNews';
				$this->model->sendTopNews();
			}
			if($next == 3) {
				echo 'sendNewItemMsg';
				$this->model->sendNewItemMsg();
			}

			if($next >= count($cq)) {
				$catLib->getCronsQuery(1);
			} else {
				$catLib->getCronsQuery($next + 1);
			}
		}

	}

	public function action_usersNews() {
		$this->model->sendGoodJob();
	}

	public function action_drun() {
		$this->model->sendTopNews();
	}
	
	public function action_runbotpays() {
		$this->model->run_BotsAddToProj(new Catalog($this->db, $this->LangId));
	}

	public function action_calcUserRankRate() {
		$this->model->calcUserRateRank();
	}

	public function action_calcUserRate() {
		$this->model->calcUserRate(new Catalog($this->db, $this->LangId));
	}

	public function action_calcItemViews() {
		$this->model->calcItemViews(new Catalog($this->db, $this->LangId));
	}

	public function action_calcUserHelp() {
		$this->model->updUserHelp(new Catalog($this->db, $this->LangId));
	}


}