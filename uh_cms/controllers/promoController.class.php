<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Promo extends PageController
{		
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$this->self_contolled = true;
	}
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['rlogin'] = $this->getReqParam("rlogin", "");
	
		return $reg;
	}
	
	private function _initRegvars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['rlogin'] = "";
	
		return $reg;
	}
	
	public function handle_action($urlparts)
	{
		if( isset($urlparts[0]) && ($urlparts[0] != "") )
		{
			$this->action_runpage($urlparts[0]);
		}		
		else
		{
			$this->action_default();			
		}
	}
	
	public function action_default()
	{
		//$this->pageView->render_default();
		header("Location: ".WWWHOST);
		exit();
	}	
	
	public function action_runpage($pageurl)
	{
		//$this->pageModel = new InfoModel($this->cfg, $this->db, $this->LangId, $pageurl);
		//$this->pageView->setPageModel($this->pageModel);		
		//$this->pageView->render_infopage();
		$this->pageView->render_promopage($pageurl.".php");
	}
	
	/*
	public function action_logout()
	{
		// Init POST vars to assoc array
		//$reg = $this->_initRegvars();
		
		// Build object to pass data to form view and model
		//$formreq = new UhCmsFormData($reg);
		
		UhCmsApp::getSesInstance()->AuthClear();
		
		$this->pageView->render_default();
	}		
	*/
}
?>