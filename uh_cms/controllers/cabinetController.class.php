<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("MSG_ITEMS_PERPAGE", 20);

class Cabinet extends PageController {

	// Проверяет авторизирован ли пользователь, и если нет, то перекидывает его на главную сайта

	private function authAllow() {

		$uid = UhCmsApp::getSesInstance()->UserId;

		if( $uid == 0 ) {
			header("Location: ".WWWHOST);
			exit();
		}
		
		return $uid;
	}
	
	// Главная страница кабинета с графами

	public function action_default() {

		$uid = $this->authAllow();
		$uinfo = $this->catLib->Buyer_Info($uid);
		$arr_count = [];
		$arr_count[] = $this->catLib->itemCount(2,0)+$this->catLib->itemCount(2,2);
		$arr_count[] = $this->catLib->itemCount(0,0);

		$city = $this->catLib->Loc_City($uinfo['city_id']);

		// QUERY TO SET POPTIP COUNT

		$this->pageView->is_pop = $this->catLib->setPoptipCount($uinfo, $date = date('Y/m/d'), 20, 2);

		if( $city != null )
			$uinfo['city'] = $city['name'];

		$uinfo['group_name'] = $this->catLib->Buyer_GroupInfo($uinfo['account_type']);
		$uinfo['helprate'] = $this->catLib->Buyer_ReqStarsRate($uid);

		$uinfo['loc_info'] = $this->catLib->Loc_InfoByCity($uinfo['city_id']);
		$this->pageView->UserId = $uid;
		$this->pageView->userinfo = $uinfo;
		$this->pageView->countWork = $arr_count;
		$this->pageView->closeTip = true;
		$this->pageView->box = $this->pageModel->getUserBox($uinfo['id']);

		$this->_prepare_tabinfo($uid);
		
		$this->pageView->render_default();
	}
	
	protected function _prepare_tabinfo($uid) {

		/*$this->pageView->tabNeedHelpTot = $this->catLib->Item_Num(Array(PROJ_NEEDHELP,PROJ_EVENT), 0, $uid, 3, "", PROJ_STATUS_ALL);
		$this->pageView->tabGiveHelpTot = $this->catLib->Item_Num(PROJ_THINGS, 0, $uid, 3, "", PROJ_STATUS_ALL);
		$this->pageView->tabNewHelp = $this->catLib->Buyer_ReqNum($uid, PROJ_SENDHELP, REQ_STATUS_ALL, PROJ_STATUS_ALL, -1, false, 0);
		//$this->pageView->tabNewMyHelp = $this->catLib->Buyer_ReqNum($uid, PROJ_NEEDHELP, REQ_STATUS_ALL, PROJ_STATUS_ALL, -1, false, 0);
		$this->pageView->tabMyHelpTot = $this->catLib->Item_Num(PROJ_SENDHELP, 0, $uid, 3, "", PROJ_STATUS_ALL);

		$this->pageView->tabDoHelpTot = $this->catLib->Buyer_ReqNum($uid, PROJ_SENDHELP, REQ_STATUS_ALL, PROJ_STATUS_ALL);

		$this->pageView->tabMsgNewNum = $this->pageModel->msg_NumNew($uid);
		$this->pageView->tabBonusNum = $this->pageModel->bonus_Num($uid);
		//$this->pageView->tabMyHelpNew = $this->pageModel->getTabCount('myhelp'); */
		$this->pageView->need_services = $this->getPageModel()->getTabCount('needservices', $uid);
		$this->pageView->my_services = $this->getPageModel()->getTabCount('myservices', $uid);
		$this->pageView->needhelp = $this->getPageModel()->getTabCount('needhelp', $uid);
		$this->pageView->myhelp = $this->getPageModel()->getTabCount('myhelp', $uid);
		$this->pageView->mymsg = $this->getPageModel()->getTabCount('mymsg', $uid);
		$this->pageView->thinghelp = $this->getPageModel()->getTabCount('thinghelp', $uid);
		$this->pageView->mybonus = $this->getPageModel()->getTabCount('mybonuswin', $uid);
		$this->pageView->helpcount = $this->catLib->getBuyerReqNum($uid, 1)[0]['hlpco'];

	}
		
	///////////////////////////////////////////////////////////////////////////////////////
	// my projects actions
	
	// Подготовка списка проектов моей помощи для визуализации

	protected function _prepare_myhelplist($uid, $proj_status=PROJ_STATUS_ALL, $pi=-1, $pn=10) {

		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );
	
		$its = $this->catLib->Item_List( PROJ_SENDHELP, 0, $uid, $pi, $pn, 3, "add", $proj_status);
		$its_total = $this->catLib->Item_Num( PROJ_SENDHELP, 0, $uid, 3, "", $proj_status);
	
		$this->pageView->UserId = $uid;
		$this->pageView->UserGroup = UhCmsApp::getSesInstance()->UserGroup;
		$this->pageView->projlist = $its;
		$this->pageView->projlist_total = $its_total;
		
		$this->_prepare_tabinfo($uid);
	}
	
	public function action_myhelp() {
		$uid = $this->authAllow();
		
		$urlparts = $this->urlparts;
		
		$filt_by = "";
		$pi = 0;
		
		//var_dump($urlparts[0]);
		
		// Check url for sort mode

		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^filt_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 ) {
				$filt_by = $matches[1];
				array_shift($urlparts);
			}
		}
		
		// Check url for paging mode

		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^p_([0-9]+)$/", $urlparts[0], $matches)>0 ) {
				$pi = $matches[1];
				array_shift($urlparts);
		
				$pi--;
			}
		}
		
		$filt_status_id = PROJ_STATUS_ALL;

		switch($filt_by) {
			case "running":		$filt_status_id = PROJ_STATUS_RUN;		break;
			case "finished":	$filt_status_id = PROJ_STATUS_ENDED;	break;
		}
		
		$this->pageView->filtby = "";

		if( $filt_status_id != PROJ_STATUS_ALL )		
			$this->pageView->filtby = $filt_by;
		
		$this->pageView->pi = $pi;
		$this->pageView->pn = 10;

		$this->pageView->closeTip = true;
		
		//$this->_prepare_needhelplist($uid, $this->pageView->filtby, $this->pageView->pi, $this->pageView->pn);
		$this->_prepare_myhelplist($uid, $filt_status_id, $this->pageView->pi, $this->pageView->pn);
		
		$this->pageView->render_proj_myhelp();
	}
	
	// Подготовка списка моих проектов для визуализации
	protected function _prepare_needhelplist($uid, $proj_status=PROJ_STATUS_ALL, $pi=-1, $pn=10) {

		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );

		$its = $this->catLib->Item_List( Array(PROJ_NEEDHELP, PROJ_EVENT), 0, $uid, $pi, $pn, 3, "add", $proj_status, $fltby = "", $cityby = "", $onlyactive = true, $coId = -1, $oblId = 0, $moder = -1, $noLoc = 0, $help_type = -1);
		//$this->pageView->projlist = $this->catLib->Item_List(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, $user_info['id'], -1, 20, 3, "", PROJ_STATUS_ALL, "", "", true, -1, 0, 1, 0, -1);

		$its_total = $this->catLib->Item_Num( Array(PROJ_NEEDHELP, PROJ_EVENT), 0, $uid, 3, "", $proj_status);
	
		$this->pageView->UserId = $uid;
		$this->pageView->UserGroup = UhCmsApp::getSesInstance()->UserGroup;
		$this->pageView->projlist = $its;
		$this->pageView->projlist_total = $its_total;
		
		$this->_prepare_tabinfo($uid);
	}

	// Показать проекты на получение помощи, которые создал пользователь

	public function action_needhelp() {
		$uid = $this->authAllow();
		
		$urlparts = $this->urlparts;
		
		$filt_by = "";
		$pi = 0;
		
		//var_dump($urlparts[0]);
		
		// Check url for sort mode
		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^filt_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 ) {
				$filt_by = $matches[1];
				array_shift($urlparts);
			}
		}
		
		// Check url for paging mode
		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^p_([0-9]+)$/", $urlparts[0], $matches)>0 ) {
				$pi = $matches[1];
				array_shift($urlparts);
		
				$pi--;
			}
		}
		
		$filt_status_id = PROJ_STATUS_ALL;

		switch($filt_by) {
			case "running":		$filt_status_id = PROJ_STATUS_RUN;		break;
			case "finished":	$filt_status_id = PROJ_STATUS_ENDED;	break;
		}
		
		$this->pageView->filtby = "";

		if( $filt_status_id != PROJ_STATUS_ALL )		
			$this->pageView->filtby = $filt_by;
		
		$this->pageView->pi = $pi;
		$this->pageView->pn = 10;

		$this->pageView->closeTip = true;
		
		//$this->_prepare_needhelplist($uid, $this->pageView->filtby, $this->pageView->pi, $this->pageView->pn);
		$this->_prepare_needhelplist($uid, $filt_status_id, $this->pageView->pi, $this->pageView->pn);
		
		$this->pageView->render_proj_needhelp();
	}

	// Удалить проект - остановить его

	public function action_projdel() {
		$uid = $this->authAllow();
	
		$projid = $this->getReqParam("projid", 0);
	
		$this->pageModel->del_Item($uid, $projid);
	
		//$this->_prepare_needhelplist($uid);
	
		//$this->pageView->render_proj_needhelp();
		$this->action_needhelp();
	}
	
	// Возобновить работу проекта - запустить его

	public function action_projstart() {

		$uid = $this->authAllow();
	
		$projid = $this->getReqParam("projid", 0);
	
		$this->pageModel->start_Item($uid, $projid);
	
		//$this->_prepare_needhelplist($uid);
	
		//$this->pageView->render_proj_needhelp();
		$this->action_needhelp();
	}
	// Завершить проект как успешный

	public function action_projsuccess() {

		$uid = $this->authAllow();
		$projid = $this->getReqParam("projid", 0);
		$none = $this->getReqParam("none", '');
		
		$pinfo = $this->catLib->getProjReq($projid);

			if ($pinfo['status'] != PROJ_STATUS_PAYED && ($pinfo['type'] == PROJ_TYPE_MONEY && (($pinfo['req'] / $pinfo['amount']) * 100) >= 60) || $pinfo['type'] != PROJ_TYPE_MONEY && $pinfo['req'] > 0) {
				$this->pageModel->success_Item($uid, $projid, $this->catLib);
			}

		//$this->_prepare_needhelplist($uid);
		//$this->pageView->render_proj_needhelp();
		if($none != 1)
			$this->action_needhelp();
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	// my give things projects
	
	protected function _prepare_givehelplist($uid, $proj_status=PROJ_STATUS_ALL, $pi=-1, $pn=10) {

		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );
	
		$its = $this->catLib->Item_List(PROJ_THINGS, 0, $uid, $pi, $pn, 3, "add", $proj_status);
		$its_total = $this->catLib->Item_Num(PROJ_THINGS, 0, $uid, 3, "", $proj_status);
	
		$this->pageView->UserId = $uid;
		$this->pageView->UserGroup = UhCmsApp::getSesInstance()->UserGroup;
		$this->pageView->projlist = $its;
		$this->pageView->projlist_total = $its_total;
		$this->_prepare_tabinfo($uid);
	}
	
	// Показать проекты - поделиться вещами, которые создал пользователь

	public function action_givehelp() {
		$uid = $this->authAllow();
	
		$urlparts = $this->urlparts;
	
		$filt_by = "";
		$pi = 0;
	
		//var_dump($urlparts[0]);
	
		// Check url for sort mode

		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^filt_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 ) {
				$filt_by = $matches[1];
				array_shift($urlparts);
			}
		}
	
		// Check url for paging mode
		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^p_([0-9]+)$/", $urlparts[0], $matches)>0 ) {
				$pi = $matches[1];
				array_shift($urlparts);
	
				$pi--;
			}
		}
	
		$filt_status_id = PROJ_STATUS_ALL;

		switch($filt_by) {
			case "running":		$filt_status_id = PROJ_STATUS_RUN;		break;
			case "finished":	$filt_status_id = PROJ_STATUS_ENDED;	break;
		}
	
		$this->pageView->filtby = "";
		if( $filt_status_id != PROJ_STATUS_ALL )
			$this->pageView->filtby = $filt_by;
	
		$this->pageView->pi = $pi;
		$this->pageView->pn = 10;
	
		//$this->_prepare_needhelplist($uid, $this->pageView->filtby, $this->pageView->pi, $this->pageView->pn);
		$this->_prepare_givehelplist($uid, $filt_status_id, $this->pageView->pi, $this->pageView->pn);
	
		//$this->pageView->render_proj_needhelp();
		$this->pageView->render_proj_givehelp();
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	// my gethelp actions
	
	// Подготовить список заявок, отправленных на оказание помощи для визуализации

	protected function _prepare_gethelplist($uid, $proj_status=PROJ_STATUS_ALL, $pi=-1, $pn=10) {

		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );
	
		//$its = $this->catLib->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_ALL, "", $pi, $pn, $proj_status, 0, -10, 0, 1);
		//$its_total = $this->catLib->Buyer_ReqNum($uid, PROJ_SENDHELP, REQ_STATUS_ALL, $proj_status);
		
		$its = $this->catLib->Buyer_MyhelpReqList($uid, PROJ_SENDHELP, REQ_STATUS_ALL, "", $pi, $pn, $proj_status, 0, -10, 0, 1);
		$its_total = $this->catLib->Buyer_MyhelpReqNum($uid, PROJ_SENDHELP, REQ_STATUS_ALL, $proj_status);
	
		$this->pageView->UserId = $uid;
		$this->pageView->projlist = $its;
		$this->pageView->projlist_total = $its_total;
		$this->pageView->uinfo = $this->catLib->Buyer_Info($uid);

		/*echo '<pre>';
		print_r($its); die();*/

		$count = count($its);

		// IF U NEED TO SHOW VIDEO-POPUP DECOMMENT

		for($i=0; $i<$count; ++$i) {
			if (!$this->catLib->checkVideoView($uid, $its[$i]['item_id']) && $its[$i]['req_status']) {
				//$this->pageView->videoSub = ['name' => $its[$i]['uname'], 'pic' => $its[$i]['pic'], 'item_id' => $its[$i]['item_id'], 'user_id' => $its[0]['id']];
				break;
			}
		}
		
		$this->_prepare_tabinfo($uid);
	}
	
	// Показать проекты, куда пользователь отправил заявки на помощь

	public function action_myhelpget() {
		$uid = $this->authAllow();
		
		$urlparts = $this->urlparts;
		
		$filt_by = "";
		$pi = 0;
		
		//var_dump($urlparts[0]);
		
		// Check url for filter mode

		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^filt_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 ) {
				$filt_by = $matches[1];
				array_shift($urlparts);
			}
		}
		
		// Check url for paging mode

		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^p_([0-9]+)$/", $urlparts[0], $matches)>0 ) {
				$pi = $matches[1];
				array_shift($urlparts);
				
				$pi--;
			}
		}

		$filt_status_id = PROJ_STATUS_ALL;

		switch($filt_by) {
			case "running":		$filt_status_id = PROJ_STATUS_RUN;		break;
			case "finished":	$filt_status_id = PROJ_STATUS_ENDED;	break;
		}
		
		$this->pageView->filtby = "";

		if( $filt_status_id != PROJ_STATUS_ALL )
			$this->pageView->filtby = $filt_by;

		$this->pageView->pi = $pi;
		$this->pageView->pn = 10;


		$this->_prepare_gethelplist($uid, $filt_status_id, $this->pageView->pi, $this->pageView->pn );
		
		$this->pageView->render_proj_myhelpget();
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	// my dohelp actions
	
	// Подготовить список заявок, отправленных на оказание помощи для визуализации

	protected function _prepare_dohelplist($uid, $proj_status=PROJ_STATUS_ALL, $pi=-1, $pn=10) {
		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );
	
		$its = $this->catLib->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_ALL, "", $pi, $pn, $proj_status, 0, -10, 0, 1);
		$its_total = $this->catLib->Buyer_ReqNum($uid, PROJ_SENDHELP, REQ_STATUS_ALL, $proj_status);
	
		$this->pageView->UserId = $uid;
		$this->pageView->projlist = $its;
		$this->pageView->projlist_total = $its_total;
		$this->pageView->uinfo = $this->catLib->Buyer_Info($uid);

		/*echo '<pre>';
		print_r($its); die();*/

		$count = count($its);

		// IF U NEED TO SHOW VIDEO-POPUP DECOMMENT

		for($i=0; $i<$count; ++$i) {

			if (!$this->catLib->checkVideoView($uid, $its[$i]['item_id']) && $its[$i]['req_status']) {
				//$this->pageView->videoSub = ['name' => $its[$i]['uname'], 'pic' => $its[$i]['pic'], 'item_id' => $its[$i]['item_id'], 'user_id' => $its[0]['id']];
				break;
			}
		}
		
		$this->_prepare_tabinfo($uid);
	}
	
	// Показать проекты, куда пользователь отправил заявки на помощь

	public function action_dohelp() {
		$uid = $this->authAllow();
		
		$urlparts = $this->urlparts;
		
		$filt_by = "";
		$pi = 0;
		
		//var_dump($urlparts[0]);
		
		// Check url for filter mode
		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^filt_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 ) {
				$filt_by = $matches[1];
				array_shift($urlparts);
			}
		}
		
		// Check url for paging mode

		if( isset($urlparts[0]) && ($urlparts[0] != "") ) {

			if( preg_match("/^p_([0-9]+)$/", $urlparts[0], $matches)>0 ) {
				$pi = $matches[1];
				array_shift($urlparts);
				
				$pi--;
			}
		}

		$filt_status_id = PROJ_STATUS_ALL;

		switch($filt_by) {
			case "running":		$filt_status_id = PROJ_STATUS_RUN;		break;
			case "finished":	$filt_status_id = PROJ_STATUS_ENDED;	break;
		}
		
		$this->pageView->filtby = "";

		if( $filt_status_id != PROJ_STATUS_ALL )
			$this->pageView->filtby = $filt_by;

		$this->pageView->pi = $pi;
		$this->pageView->pn = 10;


		$this->_prepare_dohelplist($uid, $filt_status_id, $this->pageView->pi, $this->pageView->pn );
		
		$this->pageView->render_proj_dohelp();
	}
	
	//////////////////////////////////////////////////////////////////////////////////////
	// Reqest actions
	
	// Отменить отправленную заявку на момощь

	public function action_reqcancel() {
		// Action to cancel request when it's new. Performed by author
		$uid = $this->authAllow();
	
		// Extract id from url like this (cabinet/reqcancel/22)
		$req_id = 0;
	
		if( count($this->urlparts)>0 ) {
			$req_url = $this->urlparts[0];
	
			// Check if this project exists
			$req_info = $this->catLib->Req_Info($req_url);
			if( $req_info != null )
				$req_id = $req_info['id'];
		}
	
		if( $req_id == 0 )
			$this->goToUrl($this->page_View->Page_BuildUrl("cabinet", "dohelp"));
	
		if( $req_info['sender_id'] != $uid )
			$this->goToUrl($this->page_View->Page_BuildUrl("cabinet", "dohelp"));
	
		if( $req_info['req_status'] == REQ_STATUS_NEW ) {
			$this->pageModel->set_ItemReqStatus($req_id, REQ_STATUS_CANCEL);
		}
		
		$this->_prepare_dohelplist($uid);
		
		$this->pageView->render_proj_dohelp();
	}
	
	// Подготовить перечень заявок по проекту

	protected function _prepare_reqlist($uid, $proj_info, $req_status=REQ_STATUS_ALL) {
		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );
		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet", "needhelp"), "Нуждаюсь в помощи" );
	
		$its = $this->catLib->Item_ReqList($proj_info['id'], $req_status);
		$its_total = $this->catLib->Item_ReqCollected($proj_info['id'], $req_status);
	
		$this->pageView->UserId = $uid;
		$this->pageView->projinfo = $proj_info;
		$this->pageView->reqlist = $its;
		$this->pageView->reqlist_info = $its_total;
		
		$this->_prepare_tabinfo($uid);
	}
	
	// 
	public function action_reqlist() {
		$uid = $this->authAllow();
		
		// Extract id from url like this (cabinet/reqlist/22)
		$proj_id = 0;
		$filt_by = "";
		
		if( count($this->urlparts)>0 ) {
			$proj_url = $this->urlparts[0];
				
			// Check if this project exists
			$proj_info = $this->catLib->Item_Info($proj_url);

			if( $proj_info != null )
				$proj_id = $proj_info['id'];
		}
				
		if( count($this->urlparts)>1 ) {

			if( preg_match("/^filt_([a-zA-Z]+)$/", $this->urlparts[1], $matches)>0 ) {
				$filt_by = $matches[1];				
			}
		}		
		
		if( $proj_id == 0 )
			$this->go404();

		if( $proj_info['author_id'] != $uid )
			$this->go404();
				
		$filt_req_status = REQ_STATUS_ALL;

		switch($filt_by) {
			case "new":			$filt_req_status=REQ_STATUS_NEW;	break;	
			case "confirmed":	$filt_req_status=REQ_STATUS_CONFIRM;	break;
			case "declined":	$filt_req_status=REQ_STATUS_DECLINE;	break;
		}

		if( $filt_req_status != REQ_STATUS_ALL )
			$this->pageView->filtby = $filt_by;
		
		
		$this->_prepare_reqlist($uid, $proj_info, $filt_req_status);
		
		$this->pageView->render_proj_reqlist();		
	}		
	
	// Принять заявку на помощь
	public function action_reqaccept() {
		$this->_req_accept_decline("accept");
	}
	
	// Отклонить заявку на помощь
	public function action_reqdecline() {
		$this->_req_accept_decline("decline");
	}	
	
	protected function _req_accept_decline($todo = "") {
		$uid = $this->authAllow();
		
		// Extract id from url like this (cabinet/reqlist/22)
		$proj_id = 0;
		
		if( count($this->urlparts)>0 ) {
			$proj_url = $this->urlparts[0];
		
			// Check if this project exists
			$proj_info = $this->catLib->Item_Info($proj_url);

			if( $proj_info != null )
				$proj_id = $proj_info['id'];
		}
		
		if( $proj_id == 0 )
			$this->go404();
		
		if( $proj_info['author_id'] != $uid )
			$this->go404();
		
		
		$req_id = 0;

		if( count($this->urlparts)>1 ) {
			$req_url = $this->urlparts[1];
			$req_info = $this->catLib->Item_ReqInfo($req_url);

			if( ($req_info != null) && ($proj_id == $req_info['item_id']) )
				$req_id = $req_info['id'];
		}
		
		if( $req_id == 0 )
			$this->goToUrl($this->page_View->Page_BuildUrl("cabinet", "needhelp"));
		
		if( $req_info['req_status'] == REQ_STATUS_NEW ) {

			if( $todo == "accept" )
				$this->pageModel->set_ItemReqStatus($req_id, REQ_STATUS_CONFIRM);

			else if( $todo == "decline" )
				$this->pageModel->set_ItemReqStatus($req_id, REQ_STATUS_DECLINE);
		}
		
		$this->_prepare_reqlist($uid, $proj_info);
		
		$this->pageView->render_proj_reqlist();
	}	
	
	///////////////////////////////////////////////////////////////////////////////////////
	// Editing and saving user profile

	private function _getEdtVars() {
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['rname'] = $this->getReqParam("rname", "");
		$reg['rfname'] = $this->getReqParam("rfname", "");
		$reg['rlogin'] = $this->getReqParam("rlogin", "");
		$reg['rpasswd1'] = $this->getReqParam("rpasswd1", "");
		$reg['rpasswd2'] = $this->getReqParam("rpasswd2", "");
		$reg['rtel'] = $this->getReqParam("rtel", "");
		$reg['rprof'] = $this->getReqParam("rprof", "");
		$reg['robl'] = $this->getReqParam("robl", 1);
		$reg['rcity'] = $this->getReqParam("rcity", 0);
		$reg['rrule'] = $this->getReqParam("rrule", 0);
		$reg['rdescr'] = $this->getReqParam("rdescr", "");
	
		$reg['rusepic'] = $this->getReqParam("rusepic", 0);
		$reg['pic'] = "";
		$reg['backpicid'] = $this->getReqParam("backpicid", 0);
	
		$reg['rorg'] = $this->getReqParam("rorg", "");
		$reg['rorgsphere'] = $this->getReqParam("rorgsphere", "");
		$reg['remplnum'] = $this->getReqParam("remplnum", 5);
	
		return $reg;
	}
	
	private function _initEdtVars() {
		$reg = Array();
		$reg['action'] = "";
		$reg['rname'] = "";
		$reg['rfname'] = "";
		$reg['rlogin'] = "";
		$reg['rpasswd1'] = "";
		$reg['rpasswd2'] = "";
		$reg['rtel'] = "";
		$reg['rprof'] = "";
		$reg['rcountry'] = 1;
		$reg['robl'] = 0;
		$reg['rcity'] = 0;
		$reg['rrule'] = 0;
		$reg['rdescr'] = "";
	
		$reg['rusepic'] = 0;
		$reg['pic'] = "";
		$reg['backpicid'] = 0;
	
		$reg['rorg'] = "";
		$reg['rorgsphere'] = "";
		$reg['remplnum'] = 5;
	
		return $reg;
	}
	
	private function _fillEdtVars($req, $uinf) {
		$reg['rname'] = $uinf['name'];
		$reg['rfname'] = $uinf['fname'];
		$reg['rlogin'] = $uinf['email'];
		$reg['rtel'] = $uinf['phone'];
		$reg['rprof'] = $uinf['profession'];
		$reg['rcountry'] = $uinf['country_id'];
		$reg['robl'] = $uinf['obl_id'];
		$reg['rcity'] = $uinf['city_id'];
		$reg['rdescr'] = $uinf['comments'];
		
		$reg['rusepic'] = 0;
		$reg['pic'] = $uinf['pic'];
		$reg['backpicid'] = $uinf['background'];
		
		$reg['rorg'] = $uinf['orgname'];
		$reg['rorgsphere'] = $uinf['orgsphere'];
		$reg['remplnum'] = $uinf['employee_num'];
		
		return $reg;
	}
	
	// Перейти к редактированию своего профиля

	public function action_editme() {
		$uid = $this->authAllow();
		
		$req = $this->_initEdtVars();
		
		$uinfo = $this->catLib->Buyer_Info($uid);
		$uinfo['country_id'] = $this->catLib->Loc_GetCountryIdByCity($uinfo['city_id']);
		
		
		$req = $this->_fillEdtVars($req, $uinfo);
		$formreq = new UhCmsFormData($req);
		
		$frmtype = "";

		switch($uinfo['account_type']) {
			case USR_TYPE_PERS:	$frmtype = "person";	break;	
			case USR_TYPE_COMP:	$frmtype = "company";	break;
			case USR_TYPE_ORG:	$frmtype = "organization";	break;
		}		
				
		//$this->pageView->catmodel = $catLib;
		$this->pageView->UserId = $uid;
		$this->pageView->render_edituserfrm($frmtype, $formreq);
	}
	
	// Сохранить свой профиль после редактирования

	public function action_saveme() {
		$uid = $this->authAllow();
		
		$req = $this->_getEdtVars();
		
		//$catLib = new Catalog($this->db, $this->LangId);
		$uinfo = $this->catLib->Buyer_Info($uid);
				
		$btnsetphoto = $this->getReqParam("btnsetphoto", "");

		if( $btnsetphoto != "" ) {

			if( isset($_FILES['rfile']) && ($_FILES['rfile']['tmp_name'] != "") ) {
				// Set new avatar for user
				$this->pageModel->set_UserAvatar($uinfo, $_FILES['rfile']['name'], $_FILES['rfile']['tmp_name']);				
			}

		} else {
			
			$uinfo = $this->catLib->Buyer_Info($uid);
			$req['pic'] = $uinfo['pic'];
			
			/// Check input fields for correct values
			$formreq = new UhCmsFormData($req);
			
			// Make check for all fields

			if( ($uinfo['account_type'] == USR_TYPE_COMP) || ($uinfo['account_type'] == USR_TYPE_ORG) ) {

				if( trim($formreq->rorg) == "" ) {
					$formreq->setok("rorg", false);
					$formreq->setmsg("rorg", "Вы не указали название организации");
				}
				
				if( trim($formreq->rtel) == "" ) {
				
				}
				else if( preg_match(PHONE_REGEXP, trim($formreq->rtel)) == 0 ) {
					$formreq->setok("rtel", false);
					$formreq->setmsg("rtel", "Телефон указан с ошибками");
				}
			}
			if( trim($formreq->rname) == "" ) {
				$formreq->setok("rname", false);
				$formreq->setmsg("rname", "Вы не указали свое имя");
			}
			if( trim($formreq->rfname) == "" ) {
				$formreq->setok("rfname", false);
				$formreq->setmsg("rfname", "Вы не указали свою фамилию");
			}
			
			if( trim($formreq->rlogin) == "" ) {
				$formreq->setok("rlogin", false);
				$formreq->setmsg("rlogin", "Вы не указали свой E-mail/Логин");
			}

			else if( 
				(preg_match("/^[a-zA-Z0-9\._-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/", trim($formreq->rlogin)) == 0) && 
				(preg_match(PHONE_REGEXP, trim($formreq->rlogin)) == 0) 
			) {
				$formreq->setok("rlogin", false);
				$formreq->setmsg("rlogin", "E-mail/Логин указан с ошибками");
			}
			else if( $this->pageModel->checkLoginExists(trim($formreq->rlogin), $uid) ) {
				$formreq->setok("rlogin", false);
				$formreq->setmsg("rlogin", "Такой E-mail/Логин уже зарегистрирован. Укажите другой адрес электронной почты.");
			}		
			
			if( $formreq->rcity == 0 ) {
				$formreq->setok("rcity", false);
				$formreq->setmsg("rcity", "Укажите ваш основной город");
			}
			
			if( $formreq->has_errors() )
			{$this->pageView->UserId = $uid;
				$this->pageView->msg = "Не все обязательные поля заполнены.";
				
				// Render edit profile page
				$frmtype = "";
				switch($uinfo['account_type']) {
					case USR_TYPE_PERS:	$frmtype = "person";	break;	
					case USR_TYPE_COMP:	$frmtype = "company";	break;
					case USR_TYPE_ORG:	$frmtype = "organization";	break;
				}		
						
				//$this->pageView->catmodel = $catLib;
				$this->pageView->UserId = $uid;
				$this->pageView->render_edituserfrm($frmtype, $formreq);
				return;
			}
					
			
			switch($uinfo['account_type']) {
				case USR_TYPE_PERS:	$this->pageModel->save_UserInfo($uid, $req);	break;
				case USR_TYPE_COMP:	$this->pageModel->save_CompInfo($uid, $req);	break;
				case USR_TYPE_ORG:	$this->pageModel->save_OrgInfo($uid, $req);	break;
			}
		}
		
		
		//$this->pageView->UserId = $uid;
		//$this->pageView->msg = "Изменения были сохранены.";

		// Render edit profile page
		//$this->action_editme();
		
		$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));		
	}
	
	public function action_edittopic() {
		$uid = $this->authAllow();
		
		$uinfo = $this->catLib->Buyer_Info($uid);
		
		$frmtype = "";

		switch($uinfo['account_type']) {
			case USR_TYPE_PERS:	$frmtype = "person";	break;
			case USR_TYPE_COMP:	$frmtype = "company";	break;
			case USR_TYPE_ORG:	$frmtype = "organization";	break;
		}
		
		$rtopics_list = $this->catLib->Buyer_Sects($uid);
		$rtopics = Array();

		for( $i=0; $i<count($rtopics_list); ++$i )
			$rtopics[] = $rtopics_list[$i]['sect_id'];
		
		$this->pageView->UserId = $uid;
		$this->pageView->buyertopics = $rtopics;
		$this->pageView->render_edittopicfrm($frmtype);
	}
	
	public function action_savetopic() {
		$uid = $this->authAllow();
		
		$rtopics = $this->getReqParam("rtopics", null);
		
		$this->pageModel->save_UserTopics($uid, $rtopics);
		
		$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", ""));
		
		//$this->pageView->UserId = $uid;
		//$this->pageView->buyertopics = $rtopics;
		//$this->pageView->render_edittopicfrm("");
	}
	
	public function action_postrev() {
		$uid = $this->authAllow();
		
		// Extract id from url like this (cabinet/postrev/PROJID/REQID)
		$proj_id = 0;		
		if( count($this->urlparts)>0 ) {
			$proj_url = $this->urlparts[0];
		
			// Check if this project exists
			$proj_info = $this->catLib->Item_Info($proj_url);
			if( $proj_info != null )
				$proj_id = $proj_info['id'];
		}
		
		if( $proj_id == 0 )
			$this->goToUrl($this->Page_BuildUrl("cabinet", "needhelp"));
		
		if( $proj_info['profile_id'] == PROJ_SENDHELP ) {
			// skip redirect
		}
		else if( $proj_info['author_id'] != $uid ) {
			$this->goToUrl($this->Page_BuildUrl("cabinet", "needhelp"));
			//$this->go404();
		}
		
		
		$req_id = 0;

		if( count($this->urlparts) 	> 1 ) {
			$req_url = $this->urlparts[1];
			$req_info = $this->catLib->Item_ReqInfo($req_url);

			if( ($req_info != null) && ($proj_id == $req_info['item_id']) ) {
				$req_id = $req_info['id'];

				//echo $uid.'<br>';
				//echo $req_info['sender_id'].'<br>'; die();
				
				if( $req_info['sender_id'] == $uid ) {
					$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", "mygethelp"));
					//$this->go404();
				}
			}
		}
		
		if( $req_id == 0 )
			$this->goToUrl($this->page_View->Page_BuildUrl("cabinet", "reqlist/".$proj_id));
		
		// Now we have $proj_id and $req_id from URL string, get other params
		$starrate = $this->getReqParam("starrate", 0);
		$startxt = $this->getReqParam("startxt", "");	
		
		if( ($starrate == 0) || (trim($startxt) == "") )
			$this->goToUrl($this->pageView->Page_BuildUrl("cabinet", "reqlist/".$proj_id));
		
		// Add review and user rate to database
		if( $this->pageModel->save_UserReview($uid, $req_info['sender_id'], $proj_id, $req_id, $starrate, $startxt) )
			$this->pageView->msg = "Ваша оценка с отзывом была опубликована.";
		else
			$this->pageView->msg = "Произошла ошибка при публикации вашей оценки с отзывом.";
		
		// Show reqest list page
		$this->_prepare_reqlist($uid, $proj_info);
				
		$this->pageView->render_proj_reqlist();
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Messages
	
	// подготовить список сообщений для отображения

	protected function _prepare_msglist($toid, $pi=0) {

		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );
				
		$this->pageView->UserId = $toid;
		$this->pageView->msglist_totgrpnum = $this->pageModel->msg_ListByProjNum($toid);

		$this->pageView->msglist = $this->pageModel->msg_ListByProj($toid, 0, MSG_ITEMS_PERPAGE );
		//$this->pageView->msglist = $this->pageModel->msg_InList($toid);        //$catLib->Msg_List(0, $toid);

		//echo '<pre>';
		//print_r($this->pageView->msglist); die();

		$this->pageView->pi = $pi;
		$this->pageView->pn = MSG_ITEMS_PERPAGE;
		
		$this->_prepare_tabinfo($toid);
	}
	
	// открыть диалог по указанному проекту

	public function action_msgview() {

		$uid = $this->authAllow();

		$proj_id = 0;
		$user_id = 0;

		$pi = 0;

		$uinfo = $this->catLib->Buyer_Info($uid);

		$city = $this->catLib->Loc_City($uinfo['city_id']);
		$this->pageView->box = $this->pageModel->getUserBox($uinfo['id']);

		if( $city != null )
			$uinfo['city'] = $city['name'];

		$uinfo['group_name'] = $this->catLib->Buyer_GroupInfo($uinfo['account_type']);
		$uinfo['helprate'] = $this->catLib->Buyer_ReqStarsRate($uid);

		$uinfo['loc_info'] = $this->catLib->Loc_InfoByCity($uinfo['city_id']);

		$this->pageView->uinfo = $uinfo;

		for($i=0; $i<count($this->urlparts); ++$i) {

			$tmp_url = $this->urlparts[$i];
			$tmp_urls = explode("_", $tmp_url);

			//if( (count($tmp_urls) == 2) && ($tmp_urls[0] == "proj") )
			//	$proj_id = $tmp_urls[1];

			if( (count($tmp_urls) == 2) && ($tmp_urls[0] == "user") )
				$user_id = $tmp_urls[1];

			else if( (count($tmp_urls) == 2) && ($tmp_urls[0] == "p") )
				$pi = $tmp_urls[1];
		}

		if( is_numeric($pi) )
			$pi = (int)$pi;

		$this->_prepare_msglist($uid, $pi);

		//echo $user_id."<br>"; die();

		$this->pageView->openProjId = $proj_id;
		$this->pageView->openUserId = $user_id;
		$this->pageView->render_messages();
		$this->pageView->closeTip = true;

		//echo $user_id."<br>";

		//if( $proj_id != 0 ){
		//	$this->pageModel->msg_SetRead($uid, $proj_id);
		//}

		if( $user_id != 0 ) {
			$this->pageModel->msg_SetReadUsrbrd($uid, $user_id);
		}
	}
	
	// Отправить сообщение пользователю

	public function action_sendmsg() {
		$UserId = $this->authAllow(); 
		
		// Now we have $proj_id and $req_id from URL string, get other params

		$selprojid = $this->getReqParam("selprojid", 0);
		$seluid = $this->getReqParam("seluid", 0);
		$selmsgid = $this->getReqParam("selmsgid", 0);
		$msgtxt = $this->getReqParam("msgtxt", "");
				
		$uinf = $this->catLib->Buyer_Info($seluid);

		$uinfo = $this->catLib->Buyer_Info($UserId);

		$city = $this->catLib->Loc_City($uinfo['city_id']);
		$this->pageView->box = $this->pageModel->getUserBox($uinfo['id']);

		if( $city != null )
			$uinfo['city'] = $city['name'];

		$uinfo['group_name'] = $this->catLib->Buyer_GroupInfo($uinfo['account_type']);
		$uinfo['helprate'] = $this->catLib->Buyer_ReqStarsRate($UserId);

		$uinfo['loc_info'] = $this->catLib->Loc_InfoByCity($uinfo['city_id']);

		$this->pageView->uinfo = $uinfo;
		if( empty($uinf['id']) || ($UserId == 0) ) {
			$this->_prepare_msglist($UserId);			
			$this->pageView->msg = "Ошибка отправки сообщения. Не найден адресат.";
			$this->pageView->render_messages();
			return;
		}
		
		if( $uinf['id'] == $UserId ) {
			$this->_prepare_msglist($UserId);
			$this->pageView->msg = "Ошибка отправки сообщения. Вы не можете отправлять сообщения сами себе.";
			$this->pageView->render_messages();
			return;
		}
		
		$proj_id = 0;

		if( $selprojid != 0 ) {
			$projinfo = $this->catLib->Item_Info($selprojid);
		
			if( ($projinfo != null) && isset($projinfo['id']) )
				$proj_id = $projinfo['id'];
		}
		
		// Post message to DB
		$this->pageModel->msg_Send($UserId, $uinf['id'], $msgtxt, $proj_id, $selmsgid);
		
		$this->_prepare_msglist($UserId);		
		
		$this->pageView->render_messages();
	}
	
	///////////////////////////////////////////////////////////////////////
	// Bonus wins
	
	// View list of won bonuses
	public function action_mybonus() {
		$UserId = $this->authAllow();
		
		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );
		
		$this->pageView->UserId = $UserId;
		$this->pageView->its = $this->pageModel->bonus_ListBonus($UserId);

		for( $i=0; $i<count($this->pageView->its); $i++ ) {
			$this->pageView->its[$i]['pics'] = $this->catLib->Bonus_Pics($this->pageView->its[$i]['id'], 1);
		}
		
		$this->_prepare_tabinfo($UserId);
		
		$this->pageView->render_mybonuslist();
	}

	public function action_sendhlp() {
		$UserId = $this->authAllow();

		$sum = $this->getReqParam('sum', 0);
		$pid = $this->getReqParam('pid', 0);

		if($UserId != 0 && $sum != 0 && $pid != 0) {
			$this->pageModel->updUserHelp($UserId, $pid, $sum);
		}

		$this->goToUrl($this->pageView->page_BuildUrl("cabinet", "dohelp"));
	}

	public function action_promaccept() {
		$pid = $this->getReqParam('pid', 0);
		$uid = $this->getReqParam('uid', 0);

		if($pid && $uid) {
			$this->pageModel->acceptPromPay($uid, $pid);
		}

		$this->goToUrl($this->pageView->page_BuildUrl("cabinet", "needhelp"));
	}
	public function action_globus(){
		$UserId = $this->authAllow();

		$this->pageView->addBreadcrumbs( $this->pageView->page_BuildUrl("cabinet"), "Мой профиль" );

		$this->pageView->UserId = $UserId;
		$this->pageView->its = $this->pageModel->bonus_ListBonus($UserId);
		for( $i=0; $i<count($this->pageView->its); $i++ )
		{
			$this->pageView->its[$i]['pics'] = $this->catLib->Bonus_Pics($this->pageView->its[$i]['id'], 1);
		}

		$this->_prepare_tabinfo($UserId);

		$this->pageView->render_globus();
	}
	
}