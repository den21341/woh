<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Payscallback extends Controller
{	
	protected $model;
	protected $view;
	protected $LangId;
	protected $ikobj;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		$this->LangId = UhCmsApp::getLangId();
						
		/////////////////////////////////////////////////////////////////////////
		// Load page model source code
		$fileModel = $config['MVC_PATH']."models/payscallbackModel.class.php";

		include_once($config['MVC_PATH'].'basic/PapApi.class.php');
		//include_once('analytics/analytics.php');

		if( !file_exists($fileModel) ) {
			die('404 Not Found - Controller');
		}
		
		include_once $fileModel;
		
		// Load page view class source code
		$fileView = $config['MVC_PATH']."views/payscallbackView.class.php";

		if( !file_exists($fileView) ) {
			die('404 Not Found - View');
		}

		include_once $fileView;

		//////////////////////////////////////////////////////////////////////////
		// Create Model and View
		$this->model = new PayscallbackModel($this->getCfg(), $this->db, $this->LangId);
		$this->view = new PayscallbackView($this->getCfg(), $this->model);

		$this->ikobj = new Interkassa($this->getCfg());
	}
	
	public function action_default()
	{
		// Do nothing
		//$this->view->render_default();		
	}
	
	private function ik_dumpAllVars() {

		$its = Array();
		
		foreach($_POST as $pk => $pv) {
			$its[$pk] = $pv;		
		}
		
		return $its;
	}
	
	private function ik_interactReqVars()
	{
		/*
		Пример ответа
		
		{
			"co_id":"561555503d1eafe0518b4567",
			"pm_no":"",
			"desc":"ger",
			"pw_via":"test_interkassa_test_xts",
			"am":0,
			"cur":"RUB",
			"act":"",
			"inv_id":"43800214",
			"co_prs_id":"400246572968",
			"trn_id":"",
			"inv_crt":"2015-12-25 13:39:06",
			"inv_prc":"2015-12-25 13:39:06",
			"inv_st":"success",
			"ps_price":"4692.68",
			"co_rfn":"4556",
			"sign":"tTfUZjOsjPQcNR6t3Qlt4g=="
		}
		*/
		
		$its = Array();
		$its['co_id'] = $this->getReqParam("ik_co_id", "");		// Checkout ID, 	4f498747a87344837446
		$its['pm_no'] = $this->getReqParam("ik_pm_no", "");		// Payment No.,		12635, ID_3837
		$its['desc'] = $this->getReqParam("ik_desc", "");		// Description
		$its['pw_via'] = $this->getReqParam("ik_pw_via", "");	// Payway via
		$its['am'] = $this->getReqParam("ik_am", 0);			// Amount			34, 12.34
		$its['cur'] = $this->getReqParam("ik_cur", "");			// Currency			USD, RUB, UAH
		$its['act'] = $this->getReqParam("ik_act", "");		// Action
		
		// ik_x_[name]		X Prefix		ik_x_field1=somedata1, ik_x_baggage1=2334
		
		$its['inv_id'] = $this->getReqParam("ik_inv_id", "");		// Invoice ID
		$its['co_prs_id'] = $this->getReqParam("ik_co_prs_id", "");	// Checkout Purse ID - номер кошелька кассы
		$its['trn_id'] = $this->getReqParam("ik_trn_id", "");		// Transaction ID
		$its['inv_crt'] = $this->getReqParam("ik_inv_crt", "");		// Invoice created
		$its['inv_prc'] = $this->getReqParam("ik_inv_prc", "");		// Invoice processed
		$its['inv_st'] = $this->getReqParam("ik_inv_st", "");		// Invoice state			success, fail ...
		$its['ps_price'] = $this->getReqParam("ik_ps_price", 0);	// Paysystem price			4.56
		$its['co_rfn'] = $this->getReqParam("ik_co_rfn", "");		// Checkout refund			24.00
		$its['sign'] = $this->getReqParam("ik_sign", "");
		$its['prom_val'] = $this->getReqParam('prom_val', '');
		$its['prom_curr'] = $this->getReqParam('prom_curr', '');
		$its['papid'] = $this->getReqParam('papid', '');
		$its['anonuser'] = $this->getReqParam('anonuser', '');
		$its['kadid'] = $this->getReqParam('kadid', '');
		$its['gclid'] = $this->getReqParam('gclid', '');
		$its['ut_s'] = $this->getReqParam('ut_s', '');
		$its['ut_c'] = $this->getReqParam('ut_c', '');
		$its['s_ga'] = $this->getReqParam('s_ga', '');
		$its['aid'] = $this->getReqParam('aid', '');
		$its['ga'] = $this->getReqParam('aid', '');

		//$its[''] = $this->getReqParam("ik_", "");
		
		return $its;
	}
	
	public function action_iktest() {
		$test_data = '{"ik_prom_val":"26","ik_co_id":"561555503d1eafe0518b4567","ik_co_prs_id":"12211120","ik_inv_id":"'.rand(0,132322).'","ik_inv_st":"success","ik_inv_crt":"2015-12-25 15:00:07","ik_inv_prc":"2015-12-25 15:00:07","ik_trn_id":"","ik_pm_no":"ID_994_1100","ik_pw_via":"test_interkassa_test_xts","ik_am":"6","ik_co_rfn":"100","ik_ps_price":"103","ik_cur":"UAH","ik_desc":"\u0411\u043b\u0430\u0433\u043e\u0442\u0432\u043e\u0440\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u0432\u0437\u043d\u043e\u0441","ik_sign":"9sIGKyqn2nBDAmwZNYRJsw==", "ik_gclid":"fewefwefwef", "ik_ga":"GA1.1.77409772.1479461764", "ik_utm_s":"kadam", "ik_utm_c":"266973", "ik_aid":"", "ik_s_ga":""}';

		$dataarr = json_decode($test_data, true);
		
		var_dump($dataarr);
		echo "<br>";
		echo $dataarr["ik_co_id"];
		
		$datanew = Array();
		foreach($dataarr as $k => $v)
			$datanew[substr($k,3)] = $v;
		
		echo "<br>";
		var_dump($datanew);
		echo "<br>";
		
		$catLib = new Catalog($this->db, $this->LangId);		
		$callback = $this->model->add_PayOperationIk($datanew, $catLib);

		$this->view->subid = '';
		$this->view->perc = 0;

		if($callback != 0) {
			$this->view->subid = $callback[1];
			$this->view->perc = $callback[2];
		}
		$this->view->render_default();
	}
		
	public function action_iksuccess()
	{
		$dumparr = $this->ik_dumpAllVars();
		$jsonstr = json_encode($dumparr);
		
		$this->model->dump_PayOperation(1, 1, "success - ".$jsonstr, (isset($dumparr['ik_inv_id']) ? $dumparr['ik_inv_id'] : 0 ));
	}
	
	public function action_ikfail()
	{
		$dumparr = $this->ik_dumpAllVars();
		$jsonstr = json_encode($dumparr);
		
		$this->model->dump_PayOperation(1, 2, "fail - ".$jsonstr, (isset($dumparr['ik_inv_id']) ? $dumparr['ik_inv_id'] : 0 ));
	}
	
	public function action_ikpending()
	{
		$dumparr = $this->ik_dumpAllVars();
		print_r($dumparr);
		$jsonstr = json_encode($dumparr);
		
		$this->model->dump_PayOperation(1, 3, "pending - ".$jsonstr, (isset($dumparr['ik_inv_id']) ? $dumparr['ik_inv_id'] : 0 ));
	}
	
	public function action_ikinteract()
	{
		$dumparr = $this->ik_dumpAllVars();
		$jsonstr = json_encode($dumparr);
		
		$this->model->dump_PayOperation(1, 4, $jsonstr, (isset($dumparr['ik_inv_id']) ? $dumparr['ik_inv_id'] : 0 ));
		
		$reqd = $this->ik_interactReqVars();
		
		$signreal = $this->ikobj->makeSign($dumparr);
		
		if( $signreal != $reqd['sign'] )
		{
			$this->model->dump_PayOperation(1, 4, "Ошибочная подпись: ".$jsonstr, (isset($dumparr['ik_inv_id']) ? $dumparr['ik_inv_id'] : 0 ) );
		}
		else
		{
			$this->model->dump_PayOperation(1, 4, "Подпись ок: ".$jsonstr, (isset($dumparr['ik_inv_id']) ? $dumparr['ik_inv_id'] : 0 ) );
			
			
			$catLib = new Catalog($this->db, $this->LangId);
			
			// 
			if( $reqd['inv_st'] == "success" )
			{
				// Add payment to database
			}

			$callback = $this->model->add_PayOperationIk($reqd, $catLib);

			$this->view->subid = '';
			$this->view->perc = 0;

			if($callback != 0) {
				$this->view->subid = $callback[1];
				$this->view->perc = $callback[2];
			}

			$this->view->render_default();
		}
		
		//$jsonstr = json_encode($data);
		//$this->model->dump_PayOperation(1, 4, $jsonstr);	
	}
	
	public function action_ikmanualop()
	{
		$sender_id = $this->getReqParam("sid", 0);
		$proj_id = $this->getReqParam("pid", 0);
		$recv_id = $this->getReqParam("rid", 0);
		$amount = $this->getReqParam("amount", 0);
	
		if( ($sender_id != 0) && ($proj_id != 0) && ($recv_id != 0) && ($amount != 0) )
		{
			$this->model->manual_proc_op($recv_id, $proj_id, $sender_id, $amount, true);
		}
	}
		
	
	// Calculate total project rating
	//public function action_calcrating()
	//{
	//	$catLib = new Catalog($this->db, $this->LangId);
	//	
	//	$this->model->Item_PriorityRatingUpdate($catLib);
	//}
}