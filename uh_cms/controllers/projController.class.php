<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("ADD_MODE_NORMAL", "");
define("ADD_MODE_DREAM", "dream");
define("ADD_MODE_THING", "thing");
define("ADD_MODE_EVENT", "event");
define("ADD_MODE_MYHELP", "help");

class Proj extends PageController
{		
	protected $addmode;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		$this->addmode = "";
	
		//$this->self_contolled = true;
	
		//$this->catLib = new Catalog($this->db, $this->LangId);
	}
	
	// Check if this page is allowed to unauthorized user
	// If not, then go to website start page
	private function authAllow()
	{
		$uid = UhCmsApp::getSesInstance()->UserId;
		if( $uid == 0 )
		{
			header("Location: ".$this->pageView->page_BuildUrl("registration","person"));
			exit();
		}
		
		return $uid;
	}
	
	public function action_view() {
		$proj_id = 0;
		$pix = $this->getReqParam('px', 0);
		$pix = ($pix == 1 ? 1 : 0);

		if( count($this->urlparts)>0 )
		{
			$proj_url = $this->urlparts[0];

			// Check if this project exists

			$projinfo = $this->catLib->Item_Info($proj_url);

			if( $projinfo != null )
				$proj_id = $projinfo['id'];
		}

		if( $proj_id == 0 )
			$this->go404();

		$this->catLib->Item_UpdateRate($proj_id);

		// Check Social link
		$scpar = $this->getReqParam("jhscvisitor", 0);
		$screfpar = $this->getReqParam("jhscreferer", "");

		if( $scpar == 1 )
		{
			// Check who is referer
			$ureferer_id = $this->pageModel->getUserIdByHash($screfpar);

			if( $ureferer_id != 0 && ($_SERVER['REMOTE_ADDR'] != "") )
				$this->catLib->Item_UpdateScRate($proj_id, $ureferer_id, $_SERVER['REMOTE_ADDR']);
		}

		// Find path to section and add it to breadcrumbs
		$spath = $this->catLib->Catalog_SectPath($projinfo['sectid']);

		$projinfo['city_data'] = $this->catLib->Loc_City($projinfo['city_id']);
		$projinfo['photos'] = $this->catLib->Item_Pics($projinfo['id']);
		$projinfo['currency'] = $this->catLib->Currency_Info($projinfo['currency_id']);

		$user_info = $this->catLib->Buyer_Info($projinfo['author_id']);
		$user_info['group_name'] = $this->catLib->Buyer_GroupInfo($user_info['account_type']);
		$user_info['loc_info'] = $this->catLib->Loc_InfoByCity($user_info['city_id']);
		$user_info['helprate'] = $this->catLib->Buyer_ReqStarsRate($user_info['id']);
		$user_info['helpnum'] =  $this->catLib->Buyer_ReqList($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM, '',  -1, -1, PROJ_STATUS_ALL, 0, PROJ_STATUS_ALL, 1)[0]['count'];
		$user_info['helpmoney'] =  $this->catLib->Buyer_ReqAmountTotal($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL, PROJ_TYPE_MONEY);
		$user_info['getmoney'] =  $this->catLib->Buyer_ReqAmountTotal($user_info['id'], PROJ_NEEDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL, PROJ_TYPE_MONEY);


		// Fill data to pageView object
		$this->pageView->wohPays = $this->catLib->checkWohPays($proj_id);
		$this->pageView->spath = $spath;
		$this->pageView->sid = $projinfo['sectid'];
		$this->pageView->projinfo = $projinfo;
		$this->pageView->reqlist = $this->catLib->Item_ReqList($projinfo['id'], REQ_STATUS_CONFIRM, 0, 3, 1);
		$this->pageView->reqtotal = $this->catLib->Item_ReqCollected($projinfo['id'], REQ_STATUS_CONFIRM);
		$this->pageView->needhelpsects = $this->catLib->Buyer_ItemSects($projinfo['author_id'], PROJ_NEEDHELP);
		$this->pageView->comlist = $this->catLib->Item_Comments($projinfo['id'], true, 0, 5);
		$this->pageView->comtotal = $this->catLib->Item_CommentsNum($projinfo['id']);
		$this->pageView->UserHash = UhCmsApp::getSesInstance()->UserHash;
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->user_info = $user_info;
		$this->pageView->box = $this->catLib->getUserBox($projinfo['author_id']);
		$this->pageView->subProj = $this->catLib->getViewedProject($proj_id);
		$this->pageView->wohNextStep = 0;
		$this->pageView->pix = $pix;

		$this->pageView->usernow = $this->catLib->Buyer_Info(UhCmsApp::getSesInstance()->UserId);

		$this->pageView->setPopUp = $this->catLib->getBuyerReqNum($projinfo['author_id']);

		if($this->pageView->setPopUp[0]['hlpco'] >= 20)
			$this->pageView->setPopUp = 1;
		else
			$this->pageView->setPopUp = 0;

		$this->pageView->popUpType = $this->catLib->getPagesPopUp('proj/view/'); // IF USER HAVE MORE THAN 20 HELPREQ THEN SHOW FIRST POPUP
		if(!empty($this->pageView->popUpType)) {
			$this->pageView->popUpType = $this->pageView->popUpType[0]['popup_cur'];

			if(!$this->pageView->setPopUp && $this->pageView->popUpType == 1) {
				$this->pageView->popUpType = 2;
			}
		}
		else
			$this->pageView->popUpType = -1;

		// Оххх очередной йоба-код.... шо поделать, так и живем)

		if($this->pageView->wohPays[0]['req_amount']) {
			//if($this->pageView->wohPays[0]['daysbefore'] <= $this->pageView->reqlist[2]['daysbefore']) {
			$this->pageView->wohPays[0]['author_id'] = $this->pageView->wohPays[0]['sender_id'];
			//$this->pageView->reqlist[] = $this->pageView->wohPays[0];
			array_unshift($this->pageView->reqlist, $this->pageView->wohPays[0]);

			/*function cmp($a, $b) {
                return $a['daysbefore'] - $b['daysbefore'];
            }

            usort($this->pageView->reqlist,"cmp");*/
			/*} else
				$this->pageView->wohNextStep = 1;*/
		}

		// Init comment form values
		$reqd = $this->_initComVars($projinfo['id']);
		$formreq = new UhCmsFormData($reqd);

		//$this->pageView->catmodel = $this->catLib;

		// Render page
		$this->pageView->render_default($formreq);
	}
	
	public function action_default()
	{
		//$this->catLib = new Catalog($this->db, 1);
		//$uid = $this->authAllow();
		$uid = UhCmsApp::getSesInstance()->UserId;	
		
		$uinfo = $this->catLib->Buyer_Info($uid);
		$uinfo['group_name'] = $this->catLib->Buyer_GroupInfo($uinfo['account_type']);		
		$this->pageView->userinfo = $uinfo;
		//$this->pageView->catmodel = $this->catLib;
		$this->pageView->curl = '';
		$this->pageView->render_default(null);
	}
	
	private function _getComVars()
	{
		$reg = Array();
		$reg['projid'] = $this->getReqParam("projid", 0);
		$req['action'] = $this->getReqParam("action", "");		
		$reg['comment'] = $this->getReqParam("comment", "");
		
		return $reg;
	}
	
	private function _initComVars($projid)
	{
		$reg = Array();
		$reg['projid'] = $projid;
		$req['action'] = "";
		$reg['comment'] = "";
	
		return $reg;
	}
	
	public function action_addcomment()
	{
		$uid = $this->authAllow();
		
		$reqd = $this->_getComVars();
		
		$proj_id = 0;		
		$proj_url = $reqd['projid'];
				
		// Check if this project exists
		$projinfo = $this->catLib->Item_Info($proj_url);
		if( $projinfo != null )
			$proj_id = $projinfo['id'];
		
		if( $proj_id == 0 )
			$this->go404();
		
		$this->pageModel->add_ProjComment($uid, $reqd);
		//$this->pageModel->sendUserMailMsg($projinfo['id']);
		$this->pageView->commPopTip = $this->catLib->setCommentMoney($uid, $projinfo['id'], 1);
		//header('Location:'.WWWHOST.'proj/view/'.$proj_id);
	}
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['tit'] = $this->getReqParam("tit", "");
		$reg['descr'] = $this->getReqParam("descr", "");
		$reg['descrfull'] = $this->getReqParam("descrfull", "");
		$reg['stdt'] = $this->getReqParam("stdt", date("d.m.Y", time()));
		$reg['withendt'] = $this->getReqParam("withendt", 1);
		$reg['endt'] = $this->getReqParam("endt", date("d.m.Y", time()+30*24*3600));
		$reg['ptype'] = $this->getReqParam("ptype", 0);
		$reg['sect'] = $this->getReqParam("sect", 0);
		$reg['pamount'] = $this->getReqParam("pamount", 0);
		$reg['rcountry'] = $this->getReqParam("rcountry", 1);
		$reg['robl'] = $this->getReqParam("robl", 0);
		$reg['rcity'] = $this->getReqParam("rcity", 0);
		$reg['eventtype'] = $this->getReqParam("eventtype", 0);
		$reg['thingtype'] = $this->getReqParam("thingtype", 0);
		$reg['dreamtype'] = $this->getReqParam("dreamtype", 0);
		$reg['helptype'] = $this->getReqParam("helptype", 0);
		$reg['pamountizm'] = $this->getReqParam("pamountizm", 1);
		$reg['arbitr'] = $this->getReqParam("arbitr", 0);
		
		$reg['dubguid'] = $this->getReqParam("dubguid","--");
	
		return $reg;
	}
	
	private function _initRegvars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['tit'] = "";
		$reg['descr'] = "";
		$reg['descrfull'] = "";
		$reg['stdt'] = date("d.m.Y", time());
		$reg['withendt'] = 1;
		$reg['endt'] = date("d.m.Y", time()+(6*30+1)*24*3600);
		$reg['ptype'] = 0;
		$reg['sect'] = 0;
		$reg['pamount'] = "0";
		$reg['rcountry'] = 1;
		$reg['robl'] = 0;
		$reg['rcity'] = 0;
		$reg['eventtype'] = 0;
		$reg['thingtype'] = 0;
		$reg['dreamtype'] = 0;
		$reg['helptype'] = 0;
		$reg['pamountizm'] = 1;
		$reg['dubguid'] = "--";
	
		return $reg;
	}
	
	public function action_add()
	{
		$uid = $this->authAllow();
		
		$uinf = $this->catLib->Buyer_Info($uid);
		
		// Check if the number of needhelp Projects is less then amount of dohelp requests
		// Check this only for persons and companies
		// Organizations are allowed to post many projects
		if( $this->addmode == ADD_MODE_THING ) {
			// then no limits to add new thing for gifts
		}
		else if( $this->addmode == ADD_MODE_MYHELP ) {
			// then no limits to add new 'give help' advertising
		}
		else if( $uinf['account_type'] != USR_TYPE_ORG ) {
			$u_needhelp_num = $this->catLib->Item_Num(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, $uid);
			$u_dohelp_num = $this->catLib->Buyer_ReqNum($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM);
			//$chCount = $this->pageModel->checkCountMoneyProj($uid);
			//$chCount = (!empty($chCount) ? 1 : 0 );

			if( $u_needhelp_num > $u_dohelp_num) {
				// The user should first do any help and only then ask for help
					$this->pageView->render_addnotallowed();
					return;
			}
			if($uinf['obl_id'] == 0 || $uinf['city_id'] == 0 || $uinf['email'] == ""){
				$this->pageView->render_addnotallowed('check');
				return;
			}
			/*if($chCount) {
				$this->pageView->render_addnotallowed('manyproj');
				return 0;
			}*/
		}
		
		// Init form values
		$reqd = $this->_initRegvars();
		$reqd['robl'] = $uinf['obl_id'];
		$reqd['rcity'] = $uinf['city_id'];
		$reqd['rcountry'] = $this->catLib->Loc_GetCountryIdByCity($uinf['city_id']);
				
		$formreq = new UhCmsFormData($reqd);
		
		// Pass data to View
		$this->pageView->currency = $this->catLib->Currency_List();
		$this->pageView->userinfo = $uinf;
		$this->pageView->dream_mode = false;
		$this->pageView->event_mode = false;
		$this->pageView->thing_mode = false;
		$this->pageView->myhelp_mode = false;
		
		switch( $this->addmode )
		{
			case ADD_MODE_DREAM:
				$this->pageView->dream_mode = true;
				break;
			case ADD_MODE_THING:
				$this->pageView->thing_mode = true;
				break;
			case ADD_MODE_EVENT:
				$this->pageView->event_mode = true;
				break;
			case ADD_MODE_MYHELP:
				$this->pageView->myhelp_mode = true;
				break;
		}
		
		$this->pageView->render_formnew($formreq);		
	}
	
	public function action_adddream()
	{
		$this->addmode = ADD_MODE_DREAM;
		
		$this->action_add();
	}
	
	public function action_addthing()
	{
		$this->addmode = ADD_MODE_THING;
	
		$this->action_add();
	}
	
	public function action_addevent()
	{
		//$this->pageView->dream_mode = true;
		if( UhCmsApp::getSesInstance()->UserGroup == USR_TYPE_ORG )
			$this->addmode = ADD_MODE_EVENT;
		
		$this->action_add();
	}
	
	public function action_addmyhelp()
	{
		$this->addmode = ADD_MODE_MYHELP;
		
		$this->action_add();
	}
	
	public function action_publish()
	{
		$uid = $this->authAllow();

		$loc = $this->pageView->getLocalizer();
		
		// Get POST vars to assoc array
		$reqd = $this->_getRegVars();
		
		if( UhCmsApp::getSesInstance()->UserGroup != USR_TYPE_ORG )
			$reqd['eventtype'] = 0;
		
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reqd);

		$this->pageView->reqtype = -1;

		if($formreq->ptype == PROJ_TYPE_MONEY) {
			/*$chCount = $this->pageModel->checkCountMoneyProj($uid);
			$chCount = (!empty($chCount) ? 1 : 0 );
			if($chCount) {
				$this->pageView->render_addnotallowed('manyproj');
				return 0;
			}*/
			$this->pageView->reqtype = PROJ_TYPE_MONEY;
			$this->pageView->reqmon = $this->catLib->getPaymentList();
		}
		
		$this->pageView->dream_mode = false;		
		$this->pageView->event_mode = false;
		$this->pageView->thing_mode = false;
		$this->pageView->myhelp_mode = false;
		
		if( $reqd['eventtype'] == 1 )		$this->addmode = ADD_MODE_EVENT;
		if( $reqd['thingtype'] == 1 )		$this->addmode = ADD_MODE_THING;
		if( $reqd['dreamtype'] == 1 )		$this->addmode = ADD_MODE_DREAM;
		if( $reqd['helptype'] == 1 )		$this->addmode = ADD_MODE_MYHELP;
		
		switch( $this->addmode )
		{
			case ADD_MODE_DREAM:
				$this->pageView->dream_mode = true;
				break;
			case ADD_MODE_THING:
				$this->pageView->thing_mode = true;
				break;
			case ADD_MODE_EVENT:
				$this->pageView->event_mode = true;
				break;				
			case ADD_MODE_MYHELP:
				$this->pageView->myhelp_mode = true;
				break;
		}
		
		// Make check for all fields
		if( trim($formreq->tit) == "" )
		{
			$formreq->setok("tit", false);
			$formreq->setmsg("tit", $loc->get("addproj", "err-tit")); //"Вы не указали заголовок вашего проекта");
		}

		if( $formreq->sect == 0 )
		{
			$formreq->setok("sect", false);
			$formreq->setmsg("sect", $loc->get("addproj", "err-sect")); //"Вы не указали в какой раздел размещать проект");
		}
		
		if( $formreq->robl == 0 )
		{
			$formreq->setok("robl", false);
			$formreq->setmsg("robl", $loc->get("addproj", "err-obl")); //"Вы не указали регион для проекта");
		}
		
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", $loc->get("addproj", "err-city")); //"Вы не указали город для проекта");
		}
		
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->stdt)) == 0 )
		{
			$formreq->setok("stdt", false);
			$formreq->setmsg("stdt", $loc->get("addproj", "err-stdt")); //"Дата старта проекта указана с ошибками");			
		}
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->endt)) == 0 )
		{
			$formreq->setok("endt", false);
			$formreq->setmsg("endt", $loc->get("addproj", "err-endt")); //"Дата старта проекта указана с ошибками");
		}
		
		if( ($formreq->ptype == PROJ_TYPE_MONEY) || ($formreq->ptype == PROJ_TYPE_HUMAN) )
		{
			// Check the amount
			if( is_numeric($formreq->pamount) )
			{
				if( $formreq->pamount > 0 )
				{
					// ok
				}
				else
				{
					$formreq->setok("pamount", false);
					$formreq->setmsg("pamount", $loc->get("addproj", "err-amount")); //"Объем помощи должен быть значением больше нуля.");
				}
			}
			else
			{
				$formreq->setok("pamount", false);
				$formreq->setmsg("pamount", $loc->get("addproj", "err-amount1")); //"Объем помощи должен быть допустимым числовым значением и больше нуля.");
			}
		}
		
		if( trim($formreq->descr) == "" )
		{
			$formreq->setok("descr", false);
			$formreq->setmsg("descr", $loc->get("addproj", "err-descr")); //"Вы не указали заголовок вашего проекта");
		}
				
		//$this->pageView->catmodel = $this->catLib;
		
		if( $formreq->has_errors() )
		{
			$this->pageView->msg = $formreq->get_msglist();
			$this->pageView->currency = $this->catLib->Currency_List();
			$this->pageView->render_formnew($formreq);
		}
		else
		{			
			$dubguid = $this->getReqParam("dubguid","--");

			$exists = $this->pageModel->checkDubProject($dubguid);
			
			$newpid = $this->pageModel->addProject($uid, $formreq);			
			$this->pageView->projid = $newpid;
			$this->pageView->projpics = $this->catLib->Item_Pics($newpid);
			$this->pageView->backpicid = 0;
			$this->pageView->userinfo = $this->catLib->Buyer_Info($uid);
			$this->pageView->pymntCount = $this->pageModel->getPaymntInfo();
			$this->pageView->pymntAll = $this->pageModel->getPaymntInfo('all');

			$_SESSION['backpicid'] = 0;
			
			$this->pageView->render_newdone();
		}
	}
	
	public function action_addmedia() {

		$projid = $this->getReqParam("projid", 0);		
		$uid = UhCmsApp::getSesInstance()->UserId;
		
		$addpicbtn = $this->getReqParam("addpicbtn", "");
		$addavatarbtn = $this->getReqParam("addavatarbtn", "");
		$backpicid = $this->getReqParam("backpicid", 0);
		$ptype = $this->getReqParam("ptype", -1);

		$this->pageView->backpicid = $backpicid;
		$this->pageView->userinfo = $this->catLib->Buyer_Info($uid);
		$this->pageView->projid = $projid;
		$this->pageView->projpics = $this->catLib->Item_Pics($projid);
		$this->pageView->reqtype = -1;
		$this->pageView->pymntCount = $this->pageModel->getPaymntInfo();
		$this->pageView->pymntAll = $this->pageModel->getPaymntInfo('all');

		$_SESSION['backpicid'] = $this->pageView->backpicid;

		if($ptype == PROJ_TYPE_MONEY) {
			$this->pageView->reqtype = PROJ_TYPE_MONEY;
			$this->pageView->reqmon = $this->catLib->getPaymentList();
		}
		
		if( $addavatarbtn != "" )
		{
			// Load photo and show add photo again
			if( isset($_FILES['pfileuser']) && ($_FILES['pfileuser']['name'] != "") && ($_FILES['pfileuser']['tmp_name'] != "") )
			{
				// Set new avatar for user
				$this->pageModel->set_UserAvatar($this->pageView->userinfo, $_FILES['pfileuser']['name'], $_FILES['pfileuser']['tmp_name']);
				$this->pageView->userinfo = $this->catLib->Buyer_Info($uid);
			}
					
			$this->pageView->render_newdone();
		}
		else if( $addpicbtn != "" )
		{
			// Load photo and show add photo again		
			if( isset($_FILES['pfile1']) && ($_FILES['pfile1']['name'] != "") && ($_FILES['pfile1']['tmp_name'] != "") )
			{
				$size = getimagesize($_FILES['pfile1']['tmp_name']);
				if($size[0] > minW && $size[1] > minH) {
					$this->pageModel->addProjectPhoto($uid, $projid, $_FILES['pfile1'], 0);
					$this->pageView->projpics = $this->catLib->Item_Pics($projid);
				} else {
					$this->pageView->picErr = 1;
				}
			}

			$this->pageView->render_newdone();
		}
		else
		{
			// PREPAY DECOMMENT WHILE GAME START

			//$this->pageView->render_newfinish();
			/*$this->pageModel->setProjectBackground($projid, $backpicid);
			$amountType = $this->pageModel->checkAmountTypeProj($projid);

			if($amountType[0]['amount_type'] == PROJ_TYPE_MONEY) {
				$this->goToUrl($this->pageView->Page_BuildUrl("proj", "prepay") . '?projid=' . $projid);
			} else {
				$this->goToUrl($this->pageView->Page_BuildUrl("proj", "view") . $projid);
			}
			$this->pageView->render_prepay();
			return;*/
			//$this->goToUrl($this->pageView->Page_BuildUrl("proj", "view") . $projid);*/

			$paymntCount = $this->pageModel->getPaymntInfo();
			$loc = $this->pageView->getLocalizer();

			for($i=1; $i<=$paymntCount[0]['count']; ++$i) {
				$paymnt[$i] = $this->getReqParam('p_'.$i, '');
			}

			$payWoh = $this->getReqParam('pwoh','');
			
			$this->pageModel->setProjPayment($projid, $paymnt, $payWoh);
			$this->pageModel->setProjectBackground($projid, $backpicid);
			$this->goToUrl( $this->pageView->Page_BuildUrl("proj", "view/".$projid) );
			return;
		}
	}

	/*public function action_prepay() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		$projid = $this->getReqParam('projid', 0);

		$check = $this->pageModel->checkProjUser($uid, $projid);
		$check = (!empty($check) ? 1 : 0 );
		$is_pay = $this->pageModel->checkPaymentsProj($uid, $projid);
		$is_pay =	(!empty($is_pay) ? 1 : 0);

		if(!$check || $is_pay) { 	/// DONT FORGER ADD || is_pay
			$this->goToUrl($this->pageView->Page_BuildUrl());
			return false;
		}

		$abres = $this->pageModel->checkABTesting($projid);
		
		if($abres[0]['test_type'] == 2) {
			$uhelp = $this->pageModel->checkUserHelp(PROJ_TYPE_MONEY, 'all', $uid);
			if($uhelp[0]['sum'] < $abres[0]['pays_val']) {
				$abres[0]['pays_val'] = $abres[0]['pays_val'] - $uhelp[0]['sum'];
			} else if($uhelp[0]['sum'] >= $abres[0]['pays_val']){
				$this->pageModel->runProjABtest($projid);
				$this->pageView->run = PROJ_STATUS_RUN;
			}
		}

		$this->pageView->uid = $uid;
		$this->pageView->projid = $projid;
		$this->pageView->paysVal = $abres;

		$this->pageView->render_prepay();
	}*/
	
	public function action_delmedia() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		$projid = $this->getReqParam("projid", 0);
		$picid = $this->getReqParam("picid", 0);
		$projinfo = $this->catLib->Item_Info($projid);
	
		if( $projinfo['author_id'] != $uid ) {
			$this->goToUrl(WWWHOST);
			return;
		}
	
		$this->pageModel->delProjectPhoto($picid, $projid);
		
		//$this->pageView->catmodel = $this->catLib;
		$this->pageView->backpicid = ( isset($_SESSION['backpicid']) ? $_SESSION['backpicid'] : 0 );				
		$this->pageView->projid = $projid;
		$this->pageView->projpics = $this->catLib->Item_Pics($projid);
		$this->pageView->userinfo = $this->catLib->Buyer_Info($uid);

		$this->pageView->render_newdone();
	}
	
	public function action_save() {
		$uid = $this->authAllow();
	
		// Get POST vars to assoc array
		$picid = $this->getReqParam("picid", 0);
		$reqd = $this->_getRegVars();
		$projid = $this->getReqParam("projid", 0);
		$addpicbtn = $this->getReqParam("addpicbtn", "");

		$projinfo = $this->catLib->Item_Info($projid);

		if( $uid != $projinfo['author_id'] ) {
			$this->goToUrl(WWWHOST);
			return;
		}

		if($picid && $projid) {
			$this->pageModel->delProjectPhoto($picid, $projid);
			$formreq = $this->_prepare_projedit($uid, $projinfo);
			$this->pageView->amountType = $projinfo['amount_type'];
			$this->pageView->projid = $projinfo['id'];
			$this->pageView->projpics = $this->catLib->Item_Pics($projinfo['id']);
			$this->pageView->render_formedit($formreq);

			return;
		}

		if( $addpicbtn != "" ) {
			// Load photo and show add photo again
			if( isset($_FILES['pfile1']) && ($_FILES['pfile1']['name'] != "") && ($_FILES['pfile1']['tmp_name'] != "") ) {
				if (is_uploaded_file($_FILES['pfile1']['tmp_name'])) {
					$size = getimagesize($_FILES['pfile1']['tmp_name']);
					if ($size[0] > minW && $size[1] > minH) {
						$this->pageModel->addProjectPhoto($uid, $projid, $_FILES['pfile1'], 0);
						$this->pageView->projpics = $this->catLib->Item_Pics($projid);
					} else {
						$this->pageView->picErr = 1;
					}
				}
			}
			$this->pageView->amountType = $projinfo['amount_type'];
			$this->pageView->projid = $projinfo['id'];
			$this->pageView->projpics = $this->catLib->Item_Pics($projinfo['id']);
			$formreq = $this->_prepare_projedit($uid, $projinfo);
			$this->pageView->render_formedit($formreq);

			return;
		}


		$this->pageView->amountType = $projinfo['amount_type'];
		$this->pageView->projid = $projinfo['id'];
		$this->pageView->projpics = $this->catLib->Item_Pics($projinfo['id']);
	
		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reqd);
	
		// Make check for all fields
		if( trim($formreq->tit) == "" )
		{
			$formreq->setok("tit", false);
			$formreq->setmsg("tit", "Вы не указали заголовок вашего проекта");
		}
	
		if( $formreq->sect == 0 )
		{
			$formreq->setok("sect", false);
			$formreq->setmsg("sect", "Вы не указали в какой раздел размещать проект");
		}
	
		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", "Вы не указали город для проекта");
		}
	
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->stdt)) == 0 )
		{
			$formreq->setok("stdt", false);
			$formreq->setmsg("stdt", "Дата старта проекта указана с ошибками");
		}
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->endt)) == 0 )
		{
			$formreq->setok("endt", false);
			$formreq->setmsg("endt", "Дата старта проекта указана с ошибками");
		}
		
		if( ($formreq->ptype == 0) || ($formreq->ptype == 1) )
		{
			// Check the amount
			if( is_numeric($formreq->pamount) )
			{
				if( $formreq->pamount > 0 )
				{
					// ok
				}
				else
				{
					$formreq->setok("pamount", false);
					$formreq->setmsg("pamount", "Объем помощи должен быть значением больше нуля.");
				}
			}
			else
			{
				$formreq->setok("pamount", false);
				$formreq->setmsg("pamount", "Объем помощи должен быть допустимым числовым значением и больше нуля.");
			}
		}
	
		//$this->pageView->catmodel = $this->catLib;
		if( $formreq->has_errors() )
		{
			$this->pageView->render_formedit($formreq);
		}
		else
		{
			$this->pageModel->saveProject($uid, $projid, $formreq);
			$this->pageView->projid = $projid;
			$this->pageView->render_savedone();
		}
	}
	
	protected function _prepare_projedit($uid, $projinfo)
	{
		$uinf = $this->catLib->Buyer_Info($uid);

		$reqd = $this->_initRegvars();
		$reqd['id'] = $projinfo['id'];
		$reqd['tit'] = $projinfo['title2'];
		$reqd['robl'] = $projinfo['obl_id'];
		$reqd['rcity'] = $projinfo['city_id'];
		$reqd['rcountry'] = $this->catLib->Loc_GetCountryIdByCity($projinfo['city_id']);
		$reqd['descr'] = $projinfo['descr0'];
		$reqd['descrfull'] = $projinfo['descr'];
		$reqd['ptype'] = $projinfo['amount_type'];
		$reqd['pamount'] = round($projinfo['amount']);
		$reqd['pamountizm'] = $projinfo['currency_id'];
		$reqd['sect'] = $projinfo['sectid'];
		$reqd['stdt'] = $projinfo['stdt'];
		$reqd['withendt'] = 1;
		$reqd['endt'] = $projinfo['endt'];
		
		$formreq = new UhCmsFormData($reqd);
		
		//$this->pageView->catmodel = $this->catLib;
		$this->pageView->userinfo = $uinf;
		$this->pageView->currency = $this->catLib->Currency_List();

		$this->pageView->dream_mode = false;
		$this->pageView->event_mode = false;
		$this->pageView->thing_mode = false;
		$this->pageView->myhelp_mode = false;
		
		switch($projinfo['profile_id'])
		{
			case PROJ_EVENT:
				$this->pageView->event_mode = true;
				break;
			
			case PROJ_THINGS:
				$this->pageView->thing_mode = true;
				break;
				
			case PROJ_SENDHELP:
				$this->pageView->myhelp_mode = true;
				break;
		}
		
		return $formreq; 
	}
	
	public function action_edit()
	{
		// Allow access only for authorized users
		$uid = $this->authAllow(); 
		
		$proj_id = 0;
		
		if( count($this->urlparts)>0 )
		{
			$proj_url = $this->urlparts[0];
			
			// Check if this project exists
			$projinfo = $this->catLib->Item_Info($proj_url);
			
			if( $projinfo != null )
				$proj_id = $projinfo['id'];
		}
		
		if( $proj_id == 0 )
			$this->go404();
		
		// User requests not his project
		if( $projinfo['author_id'] != $uid || $projinfo['is_success'])
		{
			$this->goToUrl(WWWHOST);
			return;
		}		
		
		$formreq = $this->_prepare_projedit($uid, $projinfo);
		$this->pageView->amountType = $projinfo['amount_type'];
		$this->pageView->projpics = $this->catLib->Item_Pics($projinfo['id']);
		$this->pageView->projid = $projinfo['id'];
		$this->pageView->render_formedit($formreq);
	}
	
	public function action_picdel()
	{
		$uid = $this->authAllow();
		$projid = $this->getReqParam("projid", 0);
		$picid = $this->getReqParam("picid", 0);
		$projinfo = $this->catLib->Item_Info($projid);
		
		if( $projinfo['author_id'] != $uid )
		{
			$this->goToUrl(WWWHOST);
			return;
		}
		
		$this->pageModel->delProjectPhoto($picid, $projid);
		
		$formreq = $this->_prepare_projedit($uid, $projinfo);
		
		$this->pageView->render_formedit($formreq);
	}
	
	public function action_addhelpmoney()
	{
		$projid = $this->getReqParam("selprojid", 0);
		$helpsum = $this->getReqParam("helpsum", 0);
		$helptxt = $this->getReqParam("helptxt", '');
		$hreqtype = $this->getReqParam("hreqtype", 'dohelp');
		$uid = UhCmsApp::getSesInstance()->UserId;
		
		if( ($projid == 0) || ($uid == 0) )
		{
			//echo $projid."<br>";
			$this->goToUrl(WWWHOST);
			return;
		}		
		
		// Get current project information
		$projinfo = $this->catLib->Item_Info($projid);
		
		// Fill params
		$req_help_type = REQ_TYPE_MONEY;
		if( $projinfo['amount_type'] == PROJ_TYPE_HUMAN )
			$req_help_type = REQ_TYPE_HUMAN;
		if( ($projinfo['amount_type'] == PROJ_TYPE_HUMAN) && ($helpsum == 0) )
			$helpsum = 1;
		if( ($projinfo['amount_type'] == PROJ_TYPE_CONSULT) || ($projinfo['amount_type'] == PROJ_TYPE_OTHER) )
		{
			$helpsum = 1;
			$req_help_type = REQ_TYPE_OTHER;
		}
		if( $hreqtype == "gething" )
		{
			$helpsum = 1;
			$req_help_type = REQ_TYPE_THING;
		}
		else if( $hreqtype == "getmyhelp" )
		{
			$helpsum = 1;
			$req_help_type = REQ_TYPE_OTHER;
		}
		
		$isReqSent = ( ($reqMy = $this->catLib->Buyer_ReqIsSend($uid, $projinfo['id'])) !== false );
		
		if( $isReqSent )
			$this->goToUrl($this->pageView->Page_BuildUrl("proj", "view/".$projinfo['id']));
		
		
		//echo "ht: ".$hreqtype."<br>";

		if( $helpsum > 0 )
		{								
			$this->pageModel->addHelpSum($projid, $uid, $helpsum, $req_help_type, $helptxt);
			//$this->pageModel->addHelpSum($projid, $uid, $helpsum, $helptxt);
			$this->pageView->projinfo = $projinfo;
			if( $hreqtype == "getthing" )
			{				 
				$this->pageView->render_myhelpdone();
			}
			else if( $hreqtype == "getthing" )
			{				 
				$this->pageView->render_thingdone();
			}
			else
				$this->pageView->render_helpdone();
		}
	}
	
	public function action_reviews() {
		$this->pageView->revlist = $this->catLib->Req_List(8, "", 0, false, -1, 0, 21);
		$this->pageView->render_reviews();
	}

	public function action_mnsend() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		$res = $this->getMnsendReqParam(34);
		$this->pageModel->setUserProjUnswer($uid, $res);
		$this->pageModel->setUserMoney($uid, 40);
		$this->pageModel->setHelpReq($uid, 1223);
		$this->goToUrl($this->pageView->Page_BuildUrl("proj", "view").'1223');
	}

	protected function getMnsendReqParam($numQuest = '') {
		$res = [];
		if($numQuest) {
			for ($i = 1; $i<=$numQuest; ++$i) {
				$res['q'.$i] = $this->getReqParam('q_'.$i, '');
			}
		}
		return $res;
	}

	public function action_promise()
	{
		$uid = UhCmsApp::getSesInstance()->UserId;
		$projid = $this->getReqParam('proj', 0);
		$val = $this->getReqParam('helpsum', 0);
		$req_byconv = $this->getReqParam('req', 0);
		$req_byconv = ($req_byconv > 0 ? 1 : 0);

		/*echo $uid.'<br>';
        echo $projid.'<br>';
        echo $val.'<br>'; die();*/

		$item_info = $this->pageModel->getItem($projid);

		if (!$uid || !$projid || !$val || ($item_info[0]['uid'] == $uid)) {
			$this->goToUrl($this->pageView->Page_BuildUrl('proj', 'view') . $projid . '/');
			return;
		}

		$val = round($val);

		//$checkVal = ($prom_ceil <= $this->pageModel->getWohPayInfo() ? 1 : 0);

		$sender_info = $this->catLib->Buyer_Info($uid);
		$loc_cou = -1;
		$def_sum = [MONEY_PROMISE, 'грн', GRN_MAX_PROM_VAL];

		if (!$sender_info['obl_id']) {
			$geo = new UhCmsGEO();

			$IP_location = $geo->getUserLocation();
			$IP_counrty = $IP_location['country']['name_ru'];

			if ($IP_counrty != '') {
				$loc_cou = $this->catLib->searchLocationDB($geo->locationStrCut($IP_counrty), 'country');
				$loc_cou = empty($loc_cou) ? -1 : $loc_cou[0]['country_id'];
			}
		} else {
			if ($this->catLib->getCountry($sender_info['obl_id'])[0]['country_id'] == RUSSIA) {
				$def_sum[0] *= UhCmsExch::conver('GRN', 'RUB');
				$def_sum[1] = 'руб';
				$def_sum[2] = RUB_MAX_PROM_VAL;

				$promiseVal = $this->pageModel->getUserSumPromiseMoney($uid, 0, 1, 'GRN', 'RUB');
				$promiseVal_payed = $this->pageModel->getUserSumPromiseMoney($uid, 1, 1, 'GRN', 'RUB');
			} else {
				$promiseVal = $this->pageModel->getUserSumPromiseMoney($uid, 0, 1, 'RUB', 'GRN');
				$promiseVal_payed = $this->pageModel->getUserSumPromiseMoney($uid, 1, 1, 'RUB', 'GRN');
			}
		}

		if ($loc_cou != -1) {
			if ($loc_cou == RUSSIA) {
				$def_sum[0] *= UhCmsExch::conver('GRN', 'RUB');
				$def_sum[1] = 'руб';
				$def_sum[2] = RUB_MAX_PROM_VAL;

				$promiseVal = $this->pageModel->getUserSumPromiseMoney($uid, 0, 1, 'GRN', 'RUB');
				$promiseVal_payed = $this->pageModel->getUserSumPromiseMoney($uid, 1, 1, 'GRN', 'RUB');
			} else {
				$promiseVal = $this->pageModel->getUserSumPromiseMoney($uid, 0, 1, 'RUB', 'GRN');
				$promiseVal_payed = $this->pageModel->getUserSumPromiseMoney($uid, 1, 1, 'RUB', 'GRN');
			}
		}

		$itm_val = $this->catLib->getItemCurrency($projid);
		$this->pageView->to_pay = ['type' => 'UAH', 'val' => 0];

		if (!empty($itm_val)) {
			if ($itm_val[0]['cur_code'] == $promiseVal[0]['pcur']) {

				$this->pageView->to_pay['val'] = ($itm_val[0]['cur_code'] == 'GRN' ? 'UAH' : $itm_val[0]['cur_code']);
				$itm_val = $val;

			} else if ($itm_val[0]['cur_code'] != $promiseVal[0]['pcur']) {

				$this->pageView->to_pay['val'] = ($itm_val[0]['cur_code'] == 'GRN' ? 'UAH' : $itm_val[0]['cur_code']);
				$itm_val = $val * UhCmsExch::conver($itm_val[0]['cur_code'], $promiseVal[0]['pcur']);
			}

		} else
			$itm_val = 0;

		$this->pageView->to_pay['val'] = ($promiseVal[0]['psum'] + $itm_val) - $promiseVal_payed[0]['psum'];

		$prom_ceil = ($this->pageView->to_pay['val'] > MONEY_PROMISE) ? ceil($this->pageView->to_pay['val'] / MONEY_PROMISE) : 0;

		/*echo 'first = '.$promiseVal[0]['psum'].'<br>';
        echo 'second = '.$promiseVal_payed[0]['psum'].'<br>';
        echo 'Minus = '.(($promiseVal[0]['psum'] + $itm_val) - $promiseVal_payed[0]['psum']); echo '<hr>';
        print_r($prom_ceil); die();*/

		$val_check = 0;

		if (($item_info[0]['currency_id'] == 1 && $val >= GRN_MAX_PROM_VAL) || ($item_info[0]['currency_id'] != 1 && $val >= RUB_MAX_PROM_VAL))
			$val_check = 1;

		//echo 'val = '.$val.'<br>';
		//echo 'check = '.$val_check.'<br>'; die();

		if ($prom_ceil == 0 && $val_check == 0) {
			$this->pageView->viewMode = 'def';
			$isReqSent = (($reqMy = $this->catLib->Buyer_ReqIsSend($uid, $projid)) !== false);
			$this->pageModel->setHelpReq($uid, $projid, PROJ_TYPE_MONEY, 1, $val, ($isReqSent ? 1 : 0), REQ_PROMISE, 1, $req_byconv);
			$reqid = $this->db->insert_id();
			$this->pageModel->setUserMoney($uid, 2);

			$loc = $this->pageView->getLocalizer();

			//$msgText = $loc->get('projmail','help-descr-pr');
			$msgText = $loc->get('projmail', 'help-descr-pr2');
			$msgText .= $val . ' ' . $item_info[0]['curr_name'];
			$msgText .= $loc->get('projmail', 'help-descr-pr3');
			$msgText .= '<a href="' . $this->pageView->Page_BuildUrl('users', 'viewrev') . $sender_info['id'] . '/?utmget=1&req=' . $reqid . '" >' . $sender_info['name'] . ' ' . $sender_info['fname'] . '</a>';
			$msgText .= $loc->get('projmail', 'help-descr-pr4');
			$msgText = str_replace('_hr_', '<a href="' . $this->pageView->Page_BuildUrl('users', 'viewrev') . $sender_info['id'] . '/?utmget=1&req=' . $reqid . '" >' . 'ссылке' . '</a>', $msgText);

			$this->pageModel->sendUserMailMsg($loc->get('projmail', 'help-tit'), $msgText, $item_info[0]['uid']);

		} else {
			$this->pageView->viewMode = 'toomany';
		}

		$this->pageView->projid = $projid;
		$this->pageView->val = $val;
		$this->pageView->uid = $uid;
		$this->pageView->allSum = $promiseVal[0]['psum'] + $itm_val;
		$this->pageView->def_sum = $def_sum;

		$this->pageView->render_promise();
	}
	
	public function action_sendsms() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		if(!$uid) {
			$this->goToUrl($this->pageView->Page_BuildUrl());
			return;
		}

		$phone = $this->getReqParam('phone', '');

		$sms = new UhCmsSMS();

			$secCode = $sms->okCode($phone);
			$r = $sms->sendSms($phone, "Ваш код подтверждения: ".$secCode);
			$this->pageModel->setBuyerNumber($uid, $secCode, $phone);

			if ($r[1] > 0) {
				return "Код подтверждения отправлен на номер " . $phone;
			}
			echo 'done';
	}

	public function action_smsok() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		if(!$uid) {
			$this->goToUrl($this->pageView->Page_BuildUrl());
			return;
		}

		$code = $this->getReqParam('code', '');
		$phone = $this->getReqParam('phone', '');

		$sms = new UhCmsSMS();

		if ($code) {
			$oc = $sms->okCode($phone);

			if ($oc == $code) {
				$this->pageModel->updBuyerNumber($uid);
				echo "done";
			}
			else
				echo "err";
		}
	}

	public function action_sendlasthlp() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		$projid = $this->getReqParam('projid', 0);
		$val = $this->getReqParam('val', 1);

		if($uid && $projid && $val) {
			if(is_numeric($uid) && is_numeric($projid) && is_numeric($val)) {
				$isReqSent = (($reqMy = $this->catLib->Buyer_ReqIsSend($uid, $projid)) !== false);
				$this->pageModel->setHelpReq($uid, $projid, PROJ_TYPE_MONEY, 1, $val, ($isReqSent ? 1 : 0), REQ_PROMISE);
				$this->pageModel->setUserMoney($uid, 2);
				echo 'done';
			}
		} else if ($uid && $projid && !$val) {
			$isReqSent = (($reqMy = $this->catLib->Buyer_ReqIsSend($uid, $projid)) !== false);
			if($isReqSent == 0) {
				$this->pageModel->setHelpReq($uid, $projid, PROJ_TYPE_CONSULT, 1, $val, ($isReqSent ? 1 : 0), REQ_NOPROMISE);
				$this->pageModel->setUserMoney($uid, 40);
				//echo 'done';
			}
		}

	}

	public function action_addhelp() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		$pid = $this->getReqParam('pid', 0);
		$sum = $this->getReqParam('sum', 0);

		if($pid != 0 && $sum != 0) {
			if(is_numeric($pid) && is_numeric($sum)) {
				$isReqSent = (($reqMy = $this->catLib->Buyer_ReqIsSend($uid, $pid)) !== false);
				$this->pageModel->setHelpReq($uid, $pid, PROJ_TYPE_MONEY, 0, $sum, ($isReqSent ? 1 : 0), REQ_NOPROMISE, 0);
				$this->goToUrl($this->pageView->Page_BuildUrl('proj', 'view') . $pid);
			} else {
				$this->goToUrl($this->pageView->Page_BuildUrl());
			}
		}

	}

	public function action_prprom() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		$projid = $this->getReqParam('proj', 0);
		$val = $this->getReqParam('helpsum', 0);
		$req_byconv = $this->getReqParam('req', 0);
		$req_byconv = ($req_byconv > 0 ? 1 : 0);
		$to_p = $this->getReqParam('to_p', '');
		$res = [];

		/*echo $uid.'<br>';
		echo $projid.'<br>';
		echo $val.'<br>'; die();*/

		$item_info = $this->pageModel->getItem($projid);

		if(!$uid || !$projid || !$val || ($item_info[0]['uid'] == $uid)) {
			//$this->goToUrl($res[Page_BuildUrl('proj','view').$projid.'/');
			return 0;
		}

		$val = round($val);

		//$checkVal = ($prom_ceil <= $this->pageModel->getWohPayInfo() ? 1 : 0);

		$sender_info = $this->catLib->Buyer_Info($uid);
		$loc_cou = -1;
		$def_sum = [MONEY_PROMISE, 'грн', GRN_MAX_PROM_VAL];

		if(!$sender_info['obl_id']) {
			$geo = new UhCmsGEO();

			$IP_location = $geo->getUserLocation();
			$IP_counrty = $IP_location['country']['name_ru'];

			if($IP_counrty != '') {
				$loc_cou = $this->catLib->searchLocationDB($geo->locationStrCut($IP_counrty), 'country');
				$loc_cou = empty($loc_cou) ? -1 : $loc_cou[0]['country_id'];
			}
		} else {
			if($this->catLib->getCountry($sender_info['obl_id'])[0]['country_id'] == RUSSIA) {
				$def_sum[0] *= UhCmsExch::conver('GRN', 'RUB');
				$def_sum[1] = 'руб';
				$def_sum[2] = RUB_MAX_PROM_VAL;

				$promiseVal = $this->pageModel->getUserSumPromiseMoney($uid, 0, 1, 'GRN', 'RUB');
				$promiseVal_payed = $this->pageModel->getUserSumPromiseMoney($uid, 1, 1, 'GRN', 'RUB');
			} else {
				$promiseVal = $this->pageModel->getUserSumPromiseMoney($uid, 0, 1, 'RUB', 'GRN');
				$promiseVal_payed = $this->pageModel->getUserSumPromiseMoney($uid, 1, 1, 'RUB', 'GRN');
			}
		}

		if($loc_cou != -1) {
			if($loc_cou == RUSSIA) {
				$def_sum[0] *= UhCmsExch::conver('GRN', 'RUB');
				$def_sum[1] = 'руб';
				$def_sum[2] = RUB_MAX_PROM_VAL;

				$promiseVal = $this->pageModel->getUserSumPromiseMoney($uid, 0, 1, 'GRN', 'RUB');
				$promiseVal_payed = $this->pageModel->getUserSumPromiseMoney($uid, 1, 1, 'GRN', 'RUB');
			} else {
				$promiseVal = $this->pageModel->getUserSumPromiseMoney($uid, 0, 1, 'RUB', 'GRN');
				$promiseVal_payed = $this->pageModel->getUserSumPromiseMoney($uid, 1, 1, 'RUB', 'GRN');
			}
		}

		$itm_val = $this->catLib->getItemCurrency($projid);
		$res['to_pay'] = ['type' => 'UAH', 'val' => 0];

		if(!empty($itm_val)) {
			if ($itm_val[0]['cur_code'] == $promiseVal[0]['pcur']) {

				$res['to_pay']['val'] = ($itm_val[0]['cur_code'] == 'GRN' ? 'UAH' : $itm_val[0]['cur_code']);
				$itm_val = $val;

			} else if ($itm_val[0]['cur_code'] != $promiseVal[0]['pcur']) {

				$res['to_pay']['val'] = ($itm_val[0]['cur_code'] == 'GRN' ? 'UAH' : $itm_val[0]['cur_code']);
				$itm_val = $val * UhCmsExch::conver($itm_val[0]['cur_code'], $promiseVal[0]['pcur']);
			}

		} else
			$itm_val = 0;

		$res['to_pay']['val'] = ($promiseVal[0]['psum'] + $itm_val) - $promiseVal_payed[0]['psum'];

		$prom_ceil = ($res['to_pay']['val'] > MONEY_PROMISE) ? ceil($res['to_pay']['val'] / MONEY_PROMISE) : 0;

		/*echo 'first = '.$promiseVal[0]['psum'].'<br>';
		echo 'second = '.$promiseVal_payed[0]['psum'].'<br>';
		echo 'Minus = '.(($promiseVal[0]['psum'] + $itm_val) - $promiseVal_payed[0]['psum']); echo '<hr>';
		print_r($prom_ceil); die();*/

		$val_check = 0;

		if(($item_info[0]['currency_id'] == 1 && $val >= GRN_MAX_PROM_VAL) || ($item_info[0]['currency_id'] != 1 && $val >= RUB_MAX_PROM_VAL))
			$val_check = 1;

		//echo 'val = '.$val.'<br>';
		//echo 'check = '.$val_check.'<br>'; die();

		if($prom_ceil == 0 && $val_check == 0) {
			$res['viewMode'] = 'def';
			$isReqSent = ( ($reqMy = $this->catLib->Buyer_ReqIsSend($uid, $projid)) !== false );
			$this->pageModel->setHelpReq($uid, $projid, PROJ_TYPE_MONEY, 1, $val, ($isReqSent ? 1 : 0), REQ_PROMISE, 1, $req_byconv);
			$reqid = $this->db->insert_id();
			$this->pageModel->setUserMoney($uid, 2);

			//$loc =;

			//$msgText = $loc->get('projmail','help-descr-pr');
			$msgText = UhCmsApp::getLocalizerInstance()->get("projmail", "help-descr-pr2");//
			$msgText .= $val.' '.$item_info[0]['curr_name'];
			$msgText .= UhCmsApp::getLocalizerInstance()->get("projmail", "help-descr-pr3"); //
			$msgText .= '<a href="'.WWWHOST.'users/viewrev/'.$sender_info['id'].'/?utmget=1&req='.$reqid.'" >'.$sender_info['name'] . ' ' . $sender_info['fname'].'</a>';
			$msgText .= UhCmsApp::getLocalizerInstance()->get("projmail", "help-descr-pr4");
			$msgText = str_replace('_hr_', '<a href="'.WWWHOST.'users/viewrev/'.$sender_info['id'].'/?utmget=1&req='.$reqid.'" >'.'ссылке'.'</a>', $msgText );

			$this->pageModel->sendUserMailMsg(UhCmsApp::getLocalizerInstance()->get("projmail", "help-tit"), $msgText, $item_info[0]['uid']);

		} else {
			$res['viewMode'] = 'toomany';
		}

		$res['projid'] = $projid;
		$res['val'] = $val;
		$res['uid'] = $uid;
		$res['allSum'] = $promiseVal[0]['psum'] + $itm_val;
		$res['def_sum'] = $def_sum;
		$res['to_p'] = $to_p;

		echo json_encode($res);

		//$res[render_promise();
	}

	public function action_userpromise() {
		$this->pageView->uid = $this->getReqParam('uid', 0);
		$this->pageView->projid = $this->getReqParam('projid', 0);
		$this->pageView->allSum = $this->getReqParam('allSum', 0);
		$this->pageView->viewMode = $this->getReqParam('viewMode', '');
		$this->pageView->val = $this->getReqParam('val', '');
		$this->pageView->to_pay = $this->getReqParam('to_pay', '');
		$this->pageView->def_sum = $this->getReqParam('def_sum', '');
		$this->pageView->to_p = $this->getReqParam('to_p', '');

		$this->pageView->render_promise();
	}

}