<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("PUBL_ITEMS_PERPAGE", 15);

define("PUBL_GROUP_NEWS", 0);
define("PUBL_GROUP_ARTIC", 1);
define("PUBL_GROUP_OBZOR", 2);

class Publ extends PageController
{		
	protected $catLib;
	protected $newsLib;
	
	protected $ngroup;
	protected $pi;
	protected $pn;
	//protected $surl;		// Current section url part
	//protected $rurl;		// Root section url part
	//protected $catmode;		// Control which section style is applied to opened section
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		//$this->self_contolled = true;
		
		$this->ngroup = PUBL_GROUP_NEWS;
		$this->pi = 0;
		$this->pn = PUBL_ITEMS_PERPAGE;
		//$this->surl = "";
		//$this->rurl = "";
		//$this->catmode = "";	

		$this->newsLib = new UhCmsNews($db, $this->LangId);
	}
	
	public function action_default()
	{
		//$uid = $this->authAllow();
		//$uid = UhCmsApp::getSesInstance()->UserId;
	
		$goto_url = "";
		switch($this->ngroup)
		{
			case PUBL_GROUP_NEWS:
				$goto_url = $this->pageView->Page_BuildUrl("publ", "news");
				break;
			case PUBL_GROUP_ARTIC:
				$goto_url = $this->pageView->Page_BuildUrl("publ", "helpwall");
				break;
			case PUBL_GROUP_OBZOR:
				$goto_url = $this->pageView->Page_BuildUrl("publ", "obzor");
				break;
		}
	
		header("Location: ".$goto_url);
		exit();
	
		// Render page
		$this->pageView->render_default();
	}	
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['rlogin'] = $this->getReqParam("rlogin", "");
	
		return $reg;
	}
	
	private function _initRegvars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['rlogin'] = "";
	
		return $reg;
	}
	
	private function _prepare_publ($pi = 0)
	{
		$this->pageView->its_total = $this->newsLib->News_ItemsNum($this->ngroup);
		$this->pageView->its = $this->newsLib->News_Items($this->ngroup, "add", 300, $pi, $this->pn);
		$this->pageView->ngroup = $this->ngroup;
		$this->pageView->pi = $pi;
		$this->pageView->pn = $this->pn;
	}
	
	private function _prepare_publ_info($post_id, $post_inf)
	{
		$this->pageView->ngroup = $this->ngroup;
		$this->pageView->pi = 0;
		$this->pageView->pn = $this->pn;
		$this->pageView->iteminf = $post_inf;
	}
	
	private function _parse_pagei()
	{
		$_pi = 0;
	
		if( count($this->urlparts)>0 )
		{
			$pi_url = $this->urlparts[0];
			
			if( preg_match("/^p_([0-9]+)\.html$/", $pi_url, $matches) > 0 )
			{
				$_pi = $matches[1]; 
			}
		}
		
		return $_pi;
	}
	
	public function action_news()
	{
		$this->ngroup = PUBL_GROUP_NEWS;
		
		$this->pi = $this->_parse_pagei();
		
		$post_id = 0;

		if( $this->pi == 0 )
		{
			if( count($this->urlparts)>0 )
			{
				$post_url = $this->urlparts[0];
					
				// Check if this project exists
				$post_info = $this->newsLib->News_ItemInfo($post_url);
				if( ($post_info != null) && isset($post_info['id']) )
					$post_id = $post_info['id'];
				
				//if( $post_id == 0 )
				//	$this->go404();
			}		
		}
		
		if( $post_id != 0 )
		{
			$this->_prepare_publ_info($post_id, $post_info);
			
			$this->pageView->render_item($this->newsLib->News_GroupInfo($this->ngroup));
		}
		else
		{
			$this->_prepare_publ();
		
			// Render page
			$this->pageView->render_default();
		}
	}
	
	public function action_helpwall()
	{
		$this->ngroup = PUBL_GROUP_ARTIC;
		
		$this->pi = $this->_parse_pagei();
		
		$post_id = 0;
		
		if( $this->pi == 0 )
		{
			if( count($this->urlparts)>0 )
			{
				$post_url = $this->urlparts[0];
					
				// Check if this project exists
				$post_info = $this->newsLib->News_ItemInfo($post_url);
				if( ($post_info != null) && isset($post_info['id']) )
					$post_id = $post_info['id'];
		
				//if( $post_id == 0 )
				//	$this->go404();
			}
		}
		
		if( $post_id != 0 )
		{
			$this->_prepare_publ_info($post_id, $post_info);
				
			$this->pageView->render_item($this->newsLib->News_GroupInfo($this->ngroup));
		}
		else
		{	
			$this->_prepare_publ();
	
			// Render page
			$this->pageView->render_default();
		}
	}
	
	public function action_obzor()
	{
		$this->ngroup = PUBL_GROUP_OBZOR;
		
		$this->pi = $this->_parse_pagei();
		
		$post_id = 0;
		
		if( $this->pi == 0 )
		{
			if( count($this->urlparts)>0 )
			{
				$post_url = $this->urlparts[0];
					
				// Check if this project exists
				$post_info = $this->newsLib->News_ItemInfo($post_url);
				if( ($post_info != null) && isset($post_info['id']) )
					$post_id = $post_info['id'];
		
				if( $post_id == 0 )
					$this->go404();
			}
		}
		
		if( $post_id != 0 )
		{
			$this->_prepare_publ_info($post_id, $post_info);
		
			$this->pageView->render_item($this->newsLib->News_GroupInfo($this->ngroup));
		}
		else
		{	
			$this->_prepare_publ();
		
			// Render page
			$this->pageView->render_default();
		}
	}
	
	// Method for routing in self controlled controllers
	/*
	public function handle_action($urlparts)
	{
		$sort_by = "";
		$flt_by = "";
		$city_by = "";
		$pi = 0;
		
		//var_dump($urlparts);
		//echo "<Br>";
		
		if( $urlparts[0] == "" )
		{
			// Show root 
			//$this->action_default();
			$this->render_section(0, $sort_by, $flt_by, $city_by, $pi);
		}
		else
		{		
			$sid = $this->catLib->Catalog_SectIdByUrl($urlparts[0]);
			if( $sid != 0 )
			{
				// url is correct and section found
				$this->surl = $urlparts[0];
				$this->sid = $sid;
				
				array_shift($urlparts);
			}
								
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^sort_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 )
				{
					$sort_by = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^flt_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 )
				{
					$flt_by = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^city_([0-9a-zA-Z]+)$/", $urlparts[0], $matches)>0 )
				{
					$city_by = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^p_([0-9]+)$/", $urlparts[0], $matches)>0 )
				{
					$pi = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( ($sid != 0) || ($sort_by != "") || ($flt_by != "") || ($city_by != "") || ($pi > 0) )
			{
				if( $sid != 0 )
					$this->render_section($sid, $sort_by, $flt_by, $city_by, $pi);
				else 
					$this->render_section($sid, $sort_by, $flt_by, $city_by, $pi);
			}		
			else
			{
				$this->go404();			
			}		
		}		
	}
	*/
	
	public function render_section($sid, $sort_by="", $flt_by="", $city_by="", $pi=0)
	{
		if( $sid != 0 )
		{
			// Find path to section and add it to breadcrumbs
			$spath = $this->catLib->Catalog_SectPath($sid);
	
			for( $i=(count($spath)-1); $i>=0; $i-- )
			{
				if( $i == (count($spath)-1) )
					$this->rurl = $spath[$i]['url'];
				
				$this->pageView->addBreadcrumbs($this->pageView->Catalog_BuildUrl($spath[$i]['url']), $spath[$i]['name']);
			}
			
			//echo $this->rurl."<br>";
			
			// Set the view mode for the catalog section 
			if( $this->rurl == "thing-market" )
				$this->catmode = CAT_MODE_THING;
			else if( $this->rurl == "fulfill-dream" )
				$this->catmode = CAT_MODE_DREAM;
			
			//echo $this->catmode."<br>";
				
			$subsectnum = $this->catLib->Catalog_SectSubNum(CAT_HELPGET_MODE, $sid);
			
			$slev = 3;
			if( count($spath) == 1 )
				$slev = 2;
			
			if( $pi > 0 )
				$pi--;
			
			$sinf = ( $sid == 0 ? Array("id" => 0) : $this->catLib->Catalog_SectInfo($sid) );
			
			$this->pageView->spath = $spath;
			$this->pageView->surl = $this->surl;
			$this->pageView->rurl = $this->rurl;
			$this->pageView->sinf = ( $sid == 0 ? Array("id" => 0) : $this->catLib->Catalog_SectInfo($sid) );
			
			if( $sinf['id'] != 0 )
				$this->pageView->page_h1 = $sinf['name'];
		}
		else 
		{
			$slev = -1;
			
			$this->pageView->spath = Array();
			$this->pageView->page_h1 = "Запросы о помощи";
			
			//echo "!!";
		}
		
		//echo "ok";
		//return;
		
		// Fill data to pageView object		
		$this->pageView->sid = $sid;
		$this->pageView->catmode = $this->catmode;		
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->pi = $pi;
		$this->pageView->pn = CAT_ITEMS_PERPAGE;
		$this->pageView->sortby = ( ($this->catmode == CAT_MODE_THING) && ($sort_by == "") ? "add" : $sort_by );
		$this->pageView->fltby = $flt_by;
		$this->pageView->cityidby = $city_by;
		$this->pageView->countrylist = $this->catLib->Loc_CountryList(0, true, ( $sid == 0 ? "all" : $sid ));
		
		$this->pageView->its_total = $this->catLib->Item_Num(PROJ_ANY, $sid, 0, $slev, $this->pageView->sortby, PROJ_STATUS_RUN, $flt_by, $city_by);
		$this->pageView->its = $this->catLib->Item_List(PROJ_ANY, $sid, 0, $this->pageView->pi, $this->pageView->pn, $slev, $this->pageView->sortby, PROJ_STATUS_RUN, $flt_by, $city_by);
		for( $i=0; $i<count($this->pageView->its); $i++ )
		{
			$this->pageView->its[$i]['locinfo'] = $this->catLib->Loc_InfoByCity($this->pageView->its[$i]['city_id']);
			$this->pageView->its[$i]['currency'] = $this->catLib->Currency_Info($this->pageView->its[$i]['currency_id']);
			if( $this->pageView->its[$i]['account_type'] == USR_TYPE_ORG )
				$this->pageView->its[$i]['photos'] = $this->catLib->Item_Pics($this->pageView->its[$i]['id'], 1);
		}
		
		//$this->pageView->catmodel = $this->catLib;
		
		// Render page
		$this->pageView->render_default();
	}		
}
?>