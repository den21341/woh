<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("CAT_ITEMS_PERPAGE", 14);

define("CAT_MODE_NORMAL", "");
define("CAT_MODE_DREAM", "dream");
define("CAT_MODE_THING", "thing");

class Bonus extends PageController
{
	protected $catLib;

	protected $sid;
	protected $surl;		// Current section url part
	protected $rurl;		// Root section url part
	protected $catmode;		// Control which section style is applied to opened section

	function __construct($config, $db)
	{
		parent::__construct($config, $db);

		//$this->self_contolled = true;

		$this->sid = 0;
		$this->surl = "";
		$this->rurl = "";
		$this->catmode = "";
	}

	// Check if this page is allowed to unauthorized user
	// If not, then go to website start page
	private function authAllow()
	{
		$uid = UhCmsApp::getSesInstance()->UserId;
		if( $uid == 0 )
		{
			header("Location: ".WWWHOST);
			exit();
		}

		return $uid;
	}


	public function action_list()
	{
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->page_h1 = "Подвешенные бонусы";
		$this->pageView->its_total = $this->catLib->Bonus_Num();
		$this->pageView->its = $this->catLib->Bonus_List(0, 0, 1000, "add");
		for( $i=0; $i<count($this->pageView->its); $i++ )
		{
			$this->pageView->its[$i]['photos'] = $this->catLib->Bonus_Pics($this->pageView->its[$i]['id'], 1);
		}

		$this->pageView->render_default();
	}

	public function action_default()
	{
		//$uid = $this->authAllow();
		//$uid = UhCmsApp::getSesInstance()->UserId;

		$this->pageView->spath = Array();
		$this->pageView->sid = 0;
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->page_h1 = "Запросы о помощи";

		$this->pageView->pi = 0;
		$this->pageView->pn = CAT_ITEMS_PERPAGE;

		$slev = -1;

		$this->pageView->its_total = $this->catLib->Item_Num(PROJ_ANY, 0, 0, $slev);
		$this->pageView->its = $this->catLib->Item_List(PROJ_ANY, 0, 0, 0, CAT_ITEMS_PERPAGE, $slev, "add", PROJ_STATUS_RUN);
		for( $i=0; $i<count($this->pageView->its); $i++ )
		{
			$this->pageView->its[$i]['currency'] = $this->catLib->Currency_Info($this->pageView->its[$i]['currency_id']);
			if( $this->pageView->its[$i]['account_type'] == USR_TYPE_ORG )
				$this->pageView->its[$i]['photos'] = $this->catLib->Item_Pics($this->pageView->its[$i]['id'], 1);
		}

		//$this->pageView->catmodel = $this->catLib;

		/*
		$uinfo = $clib->Buyer_Info($uid);
		$uinfo['group_name'] = $clib->Buyer_GroupInfo($uinfo['account_type']);		
		$this->pageView->userinfo = $uinfo;
		$this->pageView->catmodel = $this->catLib;
		$this->pageView->curl = '';
		*/
		// Render page
		$this->pageView->render_default();
	}

	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['tit'] = $this->getReqParam("tit", "");
		$reg['descr'] = $this->getReqParam("descr", "");
		$reg['descrfull'] = $this->getReqParam("descrfull", "");
		$reg['stdt'] = $this->getReqParam("stdt", date("d.m.Y", time()));
		$reg['withendt'] = $this->getReqParam("withendt", 1);
		$reg['endt'] = $this->getReqParam("endt", date("d.m.Y", time()+30*24*3600));
		$reg['ptype'] = $this->getReqParam("ptype", 0);
		$reg['sect'] = $this->getReqParam("sect", 0);
		$reg['diff'] = $this->getReqParam("diff", 16);
		$reg['pamount'] = $this->getReqParam("pamount", 0);
		$reg['rcountry'] = $this->getReqParam("rcountry", 1);
		$reg['robl'] = $this->getReqParam("robl", 0);
		$reg['rcity'] = $this->getReqParam("rcity", 0);

		return $reg;
	}

	private function _initRegVars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['tit'] = "";
		$reg['descr'] = "";
		$reg['descrfull'] = "";
		$reg['stdt'] = date("d.m.Y", time());
		$reg['withendt'] = 1;
		$reg['endt'] = date("d.m.Y", time()+30*24*3600);
		$reg['ptype'] = 0;
		$reg['diff'] = 16;
		$reg['pamount'] = "0";
		$reg['rcountry'] = 1;
		$reg['robl'] = 0;
		$reg['rcity'] = 0;

		return $reg;
	}

	public function action_add()
	{
		$uid = $this->authAllow();

		$uinf = $this->catLib->Buyer_Info($uid);

		// Check if the number of needhelp Projects is less then amount of dohelp requests
		// Check this only for persons and companies
		// Organizations are allowed to post many projects
		if( $uinf['account_type'] != USR_TYPE_ORG )
		{
			// The user should first do any help and only then ask for help
			//$this->pageView->render_addnotallowed();
			//return;		
		}

		// Init form values
		$reqd = $this->_initRegVars();
		$reqd['robl'] = $uinf['obl_id'];
		$reqd['rcity'] = $uinf['city_id'];
		$reqd['rcountry'] = $this->catLib->Loc_GetCountryIdByCity($uinf['city_id']);

		$formreq = new UhCmsFormData($reqd);

		$this->pageView->catmodel = $this->catLib;

		$this->pageView->render_add_bonus($formreq);
	}

	public function action_publish()
	{
		$uid = $this->authAllow();

		$loc = $this->pageView->getLocalizer();

		// Get POST vars to assoc array
		$reqd = $this->_getRegVars();

		//if( UhCmsApp::getSesInstance()->UserGroup != USR_TYPE_ORG )
		//	$reqd['eventtype'] = 0;

		// Build object to pass data to form view and model
		$formreq = new UhCmsFormData($reqd);

		// Make check for all fields
		if( trim($formreq->tit) == "" )
		{
			$formreq->setok("tit", false);
			$formreq->setmsg("tit", $loc->get("addbonus", "err-tit")); //"Вы не указали заголовок вашего проекта");
		}

		//if( $formreq->sect == 0 )
		//{
		//	$formreq->setok("sect", false);
		//	$formreq->setmsg("sect", $loc->get("addbonus", "err-sect")); //"Вы не указали в какой раздел размещать проект");
		//}

		if( $formreq->robl == 0 )
		{
			$formreq->setok("robl", false);
			$formreq->setmsg("robl", $loc->get("addbonus", "err-obl")); //"Вы не указали регион для проекта");
		}

		if( $formreq->rcity == 0 )
		{
			$formreq->setok("rcity", false);
			$formreq->setmsg("rcity", $loc->get("addbonus", "err-city")); //"Вы не указали город для проекта");
		}

		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->stdt)) == 0 )
		{
			$formreq->setok("stdt", false);
			$formreq->setmsg("stdt", $loc->get("addbonus", "err-stdt")); //"Дата старта проекта указана с ошибками");
		}
		if( preg_match("/^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}$/", trim($formreq->endt)) == 0 )
		{
			$formreq->setok("endt", false);
			$formreq->setmsg("endt", $loc->get("addbonus", "err-endt")); //"Дата старта проекта указана с ошибками");
		}

		// Check the amount
		if( is_numeric($formreq->pamount) )
		{
			if( $formreq->pamount > 0 )
			{
				// ok
			}
			else
			{
				$formreq->setok("pamount", false);
				$formreq->setmsg("pamount", $loc->get("addbonus", "err-amount")); //"Объем помощи должен быть значением больше нуля.");
			}
		}
		else
		{
			$formreq->setok("pamount", false);
			$formreq->setmsg("pamount", $loc->get("addbonus", "err-amount1")); //"Объем помощи должен быть допустимым числовым значением и больше нуля.");
		}


		if( trim($formreq->descr) == "" )
		{
			$formreq->setok("descr", false);
			$formreq->setmsg("descr", $loc->get("addbonus", "err-descr")); //"Вы не указали заголовок вашего проекта");
		}

		if( $formreq->has_errors() )
		{
			$this->pageView->msg = $formreq->get_msglist();
			//$this->pageView->currency = $this->catLib->Currency_List();
			$this->pageView->render_add_bonus($formreq);
		}
		else
		{
			$newpid = $this->pageModel->addBonus($uid, $formreq);
			$this->pageView->projid = $newpid;
			$this->pageView->projpics = $this->catLib->Bonus_Pics($newpid);
			$this->pageView->userinfo = $this->catLib->Buyer_Info($uid);

			//$this->catLib->setMoneyBox($uid, 30, 'bonus');

			$this->pageView->render_done_bonus();
		}
	}

	public function action_addmedia()
	{
		$projid = $this->getReqParam("projid", 0);
		$uid = UhCmsApp::getSesInstance()->UserId;

		$addpicbtn = $this->getReqParam("addpicbtn", "");
		$addavatarbtn = $this->getReqParam("addavatarbtn", "");
		//$backpicid = $this->getReqParam("backpicid", 0);

		//$this->pageView->backpicid = $backpicid;
		$this->pageView->userinfo = $this->catLib->Buyer_Info($uid);
		$this->pageView->projid = $projid;
		$this->pageView->projpics = $this->catLib->Bonus_Pics($projid);

		//$_SESSION['backpicid'] = $this->pageView->backpicid;

		if( $addavatarbtn != "" )
		{
			// Load photo and show add photo again
			if( isset($_FILES['pfileuser']) && ($_FILES['pfileuser']['name'] != "") && ($_FILES['pfileuser']['tmp_name'] != "") )
			{
				// Set new avatar for user
				$this->pageModel->set_UserAvatar($this->pageView->userinfo, $_FILES['pfileuser']['name'], $_FILES['pfileuser']['tmp_name']);
				$this->pageView->userinfo = $this->catLib->Buyer_Info($uid);
			}

			$this->pageView->render_done_bonus();
		}
		else if( $addpicbtn != "" )
		{
			// Load photo and show add photo again
			if( isset($_FILES['pfile1']) && ($_FILES['pfile1']['name'] != "") && ($_FILES['pfile1']['tmp_name'] != "") )
			{
				$this->pageModel->addBonusPhoto($uid, $projid, $_FILES['pfile1']);
				$this->pageView->projpics = $this->catLib->Bonus_Pics($projid);
			}

			$this->pageView->render_done_bonus();
		}
		else
		{
			//$this->pageView->render_newfinish();
			//$this->pageModel->setProjectBackground($projid, $backpicid);
			$this->goToUrl( $this->pageView->Page_BuildUrl("bonus", "view/".$projid) );
			return;
		}
	}

	public function action_delmedia()
	{
		$uid = UhCmsApp::getSesInstance()->UserId;
		$projid = $this->getReqParam("projid", 0);
		$picid = $this->getReqParam("picid", 0);
		$projinfo = $this->catLib->Bonus_Info($projid);

		if( $projinfo['author_id'] != $uid )
		{
			$this->goToUrl(WWWHOST);
			return;
		}

		$this->pageModel->delBonusPhoto($picid, $projid);

		//$this->pageView->catmodel = $this->catLib;
		//$this->pageView->backpicid = ( isset($_SESSION['backpicid']) ? $_SESSION['backpicid'] : 0 );
		$this->pageView->projid = $projid;
		$this->pageView->projpics = $this->catLib->Bonus_Pics($projid);
		$this->pageView->userinfo = $this->catLib->Buyer_Info($uid);
		$this->pageView->render_done_bonus();
	}

	private function _getComVars()
	{
		$reg = Array();
		$reg['projid'] = $this->getReqParam("projid", 0);
		$req['action'] = $this->getReqParam("action", "");
		$reg['comment'] = $this->getReqParam("comment", "");

		return $reg;
	}

	private function _initComVars($projid)
	{
		$reg = Array();
		$reg['projid'] = $projid;
		$req['action'] = "";
		$reg['comment'] = "";

		return $reg;
	}

	public function action_addcomment()
	{
		$uid = $this->authAllow();

		$reqd = $this->_getComVars();

		$proj_id = 0;
		$proj_url = $reqd['projid'];

		// Check if this project exists
		$projinfo = $this->catLib->Bonus_Info($proj_url);
		if( $projinfo != null )
			$proj_id = $projinfo['id'];

		if( $proj_id == 0 )
			$this->go404();

		$this->pageModel->add_BonusComment($uid, $reqd);

		$this->pageView->commPopTip = $this->catLib->setCommentMoney($uid, $projinfo['id'], 1);
		header('Location:'.WWWHOST.'bonus/view/'.$proj_id);
	}

	public function action_view()
	{
		$proj_id = 0;

		if( count($this->urlparts)>0 )
		{
			$proj_url = $this->urlparts[0];

			// Check if this project exists
			$projinfo = $this->catLib->Bonus_Info($proj_url);
			if( $projinfo != null )
				$proj_id = $projinfo['id'];
		}

		if( $proj_id == 0 )
			$this->go404();

		$this->catLib->Bonus_UpdateRate($proj_id);

		// Find path to section and add it to breadcrumbs
		//$spath = $this->catLib->Catalog_SectPath($projinfo['sectid']);
		//$projinfo['currency'] = $this->catLib->Currency_Info($projinfo['currency_id']);

		$projinfo['city_data'] = $this->catLib->Loc_City($projinfo['city_id']);

		$projinfo['photos'] = $this->catLib->Bonus_Pics($projinfo['id']);


		// Fill data to pageView object
		//$this->pageView->spath = $spath;
		//$this->pageView->sid = $projinfo['sectid'];
		$this->pageView->projinfo = $projinfo;

		//$this->pageView->reqlist = $this->catLib->Item_ReqList($projinfo['id'], REQ_STATUS_CONFIRM, 0, 3);
		//$this->pageView->reqtotal = $this->catLib->Item_ReqCollected($projinfo['id'], REQ_STATUS_CONFIRM);
		$this->pageView->needhelpsects = $this->catLib->Buyer_ItemSects($projinfo['author_id'], PROJ_NEEDHELP);
		$this->pageView->comlist = $this->catLib->Bonus_Comments($projinfo['id'], true);
		$this->pageView->UserHash = UhCmsApp::getSesInstance()->UserHash;
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->UserAngel = $this->pageModel->getAngelDay($projinfo['author_id']);
		$user_info = $this->catLib->Buyer_Info($projinfo['author_id']);
		$user_info['helpmoney'] =  $this->catLib->Buyer_ReqAmountTotal($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL, PROJ_TYPE_MONEY);

		$user_info['helpnum'] =  $this->catLib->Buyer_ReqStarsListCount($user_info['id'])[0]['count'];
		$user_info['group_name'] = $this->catLib->Buyer_GroupInfo($user_info['account_type']);

		$this->pageView->UsrInfo = $user_info['helpmoney'];
		$this->pageView->UsrGroup = $user_info['group_name'];
		$this->pageView->AllUinfo = $user_info;
		$this->pageView->HelpNum = $user_info['helpnum'];

		$this->pageView->comlists = $this->catLib->Bonus_Comments($projinfo['id'], true);

		$user_info = $this->catLib->Buyer_Info($projinfo['author_id']);
		$user_info['group_name'] = $this->catLib->Buyer_GroupInfo($user_info['account_type']);
		$user_info['loc_info'] = $this->catLib->Loc_InfoByCity($user_info['city_id']);
		$user_info['helprate'] = $this->catLib->Buyer_ReqStarsRate($user_info['id']);
		$user_info['helpnum'] =  $this->catLib->Buyer_ReqList($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM, '',  -1, -1, PROJ_STATUS_ALL, 0, PROJ_STATUS_ALL, 1)[0]['count'];
		$user_info['helpmoney'] =  $this->catLib->Buyer_ReqAmountTotal($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL, PROJ_TYPE_MONEY);

		$this->pageView->user_info = $user_info;
		//echo '<pre>';
		//print_r();
		//echo '</pre>';

		// Init comment form values
		$reqd = $this->_initComVars($projinfo['id']);
		$formreq = new UhCmsFormData($reqd);

		//$this->pageView->catmodel = $this->catLib;

		// Render page
		$this->pageView->render_view_bonus($formreq);


	}
}
?>