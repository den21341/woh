<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("CAT_ITEMS_PERPAGE", 14);

define("CAT_MODE_NORMAL", "");
define("CAT_MODE_DREAM", "dream");
define("CAT_MODE_THING", "thing");

class Catdo extends PageController
{		
	protected $catLib;
	
	protected $sid;
	protected $surl;		// Current section url part
	protected $rurl;		// Root section url part
	protected $catmode;		// Control which section style is applied to opened section
	protected $catModel;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		$this->self_contolled = true;
		
		$this->sid = 0;
		$this->surl = "";
		$this->rurl = "";
		$this->catmode = "";
		$this->catModel = new CatdoModel($this->cfg, $this->db, $this->LangId);
	}
	
	// Method for routing in self controlled controllers
	public function handle_action($urlparts)
	{
		$only_by_arr = Array("success");
		$flt_by_arr = Array("money", "work");
		
		$sort_by = "";
		$flt_by = "";
		$city_by = "";
		$only_by = "";
		$pi = 0;
		$countryId = -1;
		$regionId = 0;

		if( $urlparts[0] == "" )
		{
			// Show root 
			//$this->action_default();
			$this->render_section(0, $sort_by, $flt_by, $city_by, $only_by, $pi);
		}
		else
		{		
			$sid = $this->catLib->Catalog_SectIdByUrl($urlparts[0]);
			if( $sid != 0 )
			{
				// url is correct and section found
				$this->surl = $urlparts[0];
				$this->sid = $sid;
				
				array_shift($urlparts);
			}
			
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^only_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 )
				{
					//echo "!!";
					$only_by = $matches[1];
					array_shift($urlparts);
				}
				
				//echo $only_by."<br>";
				
				if( !in_array($only_by, $only_by_arr) )
					$only_by = "";

				//if( $only_by == 0 )
				//	$only_by = "";
			}

			if(isset($urlparts[0]) && ($urlparts[0] != "")) {
				if(preg_match("/^country_([0-9a-zA-Z]+)$/", $urlparts[0], $matches) > 0) {
					$countryId = $matches[1];
					/*if( preg_match("/^p_([0-9]+)$/", $urlparts[1], $matches)>0 )  {
						$pi = $matches[1];
					}*/
					array_shift($urlparts);
				}
			}

			if(isset($urlparts[0]) && ($urlparts[0] != "")) {
				if(preg_match("/^region_([0-9a-zA-Z]+)$/", $urlparts[0], $matches) > 0) {
					$regionId = $matches[1];
					array_shift($urlparts);
				}
			}

			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^city_([0-9a-zA-Z]+)$/", $urlparts[0], $matches)>0 )
				{
					$city_by = $matches[1];
					array_shift($urlparts);
				}
			}

			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^flt_([a-zA-Z]+)$/", $urlparts[0], $matches) >0 )
				{
					$flt_by = $matches[1];
					array_shift($urlparts);
				}

				if( !in_array($flt_by, $flt_by_arr) )
					$flt_by = "";
			}


			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^sort_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 || preg_match("/^sort_([a-zA-Z]+)$/", $urlparts[1], $matches)>0)
				{
					$sort_by = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^p_([0-9]+)$/", $urlparts[0], $matches)>0 )
				{
					$pi = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( ($sid != 0) || ($sort_by != "") || ($flt_by != "") || ($city_by != "") || ($only_by != "") || ($pi > 0) || ($countryId > -1) || $regionId > 0)
			{
				//echo $only_by."<br>";
				if( $sid != 0 )
					$this->render_section($sid, $sort_by, $flt_by, $city_by, $only_by, $pi, $countryId, $regionId);
				else 
					$this->render_section($sid, $sort_by, $flt_by, $city_by, $only_by, $pi, $countryId, $regionId);
			}		
			else
			{
				$this->go404();			
			}		
		}		
	}

	public function render_section($sid, $sort_by="", $flt_by="", $city_by="", $only_by="", $pi=0, $countryId = -1, $regionId = 0)
	{
		$loc_cou = -1;
		$loc_reg = 0;
		$is_geo = 0;

		//var_dump($flt_by); die();

		if (UhCmsApp::getSesInstance()->UserId != 0) {
			$uinfo = $this->catLib->Buyer_Info(UhCmsApp::getSesInstance()->UserId);

			if ($uinfo['obl_id'] && $uinfo['city_id']) {
				$loc_cou = $this->catLib->Loc_GetCountryIdByCity($uinfo['city_id']);
				$loc_reg = $uinfo['obl_id'];
			} else
				$is_geo = 1;

		} else {
			$is_geo = 1;
		}

		if ($is_geo == 1) {
			$geo = new UhCmsGEO();
			if ($regionId == 0 || $countryId == -1) {
				$uip = $geo->getUserIP();
				$checkIP = $this->catLib->getUserIpInfo($uip); // $uip

				if(empty($checkIP)) {
					$IP_location = $geo->getUserLocation();
					$IP_reg = $IP_location['region']['name_ru'];
					$IP_counrty = $IP_location['country']['name_ru'];

					if ($IP_reg != '') {
						$loc_reg = $this->catModel->searchLocationDB($geo->locationStrCut($IP_reg));
						$loc_reg = empty($loc_reg) ? 0 : $loc_reg[0]['region_id'];

						if (empty($res)) {
							$loc_cou = $this->catModel->searchLocationDB($geo->locationStrCut($IP_counrty), 'country');
							$loc_cou = empty($loc_cou) ? -1 : $loc_cou[0]['country_id'];

							($loc_cou != -1 && $loc_reg != 0) ? $this->catLib->setUserIpInfo(0, $uip, $loc_cou, $loc_reg) : '';
						}
					}
				} else {
					$loc_reg = $checkIP[0]['user_reg'];
					$loc_cou = $checkIP[0]['user_country'];
				}
			}
		}

		$user_loc = ['country_id' => $loc_cou, 'reg_id' => $loc_reg];

		if($flt_by != 'loc') {
			$loc_reg = 0;
			$loc_cou = -1;
		}

		if( $sid != 0 )
		{
			// Find path to section and add it to breadcrumbs
			$spath = $this->catLib->Catalog_SectPath($sid);

			for( $i=(count($spath)-1); $i>=0; $i-- )
			{
				if( $i == (count($spath)-1) )
					$this->rurl = $spath[$i]['url'];

				$this->pageView->addBreadcrumbs($this->pageView->Catalog_BuildUrl($spath[$i]['url']), $spath[$i]['name']);
			}

			//echo $this->rurl."<br>";

			// Set the view mode for the catalog section 
			if( $this->rurl == "thing-market" )
				$this->catmode = CAT_MODE_THING;
			else if( $this->rurl == "fulfill-dream" )
				$this->catmode = CAT_MODE_DREAM;

			//echo $this->catmode."<br>";

			$subsectnum = $this->catLib->Catalog_SectSubNum(CAT_HELPGIVE_MODE, $sid);

			$slev = 3;
			if( count($spath) == 1 )
				$slev = 2;

			if( $pi > 0 )
				$pi--;

			/*if($pi == 0)
				$pi = 1;
			else if($pi > 0)
				$pi--;*/

			//print_r($pi);

			$sinf = ( $sid == 0 ? Array("id" => 0) : $this->catLib->Catalog_SectInfo($sid) );

			$this->pageView->spath = $spath;
			$this->pageView->surl = $this->surl;
			$this->pageView->rurl = $this->rurl;
			$this->pageView->sinf = ( $sid == 0 ? Array("id" => 0) : $this->catLib->Catalog_SectInfo($sid) );

			if( $sinf['id'] != 0 )
				$this->pageView->page_h1 = $sinf['name'];
		}
		else
		{
			if( $pi > 0 )
				$pi--;
			//print_r($pi);
			$slev = -1;

			$this->pageView->spath = Array();

			//echo $only_by."<br>";

			if( $only_by == "success" )
			{
				$this->pageView->page_h1 = "Успешные проекты";
			}
			else
			{
				$this->pageView->page_h1 = "Запросы о помощи";
			}

			//echo "!!";
		}

		//echo "ok";
		//return;

		// Fill data to pageView object		
		$this->pageView->sid = $sid;
		$this->pageView->catmode = $this->catmode;
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->pi = $pi;
		$this->pageView->pn = CAT_ITEMS_PERPAGE;
		$this->pageView->sortby = ( ($this->catmode == CAT_MODE_THING) && ($sort_by == "") ? "add" : $sort_by );
		$this->pageView->fltby = $flt_by;
		$this->pageView->onlyby = $only_by;
		$this->pageView->cityidby = $city_by;
		$this->pageView->countrylist = $this->catLib->Loc_CountryList(0, true, ( $sid == 0 ? "all" : $sid ));
		$this->pageView->countryId = $countryId;
		$this->pageView->regionId = $regionId;


		//$start = microtime(true);
		$this->pageView->its = $this->catLib->sm_Item_List(PROJ_SENDHELP, $sid, 0, $this->pageView->pi, $this->pageView->pn, $slev, $this->pageView->sortby, ($only_by != "" ? ($only_by == 'success' ? 'success' : PROJ_STATUS_ENDED) : PROJ_STATUS_RUN), $flt_by, $city_by, $only_by, $countryId, $regionId, UhCmsApp::getSesInstance()->UserId, $loc_reg, $loc_cou);
		$this->pageView->its_total = $this->catLib->Item_ListCount(PROJ_SENDHELP, $sid, 0, $slev, ($only_by != "" ? ($only_by == 'success' ? 'success' : PROJ_STATUS_ENDED) : PROJ_STATUS_RUN), $flt_by, $city_by, $only_by, $countryId, $regionId)[0]['count'];

		//$time = microtime(true) - $start;
		//printf('Скрипт выполнялся %.4F сек.', $time);

		$count_its = count($this->pageView->its);

		for( $i=0; $i<$count_its; ++$i ) {
			$this->pageView->its[$i]['locinfo'] = $this->catLib->Loc_InfoByCity($this->pageView->its[$i]['city_id']);
			$this->pageView->its[$i]['currency'] = $this->catLib->Currency_Info($this->pageView->its[$i]['currency_id']);
			if( $this->pageView->its[$i]['account_type'] == USR_TYPE_ORG )
				$this->pageView->its[$i]['photos'] = $this->catLib->Item_Pics($this->pageView->its[$i]['id'], 1);
		}

		if($only_by == 'success'){

			$this->pageView->render_successfull();
			return;

		}
		//$this->pageView->catmodel = $this->catLib;

		// Render page
		$this->pageView->render_default();
	}
	
	public function action_default()
	{
		//$uid = $this->authAllow();
		//$uid = UhCmsApp::getSesInstance()->UserId;

		$this->pageView->catmode = $this->catmode;
		$this->pageView->spath = Array();
		$this->pageView->sid = 0;
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->page_h1 = "Запросы о помощи";
		
		$this->pageView->pi = 0;
		$this->pageView->pn = CAT_ITEMS_PERPAGE;
		
		$slev = -1;
		
		$this->pageView->its_total = $this->catLib->Item_Num(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, 0, $slev);
		$this->pageView->its = $this->catLib->Item_List(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, 0, 0, CAT_ITEMS_PERPAGE, $slev, "add", PROJ_STATUS_RUN);

		for( $i=0; $i<count($this->pageView->its); $i++ )
		{
			$this->pageView->its[$i]['locinfo'] = $this->catLib->Loc_InfoByCity($this->pageView->its[$i]['city_id']);
			$this->pageView->its[$i]['currency'] = $this->catLib->Currency_Info($this->pageView->its[$i]['currency_id']);
			if( $this->pageView->its[$i]['account_type'] == USR_TYPE_ORG )
				$this->pageView->its[$i]['photos'] = $this->catLib->Item_Pics($this->pageView->its[$i]['id'], 1);
		}
		
		//$this->pageView->catmodel = $this->catLib;
		
		/*
		$uinfo = $clib->Buyer_Info($uid);
		$uinfo['group_name'] = $clib->Buyer_GroupInfo($uinfo['account_type']);		
		$this->pageView->userinfo = $uinfo;
		$this->pageView->catmodel = $this->catLib;
		$this->pageView->curl = '';
		*/
		// Render page
		$this->pageView->render_default();
	}
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['rlogin'] = $this->getReqParam("rlogin", "");
	
		return $reg;
	}
	
	private function _initRegvars() {
		$reg = Array();
		$reg['action'] = "";
		$reg['rlogin'] = "";
	
		return $reg;
	}

	public function action_successfull(){
		$this->pageView->render_successfull();
	}
	
}