<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Captcha extends Controller
{	
	protected $model;
	protected $view;
	protected $LangId;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		$this->LangId = UhCmsApp::getLangId();
						
		// Make include for View
		// echo "__CLASS__ = ".__CLASS__."<br />";		// Class name of current method = PageController
		// echo get_class()."<br>";						// Class name of current method = PageController
		// echo get_called_class()."<br />";			// Class name of decendant class = Main, ...
		//if( get_class() != get_called_class() )
		//{
			// Load page model source code
			$fileModel = $config['MVC_PATH']."models/captchaModel.class.php";
		
			//echo "Load model: ".$fileModel."<br />";
		
			if( !file_exists($fileModel) )
			{
				die('404 Not Found - Controller');
			}
			
			include_once $fileModel;
			
			// Load page view class source code
			$fileView = $config['MVC_PATH']."views/captchaView.class.php";
		
			//echo "Load view: ".$fileView."<br />";
		
			if( !file_exists($fileView) )
			{
				die('404 Not Found - Controller');
			}
			
			include_once $fileView;		

			$this->model = new CaptchaModel($this->getCfg(), $this->db, $this->LangId);
			$this->view = new CaptchaView($this->getCfg(), $this->model);
		//}	
	}
	
	public function action_default()
	{		
		$this->view->render_default();			
	}
	
	public function action_code()
	{
		$inm = $this->getReqParam("inm", "");
		
		$this->view->render_image($inm);
	}
}
?>