<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define('UPLOAD_DIR', 'coco/');

class Ajx extends Controller {
	protected $model;
	protected $view;
	protected $LangId;

	function __construct($config, $db) {
		parent::__construct($config, $db);

		$this->LangId = UhCmsApp::getLangId();

		// Make include for View
		// echo "__CLASS__ = ".__CLASS__."<br />";		// Class name of current method = PageController
		// echo get_class()."<br>";						// Class name of current method = PageController
		// echo get_called_class()."<br />";			// Class name of decendant class = Main, ...
		//if( get_class() != get_called_class() )
		//{
		// Load page model source code
		$fileModel = $config['MVC_PATH'] . "models/ajxModel.class.php";

		//echo "Load model: ".$fileModel."<br />";

		if (!file_exists($fileModel)) {
			die('404 Not Found - Controller');
		}

		include_once $fileModel;

		// Load page view class source code
		$fileView = $config['MVC_PATH'] . "views/ajxView.class.php";

		//echo "Load view: ".$fileView."<br />";

		if (!file_exists($fileView)) {
			die('404 Not Found - Controller');
		}

		include_once $fileView;

		$this->model = new AjxModel($this->getCfg(), $this->db, $this->LangId);
		$this->view = new AjxView($this->getCfg(), $this->model);
		//}
	}

	public function action_default() {

		//$this->model = new AjxModel($this->getCfg(), $this->getPageModel(), 1);
		//$this->model = $this->getPageModel();
		$this->view = new AjxView($this->getCfg(), null);

		$this->view->render_default();
	}

	public function action_resource() {
		$resname = $this->getReqParam("resname", "");

		$txt = $this->model->load_Res($resname);

		$this->view->render_html($txt);
	}

	public function action_dialog() {
		$dlg = $this->getReqParam("dlg", "");

		$pageModel = new PageModel($this->cfg, $this->db, $this->LangId);
		$this->view->pageModel = $pageModel;
		$this->view->catLib = new Catalog($this->db, $this->LangId);
		
		$this->view->load_popDialog($dlg);
	}

	public function action_login() {
		$ulogin = $this->getReqParam("ulogin", "");
		$upass = $this->getReqParam("upass", "");

		//echo "<br>".WWWHOST." = ".strlen(WWWHOST)."<br>".$_SERVER['HTTP_REFERER']."<br>";

		$res = Array("status" => "ok");

		if (strncmp($_SERVER['HTTP_REFERER'], WWWHOST, strlen(WWWHOST)) == 0) {
			// Allow request. It's sent from the website.
			//echo "!!!";
			UhCmsApp::getSesInstance()->AuthClear();
			$ses = UhCmsApp::getSesInstance();

			if ($ses->AuthMake($ulogin, $upass)) {
				//echo "Auth done!!!!<br>";
				$res['status'] = "ok";
			} else {
				//echo "Auth error<br>";
				$res['status'] = "error";
			}

			//echo $ses."<br>";
		} else {
			//echo "Other host";
			$res['status'] = "forbidden";
		}

		$this->view->render_json($res);
	}

	///////////////////////////////////////////////////////////////////////////
	// Graph data
	public function action_mytreedata() {
		$uid = $this->getReqParam("uid", 0);

		$UserId = UhCmsApp::getSesInstance()->UserId;

		$res = Array("status" => "error", "data" => null);

		if ($UserId != $uid) {
			$this->view->render_json($res);
			return;
		}

		$catLib = new Catalog($this->db, $this->LangId);

		$mylist = $this->model->make_TreeData($uid, $catLib);
		$mypic = $catLib->getUserPic($uid);

		$res['status'] = "ok";
		$res['mypic'] = $mypic[0]['pic_sm'] != '' ? $mypic[0]['pic_sm'] : 'img/no-pic.png';
		$res['data'] = $mylist;

		$this->view->render_json($res);
	}

	public function action_mygraphdata() {
		$uid = $this->getReqParam("uid", 0);

		$UserId = UhCmsApp::getSesInstance()->UserId;

		$res = Array("status" => "error", "data" => null);

		//if( $UserId != $uid )
		//{
		//	$this->view->render_json($res);
		//	return;
		//}

		$catLib = new Catalog($this->db, $this->LangId);

		$graphdata = $this->model->make_GraphData($uid, $catLib);

		$res['status'] = "ok";
		$res['data'] = $graphdata;

		$this->view->render_json($res);
	}

	public function action_graphsrv() {
		$uid = $this->getReqParam("uid", 0);

		$UserId = UhCmsApp::getSesInstance()->UserId;

		$res = Array("status" => "error", "data" => null);

		//if( $UserId != $uid )
		//{
		//	$this->view->render_json($res);
		//	return;
		//}

		$catLib = new Catalog($this->db, $this->LangId);

		$graphdata = $this->model->make_GraphData2($uid, 0, $catLib);

		$res['status'] = "ok";
		$res['data'] = $graphdata;

		$this->view->render_json($res);
	}

	public function action_graphapi() {
		//include "graphapi/api.php";
		$api_action = $this->getReqParam("request", "");

		$catLib = new Catalog($this->db, $this->LangId);

		$res = Array("status" => "error", "data" => null);

		switch ($api_action) {
			case "tree_main":
				$musrId = $this->getReqParam("user_id", 0);

				$res = $this->model->treeapi_treeMain($musrId, $catLib);

				//$res['status'] = "ok";
				//$res['data'] = $graphdata;
				break;

			// No more used
			case "tree_user_show":
			case "tree_user_load":
				$musrId = $this->getReqParam("gid", "u0");
				$userid = substr($musrId, 1);
				if ($userid != "0") {
					$res = $this->model->treeapi_treeUser($userid, $catLib);
				}
				break;
			// Up no more used

			case "node_project_info":
				$gid = $this->getReqParam("gid", "p0");

				$projid = substr($gid, 1);
				if ($projid != "0") {
					$res = $this->model->treeapi_projInfo($projid, $catLib);
				}
				break;

			case "node_mainuser_info":
				$gidmain = true;
			case "node_user_info":
				$gid = $this->getReqParam("gid", "u0");

				if (empty($gidmain))
					$gidmain = false;

				$userid = substr($gid, 1);
				if ($userid != "0") {
					$res = $this->model->treeapi_userInfo($userid, $catLib, $gidmain);
				}
				break;

			// New
			case "unfold_mn_usr":
				$gid = $this->getReqParam("gid", "u0");
				$pcount = $this->getReqParam("count", "5");
				$pskip = $this->getReqParam("skip", "5");

				$userid = substr($gid, 1);
				if ($userid != "0") {
					$res = $this->model->treeapi_userLoadMore($userid, true, $catLib, $pskip, $pcount, 1);
				}
				break;

			case "unfold_usr":
				$gid = $this->getReqParam("gid", "u0");
				$pcount = $this->getReqParam("count", "5");
				$pskip = $this->getReqParam("skip", "0");
				$plevel = $this->getReqParam("lvl", "2");

				$userid = substr($gid, 1);
				if ($userid != "0") {
					$res = $this->model->treeapi_userLoadMore($userid, false, $catLib, $pskip, $pcount, $plevel);
				}
				break;

			case "unfold_prj":
				$gid = $this->getReqParam("gid", "p0");
				$pcount = $this->getReqParam("count", "5");
				$pskip = $this->getReqParam("skip", "0");
				$plevel = $this->getReqParam("lvl", "2");

				$projid = substr($gid, 1);
				if ($projid != "0") {
					$res = $this->model->treeapi_projLoadMore($projid, false, $catLib, $pskip, $pcount, $plevel);
				}
				break;

			case "unfold_mn_prj":
				$gid = $this->getReqParam("gid", "u0");
				$pcount = $this->getReqParam("count", "5");
				$pskip = $this->getReqParam("skip", "0");
				$plevel = $this->getReqParam("lvl", "-1");

				$userid = substr($gid, 1);
				if ($userid != "0") {
					$res = $this->model->treeapi_projLoadMore($userid, true, $catLib, $pskip, $pcount, $plevel);
				}
				break;
		}

		$this->view->render_json($res);
	}

	//////////////////////////////////////////////////////////////////////////
	// Location access
	public function action_regionlist() {
		$countryid = $this->getReqParam("countryid", 0);
		$sectid = $this->getReqParam("sectid", 0);

		$catLib = new Catalog($this->db, $this->LangId);

		$res = Array("regions" => null);
		$res['regions'] = $catLib->Loc_OblList($countryid, $sectid);
		//print_r($res);

		$this->view->render_json($res);
	}

	public function action_citylist() {
		$oblid = $this->getReqParam("oblid", 0);
		$sectid = $this->getReqParam("sectid", 0);

		$catLib = new Catalog($this->db, $this->LangId);

		$res = Array("cities" => null);
		$res['cities'] = $catLib->Loc_CityList(0, $oblid, $sectid);

		$this->view->render_json($res);
	}

	//////////////////////////////////////////////////////////////////////////
	// Proj requests
	public function action_projreqs() {
		$projid = $this->getReqParam("projid", 0);
		$pi = $this->getReqParam("pi", 0);
		$pn = $this->getReqParam("pn", 0);
		$wh = $this->getReqParam('woh', 0);
		$currency = $this->getReqParam('currency', '');

		//echo $projid."<br>".$pi."<br>".;

		if ($pi > 0 && $pn > 0) {
			$catLib = new Catalog($this->db, $this->LangId);
			$reqlist = $catLib->Item_ReqList($projid, REQ_STATUS_CONFIRM, $pi, $pn);

			if ($wh) {
				$wohPays = $catLib->checkWohPays($projid);

				$wohPays[0]['author_id'] = $wohPays[0]['sender_id'];
				$reqlist[] = $wohPays[0];

				function cmp($a, $b)
				{
					return $a['daysbefore'] - $b['daysbefore'];
				}

				usort($reqlist, "cmp");
			}

			$html = $this->view->reqlist_tpl($reqlist, $currency);

			$this->view->render_html($html);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	//
	public function action_bonustree() {
		$uid = $this->getReqParam("uid", 0);
		$bid = $this->getReqParam("bid", 0);
		$sesid = $this->getReqParam("sesid", "");

		$bqnum = $this->model->findBonusQuizNum($uid, $bid);
		$check_new_hlp = $this->model->checkHelp($uid);
		$hlp_co = count($check_new_hlp);

		$res = Array("id" => 0, "num24" => $bqnum, "playlimit" => 0, "txt" => "", "reqid" => 0, "reqco" => $hlp_co);
		//$bqnum = 3;
		$hlps = 0;

		if ($bqnum < 3) {
			$bqid = $this->model->findBonusQuiz($uid, $bid, '', "run");

			if ($bqid !== false) {
				// quiz already exists
				$quizinfo = $this->model->readBonusQuiz($bqid);
				$res = $quizinfo;
			} else {
				$bqid = $this->model->findBonusQuiz($uid, $bid, '', "finished");

				if ($bqid !== false) {
					$hlps = 1;
					// wait 24 hours
					//$res = Array("id" => 0, "txt" => UhCmsApp::getLocalizerInstance()->get("popbonus", "timelimit"));
					//$res['txt'] = UhCmsApp::getLocalizerInstance()->get("popbonus", "timelimit");
				} else {
					// build new quiz and show to user
					$catLib = new Catalog($this->db, $this->LangId);
					$quizinfo = $this->model->buildNewQuiz($catLib, $uid, $bid, $sesid);
					$res = $quizinfo;
				}
			}
		} if($bqnum >= 3 || $hlps = 1) {
			$hlps = $this->model->getBonusHelp($uid, $bid);

			if(!empty($hlps)) {
				//
				$bqid = $this->model->findBonusQuiz($uid, $bid, '', "run");
				if ($bqid !== false) {
					// quiz already exists
					$quizinfo = $this->model->readBonusQuiz($bqid);
					$res = $quizinfo;
				} else {
					$catLib = new Catalog($this->db, $this->LangId);
					$quizinfo = $this->model->buildNewQuiz($catLib, $uid, $bid, $sesid);
					$res = $quizinfo;
				}
				//
				$res['reqid'] = $hlps[0]['req_id'];
			} else {

				if(!empty($check_new_hlp)) {
					$this->model->addBonusHelp($uid, $bid, $check_new_hlp[0]['id'], 'add');
					//
					$bqid = $this->model->findBonusQuiz($uid, $bid, '', "run");
					if ($bqid !== false) {
						// quiz already exists
						$quizinfo = $this->model->readBonusQuiz($bqid);
						$res = $quizinfo;
					} else {
						$catLib = new Catalog($this->db, $this->LangId);
						$quizinfo = $this->model->buildNewQuiz($catLib, $uid, $bid, $sesid);
						$res = $quizinfo;
					}
					//
					$res['reqid'] = $check_new_hlp[0]['id'];
				} else {
					$res['playlimit'] = 1;
					$res['txt'] = UhCmsApp::getLocalizerInstance()->get("popbonus", "timelimit");
				}
			}
		}

		/*else {
			// Play 3 times for last 24 hours
			// wait 24 hours
			//$res = Array("id" => 0, "txt" => UhCmsApp::getLocalizerInstance()->get("popbonus", "timelimit"));
			$res['playlimit'] = 1;
			$res['txt'] = UhCmsApp::getLocalizerInstance()->get("popbonus", "timelimit");
		}*/
		$res['reqco'] = $hlp_co;
		$this->view->render_json($res);
	}

	public function action_bonuscheck() {
		$uid = $this->getReqParam("uid", 0);
		$bid = $this->getReqParam("bid", 0);
		$quizid = $this->getReqParam("quizid", 0);
		$pointind = $this->getReqParam("pointind", 0);
		$sesid = $this->getReqParam("sesid", "");
		$reqid = $this->getReqParam("reqid", 0);

		//$catLib = new Catalog($this->db, $this->LangId);
		$res = $this->model->checkBonusPoint($uid, $bid, $quizid, $pointind, $sesid);

		if($reqid != 'undefined' && $reqid)
			$this->model->addBonusHelp($uid, $bid, $reqid);

		$this->view->render_json($res);
	}

	public function action_save() {

		$val = $this->getReqParam('val', 0);
		//var_dump($val);
		$this->view->render_json($val);
	}

	/****** Request for ajax ******/
	public function action_addCounter() {
		$catLib = new Catalog($this->db, $this->LangId);
		$id = $this->getReqParam("id", 0);
		$count = $this->getReqParam("co", 0);
		$count++;
		if ($catLib->Set_Video_Count($id, $count) == 1) {
			$this->view->render_json('done');
		} else {
			$this->view->render_json('error');
		}
	}

	/**** SET STAR RATING TO ITEM DREAM ****/

	public function action_setStars() {
		$catLib = new Catalog($this->db, $this->LangId);
		$uid = $this->getReqParam("uid", 0);
		$itemId = $this->getReqParam("itemId", 0);
		$rating = $this->getReqParam("rating", 0);
		$oldRate = $this->getReqParam("oldRate", 0);

		$res = $catLib->setRatingStars($uid, $itemId, $rating, $oldRate);

		$this->view->render_json($res); //CHANGE TO ARRAY boxRating
	}

	public function action_setRepost() {
		$catLib = new Catalog($this->db, $this->LangId);
		$uid = $this->getReqParam("uid", 0);
		$money = $this->getReqParam("money", 0);

		$res = $catLib->setMoneyBox($uid, $money, 'repost');

		$this->view->render_json($res);
	}

	public function action_runScript() {
		$uid = $this->getReqParam("uid", 0);
		$sum = $this->getReqParam("sum", 0);
		$catLib = new Catalog($this->db, $this->LangId);
		$catLib->resetMoneyBox($uid, 'portal');
		$catLib->resetMoneyBox($uid, 'star');
		$catLib->resetMoneyBox($uid, 'help');
		$catLib->resetMoneyBox($uid, 'comment');
		$catLib->resetMoneyBox($uid, 'repost');
		$catLib->resetMoneyBox($uid, 'ad');
		$catLib->resetMoneyBox($uid, 'aw');
		$catLib->resetMoneyBox($uid, 'bonus');

		if ($sum) $catLib->setDailyWeekMoney($uid, $sum);

		$this->view->render_json('done');
	}

	public function action_morenews() {
		$pi = $this->getReqParam("pi", 0);
		$pn = $this->getReqParam("pn", 0);

		$its = $this->model->News_Items(1, "add", 300, $pi, $pn);
		$html = $this->view->newslist_tpl($its, $pi, $pn);
		return $html;
	}

	public function action_readComment() {
		$uid = $this->getReqParam("uid", 0);
		$item = $this->getReqParam("item", 0);
		$catLib = new Catalog($this->db, $this->LangId);
		return $catLib->resetPoptipItemComment($item);
	}

	public function action_setpopup()
	{
		$uid = $this->getReqParam('uid', 0);
		$catLib = new Catalog($this->db, $this->LangId);
		return $catLib->checkShowPopup($uid, 0);
	}

	public function action_getgameproj() {
		$num = $this->getReqParam('num', 0);
		$to = $this->getReqParam('to', 0);
		$from = $this->getReqParam('from', 0);
		$date = $this->getReqParam('date', 0);
		$num = (!$num ? 1 : $num);

		$catLib = new Catalog($this->db, $this->LangId);
		$its = $catLib->getItemGame($date);

		//print_r($its);
		$html = $this->view->gameproj_tpl($its, $num, $date);
		return $html;
	}

	public function action_setusrhelp() {
		$uid = $this->getReqParam('uid', 0);
		$pid = $this->getReqParam('pid', 0);

		if ($uid != 0 && $pid != 0) {

		}

	}

	public function action_loadimg() {
		$img = $this->getReqParam('image', '');
		$uid = $this->getReqParam('uid', '');
		$ownid = $this->getReqParam('oid', '');

		$upload_dir = "img/tree_img/";
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);

		$file = $upload_dir . $uid . '_' . $ownid . "_scr.png";

		if (file_exists($file))
			unlink($file);

		$success = file_put_contents($file, $data);

		return $success;
	}

	public function action_setcomm() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		$comm = $this->getReqParam('comm', '');
		$reqid = $this->getReqParam('reqid', 0);
		$owner_id = $this->getReqParam('oid', 0);

		//echo $uid.'<br>';
		//echo $comm.'<br>';
		//echo $reqid.'<br>'; die();//

		if ($reqid && $comm && $uid) {
			$catLib = new Catalog($this->db, $this->LangId);
			$catLib->setReqComment($reqid, $uid, $comm);
			//$catLib->sendUserMailMsg_ct();
		}
		header("Location: " . WWWHOST);
	}

	public function action_setNextPopup() {
		$type = $this->getReqParam('type', -1);

		//echo $type.'<br>';

		if ($type != -1) {
			$catLib = new Catalog($this->db, $this->LangId);
			$res = $catLib->getPagesPopUp('proj/view/');

			//echo $res[0]['popup_cur'].'<br>';

			if ($res[0]['popup_cur'] == $type) {
				if ($type >= count($res))
					$catLib->setPagesPopUp('proj/view/', 1);
				else
					$catLib->setPagesPopUp('proj/view/', $type + 1);
			}

			$catLib->setPopUpInfo($type);
		}

	}

	// CHECK IF EMAIL ALREADY EXIST

	public function action_checkmail() {
		$email = $this->getReqParam('email', '');

		if ($email) {
			if ($this->model->checkLogin(trim($email)))
				echo 'true';
			else
				echo 'false';
		}
	}

	// AJAX SIMPLE REGISTRATION

	public function action_ajxreg() {
		$name = $this->getReqParam('regname', '');
		$fname = $this->getReqParam('regfname', '');
		$email = $this->getReqParam('regemail', '');
		$pass = $this->getReqParam('regpass', '');
		$redir = $this->getReqParam('redirto', '');

		if ($name && $fname && $email && $pass && $redir) {
			$catLib = new Catalog($this->db, $this->LangId);
			$this->model->smplAddPerson($name, $fname, $email, $pass, $catLib, $redir);
			return 'true';
		} return 'false';

	}	// AJAX LOAD TABLE FOR POPUP

	public function action_poptable() {
		$uid = $this->getReqParam('uid', 0);
		$urate = $this->getReqParam('urate', '');
		$type = $this->getReqParam('type', '');
		$to = $this->getReqParam('to', 30);

		if($uid && $type && $urate && $to) {
			$catLib = new Catalog($this->db, $this->LangId);
			$res = $catLib->getTableUserRate($uid, $urate, $type, $to);

			$html = $this->view->poptable_tpl($res, $urate);

			$this->view->render_html($html);
		}

	}
	
	public function action_addreqmoney() {
		$uid  = $this->getReqParam('uid', 0);
		$mon = $this->getReqParam('mon', 0);
		$type = $this->getReqParam('soc', '');

		if($uid && $mon && $type) {
			$this->view->render_json($this->model->addUserMoney($uid, $mon, $type));
		}
	}

	public function action_addclick() {
		$pid = $this->getReqParam('pid', 0);

		if($pid) {
			$this->model->addProjClick($pid);
		}
	}
	
	public function action_reindexviews() {
		$pids = $this->getReqParam('pids', 0);
		$pids = explode(',', $pids);

		$catLib = new Catalog($this->db, $this->LangId);

		foreach ($pids as $pid) {
			if(is_numeric($pid)) {
				$catLib->recalcItemViewRate($pid);
			}
		}
	}
	public function action_projcomms(){
		$projid = $this->getReqParam("projid", 0);
		$pi = $this->getReqParam("pi", 0);
		$pn = $this->getReqParam("pn", 0);

		if ($pi > 0 && $pn > 0) {
			$catLib = new Catalog($this->db, $this->LangId);
			$coms = $catLib->Item_Comments($projid, true, $pi, $pn);
			
			$html = $this->view->reqlist_comments($coms);

			$this->view->render_html($html);
		}
	}

	public function action_sharecount(){
		$projid = $this->getReqParam("projid", 0);
		$res = $this->model->getShareCount($projid);
		$this->view->render_json($res);
	}

	public function action_setinpsum() {
		$pid = $this->getReqParam('pid', 0);
		$sum = $this->getReqParam('sum', '');

		if($pid && $sum != '') {
			$this->model->setInpSum($pid, $sum);
		}
	}

	public function action_updtab() {
		$tab = $this->getReqParam('tab', '');
		$uid = UhCmsApp::getSesInstance()->UserId;

		if($tab != '' && $uid) {
			$this->model->updTabCounter($tab, $uid);
		}

	}

	public function action_sendbonmsg() {
		$bid = $this->getReqParam('bid', '');
		$catLib = new Catalog($this->db, $this->LangId);
		$uinfo = $catLib->Buyer_Info(UhCmsApp::getSesInstance()->UserId);

		if (!empty($uinfo) && $bid) {
			$bid = $catLib->Bonus_Info($bid);

			$txt = UhCmsApp::getLocalizerInstance()->get("bonus", "winmsg");
			$txt = str_replace('_name_', $uinfo['name'], $txt);
			$txt = str_replace('_fname_', $uinfo['fname'], $txt);
			$txt = str_replace('_hr_', WWWHOST.'users/viewrev/'.$uinfo['id'].'/', $txt);

			$catLib->sendUserMsg(FROM_ADMIN_ID, $bid['author_id'], $txt);
		}
	}

	public function action_loadmsgbord() {
		$mess_uid = $this->getReqParam('uid', '');

		//echo 'User = '.$mess_uid;

		if($mess_uid != '' && is_numeric($mess_uid)) {
			$msg_list = $this->model->loadMsgBord($mess_uid, UhCmsApp::getSesInstance()->UserId);
			$this->model->readMsgBord($mess_uid, UhCmsApp::getSesInstance()->UserId);

			$catLib = new Catalog($this->db, $this->LangId);
			$uinfo = $catLib->Buyer_Info($mess_uid);

			if(empty($msg_list)) {
				echo 'MESSAGES ARE EMPTY';
			} else
				$this->view->html_list_msg($msg_list, $uinfo['name'].' '.$uinfo['fname']);


		} else {
			echo 'error 21';
		}
	}

	public function action_updmsgbord() {
		$mess_uid = $this->getReqParam('uid', '');

		if($mess_uid != '' && is_numeric($mess_uid)) {
			$msg_list = $this->model->updMsgBord($mess_uid, UhCmsApp::getSesInstance()->UserId);
			$this->model->readMsgBord($mess_uid, UhCmsApp::getSesInstance()->UserId);

			$this->view->html_list_msg($msg_list);
		}
	}
	
	public function action_mainmsgbord() {
		$pi = $this->getReqParam('pi', 0);
		$pn = $this->getReqParam('pn', 0);

		$its = $this->model->msg_ListByProj(UhCmsApp::getSesInstance()->UserId, $pi, $pn);

		$this->view->html_mainlist_msg($its, new Catalog($this->db, $this->LangId));
	}

	public function action_addmsg() {
		$to_id = $this->getReqParam('to_id', 0);
		$msg_text = $this->getReqParam('msg', '');

		if(is_numeric($to_id) && $to_id && $msg_text) {

			$msg_text = strip_tags($msg_text, '<br>');

			$is_send = $this->model->msg_Send(UhCmsApp::getSesInstance()->UserId, $to_id, $msg_text);
			
			if( $is_send === true) {
				$msg_list = $this->model->loadMsgBord($to_id, UhCmsApp::getSesInstance()->UserId, 'last_u');

				$this->view->html_list_msg($msg_list);
			}
		}

	}

	public function action_upddt() {
		$UserId = UhCmsApp::getSesInstance()->UserId;
		$box_tip_mess = '';

		if ($UserId != 0) {

			$cat = new Catalog($this->db, $this->LangId);

			$cat->updDateBox($UserId, date("Y/m/d"));

			$bxr = new UhCmsBoxRating($this->db, $this->LangId);

			$boxData = $cat->getDateBox(UhCmsApp::getSesInstance()->UserId);

			$sumMoney = $boxData[0]['daily_money'] + $boxData[0]['portal_money'] + $boxData[0]['star_money']
				+ $boxData[0]['help_money'] + $boxData[0]['comment_money'] + $boxData[0]['repost_money']
				+ $boxData[0]['ad_money'] + $boxData[0]['aw_money'] + $boxData[0]['bonus_money'];

			if ($sumMoney) {
				//$this->catmodel->updDateBox($UserId, date('Y/m/d'));
				$box_tip_mess .= 'Ваша <b>Сила</b> за время последней сессии пополнилась на <b>' . $sumMoney . '</b> ' . $bxr->adaptCoin($sumMoney) . ': <br>';

				if ($boxData[0]['daily_money'])
					$box_tip_mess .= '<b>' . $boxData[0]['daily_money'] . '</b> ' . $bxr->adaptCoin($boxData[0]['daily_money']) . ' за ежедневные посещения.' . '<br>';
				if ($boxData[0]['portal_money']) {
					$box_tip_mess .= '<b>' . $boxData[0]['portal_money'] . '</b> ' . $bxr->adaptCoin($boxData[0]['portal_money']) . ' за посещение портала.' . '<br>';
					//$this->catmodel->resetMoneyBox($UserId, 'portal');
				}
				if ($boxData[0]['star_money']) {
					$box_tip_mess .= '<b>' . $boxData[0]['star_money'] . '</b> ' . $bxr->adaptCoin($boxData[0]['star_money']) . ' за оценку чужой мечты. <br>';
					//$this->catmodel->resetMoneyBox($UserId, 'star');
				}
				if ($boxData[0]['help_money']) {
					$box_tip_mess .= '<b>' . $boxData[0]['help_money'] . '</b> ' . $bxr->adaptCoin($boxData[0]['help_money']) . ' за оказанную помощь. <br>';
					//$this->catmodel->resetMoneyBox($UserId, 'help');
				}
				if ($boxData[0]['comment_money']) {
					$box_tip_mess .= '<b>' . $boxData[0]['comment_money'] . '</b>' . $bxr->adaptCoin($boxData[0]['comment_money']) . ' за комментарий. <br>';
				}
				if ($boxData[0]['repost_money']) {
					$box_tip_mess .= '<b>' . $boxData[0]['repost_money'] . '</b>' . $bxr->adaptCoin($boxData[0]['repost_money']) . ' за репост. <br>';
				}
				if ($boxData[0]['ad_money']) {
					$box_tip_mess .= '<b>' . $boxData[0]['ad_money'] . '</b>' . $bxr->adaptCoin($boxData[0]['ad_money']) . ' за статус <b>Ангел дня</b>. <br>';
				}
				if ($boxData[0]['aw_money']) {
					$box_tip_mess .= '<b>' . $boxData[0]['aw_money'] . '</b>' . $bxr->adaptCoin($boxData[0]['aw_money']) . ' за статус <b>Ангел недели</b>. <br>';
				}
				if ($boxData[0]['bonus_money']) {
					$box_tip_mess .= '<b>' . $boxData[0]['bonus_money'] . '</b>' . $bxr->adaptCoin($boxData[0]['bonus_money']) . ' за <b>подвешивание бонуса</b>. <br>';
				}
			}

			$this->view->render_json([$sumMoney, $box_tip_mess]);
		}
	}
	public function action_globusapi() {
		$api_action = $this->getReqParam("request", "");

		$main_dir = dirname(dirname(dirname(__FILE__)));

		//$js = file_get_contents($main_dir."/globe/names.json");
		//$this->nms = json_decode($js)->names;

		//$UserId = 1100;






		/*$this->nms = $this->model->globLoad_getnames($p_date);
		$this->nms = json_decode(json_encode($this->nms), FALSE);*/


		//$js = file_get_contents($main_dir."/globe/avas.json");
		//$avs = json_decode($js)->avas;
		//$ava_main = json_decode($js)->ava_main;





		//$js = file_get_contents($main_dir."/globe/positions.json");

		//$js_dec = json_decode($js);






		//$this->ll_pos = $js_dec->data;

		$prjTop = 15;
		$ndsTop = 5;
		$usrIdTop = 100;

		if($this->getReqParam('cnt_prj', 0) > 0)
			$prjTop = $_REQUEST["cnt_prj"];

		if($this->getReqParam('cnt_usr', 0) > 0)
			$ndsTop = $_REQUEST["cnt_usr"];

		if($this->getReqParam('max_uid', 0) > 0)
			$usrIdTop = $_REQUEST["max_uid"];

		$answ = array();
		$ansData = array();

		$sect_ind = 1;
		$this->sect_ind_by_id = [];

		switch($api_action) {
			case 'geo_main' :
				$co_req = $this->getReqParam('co_req', 0);
				$musrId = $this->getReqParam('user_id', 0);

				$this->glob_main_usr =
				$cat = new Catalog($this->db, $this->LangId);

				$sect0 = $cat->Catalog_SectLevel(0, 0);

				for( $i=0; $i<count($sect0); $i++ )
					$this->sect_ind_by_id[$sect0[$i]['id']] = $sect_ind++;

				$p_date = $this->model->globLoad_helpProjData($musrId); // Main data from DB for globus API

				$user_info = $cat->Buyer_Info($musrId);

				$this->avs = $this->model->globLoad_getavas($p_date);
				$this->ava_main = ($user_info['pic_sm'] ? $user_info['pic_sm'] : 'img/no-pic.png');

				$loc_data = $this->model->globLoad_getloation($p_date, $this->LangId);
				$loc_data = json_decode(json_encode($loc_data), FALSE);

				$this->ll_pos = (array)$loc_data->data;

				$answ["status"] = "ok";
				$ansData["settings"] = array();

				if (file_exists($main_dir . "/globe/sets.json")) {
					$st = file_get_contents($main_dir . "globe/sets.json");
					$star = json_decode($st);
				} else {
					$star = array();
					$star["graph_layout"] = "binary_tree";
					$star["show_avas"] = true;
				}


				$star = (array)$star;


				if (isset($star["graph_layout"]))
					$ansData["settings"]["graph_layout"] = $star["graph_layout"];
				else
					$ansData["settings"]["graph_layout"] = "binary_tree";

				if (isset($star["show_avas"]))
					$ansData["settings"]["show_avas"] = $star["show_avas"];
				else
					$ansData["settings"]["show_avas"] = false;

				if (isset($star["show_labels"]))
					$ansData["settings"]["show_labels"] = $star["show_labels"];
				else
					$ansData["settings"]["show_labels"] = false;

				$ansData["settings"]["debug_config"] = array(
					"cnt_prj" => $prjTop,
					"cnt_usr" => $ndsTop,
					"max_uid" => $usrIdTop);


				$ansData["request"] = $api_action;

				// main start node
				$ansData ["source_node"] = array();

				// data for vis.js
				$ansData ["source_node"]["gid"] = "mnu" . $musrId;
				// data for graph showing


				// data for user interaction and any other stuff

				$ansData ["source_node"]["info"] = array();

				$lprj = mt_rand(1, $prjTop);

				$ansData ["source_node"]["info"]["child_skip_prj"] = rand(10, 30);
				//(rand(1,10) <8)? rand(10,30) : -1;
				$ansData ["source_node"]["info"]["child_skip_usr"] = rand(10, 30);
				//(rand(1,10) <8)? rand(10,30) : -1;
				$ansData ["source_node"]["info"]["uname"] = $user_info['name'] . ' ' . $user_info['fname'];
				$ansData ["source_node"]["info"]["url_ava"] = $this->ava_main;

				$main_user_loc = $this->model->globLoad_getMainUserLoc($user_info['city_id'], $cat);

				$ansData ["source_node"]["info"]["iso2"] = $main_user_loc['code'];

				//echo $main_user_loc['lng'].'<br>';
				//echo $main_user_loc['lat']; die();

				//echo '30.52001953125';
				//echo '50.45196533203125';

				$ansData ["source_node"]["info"]["lng"] = (float)($main_user_loc['lng']);//;
				$ansData ["source_node"]["info"]["lat"] = (float)($main_user_loc['lat']);//;


				$ansNodes = array();
				$ansEdges = array();


				// create projects

				$ecnt = 1;

				//print_r($p_date); die();

				for ($i = 0; $i < 0; $i++) {

					$prj = array();

					// data for vis.js
					$prj["gid"] = "p" . $p_date[$i]['item_id'];

					// data for graph showing
					$prj["p_type"] = 'test__';
					$prj["nd_type"] = "prj";

					$prj["lvl"] = -1;


					// data for user interaction and any other stuff
					$prj["info"] = array();
					$prj["info"]["child_skip"] = (rand(1, 10) < 6) ? rand(10, 30) : -1;
//		$prj["info"]["more_show"] = (mt_rand ( 1 , 1000 ) < 500);
					$prj["info"]["title"] = "some title - ";
					$prj["info"]["content"] = "Lorem Ipsum content";
					$prj["info"]["url_view"] = WWWHOST . "proj/view/" . $p_date[$i]['item_id'] . "/";

					$ansNodes[] = $prj;

					$ed = array();

					// add connection between MAIN_USER and MY_PROJECT

					// data for vis.js

					$ed = $this->createEdge($prj["gid"], $ansData ["source_node"]["gid"], $prj["p_type"]);


					$ed['label'] = "получил помощь";

					$ansEdges[] = $ed;

					/*for ($h = 0; $h < count($p_date); ++$h) {

						$hlp = $this->createUser($p_date[$h]['user_id'], -2, false, false);

						$hlp["info"]["child_skip"] = -1;
						$hlp["lvl"] = -2;

						$ansNodes[] = $hlp;

						// data for vis.js

						$ansEdges[] = $this->createEdge($hlp["gid"], $prj["gid"], $prj["p_type"]);
					}*/


				}


				// build tree for user's help acceptors

				//$l1 = mt_rand ( 1 ,$ndsTop );

				$p_date_count = count($p_date);

				//$i = ($co_req-1)*10; $i < $co_req*10; ++$i

				for ($i = 0; $i < $p_date_count; ++$i) {

					$rootsid = $cat->Catalog_ItemRootSect($p_date[$i]['item_id']);

					$tcolor = ( ( isset($this->sect_ind_by_id[$rootsid]) ? $this->sect_ind_by_id[$rootsid] : 1 ) % 12) + 1;

					$acp = $this->createUser($p_date[$i]['user_id'], 1, true, false, $tcolor);

					$ansNodes[] = $acp;

					$ed = $this->createEdge($ansData ["source_node"]["gid"], "u" . $p_date[$i]['user_id'], $acp["p_type"]);
					$ed['label'] = "оказал помощь";

					$ansEdges[] = $ed;
				}

				//print_r($answ);
				$ansData["nodes"] = $ansNodes;
				$ansData["edges"] = $ansEdges;
				$ansData["co_req"] = ++$co_req;
				$answ["data"] = $ansData;

				$json = json_encode($answ);
				echo $json;

				break;

			case 'node_project_info':
				$answ = array("status" => "ok");

				$gid = $this->getReqParam('gid', '');

				$ansData["request"] = $api_action;

				$ansData["source_node"] = array();
				$ansData["source_node"]["gid"] = $gid;
				$ansData["content"] = "some content about project - " . $gid;

				$answ["data"] = $ansData;

				$json = json_encode($answ);
				echo $json;
				exit;
				break;

			case 'geo_country_info' :
				$answ = array("status"=>"ok");
				$uid = $this->getReqParam('gid', 0);
				$code = $this->getReqParam('iso2', '');

				$ansData["request"] = $api_action;

				$country_info = $this->model->globLoad_countryinfo(strtolower($code), $uid);

				//echo '<pre>';
				//print_r($country_info); die();
				
				$ansData["content"] = $this->view->globView_countryinfo($country_info);
				

				$answ["data"] = $ansData;

				$json = json_encode($answ);
				echo $json;
				exit;
				break;

			case 'geo_user_info_help' :
				$answ = array("status"=>"ok");
				$uid = $this->getReqParam('gid', 0);
				$main_id = $this->getReqParam('main_id', 0);
				$uid = substr($uid, 1);

				$cat = new Catalog($this->db, $this->LangId);

				$subu_info = $this->model->globLoad_userdata($uid, $cat);
				$subu_item = $this->model->globLoad_useritem($main_id, $uid);

				//echo '<pre>';
				//print_r($subu_info);
				//print_r($subu_item);

				$its['country'] = $subu_info['loc_info']['countryname'];
				$its['city'] = $subu_info['loc_info']['name'];
				$its['title'] = $subu_item[0]['title2'];
				$its['id'] = $subu_info['user_info']['id'];
				$its['item_id'] = $subu_item[0]['item_id'];
				$its['fulltxt'] = $subu_item[0]['descr0'];
				$its['name'] = $subu_info['user_info']['name'].' '.$subu_info['user_info']['fname'];
				$its['u_pic'] = ($subu_info['user_info']['pic_sm'] != null ? $subu_info['user_info']['pic_sm'] : 'img/no-pic.png');
				$its['p_pic'] = ($subu_item[0]['filename_thumb'] != null ? $subu_item[0]['filename_thumb'] : 'img/no-pic.png');

				$its['title'] = strip_tags($its['title'], '');
				if( mb_strlen($its['title']) > 31 ) {
					$its['title'] = mb_substr($its['title'],0,29)."...";
				}

				$its['fulltxt'] = strip_tags($its['fulltxt'], '');
				if( mb_strlen($its['fulltxt']) > 320 ) {
					$its['fulltxt'] = mb_substr($its['fulltxt'],0,328)."...";
				}

				$ansData["content"] = $this->view->globView_userinfo($its);

				$answ["data"] = $ansData;

				$json = json_encode($answ);
				echo $json;
				exit;

				break;

			case 'geo_user_info_main' :
				$answ = array("status"=>"ok");
				$uid = $this->getReqParam('gid', 0);

				$cat = new Catalog($this->db, $this->LangId);

				$user_info = $this->model->globLoad_userdata($uid, $cat, 'help');

				$ansData["request"] = $api_action;

				//print_r($user_info); die();

				$its['country'] = $user_info['loc_info']['countryname'];
				$its['city'] = $user_info['loc_info']['name'];
				$its['u_pic'] = ($user_info['user_info']['pic_sm'] != null ? $user_info['user_info']['pic_sm'] : 'img/no-pic.png' );
				$its['name'] = $user_info['user_info']['name'].' '.$user_info['user_info']['fname'];
				$its['uid'] = $user_info['user_info']['id'];
				$its['numx'] = $user_info['user_help']['country_count'];
				$its['numy'] = $user_info['user_help']['all_country'];
				$its['money_help_co'] = $cat->Buyer_ReqAmountTotal($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL, PROJ_TYPE_MONEY)['totnum'];//$cat->Buyer_ReqAmountTotal($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL)['totnum'];
				$its['work_help_co'] = 	($cat->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM, '',  -1, -1, PROJ_STATUS_ALL, 0, PROJ_STATUS_ALL, 1)[0]['count'] - $cat->Buyer_ReqAmountTotal($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL, PROJ_TYPE_MONEY)['totnum']);

				$ansData["content"] = $this->view->globView_mainUserInfo($its);

				//$ansData["content"] = $user_info['user_info']['name']." ".$user_info['user_info']['fname'];

				$answ["data"] = $ansData;

				$json = json_encode($answ);
				echo $json;
				exit;
				break;

			case 'geo_projects_list' :
				$answ = array("status"=>"ok");

				$ansData ["request"] = $api_action;

				$js = file_get_contents($main_dir."/globe/cities.json");
				$country_cities = json_decode($js)->countries;

				$prjs = array();

				for($i = 0; $i < 20; $i++){
					$prj = array();
					$prj["gid"] = rand(1,100002);
					$prj["p_type"] = "t".rand(1,12);

					$prj["status"] = rand(1,12) > 7 ? "completed":"available";

					$tcntr = rand ( 1 , count($country_cities))-1;

					$tcity  = rand ( 1 , count($country_cities[$tcntr]->cities))-1;

					$prj["iso2"] = $country_cities[$tcntr]->iso2;
					$prj["lng"] = $country_cities[$tcntr]->cities[$tcity][0];
					$prj["lat"] = $country_cities[$tcntr]->cities[$tcity][1];

					$prjs[] = $prj;

				}

				$ansData["projects"] = $prjs;

				$answ["data"] = $ansData;

				$json = json_encode($answ);
				echo $json;
				exit;
				break;
		}

	}

	private function createEdge($from, $to, $p_type) {

		$ed = array();
		$ed['from'] = $from;
		$ed['to'] = $to;

		$utc = mt_rand(1060070279, time());
		$ed["info"] = array(
			"time_date" => date("Y-m-d\TH:i:s", $utc),
			//	"title" => "some info about doing help"
		);
		$ed["p_type"] = $p_type;

		return $ed;

	}

	private function createProject($gid, $mrShow) {
		// data for vis.js
		$prj["gid"] = $gid;

		// data for graph showing
		$prj["p_type"] =  $this->arTp[ rand ( 0 , count($this->arTp) -1 ) ];
		$prj["nd_type"] = "prj";
		$prj["lvl"] = -1;

		// data for user interaction and any other stuff
		$prj["info"] = array();
		$prj["info"]["title"] = "some title - ".$gid;
		$prj["info"]["content"] = "Lorem Ipsum content";
		$prj["info"]["url_view"] = WWWHOST."proj/view/". mt_rand ( 100 , 1000 ) . "/";

		return $prj;

	}

	private function createUser($gid, $lvl, $mrShow, $mrLoad, $p_type = null) {
		$acp = array();
		$acp["nd_type"] = "usr";

		$cat = new Catalog($this->db, $this->LangId);
		$uinfo = $cat->Buyer_Info($gid);

		if ($p_type != null)
			$acp["p_type"] = 't'.$p_type;

		$acp["gid"] = "u" . $gid;

		$acp["lvl"] = $lvl;

		$acp["size"] = 25;//mt_rand ( 10, 30 );

		$loc_info = $this->model->globLoad_getMainUserLoc($uinfo['city_id'], $cat);

		$acp["info"] = array("uname" => $uinfo['name'].' '.$uinfo['fname'],
			"child_skip" => (rand(1, 14) > 6) ? rand(6, 24) : -1,

			"title" => "accceptor",
			"ava_pos" => $uinfo['pic_sm'],
			"url_ava" => WWWHOST.$uinfo['pic_sm'],
			"url_view" => WWWHOST."users/viewinfo/" . $gid . "/",

			"iso2" => strtoupper($loc_info['code']),
			"lat" => (float)($loc_info['lat']),
			"lng" => (float)($loc_info['lng'])

		);

		$acp["info"]["url_ava"] = WWWHOST.$uinfo['pic_sm'];

		return $acp;

	}

}