<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Users extends PageController
{
	protected $usr;

	function __construct($config, $db)
	{
		parent::__construct($config, $db);

		//$this->self_contolled = true;	
		//$this->catLib = new Catalog($this->db, $this->LangId);
		$this->usr = new UsersModel($this->cfg, $this->db, $this->LangId);
	}

	private function authAllow() {
		$uid = UhCmsApp::getSesInstance()->UserId;
		if( $uid == 0 )
		{
			header("Location: ".WWWHOST);
			exit();
		}

		return $uid;
	}

	protected function _prepare_info($user_info, $myId) {
		$user_info['group_name'] = $this->catLib->Buyer_GroupInfo($user_info['account_type']);
		$user_info['helprate'] = $this->catLib->Buyer_ReqStarsRate($user_info['id']);
		$user_info['helpnum'] =  $this->catLib->Buyer_ReqList($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM, '',  -1, -1, PROJ_STATUS_ALL, 0, PROJ_STATUS_ALL, 1)[0]['count'];
		$user_info['helpmoney'] =  $this->catLib->Buyer_ReqAmountTotal($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL, PROJ_TYPE_MONEY);
		$user_info['getmoney'] =  $this->catLib->Buyer_ReqAmountTotal($user_info['id'], PROJ_NEEDHELP, REQ_STATUS_CONFIRM, PROJ_STATUS_ALL, PROJ_TYPE_MONEY);

		$user_info['loc_info'] = $this->catLib->Loc_InfoByCity($user_info['city_id']);

		/* User box */
		$this->pageView->box = $this->usr->getUserBox($user_info['id']);
		//$this->pageView->pics = $this->usr->getUserItemPics($);
		/***********/

		$this->pageView->userinfo = $user_info;
		$this->pageView->reqlist = $this->catLib->Buyer_ReqList($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM, "", 0, 10, PROJ_STATUS_ALL, 0);
		$this->pageView->needhelpsects = $this->catLib->Buyer_ItemSects($user_info['id'], PROJ_NEEDHELP);
		$this->pageView->dohelpsects = $this->catLib->Buyer_Sects($user_info['id'], true);
		$this->pageView->UserId = $myId;
		$this->pageView->helpcount = $this->catLib->getBuyerReqNum($user_info['id'], 1)[0]['hlpco'];

		//RENDER IMAGES FOR SHARE
		//$start = microtime(true);
		UhCmsImage::renderShareImgVK($user_info, $this->pageView->helpcount);
		UhCmsImage::renderShareImgFB($user_info, $this->pageView->helpcount);
		UhCmsImage::renderShareImgOK($user_info, $this->pageView->helpcount);

		//$time = microtime(true) - $start;
		//printf('Скрипт выполнялся %.4F сек.', $time);

		//$this->pageView->catmodel = $this->catLib;
	}

	public function action_viewinfo()
	{
		$user_id = 0;

		if( count($this->urlparts)>0 )
		{
			$user_url = $this->urlparts[0];

			// Check if this project exists
			$user_info = $this->catLib->Buyer_Info($user_url);
			if( $user_info != null )
				$user_id = $user_info['id'];
		}

		if( $user_id == 0 )
			$this->go404();

		$this->_prepare_info($user_info, UhCmsApp::getSesInstance()->UserId);

		$this->pageView->projlist = $this->catLib->Item_List(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, $user_info['id'], -1, 20, 3, "", PROJ_STATUS_ALL, "", "", true, -1, 0, 1, 0, -1);
		$this->pageView->req = $this->getReqParam('req', 0);
		// Render page
		$this->pageView->render_default();
	}

	public function action_viewrev() {
		$urlPath = $_SERVER['REDIRECT_URL'];
		$perPage = REVIEWS_PER_PAGE;

		if( preg_match("/p_([0-9]+)/", $urlPath, $matches )) {
			$matches = $matches[1];
		}

		!empty($matches) ? $matches : $matches = 1;
		$to = $perPage * $matches;
		$from = $to - $perPage;

		$user_id = 0;

		if( count($this->urlparts)>0 ) {
			$user_url = $this->urlparts[0];

			// Check if this project exists
			$user_info = $this->catLib->Buyer_Info($user_url);
			if( $user_info != null )
				$user_id = $user_info['id'];
		}

		if( $user_id == 0 )
			$this->go404();

		$this->_prepare_info($user_info, UhCmsApp::getSesInstance()->UserId);

		//$this->pageView->revlist = $this->catLib->Buyer_ReqStarsList($user_info['id']);
		$this->pageView->revlist = $this->catLib->Buyer_ReqList($user_info['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);
		$this->pageView->to = $to;
		$this->pageView->from = $from;
		$this->pageView->pageId = $matches;
		$this->pageView->perPage = $perPage;
		$this->pageView->req = $this->getReqParam('req', 0);

		// Render page

		$this->pageView->render_reviews();
	}

	public function action_default() {
		$this->go404();
	}

	public function getAdaptTit($numb) {
		$numb = $numb % 10;

		if($numb == 1)
			return array('Доброе', 'дело');
		else if($numb >= 2 && $numb <= 4)
			return array('Добрых', 'дела');
		else if(($numb >= 5 && $numb <= 9) || $numb == 0)
			return array('Добрых', 'дел');

		return array('', '');
	}

	public function renderShareImgVK($user_info) {
		$img = new UhCmsImage();
		$img->create(600, 268, true);

		$img2 = new UhCmsImage(WWWHOST.'images/woh_vk2.jpg');
		$img2->addText($user_info['name'].' '.$user_info['fname'], 'fonts/UbuntuBold.ttf', 11, 0, array(141, 247, 151), array(425, 30));
		$img2->addText($this->pageView->helpcount, 'fonts/UbuntuBold.ttf', 30, 0, array(255, 255, 255), array(
			strlen($this->pageView->helpcount) == 1 ? 545 : (strlen($this->pageView->helpcount) == 3 ? 520 : 530) ,
			140));

		$img2->addText($this->getAdaptTit($this->pageView->helpcount)[0], 'fonts/UbuntuRegular.ttf', 14, 0, array(255, 255, 255), array(520, 170));
		$img2->addText($this->getAdaptTit($this->pageView->helpcount)[1], 'fonts/UbuntuRegular.ttf', 14, 0, array(255, 255, 255), array(537, 190));
		$img->merge($img2, 0, 0);

		$img3 = new UhCmsImage(WWWHOST.($user_info['pic'] && file_exists(WWWHOST.$user_info['pic'])) ? $user_info['pic'] : 'img/no-pic.png');
		$img3->imageresize(0.8);
		$img3->circleCrop();
		$img3->cut(0, 22);

		$img->merge($img3, 56, 134);

		//$img->render();
		$img->saveImage('vk', $user_info['id']);
	}

	public function renderShareImgFB($user_info) {
		$img = new UhCmsImage();
		$img->create(486, 255, true);

		$img2 = new UhCmsImage(WWWHOST.'images/woh_fb.jpg');
		$img2->addText($user_info['name'].' '.$user_info['fname'], 'fonts/UbuntuBold.ttf', 10, 0, array(141, 247, 151), array(340, 25));
		$img2->addText($this->pageView->helpcount, 'fonts/UbuntuBold.ttf', 26, 0, array(255, 255, 255), array(
			strlen($this->pageView->helpcount) == 1 ? 440 : (strlen($this->pageView->helpcount) == 3 ? 420 : 430) ,
			145));
		$img2->addText($this->getAdaptTit($this->pageView->helpcount)[0], 'fonts/UbuntuBold.ttf', 12, 0, array(255, 255, 255), array(420, 170));
		$img2->addText($this->getAdaptTit($this->pageView->helpcount)[1], 'fonts/UbuntuBold.ttf', 12, 0, array(255, 255, 255), array(435, 190));
		$img->merge($img2, 0, 0);

		$img3 = new UhCmsImage(WWWHOST.($user_info['pic'] && file_exists(WWWHOST.$user_info['pic'])) ? $user_info['pic'] : 'img/no-pic.png');
		$img3->imageresize(0.66);
		$img3->circleCrop();

		$img->merge($img3, 50, 122);

		//$img->render();
		$img->saveImage('fb', $user_info['id']);
	}

	public function renderShareImgOK($user_info) {
		$img = new UhCmsImage();
		$img->create(176, 172, true);

		$img2 = new UhCmsImage(WWWHOST.'images/woh_ok.jpg');
		$img2->addText($user_info['name'].' '.$user_info['fname'], 'fonts/UbuntuBold.ttf', 6, 0, array(141, 247, 151), array(74, 14));
		$img2->addText($this->pageView->helpcount, 'fonts/UbuntuBold.ttf', 16, 0, array(255, 255, 255), array(
			strlen($this->pageView->helpcount) == 1 ? 151 : (strlen($this->pageView->helpcount) == 3 ? 138 : 146) ,
			110));
		$img2->addText($this->getAdaptTit($this->pageView->helpcount)[0], 'fonts/UbuntuRegular.ttf', 7, 0, array(255, 255, 255), array(140, 122));
		$img2->addText($this->getAdaptTit($this->pageView->helpcount)[1], 'fonts/UbuntuRegular.ttf', 7, 0, array(255, 255, 255), array(144, 135));
		$img->merge($img2, 0, 0);

		$img3 = new UhCmsImage(WWWHOST.($user_info['pic_sm'] && file_exists(WWWHOST.$user_info['pic_sm'])) ? $user_info['pic_sm'] : 'img/no-pic.png');
		$img3->imageresize(0.80);
		$img3->circleCrop();
		$img3->cut(0, 10);

		$img->merge($img3, 11, 118);

		//$img->render();
		$img->saveImage('ok', $user_info['id']);
	}
}