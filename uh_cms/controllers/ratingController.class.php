<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Rating extends PageController
{		
	protected $addmode;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		$this->addmode = "";
	
		//$this->self_contolled = true;
	
		//$this->catLib = new Catalog($this->db, $this->LangId);
	}
	
	// Check if this page is allowed to unauthorized user
	// If not, then go to website start page
	private function authAllow()
	{
		$uid = UhCmsApp::getSesInstance()->UserId;
		if( $uid == 0 )
		{
			header("Location: ".$this->pageView->page_BuildUrl("registration","person"));
			exit();
		}
		
		return $uid;
	}
	
	public function action_view()
	{
		$proj_id = 0;
		
		if( count($this->urlparts)>0 )
		{
			$proj_url = $this->urlparts[0];
			
			// Check if this project exists
			$projinfo = $this->catLib->Item_Info($proj_url);
			if( $projinfo != null )
				$proj_id = $projinfo['id'];
		}
		
		if( $proj_id == 0 )
			$this->go404();
		
		// Fill data to pageView object
		$this->pageView->spath = $spath;
		$this->pageView->sid = $projinfo['sectid'];
		$this->pageView->projinfo = $projinfo;
		$this->pageView->comlist = $this->catLib->Item_Comments($projinfo['id'], true);
		$this->pageView->UserHash = UhCmsApp::getSesInstance()->UserHash;
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		
		// Init comment form values
		$reqd = $this->_initComVars($projinfo['id']);
		$formreq = new UhCmsFormData($reqd);
		
		//$this->pageView->catmodel = $this->catLib;
		
		// Render page
		$this->pageView->render_default($formreq);
	}
	
	public function action_default()
	{
		//$this->catLib = new Catalog($this->db, 1);
		//$uid = $this->authAllow();
		$uid = UhCmsApp::getSesInstance()->UserId;	
		
		//$uinfo = $this->catLib->Buyer_Info($uid);
		$this->pageView->tlist = $this->pageModel->getRateTableData();
		for( $i=0; $i<count($this->pageView->tlist); $i++ )
		{
			$this->pageView->tlist[$i]['it_picnum'] = ($this->catLib->Item_PicsNum($this->pageView->tlist[$i]['id']) > 0 ? 1 : 0);
			
			$this->pageView->tlist[$i]['help_num'] = $this->catLib->Buyer_ReqNum($this->pageView->tlist[$i]['author_id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);
			$this->pageView->tlist[$i]['help_rate'] = $this->catLib->Buyer_ReqStarsRate($this->pageView->tlist[$i]['author_id']);			
		}
		//$uinfo['group_name'] = $this->catLib->Buyer_GroupInfo($uinfo['account_type']);		
		//$this->pageView->userinfo = $uinfo;
		//$this->pageView->catmodel = $this->catLib;
		$this->pageView->curl = '';
		$this->pageView->pageInfo = $this->pageModel->getPageData(16);
		$this->pageView->render_default();
	}
	
	private function _getComVars()
	{
		$reg = Array();
		$reg['projid'] = $this->getReqParam("projid", 0);
		$req['action'] = $this->getReqParam("action", "");		
		$reg['comment'] = $this->getReqParam("comment", "");
		
		return $reg;
	}
	
	private function _initComVars($projid)
	{
		$reg = Array();
		$reg['projid'] = $projid;
		$req['action'] = "";
		$reg['comment'] = "";
	
		return $reg;
	}
}
?>