<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("CAT_ITEMS_PERPAGE", 14);

define("CAT_MODE_NORMAL", "");
define("CAT_MODE_DREAM", "dream");
define("CAT_MODE_THING", "thing");

class Cat extends PageController
{		
	protected $catLib;
	
	protected $sid;
	protected $surl;		// Current section url part
	protected $rurl;		// Root section url part
	protected $catmode;		// Control which section style is applied to opened section
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		$this->self_contolled = true;
		
		$this->sid = 0;
		$this->surl = "";
		$this->rurl = "";
		$this->catmode = "";		
	}
	
	// Method for routing in self controlled controllers
	public function handle_action($urlparts)
	{
		$sort_by = "";
		$flt_by = "";
		$city_by = "";
		$pi = 0;
		
		//var_dump($urlparts);
		//echo "<Br>";
		
		if( $urlparts[0] == "" )
		{
			// Show root 
			//$this->action_default();
			$this->render_section(0, $sort_by, $flt_by, $city_by, $pi);
		}
		else
		{		
			$sid = $this->catLib->Catalog_SectIdByUrl($urlparts[0]);
			if( $sid != 0 )
			{
				// url is correct and section found
				$this->surl = $urlparts[0];
				$this->sid = $sid;
				
				array_shift($urlparts);
			}
								
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^sort_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 )
				{
					$sort_by = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^flt_([a-zA-Z]+)$/", $urlparts[0], $matches)>0 )
				{
					$flt_by = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^city_([0-9a-zA-Z]+)$/", $urlparts[0], $matches)>0 )
				{
					$city_by = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( isset($urlparts[0]) && ($urlparts[0] != "") )
			{
				if( preg_match("/^p_([0-9]+)$/", $urlparts[0], $matches)>0 )
				{
					$pi = $matches[1];
					array_shift($urlparts);
				}
			}
			
			if( ($sid != 0) || ($sort_by != "") || ($flt_by != "") || ($city_by != "") || ($pi > 0) )
			{
				if( $sid != 0 )
					$this->render_section($sid, $sort_by, $flt_by, $city_by, $pi);
				else 
					$this->render_section($sid, $sort_by, $flt_by, $city_by, $pi);
			}		
			else
			{
				$this->go404();			
			}		
		}		
	}
	
	public function render_section($sid, $sort_by="", $flt_by="", $city_by="", $pi=0)
	{
		if( $sid != 0 )
		{
			// Find path to section and add it to breadcrumbs
			$spath = $this->catLib->Catalog_SectPath($sid);
	
			for( $i=(count($spath)-1); $i>=0; $i-- )
			{
				if( $i == (count($spath)-1) )
					$this->rurl = $spath[$i]['url'];
				
				$this->pageView->addBreadcrumbs($this->pageView->Catalog_BuildUrl($spath[$i]['url']), $spath[$i]['name']);
			}
			
			//echo $this->rurl."<br>";
			
			// Set the view mode for the catalog section 
			if( $this->rurl == "thing-market" )
				$this->catmode = CAT_MODE_THING;
			else if( $this->rurl == "fulfill-dream" )
				$this->catmode = CAT_MODE_DREAM;
			
			//echo $this->catmode."<br>";
				
			$subsectnum = $this->catLib->Catalog_SectSubNum($sid);
			
			$slev = 3;
			if( count($spath) == 1 )
				$slev = 2;
			
			if( $pi > 0 )
				$pi--;
			
			$sinf = ( $sid == 0 ? Array("id" => 0) : $this->catLib->Catalog_SectInfo($sid) );
			
			$this->pageView->spath = $spath;
			$this->pageView->surl = $this->surl;
			$this->pageView->rurl = $this->rurl;
			$this->pageView->sinf = ( $sid == 0 ? Array("id" => 0) : $this->catLib->Catalog_SectInfo($sid) );
			
			if( $sinf['id'] != 0 )
				$this->pageView->page_h1 = $sinf['name'];
		}
		else 
		{
			$slev = -1;
			
			$this->pageView->spath = Array();
			$this->pageView->page_h1 = "Запросы о помощи";
			
			//echo "!!";
		}
		
		//echo "ok";
		//return;
		
		// Fill data to pageView object		
		$this->pageView->sid = $sid;
		$this->pageView->catmode = $this->catmode;		
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->pi = $pi;
		$this->pageView->pn = CAT_ITEMS_PERPAGE;
		$this->pageView->sortby = ( ($this->catmode == CAT_MODE_THING) && ($sort_by == "") ? "add" : $sort_by );
		$this->pageView->fltby = $flt_by;
		$this->pageView->cityidby = $city_by;
		$this->pageView->countrylist = $this->catLib->Loc_CountryList(0, true, ( $sid == 0 ? "all" : $sid ));
		
		$this->pageView->its_total = $this->catLib->Item_Num(PROJ_ANY, $sid, 0, $slev, $this->pageView->sortby, PROJ_STATUS_RUN, $flt_by, $city_by);
		$this->pageView->its = $this->catLib->Item_List(PROJ_ANY, $sid, 0, $this->pageView->pi, $this->pageView->pn, $slev, $this->pageView->sortby, PROJ_STATUS_RUN, $flt_by, $city_by);
		for( $i=0; $i<count($this->pageView->its); $i++ )
		{
			$this->pageView->its[$i]['locinfo'] = $this->catLib->Loc_InfoByCity($this->pageView->its[$i]['city_id']);
			$this->pageView->its[$i]['currency'] = $this->catLib->Currency_Info($this->pageView->its[$i]['currency_id']);
			if( $this->pageView->its[$i]['account_type'] == USR_TYPE_ORG )
				$this->pageView->its[$i]['photos'] = $this->catLib->Item_Pics($this->pageView->its[$i]['id'], 1);
		}
		
		//$this->pageView->catmodel = $this->catLib;
		
		// Render page
		$this->pageView->render_default();
	}
	
	public function action_default()
	{
		//$uid = $this->authAllow();
		//$uid = UhCmsApp::getSesInstance()->UserId;

		$this->pageView->catmode = $this->catmode;
		$this->pageView->spath = Array();
		$this->pageView->sid = 0;
		$this->pageView->UserId = UhCmsApp::getSesInstance()->UserId;
		$this->pageView->page_h1 = "Запросы о помощи";
		
		$this->pageView->pi = 0;
		$this->pageView->pn = CAT_ITEMS_PERPAGE;
		
		$slev = -1;
		
		$this->pageView->its_total = $this->catLib->Item_Num(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, 0, $slev);
		$this->pageView->its = $this->catLib->Item_List(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, 0, 0, CAT_ITEMS_PERPAGE, $slev, "add", PROJ_STATUS_RUN);
		for( $i=0; $i<count($this->pageView->its); $i++ )
		{
			$this->pageView->its[$i]['locinfo'] = $this->catLib->Loc_InfoByCity($this->pageView->its[$i]['city_id']);
			$this->pageView->its[$i]['currency'] = $this->catLib->Currency_Info($this->pageView->its[$i]['currency_id']);
			if( $this->pageView->its[$i]['account_type'] == USR_TYPE_ORG )
				$this->pageView->its[$i]['photos'] = $this->catLib->Item_Pics($this->pageView->its[$i]['id'], 1);
		}
		
		//$this->pageView->catmodel = $this->catLib;
		
		/*
		$uinfo = $clib->Buyer_Info($uid);
		$uinfo['group_name'] = $clib->Buyer_GroupInfo($uinfo['account_type']);		
		$this->pageView->userinfo = $uinfo;
		$this->pageView->catmodel = $this->catLib;
		$this->pageView->curl = '';
		*/
		// Render page
		$this->pageView->render_default();
	}
	
	private function _getRegVars()
	{
		$reg = Array();
		$req['action'] = $this->getReqParam("action", "");
		$reg['rlogin'] = $this->getReqParam("rlogin", "");
	
		return $reg;
	}
	
	private function _initRegvars()
	{
		$reg = Array();
		$reg['action'] = "";
		$reg['rlogin'] = "";
	
		return $reg;
	}
	
}
?>