<?php

class land extends Controller {

    protected $view;

    function __construct($config, $db)
    {
        parent::__construct($config, $db);

        $fileView = $config['MVC_PATH']."views/landView.class.php";

        if( !file_exists($fileView) )
        {
            die('404 Not Found - Views');
        }

        include_once $fileView;

        $this->model = '';
        $this->view = new LandView($this->getCfg(), $this->model);
    }

    public function index()
    {
        echo "";
    }

    public function action_default()
    {
        //$this->view->render_default();
    }

    public function action_give_and_take_things(){
        $this->view->render_landhelp();
    }

    public function action_make_you_dream(){
        $this->view->render_landdream();
    }
}