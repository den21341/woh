<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$show_send_msg_lnk = false;

////////////////////////////////////////////////////////////////////////////

$PM = $this->pageModel;

$page = $PM->get_page();

// Set page head title (h1 tag value)
$PAGE_H1 = $page->title;

$IMG_CHECK_SRC = WWWHOST.'img/icnok1.png';

$check = "";
if(isset($this->projinfo['is_check']))
	$this->projinfo['is_check'] ? $check = '<img src="'.$IMG_CHECK_SRC.'">' : '';

// Build bread crumbs
$BCHTML = $this->renderBreadcrumbs($PAGE_H1);

if( isset($this->projinfo) && ($this->viewMode == "") )
	$PAGE_H1 = $this->projinfo['title2'];

if( isset($this->title) )
	$PAGE_H1 = $this->title;

$is_dream = false;

if(isset($this->projinfo['secturl']) && $this->projinfo['secturl'] == 'realizedream') {
	$is_dream = true;
	$pCount = $this->catmodel->geStarCounter($this->projinfo['id']);
}

$mnghd = '';
$testQ = '';
if(isset($this->projinfo) && $this->projinfo['id'] == 1223) {
	$mnghd = 1;
	$testQ = $this->catmodel->getTestQuestions();
}

$req_byconv = (isset($_GET['cat']) ? ($_GET['cat'] == 1 ? 1 : 0) : 0);

$loc = (isset($this->user_info['loc_info']['name']) == "" ? $this->localize->get("cabinet", "noselected") : $this->user_info['loc_info']['name']);
$cntry = (isset($this->user_info['loc_info']['countryname']) == "" ? '('.$this->localize->get("cabinet", "noselected").')' : '('.$this->user_info['loc_info']['countryname'].')');
$location = ($loc." ".$cntry);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//

if($this->viewMode == 'edit' || $this->viewMode == 'created') { ?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=WWWHOST?>js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="<?=WWWHOST?>js/fancybox/jquery.fancybox.pack.js"></script>

	<?php if(isset($this->picErr) && $this->picErr) { ?>

		<a class="picErrorPopUp" href="#pic-err"></a>
		<div id="pic-err" ><h3><?=$this->localize->get("project", "small-photo")?></h3>
			<p style="color: #555"><?=$this->localize->get("project", "size")?></p></div>

		<script>
			$(document).ready(function () {
				$(".picErrorPopUp").fancybox({
					maxWidth	: 700,
					maxHeight	: 200,
					fitToView	: false,
					width		: '70%',
					height		: '70%',
					autoSize	: false,
					closeClick	: false,
					openEffect	: 'none',
					closeEffect	: 'none'
				});
				$('.picErrorPopUp').click();
			});
		</script>
	<?php } ?>
<?php }
if($this->viewMode == "check"){ ?>
	<div class="container">
		<div class="info info-minheight text-center">
			<?php
			echo '<p class="proj-check">'.$this->localize->get("proj", "check-project").'</p>'."<br>";
			echo '<a href="'.$this->page_BuildUrl("cabinet","editme").'">'.$this->localize->get("proj", "check-editme").'</a>';
			?>
		</div>
	</div>
<?php }
else if( $this->viewMode == "notallowed" ) {
	?>
	<div class="container">
		<div class="info info-minheight text-center">
			<?php
			echo $PM->get_txtres()->newprojnotallowed['text'].'<br>';
			echo '<a href="'.$this->page_BuildUrl("cabinet","needhelp").'">'.$this->localize->get("proj", "notallowed").'</a>';
			?>
		</div>
	</div>
	<?php
} else if($this->viewMode == 'manyproj') {
	echo '<div class="manyproj-notallowed">'.$this->localize->get("project", "no-more-project").'';
	echo '<a href="'.$this->page_BuildUrl("cabinet","needhelp").'">'.$this->localize->get("proj", "notallowed").'</a>';
	echo '</div>';
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
else if( $this->viewMode == "helpdone" )
{
	?>
	<div class="container">
		<div class="info info-minheight text-center">
			<?php
			echo $PM->get_txtres()->prodreqsend['text'].'<br>';
			echo '<a href="'.$this->page_BuildUrl("proj","view/".$this->projinfo['id']).'">'.$this->localize->get("proj", "backtoproj").' &quot;'.$this->projinfo['title2'].'&quot;</a>';
			?>
		</div>
	</div>
	<?php
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
else if( $this->viewMode == "thingdone" )
{
	?>
	<div class="container">
		<div class="info info-minheight text-center">
			<?php
			echo $PM->get_txtres()->thingreqsend['text'].'<br>';
			echo '<a href="'.$this->page_BuildUrl("proj","view/".$this->projinfo['id']).'">'.$this->localize->get("proj", "backtoproj").' &quot;'.$this->projinfo['title2'].'&quot;</a>';
			?>
		</div>
	</div>
	<?php
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
else if( $this->viewMode == "myhelpdone" )
{
	?>
	<div class="container">
		<div class="info info-minheight text-center">
			<?php
			echo $PM->get_txtres()->myhelpreqsend['text'].'<br>';
			echo '<a href="'.$this->page_BuildUrl("proj","view/".$this->projinfo['id']).'">'.$this->localize->get("proj", "backtoproj").' &quot;'.$this->projinfo['title2'].'&quot;</a>';
			?>
		</div>
	</div>
	<?php
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
else if( $this->viewMode == "saved" )
{
	?>
	<div class="container">
		<div class="info info-minheight text-center">
			<?php
			//echo $PM->get_txtres()->prodreqsend['text'].'<br>';
			echo '<a href="'.$this->page_BuildUrl("cabinet","needhelp").'">'.$this->localize->get("proj", "backtocab").'</a>';
			?>
		</div>
	</div>
	<?php
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
else if( $this->viewMode == "created")
{
	?>
	<div class="container">
		<div class="info">
			<?=( $page->content );?>
			<div class="addproj-face">
				<div class="tbl-noitems">
					<div class="tbl-noitems-txt">
						<div><span><?=$this->localize->get("addproj", "photo-promo");?></span></div>
					</div>
					<div class="tbl-noitems-face"></div>
					<div class="both"></div>
				</div>
				<div class="both"></div>
			</div>
		</div>
	</div>
	<form id="projformpic" action="<?=$this->Page_BuildUrl("proj", "addmedia");?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="ptype" value="<?=$this->reqtype?>">
		<input type="hidden" name="action" value="addpublish">
		<input type="hidden" name="projid" value="<?=$this->projid;?>">
		<div class="container">
			<a id="formstart" name="formstart"></a>
			<div class="h2 text-center"><?=$this->localize->get("addproj", "photo-hdr");?></div>
		</div>
		<div class="row-gray">
			<div class="container">
				<div class="row row-form-pad">
					<?php
					$pics = $this->projpics;
					$pic_num = count($pics);
					//print_r($pic_num);
					$pic_num > 0 ? $val_pic = $this->localize->get("addproj", "photo-lbl-add") : $val_pic = $this->localize->get("addproj", "photo-lbl");
					$pic_num > 0 ? $btn_pic = $this->localize->get("addproj", "photo-btn-next") : $btn_pic = $this->localize->get("addproj", "photo-btn");
					for( $i=0; $i<count($pics); ++$i ) {
						echo '<div class="col-xs-3 col-md-2">
				<img src="'.WWWHOST.stripslashes($pics[$i]['filename_ico']).'" class="img-thumbnail center-block" alt="">
				<div class="proj-pic-del"><a href="'.$this->Page_BuildUrl("proj", "delmedia", "projid=".$this->projid."&picid=".$pics[$i]['id']).'">'.$this->localize->get("addproj", "photo-del").'</a></div>
			</div>';
					}
					?>
					<div class="col-xs-3 col-md-3">
						<div class="form-group">
							<label for="file1" class="control-label"><?=$val_pic?>:</label>
							<div><input type="file" id="file1" name="pfile1"></div>
						</div>
					</div>
					<div class="col-xs-3 col-md-2 col-btn-pad">
						<input type="submit" id="addpicbtn" name="addpicbtn" class="btn btn-primary" value="<?=$btn_pic?>" />
					</div>
				</div>
			</div>
		</div>
		<?php /* if($this->reqtype != -1) { ?>
			<div class="container">
				<br><br>
				<div class="h2 text-center"><?=$this->localize->get("addproj", "background-hdr")?></div>
				<div class="proj-add-sundr"><?=$this->localize->get("addproj", "background-hdr-nxt")?></div>
				<div><p class="woh-err" id="woherr">Выберете одну из перечисленых систем оплаты</p></div>
				<div>
					<table>
						<tr>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>

							<?php foreach($this->reqmon as $payment) {
							if($payment['name'] == 'WayOfHelp') { ?>
								<td><label for="pwoh"><?=$payment['name']?></label></td>
								<td><input type="checkbox" name="pwoh" id="pwoh"></td>
							<?php } else { ?>
							<td><label for="p_<?=$payment['payment_id']?>"><?=$payment['name']?></label></td>
							<td class="proj-pymnt-inp" id="td_<?=$payment['payment_id']?>"><input type="input" name="p_<?=$payment['payment_id']?>" id="p_<?=$payment['payment_id']?>" ><img
									src="<?=WWWHOST?>img/proj-success.png" style="display:none" height="12px" width="12px" class="proj-ch-success" alt="Проверено" title="Проверено"></td>
						</tr>

						<?php } } ?>
					</table>
				</div>
			</div>
		<?php } */ ?>
		<div class="row-gray">
			<div class="container">
				<div class="row">

					<?php
					if( $this->userinfo['pic'] == '' ) {
					?>
				</div>
			</div>
			<div class="container">
				<br><br>
				<div class="h2 text-center"><?=$this->localize->get("addproj", "avatar-hdr");?></div>
			</div>
			<div class="row-gray">
				<div class="container">
					<div class="row row-form-pad">
						<div class="col-xs-3 col-md-3">
							<div class="form-group">
								<label for="fileuser" class="control-label"><?=$this->localize->get("addproj", "photo-user-lbl");?>:</label>
								<div><input type="file" id="fileuser" name="pfileuser"></div>
							</div>
						</div>
						<div class="col-xs-3 col-md-2 col-btn-pad">
							<input type="submit" name="addavatarbtn" class="btn btn-primary" value="<?=$this->localize->get("addproj", "photo-user-btn");?>" />
						</div>
					</div>
					<?php
					}
					?>
				</div>
			</div>
			<div class="container">
				<div class="text-center btn-form-pad"><input type="button" id="btnprojfinish" class="btn btn-primary" value="<?=$this->localize->get("addproj", "photo-btn-finish");?>" /></div>
			</div>
	</form>
	<?php
	if( count($pics) == 0 )
	{
		?>
		<!--<div class="poptip-promo">
	<div class="poptip-promo-cont">
		<div class="poptip-close"><a href="#" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-remove"></span></a></div>
		<div class="poptip-info"><?=( $PM->get_txtres()->newprojpopaddpic['text'] );?></div>
	</div>
</div>-->
		<?php
		//$PM->setPromoSeen();
	}

	?>
	<script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
	<script>
		$(document).ready(function() {

			var prtype = '<?=$this->reqtype?>';
			var pymntCount = '<?=$this->pymntCount[0]['count']?>';
			var pymntArr = [];
			var is_err = 0;
			var is_one = 0;

			// TESTING

			var pymntPattern = ["4[0-9]{12,15}", "5[0-9]{15,18}"];

			$("#btnprojfinish").bind("click", function() {
				popWnd('sharedlg', 'projid=<?=$this->projid?>'+'&mode=proj');
				return false;
			});

			$(".wnd-close a").on("click", function(){
				$("#projformpic").submit();
			});
			<?php
			if( count($pics) == 0 )
			{
			?>
			showPoptipPromo();
			<?php
			/*
	?>
		$(".poptip-close a").bind("click", function(){
			$(".poptip-promo").hide();	//removeClass("poptip-promo-show");
			$(window).off("scroll");
			return false
		});

		//var pos = $("#addpicbtn").offset();
		var winw = $(window).width();
		var winh = $(window).height();
		var topscroll = $(document).scrollTop();
		$(".poptip-promo").css("left", Math.round(winw - 600)+"px");
		$(".poptip-promo").css("top", Math.round(winh + topscroll - 320)+"px");
		$(".poptip-promo").addClass("poptip-promo-show");

		$(window).scroll(function() {
			var winw = $(window).width();
			var winh = $(window).height();
			var topscroll = $(document).scrollTop();
			$(".poptip-promo").css("left", Math.round(winw - 600)+"px");
			$(".poptip-promo").css("top", Math.round(winh + topscroll - 320)+"px");
		});
	<?php
			*/
			}
			?>
		});
	</script>
	<?php
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
else if( $this->viewMode == "create" || $this->viewMode == "edit" )
{
	$countries = $this->catmodel->Loc_CountryList();
	$obls = $this->catmodel->Loc_OblList($this->reqdata->rcountry);
	?>

	<script type="text/javascript" src="<?=WWWHOST?>js/nicEdit-latest.js"></script>

	<div class="container">
		<div class="info">
			<?=( $this->viewMode == "create" ? $page->header : "" );?>
		</div>
	</div>
	<form id="projformadd" class="regform" action="<?=( $this->viewMode == "edit" ? $this->Page_BuildUrl("proj", "save") : $this->Page_BuildUrl("proj", "publish") );?>" method="POST" enctype="multipart/form-data">
		<?php
		if( $this->viewMode == "edit" )
		{
			echo '<input type="hidden" name="action" value="savepublish">
			<input type="hidden" name="projid" value="'.$this->reqdata->id.'">';
		}
		else
		{
			echo '<input type="hidden" name="action" value="makepublish">
			<input type="hidden" name="dubguid" value="'.UhCmsUtils::makeUuid().'">';

			if( $this->event_mode )
			{
				echo '<input type="hidden" name="eventtype" value="1">';
			}
			if( $this->thing_mode )
			{
				echo '<input type="hidden" name="thingtype" value="1">';
			}
			if( $this->dream_mode )
			{
				echo '<input type="hidden" name="dreamtype" value="1">';
			}
			if( $this->myhelp_mode )
			{
				echo '<input type="hidden" name="helptype" value="1">';
			}
		}
		?>
		<div class="row-gray">
			<div class="container">
				<a id="formstart" name="formstart"></a>
				<div class="h2"><?=$this->localize->get("addproj", "hdr".( $this->event_mode ? "event" : ""));?></div>
				<?php
				if( $this->reqdata->has_errors() )
				{
					echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
				}
				?>
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div class="form-group<?=( !$this->reqdata->ok('tit') ? ' has-error' : '' );?>">
							<label for="rname" class="control-label"><?=$this->localize->get("addproj", "lbl-tit".( $this->event_mode ? "event" : ""));?>:</label>
							<a target="_blank" href="<?=$this->page_BuildUrl('info','projectinfo')?>"><div class="btn btn-primary proj-btn-how"><span><?=$this->localize->get("project", "complate-proj")?></span></div></a>
							<input type="text" class="form-control" id="tit" name="tit" placeholder="<?=$this->localize->get("addproj", "ph-tit");?>" value="<?=$this->reqdata->quote($this->reqdata->tit);?>">
							<div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-tit".( $this->event_mode ? "event" : ""));?></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div class="form-group<?=( !$this->reqdata->ok('sect') ? ' has-error' : '' );?>">
							<label for="rname" class="control-label"><?=$this->localize->get("addproj", "lbl-sect");?>:</label>
							<select class="form-control" id="sect" name="sect">
								<option value="0">--- <?=$this->localize->get("addproj", "lbl-sect0");?> ---</option>
								<?php
								if( $this->dream_mode )
									echo $this->render_SubSectCombo(RSECT_ID_DREAM, $this->reqdata->sect);
								else if( $this->thing_mode )
									echo $this->render_SubSectCombo(RSECT_ID_THING, $this->reqdata->sect);
								else if( $this->myhelp_mode )
									echo $this->render_SectCombo_Help($this->reqdata->sect);
								else
									echo $this->render_SectCombo($this->reqdata->sect);
								?>
							</select>
							<div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-sect".( $this->event_mode ? "event" : ""));?></div>
						</div>
					</div>
				</div>
				<div class="row form-horizontal">
					<div class="col-xs-12 col-md-6">
						<div class="form-group<?=( !$this->reqdata->ok('ptype') ? ' has-error' : '' );?>">
							<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("addproj", "lbl-type".( $this->event_mode ? "event" : ""));?>:</label>
							<div class="col-sm-9"><select class="form-control" id="ptype" name="ptype">
									<?php
									if( $this->thing_mode )
									{
										?>
										<option value="3" <?=($this->reqdata->ptype == PROJ_TYPE_OTHER ? ' selected' : '');?>><?=$this->localize->get("addproj", "lbl-type-3");?></option>
										<?php
									}
									else if( $this->myhelp_mode )
									{
										?>
										<option value="3" <?=($this->reqdata->ptype == PROJ_TYPE_OTHER ? ' selected' : '');?>><?=$this->localize->get("addproj", "lbl-type-3");?></option>
										<?php
									}
									else
									{
										if($this->viewMode == 'edit' && $this->amountType == PROJ_TYPE_HUMAN) {
											?>
											<!--<option value="0" <?=($this->reqdata->ptype == PROJ_TYPE_MONEY ? ' selected' : '');?>><?=$this->localize->get("addproj", "lbl-type-0");?></option>-->
											<option value="1" <?=($this->reqdata->ptype == PROJ_TYPE_HUMAN ? ' selected' : '');?>><?=$this->localize->get("addproj", "lbl-type-1");?></option>
											<!--<option value="2" <?=($this->reqdata->ptype == PROJ_TYPE_CONSULT ? ' selected' : '');?>><?=$this->localize->get("addproj", "lbl-type-2");?></option>-->
											<!--<option value="3" <?=($this->reqdata->ptype == PROJ_TYPE_OTHER ? ' selected' : '');?>><?=$this->localize->get("addproj", "lbl-type-3");?></option>-->
										<?php } else if ($this->viewMode == 'edit' && $this->amountType == PROJ_TYPE_MONEY) { ?>
											<option
												value="0" <?= ($this->reqdata->ptype == PROJ_TYPE_MONEY ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-0"); ?></option>
											<!--<option
								value="1" <?= ($this->reqdata->ptype == PROJ_TYPE_HUMAN ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-1"); ?></option>
							<option
								value="2" <?= ($this->reqdata->ptype == PROJ_TYPE_CONSULT ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-2"); ?></option>
							<option
								value="3" <?= ($this->reqdata->ptype == PROJ_TYPE_OTHER ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-3"); ?></option>-->
											<?php
										} else if ($this->viewMode == 'edit') { ?>
											<option
												value="2" <?= ($this->reqdata->ptype == PROJ_TYPE_CONSULT ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-2"); ?></option>
											<option
												value="3" <?= ($this->reqdata->ptype == PROJ_TYPE_OTHER ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-3"); ?></option>
										<?php } else { ?>
											<option
												value="0" <?= ($this->reqdata->ptype == PROJ_TYPE_MONEY ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-0"); ?></option>
											<option
												value="1" <?= ($this->reqdata->ptype == PROJ_TYPE_HUMAN ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-1"); ?></option>
											<option
												value="2" <?= ($this->reqdata->ptype == PROJ_TYPE_CONSULT ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-2"); ?></option>
											<option
												value="3" <?= ($this->reqdata->ptype == PROJ_TYPE_OTHER ? ' selected' : ''); ?>><?= $this->localize->get("addproj", "lbl-type-3"); ?></option>
										<?php }
									}

									?>
								</select></div>
						</div>
					</div>
					<div class="col-xs-12 col-md-6">
						<?php
						if( !$this->thing_mode && !$this->myhelp_mode )
						{
							if($this->viewMode != 'edit' || ($this->amountType == PROJ_TYPE_MONEY || $this->amountType == PROJ_TYPE_HUMAN)) {
								?>
								<div id="amountrow" <?=($this->viewMode == 'edit' && !$this->amountType ? 'style="visibility: hidden"' : '')?> class="form-group<?=( !$this->reqdata->ok('pamount') ? ' has-error' : '' );?>">
									<span class="hidden" id="resamount-money"><?=$this->localize->get("addproj", "lbl-amount-money");?></span><span class="hidden" id="resamount-human"><?=$this->localize->get("addproj", "lbl-amount-human");?></span>
									<label id="lblamount" for="pamount" class="col-sm-3 control-label"><?=$this->localize->get("addproj", "lbl-amount");?>:</label>

									<div class="col-sm-7">
										<input type="text" class="form-control" id="pamount" name="pamount" value="<?=$this->reqdata->pamount?>">
										<div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-amount");?></div>
									</div>
									<div class="col-sm-2">
										<select id="pamountizm0" name="pamountizm" class="form-control">
											<?php
											for( $i=0; $i<count($this->currency); $i++ ) {
												echo '<option value="'.$this->currency[$i]['id'].'" '.($this->reqdata->pamountizm == $this->currency[$i]['id'] ? ' selected' : '').'>'.$this->currency[$i]['name'].'</option>';
											}
											?>
										</select>
										<select id="pamountizm1" class="form-control" style="display:none;">
											<option value="0"><?=$this->localize->get("project", "man")?></option>
										</select>
									</div>
								</div>
							<?php } ?>
							<?php
						}
						?>
					</div>
				</div>
				<div class="row form-horizontal jhscope">
					<div class="col-xs-12 col-md-6">
						<div class="form-group<?=( !$this->reqdata->ok('stdt') ? ' has-error' : '' );?>">
							<label for="stdt" class="col-sm-3 control-label"><?=$this->localize->get("addproj", "lbl-dtst");?>:</label>
							<div class="col-sm-9"><input type="text" class="form-control" id="stdt" name="stdt" value="<?=$this->reqdata->stdt;?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-stdt");?></div></div>
						</div>
						<div class="form-group<?=( !$this->reqdata->ok('endt') ? ' has-error' : '' );?>">
							<label for="endt" class="col-sm-3 control-label"><?=$this->localize->get("addproj", "lbl-dten");?>:</label>
							<div class="col-sm-9"><input type="text" class="form-control" id="endt" name="endt" value="<?=$this->reqdata->endt;?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-endt");?></div></div>
						</div>
						<script>
							var tlblarr = Array("money", "human", "none", "none");

							$(document).ready(function() {
								<?php if(isset($this->amountType) && $this->amountType == PROJ_TYPE_HUMAN) { ?>
								$("#pamountizm0").hide();
								$("#pamountizm1").show();
								$("#amountrow").show();
								$("#lblamount").html( $("#resamount-"+tlblarr[selid]).html() + ":" );
								<?php } ?>

								$("#ptype").change("bind", function(){
									var combo = $(this).get(0);

									var selid = combo.options[combo.selectedIndex].value;
									if( tlblarr[selid] == "none" )
									{
										$("#amountrow").hide();
									}
									else
									{
										if( tlblarr[selid] == "money" )
										{
											$("#pamountizm1").hide();
											$("#pamountizm0").show();
										}
										else
										{
											$("#pamountizm0").hide();
											$("#pamountizm1").show();
										}
										$("#amountrow").show();
										$("#lblamount").html( $("#resamount-"+tlblarr[selid]).html() + ":" );
									}
								});


								$("#stdt").datepicker({
									dateFormat: "dd.mm.yy",
									dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
									dayNamesShort: [ "Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб" ],
									dayNames: [ "Воскрес.", "Понед.", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" ],
									monthNamesShort: [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
									monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
									minDate: new Date(<?=date("Y",time());?>, <?=date("m",time());?>-1, <?=date("d",time());?>)
								});

								$("#endt").datepicker({
									dateFormat: "dd.mm.yy",
									dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
									dayNamesShort: [ "Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб" ],
									dayNames: [ "Воскрес.", "Понед.", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" ],
									monthNamesShort: [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
									monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
									minDate: new Date(<?=date("Y",time()+3*24*3600);?>, <?=date("m",time()+3*24*3600);?>-1, <?=date("d",time()+3*24*3600);?>)
								});
							});
						</script>
					</div>
					<div class="col-xs-12 col-md-6">
						<div class="form-group<?=( !$this->reqdata->ok('rcountry') ? ' has-error' : '' );?>">
							<label for="rcountry" class="col-sm-3 control-label"><?=$this->localize->get("addproj", "lbl-country");?>:</label>
							<div class="col-sm-9"><select class="form-control" id="rcountry" name="rcountry" onchange="loc_LoadRegion(this, 'robl', 'rcity')">
									<?php
									for( $i=0; $i<count($countries); $i++ )
									{
										echo '<option value="'.$countries[$i]['id'].'"'.($this->reqdata->rcountry == $countries[$i]['id'] ? ' selected' : '').'>'.$countries[$i]['name'].'</option>';
									}
									?>
								</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-country");?></div></div>
						</div>
						<div class="form-group<?=( !$this->reqdata->ok('robl') ? ' has-error' : '' );?>">
							<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("addproj", "lbl-obl");?>:</label>
							<div class="col-sm-9"><select class="form-control" id="robl" name="robl" onchange="loc_LoadCity(this, 'rcity')">
									<option value="0">----- <?=$this->localize->get("addproj", "lbl-obl-0");?> -----</option>
									<?php
									for( $i=0; $i<count($obls); $i++ )
									{
										echo '<option value="'.$obls[$i]['id'].'"'.($this->reqdata->robl == $obls[$i]['id'] ? ' selected' : '').'>'.$obls[$i]['name'].'</option>';
									}
									?>
								</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-obl");?></div></div>
						</div>
						<div class="form-group<?=( !$this->reqdata->ok('rcity') ? ' has-error' : '' );?>">
							<label for="rcity" class="col-sm-3 control-label"><?=$this->localize->get("addproj", "lbl-city");?>:</label>
							<div class="col-sm-9"><select class="form-control" id="rcity" name="rcity">
									<option value="0">----- <?=$this->localize->get("addproj", "lbl-city-0");?> -----</option>
									<?php
									$cities = $this->catmodel->Loc_CityList(UKRAINE, $this->reqdata->robl);
									for( $i=0; $i<count($cities); $i++ )
									{
										echo '<option value="'.$cities[$i]['id'].'"'.($this->reqdata->rcity == $cities[$i]['id'] ? ' selected' : '').'>'.$cities[$i]['name'].'</option>';
									}
									?>
								</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-city");?></div></div>
						</div>
					</div>
				</div>
				<label for="arbitr"><?=$this->localize->get("project", "advertising")?></label>
				<input id="arbitr" checked type="checkbox" name="arbitr">
				<span><a href="<?=WWWHOST?>info/agreement/" target="_blank" style="font-size: 12px" ><?=$this->localize->get("project", "agreement");?></a></span>
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div class="form-group<?=( !$this->reqdata->ok('descr') ? ' has-error' : '' );?>">
							<label for="rdescr" class="control-label"><?=$this->localize->get("addproj", "lbl-descr");?> <span class="doplabel"><?=$this->localize->get("addproj", "lbl-descrdop");?></span></label>
							<textarea class="form-control" id="area1" id="descr" name="descr" rows="4" placeholder="<?=$this->localize->get("addproj", "lbl-descr-placeholder");?>"><?=$this->reqdata->descr;?></textarea>
							<div class="txtcount" id="ltcount1"><?=$this->localize->get("addproj", "lbl-descr-count0");?> <span>300</span> <?=$this->localize->get("addproj", "lbl-descr-count1");?></div>
							<div class="alert alert-danger" role="alert"><?=$this->localize->get("addproj", "err-descr".( $this->event_mode ? "event" : ""));?></div>
						</div>
					</div>
				</div>
				<!--<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="form-group">
					<label for="rdescr" class="control-label"><?=$this->localize->get("addproj", "lbl-descrfull");?> <span class="doplabel"><?=$this->localize->get("addproj", "lbl-descrfulldop");?></span></label>
					<textarea class="form-control  editor" id="descrfull" name="descrfull" rows="8"><?=$this->reqdata->descrfull;?></textarea>
				</div>
			</div>
		</div>-->
				<script>
					$(document).ready(function () {

						bkLib.onDomLoaded(function() {
							new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript']}).panelInstance('area1');
						});

						//alert('done');
						$('body').on('keyup', '.nicEdit-main', function(){
							//alert();
							var slblid = 'ltcount1';
							var maxl = 300;
							var ost = (maxl - $(this).text().length);

							if( ost <= 0 )
							{
								$("#"+slblid).hide();
								return;
								//inp.value = inp.value.substr(0, (maxl-1));
							}

							$("#"+slblid).show();
							$("#"+slblid+" span").html("<b>"+ost+"</b>");
							if( ost<=3 )
							{
								$("#"+slblid).addClass("txtcount-last");
							}
							else
							{
								$("#"+slblid).removeClass("txtcount-last");
							}
						});
					});
				</script>
				<?php
				if( $this->viewMode == "edit" ) {
					?>
					<div class="row row-form-pad">
						<?php
						$pics = $this->projpics;
						$pic_num = count($pics);
						//print_r($pic_num);
						$pic_num > 0 ? $val_pic = $this->localize->get("addproj", "photo-lbl-add") : $val_pic = $this->localize->get("addproj", "photo-lbl");
						$pic_num > 0 ? $btn_pic = $this->localize->get("addproj", "photo-btn-next") : $btn_pic = $this->localize->get("addproj", "photo-btn");
						for( $i=0; $i<count($pics); ++$i ) {
							echo '<div class="col-xs-3 col-md-2">
				<img src="'.WWWHOST.stripslashes($pics[$i]['filename_ico']).'" class="img-thumbnail center-block" alt="">
				<div class="proj-pic-del">
					<a href="'.$this->Page_BuildUrl("proj", "save", "projid=".$this->projid."&picid=".$pics[$i]['id']).'">'.$this->localize->get("addproj", "photo-del").'</a>
					</div>
			</div>';
						}
						?>
						<div class="col-xs-3 col-md-3">
							<div class="form-group">
								<label for="file1" class="control-label"><?=$val_pic?>:</label>
								<div><input type="file" id="file1" name="pfile1"></div>
							</div>
						</div>
						<div class="col-xs-3 col-md-2 col-btn-pad">
							<input type="submit" id="addpicbtn_edit" name="addpicbtn" class="btn btn-primary" value="<?=$btn_pic?>" />
						</div>
					</div>
					<!--<div class="row">
	<?php /*
			$pics = $this->catmodel->Item_Pics($this->reqdata->id);
			!empty($pics) ? $co_lbl = $this->localize->get('addproj', 'photo-lbl-add') : $co_lbl = $this->localize->get('addproj', 'photo-lbl');

			for($i=0; $i<count($pics); $i++)
			{
				echo '
				<div class="col-xs-3 col-md-2">
					<img src="'.WWWHOST.$pics[$i]['filename_ico'].'" alt="" class="img-thumbnail center-block">
					<div class="proj-pic-del"><a href="'.$this->Page_BuildUrl("proj", "picdel", "projid=".$this->reqdata->id."&picid=".$pics[$i]['id']."").'">'.$this->localize->get("addproj", "photo-del").'</a></div>
				</div>
				';
			}*/
					?>
			<!--<div class="col-xs-4 col-md-3">
				<div class="form-group">
					<label for="rfile" class="control-label"><?=$co_lbl?></label>
					<input type="file" id="file1" name="file1">
					<div class="btn-form-pad-sm"></div><input type="submit" name="btnaddphoto" class="btn btn-primary" value="<?='Загрузить еще'?>"></div>
				</div>
			</div>
		</div>-->
					<?php
				}
				?>
			</div>
		</div>
		<div class="container">
			<?php
			/*
        ?>
        <div class="row form-horizontal" style="padding: 18px 0 30px 0;">
            <div class="col-xs-12 col-md-6">
                Captcha
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="form-group">
                    <label for="rseccode" class="col-sm-3 control-label">Введите код:</label>
                    <div class="col-sm-9"><input type="text" class="form-control" id="rseccode" name="rseccode" placeholder=""></div>
                </div>
            </div>
        </div>
        <div class="">
            <div class="checkbox">
                <label>
                <input type="checkbox" id="rrule" name="rrule" value="1">
                Я принимаю условия <a href="#">лицензионного соглашения</a> и обязуюсь не нарушать правила работы портала Justhelp во время пользования услугами сайта
                </label>
            </div>
        </div>
        */

			$btn_text = $this->localize->get("addproj", "btn-addproj");
			if( $this->dream_mode )
				$btn_text = $this->localize->get("addproj", "btn-adddream");
			else if( $this->thing_mode )
				$btn_text = $this->localize->get("addproj", "btn-addthing");
			else if( $this->event_mode )
				$btn_text = $this->localize->get("addproj", "btn-addevent");
			if( $this->viewMode == "edit" )
				$btn_text = $this->localize->get("addproj", "btn-save");
			?>
			<a href="#"><div class="text-center btn-form-pad"><input type="submit" class="btn btn-primary" value="<?=$btn_text?>" /></div></a>
		</div>
	</form>
	<?php
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
else
{
	$usernow = $this->usernow;
	$proj = $this->projinfo;
	$pics = $proj['photos'];

	$proj_is_thing = ( $proj['profile_id'] == PROJ_THINGS );
	$proj_is_myhelp = ( $proj['profile_id'] == PROJ_SENDHELP );

	//var_dump($proj['currency']);

	$currency_name = ( isset($proj['currency']['name']) ? $proj['currency']['name'] : "" );

	// local vars for fast use
	$projType = $proj['profile_id'];	// PROJ_NEEDHELP or PROJ_SENDHELP

	// viewer request checks
	$isReqSent = ( ($reqMy = $this->catmodel->Buyer_ReqIsSend($this->UserId, $proj['id'])) !== false );
	$isMy = ($this->UserId == $proj['author_id']);

	if($isMy)
		$this->catmodel->resetPoptipItemComment($proj['id']);

	$show_addsum_btn = false;
	$show_addman_btn = false;
	$show_getthing_btn = false;
	$show_myhelp_btn = false;

	$show_send_msg_lnk = ( !$isMy && ($this->UserId != 0) );

	$pic_html = '';
	if( count($pics)>0 ) {
		$pic_html = '<img src="'.WWWHOST.$pics[0]['filename_ico'].'" alt="'.$PAGE_H1.'" title="'.$PAGE_H1.'" class="img-thumbnail" width="'.$pics[0]['ico_w'].'" height="'.$pics[0]['ico_h'].'">';
	}

	$this_url = preg_replace('/^([^?]+)(\?.*?)?(#.*)?$/', '$1$3', $_SERVER['REQUEST_URI']);
	$redir_pars = Array("redirto" => $this_url);
	$redir_pars_str = http_build_query($redir_pars);

	$back_url_p = '';

	if(isset($_SERVER['HTTP_REFERER'])) {
		$back_url_p = preg_match('/cat\/p_[0-9]+\//', $_SERVER['HTTP_REFERER'], $matches);
		$matches = explode('/', isset($matches[0]) ? $matches[0] : '');
		$back_url_p = explode('_', isset($matches[1]) ? $matches[1] : '');
		$back_url_p = isset($back_url_p[1]) ? $back_url_p[1] : '';
	}

	if(isset($_GET['subid'])) {
		setcookie("kadid", $_GET['subid'], time() + 60 * 60 * 24, '/');
	}

	?>

	<script type="text/javascript" src="<?=WWWHOST?>js/jquery.progress.js"></script>
	<style>
		#graph-wrapper{
			height: 287px !important;
		}
		#vis-container {
			height: 406px !important;
		}
	</style>

	<div class="row">
		<div class="container-fluid">
			<div class="proj-header">
				<div class="container">
					<ol class="breadcrumb">
						<?=$BCHTML;?>
					</ol>
					<div class="proj-txt-h col-md-7 col-xs-12 col-lg-8">
						<h1 class="<?=(strlen($PAGE_H1)>120) ? 'p-sm-h' : ''?>"><?=$PAGE_H1?></h1>
					</div>
				</div>
			</div>
		</div>
		<div class="font-cont container">
			<div class="col-xs-12 col-md-7 col-lg-8 col-all-bg">
				<div class="proj-big-img donimg">
					<?php

					if(isset($pics[0]['filename_big'])){
						echo '<a '.(isset($pics[0]['filename_big']) ? 'class="fancybox-thumb" rel="fancybox-thumb" href="'.WWWHOST.$pics[0]['filename_big'].'" ': '').'>';
						echo '<img class="big-img-resize" src='.WWWHOST.$pics[0]['filename_big'].' alt="'.$this->projinfo['title2'].'" title="'.$this->projinfo['title2'].'">';
						echo '</a>';
					}
					else{
						echo '<img src='.WWWHOST.'img/no-pic.png'.' alt="WayOfHelp" class="big-no-img-resize" height="500px" width="100%">'; }?>
						</div>
				<div class="social-row-soc row">
					<div class="social-row-share donshare">

						<?php if($is_dream) { ?>

							<div class="proj-share-txt"><?=$this->localize->get("projview", "sharing");?></div>
							<div class="p-share-line-ico">
								<div class="socline-block">
									<div id="ico-fb-counter">0</div>
									<a onclick="Share.facebook('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=facebook&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=fb&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>')"><div class="proj-soc-img proj-ico-fb"></div></a>
								</div>
								<div class="socline-block">
									<div id="ico-vk-counter">0</div>
									<?php echo '<div id="ico-vk-counter"></div>'?>
									<a onclick="Share.vkontakte('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=vk&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=vk&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>')"><div class="proj-soc-img proj-ico-vk"></div></a>
								</div>
								<div class="socline-block">
									<div id="ico-ok-counter">0</div>
									<a onclick="Share.odnoklassniki('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=ok&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=ok&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>')"><div class="proj-soc-img proj-ico-ok"></div></a>
								</div>
								<div class="socline-block">
									<div id="ico-gp-counter">0</div>
									<a onclick="Share.googleplus('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=gp&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=gp&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>')"><div class="proj-soc-img proj-img-tw-gp proj-ico-gp"></div></a>
								</div>
								<div class="socline-block">
									<div id="ico-tw-counter">0</div>
									<a onclick="Share.twitter('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=twitter&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=tw&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>', '<?=$proj['title']?>')"><div class="proj-soc-img proj-img-tw-gp proj-ico-tw"></div></a>
								</div>
							</div>

						<?php } else { ?>
							<div class="proj-share-txt"><?=$this->localize->get("projview", "sharing");?></div>
							<div class="p-share-line-ico">
								<div class="socline-block">
									<div id="ico-fb-counter">0</div>
									<a onclick="Share.facebook('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=facebook&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=fb&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>')"><div class="proj-soc-img proj-ico-fb"></div></a>
								</div>
								<div class="socline-block">
									<div id="ico-vk-counter">0</div>
									<?php echo '<div id="ico-vk-counter"></div>'?>
									<a onclick="Share.vkontakte('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=vk&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=vk&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>')"><div class="proj-soc-img proj-ico-vk"></div></a>
								</div>
								<div class="socline-block">
									<div id="ico-ok-counter">0</div>
									<a onclick="Share.odnoklassniki('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=ok&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=ok&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>')"><div class="proj-soc-img proj-ico-ok"></div></a>
								</div>
								<div class="socline-block">
									<div id="ico-gp-counter">0</div>
									<a onclick="Share.googleplus('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=gp&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=gp&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>')"><div class="proj-soc-img proj-img-tw-gp proj-ico-gp"></div></a>
								</div>
								<div class="socline-block">
									<div id="ico-tw-counter">0</div>
									<a onclick="Share.twitter('http://wayofhelp.com/proj/view/<?=$this->projinfo['id']?>/?utm_source=twitter&utm_medium=social&utm_campaign=ShareProject&utm_content=<?=$this->projinfo['id'].($this->UserId != 0 ? '&jhsoc=tw&jhscvisitor=1&jhscreferer='.$this->UserHash : '').(isset($_COOKIE['_ga']) ? '&s_ga='.$_COOKIE['_ga'] : '' )?>', '<?=$proj['title']?>')"><div class="proj-soc-img proj-img-tw-gp proj-ico-tw"></div></a>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="proj-hline"></div>
					<span class="share-hide">
						<div class="proj-rate-soc"><?=$this->localize->get("user","rat")." :"?></div>
						<div class="proj-count-txt"><?=$this->localize->get("projview","repost")." :"?></div></span>
						  <span class="proj-num-rate share-hide"><?=$this->box['user_rate']?><br>
							  <span id="sharecount" class="proj-count-num"></span>
</span>
					<span class="btn-mob-hide btn-proj-show fixbtn">

								<?php
								if( $isMy )
								{
									echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-owner").'</b></div>';
								}
								else if( $proj['status'] == PROJ_STATUS_STOP )
								{
									echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-proj-stoped").'</b></div>';
								}
								else if( $proj['status'] >= PROJ_STATUS_ENDED ) // OR 3 - PROJECT PAYED x% by amount
								{
									echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-proj-ended").'</b></div>';
								}
								else if( $isReqSent )
								{
									if( $reqMy['status'] == REQ_STATUS_NEW )
									{
										echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-helpwait").'</b></div>';
									}
									else if( $reqMy['status'] == REQ_STATUS_CONFIRM )
									{
										//echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-helpdone").'</b></div><div>';
										echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-helpdone").'</b></div>';
									}
									else
									{
										echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-helpdecl").'</b></div>';
									}

									if( $proj['amount_type'] == PROJ_TYPE_MONEY) {
										echo '<div id="addhelp_moneys" class="btn-getmoney"> '.$this->localize->get("projview", "btn-money").($proj['is_check'] ? '<img src="'.WWWHOST.'img/icnok1.png">' : '').'</div>';
										$show_addsum_btn = true;
									}

								}
								else
								{
									if( $proj_is_myhelp)
									{
										echo '<div id="get_myhelps" class="btn-getmoney"> '.$this->localize->get("projview", "btn-gethelp").'</div>';
										$show_myhelp_btn = true;
									}
									else if( $proj_is_thing)
									{
										echo '<div id="get_things" class="btn-getmoney"> '.$this->localize->get("projview", "btn-thing").'</div>';
										$show_getthing_btn = true;
									}
									else if( $proj['amount_type'] == PROJ_TYPE_MONEY )
									{
										echo '<div id="addhelp_moneys" class="btn-getmoney">'.$this->localize->get("projview", "btn-money").($proj['is_check'] ? '<img src="'.WWWHOST.'img/icnok1.png">' : '').'</div>';
										$show_addsum_btn = true;
									}
									else
									{
										if(!$mnghd) {
											echo '<div id="addhelp_mans" onclick="yaCounter34223620.reachGoal(\'ProjectPushButton_Pomoch_delom\'); return true;" class="btn-getmoney proj-size-fix">' . $this->localize->get("projview", "btn-help") . '</div>';
											$show_addman_btn = true;
										}
									}
								}
								?>

							</span>

					<!--<div class="sect-btn">-->

					<!--</div>-->
				</div>

				<hr class="hr-line-shar">
				<div class="proj-main-info">
					<div class="<?=($is_dream ? 'col-md-5 col-xs-5 col-lg-4' : '')?> col-ph-usr">
						<div class="info-block-pic">
							<div class="proj-min-photo">

								<?php for($i=1; $i<=count($pics); ++$i) { ?>
									<a class="fancybox-thumb" rel="fancybox-thumb" href="<?=WWWHOST.$pics[$i]['filename_big']?>">
										<img style="display:none" src="<?=WWWHOST.$pics[$i]['filename_big']?>" alt="<?=$PAGE_H1?>" title="<?=$PAGE_H1?>" width="50px" height="50px" >
									</a>
								<?php } ?>

							</div>
						</div>
						<?php if($is_dream) { ?>

							<div class="proj-social-row dreamfix">
								<div class="col-md-12 col-xs-10 col-lg-12">
									<div class="share-show sharefix">
										<div class="proj-rate-soc"><?=$this->localize->get("user","rat")." :"?><span class="proj-num-rate"><?=$this->box['user_rate']?></span></div>
										<div class="proj-count-txt"><?=$this->localize->get("projview","repost")." :"?>
											<b><span id="sharecounts" class="proj-count-num"></span></b>
										</div>
									</div>

									<div class="soc-block-dream">
										<div class="social-row-dream block-dream dream-color"><?=$this->localize->get("projview", "rate-dream");?></div>
										<span class="social-row-count"> (<?=$pCount['count']?>) </span>

										<style>
											.jq-ry-rated-group{
												margin-top: -25px;
											}
											.jq-ry-group{
												width: 100% !important;
											}
										</style>

										<div id="rateYo"></div>
										<div class="social-row-rate block-dream"><?=($this->projinfo['star_rate'])?></div>
									</div>

								</div>
							</div>
						<?php } else { ?>

						<?php } ?>
					</div>
					<div class="<?=($is_dream ? 'col-md-7 col-xs-7 col-lg-8' : 'col-md-11 col-xs-10 col-lg-11')?>">

						<?php 	if($mnghd !== 1) { ?>

							<div class="info-ground ">

								<?php
								$proj['descr0'] = strip_tags($proj['descr0'], '<br>');//preg_replace('#<a.*>.*</a>#USi', '', $proj['descr0']);


									$subTxtStrip = $proj['descr0'];
									$veloHeight = 'set-h';

								?>

								<div class="proj-txt-desc txt-teaser proj-size-fix <?=((isset($veloHeight) && $is_dream) ? $veloHeight : '')?> "><?=$subTxtStrip?><?= (isset($subMore) ? $subMore : '') ?></div>
								<?=isset($subTxtAll) ? '<div class="proj-txt-desc txt-complete proj-size-fix">'.$subTxtAll.'<span class="less"> '.$this->localize->get("bonview", "bonhide").'</span></div>' :''?>
							</div>

						<?php } else { // WARNING!!! БЫДЛОКОД DETECTED ?>

							<div class="proj-txt-desc"><?=$proj['descr0']?></div>
							<form class="proj-mngh-form" action="<?=$this->Page_BuildUrl('proj','mnsend')?>" method="post">
								<?php foreach ($testQ as $quest) { ?>
									<label class="lbl-main" for="q<?= $quest['id'] ?>"><?= $quest['id'] . ' .' . $quest['qname'] ?></label><br>
									<div class="left-br">
										<?php
										$subQ = $this->catmodel->getTestSubQuestions($quest['id']);
										if($quest['id'] == 14) { ?>
											<label><input type="radio" id="q<?=$quest['id']?>" name="q_<?=$quest['id']?>[]" value="Нет"><span><?=$this->localize->get("project", "no")?></span></label><br>
											<label><input type="radio" id="q<?=$quest['id']?>" name="q_<?=$quest['id']?>[]" value="Да"><span><?=$this->localize->get("project", "yes")?>:</span></label><br>
										<?php }
										if (!empty($subQ)) {
											foreach ($subQ as $subquest) {
												if($subquest['qtype'] == 0) { ?>
													<label><input type="radio" id="q<?=$quest['id']?>" name="q_<?=$quest['id']?>[]" value="<?=$subquest['subquestion']?>" required><span><?=$subquest['subquestion']?></span></label><br>
												<?php } else if($subquest['qtype'] == 1) { ?>
													<label><input type="radio" id="q<?=$quest['id']?>" name="q_<?=$quest['id']?>[]" value="" required><span><?=$subquest['subquestion']?>
															<input type="text" class="i-txt" name="q_<?=$quest['id']?>[]"></label><br>
													<br>
												<?php } else if($subquest['qtype'] == 2) { ?>
													<label><input type="checkbox" id="q<?=$quest['id']?>" name="q_<?=$quest['id']?>[]" value="<?=$subquest['subquestion']?>"><span><?=$subquest['subquestion']?></span></label><br>
													<?php
												} else { ?>

												<?php }
											}
										} else { ?>
											<input type="text" class="i-txt" id="lbl<?=$quest['id']?>" name="q_<?=$quest['id']?>" required><br>
										<?php } ?>
									</div>
								<?php } if(!$isReqSent) {
									if($this->UserId != 0) { ?>
										<button class="btn btn-success" type="submit"><?=$this->localize->get("project", "send")?></button><br>
									<?php } else { ?>
										<button class="btn btn-primary" onclick="subMnForm()" type="button"><?=$this->localize->get("project", "login")?></button><br>
									<?php } ?>
								<?php } ?>
							</form>
						<?php } ?>
						<div class="proj-txt-data proj-size-data">
							<div class="proj-txt-data-pub">	<?=$this->localize->get("projview", "dt-add");?>: <?=$this->build_DateStr($proj['dy'], $proj['dm'], $proj['dd'], $proj['dh'], $proj['dmin'], " в ");?><br></div>
							<div class="proj-txt-data-view"> <?=$this->localize->get("projview", "views");?>: <?=number_format($proj['item_rate'], 0, ".", " ");?><br></div>
							<div class="proj-txt-rat"><?=$this->localize->get('projview','rating').': <span>'.$proj['item_priority_rate'].'<span>'?></div>
						</div>
					</div>



					<?php if(!$proj_is_thing) { ?>
					<div class="col-md-12 col-xs-12 col-lg-12">
						<div class="proj-sub-info row">
							<div class="proj-purp">
								<?php
								if( $projType == PROJ_NEEDHELP )	// Проект по получению помощи
								{
								$confirmed = $this->catmodel->Item_ReqCollected($proj['id'], REQ_STATUS_CONFIRM);

								if($confirmed['sum'] < $proj['amount'])
									$confprog_percnt = $this->catmodel->PercentDone($confirmed['sum'], $proj['amount']);
								else
									$confprog_percnt = 100;

								$tmleft_str = $this->catmodel->Time_LeftFromMinutes($proj['tmleft']);
								$tmprog_percnt = $this->catmodel->PercentDone($proj['tmleft'], $proj['tmall']);

								// Показать отображение для проекта по сбору средств
								if( $proj['amount_type'] == PROJ_TYPE_MONEY ) {
								$targ_str = number_format($proj['amount'], 0, ",", " ")." ".$currency_name;
								$confirm_str = number_format($confirmed['sum'],  2, ",", " ")." ".$currency_name;
								$confavg_str = number_format($confirmed['avg'],  2, ",", " ")." ".$currency_name;
								?>

								<div class="purp-counter"><?=$this->localize->get("projview", "stat-target");?>: <span><?=$targ_str;?></span> <?=$this->localize->get("projview", "stat-up");?> <?=$proj['edd'].' '.$this->build_MonthStr($proj['edm']);?></div>
								<div class="purp-info">
									<div class="col-md-6 col-xs-6 col-lg-6">
										<div class="purp-block purp-block-left">
											<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-sum");?>:</div>
											<div class="block-left-sum"><?=$confirm_str;?></div>
											<svg id="cont1"></svg>
											<?php if(!$this->wohPays[0]['req_amount']) { ?>
												<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-avg");?>: <span class="span-left"><?=$confavg_str;?></span></div>
											<?php } else { ?>
												<div class="block-left-txt proj-size-data"><img src="<?=WWWHOST?>img/face-baloon.png" alt="WayOfHelp" width="30px" height="30px">&nbsp;
													<a class="block-left-txt-lnk" href="#" id="btnshowproj""><?=$this->localize->get("projview", "woh-hlp")?> <span class="way">Way</span><span class="of">Of</span><span class="help">Help</span></a>: <span class="span-left-white"><?=$this->wohPays[0]['req_amount'].$currency_name?></span></div>
											<?php } ?>
											<script>
												$("#cont1").Progress({
													width: 238,
													height: 7,
													percent: <?=$confprog_percnt?>,
													backgroundColor: '#899197',
													barColor: '#6afa7d',
													radius: 4,
													fontSize: 0,
												});
											</script>
										</div>
									</div>
									<div class="col-md-6 col-xs-6 col-lg-6">
										<div class="purp-block purp-block-right">
											<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-tm");?>:</div>
											<div class="block-right-sum"><?=$tmleft_str;?></div>
											<svg id="cont2"></svg>
											<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-num");?>: <span class="span-right"><?=$confirmed['num'];?> <?=$this->localize->human;?></span></div>
											<script>
												$("#cont2").Progress({
													width: 238,
													height: 7,
													percent: <?=$tmprog_percnt;?>,
													backgroundColor: '#899197',
													barColor: '#8eccfc',
													radius: 4,
													fontSize: 0,
												});
											</script>
										</div>
									</div>
									<?php
									}
									else if( $proj['amount_type'] == PROJ_TYPE_HUMAN )	// Показать отображение для проекта по поиску людей
									{
									$targ_str = round($proj['amount'])." ".$this->localize->human;
									$confirm_str = round($confirmed['sum'])." ".$this->localize->human;
									?>
									<div class="purp-counter"><?=$this->localize->get("projview", "stat-target");?>: <span><?=$targ_str;?></span> <?=$this->localize->get("projview", "stat-up");?> <?=$proj['edd'].' '.$this->build_MonthStr($proj['edm']);?></div>
									<div class="purp-info">
										<div class="col-md-6 col-xs-6 col-lg-6">
											<div class="purp-block purp-block-left">
												<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-numhuman");?>:</div>
												<div class="block-left-sum"><?=$confirm_str;?></div>
												<svg id="cont1"></svg>
												<script>
													$("#cont1").Progress({
														width: 238,
														height: 7,
														percent: <?=$confprog_percnt?>,
														backgroundColor: '#899197',
														barColor: '#6afa7d',
														radius: 4,
														fontSize: 0,
													});
												</script>
											</div>
										</div>
										<div class="col-md-6 col-xs-6 col-lg-6">
											<div class="purp-block purp-block-right">
												<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-tm");?>:</div>
												<div class="block-right-sum"><?=$tmleft_str;?></div>
												<svg id="cont2"></svg>
												<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-num");?>: <span class="span-right"><?=$confirmed['num'];?> <?=$this->localize->human;?></span></div>
												<script>
													$("#cont2").Progress({
														width: 238,
														height: 7,
														percent: <?=$tmprog_percnt;?>,
														backgroundColor: '#899197',
														barColor: '#8eccfc',
														radius: 4,
														fontSize: 0,
													});
												</script>
											</div>
										</div>
										<?php
										}
										else if( ($proj['amount_type'] == PROJ_TYPE_CONSULT) || ($proj['amount_type'] == PROJ_TYPE_OTHER) )	// Показать отображение для проекта по поиску консутьтаций
										{
										//$targ_str = round($proj['amount'])." чел.";
										$confirm_str = round($confirmed['sum']);
										?>
										<div class="purp-counter"><?=$this->localize->get("projview", "stat-till");?> <?=$proj['edd'].' '.$this->build_MonthStr($proj['edm']);?></div>
										<div class="purp-info">
											<div class="col-md-6 col-xs-6 col-lg-6">
												<div class="purp-block purp-block-left">
													<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-numreq");?>:</div>
													<div class="block-left-sum"><p><?=$confirm_str;?></p></div>
												</div>
											</div>
											<div class="col-md-6 col-xs-6 col-lg-6">
												<div class="purp-block purp-block-right">
													<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-tm");?>:</div>
													<div class="block-right-sum"><?=$tmleft_str;?></div>
													<svg id="cont2"></svg>
													<div class="block-left-txt proj-size-data"><?=$this->localize->get("projview", "stat-num");?>: <span class="span-right"><?=$confirmed['num'];?> <?=$this->localize->human;?></span></div>
													<script>
														$("#cont2").Progress({
															width: 238,
															height: 7,
															percent: <?=$tmprog_percnt;?>,
															backgroundColor: '#899197',
															barColor: '#8eccfc',
															radius: 4,
															fontSize: 0,
														});
													</script>
												</div>
											</div>
											<?php
											}

											} else {
												echo '<div>';
											}
											?>

											<!--<div class="blured-gig-photo">
											<?php if(isset($pics[1]['filename_big'])) { ?>
												<img src="<?=WWWHOST.$pics[1]['filename_big']?>" class="big-photo" width="660px" height="500px">
											<?php } else { ?>
												<div class="big-nophoto"></div>
											<?php } ?>
										</div>-->
											<div class="col-md-12 col-lg-12 col-xs-12">
												<?php if(strlen(preg_replace('/\s+/', '', strip_tags($proj['descr']))) > 1 ) { ?>
													<div class="proj-sub-next">
														<div class="proj-sub-next-h"><?=$this->localize->get("projview", "hdr-descr");?></div>
														<div class="proj-sub-next-txt"><?=$proj['descr']?></div>
													</div>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<hr>
							</div>
							<?php }

							$show_more_comm = ( (count($this->comlist) > 0) && (count($this->comlist) < $this->comtotal) );

							if(count($this->comlist)>=0) {

								$coms = $this->comlist;

								?>
								<div id="tocomment"></div>

								<div class="pview-comments def-bg">
									<?php
									//if( ($this->UserId != 0)) {
									echo '<div class="com-h3">'.$this->localize->get("projview", "hdr-comment").'</div>';
									//}

									for( $i=0; $i<count($coms); ++$i ) {

										$coms[$i]['content'] = strip_tags($coms[$i]['content'], '<br>');

										echo '<div class="pview-comment-it pview-comm">
								<div class="row">
									<div class="col-md-3 col-xs-3 col-lg-3 pview-comm-border">
										<div class="pview-comm-usr">';
										($coms[$i]['pic']) ? $psrc = (WWWHOST . $coms[$i]['pic']) : $psrc = (WWWHOST . 'img/no-pic.png');
										echo '<a href=' . $this->Page_BuildUrl("users", "viewrev/" . $coms[$i]['author_id']) . '><img class="usr-comm-pic" src=' . $psrc . ' alt="' . ($coms[$i]['account_type'] == USR_TYPE_PERS ? $coms[$i]['name'] . ' ' . $coms[$i]['fname'] : $coms[$i]['orgname']) . '" title="' . ($coms[$i]['account_type'] == USR_TYPE_PERS ? $coms[$i]['name'] . ' ' . $coms[$i]['fname'] : $coms[$i]['orgname']) . '" height="50px" width="50px">
										<span class="pview-usr-ico pview-com-usr">' . ($coms[$i]['account_type'] == USR_TYPE_PERS ? $coms[$i]['name'] . ' ' . $coms[$i]['fname'] : $coms[$i]['orgname']) . '</span></a>
									</div>
									</div>
									<div class="col-md-9 col-xs-7 col-lg-10">
									<div class="pview-comm-txt">
										<div class="pview-comment-msg proj-size-fix">' . $coms[$i]['content'] . '</div>
										<span class="pview-comment-dt proj-size-data">' . $this->localize->get("projview", "dt-comment") . ': ' . $coms[$i]['add_dt'] . '</span>
						
									</div>
									</div>
									
								</div>
								</div>';
										if( $this->UserId != 0 && $coms[$i]['author_id'] != UhCmsApp::getSesInstance()->UserId ) { ?>
											<a href="#answer" class="proj-answer" uname="<?=$coms[$i]['name'].'_'.$coms[$i]['fname']?>"
											   onclick='$("#comment").val("<?= $coms[$i]['name'] . ' ' . $coms[$i]['fname'] . ', ' ?>"); $("#addcomment").attr("uid", <?=$coms[$i]['author_id']?>)' ><?=$this->localize->get("project", "answer")?></a>
										<?php }
									} ?>
									<div id="answer"></div>
									<div class='pview-comm-more'></div>
									<div id="anotherone" class="text-aligndd"><a href="#" id="showmorecomm"><?=($this->comtotal > 5 ? $this->localize->get("projhelp", "showallcom") : '')?></a></div>

								</div>

							<?php }
							if( $this->UserId != 0 ) {
								?>
								<div class="pview-comments-add def-bg">
									<!--action=" //$this->Page_BuildUrl("proj", "addcomment");?>"-->
									<form class="regform" method="POST">
										<input type="hidden" name="action" value="addcomment">
										<input type="hidden" name="projid" value="<?=$proj['id'];?>">
										<a id="formstart" name="formstart"></a>
										<?php
										if( $this->reqdata->has_errors() )
										{
											echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
										}
										?>
										<div class="row">
											<div class="donecom col-xs-12 col-md-12">
												<div class="form-group<?=( !$this->reqdata->ok('comment') ? ' has-error' : '' );?>">
													<!--<h3 class="rdescr-align"><?=$this->localize->get("projview", "lbl-comment");?></h3>-->

													<textarea class="form-control" id="comment" name="comment" uid="0" rows="4"><?=$this->reqdata->comment;?></textarea>
													<div id="alert" class="alert alert-danger" role="alert"><?=$this->localize->get("projview", "err-comment");?></div>
												</div>
											</div>
										</div>
										<div class="p-btn-align btn-form-pad"><input id="addcomment" uid="0" type="button" class="btn btn-success btn-succes-p" onclick="yaCounter34223620.reachGoal('PushButton_OtpravitKommentarij'); return true;" value="<?=$this->localize->get("projview", "btn-comment");?>" /></div>
									</form>
								</div>
								<?php
							} else
								echo '<div class=" col-md-12 col-xs-12 col-lg-12 login-comment"><p>'.$this->localize->get("project", "no-login").'</p><p>'.$this->localize->get("project", "if-you-want").' <a onclick="popWnd(\'logdlg\', \'\');" rel="nofollow">'.$this->localize->get("project", "make-login").'</a>.</p>
								<p>'.$this->localize->get("project", "if-nouser").' <a href='.$this->Page_BuildUrl('registration','person').'>'.$this->localize->get("project", "registered").'</a>.</p></div>';

							$uinfo = $this->catmodel->getDateBox($proj['author_id'])[0];
							?>

							<div class="col-md-12 col-xs-12 col-lg-12 fix-proj-recomended">
								<hr class="sub-hr">
								<?php if(count($this->subProj) > 1) { ?>
									<div class="proj-sub-proj-h"><?=$this->localize->get("project", "watch-with-this")?></div>
									<div class="col-md-12 col-xs-12 col-lg-12">
										<?php $co = 0; for($i=0; $i<count($this->subProj); ++$i) {
											if($co == 3) break;
											if($this->subProj[$i]['id'] == $proj['id'] ) { continue; } else { $co++;?>
												<div class="col-md-4 col-xs-4 col-lg-4">
													<a target="_blank" onclick="yaCounter34223620.reachGoal('PushButton_DrugieProjecti'); return true;" href="<?=$this->Page_BuildUrl("proj", "view/".$this->subProj[$i]['id'])?>"><div class="sub-proj-block">
															<img src="<?=WWWHOST.$this->subProj[$i]['filename_thumb']?>" height="115px" width="100%" alt="<?=$this->subProj[$i]['title']?>" title="<?=$this->subProj[$i]['title']?>">
														</div></a>
													<a target="_blank" onclick="yaCounter34223620.reachGoal('PushButton_DrugieProjecti'); return true;" href="<?=$this->Page_BuildUrl("proj", "view/".$this->subProj[$i]['id'])?>"><p class="sub-proj-name proj-size-fix"><?=mb_strlen($this->subProj[$i]['title']) > 20 ? mb_substr($this->subProj[$i]['title'], 0, 20)."..." : $this->subProj[$i]['title']?></p></a>
												</div>

											<?php }
										} ?>
									</div>

								<?php } ?>
							</div>

						</div>
					</div>
					<div class="col-xs-12 col-md-5 col-lg-4 col-set-h">
						<div class="proj-right-info parent">
							<div class="proj-right-usr-top">
								<div class="info-usr-pic">
									<a href="<?=$this->Page_BuildUrl("users", "viewrev/".$proj['author_id']);?>"><img src="<?=WWWHOST.( $proj['pic'] != "" ? (file_exists($proj['pic']) ? $proj['pic'] : 'img/no-pic.png') : 'img/no-pic.png' );?>" class="usr-pic-img" width="72" height="72" alt="<?=$proj['name']." ".$proj['fname'];?>" title="<?=$proj['name']." ".$proj['fname'];?>" ></a>
								</div>
								<div class="info-usr-block-r">
									<div class="info-usr-name">
										<a href="<?=$this->Page_BuildUrl("users", "viewrev/".$proj['author_id']);?>"><p class="proj-name-size"><?=$proj['name']." ".$proj['fname'];?></p></a>
									</div>
									<div class="info-usr-con">

										<?php if( !$isMy ) { ?>
											<a id="sendmsglnk" href="#" onclick="yaCounter34223620.reachGoal('PushButtonSoobshenie'); return true;"><div class="con-msg con-basic"><img class="con-msg-img" src="<?=WWWHOST?>img/mail-new.png" alt="mail" title="mail"></div></a>
											<!--<a href="#" id="lnkshowtree" onclick="yaCounter34223620.reachGoal('PushButton_Derevo'); return true;" data-uid="<?=$proj['author_id'];?>" title="<?=$this->localize->get("projview", "btn-tree");?>"><div class="con-tree con-basic"></div></a>-->
											<div class="proj-helpandmoney">
												<p><span id="allhlp" onclick="wndAllhlp(-10); yaCounter34223620.reachGoal('PushButton_VsegoPomogRaz'); return true;"><?=$this->localize->get("user", "stat-tothelps")." :"?></span><span class="help-count proj-size-fix">&nbsp;<?=$this->user_info['helpnum']?></span><span class="proj-raz"> <?=$this->localize->get("user", "time")?></span></p>
												<p><span  id="monhlp" onclick="wndAllhlp(0); yaCounter34223620.reachGoal('PushButton_IzNihDengam'); return true;"><?=$this->localize->get("user", "stat-helpmoney").":"?></span> <span class="proj-donhelp-count proj-size-fix">&nbsp;<?=$this->user_info['helpmoney']['totnum'];?></span><span class="proj-raz-fix"> <?=$this->localize->get("user", "time")?></span></p>
												<p><div class="user-loc-loc-txt"><?=$location?></div></p>
												<!--<p><?=$this->localize->get("user", "stat-helpsum");?>: <span class="help-count proj-size-fix">&nbsp;&nbsp;<?=number_format($this->user_info['helpmoney']['totsum'], 2, ",", " ");?> грн.</span>-->
											</div>
										<?php } ?>

									</div>
								</div>
							</div>
							<?php if($proj['amount_type']==0) { ?>
								<div class="donrate"><div class="block-left-sum <?=$proj['amount'] > 999 ? "sum-size" : ""?>"><?=$confirm_str;?> <span><?=$this->localize->get("project", "of")?></span>  <span><?=$targ_str;?></span></div> </div>

								<svg id="cont3"></svg>
								<script>
									$("#cont3").Progress({
										width: 286,
										height: 7,
										percent: <?=$confprog_percnt?>,
										backgroundColor: '#899197',
										barColor: '#6afa7d',
										radius: 4,
										fontSize: 0,
									});
								</script><?php } ?>
							<script>
								$("#prog2").Progress({
									width: 290,
									height: 7,
									percent: <?=(($this->box['user_rate']/60)*100)?>,
									backgroundColor: '#f4f4f4',
									barColor: '#36b247',
									radius: 4,
									fontSize: 0,
								});
							</script>
							<!--</div>-->

							<!--<div class="user-help-info proj-size-data">
					<p><span id="allhlp" onclick="wndAllhlp(-10); yaCounter34223620.reachGoal('PushButton_VsegoPomogRaz'); return true;"><?=$this->localize->get("user", "stat-tothelp");?></span>: <span class="help-count proj-size-fix">&nbsp;&nbsp;<?=$this->user_info['helpnum'];?></span></p>
					<p><span  id="monhlp" onclick="wndAllhlp(0); yaCounter34223620.reachGoal('PushButton_IzNihDengam'); return true;"><?=$this->localize->get("user", "stat-helpmoney");?></span>: <span class="help-count proj-size-fix">&nbsp;&nbsp;<?=$this->user_info['helpmoney']['totnum'];?></span></p>
					<p><?=$this->localize->get("user", "stat-helpsum");?>: <span class="help-count proj-size-fix">&nbsp;&nbsp;<?=number_format($this->user_info['helpmoney']['totsum'], 2, ",", " ");?> грн.</span>
				</div>-->
							<div class="parent btn-getmoney-pos btn-proj-hide">
								<?php
								if( $isMy )
								{
									echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-owner").'</b></div>';
								}
								else if( $proj['status'] == PROJ_STATUS_STOP )
								{
									echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-proj-stoped").'</b></div>';
								}
								else if( $proj['status'] >= PROJ_STATUS_ENDED ) // OR 3 - PROJECT PAYED x% by amount
								{
									echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-proj-ended").'</b></div>';
								}
								else if( $isReqSent )
								{
									if( $reqMy['status'] == REQ_STATUS_NEW )
									{
										echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-helpwait").'</b></div>';
									}
									else if( $reqMy['status'] == REQ_STATUS_CONFIRM )
									{
										//echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-helpdone").'</b></div><div>';
										echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-helpdone").'</b></div>';
									}
									else
									{
										echo '<div class="p-req-type"><b>'.$this->localize->get("projview", "notify-helpdecl").'</b></div>';
									}

									if( $proj['amount_type'] == PROJ_TYPE_MONEY) {
										echo '<div id="addhelp_money" class="btn-getmoney proj-size-fix"> '.$this->localize->get("projview", "btn-money").($proj['is_check'] ? '<img src="'.WWWHOST.'img/icnok1.png">' : '').'</div>';
										$show_addsum_btn = true;
									}

								}
								else
								{
									if( $proj_is_myhelp)
									{
										echo '<div id="get_myhelp" class="btn-getmoney proj-size-fix"> '.$this->localize->get("projview", "btn-gethelp").'</div>';
										$show_myhelp_btn = true;
									}
									else if( $proj_is_thing)
									{
										echo '<div id="get_thing" class="btn-getmoney proj-size-fix"> '.$this->localize->get("projview", "btn-thing").'</div>';
										$show_getthing_btn = true;
									}
									else if( $proj['amount_type'] == PROJ_TYPE_MONEY )
									{
										echo '<div id="addhelp_money" class="btn-getmoney proj-size-fix" >'.$this->localize->get("projview", "btn-money").($proj['is_check'] ? '<img src="'.WWWHOST.'img/icnok1.png">' : '').'</div>';
										$show_addsum_btn = true;
									}
									else
									{
										if(!$mnghd) {
											echo '<div id="addhelp_man" onclick="yaCounter34223620.reachGoal(\'ProjectPushButton_Pomoch_delom\'); return true;" class="btn-getmoney proj-size-fix">' . $this->localize->get("projview", "btn-help") . '</div>';
											$show_addman_btn = true;
										}
									}
								}
								?>
							</div>

							<hr>
						</div>

						<div class="dontree proj-tree-pfix">
							<div class="pview-rel-tit"><?=$this->localize->get("proj", "tree")?></div>
							<div class="dontree cab-tree-size usr-tree">
								<div class="wnd-tree">
									<div class="usr-graph">
										<div id="vis-graph-wrap"></div>
									</div>
								</div>
							</div>
							<hr class="proj-hr-fix">
						</div>

						<?php
						$show_more_req = ( (count($this->reqlist) > 0) && (count($this->reqlist) < $this->reqtotal['num']) );

						if( count($this->reqlist) > 0 ) {
							$reqits = $this->reqlist;
							echo '<div class="pview-rel-hlp">
					<div class="pview-rel-tit">'.$this->localize->get("projhelp", "hdr").'</div>';

							for( $i=0; $i<count($reqits); ++$i ) {
								$uname = ( $reqits[$i]['account_type'] == USR_TYPE_PERS ? $reqits[$i]['name'].' '.$reqits[$i]['fname'] : $reqits[$i]['orgname'] );

								$help_amount = '';
								if( $reqits[$i]['req_type'] == REQ_TYPE_MONEY ) {
									$help_amount = number_format($reqits[$i]['req_amount'], ( $reqits[$i]['req_amount'] > 999 ? 0 : 2 ), ",", "")."<span>".' '.$currency_name."</span>";
								}
								else if( $reqits[$i]['req_type'] == REQ_TYPE_HUMAN ) {
									$help_amount = number_format($reqits[$i]['req_amount'], 0, "", "")." <span>".$this->localize->human."</span>";
								}
								else {
									$help_amount = number_format($reqits[$i]['req_amount'], 0, "", "")." <span>".$this->localize->human."</span>";
								}

								($reqits[$i]['pic_sm']) ? $uprevImg = WWWHOST.$reqits[$i]['pic_sm'] : $uprevImg = WWWHOST.'img/no-pic.png';

								echo '<div class="pview-rel-it pview-rel-uit">
						<table>
						<tr>
							<td>
								<div class="pview-rel-uprev">'.($reqits[$i]['author_id'] != ''
										? '<a target="_blank" onclick="yaCounter34223620.reachGoal(\'PushButtonAVA_Right\'); return true;" href='.$this->Page_BuildUrl("users","viewrev/".$reqits[$i]['author_id']).'><img class="uprev-img" alt="'.$this->localize->get("successfull", "user").' - '.$uname.'" title="'.$this->localize->get("successfull", "user").' - '.$uname.'" src='.$uprevImg.' height="78px" width="78px"></a>'
										: '<img class="uprev-img" alt="'.$this->localize->get("proj", "anon-username").'" title="'.$this->localize->get("proj", "anon-username").'" src='.WWWHOST.'img/no-usr.png'.' height="78px" width="78px">' ).'
								</div>
							</td>
							<td class="rel-td-w">
								<div class="pview-rel-ucomm">'.($reqits[$i]['author_id'] != ''
										? '<a target="_blank" onclick="yaCounter34223620.reachGoal(\'PushButtonSsilka_Right\'); return true;" href='.$this->Page_BuildUrl("users","viewrev/".$reqits[$i]['author_id']).'><div class="pview-rel-upic pview-rel-uname">'.$uname.'</div></a>'
										: '<div class="pview-rel-upic pview-rel-uname">'.$this->localize->get("proj", "anon-username").'</div>' ).'
									<p class="proj-size-data">'.( $reqits[$i]['daysbefore'] == 0 ? $this->localize->get("projhelp", "today") : $reqits[$i]['daysbefore'].' '.$this->DaysStr($reqits[$i]['daysbefore']).' '.$this->localize->get("projhelp", "ago") ).'</p>
									<div class="pview-rel-ucost">'.$help_amount.'</div>
								</div>
							</td>
						</tr>
						</table>
					</div>';
							}
							echo '<div class="pview-rel-more"></div>
					'.( $show_more_req ? '<div class="pview-more proj-size-data"><a href="#" id="showmorereq">'.$this->localize->get("projhelp", "showall").'</a>' : '<div class="pview-more proj-size-data">' ).'</div>				
				</div>';
						}
						?>

						<div class="pview-sects right-usr-sect proj-bottom-fix">

							<?php
							$slist = $this->needhelpsects;
							if( count($slist)>0 ) {
								echo '<div class="pview-sects-tit">'.$this->localize->get("projview", "hdr-need").'</div>
			<table>';

								$prev_pid = 0;

								for($i=0; $i<count($slist); $i++)
								{
									if( $prev_pid != $slist[$i]['parent_id'] )
									{
										if( $i>0 )
										{
											echo '</ul>
							</td>
						</tr>';
										}

										$prev_pid = $slist[$i]['parent_id'];

										$psinfo = $this->catmodel->Catalog_SectInfo($slist[$i]['parent_id']);

										echo '<tr>
						<td><img src="'.WWWHOST.FILE_DIR.$psinfo['filename_thumb'].'" alt="'.$psinfo['name'].'" title="'.$psinfo['name'].'"></td>
						<td>
							<p class="proj-size-fix">'.$psinfo['name'].'</p>
							<ul>';
									}

									$SLNK = $this->Page_BuildUrl("cat", $slist[$i]['url']);

									echo '<li class="proj-size-data">'.$slist[$i]['name'].'</li>';
								}

								echo '</ul>
				</td>
			</tr>';

								echo '</table>';
							}
							?>
						</div>

					</div>
				</div>

				<!-- SCRIPTS FOR PROJ VIEW ONLY -->

				<style type="text/css">
					@import url("<?=WWWHOST?>css/graph.css");
					@import url("<?=WWWHOST?>css/vis.css");
				</style>
				<script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/my_vis.min.js"></script>
				<script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/obj_graph_baloon.js"></script>
				<!--<script type="text/javascript" scr="<?=WWWHOST?>ckeditor/adapters/jquery.js"></script>
				<script type="text/javascript" src="<?=WWWHOST?>ckeditor/ckeditor.js"></script>-->
				<script src="<?=WWWHOST?>js/rateYo/jquery.rateyo.min.js"></script>
				<!-- Add mousewheel plugin (this is optional) -->
				<script type="text/javascript" src="<?=WWWHOST?>js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
				<!-- Add fancyBox -->
				<link rel="stylesheet" href="<?=WWWHOST?>js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
				<script type="text/javascript" src="<?=WWWHOST?>js/fancybox/jquery.fancybox.pack.js"></script>
				<link rel="stylesheet" href="<?=WWWHOST?>js/fancybox/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
				<script type="text/javascript" src="<?=WWWHOST?>js/fancybox/helpers/jquery.fancybox-thumbs.js"></script>

				<!-- END SCRIPTS -->

				<script>
					$(document).ready(function() {

						var graph = new ObjGraph('vis-graph-wrap', '<?=$proj['author_id']?>');
						$( ".graph-ctrl-item" ).hide( "fast", function() {});
						$( "#graph-select-layout" ).hide( "fast", function() {});

						$(".fancybox-thumb").fancybox({
							prevEffect: 'none',
							nextEffect: 'none',
							helpers: {
								thumbs: {
									width: 50,
									height: 50
								}
							}
						});

						<?php if($this->pix == 1) { ?>

							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=263516&uid=52859&h=d8c53be3a7f14ac4313dc33556b46ed8';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262846&uid=52859&h=971ad6656664c4cf1224d65d58deef35';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=263518&uid=52859&h=a15b7315f4c6a310e94c4cc7e50e042d';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262706&uid=52859&h=104cd7ced5ddc1d914bddbbbac80a8b3';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=263498&uid=52859&h=0e8975f480c99514e963c53d02413e4d';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264049&uid=52859&h=ed6961a702d75da23bfdbe5236ec777e';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264051&uid=52859&h=0af331a96cabf666633049c36611338e';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264058&uid=52859&h=5588328941e82adc66fb0096e4bcb7da';
							//
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262768&uid=52859&h=b40d1945564cd10737b6d99fb55ccf74';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264230&uid=52859&h=01cdf5a2b2f3569a471b2de735596d66';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264231&uid=52859&h=5b1f733b297c0cc845ae7b3be5b31ffd';

							//22.09
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264404&uid=52859&h=81740547a1f9b20d7a30aae7b0587a57';
							var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=264374&uid=52859&h=9da7701400268dceedf485097f62d950';
							var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=264382&uid=52859&h=574c71dbce2ecf35ea557a12d357a63d';
							var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=264385&uid=52859&h=0ec15de7ccd066ae617db7a83a333c57';
							//var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=264388&uid=52859&h=e498c0e8047e3ce9aaba511f6e1ebab9';
							var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=264389&uid=52859&h=1823007bb52317a20468c06b99283629';
							var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=264390&uid=52859&h=72f4167370bcf999675db28b8dc1b1ec';
							var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=264392&uid=52859&h=4662b70bb2ba47b985943ed3f512df26';
							var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=264393&uid=52859&h=94ba8aa160cc3a9960eb1003e1970d7e';
							var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=264394&uid=52859&h=6e471923c98ee5354f41417e4c02963c';
							var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=264395&uid=52859&h=194d3e9e9a750581bdedfda941662774';
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264407&uid=52859&h=258eae1bff037bef739272b6992f22b7';
							//26.09
							var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264739&uid=52859&h=1ce6a292a0783326c6b417fe55602181';
						<?php } ?>

						<?php if($is_dream) { ?>
						var rat = "<?=($this->projinfo['star_rate'])*10?>"+'%';
						var fun_counter = 0;
						var userId = <?=$this->UserId?>;
						var itemId = <?=$this->projinfo['id']?>;

						$(function () {
							$("#rateYo").rateYo({
								starWidth: "20px",
								numStars: 10,
								rating: "<?=($this->projinfo['star_rate'])*10?>"+'%',
								precision: 5,
								onChange: function (rating, rateYoInstance) {
									$(this).next().text(Math.round((rating/10) * 100) / 100 );
								},
								onSet: function (rating, rateYoInstance) {
									if(fun_counter > 0) {
										if(userId != 0) {
											var post_req_str = "uid=" + userId + "&itemId=" + itemId + "&rating=" + rating/10 + '&oldRate='+<?=($this->projinfo['star_rate'])?>;
											$.ajax({
												type: "GET",
												url: req_ajx_host + "ajx/setStars/",
												data: post_req_str,
												dataType: "json",
												success: function (data) {
													if(!data) {
														showTipAdder('popnotify-adder', 1, '<?=$this->localize->get("project", "scope-dream")?>', '<?=$this->localize->get("basicjs", "tip-adder")?>');
														var stCount = <?=$pCount['count']?>;
														$('.social-row-count').html('('+(stCount+1)+')');
													}
												}
											});
										} else
											alert('<?=$this->localize->get("project", "login-first")?>');
									}
									fun_counter++;
								}
							});

						});
						<?php } ?>

						<?php if(isset($this->commPopTip) && $this->commPopTip == 1) { ?>
							showTipAdder('popnotify-adder', 1, '<?=$this->localize->get("project", "scope-comm")?>', '<?=$this->localize->get("basicjs", "tip-adder")?>');
						<?php } ?>

						$(".more").click(function() {

							$('.txt-teaser').hide();
							$(".txt-complete").show('slow');
							$('.less').show();
						});

						$(".less").click(function () {
							$('.txt-teaser').show('slow');
							$(".txt-complete").hide('slow');
							$('.more').show();
						})

						/*$(".ico-ok").hover(function() {
						 animCounter('ico-ok-counter', 1);
						 }, function(){
						 animCounter('ico-ok-counter', 0);
						 });

						 $(".ico-fb").hover(function() {
						 animCounter('ico-fb-counter', 1);
						 }, function(){
						 animCounter('ico-fb-counter', 0);
						 });

						 $(".ico-vk").hover(function() {
						 animCounter('ico-vk-counter', 1);
						 }, function(){
						 animCounter('ico-vk-counter', 0);
						 });

						 $(".ico-gp").hover(function() {
						 animCounter('ico-gp-counter', 1);
						 }, function(){
						 animCounter('ico-gp-counter', 0);
						 });
						 $(".ico-tw").hover(function() {
						 animCounter('ico-tw-counter', 1);
						 }, function(){
						 animCounter('ico-tw-counter', 0);
						 });*/

						function animCounter(pid, sh) {
							if(sh) {
								$("#" + pid).css("visibility", "visible");
								$("#" + pid).animate({
									top: "-=20",
									opacity: 100
								}, 300, function () {
								});
							} else {
								$("#"+pid).animate({
									top: "+=20",
									opacity: 0
								}, 300, function() {});
							}
						}

						Share = {
							vkontakte: function(purl) {
								url  = 'http://vkontakte.ru/share.php?';
								url += 'url='          + encodeURIComponent(purl);
								Share.popup(url);
								yaCounter34223620.reachGoal('PushButton_ShareVK'); return true;
							},
							odnoklassniki: function(purl) {
								url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
								url += '&st._surl='    + encodeURIComponent(purl);
								Share.popup(url);
								yaCounter34223620.reachGoal('PushButton_ShareOK'); return true;
							},
							facebook: function(purl) {
								url  = 'http://www.facebook.com/sharer.php?s=100';
								url += '&p[url]='       + encodeURIComponent(purl);
								Share.popup(url);
								yaCounter34223620.reachGoal('PushButton_ShareFB'); return true;
							},
							googleplus: function (purl, ptitle, text) {

								url = 'http://plus.google.com/share?';
								url += 'url=' + encodeURIComponent(purl);
								Share.popup(url);
							},

							twitter: function (purl, text) {
								url = 'http://twitter.com/share?';
								url += 'text='+text;
								url += '&url='+encodeURIComponent(purl);
								Share.popup(url);
							},

							popup: function(url, soc) {
								window.open(url,'','toolbar=0,status=0,width=626,height=436');
							}
						};

						var no_pop = 0;
						var run_popup = 0;
						var show_popup = '<?= $this->projinfo['status'] != 0 ? 0 : 1 ?>';

						<?php
						if( $show_myhelp_btn )
						{
						?>
						$("#get_myhelp, #get_myhelps").bind("click",function(){
							no_pop = 1;

							if( !usr_logged ) {
								popWnd('newreg', '<?=$redir_pars_str?>'+'&type=reg');
								return false
							}

							popWnd('getmyhelpdlg', 'projid=' + <?=$proj['id'];?>);
							return false
						});
						<?php
						}
						if( $show_getthing_btn )
						{
						?>
						$("#get_thing, #get_things").bind("click",function(){
							no_pop = 1;

							if( !usr_logged ) {
								popWnd('newreg', '<?=$redir_pars_str?>'+'&type=reg');
								return false
							}

							popWnd('getthingdlg', 'projid=' + <?=$proj['id']?>);
							return false
						});
						<?php
						}
						if( $show_addsum_btn )
						{
						?>
						$("#addhelp_money, #addhelp_moneys").bind("click",function() {

							// ENHANCED ECOMMERCE FOR GOOGLE ANALITYCS
							ga('ec:addProduct', {
								'id': '<?=$proj['id']?>',
								'name': '<?=$proj['title']?>',
								'category': '<?=$proj['sectname']?>'
							});

							ga('ec:setAction','add', {
								'step': 1
							});
							ga('send', 'pageview');

							yaCounter34223620.reachGoal('ProjectPushButton_Pomoch_dengami');
							ga('send', 'event', 'project_page', 'pushbutton_pomoch_dengami');
							no_pop = 1;

							/*if( !usr_logged ) {
							 popWnd('newreg', '<?=$redir_pars_str?>'+'&type=reg');
							 return false
							 }*/

							popWnd('addmoneydlg', 'projid=' + <?=$proj['id']?>+'&cat=' + '<?=$req_byconv?>' + '&' + '<?=$redir_pars_str?>' + '&to_p=' + '<?=$back_url_p?>' );

							return false
						});

						<?php
						}
						if( $show_addman_btn )
						{
						?>
						$("#addhelp_man, #addhelp_mans").bind("click",function(){
							no_pop = 1;

							if( !usr_logged ) {
								popWnd('newreg', '<?=$redir_pars_str?>'+'&type=reg');
								return false
							}
							popWnd('addmandlg', 'projid=' + <?=$proj['id'];?>);

							return false
						});
						<?php
						}
						//if( $show_send_msg_lnk )
						//{
						?>
						$("#sendmsglnk").bind("click",function(){
							no_pop = 1;

							if( !usr_logged ) {
								popWnd('logdlg', '<?=$redir_pars_str;?>');
								return false
							}
							popWnd('sendmsgdlg', 'uid=' + <?=$proj['author_id'];?> + '&projid=' + <?=$proj['id'];?>);
							return false
						});
						<?php
						//}

						if( $show_more_req )
						{
						?>
						$("#showmorereq").bind("click", function(){
							$(".pview-rel-more").html('<div class="load-progress"><img src="<?=WWWHOST?>img/wait.gif"></div>');
							loadProjReq('<?=$proj['id'];?>'+'&currency=<?=$currency_name?>'+'&woh='+<?=$this->wohNextStep?>,".pview-rel-more");
							$(this).hide();
							return false
						});
						<?php
						}
						?>

						//alert(sessionStorage.getItem('loadPopUp'));

						$(window).scroll(function () {
							run_popup = 0;
							//console.log(run_popup);
						});

						//console.log(readCookie('of'));

						var intervalId = window.setInterval(function() {
							if(run_popup != 0 && show_popup && !no_pop) {
								window.clearInterval(intervalId);
								//alert(sessionStorage.getItem('loadPopUp'));
								if (readCookie('of') == null) {

									if ('<?=$this->popUpType?>' > -1) {
										var inFormOrLink;

										$('a').on('click', function() { inFormOrLink = true; });
										$('form').on('submit', function() { inFormOrLink = true; });

										$(window).on("beforeunload", function() {
											if(show_popup) {
												popWndsm('projpopup', 'type=' + '<?=$this->popUpType?>' + '&pid=' + '<?=$this->projinfo['id']?>');
												show_popup = 0;
												createCookie('of','1',(60 * 60 * 1000));
												return inFormOrLink ? 'Are you sure?' : null;
											}
										})

										$(window).scroll(function () {
											if (show_popup) {
												show_popup = 0;
												createCookie('of','1',(60 * 60 * 1000));
												popWndsm('projpopup', 'type=' + '<?=$this->popUpType?>'+'&pid='+'<?=$this->projinfo['id']?>');
											}
										});

									}
								}
							}
							run_popup = 1;
							//console.log(run_popup);
						}, 5000);



						<?php if($this->UserId) {  ?>

							$('#addcomment').bind('click', function(){
							setTimeout("$('textarea').val('')",1000);

								var has_err = 0;
								if ($("#comment").val() != ""){
									$( "#alert" ).hide( "fast", function() {});
									$('a[href^="#"]').click(function(){
										var el = $(this).attr('href');
										$('body').animate({
											scrollTop: $(el).offset().top}, 2000);
										return false;
									});
								}else{
									$( "#alert" ).slideToggle( "fast", function() {});
									has_err = 1;
								}

								if (!has_err) {
									$.ajax({
										type: "POST",
										url: req_ajx_host + "proj/addcomment/",
										data: "&projid=<?=$proj['id']?>&comment=" + $("#comment").val().replace(/\n/g, '<br/>') + "&to_id=" + $("#addcomment").attr('uid')
									});

									$( ".com-h3" ).after('<div class="pview-comment-it pview-comm">' +
										'<div class="row">' +
										'<div class="col-md-3 col-xs-3 col-lg-3 pview-comm-border">' +
										'<div class="pview-comm-usr">' +
										'<a href="<?=WWWHOST?>users/viewrev/<?=$this->UserId?>">' +
										'<img src="<?=($usernow['pic'] ? WWWHOST.$usernow['pic'] : WWWHOST.'img/no-pic.png')?>" class="usr-comm-pic" height="50px" width="50px">' +
										'<span class="pview-usr-ico pview-com-usr"><?=( $usernow['account_type'] == USR_TYPE_PERS ? $usernow['name'].' '.$usernow['fname'] : $usernow['orgname'] )?></span></a>' +
										'</div>' +
										'</div>' +
										'<div class="col-md-9 col-xs-7 col-lg-10">' +
										'<div class="pview-comm-txt">' +
										'<div class="pview-comment-msg proj-size-fix">' + $("#comment").val().replace(/\n/g, '<br/>') +	'</div>' +
										'<span class="pview-comment-dt proj-size-data"><?=$this->localize->get("projview", "dt-comment").': '.date('d.m.Y h:i')?></span>' +
										'</div>' +
										'</div>' +
										'</div>' +
										'</div>'
									);
								var distance = $('#tocomment').offset().top
								$('html,body').animate({scrollTop:distance},1500);
								}
								return has_err = 0;

							});

							<?php } ?>

						var numc = <?=$this->comtotal?>;
						var numb = 0.5;

						$("#showmorecomm").bind("click", function(){
							$(".pview-comm-more").append('<div class="load-progress"><img src="<?=WWWHOST?>img/wait.gif"></div>');

							loadProjComment(numb, '<?=$proj['id']?>', '.pview-comm-more');

							$(".load-progress").hide(function() {});
							numc-=10;
							if(numc > 9) {
								$( "#anotherone" ).show(function() {});
							}else{
								$( "#anotherone" ).slideToggle(function() {});
							}

							numb+=1;
							return false
						});

						$.ajax({
							type: "POST",
							url: req_ajx_host + "ajx/sharecount/",
							data: "&projid=<?=$proj['id'];?>",
							dataType: 'json',
							success: function (data) {
								$("#sharecount, #sharecounts").html(data[0].sco);
							}
						});

					});



					function wndAllhlp(money_type) {
						popWnd('projview', 'uid=' + <?=$proj['author_id']?>+'&mon='+money_type);
					}

					function noPhoto() {
						$("#nophoto").fancybox({ content: $("#nophotoContent").html() });
					}

					function subMnForm() {
						popWnd('logdlg', '<?=$redir_pars_str;?>');
						return false;
					}

				</script>
				<div id="nophotoContent" style='display: none'>
					<h5 class="nophotost"><?=$this->localize->get('projview','nophoto-txt')?></h5>
				</div>
	<?php
}
?>