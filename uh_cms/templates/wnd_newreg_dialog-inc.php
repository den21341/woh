<?php
    $type = (isset($_GET['type']) ? $_GET['type'] : 'reg');
    $redirurl = ( isset($_GET['redirto']) ? $_GET['redirto'] : '' );

    $FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);

    $FB_REDIRECT_URL = WWWHOST.'login/ajxregFB?to='.urlencode($redirurl);
    $FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);

    $VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);

    $VK_REDIRECT_URL = WWWHOST.'login/ajxregVK?to='.urlencode($redirurl);
    $VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);

    $OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);

    $ok_redirurl = str_replace('/','_',$redirurl);

    $OK_REDIRECT_URL = WWWHOST.'login/redirok?to='.urlencode($ok_redirurl);
    $OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);
?>

<style>
    .wnd-in{
        background: none;
    }
    .wnd-wnd{
        margin-left: 150px;
        background: url(<?=WWWHOST?>img/postreg-back.png) no-repeat 100% 100%;
        background-size: cover;
        width: 40%!important;
    }
</style>

<!----------------------- LOGIN ----------------------->
<p class="req-text req-postreg-text" id="succ" style="display:none"><?=$this->localize->get('newreg','to-mail')?></p>

<div id="hideall">
    <div class="col-md-6 col-xs-6 col-lg-6">
        <p id="text" class="req-text"><?=$this->localize->get('newreg','help-this-proj')?></p>

        <div id="text-big" class="req-text-big" style="display: none">
            <p class="req-text-big"><?=$this->localize->get('newreg','helping-this-proj')?></p>
            <img src="<?=WWWHOST?>img/indx-btn-lg.png">
        </div>
    </div>

    <div class="col-md-6 col-xs-6 col-lg-6">

        <div id="shares" class="req-share req-millenium req-twelve" style="display: none">

            <a target="_blank" href="<?=$FB_LINK;?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"><img src="<?=WWWHOST?>img/btn-log-fb.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-fb");?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"></a>
            <p class="req-or">или</p>
            <a target="_blank" href="<?=$VK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"><img src="<?=WWWHOST?>img/btn-log-vk.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-vk");?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"></a>
            <p class="req-or">или</p>
            <a target="_blank" href="<?=$OK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"><img src="<?=WWWHOST?>img/btn-log-od.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-ok");?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"></a>
            <p class="req-or req-or-last">или</p>
        </div>

        <div id="shows" class="req-div" style="display: none">
            <div class="row form-horizontal">
                <div class="col-xs-12 col-md-12 col-lg-12 req-share req-millenium req-twelve">
                    <div class="form-group">
                        <label for="ulogin" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input type="email" class="form-control req-hor req-fix-emaillog" id="ulogin" name="upass" placeholder="Email"></div>
                    </div>
                    <div class="form-group">
                        <label for="upass" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input type="password" class="form-control req-hor" id="upass" name="upass" placeholder="Password"></div>
                    </div>
                </div>
            </div>
        </div>
        <!------------------------ REGISTRATION ----------------------------->

        <div id="tblock" class="req-div" style="display: none">
            <div class="row form-horizontal">
                <div class="col-xs-12 col-md-12 col-lg-12 req-share req-millenium req-twelve">
                    <div class="form-group">
                        <p class="req-p-id req-fix-sec"><b><?=$this->localize->get('newreg','just')?> <span class="req-color-share"><?=$this->localize->get('newreg','sec')?></p></b></span>
                        <div id="shareg" style="display: none" class="req-reg-share">
                            <a target="_blank" onclick=" yaCounter34223620.reachGoal('ProjectforNonRegistrationUserDoublePushButton_Registration'); return true;" href="<?=$FB_LINK;?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"><img src="<?=WWWHOST?>img/btn-log-fb.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-fb");?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"></a>
                            <p class="req-or">или</p>
                            <a target="_blank" onclick=" yaCounter34223620.reachGoal('ProjectforNonRegistrationUserDoublePushButton_Registration'); return true;" href="<?=$VK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"><img src="<?=WWWHOST?>img/btn-log-vk.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-vk");?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"></a>
                            <p class="req-or">или</p>
                            <a target="_blank" onclick=" yaCounter34223620.reachGoal('ProjectforNonRegistrationUserDoublePushButton_Registration'); return true;" href="<?=$OK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"><img src="<?=WWWHOST?>img/btn-log-od.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-ok");?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"></a>
                            <p class="req-or req-or-last">или</p>
                        </div>
                        <p class="req-p-id" id="errname" style="display: none"><?=$this->localize->get('newreg','err-name')?></p>
                        <label for="regname" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input id="regname" type="text" size="20" maxlength="20" name="regname" class="form-control req-hor req-fix-reqname" placeholder="<?=$this->localize->get('newreg','name')?>"></div>
                    </div>
                    <div class="form-group">
                        <p class="req-p-id" id="errfname" style="display: none"><?=$this->localize->get('newreg','err-fname')?></p>
                        <label for="regfname" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input id="regfname" type="text" size="20" maxlength="20" name="regfname" class="form-control req-hor" placeholder="<?=$this->localize->get('newreg','fname')?>"></div>
                    </div>
                    <div class="form-group">
                        <p class="req-p-id" id="errmail" style="display: none"><?=$this->localize->get('newreg','err-mail')?></p>
                        <label for="regmail" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input id="regmail" type="email" name="regmail" class="form-control req-hor" placeholder="Email"></div>
                    </div>
                    <div class="form-group">
                        <p class="req-p-id" id="errpass" style="display: none"><?=$this->localize->get('newreg','err-pass')?></p>
                        <label for="regpass" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input id="regpass" name="regpass" type="password" class="form-control req-hor" placeholder="<?=$this->localize->get('newreg','pass')?>"></div>
                    </div>
                </div>
            </div>


        </div>
        <!------------------------ ---------- ----------------------------->

        <div class="req-top col-md-12 col-xs-12 col-lg-12">
            <div id="reg" style="display: none"><a id="btnajxreg" onclick="ajxReg()" class="btn btn-primary req-reg-btnfix req-millenium req-twelve"><span class="glyphicon glyphicon-pencil"></span> <?=$this->localize->get("poplog", "btn-reg");?></a></div>

            <div id="login" class="btn req-buttons req-log req-millenium req-twelve">
                <p>Войти</p>
            </div>
            <div id="fix">
                <div style="margin-top: 16px;"></div>
            </div>
            <div id="showbtn" style="display: none">
                <button onclick="ajxLog()" id="btnlog" class="btn req-buttons req-log req-btn-top req-millenium req-twelve"><span class="glyphicon glyphicon-user"></span> <?=$this->localize->get("poplog", "btn-enter");?></button>
            </div>
        </div>
        <div id="lostp" style="display: none" class="req-lostpass"><a id="goto-restorepass<?=(isset($passrest_nobacklink) && $passrest_nobacklink ? '2' : '');?>" href="#"><?=$this->localize->get("poplog", "restpass");?></a></div>

        <div class="req-bot">
            <div><a id="registr" class="btn btn-primary req-botto-fix req-millenium req-twelve"><span class="glyphicon glyphicon-pencil" style="visibility: hidden;"></span> <?=$this->localize->get("poplog", "btn-reg");?></a></div>
        </div>

    </div>
</div>
<script type="text/javascript">

function ajxLog() {
        var has_err = 0;

        if (document.getElementById('ulogin').value!=0) {
            $('#ulogin').css('border-color', '');
        }else{
            $('#ulogin').css('border-color', 'red');
            has_err = 1;
        }

        if (document.getElementById('upass').value!=0) {
            $('#upass').css('border-color', '');
        }else{
            $('#upass').css('border-color', 'red');
            has_err = 1;
        }

    }

    function ajxReg() {

        var has_err = 0;

        var name = /[A-Za-zА-Яа-яЁё]/;

        if (document.getElementById('regname').value.search(name)==0) {
            $( "#errname" ).hide( "slow", function() {});
            $('#regname').css('border-color', '');
        }else{
            $( "#errname" ).show( "slow", function() {});
            $('#regname').css('border-color', 'red');
            has_err = 1;
        }


        if (document.getElementById('regfname').value.search(name)==0) {
            $( "#errfname" ).hide( "slow", function() {});
            $('#regfname').css('border-color', '');
        }else{
            $( "#errfname" ).show( "slow", function() {});
            $('#regfname').css('border-color', 'red');
            has_err = 1;
        }

        if ($('#regmail').val() == '' || !isEmail($('#regmail').val()) || checkEmailbyAjax($('#regmail').val()) == true) {
            $( "#errmail" ).show( "slow", function() {});
            $('#regmail').css('border-color', 'red');
            has_err = 1;
        } else {
            $( "#errmail" ).hide( "slow", function() {});
            $('#regmail').css('border-color', '');
        }

        var pass = /[A-Za-z-0-9]{6,}/;

        if (document.getElementById('regpass').value.search(pass)==0){
            $( "#errpass" ).hide( "slow", function() {});
            $('#regpass').css('border-color', '');

        }else{
            $( "#errpass" ).show( "slow", function() {});
            $('#regpass').css('border-color', 'red');
            has_err = 1;
        }

        if (!has_err) {

            yaCounter34223620.reachGoal('ProjectforNonRegistrationUserDoublePushButton_Registration');

            $( "#hideall" ).hide( "slow", function() {});

            $( "#succ" ).show( "slow", function() {});

            var post_req_str = 'redirto='+'<?=$redirurl?>'+'&regname=' + $('#regname').val() + '&regfname=' + $('#regfname').val() + '&regemail=' + $('#regmail').val() + '&regpass=' + $('#regpass').val();

            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262853&uid=52859&h=191706572c190b0f1aa61e89a0b9640c';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=263516&uid=52859&h=d8c53be3a7f14ac4313dc33556b46ed8';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262846&uid=52859&h=971ad6656664c4cf1224d65d58deef35';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=263518&uid=52859&h=a15b7315f4c6a310e94c4cc7e50e042d';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262706&uid=52859&h=104cd7ced5ddc1d914bddbbbac80a8b3';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=263498&uid=52859&h=0e8975f480c99514e963c53d02413e4d';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264049&uid=52859&h=ed6961a702d75da23bfdbe5236ec777e';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264051&uid=52859&h=0af331a96cabf666633049c36611338e';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264058&uid=52859&h=5588328941e82adc66fb0096e4bcb7da';
            //
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262768&uid=52859&h=b40d1945564cd10737b6d99fb55ccf74';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264230&uid=52859&h=01cdf5a2b2f3569a471b2de735596d66';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264231&uid=52859&h=5b1f733b297c0cc845ae7b3be5b31ffd';
            //22.09
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264404&uid=52859&h=81740547a1f9b20d7a30aae7b0587a57';
            var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=264374&uid=52859&h=9da7701400268dceedf485097f62d950';
            var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=264382&uid=52859&h=574c71dbce2ecf35ea557a12d357a63d';
            var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=264385&uid=52859&h=0ec15de7ccd066ae617db7a83a333c57';
            //var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=264388&uid=52859&h=e498c0e8047e3ce9aaba511f6e1ebab9';
            var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=264389&uid=52859&h=1823007bb52317a20468c06b99283629';
            var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=264390&uid=52859&h=72f4167370bcf999675db28b8dc1b1ec';
            var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=264392&uid=52859&h=4662b70bb2ba47b985943ed3f512df26';
            var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=264393&uid=52859&h=94ba8aa160cc3a9960eb1003e1970d7e';
            var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=264394&uid=52859&h=6e471923c98ee5354f41417e4c02963c';
            var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=264395&uid=52859&h=194d3e9e9a750581bdedfda941662774';
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264407&uid=52859&h=258eae1bff037bef739272b6992f22b7';
            //26.09
            var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264739&uid=52859&h=1ce6a292a0783326c6b417fe55602181';

            $.ajax({
                type: "POST",
                url: req_ajx_host + "ajx/ajxreg/",
                data: post_req_str,
                dataType: "json",
                success: function (data) {
                    if(data == 'true') {}
                    //smplLogIn($('#regmail').val(), $('#regpass').val(), '<?=$redirurl?>');
                }
            });
        }
    }

    $( "#login" ).click(function() {
        $( "#login" ).hide( "fast", function() {});
        $( "#text" ).hide( "slow", function() {});
        $( "#text-big" ).show( "slow", function() {});
        $( "#shares" ).show( "slow", function() {});
        $( "#shows" ).show( "slow", function() {});
        $( "#showbtn" ).show( "slow", function() {});
        $( "#lostp" ).show( "slow", function() {});
        $( "#fix" ).hide( "slow", function() {});

        $( "#registr" ).show( "slow", function() {});
        $( "#tblock" ).hide( "slow", function() {});
        $( "#reg" ).hide( "slow", function() {});
        $( "#shareg" ).hide( "slow", function() {});
    });

    $( "#registr" ).click(function() {
        yaCounter34223620.reachGoal('ProjectforNonRegistrationUserPushButton_Registration');

        $( "#login" ).show( "fast", function() {});
        $( "#text" ).hide( "slow", function() {});
        $( "#text-big" ).show( "slow", function() {});
        $( "#shares" ).hide( "slow", function() {});
        $( "#shows" ).hide( "slow", function() {});
        $( "#showbtn" ).hide( "slow", function() {});
        $( "#lostp" ).hide( "slow", function() {});

        $( "#registr" ).hide( "slow", function() {});
        $( "#tblock" ).show( "slow", function() {});
        $( "#reg" ).show( "slow", function() {});
        $( "#shareg" ).show( "slow", function() {});

    });

    $("#btnlog").on("click", function(){
        checkLoginForm();
        return false;
    });

</script>