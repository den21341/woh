<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$PM = $this->pageModel;

$page = $PM->get_page();

$PAGE_H1 = $page->title;

// Build bread crumbs
$BCHTML = $this->renderBreadcrumbs($PAGE_H1);

$its = $this->msglist;

$need_tab_sel = ($this->viewMode == "needlist");
$do_tab_sel = ($this->viewMode == "dolist");
$give_tab_sel = ($this->viewMode == "givelist");
$my_tab_sel = ($this->viewMode == "sendlist");
$myget_tab_sel = ($this->viewMode == "getlist");

$user_mess_infoo = $this->uinfo;

$loc = (isset($user_mess_infoo['loc_info']['name']) == "" ? $this->localize->get("cabinet", "noselected") : $user_mess_infoo['loc_info']['name']);
$cntry = (isset($user_mess_infoo['loc_info']['countryname']) == "" ? "(".$this->localize->get("cabinet", "noselected").")" : '('.$user_mess_infoo['loc_info']['countryname'].')');
$location = ($loc." ".$cntry);

?>

<div class="resmsg">
	<script type="text/javascript" src="<?=WWWHOST?>js/jquery.progress.js"></script>

	<div class="row-head breacrumb-bgi cabinet-bgi">
		<div class="container">
			<ol class="breadcrumb">
				<?=$BCHTML;?>
			</ol>
			<h1><?=$PAGE_H1;?></h1>

			<div class="cab-nav bar-fix">
				<ul class="nav nav-tabs">
					<li role="presentation" class="cab-tabs-fix"><a href="<?=$this->Page_BuildUrl("cabinet");?>"><?=$this->localize->get("cab", "tab-profile");?></a></li>
					<li role="presentation"<?=($myget_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelpget");?>"><?=$this->localize->get("cabinet", "needhelp");?> <span class="<?=($this->need_services['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->need_services['tab_new'] > 0 ? '+'.$this->need_services['tab_new'] : $this->need_services['tab_tot'])?></span></a></li>
					<li role="presentation"<?=($my_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelp");?>"><?=$this->localize->get("cabinet", "givehelp");?> <span class="<?=($this->my_services['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->my_services['tab_new'] > 0 ? '+'.$this->my_services['tab_new'] : $this->my_services['tab_tot'])?></span></a></li>
					<li role="presentation"<?=($need_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","needhelp");?>"><?=$this->localize->get("cab", "tab-needhelp");?> <span class="<?=($this->needhelp['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->needhelp['tab_new'] > 0 ? '+'.$this->needhelp['tab_new'] : $this->needhelp['tab_tot'])?></span></a></li>
					<li role="presentation"<?=($do_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","dohelp");?>"><?=$this->localize->get("cab", "tab-dohelp");?> <span class="<?=($this->myhelp['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->myhelp['tab_new'] > 0 ? '+'.$this->myhelp['tab_new'] : $this->myhelp['tab_tot'])?></span></a></li>
					<li role="presentation"<?=($give_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","givehelp");?>"><?=$this->localize->get("cab", "tab-givehelp");?> <span class="badge"><?=$this->thinghelp['tab_tot']?></span></a></li>
					<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet", "msgview");?>"><?=$this->localize->get("cab", "tab-msg");?><span class="<?=($this->mymsg['tab_new'] > 0 ? 'badge-red' : 'badge' )?>"><?=($this->mymsg['tab_new'] > 0 ? '+'.$this->mymsg['tab_new'] : 0 )?></span></a></li>
					<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet","mybonus");?>"><?=$this->localize->get("cab", "tab-bonus");?> <span class="badge"><?=$this->mybonus['tab_tot']?></span></a></li>
					<li role="presentation" class="needhelp-size fix-myface"><a href="<?=$this->Page_BuildUrl("users","viewrev").$this->UserId.'/'?>"><?=$this->localize->get("cab", "tab-onlooking");?> <span class="badge"><?=""?></span></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	@import url("<?=WWWHOST?>css/graph.css");
	@import url("<?=WWWHOST?>css/vis.css");
</style>

<script type="text/javascript" src="<?=WWWHOST;?>js_vsgraph/my_vis.js"></script>
<script type="text/javascript">
	isMine = true;
</script>
<script type="text/javascript" src="<?= WWWHOST?>js_vsgraph/obj_graph_baloon.js"></script>
<div class="container msgviewfix">

	<div class="col-md-5 col-xs-4 col-lg-4 messages-left-div">
		<div class="left-usr-ground">
			<div class="<?=$user_mess_infoo['angel_week'] ? 'angel-status-week' : ($user_mess_infoo['angel_day'] ? 'angel-status-day' : 'angel-status-none') ?>"></div>
			<div class="block-photo" style="<?=($user_mess_infoo['angel_week'] || $user_mess_infoo['angel_day']) ? 'margin-top: -80px !important;' : ''?>">
				<img src="<?=WWWHOST.( $user_mess_infoo['pic'] != "" ? $user_mess_infoo['pic'] : 'img/no-pic.png' );?>" class="user-photo-usr" width="120" height="120">
				<div class="user-name" style="<?=mb_strlen($location) > 35 ? 'padding-left: 5px !important;' : ''?>">
					<p><?=$user_mess_infoo['name']?></p>
					<p><?=$user_mess_infoo['fname']?></p>
					<div class="block-u-loc-loc">
						<div class="user-loc-loc-img"></div>
						<div class="user-loc-loc-txt" style="<?=mb_strlen($location) > 35 ? 'font-size: 9.8px !important;' : ''?>"><?=$location?></div>
					</div>

				</div>
			</div>

			<div class="col-xs-12 col-md-12">
				<?php
				// Vkontakte
				$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);

				$VK_REDIRECT_URL = WWWHOST.'login/redirlinkVK';
				$VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);

				// Facebook
				$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);

				$FB_REDIRECT_URL = WWWHOST.'login/redirlinkFB';
				$FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);

				// Odnoklassniki
				$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);

				$OK_REDIRECT_URL = WWWHOST.'login/redirok';
				$OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);

				?>

				<p><?= (!$user_mess_infoo['vk_uid'] || !$user_mess_infoo['fb_uid'] || !$user_mess_infoo['od_uid']) ? $this->localize->get("cabprof","social").':' : '' ?></p>
				<?php if(!$user_mess_infoo['vk_uid']) { ?>
					<a href="<?=$VK_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/vk-link.png" class="cab-soc-mar" alt="" height="25px" width="25px"></a>
				<?php } if(!$user_mess_infoo['fb_uid']) { ?>
					<a href="<?=$FB_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/fb-link.png" class="cab-soc-mar" alt="" height="25px" width="25px"></a>
				<?php } if(!$user_mess_infoo['od_uid']) { ?>
					<a href="<?=$OK_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/ok-link.png" class="cab-soc-mar" alt="" height="25px" width="25px"></a>
				<?php } ?>
			</div>

			<hr>
			<div class="user-loc-info">
				<div class="block-u-loc-face">
					<div class="user-loc-face-img"></div>
					<div class="info-samle"><?=($user_mess_infoo['group_name']);?></div>
				</div>
			</div>
			<hr>
			<div class="user-box-info">
				<div class="user-a-row row">
					<div class="block-u-img col-md-6 col-xs-6 col-lg-6">
						<a href="<?=$this->Page_BuildUrl("info/stat")?>"><div class="user-box-status-img col-lg-3 col-xs-3 col-md-3"></div></a>
						<div class="user-box-status col-lg-9 col-xs-9 col-md-9" <?=(strlen($this->statusBox->getStatus($this->box['status'])) > 10 ? '' : "style='line-height: 30px'")?> ><div class="usr-status-ml mob-counter"><a href="<?=$this->Page_BuildUrl("info/stat")?>"><?=$this->statusBox->getStatus($this->box['status'])?></a></div></div>
					</div>
					<div class="block-u-mon col-md-4 col-xs-4 col-lg-4">
						<a href="<?=$this->Page_BuildUrl("info/power")?>"><div class="user-box-money-img"></div>
							<span class="user-box-money"><span class="usr-status-nocab ang-lh mob-counter"><?=$this->box['money']?></span></span></a>
					</div>
				</div>
				<div class="user-a-row a-row-h row">
					<div class="block-u-img block-ad-img col-md-6 col-xs-6 col-lg-6">
						<a href="<?=$this->Page_BuildUrl("info/angelsinfo")?>">
							<div class="user-box-ad-img ang-anim col-lg-3 col-xs-3 col-lg-3" title="<?=$this->localize->get("view", "ang-day")?>"></div></a>
						<div class="user-box-ang-count ang-wr col-lg-9 col-xs-9 col-md-9 mob-counter"><?=$user_mess_infoo['ad_count']?></div>
					</div>
					<div class="block-u-mon col-md-4 col-xs-4 col-lg-4">
						<a href="<?=$this->Page_BuildUrl("info/angelsinfo")?>">
							<div class="user-box-aw-img ang-anim" title="<?=$this->localize->get("view", "ang-week")?>"></div></a>
						<div class="user-box-ang-count ang-lh mob-counter"><?=$user_mess_infoo['aw_count']?></div>
					</div>
				</div>
			</div>
			<hr>
			<div class="user-rat-info">
				<div class="user-row-rat">
					<div class="user-rat-txt"><?=$this->localize->get("user","rat")?></div>
					<div class="user-rat-rating"><?=$this->box['user_rate']?></div>
				</div>
			</div>
		</div>

		<div class="fbnt-cab-pos">
			<a href="<?=$this->Page_BuildUrl("cabinet","editme");?>"><div class="btn cab-btn-def-two btn-cab-two">
					<div class="col-md-9">
						<p><?=$this->localize->get("cabinet", "redacted")?></p>
					</div>
				</div></a>
		</div>

		<div class="tbnt-cab-pos">
			<a href="<?=$this->Page_BuildUrl("cabinet","edittopic");?>"><div class="btn cab-btn-def-two btn-cab-two">
					<div class="col-md-9">
						<p><?=$this->localize->get("cabinet", "hobbies")?></p>
					</div>
				</div></a>
		</div>

	</div>

	<div class="col-md-7 col-xs-8 col-lg-8 messages-div-value messages-div-fix" style="list-style-type: none">
		<!--<div class="col-md-9 col-xs-9 col-lg-9 messages-div-fix"><div class="messages-search"></div></div>
		<div class="col-md-3 col-xs-3 col-lg-3 btnfix messages-btn-value">
			<div class="btn-bon-do"><p>Поиск</p></div>
		</div>-->

		<?php

		$TOTAL_PAGES = 1;

		if( $this->msglist_totgrpnum > 0 ) {
			$TOTAL_PAGES = ceil($this->msglist_totgrpnum / $this->pn);
		}


		echo '<div id="scroll" class="message-layer messages-scrollfix">
				<div class="scrollbars" id="style-3">
					<div class="force-overflows">
			<div><div id="msg_block_list">';

		for( $i=0; $i<count($its); ++$i ) {

			$user_id = ($its[$i]['user_id'] != UhCmsAdminApp::getSesInstance()->UserId ? $its[$i]['user_id'] : 0);

			if(!$user_id) {
				continue;

			} else {
				$user_mess_info = $this->catmodel->Buyer_Info($user_id);

				$its[$i]['last_mess'] = preg_replace('/<[^>]*>/', ' ', $its[$i]['last_mess']);

				//print_r($user_mess_info['name'].' '.$user_mess_info['fname']);
				echo '
			<div class="col-md-12 col-xs-12 col-lg-12 messages-right-div messages-choise-chat" onclick="openMsgBord('.$user_id.')">
			<div class="col-md-1 col-xs-1 col-lg-1">
				<a href="'.WWWHOST.'users/viewrev/'.$user_mess_info['id'].'/"><img target="_blank" src="'.WWWHOST.($user_mess_info['pic_sm'] != "" ? (file_exists($user_mess_info['pic_sm']) ? $user_mess_info['pic_sm'] : 'img/no-pic.png') : 'img/no-pic.png').'" class="messages-img-radius" width="60" height="60"></a>
			</div>
				<div class="col-md-9 col-xs-9 col-lg-9 messages-last-mess">
					<p class="messages-username badgefix">' . $user_mess_info['name'].' '.$user_mess_info['fname'] . ' '.( $its[$i]['new_msg'] > 0 ? '<span class="badge-red badge-green">'.$its[$i]['new_msg'].'</span>' : '' ).' </p>
					
					<p class="message-lastcomm">'.($its[$i]['to_id'] == UhCmsAdminApp::getSesInstance()->UserId ? '' : $this->localize->get("cabinet", "you").':').' ' .(strlen($its[$i]['last_mess']) > 50 ? (htmlspecialchars(substr($its[$i]['last_mess'], 0, 50))).'...' : (htmlspecialchars($its[$i]['last_mess']))). ' </p>
					
					<br/>
				 </div>
				 <div class="col-md-2 col-xs-2 col-lg-2">
				 <p class="msg-last-dt">' . date("d.m.y H:i", strtotime($its[$i]['latestdt'])) . '</p>
				 </div>
			</div><hr/>';
			}
		}

		echo '</div></div></div></div></div>';

		echo '<div style="padding-left: 19px;list-style-type: none;margin-top: 15px;" id="mes_area"></div>';

		if( count($its) == 0 ) {
			$noitems_str = $this->localize->get("cabmsg", "noitems");

			echo '<div style="padding: 30px 0px 50px 0px;">
			<div class="tbl-noitems">
				<div class="tbl-noitems-txt"><div><span>'.$noitems_str.'</span></div></div>
				<div class="tbl-noitems-face"></div>
			</div>
			<div class="both"></div>
		</div>';
		}


		?>

		<div id="mess_pag" style="padding-left: 18px;"></div>
		<br><br>
	</div>
	<!--<div class="col-md-12 col-xs-12 col-lg-12">
		<div class="cab-tree-size">
			<div class="wnd-tree">
				<div class="cab-graph">
					<div id="vis-graph-wrap"></div>
				</div>
			</div>
		</div>
	</div>-->

	<script type="text/javascript" src="<?=WWWHOST?>js/jquery.timers.min.js"></script>
	<script type="text/javascript" src="<?=WWWHOST?>js/jquery.bootpag.min.js"></script>

	<script>

		//var graph = new ObjGraph('vis-graph-wrap', '<?=$user_mess_infoo['id']?>');

		var user_id = 0;
		var request;
		var load_request;
		var msg_pn = <?=$this->pn?>;
		var msg_co = msg_pn;
		var msg_LIST = [];
		var num_pag = 0;

		onsec_updmsgBord(true);

		$('#mess_pag').bootpag({
			total: <?=$TOTAL_PAGES?>,
			page: 1,
			maxVisible: 10

		}).on('page', function(event, num) {

			num_pag = --num;

			//(num == 1 ? msg_pn = 0 : msg_pn = <?=$this->pn?>);

			$.ajax({
				type: "POST",
				url: req_ajx_host + "ajx/mainmsgbord/",
				data: {'pi': msg_pn, 'pn': msg_pn * num },
				dataType: "text",
				async: false,
				success: function (data) {
					$('#msg_block_list').html('');
					$('#msg_block_list').append(data);

					//msg_LIST = data;

					//onsec_updmsgBord(true);
					onsec_updmsg(false);
				}
			});
		});

		function openMsgBord(uid) {

			if(typeof(request) != 'undefined')
				request.abort();

			onsec_updmsgBord(false);

			request = $.ajax({
				type: "GET",
				url: req_ajx_host + "ajx/loadmsgbord/",
				data: 'uid=' + uid,
				dataType: "json",
				success: function (data) {

					//document.getElementById('mes_area').style.display = 'list-item';
					$('#mes_area').css('display', 'list-item');


					$('#msg_block_list').html('');
					$('#msg_block_list').append(data[0]);


					user_id = uid;

					$( '<div id="back" class="col-md-12 col-xs-12 col-lg-12 messages-back-chat">' +
						'<div>' +
						'<div onclick="closeMsgBord()" class="message-back messages-close-msg">&#10094; <span style="color:#5e6b75;">' + data[1] + '</span>' +
						'</div></div>' +
						'</div>' ).insertBefore( '#scroll' );

					scrollBottom($("#style-3"));

					$(window).scrollTop($(window).scrollTop() + 310);


					history.pushState(null, null, '<?=$_SERVER["REQUEST_URI"]?>');

					window.addEventListener('popstate', function (event) {
						window.location.assign("<?=WWWHOST?>cabinet/msgview/");
					});

					if ($('#mes_area').html() == '') {
						$('#mes_area').html('<textarea id="mes_tx" style="height:80px" class="form-control messages-textarea" dt_src="'+user_id+'"></textarea><div class="btnfix"><button class="btn btn-bon-do message-send-btn" id="mes_tx_ad" onclick="send_tx_msg()" >Отправить</button></div>');
					}

					onsec_updmsg(true);

				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(textStatus);
				}
			});
		}

		function send_tx_msg() {

			if (($("#mes_tx").length > 0)) {

				var text_val = $("#mes_tx").val();
				text_val = $.trim(text_val);

				if(text_val != '') {

					var data_id = $("#mes_tx").attr('dt_src');
					var tx_txt = $.trim(text_val.replace(/\n/g, "<br>"));

					$('textarea').val('');

					if (tx_txt) {

						$.ajax({
							type: "POST",
							url: req_ajx_host + "ajx/addmsg/",
							data: {'to_id': data_id, 'msg': tx_txt},
							dataType: "json",
							success: function (data) {
								$('#msg_block_list').append(data[0]);

								scrollBottom($("#style-3"));
							}
						});	
					}
				}

			}
		}

		function closeMsgBord() {
			$('#back').remove();
			$('#style-3').scrollTop(0);

			onsec_updmsgBord(true);

			if(msg_LIST.length > 0) {
				$('#msg_block_list').html('');
				$('#msg_block_list').append(msg_LIST);

			} else {

				if (typeof(request) != 'undefined')
					request.abort();

				request = $.ajax({
					type: "POST",
					url: req_ajx_host + "ajx/mainmsgbord/",
					data: {'pi': 20, 'pn': 0},
					dataType: "text",
					async: false,
					success: function (data) {
						$('#msg_block_list').html('');
						$('#msg_block_list').append(data);

						//msg_LIST = data;

						onsec_updmsg(false);
						onsec_updmsgBord(true);
					}
				});
			}

			$('#mes_area').css('display', 'none');
			$('#mess_pag').css('display', 'list-item');
		}


		function onsec_updmsg(stop_load) {

			if(stop_load == false) {
				$("#msg_block_list").stopTime();

			} else {

				if (($("#mes_tx").length > 0)) {

					$("#msg_block_list").everyTime(3000, function () {

						if (stop_load == true) {

							if (typeof(request) != 'undefined')
								request.abort();

							request = $.ajax({
								type: "POST",
								url: req_ajx_host + "ajx/updmsgbord",
								data: {"uid": $("#mes_tx").attr('dt_src')},
								dataType: "json",
								success: function (data) {

									if(data[0] != '') {
										$('#msg_block_list').append(data[0]);
										scrollBottom($("#style-3"));
									}
								}
							});
						}
					});
				}
			}
		}

		function onsec_updmsgBord(stop_load) {

		//	onsec_updmsg(false);

			if(stop_load == false) {
				$("#msg_block_list").stopTime();

			} else {
					$("#msg_block_list").everyTime(3000, function () {

						if (stop_load == true) {

							if (typeof(request) != 'undefined')
								request.abort();

							request = $.ajax({
								type: "POST",
								url: req_ajx_host + "ajx/mainmsgbord/",
								data: {'pi': msg_pn, 'pn': msg_pn * num_pag },
								dataType: "text",
								async: false,
								success: function (data) {
									$('#msg_block_list').html('');
									$('#msg_block_list').append(data);

								}
							});
						}
					});
			}
		}

		function scrollBottom(id) {
			if(id.length > 0) {
				id.scrollTop(id.prop("scrollHeight"));
			}
		}

		function addTxBreak() {
			alert('1');
			$(this).val($(this).val() + "<br/>");
		}

	</script>
</div>