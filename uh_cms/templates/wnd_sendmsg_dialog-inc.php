<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$PM = $this->pageModel;

$projid = ( isset($_GET['projid']) ? $_GET['projid'] : 0 );
$uid = ( isset($_GET['uid']) ? $_GET['uid'] : 0 );
$replyto = ( isset($_GET['replyto']) ? $_GET['replyto'] : 0 );

$UserId = UhCmsApp::getSesInstance()->UserId;
$UserGroup = UhCmsApp::getSesInstance()->UserGroup;
$msg_list = $this->ajxModel->loadMsgBord($uid, $UserId);

if( ($uid != 0) && ($UserId != $uid) ) {

	$uinf = $this->ajxModel->get_UserInfo($uid, $this->catLib);

	$reciever_name = ($uinf['account_type'] == USR_TYPE_PERS ? $uinf['name'] . ' ' . $uinf['fname'] : $uinf['orgname']);
	?>
	<div class="wnd-hdr"><?= $this->localize->get("popmsg", "hdr"); ?></div>
	<p>
		<?php
		// echo $PM->get_txtres()->pophelpmoney['text'];
		?>
	</p>
	<div id="doerror" class="wnd-error"></div>
	<div class="smsgfix">
		<!--<input type="hidden" id="rprojid" name="selprojid" value="<?= $projid; ?>">
		<input type="hidden" id="ruid" name="seluid" value="<?= $uid; ?>">
		<input type="hidden" id="rmsgid" name="selmsgid" value="<?= $replyto; ?>">-->
		<div class="row form-horizontal">
			<div class="col-xs-12 col-md-12">
				<div class="">
					<label for="upass" class="control-label">Кому: <span
							class="sendmsg-whom"><?= str_replace("\"", "&nbsp;", $reciever_name); ?></span></label>
					<div style="display: none"><input type="text" class="form-control" readonly
													  value="<?= str_replace("\"", "&nbsp;", $reciever_name); ?>"></div>
				</div>
			</div>
		</div>
		<div id="scroll" class="message-layer messages-scrollfix">
			<div class="scrollbars" id="style-3">
				<div class="force-overflows">
					<div id="msg_block_list">
						<?php
						for($i = 0; $i < count($msg_list); ++$i) {
						 echo '<div class="col-md-12 col-xs-12 col-lg-12 messages-right-div" style="margin: 5px 0;padding: 12px 0px 0px 10px;">
							<div class="col-md-1 col-xs-1 col-lg-1">
								<a href="' . WWWHOST . 'users/viewrev/' . $msg_list[$i]['from_id'] . '/"><img src="' . WWWHOST . ($msg_list[$i]['pic_sm'] != "" ? (file_exists($msg_list[$i]['pic_sm']) ? $msg_list[$i]['pic_sm'] : 'img/no-pic.png') : 'img/no-pic.png') . '" class="messages-img-radius" width="60" height="60"></a>
							</div>
							<div class="col-md-9 col-xs-9 col-lg-9 in-msg" style="padding-left: 22px;">
								<p class="messages-username"><a class="messages-username" href="' . WWWHOST . 'users/viewrev/' . $msg_list[$i]['from_id'] . '/" style="text-decoration:none">' . ($msg_list[$i]['from_id'] == UhCmsApp::getSesInstance()->UserId ? $this->localize->get("cabinet", "you") : $msg_list[$i]['name'] . ' ' . $msg_list[$i]['fname']) . '</a></p>
								<p style="padding: 0px 0px 4px 0px; margin-top: -5px;">' . $msg_list[$i]['message'] . '</p>
							</div>
							<div class="col-md-2 col-xs-2 col-lg-2">
								<p class="msg-last-dt">' . date("d.m.y H:i", strtotime($msg_list[$i]['add_date'])) . '</p>
							</div>

						</div>';
						}
						//echo json_encode([$html]);
						?>

					</div>
				</div>
			</div>
		</div>
		<!--<div class="row form-horizontal">
			<div class="col-xs-12 col-md-12">
				<div class="form-group">
					<div class="col-sm-12"><textarea class="form-control" id="msgtxt" name="msgtxt" rows="4" placeholder="<?=$this->localize->get("popmsg", "ph-msg");?>"></textarea></div>
				</div>
			</div>
		</div>-->
		<div class="row form-horizontal">
			<div class="col-xs-12 col-md-12 ">
				<div class="form-group">
					<textarea id="mes_tx" class="form-control messages-textarea" dt_src="<?=$uid?>"></textarea><div class="btnfix">
						<button class="btn btn-bon-do message-send-btn" onclick="send_tx_msg()"><?=$this->localize->get("popmsg", "btn-send");?></button></div>
					<a href="<?=WWWHOST?>cabinet/msgview/" class="sendmsg-allmsg">Все мои сообщения</a>

					<!--<div class="btnfix"><button id="sendmsgbtn" class="btn btn-bon-do message-send-btn"><?=$this->localize->get("popmsg", "btn-send");?></button></div>
				--></div>
			</div>
		</div>
	</div>
	<style>
		.btn-info{
			float: right;
		}
		.wnd-in{
			background: white;
			border-radius: 15px;
			padding: 10px 40px 20px 40px;
		}
		.wnd-close{
			margin-right: -30px;
		}
		.wnd-hdr{
			text-align: center;
			font-size: 24px;
		}
		.wnd-wnd{
			width:700px;
			height: 450px;
			border-radius: 15px;
		}
		.form-horizontal .form-control{
			height:80px;
		    width: 100%;
			margin-left: 0px;
		}
	</style>
	<script>

		scrollBottom($("#style-3"));

			if(typeof(request) != 'undefined')
				request.abort();

			request = $.ajax({
				type: "POST",
				url: req_ajx_host + "ajx/loadmsgbord/",
				data: 'uid=' + <?=$uid?>,
				dataType: "json",
				success: function (data) {

					//document.getElementById('mes_area').style.display = 'list-item';
					$('#mes_area').css('display', 'list-item');


					$('#msg_block_list').html('');
					$('#msg_block_list').append(data[0]);

					scrollBottom($("#style-3"));

					//$(window).scrollTop($(window).scrollTop() + 310);


					history.pushState(null, null, '<?=$_SERVER["REQUEST_URI"]?>');

					window.addEventListener('popstate', function (event) {
						window.location.assign("<?=WWWHOST?>cabinet/msgview/");
					});

				}
			});


		function send_tx_msg() {

			if (($("#mes_tx").length > 0)) {

				var text_val = $("#mes_tx").val();
				text_val = $.trim(text_val);

				if(text_val != '') {

					var data_id = $("#mes_tx").attr('dt_src');
					var tx_txt = $.trim(text_val.replace(/\n/g, "<br>"));

					$('textarea').val('');

					if (tx_txt) {

						$.ajax({
							type: "POST",
							url: req_ajx_host + "ajx/addmsg/",
							data: {'to_id': data_id, 'msg': tx_txt},
							dataType: "json",
							success: function (data) {
								$('#msg_block_list').append(data[0]);

								scrollBottom($("#style-3"));
							}
						});
					}
				}

			}
		}

			function scrollBottom(id) {
				if(id.length > 0) {
					id.scrollTop(id.prop("scrollHeight"));
				}
			}

	</script>
	<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=H8/yiKQpDSWZs1I1lLY6U*dc4UboAj0q9TQ5zZsIoauQzcE0bdIdigq9oCiwHprT5ANmEfr/6WttKKvoSc9Oso6OBSLhDLfYjb5NerbihZaP4oIZlNleX4/vIwUCMFQexyHsZ6FEcGknL/CnUz9U36wf8*noDAyMAlUUfCy8QOA-';</script>
	<?php
}
?>