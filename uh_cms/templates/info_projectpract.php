
<?php
$BCHTML = $this->renderBreadcrumbs();
?>
<div class="row">
    <div class="top-block top-block-pr col-md-12 col-xs-12 col-lg-12">
        <div class="container">
            <ol class="breadcrumb">
                <?=$BCHTML;?>
            </ol>
        </div>
        <div class="top-block-txt top-block-pr-txt">
            <h1><?=$this->localize->get("info-projectpract", "example")?></h1>
        </div>
    </div>
</div>
<style>
    body{
        font-size: 15px;
    }
</style>
<div class="row">
    <div class="container des-cont-all">
        <div class="fees-about conf-about">
            <div class="page-plan">
                <p><b><?=$this->localize->get("info-projectpract", "content")?></b> [<a id="to-hide" style="cursor: pointer"> <?=$this->localize->get("info-projectpract", "hide")?> </a><a id="to-show" style="display:none;cursor: pointer"> <?=$this->localize->get("info-projectpract", "show")?> </a>]</p>
                <ul class="rise-decimal" id="hide-list">
                    <a href="#decor"><li><?=$this->localize->get("info-projectpract", "decor")?></li></a>
                    <a href="#fundraising"><li><?=$this->localize->get("info-projectpract", "fundraising")?></li></a>
                    <a href="#impressions"><li><?=$this->localize->get("info-projectpract", "guaranteed-shows")?>.</li></a>
                    <a href="#social"><li><?=$this->localize->get("info-projectpract", "social")?></li></a>
                    <a href="#collection"><li><?=$this->localize->get("info-projectpract", "results")?></li></a>
                    <a href="#conclusion"><li><?=$this->localize->get("info-projectpract", "conclusions")?>.</li></a>
                </ul>
            </div>
            <div class="example-info-div"><a target="_blank" href="http://wayofhelp.com/proj/view/2740/" title="Поможем Бондарику встать на ноги !!!"><img class="example-info-img" src="<?=WWWHOST?>img/example.png"></a></div>
            <p id="decor">	&emsp; <?=$this->localize->get("info-projectpract", "for-example")?></p>
            <br><b><p style="text-align: center; font-size: 20px"><?=$this->localize->get("info-projectpract", "recomendation")?></p></b><br>
            <p><b><?=$this->localize->get("info-projectpract", "making")?></b></p>
            <p id="fundraising">&emsp; <?=$this->localize->get("info-projectpract", "moder")?> <a target="_blank" href="<?=WWWHOST?>info/projectinfo/"><?=$this->localize->get("info-projectpract", "tips")?></a> <?=$this->localize->get("info-projectpract", "make-proj")?></p>
            <p id="impressions"><b>2. <?=$this->localize->get("info-projectpract", "fundraising")?></b></p>
            <p>	&emsp;<?=$this->localize->get("info-projectpract", "after-then")?></p>
            <p>&emsp;<?=$this->localize->get("info-projectpract", "im-tell")?> <a target="_blank" href="<?=WWWHOST?>info/projectrise/"><?=$this->localize->get("info-projectpract", "here")?></a>.</p>
            <p id="social"><b>2.1. <?=$this->localize->get("info-projectpract", "guaranteed-shows")?></b></p>
            <p>	&emsp;<?=$this->localize->get("info-projectpract", "project-added")?></p>
            <p><b>2.2 <?=$this->localize->get("info-projectpract", "social")?></b></p>
            <p>	&emsp;<?=$this->localize->get("info-projectpract", "social-work")?></p>
            <p id="collection">	&emsp;<?=$this->localize->get("info-projectpract", "earlier")?></p>
            <p> &emsp;<?=$this->localize->get("info-projectpract", "more-visits")?></p>
            <div class="example-info-div"><a target="_blank" href="http://wayofhelp.com/proj/view/2740/"><img class="example2-info-img" src="<?=WWWHOST?>img/example2.png"></a></div>
            <p id="conclusion"><b><?=$this->localize->get("info-projectpract", "results")?>:</b></p>
            <p> &emsp;<?=$this->localize->get("info-projectpract", "money-auction")?></p>
            <p> &emsp;<?=$this->localize->get("info-projectpract", "money-ofauction")?></p>
            <p><b><?=$this->localize->get("info-projectpract", "conclusions")?>:</b></p>
            <p> &emsp;<?=$this->localize->get("info-projectpract", "experience")?> </p>
            <ul class="rise-decimal">
                <li><?=$this->localize->get("info-projectpract", "big-yours")?> <a target="_blank" href="<?=WWWHOST?>info/power/"><?=$this->localize->get("info-projectpract", "points")?></a> <?=$this->localize->get("info-projectpract", "chanse")?></li>
                <li><?=$this->localize->get("info-projectpract", "row-social")?></li>
                <li><?=$this->localize->get("info-projectpract", "ask-friend")?></li>
                <li><?=$this->localize->get("info-projectpract", "make-repost")?></li>
            </ul>
        </div>
    </div>
</div>
<script>
    $( "#to-hide" ).click(function() {
        $( "#hide-list" ).hide();
        $( "#to-hide" ).hide();
        $( "#to-show" ).show();
    });
    $( "#to-show" ).click(function() {
        $( "#to-show" ).hide();
        $( "#hide-list" ).show();
        $( "#to-hide" ).show();
    });
</script>
