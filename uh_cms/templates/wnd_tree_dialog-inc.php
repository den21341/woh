<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$uid = ( isset($_GET['uid']) ? $_GET['uid'] : 0 );
?>
<div class="wnd-hdr"><?=$this->localize->get("popgraph", "hdr");?></div>
<p></p>
<div class="wnd-tree">
	<div class="cab-graph"><div class="cab-graph-canv">
		<canvas id="maingraph" data-usr="<?=$uid;?>" width="780" height="460" style="margin: 0 auto;"></canvas>
	</div></div>
</div>
<script>
drawMaingraphCabinet('maingraph');
</script>
