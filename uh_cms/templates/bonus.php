<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();
	
	$PAGE_H1 = $page->title;
	if( isset($this->page_h1) )
		$PAGE_H1 = $this->page_h1;
	
	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs("");
?>
<div class="row-head breacrumb-bgi">
	<div class="container">
		<ol class="breadcrumb">
			<?=$BCHTML;?>			
		</ol>
		<h1><?=$PAGE_H1;?></h1>
	</div>
</div>
<div class="row">
	<div class="container">
		<div class="col-xs-5 col-md-3">
			<div class="h3"><?=$this->localize->get("cat", "lcol-hdr");?></div>
			<div class="cat-list">
				<div class="cat-it cat-it-open">
					<table>
					<tr><td><img src="/files/sects/br-bl-0-bonus.png" alt="Бонус"></td><td><a href="<?=$this->Page_BuildUrl("bonus","list");?>" class="cat-color"><span><?=$this->localize->get("cat", "free-bonus");?></span></a></td></tr>
					</table>
				</div>
<?php
	$cat = $this->catmodel;
	
	$sgroup = 0;

	$sect0 = $cat->Catalog_SectLevel($sgroup, 0);
	for( $i=0; $i<count($sect0); $i++ )
	{
		//$isopen0 = $cat->Catalog_SectIsInPath($sect0[$i]['id'], $this->spath);
		
		$caturl = $this->Catalog_BuildUrl($sect0[$i]['url']);

		$ico_file = ( $sect0[$i]['filename_thumb'] != "" ? WWWHOST.FILE_DIR.stripslashes($sect0[$i]['filename_thumb']) : WWWHOST.'img/br-br-1-build.png');
		//if( $isopen0 && ($sect0[$i]['filename_thumb_sel'] != "") )
		//	$ico_file = WWWHOST.FILE_DIR.stripslashes($sect0[$i]['filename_thumb_sel']);
			
	
		echo '<div class="cat-it'.(false ? ' cat-it-open' : '').'">
			<table>
			<tr><td><img src="'.$ico_file.'" alt=""></td><td><a class="cat-color" href="'.$caturl.'"><span>'.$sect0[$i]['name'].'</span></a></td></tr>
			</table>';
		
		echo '</div>';
	}	
?>
			</div>
		</div>
		<div class="col-xs-7 col-md-9 bonus-row master-card bonout">
			<div class="catalog-block">
				<div class="col-lg-12 col-md-12 col-xs-12">
					<div class="col-lg-2 col-md-2 col-xs-2"><img class="catalog-block-img" src="<?=WWWHOST?>img/info-pr-img.png" width="80" height="80"></div>
					<div class="col-lg-10 col-md-10 col-xs-10 catalog-block-text"><span><?=$this->localize->get("catalog", "press-bonus-out")?></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-7 col-md-9 cat-triangle">
			<img src="<?=WWWHOST?>img/triangle-top.png">
		</div>
		<div class="col-xs-7 col-md-9">
			<div class="row clearfix">
				<div class="col-xs-12 col-md-4">
					<?php if( $this->UserId != 0 ) { ?>
							<a class="bon-a-def" href="<?=$this->Page_BuildUrl('bonus', 'add')?>"><div class="bon-create-btn"><img src="<?=WWWHOST?>img/bon-glyph-fire.png" alt="Подвесить бонус"><span><?=$this->localize->get("bonus", "btn-bonus")?><span></div></a>
					<?php } ?>
				</div>
			</div>
		<div class="row bonus-row">
			<div class="col-md-12 col-xs-12 col-lg-12">
				<?php
					$its_total = $this->its_total;
					$its = $this->its;
					$i = 0;

					foreach ($its as $bonit) {
						$short = strip_tags($bonit['descr'], '');

						if (mb_strlen($short) > 105) {
							$short = mb_substr($short, 0, 105) . "...";
						}

						$AUTHOR_LNK = $this->Page_BuildUrl('users', 'viewrev/' . $bonit['author_id']);
						$PROJ_LNK = $this->Page_BuildUrl('bonus', "view/" . $bonit['id']);
						$bonCount = $its[$i]['amount'] - $its[$i]['amount_used'];

						$progress_str = "";
						$tmleft_str = $this->catmodel->Time_LeftFromMinutes($bonit['tmleft']);
						$tmprog_percnt = ($this->catmodel->PercentDone($bonit['tmleft'], $bonit['tmall']));
						$proj_avatar = WWWHOST . ($bonit['pic_sm'] != "" ? $bonit['pic_sm'] : 'img/no-pic.png');
						$proj_pic = '';

						if (isset($bonit['photos']) && (count($bonit['photos']) > 0)) {
							$proj_pic = $bonit['photos'][0]['filename_ico'];
						}

						if(!($i % 3)) {
							echo '</div>';
							echo '<div class="bon-stack">';
						}
						$short_title = $bonit['title'];
						if( mb_strlen($bonit['title']) > 42 ) {
							$short_title = mb_substr($bonit['title'],0,42)."...";
						}
						?>

						<div class="bon-single">
							<?=(!$bonCount ? '' : '<a href="'.$PROJ_LNK.'">')?><img <?=(!$bonCount ? 'id="'.$bonit['id'].'"' : '')?> onclick="$('#<?=$i?>').show();$('#<?=$bonit['id']?>').css('filter', 'blur(3px) grayscale(80%)');setTimeout(function() { $('#<?=$i?>').hide(); }, 1500);setTimeout(function() { $('#<?=$bonit['id']?>').css('filter', 'none'); }, 1500);" class="cursor-active" src="<?=WWWHOST.( $proj_pic != "" ? (file_exists($proj_pic) ? $proj_pic : 'img/no-pic.png') : 'img/no-pic.png' )?>" alt="<?=$bonit['title']?>" title="<?=(!$bonCount ? 'Бонус уже выигран' : $bonit['title'])?>" alt="<?=(!$bonCount ? 'Бонус уже выигран' : $bonit['title'])?>" width="100%" height="250px"><?=(!$bonCount ? '' : '</a>')?>
							<div <?=(!$bonCount ? 'id="'.$i.'"' : '')?> class="active-div">Бонус уже выигран</div>
							<?=(!$bonCount ? '' : '<a href="'.$PROJ_LNK.'">')?><div id="btngetbon" onclick="$('#<?=$i?>').show();$('#<?=$bonit['id']?>').css('filter', 'blur(3px) grayscale(80%)');setTimeout(function() { $('#<?=$i?>').hide(); }, 1500);setTimeout(function() { $('#<?=$bonit['id']?>').css('filter', 'none'); }, 1500);yaCounter34223620.reachGoal('PushButtonNaKonkretnijBonus'); return true;" class="bon-get-btn <?=(!$bonCount ? 'disabledbutton' : '')?> " title="<?=(!$bonCount ? 'Бонус уже выигран' : '')?>" alt="<?=(!$bonCount ? 'Бонус уже выигран' : '')?>"><img src="<?=WWWHOST?>img/bon-gliph-btn.png" alt="Получить бонус"><span>Получить бонус</span></div><?=(!$bonCount ? '' : '</a>')?>
							<div class="bon-title"><?=$short_title?></div>
							<p class="bon-left"><?=$this->localize->avail?>: <span><?=$bonCount?> штук</span></p>
							<p class="bon-descr"><?=$short?></p>
							<hr>
							<div class="col-md-8 col-lg-8 col-xs-8">
								<div class="proj-tm-txt"><?=$this->localize->get("cat", "it-started").': '.$its[$i]['stdt']?></div>
								<div class="proj-tm-progress">
									<!--<span><img src="<?=WWWHOST?>img/cat-progress-act.png" width="<?=$tmprog_percnt?>" height="8" alt="Осталось времени" title="Осталось времени"></span>-->
									<br>
									<?=$tmleft_str?>
								</div>
							</div>
							<div class="col-md-4 col-lg-4 col-xs-4">
								<a href="<?=$AUTHOR_LNK?>" class="bon-usr-a"><img src="<?=WWWHOST?>img/bon-usr.png" alt="Пользователь - <?=$bonit['name']?>" title="Пользователь - <?=$bonit['name']?>"><span><?=$bonit['name']?></span></a>
							</div>
						</div>
						<div class="bon-deli"></div>

					<?php if(!($i % 3)) {
							//echo '</div>';
						} $i++;
					} ?>
				<div class="bon-bot"></div>
			</div>
		</div>

		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
		var disable = '<?=$bonCount?>';
		if(!disable) {
			$('#btngetbon').addClass("disabledbutton");
		}


	});
</script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=aLR7H1p2LKuTHmEPXnW06ow9LY3mMV2emynuZ*DCzAdDUXq8jA/C*gDCCXmuo/FKFEjRekUqzqaBRynz1hYdeVcxsvXN1y8o8MvqWLJec4JfrNo/MB3o6BjwoO57qWq6szMoYXvLUmRUcJrafYG7pLUId*qH962gyQrNT3p1UF8-';</script>
