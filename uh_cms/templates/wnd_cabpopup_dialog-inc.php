<?php

    $type = (isset($_GET['type']) ? $_GET['type'] : 0);

    $uid = UhCmsApp::getSesInstance()->UserId;
    $shareTitle = "";
    $shareText = "";

    $buyer_info = $this->catLib->Buyer_Info($uid);
    $shareTitle = $this->localize->get('popup','postreg-sharetit');
    $pic = WWWHOST.($buyer_info['pic_sm'] ? $buyer_info['pic_sm'] : 'img/no-pic.png');
    //print_r($shareTitle);
    $shareText = $this->localize->get('popup','postreg-share');

if(!$type) { ?>
<div class="post-reg">
<h3><?=$this->localize->get('cabpopup','road-to-1000')?></h3>
<p><?=$this->localize->get('cabpopup','we-offer')?>
    <ul>
        <li><span class="woh-blue"><?=$this->localize->get('cabpopup','your-tree')?></span></li>
        <li><span class="woh-blue"><?=$this->localize->get('cabpopup','power-up')?></span></li>
    </ul>
    <b><?=$this->localize->get('cabpopup','good-start')?></b></p>
    <p><?=$this->localize->get('cabpopup','share')?></p>
</div>

<!--<script src="<?=WWWHOST?>js/watermark.min.js"></script>-->

<div class="post-reg-btn">
    <div class="share-line-ico">
        <div class="socline-block">
            <a class="svk" onclick="Share.facebook('http://wayofhelp.com/','<?=$shareTitle?>', '<?=$shareText?>')"><div class="line-ico ico-fb"></div></a>
        </div>
        <div class="socline-block">
            <a class="svk" onclick="Share.vkontakte('http://wayofhelp.com/', '<?=$shareText?>')" ><div class="line-ico ico-vk"></div></a>
        </div>
        <div class="socline-block">
            <a class="svk" onclick="Share.odnoklassniki('http://wayofhelp.com/','<?=$shareTitle?>', '<?=$shareText?>')"><div class="line-ico ico-ok"></div></a>
        </div>
        <div class="socline-block">
            <a class="svk" onclick="Share.twitter('http://wayofhelp.com/','<?=$shareTitle.', '.$shareText?>')"><div class="line-ico ico-tw"></div></a>
        </div>
    </div>
</div>
<div id="watermark_img"></div>
<script>
    $(document).ready(function () {
        var pic = '<?=$pic?>';
        var shareTit = "<?=$this->localize->get('infoother','user')?> "+"<?=$buyer_info['name']?>"+" "+"<?=$buyer_info['fname']?>" + "<?=$shareTitle?>";

        Share = {
            vkontakte: function(purl, text) {
                var vk_id = '<?=$buyer_info['vk_uid']?>';
                if(vk_id) {
                    $.getJSON('http://api.vkontakte.ru/method/users.get?uids=' + '<?=$buyer_info['vk_uid']?>' + '&fields=photo_200,status&callback=?', function (resp) {
                        shareTit = 'Пользователь ' + resp.response[0].first_name + ' ' + resp.response[0].last_name + '<?=$shareTitle?>';
                        pic = resp.response[0].photo_200;
                    });
                }
                url = 'http://vkontakte.ru/share.php?';
                url += 'url=' + purl;
                url += '&title=' + encodeURIComponent(shareTit);
                url += '&description=' + encodeURIComponent(text);
                url += '&image=' + encodeURIComponent(pic);
                Share.popup(url);
            },
            odnoklassniki: function(purl, text) {
                url  = 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1';
                url += '&st.comments=' + encodeURIComponent(shareTit);
                url += '&st._surl='    + encodeURIComponent(purl);
                Share.popup(url);
            },
            facebook: function(link, title, descr) {
                var facebook_appID = <?=FB_APP_ID?>;
                url = "https://www.facebook.com/dialog/feed?app_id="+ facebook_appID +    "&link=" + encodeURIComponent(link)+
                    "&picture=" + encodeURIComponent(pic) +
                    "&name=" + encodeURIComponent(shareTit) +
                    "&description=" + encodeURIComponent(descr) +
                    "&redirect_uri=https://www.facebook.com";
                Share.popup(url);
            },
            twitter: function(purl, ptitle) {
                url  = 'http://twitter.com/share?';
                url += 'text='      + encodeURIComponent(shareTit);
                url += '&url='      + encodeURIComponent(purl);
                url += '&counturl=' + encodeURIComponent(purl);
                Share.popup(url);
            },

            popup: function(url, soc) {
                window.open(url,'','toolbar=0,status=0,width=626,height=436');
            }

        };

        $('.svk').bind('click', function () {
            setUserHelp('<?=$buyer_info['id']?>', '1422');
        });

        function postToFeed(title, desc, url, image) {
            var obj = {
                method: 'feed',
                link: url,
                picture: 'http://www.url.com/images/' + image,
                name: title,
                description: desc
            };

            function callback(response) {
            }

            FB.ui(obj, callback);
        }

    });
</script>
<?php } else if ($type == 1) { ?>
    <div class="cabpop-addph">
        <img src="<?=WWWHOST?>img/face-noreviews.png" alt="<?=$this->localize->get('cabpopup','add-photo')?>" title="<?=$this->localize->get('cabpopup','add-photo')?>">
        <p><a href="<?=$this->Page_BuildUrl('cabinet','editme')?>"><?=$this->localize->get('cabpopup','add-avatar')?></a> <?=$this->localize->get('cabpopup','in-profile')?></p>
    </div>
<?php } ?>