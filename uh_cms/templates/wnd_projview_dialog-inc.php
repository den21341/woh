<?php

$uid = ( isset($_GET['uid']) ? $_GET['uid'] : 0 );
$mon = ( isset($_GET['mon']) ? $_GET['mon'] : -10 );

$its = $this->catLib->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM, '',  -1, -1, PROJ_STATUS_ALL, 0, $mon);
$its_count = $this->catLib->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM, '',  -1, -1, PROJ_STATUS_ALL, 0, $mon, 1);

?>
<div class="review-tbl rev-usr-table wnd-rev-tbl">
    <div class="tbl-help col-lg-2 col-md-2 col-xs-2"><?=$this->localize->get('user','tbl-projhelp')?></div>
    <a href="#" onclick="wndGetTree(<?=$uid?>)"><div class="tbl-tree-btn col-lg-3 col-md-3 col-xs-3">
            <img src="<?=WWWHOST?>img/usr-s-tree.png" alt="ерево пользователя" title="Дерево пользователя">
            <p><?=$this->localize->get('user','tbl-btntree')?></p></a>
</div>
<table>
    <tr>
        <th><?=$this->localize->get("user", "tbl-proj")?></th>
        <th><?=$this->localize->get("user", "tbl-feed")?></th>
        <th><?=$this->localize->get("user", "tbl-rate")?></th>
    </tr>

    <?php for($i=0; $i<$its_count; ++$i) {
        if(!isset($its[$i])) break;
        if(!isset($its[$i]['req_amount'])) break;
        $its_rate = $this->catLib->getBuyerReqRate($its[$i]['item_id']);

        if(empty($its_rate)) {
            $its_rate[0]['rate_comments'] = 'Спасибо';
            $its_rate[0]['rate'] = 10;
            $its_rate[0]['author_id'] = $its[$i]['uid'];
        }

        ?>

        <tr>
            <td>
                <div class="p-img"><img src=<?=WWWHOST.$its[$i]['pic_sm']?> heigh="60px" width="60px" alt=""></div>
                <div class="p-txt">
                    <div class="p-tit"><a href="<?=$this->Page_BuildUrl("proj", "view/".$its[$i]['item_id']).'">'.$its[$i]['title2']?></a></div>
                        <div class="p-date"><?=$its[$i]['add_date']?></div>
                </div>
            </td>
            <td>
                <div class="r-comment"><?=$its_rate[0]['rate_comments']?></div>
                <div class="r-author"><a href="<?=$this->Page_BuildUrl("users", "viewrev/".$its_rate[0]['author_id'])?>"><?=($its[$i]['account_type'] == USR_TYPE_PERS ? $its[$i]['name'].' '.$its[$i]['fname'] : $its[$i]['orgname'])?></a></div>
            </td>
            <td>
                <div class="r-rate">
                    <span><?=$its_rate[0]['rate']?></span>
                    <div class="p-progress"><img src="<?=WWWHOST.'img/rateorg-'.($its_rate[0]['rate'])?>.png" alt=""></div>
                </div>
            </td>
        </tr>
    <?php }

    if( $its_count == 0 )
    {
        echo '<tr><td colspan="3" class="noitems">
                    <div class="tbl-noitems">
                        <div class="tbl-noitems-txt">
                            <div><span>'.$this->localize->get("user", "tbl-norate").'</span></div>
                        </div>
                        <div class="tbl-noitems-face"></div>
                    </div>
                </td></tr>';
    }

    echo '</table>
    </div>';
    ?>

    <script>
        function wndGetTree(id) {
            popWnd('showtreedlg', 'uid=' + id);
        }
    </script>
