	<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();
	$pname = $PM->getPageName();
	
	$page = $PM->get_page();
	
	$UserId = $PM->ses->UserId;

	$show_popup = ($UserId ? $this->catmodel->checkShowPopup($UserId)[0]['show_popup'] : -1);
	$lnk = $_SERVER['REQUEST_URI'];

	preg_match('/\/registration\//', $lnk, $matches);

	$show_popup = (isset($matches[0]) ? 0 : $show_popup);

	if(isset($_GET['gclid'])) {
		setcookie("gclid", $_GET['gclid'], time() + 60 * 60 * 24, '/');
	}

	if(isset($_GET['utm_source'])) {
		setcookie("ut_s", $_GET['utm_source'], time() + 60 * 60 * 24, '/');
	}

	if(isset($GET['utm_compaing'])) {
		setcookie("ut_c", $_GET['utm_compaing'], time() + 60 * 60 * 24, '/');
	}

	if(isset($_GET['s_ga'])) {
		setcookie("s_ga", $_GET['s_ga'], time() + 60 * 60 * 24, '/');
	}

	if(isset($_GET['a_aid'])) {
		setcookie("aid", $_GET['a_aid'], time() + 60 * 60 * 24, '/');
	}
	
	////////////////////////////////////////////////////////////////////////////
	if( ($pname == "catalog") || ($pname == "product") )
	{
		echo '<div class="both"></div>

		<div class="seen-pan">
			<div class="lbl-hdr"><span class="hdr-w">'.$this->localize->get("footer", "you-see").'</span></div>
			<div class="seen-blk">
				<ul id="seen-list" class="jcarousel-skin-seen">';

		$seenarr = Product_SeenList();
		if( count($seenarr) > 0 )
		{
			for( $i=0; $i<count($seenarr); $i++ )
			{
				$pit = Product_Info( $LangId, $seenarr[$i] );

				if( empty($pit['id']) )	continue;

				$PURL = Product_BuildUrl($LangId, $pit['id'], $pit['prod_url'], $pit['make_url'], $pit['sect_url']);

				$ico_f = $IMGHOST."img/no-photo-sm.gif";
				$ico_w = $NOPHOTO_S_W;
				$ico_h = $NOPHOTO_S_H;

				if( count($pit['pics']) > 0 )
				{
					$p = $pit['pics'][0];

					$ico_f = $p['ico'];
					$ico_w = $p['ico_w'];
					$ico_h = $p['ico_h'];

					$dsize = Image_RecalcSize( $p['ico_w'], $p['ico_h'], 120, 100 );
					$ico_w = $dsize['w'];
					$ico_h = $dsize['h'];
				}

				$costobj = $pit['price'];
				if( $pit['is_series'] )
				{
					$costobj = Product_SubmodsMinCost( $pit['id'] );
				}

				/*
				echo '<li>
					<div class="seen-it">
						<div class="seen-img"><table><tr><td><a href="'.$PURL.'"><img src="'.$ico_f.'" width="'.$ico_w.'" height="'.$ico_h.'" alt="'.$pit['model'].'" /></a></td></tr></table></div>
						<div class="seen-mod"><a href="'.$PURL.'">'.TrimTo($pit['make'].' '.$pit['model'], 53).'</a></div>
						<div class="seen-cost">'.( $costobj['cost']['pdb'] != 0 ? ($pit['is_series'] ? 'от ' : '').Show_Price( $costobj['cost']['g'] ).' '.$CURRENCY_NAME : 'заказ' ).'</div>
					</div>
				</li>';
				*/

				echo '<li>
				<div class="bc-it">
					<div class="bc-img"><table><tr><td><a href="'.$PURL.'"><img src="'.$ico_f.'" width="'.$ico_w.'" height="'.$ico_h.'" alt="'.$pit['model'].'" /></a></td></tr></table></div>
					<div class="bc-mod"><a href="'.$PURL.'">'.TrimTo($pit['make'].' '.$pit['model'], 53).'</a></div>
					<div class="bc-cost">'.( $costobj['cost']['pdb'] != 0 ? ($pit['is_series'] ? 'от ' : '').Show_Price( $costobj['cost']['g'] ).' '.$CURRENCY_NAME : $this->localize->get("footer", "order") ).'</div>
				</div>
				</li>';
			}
		}

		echo '</ul>
			</div>
			<span class="indmakes-bot"></span>
		</div>';
	}
?>

<!-- JQueryUI -->
<link href="<?=WWWHOST?>css/jquery-ui.min.css" rel="stylesheet">
<link href="<?=WWWHOST?>css/jquery-ui.theme.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?=WWWHOST.'css/owl.carousel.css'?>">
<script src="<?=WWWHOST?>js/Ubjh_300-Ubjh_700.font.js"></script>
<script src="<?=WWWHOST?>js/basic.js"></script>
<?php
/*
<script src="<?=WWWHOST?>js/basic.min.js"></script>
*/
?>

<footer>
	<div class="container-fluid footer-container">
		<div class="container foot-xs-cont">
			<!--noindex--><div class="col-md-2 col-lg-2 col-xs-2 foot-block foot-block-f">
				<p><?=$this->localize->get("footer", "start")?></p>
				<ul>
					<li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","siterules");?>"><?=$this->localize->get("footer", "rights")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","howitwork");?>"><?=$this->localize->get("footer", "howitwork")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('cat','only_success')?>"><?=$this->localize->get("footer", "good-proj")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','specrules')?>"><?=$this->localize->get("footer", "rights-sec")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=($UserId == 0 ? $this->Page_BuildUrl("registration","person") : $this->Page_BuildUrl("cabinet") )?>"><?=($UserId == 0 ? $this->localize->get("footer", "register") : $this->localize->get("footer", "mycab") )?></a></li>
				</ul>
			</div><!--/noindex-->
			<div class="col-md-2 col-lg-2 col-xs-2 foot-block">
				<p><?=$this->localize->get("footer", "know-more")?></p>
				<ul>
					<li><a class="foo-a lnkviewpromo" href="#"><?=$this->localize->get("footer", "watch-video")?></a></li>
					<li class="foot-txt-gr"><a class="foo-a" href="<?=$this->Page_BuildUrl('info','tree')?>"><?=$this->localize->get("footer", "good-deal-tree")?></a></li>
					<li><a class="foo-a" href="<?=$this->Page_BuildUrl("faq","");?>">FAQ</a></li>
					<li class="foot-txt-gr"><a class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules");?>"><?=$this->localize->get("footer", "rating-status")?></a></li>
					<li><a class="foo-a" href="<?=$this->Page_BuildUrl('info','rules').'#cathelp'?>"><?=$this->localize->get("footer", "help-cat")?></a></li>
				</ul>
			</div>
			<!--noindex--><div class="col-md-2 col-lg-2 col-xs-2 foot-block">
				<p><?=$this->localize->get("footer", "contact")?></p>
				<ul>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","about");?>"><?=$this->localize->get("footer", "ofus")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>"><?=$this->localize->get("footer", "contacts")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>"><?=$this->localize->get("footer", "support")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#sendmsg'?>"><?=$this->localize->get("footer", "questions")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','about').'#ourteam'?>"><?=$this->localize->get("footer", "crew")?></a></li>
				</ul>
			</div><!--/noindex-->
			<!--noindex--><div class="col-md-3 col-lg-3 col-xs-3 foot-block footbl-sm">
				<p><?=$this->localize->get("footer", "other")?></p>
				<ul>
					<li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("publ","helpwall");?>"><?=$this->localize->get("footer", "good-wall")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','rules').'#copl'?>"><?=$this->localize->get("footer", "complaints")?></a></li>
					<li><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl('info','sanction')?>"><?=$this->localize->get("footer", "sanctions")?></a></li>
					<li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules").'#ratproj';?>"><?=$this->localize->get("footer", "up-projrating")?></a></li>
					<li class="foot-txt-gr"><a rel="nofollow" class="foo-a" href="<?=$this->Page_BuildUrl("info","ratrules").'#ratuser';?>"><?=$this->localize->get("footer", "up-userrating")?></a></li>
				</ul>
			</div><!--/noindex-->
			<!--noindex--><div class="col-md-3 col-lg-3 col-xs-3 foot-block footbl-bg">

					<?php

				if($_SERVER['REQUEST_URI'] != '/info/specrules/') { ?>
					<div class="footer-fb-back">
						<div class="footer-db-hat">
							<p class="footer-hat-txt"><?=$this->localize->get("footer", "add-fb")?></p>
						</div>
						<div class="footer-wh-img">
							<img class="footer-img-top" src="<?=WWWHOST?>img/wayofhelp-footer.png">
							<p class="footer-wh-c">&copy; <?=$this->localize->get("footer", "copyright")?></p>
						</div>
						<div class="footer-iframe-fb">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fwayofhelp&width=300&layout=standard&action=like&size=small&show_faces=true&share=true&height=80&appId" width="300" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
					</div>
				<?php } else { ?>
					<div class="footer-logo-block">
						<a rel="nofollow" href="<?=WWWHOST?>"><div class="footer-logo"></div></a>
						<div class="all-right-res">© wayofhelp.com - Все права защищены</div>
						<div class="webmon-2 col-lg-6"></div>
						<div class="webmon-1 col-lg-6"></div>
						<a rel="nofollow" class="foot-at-a" href="https://passport.webmoney.ru/asp/certview.asp?wmid=235515103904" target="_blank">Проверить аттестат</a>
					</div>
				<?php } ?>

			</div><!--/noindex-->
		</div>
	</div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?=WWWHOST;?>js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->

<div class="wnd-bg-sm" id="wnd-bg-sm"></div>
<div class="wnd-wnd-sm" id="wnd-wnd-sm">
	<div class="wnd-in-sm">
		<div class="wnd-close-sm"><a href="#" class="wnd-close-img"></a></div>
		<div class="wnd-cont-sm" id="wnd-conts">
			<div class="wnd-loading-sm"></div>
		</div>
	</div>
</div>

<div class="wnd-bg" id="wnd-bg"></div>
<div class="wnd-wnd" id="wnd-wnd">
	<div class="wnd-in">
		<div class="wnd-close"><a href="#" class="wnd-close-img"></a></div>
		<div class="wnd-cont" id="wnd-cont">
			<div class="wnd-loading"></div>
		</div>
	</div>
</div>

<!--<div id="wnd-social" class="social-wnd">
	<div class="social-wnd-tit">		
		<div class="social-wnd-close"><a href="#"></a></div>
		<span></span>
	</div>
	<div class="social-wnd-liketxt">Нажмите «нравиться», чтобы читать WayOfHelp.com в соцсетях</div>
	<div class="social-wnd-widget">
		<div class="fb-like" data-href="<?=WWWHOST?>" data-layout="standard" data-action="like" data-width="280" data-show-faces="true" data-share="true"></div>
	</div>
	<div class="social-wnd-bot"><span>Я уже читаю WayOfHelp.com в соцсетях</span></div>
</div>
<div id="fb-root"></div>-->

	<script type="text/javascript">
		document.write(decodeURI("%3Cscript id='pap_x2s6df8d' src='" + (("https:" == document.location.protocol) ? "https://" : "http://") +
			"wayofhelp.com/affiliate/scripts/trackjs.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		PostAffTracker.setAccountId('default1');
		try {
			PostAffTracker.track();
		} catch (err) { }
	</script>

<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '1179816618731293',
			xfbml      : true,
			version    : 'v2.7'
		});
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

  var uid = '<?=$UserId?>';
  var show_popup = '<?=$show_popup?>';

	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter34223620 = new Ya.Metrika({
					id:34223620,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true,
					webvisor:true
				});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");

</script>
<noscript><div><img src="https://mc.yandex.ru/watch/34223620" style="position:absolute; left:-9999px;" alt="Without js" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>