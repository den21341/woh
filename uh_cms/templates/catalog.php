<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$PM = $this->pageModel;

$page = $PM->get_page();

$PAGE_H1 = $page->title;
if( isset($this->page_h1) )
	$PAGE_H1 = $this->page_h1;

// Build bread crumbs
$BCHTML = $this->renderBreadcrumbs("");

($this->cityidby == '' ? $this->cityidby = 0 : '');

//echo $_SERVER['REMOTE_ADDR'];
?>
	<div class="cat-row-head">
		<div class="container">
			<div class="breadcrumb-bg">
				<ol class="breadcrumb">
					<?=$BCHTML;?>
				</ol>
				<h1><?=$PAGE_H1;?></h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="container">
			<div class="col-xs-3 col-md-3">
				<div class="h3"><?=$this->localize->get("cat", "lcol-hdr");?></div>
				<div class="cat-list">
					<div class="cat-it ">
						<table>
							<tr><td><img src="/files/sects/br-red-0-bonus.png" alt="<?=$this->localize->get("catalog", "bonus")?>"></td><td><a href="<?=$this->Page_BuildUrl("bonus","list");?>" class="cat-color"><span><?=$this->localize->get("cat", "free-bonus");?></span></a></td></tr>
						</table>
					</div>
					<?php
						$cat = $this->catmodel;
						$sgroup = 0;

						$sect0 = $cat->Catalog_SectLevel($sgroup, 0);
						//echo '<pre>';
						//print_r($sect0);
						for( $i=0; $i<count($sect0); ++$i ) {
							$isopen0 = $cat->Catalog_SectIsInPath($sect0[$i]['id'], $this->spath);

							$caturl = $this->Catalog_BuildUrl($sect0[$i]['url']);

							$ico_file = ( $sect0[$i]['filename_thumb'] != "" ? WWWHOST.FILE_DIR.stripslashes($sect0[$i]['filename_thumb']) : WWWHOST.'img/br-br-1-build.png');
							if( $isopen0 && ($sect0[$i]['filename_thumb_sel'] != "") )
								//print_r($sect0[$i]['filename_thumb_sel']);
								$ico_file = WWWHOST.FILE_DIR.stripslashes($sect0[$i]['filename_thumb_sel']);

							echo '<div class="cat-it'.($isopen0 ? ' cat-it-open' : '').'">
									<table>
									<tr><td><img src="'.$ico_file.'" alt="'.$sect0[$i]['name'].'" title="'.$sect0[$i]['name'].'"></td><td><a class="cat-color" href="'.$caturl.'"><span>'.$sect0[$i]['name'].'</span></a></td></tr>
									</table>';

							if( $isopen0 )
							{
								$sect1 = $cat->Catalog_SectLevel($sgroup, $sect0[$i]['id']);

								echo '<div class="cat-lev cat-lev-open">';
								for( $j=0; $j<count($sect1); $j++ )
								{
									$isopen1 = $cat->Catalog_SectIsInPath($sect1[$j]['id'], $this->spath);

									$caturl = $this->Catalog_BuildUrl($sect1[$j]['url']);

									echo '<div'.($isopen1 ? ' class="cat-lev-sel"' : '').'><a class="cat-color" href="'.$caturl.'"><span>'.$sect1[$j]['name'].'</span></a></div>';

									if( $isopen1 )
									{
										$sect2 = $cat->Catalog_SectLevel($sgroup, $sect1[$j]['id']);
										echo '<ul>';
										for( $k=0; $k<count($sect2); $k++ )
										{
											$isopen2 = $cat->Catalog_SectIsInPath($sect2[$k]['id'], $this->spath);

											$caturl = $this->Catalog_BuildUrl($sect2[$k]['url']);

											echo '<li'.($isopen2 ? ' class="sel"' : '').'><a href="'.$caturl.'">'.$sect2[$k]['name'].'</a></li>';
										}
										echo '</ul>';
									}
								}
								echo '</div>';
							}

							echo '</div>';
						}
					?>
				</div>
			</div>
			<div class="col-xs-9 col-md-9 catalogfix">
				<div class="col-lg-12 col-md-12 col-xs-12 cat-col-h catalogfix">
					<div class="catalog-block">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-2 col-md-2 col-xs-2"><img class="catalog-block-img" src="<?=WWWHOST?>img/info-pr-img.png" width="80" height="80"></div>
						<div class="col-lg-10 col-md-10 col-xs-10 catalog-block-text"><span><?=$this->localize->get("catalog", "press-cat")?></span>
					</div>
					</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-xs-12 cat-triangle">
					<img src="<?=WWWHOST?>img/triangle-top.png">
				</div>
				
				<div class="col-lg-12 col-md-12 col-xs-12 cat-col-h	">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<!--<form action="<?=WWWHOST?>cat/search/" method="post">
					<input type="search" name="search" value="test">
					<input type="submit" value="OK">
				</form>-->
						<a href="<?=$this->Page_BuildUrl('proj','add')?>"><div class="btn btn-primary btn-add-proj main-btn-promise"><img src="<?=WWWHOST.'img/cat-add-proj.png'?>" alt="<?=$this->localize->get("catalog", "makeproject")?>"><span><?=$this->localize->get("catalog", "makeproject")?></span></div></a>
						<?php
						if( $this->sid != 0) {
							?>
							<div class="cat-col-sort">
								<div class="btn-group" role="group"
									 aria-label="<?= $this->localize->get("cat", "flt-hdr"); ?>">
									<a href="<?= (isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat", "")) . ($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.'' : '')?>"
									   class="btn btn-default<?= ($this->sortby == "" ? ' active' : ''); ?>"><?= $this->localize->get("cat", "flt-all"); ?></a>
									<a href="<?= (isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat", "")) . ($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.($this->fltby != '' ? '/flt_'.$this->fltby : '/flt_').'/sort_popular' : ($this->fltby != '' ? 'flt_'.$this->fltby.'/sort_popular' : 'sort_popular/'))?>"
									   class="btn btn-default<?= ($this->sortby == "popular" ? ' active' : ''); ?>"><?= $this->localize->get("cat", "flt-popular"); ?></a>
									<a href="<?= (isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat", "")) . ($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.($this->fltby != '' ? '/flt_'.$this->fltby : '/flt_').'/sort_new' : ($this->fltby != '' ? 'flt_'.$this->fltby.'/sort_new' : 'sort_new/'))?>"
									   class="btn btn-default<?= ($this->sortby == "new" ? ' active' : ''); ?>"><?= $this->localize->get("cat", "flt-new"); ?></a>
									<a href="<?= (isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat", "")) . ($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.($this->fltby != '' ? '/flt_'.$this->fltby : '/flt_').'/sort_ended' : ($this->fltby != '' ? 'flt_'.$this->fltby.'/sort_ended' : 'sort_ended/'))?>"
									   class="btn btn-default<?= ($this->sortby == "ended" ? ' active' : ''); ?>"><?= $this->localize->get("cat", "flt-ending"); ?></a>
								</div>
							</div>
							<?php
						} else {
							?>
							<!--<div class="cat-col-sort-none"></div>-->
						<?php } ?>
						<?php
						//if( $this->UserId != 0 )
						//if( ($this->UserId != 0) && ($this->catmode == CAT_MODE_DREAM) )
						if( $this->catmode == CAT_MODE_DREAM ) {
							//echo '<div class="cat-addbtn"><a href="'.$this->Page_BuildUrl("proj", ( isset($this->surl) && ($this->surl == "fulfill-dream") ? 'adddream' : 'add')).'" class="btn btn-needhelp"><span class="glyphicon glyphicon-bullhorn"></span> '.( isset($this->surl) && ($this->surl == "fulfill-dream") ? $this->localize->get("cat", "btn-dream") : $this->localize->get("cat", "btn-gethelp") ).'</a></div>';
							echo '<div class="cat-btn-thing"><a href="'.$this->Page_BuildUrl("proj", 'adddream').'" class="btn btn-needhelp"><span class="glyphicon glyphicon-bullhorn"></span> '.$this->localize->get("cat", "btn-dream").'</a></div>';
						}
						//else if( ($this->UserId != 0) && ($this->catmode == CAT_MODE_THING) )
						else if( $this->catmode == CAT_MODE_THING ) {
							echo '<div class="cat-btn-thing"><a href="'.$this->Page_BuildUrl("proj", "addthing").'" class="btn btn-needhelp"><span class="glyphicon glyphicon-bullhorn"></span> '.$this->localize->get("cat", "btn-thing").'</a></div>';
						}
						else {
							?>
							<div class="cat-col-als">
								<div class="btn-group" role="group" aria-label="<?=$this->localize->get("cat", "flt-thdr");?>">
									<a href="<?=( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat","") ).($this->onlyby != "" ? "only_".$this->onlyby."/" : "").($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.'/' : '')?>" class="btn btn-default<?=($this->fltby == "" ? ' active' : '');?>"><?=$this->localize->get("cat", "flt-all");?></a>
									<a href="<?=( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat","") ).(($this->onlyby != "") ? "/only_".$this->onlyby."/" : "").($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.'/flt_money/'.($this->sortby != '' ? 'sort_'.$this->sortby : '') : ($this->sortby != '' ? 'flt_money/'.'sort_'.$this->sortby : 'flt_money/'))?>" class="btn btn-default<?=($this->fltby == "money" ? ' active' : '');?>"><?=$this->localize->get("cat", "flt-tmoney");?></a>
									<a href="<?=( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat","") ).($this->onlyby != "" ? "only_".$this->onlyby."/" : "").($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.'/flt_work/'.($this->sortby != '' ? 'sort_'.$this->sortby : '') : ($this->sortby != '' ? 'flt_work/'.'sort_'.$this->sortby : 'flt_work/'))?>" class="btn btn-default<?=($this->fltby == "work" ? ' active' : '');?>"><?=$this->localize->get("cat", "flt-twork");?></a>
									<a href="<?=( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat","") ).($this->onlyby != "" ? "only_".$this->onlyby."/" : "").($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.'/flt_loc/'.($this->sortby != '' ? 'sort_'.$this->sortby : '') : ($this->sortby != '' ? 'flt_loc/'.'sort_'.$this->sortby : 'flt_loc/'))?>" class="btn btn-default<?=($this->fltby == "loc" ? ' active' : '');?>"><?=$this->localize->get("cat", "flt-tloc");?></a>
									<a href="<?=( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat","") ).($this->onlyby != "" ? "only_".$this->onlyby."/" : "").($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.'/flt_easy/'.($this->sortby != '' ? 'sort_'.$this->sortby : '') : ($this->sortby != '' ? 'flt_easy/'.'sort_'.$this->sortby : 'flt_easy/'))?>" class="btn btn-default<?=($this->fltby == "easy" ? ' active' : '');?>"><?=$this->localize->get("cat", "flt-teasy");?></a>
								</div>
							</div>
							<?php
						}
						?>
					</div>
					<?php
					if( $this->onlyby == "" )
					{
						?>
						<div class="col-lg-12 col-md-12 col-xs-12 cat-proj-al">
							<div class="col-md-4 col-xs-4 col-lg-4"><select name="rcc" id="rcc" class="form-control" onchange="loadRegs(this)">
									<option value="0"><?=$this->localize->get("catalog", "all-country")?></option>
									<?php
									echo $this->render_combo_country($this->countrylist, $this->countryId);
									?>
								</select></div>
							<div class="col-md-4 col-xs-4 col-lg-4"><select name="rcr" id="rcr" class="form-control" onchange="loadCities(this)"><option value="0"><?=$this->localize->get("catalog", "all-region")?></option></select></div>
							<div class="col-md-4 col-xs-4 col-lg-4"><select name="rcg" id="rcg" class="form-control" onchange="goselCity(this)"><option value="0"><?=$this->localize->get("catalog", "all-city")?></option></select></div>
						</div>
						<script type="text/javascript">

							function loadRegs(sobj)
							{
								var sccid = sobj.options[sobj.selectedIndex].value;

								if(sccid == 0) location.href = '<?=$this->Page_BuildUrl('cat')?>';
								else
									location.href = '<?=( ( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat","") )."country_")?>'+sccid+'/region_0/city_0';
							}

							function loadCities(cobj)
							{
								var rccid = cobj.options[cobj.selectedIndex].value;

								/*if( rccid != 0 )
								 {
								 _loadCities(rccid, '<?=($this->sid == 0 ? "all" : $this->sid);?>', "rcg");
							 }*/
								if(rccid == 0) location.href = '<?=$this->Page_BuildUrl('cat')?>';
								else
									location.href = '<?=( ( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat","") )."country_".$this->countryId."/region_");?>'+rccid+'/city_0';
							}

							function goselCity(obj)
							{
								var gccid = obj.options[obj.selectedIndex].value;

								location.href = '<?=( ( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Page_BuildUrl("cat","") )."country_".$this->countryId."/region_".$this->regionId."/city_");?>'+gccid+'/';
							}

							function _loadRegs(cid, sid, robjid, cobjid)
							{
								//function loc_LoadRegion(countryobj,oblobjid, cityobjid)
								//var countryid = countryobj.options[countryobj.selectedIndex].value;
								var oblobj = document.getElementById(robjid);
								var cityobj = document.getElementById(cobjid);
								var regId = <?=$this->regionId?>;

								if( cid == 0 )
									return;

								var post_req_str = "countryid=" + cid + "&sectid=" + sid;

								$.ajax({
									type: "GET",
									url: req_ajx_host+"ajx/regionlist/",
									data: post_req_str,
									dataType: "json",
									success: function(data){
										try
										{
											//alert(data);
											var regions = data.regions;

											oblobj.options.length = 0;
											oblobj.options[0] = new Option("--- <?=$this->localize->get("catalog", "all-region")?> ---", 0);

											cityobj.options.length = 0;
											cityobj.options[0] = new Option("--- <?=$this->localize->get("catalog", "all-city")?> ---", 0);

											for( var i=0; i<regions.length; i++ )
											{
												oblobj.options[i+1] = new Option(regions[i]['name'], regions[i]['id']);
												//console.log(oblobj.options[i+1]);
											}
											$("#rcr option[value="+regId+"]").attr('selected','selected');
											//$("#wnd-cont").html( data );
										}
										catch(e1)
										{
											// Some errors occure while retieving city list so do nothing
											alert(e1);
										}

									}
								});
							}

							function _loadCities(rid, sid, cobjid)
//function _loadCities(oblobj,cityobjid)
							{
								//var oblid = oblobj.options[oblobj.selectedIndex].value;
								var cityobj = document.getElementById(cobjid);
								var cityId = <?=$this->cityidby?>;

								if( rid == 0 )
									return;

								var post_req_str = "oblid=" + rid + "&sectid=" + sid;

								$.ajax({
									type: "GET",
									url: req_ajx_host+"ajx/citylist/",
									data: post_req_str,
									dataType: "json",
									success: function(data){
										try
										{
											//alert(data);
											var cities = data.cities;

											cityobj.options.length = 0;
											cityobj.options[0] = new Option("--- <?=$this->localize->get("catalog", "all-city")?> ---", 0);

											for( var i=0; i<cities.length; i++ )
											{
												cityobj.options[i+1] = new Option(cities[i]['name'], cities[i]['id']);
											}
											$("#rcg option[value="+cityId+"]").attr('selected','selected');
											//$("#wnd-cont").html( data );
										}
										catch(e1)
										{
											// Some errors occure while retieving city list so do nothing
											alert(e1);
										}

									}
								});
							}

							$(document).ready(function(){
								var coId = <?=$this->countryId?>;
								var regId = <?=$this->regionId?>;
								var cyId = <?=$this->cityidby?>;

								if( coId != 0 ) {
									_loadRegs(coId, '<?=($this->sid == 0 ? "all" : $this->sid);?>', "rcr", "rcg");
								}
								if(regId != 0) {
									_loadCities(regId, '<?=($this->sid == 0 ? "all" : $this->sid);?>', "rcg");
								}
							});
						</script>
						<?php
					}
					?>
				</div>
				<div class="col-xs-12 col-md-12 col-lg-12">
					<div class="proj-list re-proj-list">
						<?php
							$its_total = $this->its_total;
							$its = $this->its;

							$calcRate = [];

							//print_r($its_total);
							for( $i=0; $i<count($its); ++$i ) {
								//$author = $its[$i]['name']." ".$its[$i]['fname'];
								$author = $its[$i]['name'];

								if($i <= CAT_LIMIT_NEW_PROJ_ROTATION) {
									$this->catmodel->addProjViews($its[$i]['id'], 1);

									if($this->pi == 0) {
										$calcRate[] = $its[$i]['id'];
									}
								}
								else
									$this->catmodel->addProjViews($its[$i]['id'], 0.5);

								$short = strip_tags($its[$i]['descr'], '');
								if( mb_strlen($short) > 200 ) {
									$short = mb_substr($short,0,250)."...";
								}

								$AUTHOR_LNK = $this->Page_BuildUrl('users', 'viewrev/'.$its[$i]['author_id']);
								$PROJ_LNK = $this->Page_BuildUrl('proj', "view/".$its[$i]['id']).'?cat=1';
								// url to check image
								$IMG_CHECK_SRC = WWWHOST.'img/icnok2.png';

								$progress_str = "";
								$amountByWoh = "";

								if( $its[$i]['profile_id'] == PROJ_NEEDHELP )	// Проект по получению помощи
								{
									$confirmed = $this->catmodel->Item_ReqCollected($its[$i]['id'], REQ_STATUS_CONFIRM);
									$confprog_percnt = $this->catmodel->PercentDone($confirmed['sum'], $its[$i]['amount']);

									$tmleft_str = $this->catmodel->Time_LeftFromMinutes($its[$i]['tmleft']);
									$tmprog_percnt = $this->catmodel->PercentDone($its[$i]['tmleft'], $its[$i]['tmall']);

									// Показать отображение для проекта по сбору средств
									if( $its[$i]['amount_type'] == PROJ_TYPE_MONEY ) {

										$currency_name = (!empty($its[$i]['currency']['name']) ? $its[$i]['currency']['name'] : '');

										$targ_str = number_format($its[$i]['amount'], 0, ",", " ")." ".$currency_name;
										$confirm_str = number_format($confirmed['sum'],  0, ",", " ")." ".$currency_name;
										$money = $confirmed['sum'];
										$totmoney = $its[$i]['amount'];

										$progress_str = $confirm_str." / ".$targ_str;
										$amountByWoh = $this->catmodel->checkWohPays($its[$i]['id']);


										$sum_money = ($money*100)/$totmoney;
										$sum_persent =($sum_money <= 0? '0' : $sum_money);
										$moneybar = '<div class="cat-moneybar '.(strlen($targ_str) > 15 ? 'million-size' : '').'">'.$targ_str.'</div>';
									}
									else if( $its[$i]['amount_type'] == PROJ_TYPE_HUMAN ) {
										$targ_str = round($its[$i]['amount'])." ".$this->localize->human;
										$confirm_str = round($confirmed['sum'])." ".$this->localize->human;

										$progress_str = $confirm_str." / ".$targ_str;
										//$sum_persent = ($its[$i]['amount']*100)/$confirmed['sum'];
									}
								}
								else
								{
									$confirmed = $this->catmodel->Item_ReqCollected($its[$i]['id']);

									$tmleft_str = $this->catmodel->Time_LeftFromMinutes($its[$i]['tmleft']);
									$tmprog_percnt = $this->catmodel->PercentDone($its[$i]['tmleft'], $its[$i]['tmall']);
								}
								// Setup picture for project
								$proj_avatar = WWWHOST . ($its[$i]['pic_sm'] != "" ? (file_exists($its[$i]['pic_sm']) ? $its[$i]['pic_sm'] : 'img/no-pic.png') : 'img/no-pic.png');

								$proj_image = WWWHOST.(isset($its[$i]['photos'][0]['filename_thumb'])? (file_exists($its[$i]['photos'][0]['filename_thumb']) ? $its[$i]['photos'][0]['filename_thumb'] : 'img/no-pic.png') : 'img/no-pic.png');

								$timebar = '<div class="proj-tm-txt">'.$this->localize->get("cat", "it-started").':'.date("d.m.y", strtotime($its[$i]['stdt'])).'<br>'.$tmleft_str.'</div>';

								$timeline = '<span class="cabinet-timeline" alt="'.$this->localize->get("catalog", "collected-money").'" title="'.$this->localize->get("catalog", "collected-money").'"><img src="'.WWWHOST.'img/cat-progress-act.png" width="'.(ceil((isset($sum_persent) ? $sum_persent : "") * 2.45)).'" height="10" alt="'.$this->localize->get("catalog", "collected-money").'" title="'.$this->localize->get("catalog", "collected-money").'"></span>';

								$notimeline = '<span style="border:none"></span>';
								$btn_label = $this->localize->get("cat", "it-btn-help");
								if( $its[$i]['profile_id'] == PROJ_EVENT )
									$btn_label = $this->localize->get("cat", "it-btn-event");
								else if( $its[$i]['profile_id'] == PROJ_THINGS )
									$btn_label = $this->localize->get("cat", "it-btn-thing");

								$check = "";
								$its[$i]['is_check'] ? $check = '<img src="'.$IMG_CHECK_SRC.'">' : '';   /// Проверка is_check

								$title = (mb_strlen($its[$i]['title2']) > 35 ? mb_substr($its[$i]['title2'],0,30)."..." : $its[$i]['title2']);
								//<div class="row proj-head'.($its[$i]['background'] > 0 ? ' proj-head-bg'.$its[$i]['background'] : '').'">
								echo '<div class="col-xs-4 col-md-4 col-lg-4 cat-mobil">
									<div class="proj-it cat-borders cattit">
									<a href="'.$PROJ_LNK.'"><img class="cat-avatar" src="'.$proj_image.'" alt="'.$its[$i]['title2'].'" title="'.$its[$i]['title2'].'" width="242" height="150"></a>
									<div class="proj-tm-progress catfix">
										'.($its[$i]['amount_type'] == 0 ? $timeline : $notimeline).'
										<div class="col-xs-3 col-md-3 col-lg-3">
	
											<a href="'.$AUTHOR_LNK.'"><div class="proj-upic-block"><img src="'.$proj_avatar.'" width="43" height="43" alt="'.$its[$i]['name'].'&nbsp;'.$its[$i]['fname'].'" title="'.$its[$i]['name'].'&nbsp;'.$its[$i]['fname'].'" class="proj-upic-img"></div></a>
										</div>
										<div class="col-xs-3 col-md-3 col-lg-3">
											<p class="cat-persent woh-color">'.($its[$i]['amount_type'] == 0 ? mb_substr($sum_persent, 0, 4).'%' : $confirmed['num']).'</p>
										</div>
										<div class="col-xs-6 col-md-6 col-lg-6">
											'.($its[$i]['amount_type'] != 0 ? $timebar : $moneybar).'
										</div>
									</div>
							<div class="re-proj-tit"><a onclick="addItemClick(\''.$its[$i]['id'].'\')" href="'.$PROJ_LNK.'" alt="'.$its[$i]['title2'].'" title="'.$its[$i]['title2'].'">'.$title.'</a></div>

						
					
						
							<!--<div class="proj-review">
								'.$short.'
							</div>-->
						<a onclick="addItemClick(\''.$its[$i]['id'].'\')" href="'.$PROJ_LNK.'" class="btn btn-success cat-bnt-clr '.($its[$i]['profile_id'] == PROJ_THINGS ? 'grab-think' : '').'"><span class="glyphicon glyphicon-thumbs-up"></span> '.$btn_label.'</a>
						<div class="fixp">
							<div class="proj-subs">
								'.$its[$i]['locinfo']['countryname'].', '.$its[$i]['locinfo']['regname'].', '.$its[$i]['locinfo']['name'].'<br>
								'.$confirmed['num'].' '.$this->localize->human_5.' '.( $its[$i]['profile_id'] == PROJ_EVENT ?
											$this->localize->get("cat", "it-event") :
											( $this->catmode == CAT_MODE_THING ? $this->localize->get("cat", "it-getthing") : $this->localize->get("cat", "it-help") )
										).'
							</div>
							
						</div>
					<div class="col-xs-9 col-md-10">
						<div class="check">
						'.$check.'
						</div>
						
					</div>
				</div>
			</div>';
							}

							//echo $this->catmode."<br>";

							if( count($its) == 0 )
							{
								$addurl = $this->Page_BuildUrl("proj", 'add');
								$tiptext = $this->localize->get("cat", "noproj");
								$tipbtn = $this->localize->get("cat", "btn-gethelp");

								//if( $this->catmode == "thing" )
								if( $this->catmode == CAT_MODE_THING )
								{
									$addurl = $this->Page_BuildUrl("proj", 'addthing');
									$tiptext = $this->localize->get("cat", "nothing");
									$tipbtn = $this->localize->get("cat", "btn-thing");
								}
								//if( $this->catmode == "dream" )
								else if( $this->catmode == CAT_MODE_DREAM )
								{
									$addurl = $this->Page_BuildUrl("proj", 'adddream');
									$tipbtn = $this->localize->get("cat", "btn-dream");
								}


								echo '<div class="proj-it-noitems">
				<div class="tbl-noitems">
					<div class="tbl-noitems-txt">
						<div><span>
							'.$tiptext.'<br><br>
							<a href="'.$addurl.'" class="btn btn-needhelp lnkgethelp"><span class="glyphicon glyphicon-bullhorn"></span> '.$tipbtn.'</a>
						</span></div>
					</div>
					<div class="tbl-noitems-face"></div>
				</div>							
			</div>';
							}

							//echo $this->catmode."<br>";

							if( count($its) == 0 )
							{
								$addurl = $this->Page_BuildUrl("proj", 'add');
								$tiptext = $this->localize->get("cat", "noproj");
								$tipbtn = $this->localize->get("cat", "btn-gethelp");

								//if( $this->catmode == "thing" )
								if( $this->catmode == CAT_MODE_THING )
								{
									$addurl = $this->Page_BuildUrl("proj", 'addthing');
									$tiptext = $this->localize->get("cat", "nothing");
									$tipbtn = $this->localize->get("cat", "btn-thing");
								}
								//if( $this->catmode == "dream" )
								else if( $this->catmode == CAT_MODE_DREAM )
								{
									$addurl = $this->Page_BuildUrl("proj", 'adddream');
									$tipbtn = $this->localize->get("cat", "btn-dream");
								}


								echo '<div class="proj-it-noitems">
				<div class="tbl-noitems">
					<div class="tbl-noitems-txt">
						<div><span>
							'.$tiptext.'<br><br>
							<a href="'.$addurl.'" class="btn btn-needhelp lnkgethelp"><span class="glyphicon glyphicon-bullhorn"></span> '.$tipbtn.'</a>
						</span></div>
					</div>
					<div class="tbl-noitems-face"></div>
				</div>							
			</div>';
							}
							/**** PAGINATION WITH ELLISPSIS ****/
							else if ($its_total > 0) {
								$itemCount = $its_total;
								$currentPage = $this->pi;
								$itemsPerPage = 14;
								$adjacentCount = 4;
								//$pageLinkTemplate =  ( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Catalog_BuildUrl("") ).($this->sortby != "" ? 'sort_'.$this->sortby.'/' : '').($this->fltby != "" ? 'flt_'.$this->fltby.'/' : '').($this->countryId > -1 ? 'country_'.$this->countryId.'/' : '').($this->countryId > -1 && $this->regionId > 0 ? 'region_'.$this->regionId.'/' : '').($this->countryId > -1 && $this->regionId > 0 && $this->cityidby > 0 ? 'city_'.$this->cityidby.'/' : '');
								$pageLinkTemplate = $this->Catalog_BuildUrl("").(isset($this->surl) ? $this->surl.'/' : '').($this->countryId > -1 ? 'country_'.$this->countryId.'/region_'.$this->regionId.'/city_'.$this->cityidby.'/' : '').($this->fltby != '' ? 'flt_'.$this->fltby.'/' : ($this->sortby != '' ? 'sort_'.$this->sortby.'/' : ''));
								$pageLinkTemplate .= "p_" . '%d' . "/";

								$showPrevNext = true;

								$firstPage = 1;
								$lastPage = ceil($itemCount / $itemsPerPage);
								//echo $itemCount.'/'.$itemsPerPage.' = '.$lastPage;
								if ($lastPage != 1) {
									if ($currentPage+1 <= $adjacentCount + $adjacentCount) {
										$firstAdjacentPage = $firstPage;
										$lastAdjacentPage = min($firstPage + $adjacentCount + $adjacentCount, $lastPage);
									} elseif ($currentPage > $lastPage - $adjacentCount - $adjacentCount) {
										$lastAdjacentPage = $lastPage;
										$firstAdjacentPage = $lastPage - $adjacentCount - $adjacentCount;
									} else {
										//echo $firstPage.'<br>';
										//echo $currentPage.'<br>';
										//!($currentPage - ($firstPage+3)) ? $firstAdjacentPage = $currentPage-2 : $firstAdjacentPage = $currentPage - $adjacentCount;
										$firstAdjacentPage = $currentPage - 1;
										//($currentPage + ($firstPage)+2) == $lastPage ? $lastAdjacentPage = $currentPage+2 : $lastAdjacentPage = $currentPage + $adjacentCount;
										$lastAdjacentPage = $currentPage + $adjacentCount;
									}

									echo '<div class="proj-pages">			
				<nav class="navbar-right">
				<ul class="pagination">';
									if ($showPrevNext) {
										if ($currentPage <= $firstPage) {
											echo '<li><span>&lt;</span></li>';
										} else {
											echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($currentPage) : sprintf($pageLinkTemplate, $currentPage)) . '">&lt;</a></li>';
										}
									}
									if ($firstAdjacentPage > $firstPage) {
										echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($firstPage) : sprintf($pageLinkTemplate, $firstPage)) . '">' . $firstPage . '</a></li>';
										if ($firstAdjacentPage > $firstPage + 1) {
											echo '<li><span>...</span></li>';
										}
									}
									for ($i = $firstAdjacentPage; $i <= $lastAdjacentPage; $i++) {

										if ($currentPage+1 == $i) {
											echo '<li class="active"><span>' . $i . '</span></li>';
										} else {
											echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($i) : sprintf($pageLinkTemplate, $i)) . '">' . $i . '</a></li>';
										}
									}
									if ($lastAdjacentPage < $lastPage) {
										if ($lastAdjacentPage < $lastPage - 1) {
											echo '<li><span>...</span></li>';
										}
										echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($lastPage) : sprintf($pageLinkTemplate, $lastPage)) . '">' . $lastPage . '</a></li>';
									}
									if ($showPrevNext) {
										if ($currentPage >= $lastPage) {
											echo '<li><span>&gt;</span></li>';
										} else {
											$currentPage = (!$currentPage ? 1 : $currentPage+1);
											echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($currentPage + 1) : sprintf($pageLinkTemplate, $currentPage + 1)) . '">&gt;</a></li>';
										}
									}
									echo '</ul>
						</nav>
						<div class="clearfix"></div>
				</div>';
								}
							}
							//else if( ($its_total > 0) && isset($this->surl) && ($this->surl != "") )
							/*else if( $its_total > 0 )
							{
								$TOTAL_PAGES = ceil($its_total / $this->pn);

								if( $TOTAL_PAGES > 1 )
								{
									echo '<div class="proj-pages">
									<nav class="navbar-right">
									<ul class="pagination">';

									for( $i=1; $i<=$TOTAL_PAGES; $i++ )
									{
										$PPURL = ( isset($this->surl) ? $this->Catalog_BuildUrl($this->surl) : $this->Catalog_BuildUrl("") ).($this->sortby != "" ? 'sort_'.$this->sortby.'/' : '').($this->fltby != "" ? 'flt_'.$this->fltby.'/' : '');
										if( $i>1 )
											$PPURL = $PPURL."p_".$i."/";
										echo '<li'.(($i == $this->pi) || (($this->pi == 0) && ($i==1)) ? ' class="active"' : '').'><a href="'.$PPURL.'">'.$i.'</a></li>';
									}

									/*
									 ?>
									 <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
									 <li class="active"><a href="#">1</a></li>
									 <li><a href="#">2</a></li>
									 <li><a href="#">3</a></li>
									 <li><a href="#">4</a></li>
									 <li><a href="#">5</a></li>
									 <li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>


									echo '</ul>
											</nav>
											<div class="clearfix"></div>
									</div>';
								}
							}*/

							?>

						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function () {
				var itmClcRate = <?=json_encode($calcRate)?>;

				if(itmClcRate.length > 0) {
					$.ajax({
						type: "POST",
						url: req_ajx_host + "ajx/reindexviews/",
						data: "pids="+itmClcRate,
						dataType: 'json',
						success: function (data) {}
					});
				}

				(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=isWfOyeLuWB26paxtdM5dnKc6DyTcUebev9/zmWPVgXqFhYZ*rOz8rBssZZ7UfAz52Y2WcCLvIERg2Q8XjLNOYNfFyeJyQYONBwwUFHZxQMaDJB5b4a3WdXEoWAJW6m9*6KNO/3cAHHA6*7EIURt4RZ*CGuN/oLiTRTvN9IuUMY-';
			});
		</script>
	<?php if(preg_match('/cat\/.*\//',$_SERVER['REQUEST_URI'], $matches)) {
		$matches = explode('/', $matches[0]);
		switch ($matches[1]) {
			case 'thing-market' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=jbx8WpI*ZsmIdMRzrnLue/cd*7KhTQQYnD6F6mbsq5wV65m*sPlS26GPHAJyMG4lNp9L10MmClzPV2SDJeA8FHAsAxFyKOqnIVmOGohDf9AqYJo5WJN2MikQ77sbsNz5D80Iw*UCRdTAsmBLud*52XmQlFkAlt5Td519hZiXx0w-';</script> <?php break;
			case 'fulfill-dream' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=iul8YRkxS4HQsHNHVXXdS3zg8hYBUZV40O8vL7/Rk0cAfejilxEc7CbNx5kHD2gOW*W23xFIT/1i7W6gWjfzI3HQ0K16TrVvcwdAleBjDadOYYhFozEKAu*cP7xSWc1FVsNxarkBJNAZMtuy/q0iM2VddVX5rv8CGcrtFIWj8Ok-';</script> <?php break;
			case 'goods' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=yM6HkcLcO8XUeG4qpA3J6W7h0dU*RPLB2yqgB65c5Sz8TAHR*4xqNH63UmBVhe4y4tR2SoBa*qTzFkwyMywjcAVB9Z3F*2/57nxSJBbhVRNDXFyLvmy/fT/vuDvHtOpxV81tFjCC/jb2THTYBzNBqvgXpCEbGE0rYv4GeEvhnqc-';</script> <?php break;
			case 'sport' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=Sh4qeoIJbecMYzCS/PvWngr4jkYSloEwhx3FHH*v9zfzKfs8E2I2VkNcjnAuvlvGAZXUY8prIcyCLScWQ8/eHHPOA33JZy/xRXBP/MRbstxZpyMtJqJobIIh1btKcBqtS6XSwzzzQ/7vuHFAZu4LOgEnAvgsrKF9Iw7dp7ZExSM-';</script> <?php break;
			case 'pet-help' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=XmD6BpB/rqs9gMQtf8zOCm2QuGmHijoc5qEQaWv9DXugMDYVdRGLW/AzUo1*tWZFg5HYwdchj2SzSH/vNcdYEGl0YE7HqwO5Y/ykZ8PRyy419*ow1U9430jH/6GYHje4uHf182ERuF3fAPGMid5q0Ubxa8r337GjS7pgZno2Sbo-';</script> <?php break;
			case 'employment' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=w4040UPgEWGr3I45dy7p4U/gbjL0z8QMgbK20TeUQGlcSAIJhxOHkoLc1cTyDbaJNrV91GCG99n7bjQqjHbXgOzmr5SMj1P6*IpbpYy1fwz7npKjDoGOnfX9cdg7R*IkYRMforTUBiVbq15KbQ0bsdKMe8hribKfC5KU5bUcvaM-';</script> <?php break;
			case 'medicine' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=du5mZhqiqT0K88jpxhFp37KZ4pG60PmLaX2BIaxuEFxLWVU7bGtOfOXnE//pXxw8BZhc9EbYdcTzSD/zE2o2PFB7*VLTstJJVJBcjGLIULH8xiTAsMRo085wLyhpcgsf5N8jiTxM73X24BTrzAuAksm*5I9fIgkl9*/zDPMnzro-';</script> <?php break;
			case 'intellect' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=CzkMOl2*JbjZA9G67h0yyYNwAeYq/kgVU1cRNXfkyn/U8jXuoBQTZafWXSdauSJy1LtV3QuLGKMeD4uZbbnpxSEOHWjurLyoCvLt3ikEihbhryaPZGFmizDqeVXficL*mtLGguP2vD0/yo4zJ8kw9DD6Ge5r19RHeO8vesJLuBw-';</script> <?php break;
			case 'art-culture' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=U2XATzhSArxBJrahEmpsyp4d31lYsvN0kaavTgVz5xOXFClOPEsQ8sY9CztVtmOtHUqFboHwVE46ut2SfB4TpLm9ErSjZL3AeW9/PVzm48j3RQQ5R8L6fghLhrj8TsrXSzL/iFT*eCv9j*rPWT752pQqwIaDx19TEL022M2lVcI-';</script> <?php break;
			case 'travel-lodging' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=t87UMN2ixVeyhipLUaYvLc1A33j7xWefA2pvK3IkTaBKXkDFZS*v9YDxEPIIt/LUtN*eSQ6BGlwn0hxdQxay/4LJFgP2HNnExz1gBQQAiHLNY1eEh9EjfamUFVcH6Qwgwb1L3Y34Ea5Jmbk5ntUXXFBoV2PrUbYMpEaRjF3cGBc-';</script> <?php break;
			case 'startups' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=H6z7fY0Vneb6AjZYCZvz1VbbYbHqpuXDKLKvpGTQWlIylTbPAK1DoDgRpWaYndMf564*InY9LrHvA0ttrum08BPlfglGV2aPCTrQe6FtrMa*tZcmwBxiLN*5IwP*I8dOIfM2*L9L2uTeQIwmTxJbf815xOPB7GSVinAaAj8GL7o-';</script> <?php break;
			case 'construction-repair' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=c7bGh*HuD1jWd/EvLzVqeWd7YagXn8VMIpmM3C*2C2T/ks/27QuZz3qcDaOmqbJ7c8ScBwzcGkEsof/plGwIJR58FAg1hRyRuGuHKIEs1j9Zm987opEUwhH/V7q3pe5lT1pcgUMvQ7uUMkYJI0U4bSSC7z0Vcj8eZCmM1KfzHdU-';</script> <?php break;
			case 'other' : ?>
				<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=dLWKRnLhXWQ6ow6KvmhApiZZxOEU7a*JpHUGBkU/tyfLd*sGcg*ZLTuXrgidBnIRaD*Xqv7x3jp3fBSpXgKxMXmWuMj3kaT8xZamHpdV0Sw5xqRtx5BXSSPg9QC34F9bX0kO3/SdhHIkAbuxUUGyw9nxKiw/6H4M2O4*WAoWh5I-';</script> <?php break;
		}
} ?>