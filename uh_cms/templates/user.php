<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$PM = $this->pageModel;

$page = $PM->get_page();

// Set page head title (h1 tag value)
$PAGE_H1 = $page->title;

// Build bread crumbs
$BCHTML = $this->renderBreadcrumbs($PAGE_H1);


$uinfo = $this->userinfo;
$loc = (isset($uinfo['loc_info']['name']) == "" ? $this->localize->get("cabinet", "noselected") : $uinfo['loc_info']['name']);
$cntry = (isset($uinfo['loc_info']['countryname']) == "" ? "(".$this->localize->get("cabinet", "noselected").")" : '('.$uinfo['loc_info']['countryname'].')');
$location = ($loc." ".$cntry);

$isMe = ($this->UserId == $uinfo['id']);

$share_title = $this->localize->get("tree","share-tit");
$share_descr = $this->localize->get("tree","share-descr");

$share_title = str_replace('_name_', $uinfo['name'], $share_title);
$share_title = str_replace('_fname_', $uinfo['fname'], $share_title);

$share_descr = str_replace('_d1_', $this->helpcount, $share_descr);
$share_descr = str_replace('_d2_', ($this->helpcount < 11) ? rand($this->helpcount, $this->helpcount + 10) : rand($this->helpcount - 10, $this->helpcount + 10), $share_descr);

//print_r($this->box['angel_day']);

//print_r($uinfo);
?>
<script type="text/javascript" src="<?=WWWHOST?>js/jquery.progress.js"></script>

<style type="text/css">
	@import url("<?=WWWHOST?>css/graph.css");
	@import url("<?=WWWHOST?>css/vis.css");
</style>
<?php
/*
	<style type="text/css" rel="stylesheet" href="<?=WWWHOST?>css/graph.css"></style>
<style type="text/css" rel="stylesheet" href="<?=WWWHOST?>css/vis.css"></style>
*/
?>
<script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/my_vis.min.js"></script>
<script type="text/javascript">
	//isMine = true;
</script>
<script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/obj_graph_baloon.js"></script>
<?php
/*
?>
<script type="text/javascript">
isMine = true;
</script>
<script type="text/javascript" src="http://numberimage.ru/graph/js/visgraph/obj_graph_baloon.js"></script>
*/
?>
<div class="row all-indent">
	<div class="container-fluid">
		<div class="top-indent">
			<div class="container">
				<ol class="breadcrumb">
					<?=$BCHTML;?>
				</ol>
			</div>
			<div class="col-md-5 col-xs-5 col-lg-5 back-to-top">
				<div class="left-usr-ground usr-pic-top">
					<div class="top-txt-main"><?=$this->localize->get("user","moreinfo")?></div>
					<div class="<?=$uinfo['angel_week'] ? 'angel-status-week' : ($uinfo['angel_day'] ? 'angel-status-day' : 'angel-status-none') ?>"></div>
					<div class="block-photo <?= $this->UserId==""? 'cab-fix-img':''?>" style="<?=($uinfo['angel_week'] || $uinfo['angel_day']) ? 'margin-top: -80px !important;' : ''?>">
						<img id="upic" src="<?=WWWHOST.( $uinfo['pic'] != "" ? $uinfo['pic'] : 'img/no-pic.png' );?>" class="user-photo-usr" width="120" height="120" alt="<?=$this->localize->get("infoother", "user")?> <?=$uinfo['name'].' '.$uinfo['fname']?>" title="<?=$this->localize->get("infoother", "user")?> <?=$uinfo['name'].' '.$uinfo['fname']?>">
						<div class="user-name" style="<?=mb_strlen($location) > 35 ? 'padding-left: 5px !important;' : ''?>">
							<p><?=$uinfo['name']?></p>
							<p><?=$uinfo['fname']?></p>
							<div class="block-u-loc-loc">
								<div class="user-loc-loc-img"></div>
								<div class="user-loc-loc-txt" style="<?=mb_strlen($location) > 35 ? 'font-size: 9.8px !important;' : ''?>"><?=$location?></div>
							</div>
							<div class="u-con-block">
								<?php
								if( ($this->UserId != 0) && !$isMe )
								{
									?>
									<a id="usrmsglnk" onclick="yaCounter34223620.reachGoal('PushButtonSoobshenieIzProfilya_Usera'); return true;" href="#" data-uid="<?=$uinfo['id'];?>" data-proj="0" data-rmsg="0">
										<div class="btn usr-msg-btn btn-info-work-do">
											<div class="col-md-3">
												<img src="<?=WWWHOST?>img/user-msg-send.png" class="usr-msg-img" alt="<?=$this->localize->get("cabmsg", "newmsg")?>" title="<?=$this->localize->get("cabmsg", "newmsg")?>">
											</div>
											<div class="col-md-9">
												<p><?=$this->localize->get("cabmsg", "newmsg")?></p>
											</div>
										</div>
									</a>

									<?php
								}
								?>
								<?php
								if($isMe )
								{
									?>
									<a id="usrmsglnk" href="#" data-uid="<?=$uinfo['id'];?>" data-proj="0" data-rmsg="0">
										<div class="btn usr-msg-btn btn-info-work-do usr-hidden-msg">
											<div class="col-md-3">
												<img src="<?=WWWHOST?>img/user-msg-send.png" class="usr-msg-img" alt="<?=$this->localize->get("cabmsg", "newmsg")?>" title="<?=$this->localize->get("cabmsg", "newmsg")?>">
											</div>
											<div class="col-md-9">
												<p><?=$this->localize->get("cabmsg", "newmsg")?></p>
											</div>
										</div>
									</a>

									<?php
								}
								?>

							</div>
						</div>
					</div>
					<?php if($this->req) { ?>
						<div id="spsbtn" class="btn usr-sps-btn usr-sps-btn-color">
							<div class="col-md-12">
								<p><?=$this->localize->get("user", "say-thanks")?></p>
							</div>
						</div>
					<?php } ?>

					<!--<hr class="left-hr-usr">
						<div class="user-loc-info">
							<div class="block-u-loc-face">
								<div class="user-loc-face-img"></div>
								<div class="info-samle"><?=($uinfo['group_name']);?></div>
							</div>
						</div>-->
					<!--<button id="btn">BTN</button>-->
					<hr class="left-hr-usr">
					<div class="users-box-info">
						<div class="user-a-row row">
							<div class="block-u-img col-md-6 col-xs-6 col-lg-6">
								<a href="<?=$this->Page_BuildUrl("info/stat")?>"><div class="user-box-status-img col-lg-3 col-xs-3 col-md-3"></div></a>
								<div class="users-box-status col-lg-9 col-xs-9 col-md-9" <?=(strlen($this->statusBox->getStatus($this->box['status'])) > 10 ? '' : "style='line-height: 30px'")?> ><div class="usr-status-ml mob-counter"><a href="<?=$this->Page_BuildUrl("info/stat")?>"><?=$this->statusBox->getStatus($this->box['status'])?></a></div></div>
							</div>
							<div class="block-u-mon col-md-4 col-xs-4 col-lg-4 <?=($this->box['money']>999? "block-money" : "")?>">
								<a href="<?=$this->Page_BuildUrl("info/power")?>"><div class="user-box-money-img usr-ang-pow"></div>
									<span class="user-box-money"><span class="usr-status-nocab ang-lh mob-counter"><?=$this->box['money']?></span></span></a>
							</div>
						</div>
						<div class="user-a-row a-row-h row">
							<div class="block-u-img block-ad-img col-md-6 col-xs-6 col-lg-6">
								<a href="<?=$this->Page_BuildUrl("info/angelsinfo")?>">
									<div class="user-box-ad-img ang-anim col-lg-3 col-xs-3 col-lg-3" title="<?=$this->localize->get("view", "ang-day")?>"></div></a>
								<div class="user-box-ang-count ang-wr col-lg-9 col-xs-9 col-md-9 mob-counter"><?=$uinfo['ad_count']?></div>
							</div>
							<div class="block-u-mon col-md-4 col-xs-4 col-lg-4 <?=($uinfo['aw_count']>9? "block-money" : "")?>">
								<a href="<?=$this->Page_BuildUrl("info/angelsinfo")?>">
									<div class="user-box-aw-img ang-anim usr-ang-pow" title="<?=$this->localize->get("view", "ang-week")?>"></div></a>
								<div class="user-box-ang-count ang-lh mob-counter"><?=$uinfo['aw_count']?></div>
							</div>
						</div>
					</div>
					<hr class="left-hr-usr">

					<div class="user-help-info">
						<p class="usr-p-size usr-p-color"><?=$this->localize->get("user", "stat-tothelps");?>:<span class="usr-help-count">&nbsp;&nbsp;<?=$uinfo['helpnum'];?><span class="usr-p-size usr-p-color"> <?=$this->localize->get("user", "time")?></span></span></p>
						<p class="usr-p-size2"><?=$this->localize->get("user", "stat-helpmoney");?>:<span class="usr-help-count2">&nbsp;&nbsp;<?=$uinfo['helpmoney']['totnum'];?><span class="usr-p-size2"> <?=$this->localize->get("user", "time")?></span></span></p>
						<!--<p><?=$this->localize->get("user", "stat-helpsum");?>: <span class="help-count">&nbsp;&nbsp;<?=number_format($uinfo['helpmoney']['totsum'], 2, ",", " ");?> грн.</span>-->
					</div>

				</div>
				<div class="soc-usr-line">
					<div class="usr-share-tree">
						<div class="col-md-12">
							<p class="usr-watch-tree"><?=$this->localize->get("user", "share-tree")?></p>
							<div class="group-soc-line-usr">
								<div class="soc-line">
									<!--<a href="#" id="jh-share-fbb" onclick="uShareFb()" class="usr-btn-line-fb" data-share-url="<?=$this->Page_BuildUrl("users", "viewrev/".$uinfo['id'])?>"></a>-->
									<a onclick="Share.facebook('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=facebook&utm_medium=social&utm_campaign=ShareDerevoizProfil_left&utm_content=<?=$uinfo['id']?>','<?=$share_title?>', '<?=$share_descr?>');yaCounter34223620.reachGoal('PushButton_ShareDerevoFBLeft'); return true;"><div class="usr-btn-line-fb"></div></a>
								</div>
								<div class="soc-line">
									<a onclick="Share.vkontakte('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=vk&utm_medium=social&utm_campaign=ShareDerevoizProfil_left&utm_content=<?=$uinfo['id']?>', '<?=$share_title?>', '<?=$share_descr?>');yaCounter34223620.reachGoal('PushButton_ShareDerevoVKLeft'); return true;"><div class="usr-btn-line-vk"></div></a>
								</div>
								<div class="soc-line">
									<a onclick="Share.odnoklassniki('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=ok&utm_medium=social&utm_campaign=ShareDerevoizProfil_left&utm_content=<?=$uinfo['id']?>');yaCounter34223620.reachGoal('PushButton_ShareDerevoOKLeft'); return true;"><div class="usr-btn-line-ok"></div></a>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="pview-sects-usr <?= $this->dohelpsects[0]['parent_id'] == 0 && $this->needhelpsects[0]['parent_id'] == 0 ? "usr-visibility-fix" : ""?>">
					<?php
					$slist = $this->needhelpsects;
					if( count($slist)>0 )
					{
						echo '<div class="pview-sects-tit">'.$this->localize->get("user", "need-hdr").'</div>
						<table>';

						$prev_pid = 0;

						for($i=0; $i<count($slist); $i++)
						{
							if( $prev_pid != $slist[$i]['parent_id'] )
							{
								if( $i>0 )
								{
									echo '</ul>
										</td>
									</tr>';
								}

								$prev_pid = $slist[$i]['parent_id'];

								$SLNK = $this->Page_BuildUrl("cat", $slist[$i]['url']);

								$psinfo = $this->catmodel->Catalog_SectInfo($slist[$i]['parent_id']);
								echo '<tr>
									<td class="usr-givemem">
									<a href='.$SLNK.'><img src="'.WWWHOST.FILE_DIR.$psinfo['filename_thumb'].'" alt=""></a></td>
									<td>
										<p><a href='.$SLNK.'>'.$psinfo['name'].'</a></p>
										<ul>';
							}

							echo '<li>'.$slist[$i]['name'].'</li>';
						}

						echo '</ul>
						</td>
						</tr>';

						echo '</table>';
					}


					$slist = $this->dohelpsects;
					if( count($slist)>0 ) {
						echo '<br><br>
						<div class="pview-sects-tit">'.$this->localize->get("user", "willhelp-hdr").'</div>
						<table>';

						$prev_pid = 0;

						for($i=0; $i<count($slist); $i++) {
							if ($prev_pid != $slist[$i]['parent_id']) {
								if ($i > 0) {
									echo '</ul>
									</td>
									</tr>';
								}

								$prev_pid = $slist[$i]['parent_id'];

								$SLNK = $this->Page_BuildUrl("cat", $slist[$i]['url']);
								//$psinfo = $this->catmodel->Catalog_SectInfo($slist[$i]['parent_id']);

								if (($slist[$i]['ppic'] == '') ? $dontShow = 0 : $dontShow = 1) {
									echo '<tr>
									<td class="usr-givemem"><a href=' . $SLNK . '><img src="' . WWWHOST . FILE_DIR . $slist[$i]['ppic'] . '" alt=""></a></td>
									<td>
										<p><a href=' . $SLNK . '>' . $slist[$i]['pname'] . '</a></p>
										<ul>';

									echo '<li class="tst">' . $slist[$i]['name'] . '</li>';
								}
							}
						}


						echo '</ul>
						</td>
						</tr>';

						echo '</table>';
					}
					?>


				</div>

				<div class="user-rat-info usr-rate-pos">
					<div class="user-row-rat">
						<div class="user-rat-txt"><?=$this->localize->get("user","rat")?></div>
						<div class="user-rat-rating"><?=$this->box['user_rate']?></div>
					</div>
					<svg id="prog1"></svg>
					<!--<img class="rat-img u-rat-img" src="<?=WWWHOST;?>img/myrate-<?=(round($this->box['user_rate']/6));?>.png" with="170" height="6" alt="">-->
					<script>
						$("#prog1").Progress({
							width: 273,
							height: 7,
							percent: <?=(($this->box['user_rate']/60)*100)?>,
							backgroundColor: '#f4f4f4',
							barColor: '#36b247',
							radius: 4,
							fontSize: 0,
						});
					</script>
				</div>
			</div>

			<div class="col-md-7 col-xs-7 col-lg-6 block-tabs-left-usr back-to-top">
				<style>
					#graph-wrapper{
						height: 365px !important;
					}
					#vis-container {
						height: 406px !important;
					}
					#graph-control-wrap{
						margin-left: 185px;
						height: 45px;
					}
					#graph-select-layout, .graph-ctrl-item{
						font-size: 10px;
						padding-right: 7px;
					}
					#graph-items-count{
						font-size: 10px;
					}
				</style>
				<div class="col-md-4 col-xs-4 col-lg-4 tree-share-block"><p><?=$this->localize->get("user", "open-tree")?><br>
						<?=$this->localize->get("user", "good-job-friend")?>
						</p>
					<div class="socline-pop">
						<a onclick="Share.facebook('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=facebook&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>','<?=$share_title?>', '<?=$share_descr?>');yaCounter34223620.reachGoal('PushButton_ShareDerevoFB'); return true;"><div class="line-ico-tree ico-fb-pop"></div></a>
					</div>
					<div class="socline-pop">
						<a onclick="Share.vkontakte('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=vk&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>', '<?=$share_title?>', '<?=$share_descr?>');yaCounter34223620.reachGoal('PushButton_ShareDerevoVK'); return true;"><div class="line-ico-tree ico-vk-pop"></div></a>
					</div>
					<div class="socline-pop">
						<a onclick="Share.odnoklassniki('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=ok&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>');yaCounter34223620.reachGoal('PushButton_ShareDerevoOK'); return true;"><div class="line-ico-tree ico-ok-pop"></div></a>
					</div>
					<div class="socline-pop" itemscope itemtype="http://schema.org/Movie">
						<a onclick="Share.googleplus('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=google&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>', '<?=$share_title?>', '<?=$share_descr?>');yaCounter34223620.reachGoal('PushButton_ShareDerevoGP'); return true;"><div class="line-ico-tree ico-gp-pop"></div></a>
					</div>
					<div class="socline-pop">
						<a onclick="Share.twitter('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=twitter&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>', '<?=$share_title?>');yaCounter34223620.reachGoal('PushButton_ShareDerevoTW'); return true; "><div class="line-ico-tree ico-tw-pop"></div></a>
					</div>
				</div>
				<style>
					#graph-wrapper{
						height: 365px !important;
					}
					#vis-container {
						height: 406px !important;
					}
				</style>
				<div class="cab-tree-size usr-tree">
					<div class="wnd-tree">
						<div class="usr-graph">
							<div id="vis-graph-wrap"></div>
						</div>
					</div>
				</div>

				<script>
					var graph = new ObjGraph('vis-graph-wrap', '<?=$uinfo['id']?>');
				</script>

				<div class="utabs">
					<ul class="nav nav-tabs nav-justified mytabs">
						<li role="presentation"<?=($this->viewMode == "reviews" ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("users","viewrev/".$uinfo['id']);?>"><?=$this->localize->get("user", "tab-feedbacks");?></a></li>
						<li role="presentation"<?=($this->viewMode == "" ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("users","viewinfo/".$uinfo['id']);?>"><?=$this->localize->get("user", "tab-needhelp");?></a></li>
					</ul>
				</div>
				<?php
				if( $this->viewMode == "" )
				{
					$its = $this->projlist;

					echo '<div class="proj-tbl" style="padding-left: 0; padding-right: 0;">
				<table>
				<tr>
					<th class="name">'.$this->localize->get("user", "tbl-proj").'</th>
					<th>'.$this->localize->get("user", "tbl-sten").'</th>
					<th>'.$this->localize->get("user", "tbl-left").'</th>
				</tr>';

					for( $i=0; $i<count($its); $i++ ) {
						$confirmed = $this->catmodel->Item_ReqCollected($its[$i]['id'], REQ_STATUS_CONFIRM);
						$confprog_percnt = $this->catmodel->PercentDone($confirmed['sum'], $its[$i]['amount']);
						$pics = $this->catmodel->getUserItemPics($its[$i]['id']);

						$tmleft_str = $this->catmodel->Time_LeftFromMinutes($its[$i]['tmleft']);
						$tmprog_percnt = $this->catmodel->PercentDone($its[$i]['tmleft'], $its[$i]['tmall']);

						// Показать отображение для проекта по сбору средств
						if ($its[$i]['amount_type'] == PROJ_TYPE_MONEY) {

							$monType = $this->catmodel->Currency_Info($its[$i]['currency_id'])['name'];
							$targ_str = number_format($its[$i]['amount'], 0, ",", " ") . ' ' . $monType;
							$confirm_str = number_format($confirmed['sum'], 0, ",", " ") . ' ' . $monType;
							$progress_str = $confirm_str . " / " . $targ_str;


						} else if ($its[$i]['amount_type'] == PROJ_TYPE_HUMAN) {
							$targ_str = round($its[$i]['amount']) . " чел.";
							$confirm_str = round($confirmed['sum']) . " чел.";

							$progress_str = $confirm_str . " / " . $targ_str;
						} else {
							$progress_str = round($confirmed['sum']) . " " . $this->localize->get("user", "tot-req");
						}


						$in_msg_new = $this->catmodel->Item_MessagesNum($this->UserId, $its[$i]['id'], "in", MSG_STATUS_NEW);

						echo '<tr>
        <td class="name">';
						if (isset($pics[0]['filename_ico'])) {
							echo '<div class="p-pic"><img src=' . WWWHOST . $pics[0]['filename_ico'] . ' alt="'.$its[$i]['name'].' '.$its[$i]['fname'].'" heigh="70px" width="70px"></div>';
						}else{
							echo '<div class="p-pic"><img src='.WWWHOST.'img/no-pic.png'.' alt="'.$its[$i]['name'].' '.$its[$i]['fname'].'" heigh="70px" width="70px"></div>';}
						echo '<div class="pic-align"><div class="p-tit"><a onclick="yaCounter34223620.reachGoal(\'PushButtonLinkUjePomog\'); return true;" href="'.$this->Page_BuildUrl("proj", "view/".$its[$i]['id']).'">'.$its[$i]['title2'].'</a></div>
				<div class="p-sums">'.$progress_str.'</div></div>
				'.( ($its[$i]['amount_type'] == PROJ_TYPE_MONEY) || ($its[$i]['amount_type'] == PROJ_TYPE_HUMAN) ? '<div class="p-progress"><img src="'.WWWHOST.'img/rateorg-'.(round($confprog_percnt / 10.0)).'.png" alt=""></div>' : '' ).'
        </td>
        <td><p>'.$its[$i]['stdt'].'<br>'.$its[$i]['endt'].'</p></td>
        <td>
            <div class="p-tm">
                <span><img src="'.WWWHOST.'img/progress-act.png" width="'.(ceil($tmprog_percnt * 1.2)).'" height="8" alt=""></span>
                <br>
                '.$tmleft_str.'
            </div>
        </td>
    </tr>';
					}

					if( count($its) == 0 )
					{
						echo '<tr><td colspan="3" class="noitems">
            <div class="tbl-noitems">
                <div class="tbl-noitems-txt">
                    <div><span>'.$this->localize->get("user", "tbl-noproj").'</span></div>
                </div>
                <div class="tbl-noitems-face"></div>
            </div>
        </td></tr>';
					}

					echo '</table>
    </div>';
				}
				else
				{
					$its = $this->revlist;

					echo '<div class="review-tbl rev-usr-table">
    <table>
    <tr>
        <th>'.$this->localize->get("user", "tbl-projhelp").'</th>
        <th>'.$this->localize->get("user", "tbl-feed").'</th>
        <th></th>
        <th>'.$this->localize->get("user", "tbl-rate").'</th>
    </tr>';


					for($i=$this->from; $i<$this->to; ++$i) {
						if(!isset($its[$i]['req_amount'])) break;
						$moneyname = ($its[$i]['currency_id']==1? $its[$i]['req_amount']."<br>".$this->localize->get("successfull", "uah") : $its[$i]['req_amount']."<br>".$this->localize->get("successfull", "rub"));
						$money = ($its[$i]['req_amount']==1? "1<br>".$this->localize->get("successfull", "people") : $moneyname);

						if(!isset($its[$i])) break;

						//echo '<pre>';
						//print_r($its[$i]);

						$its_rate = $this->catmodel->getBuyerReqRate($its[$i]['id']);

						if(empty($its_rate)) {
							$its_rate[0]['rate_comments'] = $this->localize->get("projview", "u-ratecomm");
							$its_rate[0]['rate'] = 10;
							$its_rate[0]['author_id'] = $its[$i]['uid'];
						}


						echo '<tr>
            <td>
            	<div class="p-img"><img src='.WWWHOST.($its[$i]['pic_sm'] ? $its[$i]['pic_sm'] : 'img/no-pic.png' ).' heigh="60px" width="60px" alt=""></div>
            	<div class="p-txt">
                <div class="p-tit"><a onclick="yaCounter34223620.reachGoal(\'PushButtonLinkUjePomog\'); return true;" href="'.$this->Page_BuildUrl("proj", "view/".$its[$i]['item_id']).'">'.$its[$i]['title2'].'</a></div>
                <div class="p-date">'.$its[$i]['add_date'].'</div>
            	</div>
            </td>
            <td>
                <div class="r-comment">'.$its_rate[0]['rate_comments'].'</div>
                <div class="r-author"><a href="'.$this->Page_BuildUrl("users", "viewrev/".$its_rate[0]['author_id']).'">'.($its[$i]['account_type'] == USR_TYPE_PERS ? $its[$i]['name'].' '.$its[$i]['fname'] : $its[$i]['orgname']).'</a></div>
            </td>
            <td>
            	<div class="usr-money-txt">'.$money.'</div>
            </td>
            <td>
                <div class="r-rate">
                    <span>'.$its_rate[0]['rate'].'</span>
                    <!--<div class="p-progress"><img src="'.WWWHOST.'img/rateorg-'.($its_rate[0]['rate']).'.png" alt=""></div>-->
                </div>
            </td>
        </tr>';
					}


					if( count($its) == 0 )
					{
						echo '<tr><td colspan="3" class="noitems">
            <div class="tbl-noitems">
                <div class="tbl-noitems-txt">
                    <div><span>'.$this->localize->get("user", "tbl-norate").'</span></div>
                </div>
                <div class="tbl-noitems-face"></div>
            </div>
        </td></tr>';
					}

					echo '</table>
    </div>';
				}
				?>

				<?php /**** PAGINATION WITH ELLISPSIS ****/
				if (count($its) > 0 && $this->viewMode == "reviews") {
					$itemCount = count($its);
					$currentPage = $this->pageId;
					$itemsPerPage = $this->perPage;
					$adjacentCount = 1;
					$pageLinkTemplate = (isset($this->surl) ? $this->Page_BuildUrl($this->surl) : $this->Page_BuildUrl("users", "viewrev/" . $uinfo['id']));
					$pageLinkTemplate .= "p_" . '%d' . "/";

					$showPrevNext = true;

					$firstPage = 1;
					$lastPage = ceil($itemCount / $itemsPerPage);
					if ($lastPage != 1) {
						if ($currentPage <= $adjacentCount + $adjacentCount) {
							$firstAdjacentPage = $firstPage;
							$lastAdjacentPage = min($firstPage + $adjacentCount + $adjacentCount, $lastPage);
						} elseif ($currentPage > $lastPage - $adjacentCount - $adjacentCount) {
							$lastAdjacentPage = $lastPage;
							$firstAdjacentPage = $lastPage - $adjacentCount - $adjacentCount;
						} else {
							//echo $firstPage.'<br>';
							//echo $currentPage.'<br>';
							!($currentPage - ($firstPage+3)) ? $firstAdjacentPage = $currentPage-2 : $firstAdjacentPage = $currentPage - $adjacentCount;
							//$firstAdjacentPage = $currentPage - 1;
							($currentPage + ($firstPage)+2) == $lastPage ? $lastAdjacentPage = $currentPage+2 : $lastAdjacentPage = $currentPage + $adjacentCount;
							//$lastAdjacentPage = $currentPage + $adjacentCount;
						}

						echo '<div class="proj-pages">			
			<nav class="navbar-right">
			<ul class="pagination">';
						if ($showPrevNext) {
							if ($currentPage == $firstPage) {
								echo '<li><span>&lt;</span></li>';
							} else {
								echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($currentPage - 1) : sprintf($pageLinkTemplate, $currentPage - 1)) . '">&lt;</a></li>';
							}
						}
						if ($firstAdjacentPage > $firstPage) {
							echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($firstPage) : sprintf($pageLinkTemplate, $firstPage)) . '">' . $firstPage . '</a></li>';
							if ($firstAdjacentPage > $firstPage + 1) {
								echo '<li><span>...</span></li>';
							}
						}
						for ($i = $firstAdjacentPage; $i <= $lastAdjacentPage; $i++) {

							if ($currentPage == $i) {
								echo '<li class="active"><span>' . $i . '</span></li>';
							} else {
								echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($i) : sprintf($pageLinkTemplate, $i)) . '">' . $i . '</a></li>';
							}
						}
						if ($lastAdjacentPage < $lastPage) {
							if ($lastAdjacentPage < $lastPage - 1) {
								echo '<li><span>...</span></li>';
							}
							echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($lastPage) : sprintf($pageLinkTemplate, $lastPage)) . '">' . $lastPage . '</a></li>';
						}
						if ($showPrevNext) {
							if ($currentPage == $lastPage) {
								echo '<li><span>&gt;</span></li>';
							} else {
								echo '<li><a href="' . (is_callable($pageLinkTemplate) ? $pageLinkTemplate($currentPage + 1) : sprintf($pageLinkTemplate, $currentPage + 1)) . '">&gt;</a></li>';
							}
						}
						echo '</ul>
					</nav>
					<div class="clearfix"></div>
			</div>';
					}
				}
				/*$its_total = count($its);

                if( $its_total > 0 )
                {
                    $TOTAL_PAGES = ceil($its_total / REVIEWS_PER_PAGE);

                    if( $TOTAL_PAGES > 1 )
                    {
                        echo '<div class="proj-pages">
                    <nav class="navbar-right">
                    <ul class="pagination">';

                        for( $i=1; $i<=$TOTAL_PAGES; $i++ )
                        {
                            $PPURL = ( isset($this->surl) ? $this->Page_BuildUrl($this->surl) : $this->Page_BuildUrl("users", "viewrev/".$uinfo['id']) );
                            if( $i>1 )
                                $PPURL = $PPURL."p_".$i."/";
                            echo '<li'.(($i == $this->pageId) || (($this->pageId == 0) && ($i==1)) ? ' class="active"' : '').'><a href="'.$PPURL.'">'.$i.'</a></li>';
                        }

                        echo '</ul>
                            </nav>
                            <div class="clearfix"></div>
                    </div>';
                    }
                }*/
				?>
				<script src="<?=WWWHOST.'js/owl.carousel.min.js'?>"></script>
				<div class="user-last-help rev-usr-table">
					<div class="last-help-txt"><?=$this->reqlist==null? "":$this->localize->get("user", "dohelp-hdr").' '.$uinfo['name'].' '.$uinfo['fname']?></div>
					<div class="owl-carousel-prt">
						<?php for($i=0; $i<count($this->reqlist); $i++) { ?>
							<div class="ind-car-act">
								<a href="<?=$this->Page_BuildUrl("proj", "view/".$this->reqlist[$i]['item_id'])?>">  <div class="car-act-usr-photo">
										<style>
											.owl-carousel .owl-item img{
												width: 70px !important;
											}
										</style>
										<img src="<?=WWWHOST.($this->reqlist[$i]['pic_sm'] ? $this->reqlist[$i]['pic_sm'] : 'img/no-pic.png')?>" class="usr-photo-resize" alt="" height="50px" width="50px">
									</div>
									<span class="pview-rel-usrs pview-rel-usrpers"><a class="usr-name-pos usr-name-color" href="<?=$this->Page_BuildUrl("users", "viewrev/".$this->reqlist[$i]['uid'])?>"><?=$this->reqlist[$i]['name']?></a></span>


								</a>
							</div>
						<?php } ?>
					</div>
					<script>
						$('.owl-carousel-prt').owlCarousel({
							loop:true,
							margin:10,
							nav:true,
							responsive:{
								0:{
									items:3
								},
								600:{
									items:4
								},
								1000:{
									items:5
								}
							}
						});
						$('.owl-prev').html('');
						$('.owl-prev').addClass('owl-hlp-lt').addClass('my-owl-nav');
						$('.owl-next').html('');
						$('.owl-next').addClass('owl-hlp-rg').addClass('my-owl-nav');
					</script>
				</div>

			</div>
		</div>
	</div>
</div>
<?php
$this_url = $_SERVER['REQUEST_URI'];
$redir_pars = Array("redirto" => $this_url);
$redir_pars_str = http_build_query($redir_pars);
?>
<script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
<script>
	$(document).ready(function () {

		Share = {
			facebook: function (link, title, descr) {

				//var canvas = document.getElementsByTagName("canvas");

				//SaveTreeImg(canvas, '<?=$this->UserId?>', '<?=$uinfo['id']?>');

				//var pic = '<?=WWWHOST?>' + 'img/tree_img/' + '<?=$this->UserId?>' + '_' + '<?=$uinfo['id']?>' + "_scr.png?rnd=" + Math.random();

				url = "https://www.facebook.com/dialog/feed?app_id=" + '<?=FB_APP_ID?>' + "&link=" + encodeURIComponent(link) +
					"&picture=" + encodeURIComponent('<?=WWWHOST.'img/user_share/fb_'.$uinfo['id'].'.png'?>') +
					"&name=" + encodeURIComponent(title) +
					"&description=" + encodeURIComponent(descr) +
					"&redirect_uri=https://www.facebook.com";
				Share.popup(url);
			},
			vkontakte: function (purl, ptitle, text) {

				//var canvas = document.getElementsByTagName("canvas");

				//SaveTreeImg(canvas, '<?=$this->UserId?>', '<?=$uinfo['id']?>');

				//var pic = '<?=WWWHOST?>' + 'img/tree_img/' + '<?=$this->UserId?>' + '_' + '<?=$uinfo['id']?>' + "_scr.png?rnd=" + Math.random();

				url = 'http://vkontakte.ru/share.php?';
				url += 'url=' + encodeURIComponent(purl);
				url += '&title=' + encodeURIComponent(ptitle);
				url += '&description=' + encodeURIComponent(text);
				url += '&image=' + encodeURIComponent('<?=WWWHOST.'img/user_share/vk_'.$uinfo['id'].'.png'?>');
				Share.popup(url);
			},
			odnoklassniki: function (purl) {

				//var canvas = document.getElementsByTagName("canvas");

				//SaveTreeImg(canvas, '<?=$this->UserId?>', '<?=$uinfo['id']?>');

				url = 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1';
				url += '&st.comments=' + encodeURIComponent('text');
				url += '&st._surl=' + encodeURIComponent(purl);
				Share.popup(url);
			},
			googleplus: function (purl, ptitle, text) {

				//var canvas = document.getElementsByTagName("canvas");

				//SaveTreeImg(canvas, '<?=$this->UserId?>', '<?=$uinfo['id']?>');

				//var pic = '<?=WWWHOST?>' + 'img/tree_img/' + '<?=$this->UserId?>' + '_' + '<?=$uinfo['id']?>' + "_scr.png?rnd=" + Math.random();

				url = 'http://plus.google.com/share?';
				url += 'url=' + encodeURIComponent(purl);
				Share.popup(url);
			},

			twitter: function (purl, text) {
				url = 'http://twitter.com/share?';
				url += 'text='+text;
				url += '&url='+ encodeURIComponent(purl);
				Share.popup(url);
			},

			popup: function (url, soc) {
				window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
			}
		}

		function SaveTreeImg(canvas, uid, oid) {

			var dataURL = 'image=' + canvas[0].toDataURL('image/png', 0.8) + '&uid=' + uid + '&oid=' + oid;

			$.ajax({
				type: "POST",
				url: req_ajx_host + "ajx/loadimg/",
				data: dataURL,
			}).done(function (o) {
				//console.log(o);
			});
		}

		$('#spsbtn').bind('click', function () {
			if( !usr_logged ) {
				popWnd('logdlg', '<?=$redir_pars_str?>');
				return false
			}

			popWnd('gretitude', 'req='+'<?=$this->req?>');
			return false
		});
	});

</script>