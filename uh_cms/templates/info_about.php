<?php $BCHTML = $this->renderBreadcrumbs();	 ?>
<div class="row">
    <div class="top-block top-block-pr col-md-12 col-xs-12 col-lg-12">
        <div class="top-block-txt top-block-pr-txt">
            <div class="container">
                <ol class="breadcrumb">
                    <?=$BCHTML;?>
                </ol>
            </div>
            <h1><?=$this->pageInfo[0]['page_text']?></h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="container des-cont-all info-cont-all">
        <div class="fees-about info-about"><?=$this->pageInfo[1]['page_text']?></div>
    </div>
</div>
<div class="row">
    <div class="container-fluid about-top-bl">
        <div class="container">
            <div class="about-subbl">
                <div class="about-subtop-img"></div>
                <div class="about-subtop-txt-img">
                    <div><?=$this->pageInfo[2]['page_text']?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="ourteam">
    <div class="container">
        <div class="about-res-txt"><?=$this->pageInfo[3]['page_text']?></div>
        <div class="about-res-bl">
            <div class="res-bl-info res-bl-info-bl1">
                <div class="about-cir-img"></div>
                <div class="about-res-info-txt"><?=$this->pageInfo[4]['page_text']?></div>
            </div>
            <div class="res-bl-info res-bl-info-bl2">
                <div class="about-res-info-txt"><?=$this->pageInfo[5]['page_text']?></div>
                <div class="about-cir-img"></div>
            </div>
            <div class="res-bl-info res-bl-info-bl3">
                <div class="about-cir-img"></div>
                <div class="about-res-info-txt"><?=$this->pageInfo[6]['page_text']?></div>
            </div>
            <div class="about-res-txt abou-res-info-h"><?=$this->pageInfo[7]['page_text']?></div>
            <div class="res-bl-info res-bl-info-bl4">
                <div class="about-res-info-txt"><?=$this->pageInfo[8]['page_text']?></div>
                <div class="about-cir-img"></div>
            </div>
            <div class="res-bl-info res-bl-info-bl5">
                <div class="about-res-info-txt"><?=$this->pageInfo[9]['page_text']?></div>
                <div class="about-cir-img"></div>
            </div>
            <div class="res-bl-info res-bl-info-bl6">
                <div class="about-res-info-txt"><?=$this->pageInfo[10]['page_text']?></div>
                <div class="about-cir-img"></div>
            </div>
        </div>
        <div class="about-subp-pos">
            <div class="about-res-info-subp"><?=$this->pageInfo[11]['page_text']?></div>
            <hr class="res-info-hr">
        </div>
        <div class="about-res-txt about-uhelp-h"><?=$this->pageInfo[12]['page_text']?></div>
        <div class="about-uhelp-a">
            <a href="<?=$this->Page_BuildUrl('info','tree')?>"><div class="about-uhelp-btn"><?=$this->localize->get("info-about", "watch-how")?></div></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="container-fluid">
        <div class="info-about-map">
            <div class="container">
                <p><?=$this->pageInfo[13]['page_text']?></p>
            </div>
        </div>
    </div>
</div>
<div class="row" id="sendmsg">
    <div class="container-fluid">
        <div class="info-about-con">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-xs-6 about-con-lbl">
                    <div class="con-lbl-txt col-lg-12 col-md-12 col-xs-12"><?=$this->pageInfo[14]['page_text']?></div>
                    <div class="con-lbl-txt con-lbl-contacts col-lg-12 col-md-12 col-xs-12"><?=$this->pageInfo[15]['page_text']?></div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6 about-con-rbl">
                    <form method="post" action="<?=$this->Page_BuildUrl('info','about').'send/'?>">
                        <label for="name"><?=$this->localize->get("info-about", "your-name")?></label><br>
                        <input id="name" name="name" required><br>
                        <label for="mail">E-mail</label><br>
                        <input id="mail" name="email" required><br>
                        <label for="phone"><?=$this->localize->get("info-about", "your-phone")?></label><br>
                        <input id="phone" name="phone" required><br>
                        <label for="umasg"><?=$this->localize->get("info-about", "what-can-help")?></label><br>
                        <textarea id="umsg" name="umsg" cols="40" rows="10" required></textarea><br>
                        <input type="submit" name="submit" class="about-btn-send">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
