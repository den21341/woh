<?php
    $shareTitle = strip_tags($this->localize->get("popup", "postreg-title"));
    $shareText = $this->localize->get('popup','postreg-share');

?>

<div class="post-reg">
        <h3><?=$this->localize->get("popup", "postreg-title")?></h3>
        <p><?=$this->localize->get("popup", "postreg-text")?></p>
 </div>