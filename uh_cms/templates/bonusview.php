<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$show_send_msg_lnk = false;

////////////////////////////////////////////////////////////////////////////

$PM = $this->pageModel;

$page = $PM->get_page();

// Set page head title (h1 tag value)
$PAGE_H1 = $page->title;

// Build bread crumbs
$this->addBreadcrumbs(WWWHOST.'bonus/list/', $this->localize->get("bonview", "boncat"));
$BCHTML = $this->renderBreadcrumbs($PAGE_H1);


if( isset($this->projinfo) && ($this->viewMode == "") )
	$PAGE_H1 = $this->projinfo['title'];

if( isset($this->title) )
	$PAGE_H1 = $this->title;
?>
<script type="text/javascript" src="<?=WWWHOST?>js/jquery.progress.js"></script>

<div class="row-head breacrumb-bgi">
	<div class="container">
		<ol class="breadcrumb">
			<?=$BCHTML;?>
		</ol>
		<h1><?=$PAGE_H1;?></h1>
	</div>
</div>

<?php

$proj = $this->projinfo;
$pics = $proj['photos'];

//$proj_is_thing = ( $proj['profile_id'] == PROJ_THINGS );

//var_dump($proj['currency']);

//$currency_name = ( isset($proj['currency']['name']) ? $proj['currency']['name'] : "" );

// local vars for fast use

// viewer request checks
$isReqSent = ( ($reqMy = $this->catmodel->Buyer_ReqIsSend($this->UserId, $proj['id'])) !== false );
$isMy = ($this->UserId == $proj['author_id']);

$show_addsum_btn = false;
$show_addman_btn = false;
$show_getthing_btn = false;

$show_send_msg_lnk = ( !$isMy && ($this->UserId != 0) );

$uinfo = $this->catmodel->getDateBox($proj['author_id']);

$total_sum = $this->UsrInfo;
$ugroup = $this->UsrGroup;

$allUinfo = $this->AllUinfo;

$helpnum = $this->HelpNum;

$comlist = $this->comlists;

$loc = (isset($this->user_info['loc_info']['name']) == "" ? $this->localize->get("cabinet", "noselected") : $this->user_info['loc_info']['name']);
$cntry = (isset($this->user_info['loc_info']['countryname']) == "" ? '('.$this->localize->get("cabinet", "noselected").')' : '('.$this->user_info['loc_info']['countryname'].')');
$location = ($loc." ".$cntry);
//echo '<pre>';
//print_r($comlist);
//echo '</pre>';


$pic_html = '';

?>
<div class="row">
	<div class="container">
		<div class="col-xs-12 col-md-7 col-sm-8">
			<div class="pview-bon-left">
				<div class="pview-pix clearfix">
					<div>
						<div class="col-md-4">
							<button id="getbonusimg" class="getbonus-img" onclick="yaCounter34223620.reachGoal('PushButtonPoluchitBonus'); return true;"><span class="fancybox.ajax"><img src="<?=isset($pics[0]['filename_ico']) ? WWWHOST.$pics[0]['filename_ico'] : WWWHOST."img/no-pic.png"?>" class="img-border" width="215" height="215""></span></button>

							<?php
							if( $proj['amount'] <= $proj['amount_used'] )
							{
								echo '<b>'.$this->localize->get("bonusview", "nomore").'</b>';
							}
							else
							{
								echo '
	<button id="getbonusbtn" onclick="yaCounter34223620.reachGoal(\'PushButtonPoluchitBonus\'); return true;" class="btn bon-btn-def btn-bon-do"><span class="fancybox.ajax"><img src="'.WWWHOST.'img/bon-hand.png" width="31" height="27"></span> <a class="bon-text-btn">'.$this->localize->get("bonusview", "btn-getbonus").'</a></button>';
								$show_addman_btn = true;
							}
							?>

						</div>
						<div class="col-md-8 bon-width-text">
							<!--<p class="text-bot-padding"><?=$proj['descr0'];?></p></div>-->
							<div class="info-ground bon-text-all">
								<?php
								//$proj['descr0'] = preg_replace('<br/>', '', $proj['descr0']);
								$proj['descr0'] = preg_replace('#<a.*>.*</a>#USi', '', $proj['descr0']);
								if(strlen($proj['descr0']) > 260) {
									$subTxt = substr($proj['descr0'], 0, 260).'...';
									$subTxtStrip = strip_tags($subTxt);
									$subMore = '<span class="more"> '.$this->localize->get("bonview", "bonshow").'</span>';
									//$subTxtAll = substr($proj['descr0'], 540, strlen($proj['descr0']));
									$subTxtAll = $proj['descr0'];
								} else {
									$subTxtStrip = $proj['descr0'];
									$veloHeight = 'set-h';
								}

								?>
								<div class="proj-txt-desc txt-teaser <?=((isset($veloHeight) && $is_dream) ? $veloHeight : '')?> "><?=$subTxtStrip?><?= (isset($subMore) ? $subMore : '') ?></div>
								<?=isset($subTxtAll) ? '<div class="proj-txt-desc txt-complete ">'.$subTxtAll.'<span class="less"> '.$this->localize->get("bonview", "bonhide").'</span></div>' :''?>
							</div>


							<div class="pview-bon-dates">
								<?=$this->localize->get("bonusview", "dt-add");?>: <?=$this->build_DateStr($proj['dy'], $proj['dm'], $proj['dd'], $proj['dh'], $proj['dmin'], " в ");?><br>
								<?=$this->localize->get("bonusview", "views");?>: <?=number_format($proj['item_rate'], 0, ".", " ");?>
							</div>

							<div class="pview-bon-target">

								<p class="text-bon-size"><?=$this->localize->get("bonusview", "stat-till");?> <span class="text-size-color-bon"><?=$proj['edd'].' '.$this->build_MonthStr($proj['edm']);?></span>
									<br>Осталось бонусов: <span class="text-size-color-bon"><?=$proj['amount'] - $proj['amount_used'];?></span></p>


							</div>

							<div class="jh-sclike-rows">
								<div class="jh-sclike jh-sclike-fb sclike-but sclike-fb-count">
									<iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fwayofhelp.com%2Fbonus%2Fview%2F<?=$proj['id']?>%2F&layout=button_count&size=small&mobile_iframe=true&width=110&height=20&appId" width="110" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
								</div>

								<div class="jh-sclike jh-sclike-vk sclike-but">

									<script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
									<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
									<!-- Put this div tag to the place, where the Like block will be -->
									<div id="vk_like"></div>
									<script type="text/javascript">
										<?php
										/*
                                        ?>
                                        VK.Share.button(false,{type: "round", text: "Поделиться"});
                                        VK.Widgets.Like("vk_like", {type: "button", verb: 1, height: 20});

                                        document.getElementById('vk_like').innerHTML = VK.Share.button('<?=$this->Page_BuildUrl("bonus", "view/".$proj['id']);?>', {type: 'button', text: 'Поделиться', height: '20px'});
                                        */
										?>

										/************ VK INIT *****id=4923828*******/

										VK.init({
											apiId: 5361534
										});
										VK.Widgets.Like("vk_like", {type: 'button', height: '20px', pageUrl:'<?=WWWHOST.'bonus/view/'.$proj['id'].'/?run=bonus&utm_source=vk&utm_medium=social&utm_campaign=ShareBonus&utm_content='.$proj['id']?>'}, '<?=$proj['id']?>');

										VK.Observer.subscribe("widgets.like.shared", function f(){
											is_true = true;
											yaCounter34223620.reachGoal('PushButtonBonusFormaVK');
										});

										/*********************************/
										//document.getElementById('vk_like').innerHTML = VK.Share.button('<?=$this->Page_BuildUrl("bonus", "view/".$proj['id']).($this->UserId != 0 ? '?jhscvisitor=1&jhscreferer='.$this->UserHash : '');?>', {type: 'custom', text: '<span class="jh-vk-share"></span>', height: '25px'});
									</script>
								</div>
								<div class="jh-sclike jh-sclike-od sclike-but">
									<div id="ok_shareWidget"></div>
									<script>
										!function (d, id, did, st) {
											var js = d.createElement("script");
											js.src = "http://connect.ok.ru/connect.js";
											js.onload = js.onreadystatechange = function () {
												if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
													if (!this.executed) {
														this.executed = true;
														setTimeout(function () {
															OK.CONNECT.insertShareWidget(id,did,st);
														}, 0);
													}
												}};
											d.documentElement.appendChild(js);
										}(document,"ok_shareWidget","<?=$this->Page_BuildUrl("bonus", "view/".$proj['id']).($this->UserId != 0 ? '?jhscvisitor=1&jhscreferer='.$this->UserHash.'&utm_source=ok&utm_medium=social&utm_campaign=ShareBonus&utm_content='.$proj['id'] : '?utm_source=ok&utm_medium=social&utm_campaign=ShareBonus&utm_content='.$proj['id'])?>","{width:115,height:22,st:'straight',sz:20,ck:2}");

										function listenForShare() {
											if (window.addEventListener) {
												window.addEventListener('message', onShare, false);
											} else {
												window.attachEvent('onmessage', onShare);
											}
										}
										function onShare(e) {
											var args = e.data.split("$");
											yaCounter34223620.reachGoal('PushButtonBonusFormaOK');
											if (args[0] == "ok_shared") {
												console.log(args[1]); // Вывод идентификатора фрейма кнопки - в случае нескольких кнопок на одной странице, по нему можно определить какая именно была кликнута
												is_true = true;
											}
										}
										listenForShare();

									</script>
								</div>
								<div class="both"></div>
							</div>

						</div>
					</div>
					<script type="text/javascript">
						var is_true = false;

						$(document).ready(function(){
							$("#jh-share-fb_").bind("click", function(){
								yaCounter34223620.reachGoal('PushButtonBonusFormaFB');

								FB.ui({
									method: 'share',
									href: $(this).attr("data-share-url"),
								}, function(response){
									if (response === null) {
										//console.log('was not shared');
									} else {
										is_true = true;
									}
								});
								return false
							});
							var url = '<?=$this->Page_BuildUrl("bonus", "view/".$proj['id']).($this->UserId != 0 ? '?jhscvisitor=1&jhscreferer='.$this->UserHash : '').'&utm_source=facebook&utm_medium=social&utm_campaign=ShareBonus&utm_content='.$proj['id']?>';
							$.getJSON('http://graph.facebook.com/' + url, function (json)
							{
							});
						});
					</script>


					<div class="both"></div>

					<?php
					// For testing (alex@uh.ua account)
					/*
                    if($this->UserId == 50)
                    {
                        echo '
                            <div class="pview-actions-avail">Осталось бонусов: <span>'.($proj['amount'] - $proj['amount_used']).'</span></div>
                            <button id="getbonusbtn" class="btn btn-primary"><span class="glyphicon glyphicon-thumbs-up"></span> '.$this->localize->get("bonusview", "btn-getbonus").'</button>';
                        $show_addman_btn = true;
                    }
                    else
                    {
                    */

					//}
					?>


					<?php if(strlen($proj['descr'])) { ?>
						<div class="pview-full bon-justy">
							<div class="h3 text-bon-center"><?=$this->localize->get("bonusview", "hdr-descr");?></div>
							<?=$proj['descr']?>
						</div>
					<?php }
					$coms = $this->comlists;

					if(count($coms)>0) { ?>

						<div class="pview-comments def-bg">
							<?php
							$coms = $this->comlists;
							//print_r($coms);
							if( ($this->UserId != 0) || (count($coms)>0) )
							{
								echo '<div class="com-h3">'.$this->localize->get("bonusview", "hdr-comment").'</div>';
							}
							if(empty($coms))
								echo '<div class="no-comment">'.$this->localize->get("bonview", "bonnocomm").'</div>';
							else
								for( $i=0; $i<count($coms); $i++ )
								{
									echo '<div class="pview-comment-it pview-comm">
								<div class="row">
									<div class="col-md-3 col-xs-3 col-lg-3 pview-comm-border">
										<div class="pview-comm-usr">';
									($coms[$i]['pic']) ? $psrc = (WWWHOST.$coms[$i]['pic']) : $psrc = (WWWHOST.'img/no-pic.png');
									echo '<a href='.$this->Page_BuildUrl("users", "viewrev/".$coms[$i]['author_id']).'><img class="usr-comm-pic" src='.$psrc.' alt="" height="50px" width="50px">
										<span class="pview-usr-ico pview-com-usr">'.( $coms[$i]['account_type'] == USR_TYPE_PERS ? $coms[$i]['name'].' '.$coms[$i]['fname'] : $coms[$i]['orgname'] ).'</span></a>
									</div>
									</div>
									<div class="col-md-9 col-xs-7 col-lg-10">
									<div class="bon-pview-comm-txt">
										<div class="pview-comment-msg">'.$coms[$i]['content'].'</div>
										<span class="pview-comment-dt">'.$this->localize->get("projview", "dt-comment").': '.$coms[$i]['add_dt'].'</span>
									</div>
									</div>
									
								</div>
								</div>';
									//print_r($coms[$i]['id']);
								}
							?>


						</div>

					<?php }

					if( $this->UserId != 0 )
					{
						?>
						<div class="pview-comments-add def-bg">
							<form id="projcomadd" class="regform" action="<?=$this->Page_BuildUrl("bonus", "addcomment");?>" method="POST">
								<input type="hidden" name="action" value="addcomment">
								<input type="hidden" name="projid" value="<?=$proj['id'];?>">
								<a id="formstart" name="formstart"></a>
								<?php
								if( $this->reqdata->has_errors() )
								{
									echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
								}
								?>
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<div class="form-group<?=( !$this->reqdata->ok('comment') ? ' has-error' : '' );?>">
											<h3 class="rdescr-align"><?=$this->localize->get("bonusview", "lbl-comment");?></h3>
											<textarea class="form-control" id="comment" name="comment" rows="4"><?=$this->reqdata->comment;?></textarea>
											<div class="alert alert-danger" role="alert"><?=$this->localize->get("bonusview", "err-comment");?></div>
										</div>
									</div>
								</div>
								<div class="p-btn-align btn-form-pad"><input type="submit" class="btn btn-success btn-succes-p" value="<?=$this->localize->get("bonusview", "btn-comment");?>" /></div>
							</form>
						</div>
						<?php
					} else
						echo '<div class="login-comment"><p>'.$this->localize->get("bonview", "nolog").'</p><p>'.$this->localize->get("bonview", "nologcomm").' <a onclick="popWnd(\'logdlg\', \'\');" rel="nofollow">'.$this->localize->get("bonview", "nologon").'</a>.</p>
								<p>'.$this->localize->get("bonview", "nouser").' <a href='.$this->Page_BuildUrl('registration','person').'>'.$this->localize->get("bonview", "register").'</a>.</p></div>';
					?>
				</div>
			</div>
		</div>
		<style type="text/css">
			@import url("<?=WWWHOST?>css/graph.css");
			@import url("<?=WWWHOST?>css/vis.css");
		</style>
		<script type="text/javascript" src="<?=WWWHOST;?>js_vsgraph/my_vis.js"></script>
		<script type="text/javascript">
			isMine = true;
		</script>
		<script type="text/javascript" src="<?= WWWHOST?>js_vsgraph/obj_graph_baloon.js"></script>
		<div class="col-xs-12 col-md-5 col-sm-5">
			<!--<div class="proj-right-info">
				<div class="proj-right-usr-top">
					<div class="info-usr-pic">
						<a href="<?=$this->Page_BuildUrl("users", "viewrev/".$proj['author_id']);?>"><img src="<?=WWWHOST.( $proj['pic'] != "" ? $proj['pic'] : 'img/no-pic.png' );?>" class="usr-pic-img" width="72" height="72"></a>
					</div>
					<div class="info-usr-block-r">
						<div class="info-usr-name">
							<a href="<?=$this->Page_BuildUrl("users", "viewinfo/".$proj['author_id']);?>"><?=$proj['name']." ".$proj['fname'];?></a>
						</div>
						<div class="info-usr-con">
							<?php
							if( !$isMy )
							{
								?>
								<a id="sendmsglnk" href="#"><div class="con-msg con-basic"></div></a>
								<a href="#" id="lnkshowtree" data-uid="<?=$proj['author_id'];?>" title="<?=$this->localize->get("projview", "btn-tree");?>"><div class="con-tree con-basic"></div></a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="info-usr-stat">
					<div class="usr-stat-status">
						<a href="<?=$this->Page_BuildUrl("info/stat")?>" class="proj-a-style"><div class="status-img p-anim"></div>
							<div class="status-name <?=strlen($this->statusBox->getStatus($uinfo['status']))<=12 ? 'bon-stat-w' : 'bon-sm-stat-w'?>"><?=$this->statusBox->getStatus($uinfo[0]['status'])?></div></a>
					</div>
					<div class="usr-stat-money">
						<a href="<?=$this->Page_BuildUrl("info/power")?>" class="proj-a-style"><div class="money-img p-anim" ></div>
							<div class="money-count <?=strlen($this->statusBox->getStatus($uinfo['status']))<=12 ? 'bon-stat-w' : 'bon-sm-stat-w'?>"><?=$uinfo[0]['money']?></div></a>
					</div>
				</div>
				<div class="user-a-row a-row-h row">
					<div class="block-ad-img col-md-6 col-xs-6 col-lg-6">
						<a href="<?=$this->Page_BuildUrl("info/angelsinfo")?>">
							<div class="user-box-ad-img ang-anim col-lg-3 col-xs-3 col-lg-3" title="<?=$this->localize->get("view", "ang-day");?>"></div></a>
						<div class="user-box-ang-count ang-wr col-lg-9 col-xs-9 col-md-9 mob-counter"><?=$allUinfo['ad_count']?></div>
					</div>
					<div class="block-u-mon col-md-4 col-xs-4 col-lg-4">
						<a href="<?=$this->Page_BuildUrl("info/angelsinfo")?>">
							<div class="bon-box-aw-img ang-anim" title="<?=$this->localize->get("view", "ang-week");?>"></div></a>
						<div class="user-box-ang-count ang-lh mob-counter proj-a-counter"><?=$allUinfo['aw_count']?></div>
					</div>
				</div>
				<hr>
				<div class="info-usr-loc">
					<div class="usr-loc-face">
						<div class="location-sample">
							<div class="face-img p-anim"></div>
						</div>
						<div class="face-txt location-txt"><?=$ugroup?></div>
					</div>
					<div class="usr-loc-location">
						<div class=" location-sample">
							<div class="location-img p-anim"></div>
						</div>
						<div class="location-txt location-txt"><?=$proj['city_data']['name']?></div>
					</div>
				</div>
				<hr>
				<div class="user-rat-info bon-center-rate">
					<div class="user-row-rat">
						<div class="user-rat-txt"><?=$this->localize->get("user","rat")?></div>
						<div class="user-rat-rating"><?=$uinfo[0]['user_rate']?></div>
					</div>
					<svg id="prog2"></svg>
					<!--<img class="rat-img u-rat-img" src="<?=WWWHOST_TEST;?>img/myrate-<?=(round($this->box['user_rate']/6));?>.png" with="170" height="6" alt="">-->
					<!--<script>
						$("#prog2").Progress({
							width: 290,
							height: 7,
							percent: <?=(($uinfo[0]['user_rate']/60)*100)?>,
							backgroundColor: '#f4f4f4',
							barColor: '#36b247',
							radius: 4,
							fontSize: 0,
						});
					</script>
				</div>
				<div class="user-help-info">
					<p><span id="allhlp" onclick="wndAllhlp(-1)"><?=$this->localize->get("user", "stat-tothelp");?></span>: <span class="help-count">&nbsp;&nbsp;<?=$allUinfo['helpnum']?></span></p>
					<p><span  id="monhlp" onclick="wndAllhlp(0)"><?=$this->localize->get("user", "stat-helpmoney");?></span>: <span class="help-count">&nbsp;&nbsp;<?=$allUinfo['helpmoney']['totnum']?></span></p>
					<p><?=$this->localize->get("user", "stat-helpsum");?>: <span class="help-count">&nbsp;&nbsp;<?=($total_sum['totsum']==0? "0" : $total_sum['totsum'])?> грн.</span>
				</div>
				<hr>
			</div>-->

			<div class="proj-right-info parent">
				<div class="proj-right-usr-top">
					<div class="info-usr-pic">
						<a href="<?=$this->Page_BuildUrl("users", "viewrev/".$proj['author_id']);?>"><img src="<?=WWWHOST.( $proj['pic'] != "" ? (file_exists($proj['pic']) ? $proj['pic'] : 'img/no-pic.png') : 'img/no-pic.png' );?>" class="usr-pic-img" width="72" height="72" alt="<?=$proj['name']." ".$proj['fname'];?>" title="<?=$proj['name']." ".$proj['fname'];?>" ></a>
					</div>
					<div class="info-usr-block-r">
						<div class="info-usr-name">
							<a href="<?=$this->Page_BuildUrl("users", "viewrev/".$proj['author_id']);?>"><p class="proj-name-size"><?=$proj['name']." ".$proj['fname'];?></p></a>
						</div>
						<div class="info-usr-con">

							<?php if( !$isMy ) { ?>
								<a id="sendmsglnk" href="#" onclick="yaCounter34223620.reachGoal('PushButtonSoobshenie'); return true;"><div class="con-msg con-basic"><img class="con-msg-img" src="<?=WWWHOST?>img/mail-new.png" alt="mail" title="mail"></div></a>
								<!--<a href="#" id="lnkshowtree" onclick="yaCounter34223620.reachGoal('PushButton_Derevo'); return true;" data-uid="<?=$proj['author_id'];?>" title="<?=$this->localize->get("projview", "btn-tree");?>"><div class="con-tree con-basic"></div></a>-->
								<div class="proj-helpandmoney">
									<p><span id="allhlp" onclick="wndAllhlp(-10); yaCounter34223620.reachGoal('PushButton_VsegoPomogRaz'); return true;"><?=$this->localize->get("user", "stat-tothelps")." :"?></span><span class="help-count proj-size-fix">&nbsp;<?=$this->user_info['helpnum']?></span><span class="proj-raz"> раз</span></p>
									<p><span id="monhlp" onclick="wndAllhlp(0); yaCounter34223620.reachGoal('PushButton_IzNihDengam'); return true;"><?=$this->localize->get("user", "stat-helpmoney").":"?></span> <span class="proj-donhelp-count proj-size-fix">&nbsp;<?=$this->user_info['helpmoney']['totnum'];?></span><span class="proj-raz-fix"> раз</span></p>
									<p><div class="user-loc-loc-txt"><?=$location?></div></p>

									<!--<p><?=$this->localize->get("user", "stat-helpsum");?>: <span class="help-count proj-size-fix">&nbsp;&nbsp;<?=number_format($this->user_info['helpmoney']['totsum'], 2, ",", " ");?> грн.</span>-->
								</div>
							<?php } ?>

						</div>
					</div>
				</div>

				<hr>
			</div>

			<div class="dontree proj-tree-pfix">
				<div class="pview-rel-tit"><?=$this->localize->get("proj", "tree")?></div>
				<div class="dontree cab-tree-size usr-tree">
					<div class="wnd-tree">
						<div class="usr-graph">
							<div id="vis-graph-wrap"></div>
						</div>
					</div>
				</div>
				<hr class="proj-hr-fix">
			</div>

			<div class="pview-sects right-usr-sect">
				<?php
				$slist = $this->needhelpsects;
				if( count($slist)>0 )
				{
					echo '<div class="pview-sects-tit">'.$this->localize->get("projview", "hdr-need").'</div>
			<table>';

					$prev_pid = 0;

					for($i=0; $i<count($slist); $i++)
					{
						if( $prev_pid != $slist[$i]['parent_id'] )
						{
							if( $i>0 )
							{
								echo '</ul>
							</td>
						</tr>';
							}

							$prev_pid = $slist[$i]['parent_id'];

							$psinfo = $this->catmodel->Catalog_SectInfo($slist[$i]['parent_id']);

							echo '<tr>
						<td><img src="'.WWWHOST.FILE_DIR.$psinfo['filename_thumb'].'" alt=""></td>
						<td>
							<p>'.$psinfo['name'].'</p>
							<ul>';
						}

						$SLNK = $this->Page_BuildUrl("cat", $slist[$i]['url']);

						echo '<li>'.$slist[$i]['name'].'</li>';
					}

					echo '</ul>
				</td>
			</tr>';

					echo '</table>';
				}
				?>
			</div>

		</div>


	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?=WWWHOST?>js/fancybox/jquery.fancybox.css" />
<script src="<?=WWWHOST?>js/fancybox/jquery.fancybox.pack.js"></script>

<script>
	$(document).ready(function(){

		var graph = new ObjGraph('vis-graph-wrap', '<?=$proj['author_id']?>');
		$( ".graph-ctrl-item" ).hide( "fast", function() {});
		$( "#graph-select-layout" ).hide( "fast", function() {});

		function wndAllhlp(money_type) {
			popWnd('projview', 'uid=' + <?=$proj['author_id']?>+'&mon='+money_type);
		}

		<?php
		if( $show_addman_btn )
		{
		?>

		$("#getbonusbtn, #getbonusimg").bind("click",function(){

			if( !usr_logged ) {
				popWnd('logdlg', '');
				return false
			}

			if(is_true) {

				$.fancybox.open({type: 'ajax',
					maxWidth    : 1000,
					fitToView	: false,
					width: 903,
					height: 503,
					helpers: {
						title: {
							type: 'float'
						},
					},
					autoSize    : false,
					closeClick	: false,
					openEffect	: 'none',
					closeEffect	: 'none',
					padding : 0,
					href		: req_ajx_host+'ajx/dialog/?dlg=getbonusdlg&projid=<?=$proj['id']?>'
				})
				return false
			}
			else if(!is_true){
				popWnd('getsocial', 'projid=' + <?=$proj['id'];?>+'&uid='+<?=$this->UserId;?>);
			}
		});


		<?php
		}
		//if( $show_send_msg_lnk )
		//{
		?>
		$("#sendmsglnk").bind("click",function(){
			if( !usr_logged )
			{
				popWnd('logdlg', '');
				return false
			}
			popWnd('sendmsgdlg', 'uid=' + <?=$proj['author_id']?>  + '&projid=0');
			return false
		});

		$(".more").click(function() {
			$('.txt-teaser').hide();
			$(".txt-complete").show('slow');
			$('.less').show();
		});



		$(".less").click(function () {
			$('.txt-teaser').show('slow');
			$(".txt-complete").hide('slow');
			$('.more').show();
		})
		<?php
		//}

		/*
        if( $show_more_req )
        {
    ?>
        $("#showmorereq").bind("click", function(){
            $(".pview-rel-more").html('<div class="load-progress"><img src="/img/wait.gif"></div>');
            loadProjReq(<?=$proj['id'];?>,".pview-rel-more");
            $(this).hide();
            return false
        });
    <?php
        }
        */
		?>
	});


</script>