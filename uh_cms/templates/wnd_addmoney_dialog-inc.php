<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define('DEF_VAL_GRN', 5);
define('DEF_VAL_RUB', 12);

$PM = $this->pageModel;

$projid = ( isset($_GET['projid']) ? $_GET['projid'] : 0 );
$is_promise = ( isset($_GET['prom']) ? $_GET['prom'] : 0 );
$sum = (isset($_GET['sum']) ? $_GET['sum'] : 0);
$is_pop = (isset($_GET['pop']) ? $_GET['pop'] : 0);
$req_byconv = (isset($_GET['cat']) ? (($_GET['cat'] == 1) ? 1 : 0) : 0);
$to_p = (isset($_GET['to_p']) ? $_GET['to_p'] : '');

$UserId = (UhCmsApp::getSesInstance()->UserId != 0 ? UhCmsApp::getSesInstance()->UserId : -1 );
$UserGroup = UhCmsApp::getSesInstance()->UserGroup;

$pymntList = $this->catLib->getPaymentProjList($projid);
$pymntList = count($pymntList) > 0 ? 1 : 0;

//$pymntList = $this->catLib->getPaymentProjList($projid);

/*********** Проверка счетчик с видео и вывод PopUp видосика *************/
// Перенести в базу
$videos = [
	'Y04LsL10Sl8',
	'jfSGcn5dPIA',
	'AwhY0QMRV-o',
	'mDU9yWyH7XU',
	'NMTnIY97w2o'
];
//******** DONT FORGET COMMENT THIS SHIT *********/
//$this->ajxModel->set_Counter($UserId, 0, $this->catLib);

$uinf = $this->ajxModel->get_UserInfo($UserId, $this->catLib);

$videoON = 0;
$v_src = '';
//print_r($uinf);

/**********************/

$redirurl = ( isset($_GET['redirto']) ? $_GET['redirto'] : '' );


$projinfo = $this->catLib->Item_Info(addslashes($projid));

/*
$user_ = $this->catLib->Buyer_Info($projinfo['author_id']);
$user_ = [$user_['name'], $user_['fname']];
$video_desc = '<span class="popup-text">'.'<span class="user_h">'.$user_[0].' '.$user_[1].'</span> и команда <span class="way">Way</span><span class="of">Of</span><span class="help">Help</span> благодарит Вас за оказанную помощь. Ваше "Дерево добрых дел" начнет расти сразу после подтверждения помощи.'.'</span>';
$video_share = $user_[0].' '.$user_[1].' и команда WayOfHelp благодарит пользователя '.$uinf['name'].' '.$uinf['fname']. ' за оказаную помощь.';
*/

$currency_data = Array(
	0 => Array("name" => "UAH", "defval" => "100.0"),
	1 => Array("name" => "UAH", "defval" => "100.0"),
	2 => Array("name" => "RUB", "defval" => "200.0"),
	3 => Array("name" => "UAH", "defval" => "100.0"),
	//4 => Array("name" => "EUR", "defval" => "20.0")
	4 => Array("name" => "UAH", "defval" => "100.0")
);

if($UserId == -1) {

	if(!isset($_COOKIE["anonuser"]) || $_COOKIE["anonuser"] == '') {
		$cok_hash = md5(rand(0, PHP_INT_MAX));
		setcookie("anonuser", $cok_hash, time() + 60 * 60 * 24 * 365, '/');
	} else {
		$cok_hash = $_COOKIE["anonuser"];
	}
}

?>

<style>
	.wnd-in {
		background: url(<?=WWWHOST?>/img/madness.png) no-repeat 100% 100%;
		background-color: #5ae26c;
		background-position: -2px 5px;
		background-size: contain;
		height: 290px;
		border-radius: 10px;
	}
	.form-control{
		border: 1px solid #dbffe0;
		background-color: #dbffe0;
		box-shadow:none;
		text-align: center;
		resize: none;
	}
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
		/* display: none; <- Crashes Chrome on hover */
		-webkit-appearance: none;
		margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
	}
	.wnd-cont{
		text-align: center;
	}
	.wnd-wnd {
		background-color: #5ae26c;
		width: 670px;
		border-radius: 10px;
	}
	.btn{
		border-radius: 0 0 10px 10px;
	}
	.wnd-close-img{
		margin-right: -25px;
	}
</style>

<div class="wnd-hdr"><?=!$is_promise ? ($this->localize->get("pophelp", "hdr".( $this->projtype == PROJ_SENDHELP ? "my" : ""))) : 'Обещание помочь'?></div>
<p class="pophelhide_test">
	<?php
	if( $this->projtype == PROJ_SENDHELP )
	{
		echo $PM->get_txtres()->popgetmyhelp['text'];
	}
	else if( $this->projtype == PROJ_THINGS )
	{
		echo $PM->get_txtres()->popgetthing['text'];
	}
	else
	{
		if( $this->helpType == PROJ_TYPE_MONEY )
		{
			echo $PM->get_txtres()->pophelpmoney['text'];
		}
		else if( $this->helpType == PROJ_TYPE_HUMAN )
		{
			//$PM->get_txtres()->pophelphuman['text'];
		}
		else
		{
			echo $PM->get_txtres()->pophelpsome['text'];
		}
	}
	?>
</p>
<div id="doerror" class="wnd-error error-fix"></div>
<?php
	
	if( $this->helpType == PROJ_TYPE_MONEY ) {
		if(!$is_promise) {

			?>

			<div class="row form-horizontal">
				<div class="col-xs-12 col-md-12">
					<div class="form-group">
						<div class="payfix"><input autofocus type="number" class="form-control" autocomplete="off" id="helpsum" name="helpsum"
												   placeholder="0<?='' /*(&#9650;$currency_data[$projinfo['currency_id']]['defval'])*/ ?>"
												   value="<?= $sum <= 0 ? '' : $sum ?>">
							<!--<span id="showlist" onclick="document.getElementById('showlist').style.display = 'none';document.getElementById('hidelist').style.display = 'inline-block'; document.getElementById('list').style.display = 'inline-block';" class="deque-list">&#9660;</span>
							<span id="hidelist" onclick="document.getElementById('showlist').style.display = 'inline-block';document.getElementById('hidelist').style.display = 'none'; document.getElementById('list').style.display = 'none';" style="display: none" class="deque-list">&#9650;</span>
							<div class="col-xs-12 col-md-12">
								<div id="list" style="display: none;" class="sum-to-pay">
									<p onclick="$('#helpsum').val(20); document.getElementById('list').style.display = 'none';document.getElementById('showlist').style.display = 'inline-block';document.getElementById('hidelist').style.display = 'none';">20</p>
									<p onclick="$('#helpsum').val(50); document.getElementById('list').style.display = 'none';document.getElementById('showlist').style.display = 'inline-block';document.getElementById('hidelist').style.display = 'none';">50</p>
									<p onclick="$('#helpsum').val(100); document.getElementById('list').style.display = 'none';document.getElementById('showlist').style.display = 'inline-block';document.getElementById('hidelist').style.display = 'none';">100</p>
									<p onclick="$('#helpsum').val(200); document.getElementById('list').style.display = 'none';document.getElementById('showlist').style.display = 'inline-block';document.getElementById('hidelist').style.display = 'none';">200</p>
									<p onclick="$('#helpsum').val(500); document.getElementById('list').style.display = 'none';document.getElementById('showlist').style.display = 'inline-block';document.getElementById('hidelist').style.display = 'none';">500</p>
									<p onclick="$('#helpsum').val(1000); document.getElementById('list').style.display = 'none';document.getElementById('showlist').style.display = 'inline-block';document.getElementById('hidelist').style.display = 'none';" class="border-none">1000</p>
								</div>
							</div>-->
						</div>
						<div class="addmoney-lock-top">
							<img src="<?=WWWHOST?>img/lock.png"><span class="addmoney-lock"><?=$this->localize->get("addmoney", "confidential")?></span>
						</div>
						<div class="">
							<p class="addmoney-pay"><?=$this->localize->get("addmoney", "payment")?></p>
						</div>
					</div>
				</div>
			</div>

			<div class="payment">

				<ul class="hr">
					<li><div class="col-xs-4 col-md-4 payment-padding">
							<div id="banks" href="javascript:void(0)" onclick="openPay('card');openTabs('banks');$('#helpsum')[0].focus();" class="tabs payment-buttons">
								<p class="payment-text"><?=$this->localize->get("addmoney", "bank-card")?></p>
							</div>
						</div></li>
					<li>
						<div class="col-xs-4 col-md-4 payment-padding">
							<div id="electrons" href="javascript:void(0)" onclick="openPay('purse');openTabs('electrons');$('#helpsum')[0].focus();" class="tabs payment-buttons">
								<p class="payment-text"><?=$this->localize->get("addmoney", "electrons")?></p>
							</div>
						</div>
					</li>
					<li>
						<div class="col-xs-4 col-md-4 payment-padding">
							<div id="telephones" href="javascript:void(0)" onclick="openPay('mobil');openTabs('telephones');$('#helpsum')[0].focus();" class="tabs payment-buttons">
								<p class="payment-text"><?=$this->localize->get("addmoney", "telephones")?></p>
							</div>
						</div>
					</li>
				</ul>

				<div id="card" class="paycard">
					<div class="col-xs-3 col-md-3 stmasfix"><div id="mastercard" href="javascript:void(0)" onclick="select('mastercard');" class="ptabs choise-height"><img  class="master-card" src="<?=WWWHOST?>img/mastercard.png" width="80" height="64"></div></div>
					<div class="col-xs-3 col-md-3 stvisfix"><div id="visa" href="javascript:void(0)" onclick="select('visa');" class="ptabs choise-height"><img class="other-card" src="<?=WWWHOST?>img/visa.png" width="100" height="25"></div></div>
					<div class="col-xs-3 col-md-3 other-width privfix stprvfix"><div id="privat24" href="javascript:void(0)" onclick="select('privat24');" class="ptabs choise-height"><img class="other-card" src="<?=WWWHOST?>img/privat24.png" width="130" height="30"></div></div>
					<div class="col-xs-3 col-md-3 other-width sberb-fix stsbrfix"><div id="sberonline" href="javascript:void(0)" onclick="select('sberonline');" class="ptabs choise-height"><img class="sberb-pos" src="<?=WWWHOST?>img/sberb.png" width="130" height="30"></div></div>
				</div>

				<div id="purse" class="paycard refixp">
					<div class="col-xs-3 col-md-3 stlqpfix">
						<div class="col-xs-12 col-md-12">
							<div id="liqpay" href="javascript:void(0)" onclick="select('liqpay');" class="choise-height ptabs">
								<img class="other-position other-card" src="<?=WWWHOST?>img/liqpay.png" width="130" height="40">
							</div>
						</div>
					</div>
					<div class="col-xs-3 col-md-3 stbtcfix">
						<div class="col-xs-12 col-md-12">
							<div id="bitcoin" href="javascript:void(0)" onclick="select('bitcoin');" class="choise-height ptabs">
								<img class="other-position other-card" src="<?=WWWHOST?>img/bitcoin.png" width="130" height="30">
							</div>
						</div>
					</div>
					<div class="col-xs-3 col-md-3 stlqpfix">
						<div class="col-xs-12 col-md-12">
							<div id="perfectmoney" href="javascript:void(0)" onclick="select('perfectmoney');" class="choise-height ptabs">
								<img class="other-position other-card" style="margin-top: 22px;" src="<?=WWWHOST?>img/Perfect_Money.png" width="130" height="40">
							</div>
						</div>
					</div>
					<div class="col-xs-3 col-md-3 stpyrfix">
						<div class="col-xs-12 col-md-12">
							<div id="payeer" href="javascript:void(0)" onclick="select('payeer');" class="choise-height ptabs">
								<img class="other-position other-card" style="margin-left: -9px;" src="<?=WWWHOST?>img/proj-payeer.png" width="150" height="40">
							</div>
						</div>
					</div>
				</div>

			<div id="mobil" class="paycard refix stmobfix">
				<!--<div class="col-xs-3 col-md-3"><a href="#"><img id="life" href="javascript:void(0)" onclick="select('life');" class="other-card ptabs" src="<?=WWWHOST?>img/lifecell.png" width="116" height="45"></a></div>-->
				<div class="col-xs-4 col-md-4">
					<div id="beeline" href="javascript:void(0)" onclick="select('beeline');" class="choise-height ptabs"><img class="other-card" src="<?=WWWHOST?>img/beline.png" width="121" height="37"></div>
				</div>
				<div class="col-xs-4 col-md-4">
					<div id="mts" href="javascript:void(0)" onclick="select('mts');" class="choise-height ptabs"><img class="other-card" src="<?=WWWHOST?>img/mts.png" width="90" height="35"></div>
				</div>
				<div class="col-xs-4 col-md-4 stmgffix">
					<div id="megafon" href="javascript:void(0)" onclick="select('megafon');" class="choise-height ptabs"><img class="other-card" src="<?=WWWHOST?>img/megafone.png" width="130" height="34"></div>
				</div>
			</div>

			<script>

				openPay("card");
				openTabs("banks");
				var pay_type = '';

				function openPay(PayName) {
					var i;
					var x = document.getElementsByClassName("paycard");

					for (i = 0; i < x.length; ++i) {
						x[i].style.display = "none";
					}

					document.getElementById(PayName).style.display = "block";
				}

				function openTabs(btnName) {
					var j;
					var y = document.getElementsByClassName("tabs");

					for (j = 0; j < y.length; ++j) {
						y[j].style.background = "#74f284";
					}

					document.getElementById(btnName).style.background = "white";
				}

				function select(payTab) {
					pay_type = payTab;

					$("#payment").attr('action', 'https://sci.interkassa.com/#/paysystem/'+payTab);

					//alert($("#payment").attr('action'));

					var k;
					var z = document.getElementsByClassName("ptabs");

					for (k = 0; k < z.length; ++k) {
						z[k].style.border = "2px solid white";//border: 2px solid #e6ffe0;
						$( '#grks' ).remove();
						//z[k].style.background = "white";//border: 2px solid #e6ffe0;
					}
					$('#'+payTab).css('border', '2px solid #71b52c');
					$('#'+payTab).css('border-radius', '10px');
					$( '<img id="grks" class="green-stiks" src="<?=WWWHOST?>img/green-stiks.png" width="25" height="25">' ).insertAfter( '#'+payTab );

					//document.getElementById(payTab).style.border = "2px solid #71b52c";
					//document.getElementById(payTab).style.borderradius = "10px";
					//document.getElementById(payTab).style.background = "#e6ffe0";
					$('#helpsum')[0].focus();
				}

			</script>

			</div>

				<div class="row form-horizontal">
					<div class="col-xs-12 col-md-12 ">
						<div class="">
							<div class="mon-btn-block">
									<form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
										<input type="hidden" name="ik_co_id" value="561555503d1eafe0518b4567"/>
										<input type="hidden" name="ik_pm_no" value="ID_<?=$projid?>_<?=$UserId.($req_byconv == 1 ? '_1' : '')?>"/>
										<input type="hidden" name="ik_am" id="ik_am_inp"
											   value="<?= ($currency_data[$projinfo['currency_id']]['defval']); ?>"/><!--  100.0  -->
										<input type="hidden" name="ik_cur" id="ik_cur"
											   value="<?= ($currency_data[$projinfo['currency_id']]['name']); ?>"/><!-- UAH -->
										<input type="hidden" name="ik_desc" value="<?=$this->localize->get("myprojects", "charitable")?>"/>
										<input type="hidden" name="ik_exp" value="<?= date("Y-m-d", time() + 24 * 3600); ?>"/>
										<input type="hidden" name="ik_ia_u" value="http://wayofhelp.com/payscallback/ikinteract/?papid=<?=(isset($_COOKIE['PAPVisitorId']) ? $_COOKIE['PAPVisitorId'] : '').($UserId == -1 ? '&anonuser='.$cok_hash : '').(isset($_COOKIE['kadid']) ? '&kadid='.$_COOKIE['kadid'] : '')?>"/>
										<input type="hidden" name="ik_ia_m" value="post"/>
										<input type="hidden" name="ik_suc_u" value="<?= $this->Page_BuildUrl("proj", "view/" . $projid); ?>"/>
										<input type="hidden" name="ik_suc_m" value="get"/>
										<input type="hidden" name="ik_fal_u" value="<?= $this->Page_BuildUrl("proj", "view/" . $projid); ?>"/>
										<input type="hidden" name="ik_fal_m" value="get"/>
										<!--<input type="hidden" name="ik_int" value="web" />
										<input type="hidden" name="ik_am_t" value="payway" />-->
										<!--<input type="submit" value="Pay">-->
									</form>
							</div>
							<div class="row form-horizontal">
								<div class="col-xs-12 col-md-12 ">
									<div class="form-group">
											<div id="addmonloader" style="background: #fff;width: 40px;">
												<button type="button" id="helpikbtn" onclick="$('#helpsum')[0].focus();" class="btn addmoney-btncolor">
													<?= $this->localize->get("pophelp", "btn-send") ?>
												</button>
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

		<?php

	} else { ?>
		<form id="promform" action="<?=$this->Page_BuildUrl('proj','promise')?>" method="post">
			<input type="hidden" name="proj" value="<?=$projid?>">
			<input type="hidden" name="req" value="<?=$req_byconv?>">
			<div class="row form-horizontal">
				<div class="col-xs-12 col-md-12">
					<div class="form-group">
						<label for="upass"
							   class="col-sm-3 control-label"><?= $this->localize->get("pophelp", "lbl-sum"); ?></label>
						<div class="col-sm-7"><input type="number" class="form-control" autocomplete="off" id="helpsum" name="helpsum"
													 placeholder="0<?='' /*($currency_data[$projinfo['currency_id']]['defval'])*/ ?>"
													 value="<?= $sum <= 0 ? '' : $sum ?>">
						</div>
					</div>
				</div>
			</div>
			<div class="row form-horizontal">
				<div class="col-xs-12 col-md-12 ">
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<div id="addmonloader" style="background: #fff;width: 40px;">
								<button type="button" id="prombtn" class="btn addmoney-btncolor">
									<span class="glyphicon glyphicon-thumbs-up"></span> <?= $this->localize->get("pophelp", "btn-send"); ?>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php }
}
else
{
	$hreqtype = "dohelp";
	switch($this->projtype)
	{
		case PROJ_THINGS:	$hreqtype = "getthing";		break;
		case PROJ_SENDHELP:	$hreqtype = "getmyhelp";	break;
	}
	?>
	<form id="frmaddfund" action="<?=( $this->Page_BuildUrl("proj", "addhelpmoney") );?>" method="POST">
		<input type="hidden" id="hmonprojid" name="selprojid" value="<?=$projid;?>">
		<input type="hidden" id="hreqtype" name="hreqtype" value="<?=$hreqtype;?>">
		<?php
		if( $redirurl != '' )
		{
			echo '<input type="hidden" name="redirto" value="'.$redirurl.'">';
		}

		if( $this->helpType == PROJ_TYPE_MONEY )
		{
			?>
			<div class="row form-horizontal">
				<div class="col-xs-12 col-md-12">
					<div class="form-group">
						<label for="upass" class="col-sm-3 control-label"><?=$this->localize->get("pophelp", "lbl-sum");?></label>
						<div class="col-sm-9"><input type="text" class="form-control" autocomplete="off" id="helpsum" name="helpsum" placeholder="100.00" value="100.00"></div>
					</div>
				</div>
			</div>
			<?php
		}
		else if( ($this->helpType == PROJ_TYPE_HUMAN) && (($UserGroup == USR_TYPE_COMP) || ($UserGroup == USR_TYPE_ORG)) )
		{

			?>

			<div class="row form-horizontal">
				<div class="col-xs-12 col-md-12">
					<div class="form-group">
						<label for="upass" class="col-sm-3 control-label"><?=$this->localize->get("pophelp", "lbl-amount");?></label>
						<div class="col-sm-9"><input type="text" class="form-control" autocomplete="off" id="helpsum" name="helpsum" placeholder="1" value="1"></div>
					</div>
				</div>
			</div>
			<?php
		}
		else
		{
			echo '<input type="hidden" id="helpsum" autocomplete="off" name="helpsum" value="1">';
		}
		?>
		<div class="row form-horizontal">
			<div class="col-xs-12 col-md-12">
				<div class="form-group">
					<label for="upass" class="control-label"><?=$this->localize->get("pophelp", "lbl-msg");?></label>
					<div class=""><textarea class="help-textarea" id="helptxt" name="helptxt" rows="4" placeholder="<?=$this->localize->get("pophelp", "ph-msg");?>"></textarea></div>
				</div>
			</div>
		</div>
		<script>
				$("#helptxt").focus();
		</script>
		<div class="row form-horizontal">
			<div class="col-xs-12 col-md-12 ">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3 helpfix"><button id="helpmoneybtn" class="btn addmoney-btncolor" onclick="if('<?=$is_pop?>' == 1) {
							yaCounter34223620.reachGoal('PushButtonPodtverditPomoshDelomProjectFromPopupGoodMan'); return true;
							} else {
							yaCounter34223620.reachGoal('ProjectPushButton_Pomoch_delom_PushButton_Podtverdit'); return true;
							}"><?=$this->localize->get("pophelp", "btn-send");?></button></div>
				</div>
			</div>
		</div>
	</form>
	<?php
}
?>

<script src="<?=WWWHOST?>js/jquery.redirect.js"></script>
<script>

	fbq('track', 'InitiateCheckout');
	<?php
	if( $this->helpType == PROJ_TYPE_MONEY )
	{
	?>
	$("#helpikbtn").on("click", function() {
		$("#doerror").html("");
		$("#doerror").css("display", "none");

		var newval = $("#helpsum").val();
		var re = /^[0-9]+\.?[0-9]*$/;
		//var monType = $("#sel").val();
		var currType = "<?=$projinfo['currency_id']?>";
//alert('sdf');
		if (re.test(newval)) {

			if (currType == 1 && newval < <?=DEF_VAL_GRN?>) {
				$(".pophelhide_test").css("display", "none");
				$("#doerror").css("display", "block");
				$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt") . DEF_VAL_GRN . ' ' . ' грн.'?>");

			} else if (currType == 2 && newval < <?=DEF_VAL_RUB?>) {
				$(".pophelhide_test").css("display", "none");
				$("#doerror").css("display", "block");
				$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt") . DEF_VAL_RUB . ' ' . ' руб.'?>");

			} else if (!pay_type) {
				$("#doerror").css("display", "block");
				$("#doerror").html("<?=$this->localize->get("pophelp", "payerr")?>");

			} else {

				/*if (!usr_logged) {
					popWnd('newreg', '&redirto=<?=$redirurl?>' + '&type=reg');
					yaCounter34223620.reachGoal('ProjectPushButton_Pomoch_dengami_Forma_vvoda');

					$.ajax({
						type: "POST",
						url: req_ajx_host + "ajx/setinpsum/",
						data: "pid=<?=$projid?>" + "&sum=" + newval,
						dataType: 'json',
						success: function (data) {}
					});

					return false;
				} else {*/

					// THIS IS VK BITE CODE
					(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=dEmRcscL24QtH7cMe/7C7U7dAb/hpV1wJe0bitMHBTGuNHJ5QpMEYqHzooRir7WVYnqY3*GpeOnpco0ldjgfQ36NXmfvtkVNsy/kEvzUdapBj0PzjSzHi7E*/hC6vUQgOsU*XzQhga*A3LpDzLnT*NBq4Kdzm7tls27QB9FgNo0-';
					// THIS IS FB BITE CODE
					fbq('track', 'Purchase', {value: '0.00', currency: 'USD'});

					if ('<?=$is_pop?>' == 1) {
						yaCounter34223620.reachGoal('PushButtonPodtverditSummuProjectFromPopupGoodMan');
					} else {
						yaCounter34223620.reachGoal('ProjectPushButton_Pomoch_dengami_PushButton_Podtverdit');
					}
						yaCounter34223620.reachGoal('ProjectPushButton_Pomoch_dengami_Forma_vvoda');
					//
				ga('ec:addProduct', {
					'id': <?=$projid?>,
					'price': newval,
					'quantity': 1
				});

				ga('ec:setAction','checkout', {
					'step': 2,            // A value of 1 indicates this action is first checkout step.
					'option': pay_type      // Used to specify additional info about a checkout stage, e.g. payment method.
				});
				ga('send', 'pageview');
				ga('send', 'event', 'woh_popupoplati', 'pushbutton_podtverdit');

					$("#ik_am_inp").val(newval);
					//$("#promform").submit();

					$('#helpikbtn').html('');
					$("#addmonloader").html('<div><img src="<?=WWWHOST?>img/ajax-loader.gif" width="25" height="10"></div>');

					$('#payment').submit();
				//}
			}
		} else {
			$(".pophelhide_test").css("display", "none");
			$("#doerror").css("display", "block");
			$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt-def")?>");
		}
	});

	$("#helpsum").bind("keyup", function(){
		var newval = $(this).val();
		var re = /^[0-9]+\.?[0-9]*$/;

		if( re.test(newval) ) {
			$("#ik_am_inp").val(newval);
		}
	});

	$('#prombtn').bind('click', function () {

			$("#doerror").html("");
			$("#doerror").css("display", "none");

			var newval = $("#helpsum").val();
			var re = /^[0-9]+\.?[0-9]*$/;
			//var monType = $("#sel").val();
			var currType = "<?=$projinfo['currency_id']?>";

			if (re.test(newval)) {

				if (currType == 1 && newval < <?=DEF_VAL_GRN?>) {
					$("#doerror").css("display", "block");
					$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt") . DEF_VAL_GRN . ' ' . ' грн.'?>");

				} else if (currType == 2 && newval < <?=DEF_VAL_RUB?>) {
					$("#doerror").css("display", "block");
					$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt") . DEF_VAL_RUB . ' ' . ' руб.'?>");

				} else {

					/*if (!usr_logged) {
						popWnd('newreg', '&redirto=<?=$redirurl?>' + '&type=reg');
						yaCounter34223620.reachGoal('ProjectPushButton_Pomoch_dengami_Forma_vvoda');

						$.ajax({
							type: "POST",
							url: req_ajx_host + "ajx/setinpsum/",
							data: "pid=<?=$projid?>" + "&sum=" + newval,
							dataType: 'json',
							success: function (data) {}
						});

						return false;
					} else {*/

						// THIS IS VK BITE CODE
						(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=dEmRcscL24QtH7cMe/7C7U7dAb/hpV1wJe0bitMHBTGuNHJ5QpMEYqHzooRir7WVYnqY3*GpeOnpco0ldjgfQ36NXmfvtkVNsy/kEvzUdapBj0PzjSzHi7E*/hC6vUQgOsU*XzQhga*A3LpDzLnT*NBq4Kdzm7tls27QB9FgNo0-';
						// THIS IS FB BITE CODE
						fbq('track', 'Purchase', {value: '0.00', currency: 'USD'});

						if ('<?=$is_pop?>' == 1) {
							yaCounter34223620.reachGoal('PushButtonPodtverditSummuProjectFromPopupGoodMan');
						} else {
							yaCounter34223620.reachGoal('ProjectPushButton_Pomoch_dengami_PushButton_Podtverdit');
						}
						//

						$("#ik_am_inp").val(newval);
						//$("#promform").submit();

						$('#prombtn').html('');
						$("#addmonloader").html('<div><img src="<?=WWWHOST?>img/ajax-loader.gif" width="25" height="10"></div>');

						$.ajax({
							type: "POST",
							url: req_ajx_host + "proj/prprom/",
							data: "proj=<?=$projid?>" + "&helpsum=" + newval + "&req=" + "<?=$req_byconv?>" + "&to_p=" + "<?=$to_p?>",
							dataType: 'json',
							success: function (data) {
								if (data['viewMode'] == 'def' || data['viewMode'] == 'toomany') {
									//console.log(data);
									$.redirect(req_ajx_host + 'proj/userpromise/', data, 'post');
								}
							}
						});
					//}
				}
			} else {
					$("#doerror").css("display", "block");
					$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt-def")?>");
				}

	});

	<?php
	}
	else
	{
	?>
	$("#helpmoneybtn").on("click", function() {
		// THIS IS BITE CODE
		(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=aFtYmPQzMujwlhvQDdiBDYtbvMFNe5Ty4*w7UrvXaSHfvlySdqcSAr4ZePUMJ4NAOgarsTbfBufUgOd0Yusa8aW0eDDVC3w8k3qWQq0r364H07zYu*gMRUN4OGNtnciSF5dOI2b2wwUPrxQnzNt4J5Oa*XyK2lcHDsGkHyuUedQ-';
		fbq('track', 'Purchase', {value: '0.00', currency:'USD'});

		redirectTo();
	});

	function redirectTo() {
		$("#frmaddfund").submit();
	}
	<?php
	}
	?>

	$('#addhelp_promise').bind('click', function () {
		popWnd('addmoneydlg', 'projid=' + <?=$projid?>+'&prom=1&sum='+$("#helpsum").val());
		return false
	});

	$('#getpymnt').bind('click', function () {
		$("#doerror").html("");

		var newval = $("#helpsum").val();
		var re = /^[0-9]+\.?[0-9]*$/;
		//var monType = $("#sel").val();
		var currType = "<?=$projinfo['currency_id']?>";

		if (re.test(newval)) {
			if(currType == 1 && newval < <?=DEF_VAL_GRN?>) {
				$("#doerror").css("display","block");
				$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt").DEF_VAL_GRN.' '.' грн.'?>");
			} else if(currType == 2 && newval < <?=DEF_VAL_RUB?>) {
				$("#doerror").css("display","block");
				$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt").DEF_VAL_RUB.' '.' руб.'?>");
			} else {
				// THIS IS VK BITE CODE
				(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=dEmRcscL24QtH7cMe/7C7U7dAb/hpV1wJe0bitMHBTGuNHJ5QpMEYqHzooRir7WVYnqY3*GpeOnpco0ldjgfQ36NXmfvtkVNsy/kEvzUdapBj0PzjSzHi7E*/hC6vUQgOsU*XzQhga*A3LpDzLnT*NBq4Kdzm7tls27QB9FgNo0-';
				// THIS IS FB BITE CODE
				fbq('track', 'Purchase', {value: '0.00', currency: 'USD'});

				popWnd('projpayment', 'projid=' + <?=$projid?> + '&sum=' + newval);

				//popWnd('projpayment', 'projid=' + <?=$projid?>+ '&sum=' + newval );
				//popWnd('addmoneydlg', 'projid=' + <?=$projid?>+'&prom=1');
				return false;

			}
		}
		else {
			$("#doerror").html("<?=$this->localize->get("pophelp", "err-payfmt");?>");
		}

	});

</script>