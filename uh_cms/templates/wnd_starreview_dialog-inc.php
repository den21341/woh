<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$projid = ( isset($_GET['projid']) ? $_GET['projid'] : 0 );
	$reqid = ( isset($_GET['reqid']) ? $_GET['reqid'] : 0 );

?>
<div class="wnd-hdr"><?=$this->localize->get("poprev", "hdr");?></div>
<p><?=( $PM->get_txtres()->popstarrev['text'] );?></p>
<div id="doerror" class="wnd-error"></div>
<form id="frmstarrev" action="<?=( $this->Page_BuildUrl("cabinet", "postrev/".$projid."/".$reqid) );?>" method="POST">
	<input type="hidden" id="hprojid" name="selprojid" value="<?=$projid;?>">
	<input type="hidden" id="hreqid" name="selreqid" value="<?=$reqid;?>">
	<div class="row form-horizontal">		
		<div class="col-xs-2 col-md-2">
			<div class="form-group">
				<label class="control-label"><?=$this->localize->get("poprev", "lbl-rate");?></label>				
			</div>
		</div>
<?php
	for( $i=1; $i<=10; $i++ )
	{
		echo '<div class="col-xs-1 col-md-1 starratebg'.$i.'">
			<div class="radio"><label><input type="radio" name="starrate" id="radio'.$i.'" value="'.$i.'" '.( $i == 10 ? ' checked' : '').'> '.$i.'</label></div>
		</div>';
	}	 
?>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="form-group">
				<label for="upass" class="control-label"><?=$this->localize->get("poprev", "lbl-msg");?></label>
				<textarea class="form-control" id="startxt" name="startxt" rows="4" placeholder="<?=$this->localize->get("poprev", "ph-msg");?>"></textarea>
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-xs-12 col-md-12 text-center">
			<button id="starrevbtn" class="btn btn-info"><span class="glyphicon glyphicon-thumbs-up"></span> <?=$this->localize->get("poprev", "btn-send");?></button>			
		</div>
	</div>		
</form>
<script>

	$("#starrevbtn").on("click", function() {
		if( checkStarForm('<?=$this->localize->get("basicjs", "checkstar-err")?>') )
			$("#frmstarrev").submit();

		return false
	});

</script>