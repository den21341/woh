<?php
$localize['ru'] = [
    'Нужны вещи',
    'Отдам вещи',
    'Осуществить мечту',
    'Спорт',
    'Растения и животные',
    'Трудоустройство',
    'Медицинсткая помощь',
    'Интеллектуальная помошь',
    'Исскуство и культура',
    'Стартапы',
    'Путешествия и ночлег',
    'Другое'
];

$links = $this->sects;
$thnm = $links[0]['url'];
$dream = $links[1]['url'];
$goods = $links[2]['url'];
$links[0]['url'] = $goods;
$links[1]['url'] = $thnm;
$links[2]['url'] = $dream;

$BCHTML = $this->renderBreadcrumbs();

?>

<div class="row">
    <div class="top-block top-block-pr col-md-12 col-xs-12 col-lg-12">
        <div class="container">
            <ol class="breadcrumb">
                <?=$BCHTML;?>
            </ol>
        </div>
        <div class="top-block-txt top-block-pr-txt">
            <h1><?=$this->localize->get("info-rules", "rules")?></h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="container">
        <div class="prul-stack">
            <?php for($i=0; $i<15; $i++) { ?>
                <div class="info-prul-block">
                        <div class="prul-block-numb <?=(strlen($this->pageInfo[$i]['page_text']) > 460 ? 'prul-bl-n-big' : (strlen($this->pageInfo[$i]['page_text']) < 400 ? 'prul-bl-n-min' : 'prubl-bl-n-mid'))?><?=$this->pageInfo[$i]['page_block'] == 12 ? ' prubl-bl-tw' : ''?>"><p><?=$this->pageInfo[$i]['page_block']?></p></div>
                        <div class="prul-block-txt <?=(strlen($this->pageInfo[$i]['page_text']) > 460 ? 'prul-bl-big' : (strlen($this->pageInfo[$i]['page_text']) < 400 ? 'prul-bl-min' : 'prubl-bl-mid'))?>"><p><?=$this->pageInfo[$i]['page_text']?></p></div>
                </div>
            <?php } ?>
            <div class="prul-align">
                <div class="prul-howto">
                    <div class="howto-h">
                        <div class="howto-h-img"></div>
                        <div class="howto-h-txt"><?=$this->pageInfo[15]['page_text']?></div>
                    </div>
                    <div class="howto-txt"><?=$this->pageInfo[16]['page_text']?></div>
                </div>
                <hr>
                <div class="prul-howto">
                    <div class="howto-h">
                        <div class="howto-h-stop-img"></div>
                        <div class="howto-h-txt"><?=$this->pageInfo[17]['page_text']?></div>
                    </div>
                    <div class="howto-txt prul-dng-txt"><?=$this->pageInfo[18]['page_text']?></div>
                </div>
                <a name="cathelp"><hr style="visibility: hidden"></a>
               <hr>
                <div class="prul-howto">
                    <div class="howto-h">
                        <div class="howto-h-cat-img"></div>
                        <div class="howto-h-txt"><?=$this->pageInfo[19]['page_text']?></div>
                    </div>
                    <div class="howto-txt howto-cat-txt"><?=$this->pageInfo[20]['page_text']?></div>
                </div>
                <div class="prul-howto">
                    <div class="prul-cat-item">
                        <div class="col-md-6 col-lg-6 col-xs-6">
                            <?php for($i=1; $i<=count($localize['ru']); ++$i) {
                                if($i==7) {
                                    echo '</div><div class="col-md-6 col-lg-6 col-xs-6"><div>';
                                } ?>
                                <div class="prul-th">
                                    <div class="prul-th-img col-lg-5 col-md-5 col-xs-5">
                                        <img src="<?=WWWHOST?>files/sects/th_<?=$i?>.png" alt="<?=$localize['ru'][$i]?>">
                                    </div>
                                    <a class="prul-th-txt col-lg-7 col-md-5 col-xs-5" href="<?=($this->Page_BuildUrl('cat',$links[$i-1]['url']))?>" target="_blank"><?=$localize['ru'][$i-1]?></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="howto-txt prul-cat-item-txt"><?=$this->pageInfo[21]['page_text']?></div>
                <a name="copl"><hr style="visibility: hidden"></a>
                <hr>
                <div class="prul-howto">
                    <div class="howto-h">
                        <div class="howto-h-copl-img"></div>
                        <div class="howto-h-txt"><?=$this->pageInfo[22]['page_text']?></div>
                    </div>
                    <div class="howto-txt howto-copl-txt"><?=$this->pageInfo[23]['page_text']?></div>
                    <div class="howto-copl-sub-h"><?=$this->pageInfo[24]['page_text']?></div>
                    <div class="howto-txt howto-copl-txt"><?=$this->pageInfo[25]['page_text']?></div>
                </div>
                <hr>
                <div class="prul-howto prul-bon-item">
                    <div class="col-md-3 col-lg-3 col-xs-3">
                        <a href="<?=$this->Page_BuildUrl('info','sanction')?>"><div class="bon-item-block">
                            <img src="<?=WWWHOST?>img/info-prul-sanct.png" alt="<?=$this->localize->get("info-rules", "sanction")?>">
                            <div><?=$this->localize->get("info-rules", "punitive-sanction")?></div>
                        </div></a>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3">
                        <a href="<?=$this->Page_BuildUrl('info','specrules')?>"><div class="bon-item-block">
                            <img src="<?=WWWHOST?>img/info-prul-guard.png" alt="<?=$this->localize->get("info-rules", "protection")?>">
                            <div><?=$this->localize->get("info-rules", "safity-rule")?></div>
                        </div></a>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3">
                        <a href="<?=$this->Page_BuildUrl("info","ratrules").'#ratproj';?>"><div class="bon-item-block">
                            <img src="<?=WWWHOST?>img/info-prul-rat.png" alt="<?=$this->localize->get("info-rules", "project-rating")?>">
                            <div class="bon-item-align"><?=$this->localize->get("info-rules", "rating-up")?></div>
                        </div></a>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3">
                        <a href="<?=$this->Page_BuildUrl("info","ratrules").'#ratuser';?>"><div class="bon-item-block">
                            <img src="<?=WWWHOST?>img/info-prul-urat.png" alt="<?=$this->localize->get("info-rules", "user-rating")?>">
                            <div class="bon-item-align"><?=$this->localize->get("info-rules", "user-ratup")?></div>
                        </div></a>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>