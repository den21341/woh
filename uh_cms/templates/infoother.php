<?php $BCHTML = $this->renderBreadcrumbs();	 ?>
<div class="row">
    <div class="top-block top-block-pr col-md-12 col-xs-12 col-lg-12">
        <div class="top-block-txt top-block-pr-txt">
            <div class="container">
                <ol class="breadcrumb">
                    <?=$BCHTML;?>
                </ol>
            </div>
            <h1><?=$this->pageInfo[0]['page_text']?></h1>
        </div>
    </div>
</div>
<?php if($this->viewMode == 'project') { ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="info-rule-block info-pr-block container-fluid">
                <div class="container info-container">
                    <img src="<?=WWWHOST.'img/info-pr-img.png'?>" class="info-pr-img col-lg-2" alt="">
                    <div class="info-proj-txt col-lg-9"><?=$this->pageInfo[1]['page_text']?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container info-container">
        <div class="row">
            <div class="info-pr-main">
                <?=$this->pageInfo[2]['page_text']?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="info-pr-spec">
            <div class="container info-container">
                <?=$this->pageInfo[3]['page_text']?>
            </div>
        </div>
        <div class="info-pr-spec-sub">
            <div class="container info-container sub-info-cont">
                <div class="col-md-12 col-xs-12 col-lg-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sub-tree">
                        <div class="info-sub-img-tree col-lg-4"></div>
                        <div class="info-sub-txt col-lg-8"><?=$this->pageInfo[4]['page_text']?></div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6">
                        <div class="info-sub-img-box col-lg-3"></div>
                        <div class="info-sub-txt col-lg-9"><?=$this->pageInfo[5]['page_text']?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? } else if($this->viewMode == 'concepts') { ?>
    <div class="row">
        <div class="container">
            <div class="conc-pad">
                <div class="col-md-6 col-lg-6 col-xs-6">
                    <div class="info-conc-item">
                        <div class="col-md-5 col-lg-5 col-xs-5">
                            <img src="<?=WWWHOST?>img/info-conc-ar.png" alt="<?=$this->localize->get("infoother", "social")?>">
                        </div>
                        <div class="col-md-5 col-lg-6 col-xs-5 conc-item-txt"><?=$this->pageInfo[1]['page_text']?></div>
                    </div>
                    <div class="info-conc-item">
                        <div class="col-md-5 col-lg-5 col-xs-5">
                            <img src="<?=WWWHOST?>img/info-cons-people.png" alt="<?=$this->localize->get("infoother", "users")?>">
                        </div>
                        <div class="col-md-5 col-lg-6 col-xs-5 conc-item-txt"><?=$this->pageInfo[2]['page_text']?></div>
                    </div>
                    <div class="info-conc-item">
                        <div class="col-md-5 col-lg-5 col-xs-5">
                            <img src="<?=WWWHOST?>img/info-cons-hpp.png" alt="<?=$this->localize->get("infoother", "charity")?>">
                        </div>
                        <div class="col-md-5 col-lg-6 col-xs-5 conc-item-txt"><?=$this->pageInfo[3]['page_text']?></div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6">
                    <div class="info-conc-item info-conc-item-r">
                        <div class="col-md-2 col-lg-2 col-xs-2">
                            <img src="<?=WWWHOST?>img/info-conc-pc.png" alt="<?=$this->localize->get("infoother", "project")?>">
                        </div>
                        <div class="col-md-7 col-lg-7 col-xs-7 conc-item-txt"><?=$this->pageInfo[4]['page_text']?></div>
                    </div>
                    <div class="info-conc-item info-conc-item-r">
                        <div class="col-md-2 col-lg-2 col-xs-2">
                            <img class="conc-pic-align" src="<?=WWWHOST?>img/info-conc-guard.png" alt="<?=$this->localize->get("infoother", "help")?>">
                        </div>
                        <div class="col-md-7 col-lg-7 col-xs-7 conc-item-txt"><?=$this->pageInfo[5]['page_text']?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else if($this->viewMode == 'users') { ?>
    <?for($i=1; $i<count($this->pageInfo); ++$i) { ?>
        <div class="row info-users-row">
            <div class="container">
                <div class="col-users-align">
                    <div class="col-lg-2 col-md-2 col-xs-2">
                        <img src="<?=WWWHOST?>img/info-users_p<?=$i?>.png" alt="<?=$this->localize->get("infoother", "user")?> <?=$i?>">
                    </div>
                    <div class="col-lg-10 col-md-10 col-xs-10">
                        <span><?=$this->pageInfo[$i]['page_text']?></span>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row info-users-foot"></div>
<?php } else if($this->viewMode == 'stat') { ?>
    <div class="row">
        <div class="container stat-cont">
            <div class="stat-about"><?=$this->pageInfo[1]['page_text']?></div>
            <div class="stat-descr"><?=$this->pageInfo[2]['page_text']?></div>
            <div class="stat-main-text"><?=$this->pageInfo[3]['page_text']?></div>
            <hr>
            <div class="stat-hight-stat"><?=$this->pageInfo[4]['page_text']?></div>
        </div>
    </div>
<?php } else if($this->viewMode == 'power') { ?>
<div class="row">
    <div class="container power-cont">
        <div class="stat-about"><?=$this->pageInfo[1]['page_text']?></div>
        <div class="stat-descr"><?=$this->pageInfo[2]['page_text']?></div>
        <div class="stat-main-text"><?=$this->pageInfo[3]['page_text']?></div>
        <hr>
        <div class="pwer-sub-txt"><?=$this->pageInfo[4]['page_text']?></div>
    </div>
</div>
<?php } else if($this->viewMode == 'fees') { ?>
    <div class="row">
        <div class="container des-cont-all">
            <div class="fees-about"><?=$this->pageInfo[1]['page_text']?></div>
        </div>
    </div>
<?php } else if($this->viewMode == 'conf' || $this->viewMode = 'reports' || $this->viewMode == 'ratrules' || $this->viewMode == 'sanction'
                || $this->viewMode == 'active' || $this->viewMode == 'treeup') { ?>
    <div class="row">
        <div class="container des-cont-all">
            <div class="fees-about conf-about"><?=$this->pageInfo[1]['page_text']?></div>
        </div>
    </div>
<?php } else if($this->viewMode == 'donors') { ?>
    <div class="row">
        <div class="container des-cont-all don-cont">
            <div class="col-lg-6 col-md-6 col-xs-6">
                <div class="donors-block col-lg-12 col-md-12 col-xs-12">
                    <div class="donors-block-num col-lg-1 col-md-1 col-xs-1">1</div>
                    <div class="donors-block-txt col-lg-10 col-md-10 col-xs-10"><?=$this->pageInfo[1]['page_text']?></div>
                </div>
                <div class="donors-block donors-block-2 col-lg-12 col-md-12 col-xs-12">
                    <div class="donors-block-num col-lg-1 col-md-1 col-xs-1">2</div>
                    <div class="donors-block-txt col-lg-10 col-md-10 col-xs-10"><?=$this->pageInfo[2]['page_text']?></div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-6">
                <div class="donors-block donors-block-r col-lg-12 col-md-12 col-xs-12">
                    <div class="donors-block-num col-lg-1 col-md-1 col-xs-1">3</div>
                    <div class="donors-block-txt col-lg-10 col-md-10 col-xs-10"><?=$this->pageInfo[3]['page_text']?></div>
                </div>
                <div class="donors-block donors-block-r col-lg-12 col-md-12 col-xs-12">
                    <div class="donors-block-num col-lg-1 col-md-1 col-xs-1">4</div>
                    <div class="donors-block-txt col-lg-10 col-md-10 col-xs-10"><?=$this->pageInfo[4]['page_text']?></div>
                </div>
                <div class="donors-block donors-block-r col-lg-12 col-md-12 col-xs-12">
                    <div class="donors-block-num col-lg-1 col-md-1 col-xs-1">5</div>
                    <div class="donors-block-txt col-lg-10 col-md-10 col-xs-10"><?=$this->pageInfo[5]['page_text']?></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="foot-pre-h"></div>
        </div>
    </div>
<?php } else if($this->viewMode == 'makehlp') { ?>
    <div class="row">
        <div class="container des-cont-all">
            <div class="fees-about conf-about"><?=$this->pageInfo[1]['page_text']?></div>
        </div>
    </div>
<?php } ?>
