<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();
	
	$uinfo = $this->userinfo;
	//echo '<pre>';
	//print_r($uinfo);
	$PAGE_H1 = $page->title;
	//$dbcat = new Catalog();
	//print_r($dbcat);
	$countWork = $this->countWork;
	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);

	$loc = (isset($uinfo['loc_info']['name']) == "" ? $this->localize->get("cabinet", "noselected") : $uinfo['loc_info']['name']);
	$cntry = (isset($uinfo['loc_info']['countryname']) == "" ? "(".$this->localize->get("cabinet", "noselected").")" : '('.$uinfo['loc_info']['countryname'].')');
	$location = ($loc." ".$cntry);
	
	$share_title = $this->localize->get("tree","share-tit");
	$share_descr = $this->localize->get("tree","share-descr");

	$share_title = str_replace('_name_', $uinfo['name'], $share_title);
	$share_title = str_replace('_fname_', $uinfo['fname'], $share_title);

	$share_descr = str_replace('_d1_', $this->helpcount, $share_descr);
	$share_descr = str_replace('_d2_', ($this->helpcount < 11) ? rand($this->helpcount, $this->helpcount + 10) : rand($this->helpcount - 10, $this->helpcount + 10), $share_descr);

//print_r($this->statusBox->getStatus($this->box['status']));
	//$num_new_msg = $PM->msg_NumNew($this->UserId);
	$show_popup = ($uinfo['id'] ? $this->catmodel->checkShowPopup($uinfo['id'])[0]['show_popup'] : -1);

	$this_url = $_SERVER['REQUEST_URI'];
	$redir_pars = Array("redirto" => $this_url);
	$redir_pars_str = http_build_query($redir_pars);

	$need_tab_sel = ($this->viewMode == "needlist");
	$do_tab_sel = ($this->viewMode == "dolist");
	$give_tab_sel = ($this->viewMode == "givelist");
	$my_tab_sel = ($this->viewMode == "sendlist");
	$myget_tab_sel = ($this->viewMode == "getlist");

?>
<script type="text/javascript" src="<?=WWWHOST?>js/jquery.progress.js"></script>
<div class="row-head breacrumb-bgi cab-bgi">
	<div class="container">
		<ol class="breadcrumb">
			<?=$BCHTML;?>
		</ol>
		<h1><?=$PAGE_H1;?></h1>

		<div class="cab-nav bar-fix">
			<ul class="nav nav-tabs">
				<li role="presentation" class="cab-tabs-fix"><a href="<?=$this->Page_BuildUrl("cabinet");?>"><?=$this->localize->get("cab", "tab-profile");?></a></li>
				<li role="presentation"<?=($myget_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelpget");?>"><?=$this->localize->get("cabinet", "needhelp");?><span class="<?=($this->need_services['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->need_services['tab_new'] > 0 ? '+'.$this->need_services['tab_new'] : $this->need_services['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($my_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelp");?>"><?=$this->localize->get("cabinet", "givehelp");?><span class="<?=($this->my_services['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->my_services['tab_new'] > 0 ? '+'.$this->my_services['tab_new'] : $this->my_services['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($need_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","needhelp");?>"><?=$this->localize->get("cab", "tab-needhelp");?> <span class="<?=($this->needhelp['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->needhelp['tab_new'] > 0 ? '+'.$this->needhelp['tab_new'] : $this->needhelp['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($do_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","dohelp");?>"><?=$this->localize->get("cab", "tab-dohelp");?> <span class="<?=($this->myhelp['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->myhelp['tab_new'] > 0 ? '+'.$this->myhelp['tab_new'] : $this->myhelp['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($give_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","givehelp");?>"><?=$this->localize->get("cab", "tab-givehelp");?> <span class="badge"><?=$this->thinghelp['tab_tot']?></span></a></li>
				<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet", "msgview");?>"><?=$this->localize->get("cab", "tab-msg");?><span class="<?=($this->mymsg['tab_new'] > 0 ? 'badge-red' : 'badge' )?>"><?=($this->mymsg['tab_new'] > 0 ? '+'.$this->mymsg['tab_new'] : 0 )?></span></a></li>
				<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet","mybonus");?>"><?=$this->localize->get("cab", "tab-bonus");?> <span class="badge"><?=$this->mybonus['tab_tot']?></span></a></li>
				<li role="presentation" class="needhelp-size fix-myface"><a href="<?=$this->Page_BuildUrl("users","viewrev").$this->UserId.'/'?>"><?=$this->localize->get("cab", "tab-onlooking");?> <span class="badge"><?=""?></span></a></li>
			</ul>
		</div>
	</div>
</div>
<div class="container cab-cont">
	<div class="col-md-5 col-xs-4 col-lg-4">
		<div class="left-usr-ground">
			<div class="<?=$uinfo['angel_week'] ? 'angel-status-week' : ($uinfo['angel_day'] ? 'angel-status-day' : 'angel-status-none') ?>"></div>
			<div class="block-photo" style="<?=($uinfo['angel_week'] || $uinfo['angel_day']) ? 'margin-top: -80px !important;' : ''?>">
				<img src="<?=WWWHOST.( $uinfo['pic'] != "" ? $uinfo['pic'] : 'img/no-pic.png' );?>" class="user-photo-usr" width="120" height="120">
				<div class="user-name" style="<?=mb_strlen($location) > 35 ? 'padding-left: 5px !important;' : ''?>">
					<p><?=$uinfo['name']?></p>
					<p><?=$uinfo['fname']?></p>
					<div class="block-u-loc-loc">
						<div class="user-loc-loc-img"></div>
						<div class="user-loc-loc-txt" style="<?=mb_strlen($location) > 35 ? 'font-size: 9.8px !important;' : ''?>"><?=$location?></div>
					</div>

				</div>
			</div>

			<div class="col-xs-12 col-md-12">
				<?php
				// Vkontakte
				$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);

				$VK_REDIRECT_URL = WWWHOST.'login/redirlinkVK';
				$VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);

				// Facebook
				$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);

				$FB_REDIRECT_URL = WWWHOST.'login/redirlinkFB';
				$FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);

				// Odnoklassniki
				$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);

				$OK_REDIRECT_URL = WWWHOST.'login/redirok';
				$OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);
				
				?>

				<p><?= (!$uinfo['vk_uid'] || !$uinfo['fb_uid'] || !$uinfo['od_uid']) ? $this->localize->get("cabprof","social").':' : '' ?></p>
				<?php if(!$uinfo['vk_uid']) { ?>
				<a href="<?=$VK_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/vk-link.png" class="cab-soc-mar" alt="" height="25px" width="25px"></a>
				<?php } if(!$uinfo['fb_uid']) { ?>
				<a href="<?=$FB_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/fb-link.png" class="cab-soc-mar" alt="" height="25px" width="25px"></a>
				<?php } if(!$uinfo['od_uid']) { ?>
				<a href="<?=$OK_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/ok-link.png" class="cab-soc-mar" alt="" height="25px" width="25px"></a>
				<?php } ?>
			</div>

			<hr>
			<div class="user-loc-info">
				<div class="block-u-loc-face">
					<div class="user-loc-face-img"></div>
					<div class="info-samle"><?=($uinfo['group_name']);?></div>
				</div>
			</div>
			<hr>
			<div class="user-box-info">
				<div class="user-a-row row">
					<div class="block-u-img col-md-6 col-xs-6 col-lg-6">
						<a href="<?=$this->Page_BuildUrl("info/stat")?>"><div class="user-box-status-img col-lg-3 col-xs-3 col-md-3"></div></a>
						<div class="user-box-status col-lg-9 col-xs-9 col-md-9" <?=(strlen($this->statusBox->getStatus($this->box['status'])) > 10 ? '' : "style='line-height: 30px'")?> ><div class="usr-status-ml mob-counter"><a href="<?=$this->Page_BuildUrl("info/stat")?>"><?=$this->statusBox->getStatus($this->box['status'])?></a></div></div>
					</div>
					<div class="block-u-mon col-md-4 col-xs-4 col-lg-4">
						<a href="<?=$this->Page_BuildUrl("info/power")?>"><div class="user-box-money-img"></div>
							<span class="user-box-money"><span class="usr-status-nocab ang-lh mob-counter"><?=$this->box['money']?></span></span></a>
					</div>
				</div>
				<div class="user-a-row a-row-h row">
					<div class="block-u-img block-ad-img col-md-6 col-xs-6 col-lg-6">
						<a href="<?=$this->Page_BuildUrl("info/angelsinfo")?>">
						<div class="user-box-ad-img ang-anim col-lg-3 col-xs-3 col-lg-3" title="<?=$this->localize->get("view", "ang-day")?>"></div></a>
						<div class="user-box-ang-count ang-wr col-lg-9 col-xs-9 col-md-9 mob-counter"><?=$uinfo['ad_count']?></div>
					</div>
					<div class="block-u-mon col-md-4 col-xs-4 col-lg-4">
						<a href="<?=$this->Page_BuildUrl("info/angelsinfo")?>">
						<div class="user-box-aw-img ang-anim" title="<?=$this->localize->get("view", "ang-week")?>"></div></a>
						<div class="user-box-ang-count ang-lh mob-counter"><?=$uinfo['aw_count']?></div>
					</div>
				</div>
			</div>
			<hr>
			<div class="user-rat-info">
				<div class="user-row-rat">
					<div class="user-rat-txt"><?=$this->localize->get("user","rat")?></div>
					<div class="user-rat-rating"><?=$this->box['user_rate']?></div>
				</div>
				<svg id="prog1"></svg>
				<!--<img class="rat-img u-rat-img" src="<?=WWWHOST;?>img/myrate-<?=(round($this->box['user_rate']/6));?>.png" with="170" height="6" alt="">-->
				<script>
					$("#prog1").Progress({
						width: 355,
						height: 7,
						percent: <?=(($this->box['user_rate']/60)*100)?>,
						backgroundColor: '#f4f4f4',
						barColor: '#36b247',
						radius: 4,
						fontSize: 0,
					});
				</script>
			</div>
		</div>

		<div class="fbnt-cab-pos">
			<a href="<?=$this->Page_BuildUrl("cabinet","editme");?>"><div class="btn cab-btn-def-two btn-cab-two">
				<div class="col-md-9">
					<p><?=$this->localize->get("cabinet", "redacted")?></p>
				</div>
			</div></a>
		</div>

		<div class="tbnt-cab-pos">
			<a href="<?=$this->Page_BuildUrl("cabinet","edittopic");?>"><div class="btn cab-btn-def-two btn-cab-two">
				<div class="col-md-9">
					<p><?=$this->localize->get("cabinet", "hobbies")?></p>
				</div>
			</div></a>
		</div>

		<div class="cab-border-hor-2"></div>

	</div>

	<div class="col-md-7 col-xs-6 col-lg-6">
		<div class="work-line-l cab-position-two">

						<div class="col-md-3 col-xs-3 col-lg-3">
							<img src="<?=WWWHOST?>img/tree.png" alt="tree" title="tree">
								<div class="text-cab-l"><?=$this->localize->get("cabinet", "mytree")?></div>
						</div>
			<a href="<?=WWWHOST?>info/tree">
						<div class="col-md-3 col-xs-3 col-lg-3">
							<div class="qmark size-cab-bolls"></div>
						</div></a>
		</div>

		<div class="col-md-1 col-md-pos">
			<div id="square-g"></div>

			<div id="number-square-1">
				<div class="cab-utmsg-in cab-margin"><span><?=$this->catmodel->Buyer_ReqNum($uinfo['id'], PROJ_NEEDHELP, REQ_STATUS_CONFIRM);?></span></div>
			</div>

			<div class="text-cab-r">
				<p class="text-left-position text-left-right-size <?=($this->catmodel->Buyer_ReqNum($uinfo['id'], PROJ_NEEDHELP, REQ_STATUS_CONFIRM)>99? "cab-left-hun" : "")?>"><?=$this->localize->get("cabinet", "takemyhelp")?></p>
			</div>
		</div>
		

		<div class="col-md-1 text-cab-r">
			<div id="square-r"></div>
			<div id="number-square-2">
				<div class="cab-utmsg-out"><span><?= $sendhelp = $this->catmodel->Buyer_ReqNum($uinfo['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);?></span></div>
			</div>
				<div class="text-cab-r">
					<p class="text-right-position text-left-right-size <?=($sendhelp = $this->catmodel->Buyer_ReqNum($uinfo['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM)>99? "cab-right-hun" : "")?>"><?=$this->localize->get("cabinet", "givemyhelp")?></p>
				</div>
		</div>
		
		<div class="col-xs-12 col-md-6">
			<div class="cab-utree-pos">
				<div class="cab-utree-graph" id="canvblock">
					<div id="mytree" data-usr="<?=$uinfo['id']?>"></div>
					<?php if($sendhelp == 0) { ?>
						<span class=" tree-arr-top">
						</span>
					<?php } ?>
				</div>
			</div>
		</div>

			<div class="cab-border-ver"></div>
		<div class="circle-cab-position">
			<table>

				<tr>
					<td><div class="circle-cab-paint1 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint2 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint3 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint4 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint5 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint6 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint7 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint8 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint9 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint10 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint11 circle-cab-paint"></div></td>
					<td><div class="circle-cab-paint12 circle-cab-paint"></div></td>
				</tr>

			</table>

		</div>
			<div class="cab-border-hor"></div>


				<div class="gbnt-cab-pos">
					<div class="btn cab-btn-def btn-info-work-do">
						<div class="col-md-3">
							<a href="<?=WWWHOST?>cat/">
							<img src="<?=WWWHOST?>img/work-button-hand.png" alt="<?=$this->localize->get("cabinet", "assist")?>" title="<?=$this->localize->get("cabinet", "assist")?>">
						</div>
						<div class="col-md-9">
							<p><?=$this->localize->get("cabinet", "makeassist")?></p>
						</div></a>
					</div>
				</div>

					<div class="rbnt-cab-pos">
						<div class="btn cab-btn-def btn-info-work-make">
							<div class="col-md-3">
								<a href="<?=WWWHOST?>proj/add/">
								<img src="<?=WWWHOST?>img/work-button-ball.png" alt="<?=$this->localize->get("cabinet", "needassist")?>" title="<?=$this->localize->get("cabinet", "needassist")?>">
							</div>
							<div class="col-md-9">
								<p><?=$this->localize->get("cabinet", "takeassist")?></p>
							</div></a>
						</div>
					</div>

					<div class="bbnt-cab-pos">
						<div class="btn cab-btn-def-adm btn-cab-msgadmin">
								<a id="sendmsglnk" href="#" ><div class="col-md-9">
								<p><?=$this->localize->get("cabinet", "adminmsg")?></p>
							</div></a>
						</div>
					</div>


			<div class="cab-border-hor-3"></div>
			<div class="cab-border-hor-4"></div>
			<div class="cab-border-hor-5"></div>

	</div>

	<?php /*
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="cab-uinfo">
				<div class="cab-uname"><?=( $uinfo['account_type'] == USR_TYPE_PERS ? $uinfo['fname'].' '.$uinfo['name'] : $uinfo['orgname'])?></div>
				<div class="row">
					<div class="col-xs-6 col-md-6">
						<div class="cab-upic"><img src="<?=WWWHOST.( $uinfo['pic'] != "" ? $uinfo['pic'] : 'img/no-pic.png' );?>" width="200" height="200" alt=""></div>
					</div>
					<div class="col-xs-6 col-md-6">
						<div class="cab-uirow cab-iusr"><?=($uinfo['group_name']);?></div>
					<?php
						if( $uinfo['account_type'] == USR_TYPE_PERS )
						{
							echo '
								<div class="cab-uirow">'.$this->localize->get("cabprof", "u-profession").': '.$uinfo['profession'].'</div>
								';
						}
					
						if( $uinfo['account_type'] != USR_TYPE_PERS )
						{ 
							echo '
							<div class="cab-uirow">'.$this->localize->get("cabprof", "u-sphere").': '.( $uinfo['orgsphere'] != "" ? $uinfo['orgsphere'] : $this->localize->get("cabprof", "u-sphere-notset") ).'</div>
							<div class="cab-uirow">'.$this->localize->get("cabprof", "u-contact").': '.$uinfo['fname'].' '.$uinfo['name'].'</div>							
							';
						}
					?>					
						<div class="cab-uirow cab-imap"><?=($uinfo['city']);?></div>
						<!--<div class="cab-uirow cab-iage">33 года</div>-->
						<br>
						<div>
									<?php
									$ubox = $this->catmodel->getDateBox($uinfo['id'])[0];
									?>
									<div class="cab-status"><span>Статус: </span><b><a href="<?=$this->Page_BuildUrl("info/ratrules")?>"><span class="usr-status"><?=$this->statusBox->getStatus($ubox['status'])?></a></span></b></div>
									<div class="cab-money"><span>Сила: </span><b><a href="<?=$this->Page_BuildUrl("info/power")?>"><span class="usr-money"><?=$ubox['money']?></a></span></b></div>
						</div>
						<br>
					<?php 
						if( trim($uinfo['web_url']) != '' ) {
							echo '<div class="cab-uwww">'.$this->localize->get("cabprof", "u-mysite").':<br>'.(trim($uinfo['web_url']) == '' ? '<span>'.$this->localize->get("cabprof", "u-nosite").'</span>' : '<a href="'.$uinfo['web_url'].'" target="_blank">'.$uinfo['web_url'].'</a>' ).'</div>';
						}
					?>
						
						<p class="cab-uratetit"><?=$this->localize->get("cabprof", "u-stat-rate");?>:</p>
						<table class="cab-urate">
						<tr><td><img src="<?=WWWHOST;?>img/rate-<?=(round($uinfo['helprate']['avgrate']));?>.png" with="138" height="8" alt=""></td><td><?=number_format($uinfo['helprate']['avgrate'], 2, ",","");?></td></tr>
						</table>
						<br>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-xs-12 col-md-12">					
						<br>
						<a href="<?=$this->Page_BuildUrl("cabinet","editme");?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> <?=$this->localize->get("cabprof", "u-btn-edit");?></a>							
						<a href="<?=$this->Page_BuildUrl("cabinet","edittopic");?>" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> <?=$this->localize->get("cabprof", "u-btn-topics");?></a>
					</div>
					<div class="col-xs-12 col-md-12">
						<?php
						// Vkontakte
						$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);

						$VK_REDIRECT_URL = WWWHOST.'login/redirlinkVK';
						$VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);

						// Facebook
						$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);

						$FB_REDIRECT_URL = WWWHOST.'login/redirlinkFB';
						$FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);

						// Odnoklassniki
						$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);

						$OK_REDIRECT_URL = WWWHOST.'login/redirlinkOK';
						$OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);
						?>
						<br>
						<p>Привязать соц. сети:</p>
						<a href="<?=$VK_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/vk-link.png" alt="" height="25px" width="25px"></a>
						<a href="<?=$FB_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/fb-link.png" alt="" height="25px" width="25px"></a>
						<a href="<?=$OK_LINK?>" target="_blank"><img src="<?=WWWHOST?>img/ok-link.png" alt="" height="25px" width="25px"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="cab-utree">
				<div class="cab-utree-tit"><?=$this->localize->get("cabprof", "tree-hdr");?><a href="#"><span>?</span></a></div>
				<div class="cab-utree-msg">
					<div class="cab-utmsg-in"><span><?=$this->catmodel->Buyer_ReqNum($uinfo['id'], PROJ_NEEDHELP, REQ_STATUS_CONFIRM);?></span> <?=$this->localize->get("cabprof", "tree-in");?></div>
					<div class="cab-utmsg-out"><span><?= $sendhelp = $this->catmodel->Buyer_ReqNum($uinfo['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);?></span> <?=$this->localize->get("cabprof", "tree-out");?></div>
				</div>
				<div class="cab-utree-graph">
					<canvas id="mytree" data-usr="<?=$uinfo['id'];?>" width="360" height="300"></canvas>
					<?php if($sendhelp == 0) { ?>
						<span class="tree-about">Дерево начнет расти, когда вы окажете помощь</span>
						<span class=" tree-arr-top">
							<span class="utree-top">Ваше дерево</span>
							<span class="arr-top"></span>
						</span>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="cab-rndmenu-row">
				<div class="cab-rndmenu">
					<table>
					<tr>
						<td class="cab-ahelpdo"><a href="<?=$this->Page_BuildUrl("cabinet","dohelp");?>"><?=$this->localize->get("cabprof", "rnd-dohelp");?></a></td>
						<td class="cab-ahelpget"><a href="<?=$this->Page_BuildUrl("cabinet","needhelp");?>"><?=$this->localize->get("cabprof", "rnd-gethelp");?></a></td>
					</tr>
					<tr>
						<td colspan="2" class="cab-amsg"><a href="<?=$this->Page_BuildUrl("cabinet", "msgview");?>"><?=$this->localize->get("cabprof", "rnd-msg");?></a></td>
					</tr>
					</table>
				<?php
				/*
					<table>
					<tr>
						<td class="cab-ahelpdo"><a href="<?=$this->Page_BuildUrl("cabinet","dohelp");?>"><?=$this->localize->get("cabprof", "rnd-dohelp");?></a></td>
						<td class="cab-astat"><a href="#"><?=$this->localize->get("cabprof", "rnd-stat");?></a></td>
						<td class="cab-ahelpget"><a href="<?=$this->Page_BuildUrl("cabinet","needhelp");?>"><?=$this->localize->get("cabprof", "rnd-gethelp");?></a></td>
					</tr>
					<tr>
						<td></td>
						<td class="cab-amsg"><a href="<?=$this->Page_BuildUrl("cabinet", "msgview");?>"><?=$this->localize->get("cabprof", "rnd-msg");?></a></td>
						<td></td>
					</tr>
					</table>
				*/

				?>
				</div>
			</div>
		</div>
	</div>

	<!--<div class="row">
		<div class="col-md-12">
			<div class="tree-msg">
				<?php if($sendhelp == 0) { ?>
				<p>Ваше дерево начнет расти, когда Вы окажете помощь любому проекту из нашей базы.</p> <p>Сейчас в базе <span><a
							href="<?=WWWHOST.'cat/flt_work/'?>"><?=$countWork?></a><span> проектов на "помощь делом" </p>
				<?php } ?>
			</div>
		</div>
	</div>
	<!-- <div class="row">
		<div class="col-xs-12 col-md-6">Поделиться</div>
		<div class="col-xs-12 col-md-6"><a href="#">Пожаловаться</a></div>
	</div>-->
<?php /*
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="cab-graph canvas-main"><div class="cab-graph-canv">
				<canvas id="maingraph" data-usr="<?=$uinfo['id'];?>" width="920" height="460" style="margin: 0 auto;"></canvas>
				<!--<img src="<?=WWWHOST;?>img/graph-myrelation.png" alt="">-->
					<div class="tree-table-noitems table-noitems">
						<?php if($sendhelp == 0) { ?>
						<div class="tbl-noitems-face tree-face"></div>
						<div class="tree-main  tbl-noitems-txt">
							<div>
									<span class="ext-p">Ваше дерево начнет расти, когда Вы окажете помощь любому проекту из нашей базы. Сейчас в базе <a
												href="<?=WWWHOST.'cat/flt_work/'?>"><?=$countWork[0]?></a> проектов на "помощь делом" и <a
											href="<?=WWWHOST.'cat/flt_money/'?>"><?=$countWork[1]?></a> на "помощь деньгами"</span>
							</div>
						</div>
						<span class="tree-arr">
							<span class="utree">Это ваше дерево</span>
							<span class="arr"></span>
						</span>
						<?php } ?>
					</div>
					<!--<div class="together">
							<div class="canvas-pict"></div>
							<div class="tree-msg">
									<?php if($sendhelp == 0) { ?>
										<p class="ext-p">Ваше дерево начнет расти, когда Вы окажете помощь любому проекту из нашей базы.</p> <p class="ext-p">Сейчас в базе <span><a
													href="<?=WWWHOST.'cat/flt_work/'?>"><?=$countWork?></a><span> проектов на "помощь делом" </p>
									<?php } ?>
							</div>
					</div>-->

			</div></div>
		</div>
	</div>
	<br><br>
</div> */ ?>
<?php
	//if( !$PM->isPromoSeen() )
	//{
//var_dump($this->is_pop);
if($this->is_pop) {
	?>

	<div class="poptip-promo">
		<div class="poptip-promo-cont">
			<div class="poptip-close"><a href="#" class="btn btn-sm btn-default"><span
						class="glyphicon glyphicon-remove"></span></a></div>
			<div class="poptip-info"><?= ($PM->get_txtres()->poppromo1['text']); ?></div>
			<div class="poptip-next"><a href="#" id="promonextlnk" class="btn btn-primary"><?=$this->localize->get("cabinet", "next")?></a></div>
		</div>
	</div>
	<?php
}
		//$PM->setPromoSeen();
	//}
?>
<style type="text/css">
	@import url("<?=WWWHOST?>css/graph.css");
	@import url("<?=WWWHOST?>css/vis.css");
</style>
<script type="text/javascript" src="<?=WWWHOST;?>js_vsgraph/my_vis.js"></script>
<script type="text/javascript">
	isMine = true;
</script>
<script type="text/javascript" src="<?= WWWHOST?>js_vsgraph/obj_graph_baloon.js"></script>
<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$PM = $this->pageModel;

$uid = ( isset($_GET['uid']) ? $_GET['uid'] : 0 );

?>
<p></p>
<div class="col-md-4 col-xs-4 col-lg-4 tree-share-block-cab cabdon"><p><?=$this->localize->get("cabinet", "open-your-tree")?><br>
		<?=$this->localize->get("cabinet", "good-deeds")?><br>
		<?=$this->localize->get("cabinet", "they-friend")?><br></p>
	<div class="socline-pop">
		<a onclick="Share.facebook('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=facebook&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>','<?=$share_title?>', '<?=$share_descr?>')"><div class="line-ico-tree cab-share-ico cab-soc-img proj-ico-fb"></div></a>
	</div>
	<div class="socline-pop">
		<a onclick="Share.vkontakte('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=vk&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>', '<?=$share_title?>', '<?=$share_descr?>');yaCounter34223620.reachGoal('PushButton_ShareVK'); return true;"><div class="line-ico-tree cab-share-ico cab-soc-img proj-ico-vk"></div></a>
	</div>
	<div class="socline-pop">
		<a onclick="Share.odnoklassniki('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=ok&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>');yaCounter34223620.reachGoal('PushButton_ShareOK'); return true;"><div class="line-ico-tree cab-share-ico cab-soc-img proj-ico-ok"></div></a>
	</div>
	<div class="socline-pop">
		<a onclick="Share.googleplus('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=google&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>', '<?=$share_title?>', '<?=$share_descr?>');yaCounter34223620.reachGoal('PushButton_ShareGP'); return true;"><div class="line-ico-tree cab-share-ico cab-soc-img proj-ico-gp cab-img-tw-gp"></div></a>
	</div>
	<div class="socline-pop">
		<a onclick="Share.twitter('<?=WWWHOST?>users/viewrev/<?=$uinfo['id']?>/?utm_source=twitter&utm_medium=social&utm_campaign=ShareDerevoizProfil&utm_content=<?=$uinfo['id']?>', '<?=$share_title?>');yaCounter34223620.reachGoal('PushButton_ShareTW'); return true;"><div class="line-ico-tree cab-share-ico cab-soc-img proj-ico-tw cab-img-tw-gp"></div></a>
	</div>
</div>
<style>
	#wnd-tree{
		z-index: 2;
	}
	#graph-control-wrap{
		position: relative;
		z-index: 3;
	}
#graph-wrapper {
margin: 4px 0px 0px;
height: 100%;
width: 100%;
position: relative;
top: -137px;
background: #fff;
overflow: hidden;
	z-index: 0;
}
</style>
<div class="cab-tree-size">
<div class="wnd-tree">
	<div class="cab-graph">
		<div id="vis-graph-wrap"></div>
	</div>
</div>
</div>
<script>
	var graph = new ObjGraph('vis-graph-wrap', '<?=$uinfo['id']?>');
</script>

<!--<canvas id="canvass" width="300" height="150"></canvas>
<form name="form1" method="post" action="<?=WWWHOST?>ajx/loadimg/">
	<input type="text" id="my_hidden">
	<input type="text" value="yoba">
</form> -->

<script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
<script>
	$(document).ready(function () {
		Share = {
			facebook: function (link, title, descr) {

				var canvas = document.getElementsByTagName("canvas");

				SaveTreeImg(canvas, '<?=$this->UserId?>', '<?=$uinfo['id']?>');

				var pic = '<?=WWWHOST?>' + 'img/tree_img/' + '<?=$this->UserId?>' + '_' + '<?=$uinfo['id']?>' + "_scr.png?rnd=" + Math.random();

				var facebook_appID = <?=FB_APP_ID?>;
				url = "https://www.facebook.com/dialog/feed?app_id=" + facebook_appID + "&link=" + encodeURIComponent(link) +
					"&picture=" + encodeURIComponent(pic) +
					"&name=" + encodeURIComponent(title) +
					"&description=" + encodeURIComponent(descr) +
					"&redirect_uri=https://www.facebook.com";
				Share.popup(url);
			},
			vkontakte: function (purl, ptitle, text) {

				var canvas = document.getElementsByTagName("canvas");

				SaveTreeImg(canvas, '<?=$this->UserId?>', '<?=$uinfo['id']?>');

				var pic = '<?=WWWHOST?>' + 'img/tree_img/' + '<?=$this->UserId?>' + '_' + '<?=$uinfo['id']?>' + "_scr.png?rnd=" + Math.random();

				url = 'http://vkontakte.ru/share.php?';
				url += 'url=' + purl;
				url += '&title=' + encodeURIComponent(ptitle);
				url += '&description=' + encodeURIComponent(text);
				url += '&image=' + encodeURIComponent(pic);
				Share.popup(url);
			},
			odnoklassniki: function (purl) {

				var canvas = document.getElementsByTagName("canvas");

				SaveTreeImg(canvas, '<?=$this->UserId?>', '<?=$uinfo['id']?>');

				url = 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1';
				url += '&st.comments=' + encodeURIComponent('text');
				url += '&st._surl=' + encodeURIComponent(purl);
				Share.popup(url);
			},
			googleplus: function (purl, ptitle, text) {

				var canvas = document.getElementsByTagName("canvas");

				SaveTreeImg(canvas, '<?=$this->UserId?>', '<?=$uinfo['id']?>');

				var pic = '<?=WWWHOST?>' + 'img/tree_img/' + '<?=$this->UserId?>' + '_' + '<?=$uinfo['id']?>' + "_scr.png?rnd=" + Math.random();

				url = 'https://plus.google.com/share?url=';
				url += 'url=' + purl;
				url += '&title=' + encodeURIComponent(ptitle);
				url += '&description=' + encodeURIComponent(text);
				url += '&image=' + encodeURIComponent(pic);
				Share.popup(url);
			},
			twitter: function(purl, text) {
				url = 'http://twitter.com/share?';
				url += 'text='+text;
				url += '&url='+purl;
				Share.popup(url);
			},
			popup: function (url, soc) {
				window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
			}
		}

		function SaveTreeImg(canvas, uid, oid) {

			var dataURL = 'image=' + canvas[0].toDataURL('image/png', 0.8) + '&uid=' + uid + '&oid=' + oid;

			$.ajax({
				type: "POST",
				url: req_ajx_host + "ajx/loadimg/",
				data: dataURL,
			}).done(function (o) {
				//console.log(o);
			});
		}
	});

</script>
<script>

$(document).ready(function() {
	drawMyTreeCabinet('mytree')	;
	/*ctx = document.getElementById("maingraph").getContext("2d");
	ctx.beginPath();
	canvas_arrow(ctx,600,327,480,240);
	ctx.textAlign="start";
	ctx.font = "15px Arial";
	ctx.fillText("Ваше дерево",580,350);
	ctx.stroke();


	function canvas_arrow(context, fromx, fromy, tox, toy){
		var headlen = 10;	// length of head in pixels
		var dx = tox-fromx;
		var dy = toy-fromy;
		var angle = Math.atan2(dy,dx);
		context.moveTo(fromx, fromy);
		context.lineTo(tox, toy);
		context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
		context.moveTo(tox, toy);
		context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
	}*/
	//drawMaingraphCabinet('maingraph');
<?php
	//if( !$PM->isPromoSeen() )
	//{		
?>
	
	var pic = '<?=$uinfo['pic_sm']?>';
	
	$("#promonextlnk").bind("click", function(){
		$(".poptip-promo").removeClass("poptip-promo-show");		
		popWnd('promohelpdlg2', '');
		return false
	});	

	$(".cab-utree-tit a").bind("click", function(){
		popWnd('promohelpdlg3', '');
		return false
	});

	if(show_popup > 0 && pic) {
		popWnd('cabpopup', '');

		if (uid) {
			var post_req_str = 'uid=' + '<?=$uinfo['id']?>';

			$.ajax({
				type: "GET",
				url: req_ajx_host + "ajx/setpopup/",
				data: post_req_str,
				dataType: "json",
				success: function (data) {
				}
			});
		}
	} else if(show_popup > 0 && !pic)
		popWnd('cabpopup','type=1');


	showPoptipPromo();

<?php
	/*
	$(".poptip-close a").bind("click", function(){
		$(".poptip-promo").removeClass("poptip-promo-show").hide();
		return false
	});
	
	var pos = $(".cab-rndmenu").offset();
	$(".poptip-promo").css("left", Math.round(pos.left - 150)+"px");
	$(".poptip-promo").css("top", Math.round(pos.top - 300)+"px");
	$(".poptip-promo").addClass("poptip-promo-show");
	*/
	//} 
?>

});
</script>
<?php //----------------------------------------ARTUR----------------------------------------------- ?>
<script>
	$(document).ready(function() {


			/*var canvas = document.getElementById('canvass');
			var context = canvas.getContext("2d");
			context.beginPath();
			context.moveTo(170, 80);
			context.bezierCurveTo(130, 100, 130, 150, 230, 150);
			context.bezierCurveTo(250, 180, 320, 180, 340, 150);
			context.bezierCurveTo(420, 150, 420, 120, 390, 100);
			context.bezierCurveTo(430, 40, 370, 30, 340, 50);
			context.bezierCurveTo(320, 5, 250, 20, 250, 50);
			context.bezierCurveTo(200, 5, 150, 20, 170, 80);

			// complete custom shapes
			context.closePath();
			context.lineWidth = 5;
			context.fillStyle = '#8ED6FF';
			context.fill();
			context.strokeStyle = 'blue';
			context.stroke();*/

			$('#btn').on('click', function() {
				// opens dialog but location doesnt change due to SaveAs Dialog
				//window.open('', document.getElementById('canvass').toDataURL());

				var canvas = document.getElementsByTagName("canvas");
				console.log(canvas);
				var dataURL = canvas[0].toDataURL('image/png');

				$.ajax({
					type: "POST",
					url: req_ajx_host+"ajx/loadimg/",
					data: 'image='+dataURL,
				}).done(function(o) {
					console.log('saved');
				});
			});

	$("#sendmsglnk").bind("click", function () {

		if (!usr_logged) {
			popWnd('logdlg', '<?=$redir_pars_str;?>');
			return false
		}
		popWnd('sendmsgdlg', 'uid=481');
		return false
		});

		});
</script>


<?php //------------------------------------------------------------------------------------------------------- ?>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=RX5e1YRRC5/K/T*aPFztj2AoUBHtwrhilP7weOZCG1NbfGMFLDwz88vz1lH1rEbiCFRQm9zftbYKI1i1h3Gx2zAUBj1fdhxfzTT3b/sD4RTK7wm3VBvdErk2x2GDf/86PnhO9gP38rUHNpZz/C9OMC6YrbNrtx0/je5OcHOa0Oc-';</script>