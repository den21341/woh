<?php
	const NEWS_PER_SCREEN = 12;

	$PM = $this->pageModel;

	$page = $PM->get_page();

	// Set page head title (h1 tag value)
	$PAGE_H1 = ( isset($this->seo[SEO_H1]) ? $this->seo[SEO_H1] : $page->title );

	if( $this->viewMode == "item" )
	{
		$PAGE_H1 = $this->iteminf['title'];
	}

	$sharecont = substr(strip_tags(isset($this->iteminf['content']) ? $this->iteminf['content'] : 0 ),0, 50);
	$sharecont .= '...';

	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);
?>
<div class="row">
	<div class="container-fluid">
		<div class="proj-header">
			<div class="container">
				<ol class="breadcrumb">
					<?=$BCHTML;?>
				</ol>
				<div class="proj-txt-h hlpw-txt-h col-lg-12 col-xs-12 col-md-12">
					<h1><?=($this->viewMode == 'item') ? $this->iteminf['title'] : $this->localize->get("footer", "good-wall")?></h1>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container helpw-container">
	<div class="scroll-row">
<?php if( $this->viewMode == "item" ) {
		$pic = WWWHOST.($this->iteminf['filename_src'] != '' ? substr($this->iteminf['filename_src'], 3) : 'img/no-pic.png')?>
	<div class="hlpw-news">
			<img class="img-responsive" src="<?=$pic?>" alt="<?=$this->iteminf['title']?>" title="<?=$this->iteminf['title']?>">
			<div class="social-row-share">
					<div class="p-share-txt"><?=$this->localize->get("projview", "share");?></div>
					<div class="share-line-ico">
						<div class="socline-block">
							<a onclick="Share.facebook('http://wayofhelp.com/publ/helpwall/<?=$this->iteminf['id']?>.html/?utm_source=vk&utm_medium=social&utm_campaign=dobraya_stena&utm_content=<?=$this->iteminf['id']?>','<?=$this->iteminf['title']?>', '<?=$sharecont?>')"><div class="line-ico ico-fb"></div></a>
						</div>
						<div class="socline-block">
							<a onclick="Share.vkontakte('http://wayofhelp.com/publ/helpwall/<?=$this->iteminf['id']?>.html/?utm_source=facebook&utm_medium=social&utm_campaign=dobraya_stena&utm_content=<?=$this->iteminf['id']?>', '<?=$this->iteminf['title']?>', '<?=$sharecont?>')"><div class="line-ico ico-vk"></div></a>
						</div>
						<div class="socline-block">
							<a onclick="Share.odnoklassniki('http://wayofhelp.com/publ/helpwall/<?=$this->iteminf['id']?>.html/?utm_source=ok&utm_medium=social&utm_campaign=dobraya_stena&utm_content=<?=$this->iteminf['id']?>', '<?=$this->iteminf['title']?>')"><div class="line-ico ico-ok"></div></a>
						</div>
					</div>
			</div>
			<div class="news-item-inf"><?=$this->iteminf['content']?></div>
		</div>
	<script>
		$(document).ready(function () {
			var pic = '<?=$pic?>';   // THIS TEST, CHECK HOW IT'll BE WORK ON SERVER

			Share = {
				vkontakte: function(purl, ptitle, text) {
					url  = 'http://vkontakte.ru/share.php?';
					url += 'url='          + purl;
					url += '&title='       + encodeURIComponent(ptitle);
					url += '&description=' + encodeURIComponent(text);
					url += '&image='       + encodeURIComponent(pic);
					Share.popup(url);
				},
				odnoklassniki: function(purl, text) {
					url  = 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1';
					url += '&st.comments=' + encodeURIComponent(text);
					url += '&st._surl='    + encodeURIComponent(purl);
					Share.popup(url);
				},
				facebook: function(link, title, descr) {
					var facebook_appID = <?=FB_APP_ID?>;
					url = "https://www.facebook.com/dialog/feed?app_id="+ facebook_appID +    "&link=" + encodeURIComponent(link)+
						"&picture=" + encodeURIComponent(pic) +
						"&name=" + encodeURIComponent(title) +
						"&description=" + encodeURIComponent(descr) +
						"&redirect_uri=https://www.facebook.com";
					Share.popup(url);
				},

				popup: function(url, soc) {
					window.open(url,'','toolbar=0,status=0,width=626,height=436');
				}
			};

		});
	</script>
	<?php } else {
		echo '<div class="row">';
		for ($i = 0; $i < 12; $i++) {
			if ($i % 4 == 0) { ?>
				<div class="hlpw-row col-md-4 col-lg-4 col-xs-4">
			<?php } ?>
			<div class="row">
				<?php if (($i == 0 || $i == 5 || $i == 8) && isset($this->its[$i])) { ?>
					<a href="<?=$this->publ_BuildUrl($this->ngroup, 0, 0, $this->its[$i]['id'])?>">
						<div class="hlpw-big-news">
							<img src="<?=WWWHOST.($this->its[$i]['filename_ico'] != '' ? substr($this->its[$i]['filename_ico'], 3) : 'img/no-pic.png') ?>" alt="<?=$this->its[$i]['title']?>" title="<?=$this->its[$i]['title']?>">
							<p><?=(strlen($this->its[$i]['title']) > 50 ? mb_substr($this->its[$i]['title'], 0, 50).'...' : $this->its[$i]['title']) ?></p>
						</div>
					</a>
				<?php } else if(isset($this->its[$i])) { ?>
					<a href="<?=$this->publ_BuildUrl($this->ngroup, 0, 0, $this->its[$i]['id'])?>">
						<div class="hlpw-sm-news">
							<table>
								<tr>
									<td><img src="<?=WWWHOST.($this->its[$i]['filename_ico'] != '' ? substr($this->its[$i]['filename_ico'], 3) : 'img/no-pic.png') ?>" alt="<?=$this->its[$i]['title']?>" title="<?=$this->its[$i]['title']?>"></td>
									<td><p><?=(strlen($this->its[$i]['title']) > 70 ? mb_substr($this->its[$i]['title'], 0, 70).'...' : $this->its[$i]['title']) ?></p></td>
								</tr>
							</table>
						</div>
					</a>
				<?php } ?>
			</div>
			<?php if ($i == 3 || $i == 7 || $i == 12) { ?>
				</div>
			<?php } ?>
		<?php }
		echo '</div>'; ?>
	</div>
	<script>
		its_total = <?=$this->its_total?>;
		pi = 12;
		i = 0;

		function loadNext() {
			var pn = pi+12;
			if(pi <= its_total) {
				//console.log('pi = ' + pi + ' ' + 'pn = ' + pn);
				$('#fountainG').css('display', 'block');
				loadMoreNews(pi, pn, '.scroll-row');
				pi += 12;
			}
			$('#fountainG').animate({
				opacity: 0.8
			}, 1100, function () {
				$('#fountainG').css('display', 'none');
			});
		}

		$(window).scroll(function() {
			if ($(document).height() - $(window).height() <= $(window).scrollTop() + 250) {
				loadNext();
			}
		});

		$(document).ready(function () {
			if(($(document).height() - $(window).height()) == 0) {
				loadNext();
			}
		});

	</script>
	<?php } ?>
	</div>
	<div id="fountainG">
		<div id="fountainG_1" class="fountainG"></div>
		<div id="fountainG_2" class="fountainG"></div>
		<div id="fountainG_3" class="fountainG"></div>
		<div id="fountainG_4" class="fountainG"></div>
		<div id="fountainG_5" class="fountainG"></div>
		<div id="fountainG_6" class="fountainG"></div>
		<div id="fountainG_7" class="fountainG"></div>
		<div id="fountainG_8" class="fountainG"></div>
	</div>
</div>