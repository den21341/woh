<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();
	
	$PAGE_H1 = $page->title;	
	
	// Build bread crumbs
	$BCROOTNAME = $this->localize->get("all", "bc-main");// "Главная";
	$BCROORURL = WWWHOST;
	
	$BCHTML = '<li><a href="'.$BCROORURL.'">'.$BCROOTNAME.'</a></li>';
	
	if( isset($this->breadcrumbs) && (count($this->breadcrumbs)>0) )
	{
		for( $i=0; $i<count($this->breadcrumbs); $i++ )
		{
			$BCHTML .= '<li><a href="'.$this->breadcrumbs[$i]['url'].'">'.$this->breadcrumbs[$i]['name'].'</a></li>';
		}
	}	
	
	$BCHTML .= '<li class="active">'.$PAGE_H1.'</li>';
?>
<div class="row-head">
	<div class="container">
		<ol class="breadcrumb">
			<?=$BCHTML;?>			
		</ol>
		<h1><?=$PAGE_H1;?></h1>
	</div>
</div>
<div class="container">
	<div class="info">
<?php
	echo $page->header;
	
	// Show videos attached to the page
	$clips = $page->clips;
	if( count($clips) > 0 )
	{
		echo "<script type='text/javascript' src='".WWWHOST."swfobject.js'></script>";
		echo '<div id="ipageclips">';
		echo '<table cellspacing="0" cellpadding="0" border="0">';
		
		for($i=0; $i<count($clips); $i++)
		{			
				echo '<tr>
					<td valign="top">';
				$pic = $clips[$i];
				if( trim($pic['tube_code']) != "" )
				{
					echo '<div style="padding: 0px 0px 0px 0px; width: 310px; height: 270px; text-align: center;">'.html_entity_decode(trim($pic['tube_code']), ENT_COMPAT).'</div>';
				}
				else
				{
					//echo '<embed src="'.$FILE_DIR.$orglogo.'" style="width: 300px;" loop="false" autostart="false" volume="25" hidden="false"></embed>';
			?>
				<div id='mediaspace<?=$i;?>'>This text will be replaced</div>

				<script type='text/javascript'>
				  var so<?=$i;?> = new SWFObject('player.swf','ply','310','270','9','#ffffff');
				  so<?=$i;?>.addParam('allowfullscreen','true');
				  so<?=$i;?>.addParam('allowscriptaccess','always');
				  so<?=$i;?>.addParam('wmode','opaque');
				  so<?=$i;?>.addVariable('file','<?=(WWWHOST.FILE_DIR.$pic['clip']);?>');
				  so<?=$i;?>.write('mediaspace<?=$i;?>');
				</script>
				<br />
			<?php
				}
				echo '</td>
				<td valign="top">
					<div class="pinfo_v_title">'.$pic['title'].'</a></div>
					<div class="pinfo_v_descr">'.$pic['descr'].'</div>
				</td>
				</tr>';
		}
		echo '</table>';
		echo '</div>';
	}
	
	// Show page's picture galery
	$pics = $page->pics;
	if( count($pics) > 0 )
	{
		echo '<div id="ipagepics">';
		for( $i=0; $i<count($pics); $i++ )
		{
			$pic = $pics[$i];
			echo '<div class="ipicit">
			<div class="ipic"><a href="'.$pic['thumb'].' rel="photo_group""><img src="'.$pic['ico'].'" width="'.$pic['ico_w'].'" height="'.$pic['ico_h'].'" border="0" class="ipicimg" alt="'.$pic['title'].'" /></a></div>';
			if( trim($pic['title']) != "" )
			{
				echo '<div class="ipictit">'.$pic['title'].'</div>';
			}
			echo '</div>';
		}
		echo '</div>';
	}
	
	echo $page->content;
?>
	</div>
</div>