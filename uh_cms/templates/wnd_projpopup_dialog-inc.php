<?php

$type = (isset($_GET['type']) ? $_GET['type'] : 1);
$pid = (isset($_GET['pid']) ? $_GET['pid'] : 0);

$this_url = $_SERVER['REQUEST_URI'];
$redir_pars = Array("redirto" => $this_url);
$redir_pars_str = http_build_query($redir_pars);

$item_type = $this->catLib->Item_Info($pid);
$item_type = $item_type['amount_type'];

if($type == 1 && $pid != 0) { ?>
    <style>
        .wnd-in-sm {
            background: url(<?=WWWHOST?>img/oblaka-pop.png) no-repeat;
            background-size: cover;
        }
    </style>
	 <div class="reratpos">
        <img class="rate-logo-pos" src="<?=WWWHOST?>img/wayofhelp-logo.png">
    </div>
    <p class="first-pop-toptext first-pop-txt3">Человек, который много помогает другим,<br>
        теперь сам нуждается в вашей помощи.<br><br>
        Любая помощь, включая репост его проекта в социальных сетях,<br>
        очень поможет этому пользователю.<br><br>
        Делайте добрые дела вместе с нами!</p>
    <div class="col-lg-12 col-md-12 col-xs-12 wnd-div-margin pop-div-pos">
        <div class="col-lg-3 col-md-3 col-xs-3">
            <div id="popup_getmoney" onclick="yaCounter34223620.reachGoal('PushButtonPomochProjectFromPopupGoodMan');" class="btn btn-pop-help btn-info-work-do">
                <div class="col-md-3">
                    <img src="<?=WWWHOST?>img/work-button-hand.png" class="btn-pop-true" alt="Помочь" title="Помочь">
                </div>
                <div class="col-md-9">
                    <p><b>Помочь</b></p>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-xs-2 wnd-div-margin">
            <div class="col-md-9">
                <p class="first-pop-txt2"><b>Поделиться:</b></p>
            </div>
        </div>

        <div class="col-lg-1 col-md-1 col-xs-1 wnd-div-margin">
            <a onclick="Share.facebook('http://wayofhelp.com/proj/view/<?=$pid?>/?utm_source=vk&utm_medium=social&utm_campaign=ShareProjectFromPopup2&utm_content=<?=$pid?>'); yaCounter34223620.reachGoal('PushButtonShareProjectFromPopupGoodManFB'); return true;"><div class="line-ico-pop2 ico-fb-pop"></div></a>
        </div>
        <div class="col-lg-1 col-md-1 col-xs-1 wnd-div-margin">
            <a onclick="Share.vkontakte('http://wayofhelp.com/proj/view/<?=$pid?>/?utm_source=facebook&utm_medium=social&utm_campaign=ShareProjectFromPopup2&utm_content=<?=$pid?>'); yaCounter34223620.reachGoal('PushButtonShareProjectFromPopupGoodManVK'); return true;"><div class="line-ico-pop2 ico-vk-pop"></div></a>
        </div>
        <div class="col-lg-1 col-md-1 col-xs-1 wnd-div-margin">
            <a onclick="Share.odnoklassniki('http://wayofhelp.com/proj/view/<?=$pid?>/?utm_source=ok&utm_medium=social&utm_campaign=ShareProjectFromPopup2&utm_content=<?=$pid?>'); yaCounter34223620.reachGoal('PushButtonShareProjectFromPopupGoodManOK'); return true; "><div class="line-ico-pop2 ico-ok-pop"></div></a>
        </div>

    </div>

<?php } else if($type == 2 && $pid != 0) { ?>
    <style>
        .wnd-in-sm {
            background: url(<?=WWWHOST?>img/back-popup.png) no-repeat;
            background-size: cover;
        }
    </style>
	 <div class="reratpos">
        <img class="rate-logo-pos" src="<?=WWWHOST?>img/wayofhelp-logo.png">
    </div>
    <p class="first-pop-toptext">Ваш репост может сильно помочь этому проекту!</p>
    <p class="first-pop-toptext first-pop-txt2">Еще не готовы оказать помощь?<br><br>Вы можете дать большой толчок этому проекту,<br>просто поделившись в социальных сетях </p>
    <div class="first-pop-share">

        <div class="socline-block">
            <a onclick="Share.facebook('http://wayofhelp.com/proj/view/<?=$pid?>/?utm_source=vk&utm_medium=social&utm_campaign=ShareProjectFromPopup1&utm_content=<?=$pid?>'); yaCounter34223620.reachGoal('PushButtonShareProjectFromPopup20baksovFB'); return true;"><div class="line-ico-pop ico-fb-pop"></div></a>
        </div>
        <div class="socline-block">
            <a onclick="Share.vkontakte('http://wayofhelp.com/proj/view/<?=$pid?>/?utm_source=facebook&utm_medium=social&utm_campaign=ShareProjectFromPopup1&utm_content=<?=$pid?>'); yaCounter34223620.reachGoal('PushButtonShareProjectFromPopup20baksovVK'); return true;"><div class="line-ico-pop ico-vk-pop"></div></a>
        </div>
        <div class="socline-block">
            <a onclick="Share.odnoklassniki('http://wayofhelp.com/proj/view/<?=$pid?>/?utm_source=ok&utm_medium=social&utm_campaign=ShareProjectFromPopup1&utm_content=<?=$pid?>'); yaCounter34223620.reachGoal('PushButtonShareProjectFromPopup20baksovOK'); return true;"><div class="line-ico-pop ico-ok-pop"></div></a>
        </div>
        <div class="socline-block">
            <a onclick="Share.google('http://wayofhelp.com/proj/view/<?=$pid?>/?utm_source=google&utm_medium=social&utm_campaign=ShareProjectFromPopup1&utm_content=<?=$pid?>'); yaCounter34223620.reachGoal('PushButtonShareProjectFromPopup20baksovGPlus'); return true;"><div class="line-ico-pop ico-gp-pop"></div></a>
        </div>
        <div class="socline-block">
            <a onclick="Share.twitter('http://wayofhelp.com/proj/view/<?=$pid?>/?utm_source=twitter&utm_medium=social&utm_campaign=ShareProjectFromPopup1&utm_content=<?=$pid?>'); yaCounter34223620.reachGoal('PushButtonShareProjectFromPopup20baksovTwitter'); return true;"><div class="line-ico-pop ico-tw-pop"></div></a>
        </div>
        <!--<div class="socline-block">
            <a href="#" "><div class="line-ico-pop ico-in-pop"></div></a>
        </div>-->
    </div>

<?php } ?>

<script>
    $(document).ready(function () {
        var post_req_str = 'type='+'<?=$type?>';

        $.ajax({
            type: "GET",
            url: req_ajx_host + "ajx/setNextPopup/",
            data: post_req_str,
            dataType: "json",
            success: function (data) {}
        });

        Share = {
            vkontakte: function(purl) {
                url  = 'http://vkontakte.ru/share.php?';
                url += 'url='          + purl;
                Share.popup(url);
            },
            odnoklassniki: function(purl) {
                url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
                url += '&st._surl='    + encodeURIComponent(purl);
                Share.popup(url);
            },
            facebook: function(purl) {
                url  = 'http://www.facebook.com/sharer.php?s=100';
                url += '&p[url]='       + encodeURIComponent(purl);
                Share.popup(url);
            },
            twitter: function(purl) {
                url  = 'http://twitter.com/share?';
                url += 'url='      + encodeURIComponent(purl);
                Share.popup(url);
            },
            google: function (purl) {
                url = 'https://plus.google.com/share?';
                url += 'url='+encodeURIComponent(purl);
                Share.popup(url);
            },

            popup: function(url, soc) {
                window.open(url,'','toolbar=0,status=0,width=626,height=436');
            }
        };

        $('#popup_getmoney').bind('click', function () {
            if (!usr_logged) {
                popWnd('logdlg', '<?=$redir_pars_str;?>');
                return false
            }
            
            if('<?=$item_type?>' == 0) {
                popWnd('addmoneydlg', 'projid=' + <?=$pid?>+'&prom=1&pop=1');
            } else {
                popWnd('addmandlg', 'projid=' + <?=$pid?>+'&pop=1');
            }

            return false
        });

    });
</script>

