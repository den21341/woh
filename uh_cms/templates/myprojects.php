<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();	
	
	$PAGE_H1 = $page->title;	
	
	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);
	
	$need_tab_sel = ($this->viewMode == "needlist");
	$do_tab_sel = ($this->viewMode == "dolist");
	$give_tab_sel = ($this->viewMode == "givelist");
	$my_tab_sel = ($this->viewMode == "sendlist");
	$myget_tab_sel = ($this->viewMode == "getlist");
	
	$num_new_msg = $PM->msg_NumNew($this->UserId);

	if(!isset($uinf['video_check'])) {
		$uinf['video_check'] = -1;
	}


?>
<!--<link rel="stylesheet" type="text/css" href="<?=WWWHOST?>js/fancybox/jquery.fancybox.css" />
<script src="<?=WWWHOST?>js/fancybox/jquery.fancybox.pack.js"></script>-->
<div class="row-head breacrumb-bgi cab-bgi">
	<div class="container">
		<ol class="breadcrumb">
			<?=$BCHTML?>
		</ol>
		<h1><?=$PAGE_H1?></h1>

		<div class="cab-nav bar-fix">
			<ul class="nav nav-tabs">
				<!--<li role="presentation" class="cab-tabs-fix"><a href="<?=$this->Page_BuildUrl("cabinet");?>"><?=$this->localize->get("cab", "tab-profile");?></a></li>
				<li role="presentation"<?=($myget_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelpget");?>">Попросил услуги<span class="<?=$this->tabNewHelp > 0 ? 'badge-red' : 'badge'?>"><?=($this->tabNewHelp > 0 ? '+'.$this->tabNewHelp : $this->tabDoHelpTot)?></span></a></li>
				<li role="presentation"<?=($my_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelp");?>">Предлагаю услуги<span class="badge"><?=$this->tabMyHelpTot;?></span></a></li>
				<li role="presentation"<?=($need_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","needhelp");?>"><?=$this->localize->get("cab", "tab-needhelp");?> <span class="badge"><?=$this->tabNeedHelpTot;?></span></a></li>
				<li role="presentation"<?=($do_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","dohelp");?>"><?=$this->localize->get("cab", "tab-dohelp");?> <span class="<?=$this->tabNewHelp != 0 ? 'badge-red' : 'badge'?>"><?=$this->tabNewHelp != 0 ? '+'.$this->tabNewHelp : ($this->tabDoHelpTot)?></span></a></li>
				<li role="presentation"<?=($give_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","givehelp");?>"><?=$this->localize->get("cab", "tab-givehelp");?> <span class="badge"><?=$this->tabGiveHelpTot;?></span></a></li>
				<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet", "msgview");?>"><?=$this->localize->get("cab", "tab-msg");?> <?=( $this->tabMsgNewNum > 0 ? '<span class="badge-red">+'.$this->tabMsgNewNum.'</span>' : '<span class="badge">0</span>' );?></a></li>
				<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet","mybonus");?>"><?=$this->localize->get("cab", "tab-bonus");?> <span class="badge"><?=$this->tabBonusNum;?></span></a></li>
				<li role="presentation" class="needhelp-size fix-myface"><a href="<?=$this->Page_BuildUrl("users","viewrev").$this->UserId.'/'?>"><?=$this->localize->get("cab", "tab-onlooking");?> <span class="badge"><?=""?></span></a></li>-->

				<li role="presentation" class="cab-tabs-fix"><a href="<?=$this->Page_BuildUrl("cabinet");?>"><?=$this->localize->get("cab", "tab-profile");?></a></li>
				<li role="presentation"<?=($myget_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelpget");?>"><?=$this->localize->get("cabinet", "needhelp");?> <span class="<?=($this->need_services['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->need_services['tab_new'] > 0 ? '+'.$this->need_services['tab_new'] : $this->need_services['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($my_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelp");?>"><?=$this->localize->get("cabinet", "givehelp");?> <span class="<?=($this->my_services['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->my_services['tab_new'] > 0 ? '+'.$this->my_services['tab_new'] : $this->my_services['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($need_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","needhelp");?>"><?=$this->localize->get("cab", "tab-needhelp");?> <span class="<?=($this->needhelp['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->needhelp['tab_new'] > 0 ? '+'.$this->needhelp['tab_new'] : $this->needhelp['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($do_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","dohelp");?>"><?=$this->localize->get("cab", "tab-dohelp");?> <span class="<?=($this->myhelp['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->myhelp['tab_new'] > 0 ? '+'.$this->myhelp['tab_new'] : $this->myhelp['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($give_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","givehelp");?>"><?=$this->localize->get("cab", "tab-givehelp");?> <span class="badge"><?=$this->thinghelp['tab_tot']?></span></a></li>
				<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet", "msgview");?>"><?=$this->localize->get("cab", "tab-msg");?><span class="<?=($this->mymsg['tab_new'] > 0 ? 'badge-red' : 'badge' )?>"><?=($this->mymsg['tab_new'] > 0 ? '+'.$this->mymsg['tab_new'] : 0 )?></span></a></li>
				<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet","mybonus");?>"><?=$this->localize->get("cab", "tab-bonus");?> <span class="badge"><?=$this->mybonus['tab_tot']?></span></a></li>
				<li role="presentation" class="needhelp-size fix-myface"><a href="<?=$this->Page_BuildUrl("users","viewrev").$this->UserId.'/'?>"><?=$this->localize->get("cab", "tab-onlooking");?> <span class="badge"><?=""?></span></a></li>
			</ul>
		</div>
	</div>
</div>

<div class="container">
<?php
	if( ($this->viewMode == "needlist") || ($this->viewMode == "givelist") || ($this->viewMode == "sendlist") )
	{
		$base_url_part = "needhelp";
		if( $this->viewMode == "givelist" )
			$base_url_part = "givehelp";
		else if( $this->viewMode == "sendlist" )
			$base_url_part = "myhelp";
		
		$its = $this->projlist;
?>
	<div class="proj-flt">
		<div class="row">
			<div class="col-xs-12 col-md-6 proj-flt-col">
				<div class="btn-group" role="group" aria-label="<?=$this->localize->get("cabneed", "flt-hdr");?>">
					<a href="<?=$this->Page_BuildUrl("cabinet", $base_url_part);?>" class="btn btn-default<?=($this->filtby == "" ? ' active' : '');?>"><?=$this->localize->get("cabneed", "flt-all");?></a>
					<a href="<?=$this->Page_BuildUrl("cabinet", $base_url_part);?>filt_running/" class="btn btn-default<?=($this->filtby == "running" ? ' active' : '');?>"><?=$this->localize->get("cabneed", "flt-cur");?></a>
					<a href="<?=$this->Page_BuildUrl("cabinet", $base_url_part);?>filt_finished/" class="btn btn-default<?=($this->filtby == "finished" ? ' active' : '');?>"><?=$this->localize->get("cabneed", "flt-ended");?></a>
				</div>
			</div>			
			<?php
				/*
			<div class="col-xs-12 col-md-4 ta_center proj-flt-col">
				<select class="form-control">
					<option value="0">-- Раздел не выбран --</option>
				</select>
			</div>
				*/
			?>						
			<div class="col-xs-12 col-md-6 ta_right proj-flt-col">
			<?php
				if( $this->UserGroup == USR_TYPE_ORG )
				{
					echo '<a href="'.$this->Page_BuildUrl("proj", "addevent").'" class="btn btn-needhelp"><span class="glyphicon glyphicon-ok-sign"></span> '.$this->localize->get("cabneed", "btn-newevent").'</a> &nbsp; ';
				} 
				
				if( $this->viewMode == "givelist" )
				{
			?>
				<a href="<?=$this->Page_BuildUrl("proj", "addthing");?>" class="btn btn-needhelp"><span class="glyphicon glyphicon-ok-sign"></span> <?=$this->localize->get("cat", "btn-thing");?></a>
			<?php
				}
				if( $this->viewMode == "sendlist" )
				{
			?>
				<a href="<?=$this->Page_BuildUrl("proj", "addmyhelp");?>" class="btn btn-needhelp"><span class="glyphicon glyphicon-ok-sign"></span> <?=$this->localize->get("cat", "btn-myhelp");?></a>
			<?php
				}
				else
				{
			?>
				<a href="<?=$this->Page_BuildUrl("proj", "add");?>" class="btn btn-needhelp"><span class="glyphicon glyphicon-ok-sign"></span> <?=$this->localize->get("cabneed", "btn-new");?></a>
			<?php
				} 
			?>
			</div>
		</div>
	</div>
	
	<div class="proj-tbl">
		<table>
		<tr>
			<th><?=$this->localize->get("cabneed", "tbl-id");?></th>
			<th class="name"><?=$this->localize->get("cabneed", "tbl-proj");?></th>
			<th><?=$this->localize->get("cabneed", "tbl-status");?></th>
			<th><?=$this->localize->get("cabneed", "tbl-msg");?></th>
			<th><?=$this->localize->get("cabneed", "tbl-sten");?></th>
			<th><?=$this->localize->get("cabneed", "tbl-left");?></th>
			<th><?=$this->localize->get("cabneed", "tbl-func");?></th>
		</tr>
	<?php
	//print_r($its); die();
	$currency_data = Array(
		0 => Array("name" => "UAH", "defval" => "100.0"),
		1 => Array("name" => "UAH", "defval" => "100.0"),
		2 => Array("name" => "RUB", "defval" => "200.0"),
		3 => Array("name" => "UAH", "defval" => "100.0"),
		//4 => Array("name" => "EUR", "defval" => "20.0")
		4 => Array("name" => "UAH", "defval" => "100.0")
	);

		for( $i=0; $i<count($its); ++$i ) {

			if($its[$i]['amount_type'] == PROJ_TYPE_MONEY)
				$newreq = $this->catmodel->Item_ReqCollected($its[$i]['id'], -10, 1, -1, 0);
			else
				$newreq = $this->catmodel->Item_ReqCollected($its[$i]['id'], REQ_STATUS_NEW);
			$confirmed = $this->catmodel->Item_ReqCollected($its[$i]['id'], REQ_STATUS_CONFIRM);			
			$confprog_percnt = $this->catmodel->PercentDone($confirmed['sum'], $its[$i]['amount'], 0, 'money');
			$tmleft_str = $this->catmodel->Time_LeftFromMinutes($its[$i]['tmleft']);
			$tmprog_percnt = $this->catmodel->PercentDone($its[$i]['tmleft'], $its[$i]['tmall']);

			//print_r($confprog_percnt);
			
			// Показать отображение для проекта по сбору средств
			if( $its[$i]['amount_type'] == PROJ_TYPE_MONEY ) {
				$curr_name = $this->catmodel->Currency_Info($its[$i]['currency_id']);
				$targ_str = number_format($its[$i]['amount'], 0, ",", " ").' '.$curr_name['name'];
				$confirm_str = number_format($confirmed['sum'],  0, ",", " ").' '.$curr_name['name'];
				$progress_str = $confirm_str." / ".$targ_str;
			}
			else if( $its[$i]['amount_type'] == PROJ_TYPE_HUMAN ) {
				$targ_str = round($its[$i]['amount'])." ".$this->localize->human;
				$confirm_str = round($confirmed['sum'])." ".$this->localize->human;
				$progress_str = $confirm_str." / ".$targ_str;
			}
			else {
				$progress_str = round($confirmed['sum'])." ".$this->localize->requests;
			}

			
			$in_msg_new = $this->catmodel->Item_MessagesNum($this->UserId, $its[$i]['id'], "in", MSG_STATUS_NEW);
			
			$proj_status_class = "aplnk-start";
			$proj_status_lbl = $this->localize->get("cabneed", "stat-run"); //"Проект запущен";
			switch($its[$i]['status'])
			{
				case PROJ_STATUS_STOP:
					$proj_status_class = "aplnk-pause";
					$proj_status_lbl = $this->localize->get("cabneed", "stat-stopped"); //"Проект приостановлен";
					break;
					
				case PROJ_STATUS_ENDED:
					$proj_status_class = "aplnk-pause";
					$proj_status_lbl = $this->localize->get("cabneed", "stat-ended"); //"Проект приостановлен";
					break;
			}
			//$show_prepay_lnk = ( !$its[$i]['is_play'] && !$its[$i]['amount_type'] );

			$show_edit_lnk	= ( $its[$i]['status'] != PROJ_STATUS_ENDED && !$its[$i]['is_success']);
			$show_run_lnk 	= ( $its[$i]['status'] == PROJ_STATUS_STOP && !$its[$i]['is_success'] );
			$show_stop_lnk 	= ( $its[$i]['status'] == PROJ_STATUS_RUN );
			$show_success_lnk = ($its[$i]['amount_type'] == 0 && !$its[$i]['is_success']) ? ( $confprog_percnt >= 60 ? 1 : 0 ) : ($confirmed['num'] && !$its[$i]['is_success'] ? 1 : 0);
			($show_success_lnk ? $show_stop_lnk = '' : '');
			($show_run_lnk ? $show_success_lnk = '' : '');

			$show_woh_link = ( $its[$i]['amount_type'] == PROJ_TYPE_MONEY && $its[$i]['is_success'] && $its[$i]['status'] != PROJ_STATUS_PAYED);

			echo '<tr>
				<td>'.$its[$i]['id'].'</td>
				<td class="name">
					<div class="p-tit"><a href="'.$this->Page_BuildUrl("proj", "view/".$its[$i]['id']).'">'.$its[$i]['title2'].'</a></div>
					<div class="p-sums">'.$progress_str.'</div>
					'.( ($its[$i]['amount_type'] == PROJ_TYPE_MONEY) || ($its[$i]['amount_type'] == PROJ_TYPE_HUMAN) ? '<div class="p-progress"><img src="'.WWWHOST.'img/rateorg-'.(round(($confprog_percnt > 100 ? 100 : $confprog_percnt) / 10.0)).'.png" alt=""></div>' : '' ).'
					'.( $newreq['num'] > 0 ? 
						'<div class="p-newhelpreq"><a href="'.$this->Page_BuildUrl("cabinet", "reqlist/".$its[$i]['id']).'">'.$this->localize->get( ( $this->viewMode == "sendlist" ? "cabmyhelp" : "cabneed" ), "req-new").': <span class="badge">+'.$newreq['num'].'</span></a></div>' : 
						'<div class="p-helpreq"><a href="'.$this->Page_BuildUrl("cabinet", "reqlist/".$its[$i]['id']).'">'.$this->localize->get( ( $this->viewMode == "sendlist" ? "cabmyhelp" : "cabneed" ), "req-all").'</a></div>'
					).'
				</td>
				<td><span class="'.$proj_status_class.'" title="'.$proj_status_lbl.'"></span></td>			
				<td>'.( $in_msg_new > 0 ? '<span class="badge">+'.$in_msg_new.'</span>' : ' - ' ).'</td>
				<td><p>'.$its[$i]['stdt'].'<br>'.$its[$i]['endt'].'</p></td>
				<td>
					<div class="p-tm">
						<span><img src="'.WWWHOST.'img/progress-act.png" width="'.(ceil($tmprog_percnt * 1.2)).'" height="8" alt=""></span>
						<br>
						'.$tmleft_str.'
					</div>
				</td>
				<td>
					'.''/*( $show_prepay_lnk ? '<div class="checked"></div><a href="'.$this->Page_BuildUrl("proj","prepay")."?projid=".$its[$i]['id'].'" class="cab-prepay">'.$this->localize->get("cabneed", "prepay").'</a><br>' : '' )*/.'
					'.( $show_edit_lnk ? '<a href="'.$this->Page_BuildUrl("proj","edit/".$its[$i]['id']).'" class="aplnk-edit">'.$this->localize->get("cabneed", "func-edit").'</a><br>' : '' ).'
					'.( $show_run_lnk ? '<a href="'.$this->Page_BuildUrl("cabinet","projstart", "projid=".$its[$i]['id']).'" class="aplnk-run">'.$this->localize->get("cabneed", "func-start").'</a>' : '' ).'
					'.( $show_stop_lnk ? '<a href="'.$this->Page_BuildUrl("cabinet","projdel", "projid=".$its[$i]['id']).'" class="aplnk-del">'.$this->localize->get("cabneed", "func-stop").'</a>' : '' ).'
					'.( $show_success_lnk ? '<div class="checked"></div><a href="'.$this->Page_BuildUrl("cabinet","projsuccess", "projid=".$its[$i]['id']).'" class="aplnk-success">'.$this->localize->get("cabneed", "func-success").'</a>' : '' ).'';
					if($show_woh_link) { ?>
						<form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
							<input type="hidden" name="ik_co_id" value="561555503d1eafe0518b4567"/>
							<input type="hidden" name="ik_pm_no" value="ID_3_<?= $its[$i]['id'] ?>_<?= $this->UserId ?>"/>
							<input type="hidden" name="ik_am" id="ik_am_inp" value="<?=(round($confirmed['sum']*0.05, 2))?>" /><!--  100.0  -->
							<input type="hidden" name="ik_cur" id="ik_cur"
								   value="<?= ($currency_data[$its[$i]['currency_id']]['name']); ?>"/><!-- UAH -->
							<input type="hidden" name="ik_desc" value="<?=$this->localize->get("myprojects", "charitable");?>"/>
							<input type="hidden" name="ik_exp" value="<?= date("Y-m-d", time() + 24 * 3600); ?>"/>
							<input type="hidden" name="ik_ia_u" value="http://wayofhelp.com/payscallback/ikinteract/"/>
							<input type="hidden" name="ik_ia_m" value="post"/>
							<input type="hidden" name="ik_suc_u" value="<?= $this->Page_BuildUrl("cabinet", "" ); ?>"/>
							<input type="hidden" name="ik_suc_m" value="get"/>
							<input type="hidden" name="ik_fal_u" value="<?= $this->Page_BuildUrl("cabinet", "" ); ?>"/>
							<input type="hidden" name="ik_fal_m" value="get"/>
							<!--<input type="hidden" name="ik_int" value="web" />
                            <input type="hidden" name="ik_am_t" value="payway" />-->
							<input type="submit" class="btn btn-success" value="<?=$this->localize->get("myprojects", "payed");?> <?=WOH_PERCENT_STR?>%">
						</form>
					<?php }
					echo '
				</td>
			</tr>';
		}

		if( count($its) == 0 )
		{			
			if( $this->filtby == "running" )		$noitems_str = $this->localize->get("cabneed", "tbl-noproj-running");
			else if( $this->filtby == "finished" )	$noitems_str = $this->localize->get("cabneed", "tbl-noproj-finished");
			else									$noitems_str = $this->localize->get("cabneed", "tbl-noproj"); 
				
			echo '<tr><td colspan="7" class="noitems">
				<div class="tbl-noitems">
					<div class="tbl-noitems-txt"><div><span>'.$noitems_str.'</span></div></div>
					<div class="tbl-noitems-face"></div>
				</div>
			</td></tr>';
		}		
	?>
		</table>
	</div>
	
	<?php
		$PAGES_NUM = ceil($this->projlist_total / $this->pn);
		
		//echo $this->projlist_total.":".$this->pn.":".$PAGES_NUM."<br>";
		
		if( $PAGES_NUM > 1 )
		{		
	?>
	<div class="proj-pages">
		<nav class="navbar-right">
			<ul class="pagination">
	<?php
			for( $i=1; $i<=$PAGES_NUM; $i++)
			{
				echo '<li'.( $this->pi == ($i-1) ? ' class="active"' : '' ).'><a href="'.$this->Page_BuildUrl("cabinet", "needhelp").($this->filtby != "" ? 'filt_'.$this->filtby.'/' : '').($i>1 ? 'p_'.$i.'/' : '').'">'.$i.'</a></li>';
			}
			/*
	?>
			<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
			<li class="active"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
			*/
	?>
			</ul>
		</nav>
		<div class="clearfix"></div>
	</div>
<?php
		}
	}
	else if( ($this->viewMode == "dolist") || ($this->viewMode == "getlist") ) {
		$its = $this->projlist;
		$uinf = $this->uinfo;

		$videos = [
			'Y04LsL10Sl8',
			'jfSGcn5dPIA',
			'AwhY0QMRV-o',
			'mDU9yWyH7XU',
			'NMTnIY97w2o'
		];

		/*echo '<pre>';
		print_r($this->uinfo);*/

		if(isset($videos[$uinf['video_check']])) {
			$v_src = $videos[$uinf['video_check']];
		} else {
			$v_src = $videos[4];
		}

?><!--<script src="http://www.youtube.com/player_api"></script>-->

	<div class="proj-flt">
		<?php if(isset($this->videoSub)) { /*
			$shareDesc = $this->videoSub['name'].' и команда WayOfHelp благодарит пользователя '.$uinf['name'].' '.$uinf['fname']. ' за оказанную помощь.';
			?>
			<a class="fancybox fancybox.iframe" style="display: none" href="http://www.youtube.com/embed/<?=$v_src?>?enablejsapi=1&wmode=opaque" title='<?=$shareDesc?>'></a>

			<script>
				function onPlayerReady(event) {
					event.target.playVideo();
				}

				function onPlayerStateChange(event) {
					if (event.data === 0) {
						$.fancybox.close();
						$("#various1").fancybox({
							'titlePosition'     : 'inside',
							'transitionIn'      : 'none',
							'transitionOut'     : 'none'
						});
						$("#various1").eq(0).trigger('click');
					}
				}

				function onYouTubePlayerAPIReady() {
					$(document).ready(function() {
						$(".fancybox")
							.attr('rel', 'gallery')
							.fancybox({
								openEffect  : 'none',
								closeEffect : 'none',
								nextEffect  : 'none',
								prevEffect  : 'none',
								padding     : 0,
								margin      : 50,
								beforeShow  : function() {
									var id = $.fancybox.inner.find('iframe').attr('id');

									var player = new YT.Player(id, {
										events: {
											'onReady': onPlayerReady,
											'onStateChange': onPlayerStateChange
										}
									});
								}
							});
						$(".fancybox").eq(0).trigger('click');
					});

				}
			</script>

			<a id="various1" style="display: none" href="#inline1" title="Сделай репост"></a>
			<div style="display: none;">
				<div id="inline1">
					<div class="video-soc-block">
						<a onclick="Share.facebook('http://wayofhelp.com/proj/view/<?=$this->videoSub['item_id']?>/','Спасибо за помощь', '<?=$shareDesc?>')"><div class="req-video-fb req-video-soc"></div></a>
						<a onclick="Share.vkontakte('http://wayofhelp.com/proj/view/<?=$this->videoSub['item_id']?>/', 'Спасибо за помощь', '<?=$shareDesc?>')"><div class="req-video-vk req-video-soc"></div></a>
						<a onclick="Share.odnoklassniki('http://wayofhelp.com/proj/view/<?=$this->videoSub['item_id']?>/', '<?=$shareDesc?>')"><div class="req-video-ok req-video-soc"></div></a>
					</div>
				</div>
			</div>
		<?php */ }  ?>
		<div class="row">
			<div class="col-xs-12 col-md-6 proj-flt-col">
				<div class="btn-group" role="group" aria-label="<?=$this->localize->get("cabdohelp", "flt-hdr");?>">
					<a href="<?=$this->Page_BuildUrl("cabinet", "dohelp");?>" class="btn btn-default<?=($this->filtby == "" ? ' active' : '');?>"><?=$this->localize->get("cabdohelp", "flt-all");?></a>
					<a href="<?=$this->Page_BuildUrl("cabinet", "dohelp");?>filt_running/" class="btn btn-default<?=($this->filtby == "running" ? ' active' : '');?>"><?=$this->localize->get("cabdohelp", "flt-cur");?></a>
					<a href="<?=$this->Page_BuildUrl("cabinet", "dohelp");?>filt_finished/" class="btn btn-default<?=($this->filtby == "finished" ? ' active' : '');?>"><?=$this->localize->get("cabdohelp", "flt-ended");?></a>
				</div>
			</div>			
			<?php
				/*
			<div class="col-xs-12 col-md-4 ta_center proj-flt-col">
				<select class="form-control">
					<option value="0">-- Раздел не выбран --</option>
				</select>
			</div>
				*/
			?>			
			<div class="col-xs-12 col-md-6 ta_right proj-flt-col">			
				<a href="<?=$this->Page_BuildUrl("cat", "");?>" class="btn btn-dohelp"><span class="glyphicon glyphicon-flag"></span> <?=$this->localize->get("cabdohelp", "btn-dohelp");?></a>
			</div>
		</div>
	</div>
		
	<div class="proj-tbl">
		<table>
		<tr>
			<th colspan="2" class="name"><?=$this->localize->get("cabdohelp", "tbl-proj");?></th>
			<th><?=$this->localize->get("cabdohelp", "tbl-status");?></th>
			<th><?=$this->localize->get("cabdohelp", "tbl-msg");?></th>
			<th><?=$this->localize->get("cabdohelp", "tbl-left");?></th>
			<th><?=$this->localize->get("cabdohelp", "tbl-func");?></th>
		</tr>
	<?php
		for( $i=0; $i<count($its); ++$i ) {
			$confirmed = $this->catmodel->Item_ReqCollected($its[$i]['item_id'], REQ_STATUS_CONFIRM);			
			$confprog_percnt = $this->catmodel->PercentDone($confirmed['sum'], $its[$i]['amount'], 0 , 'money');
				
			$tmleft_str = $this->catmodel->Time_LeftFromMinutes($its[$i]['tmleft']);
			$tmprog_percnt = $this->catmodel->PercentDone($its[$i]['tmleft'], $its[$i]['tmall']);

			//$check_prom_send = $this->catmodel->checkPromiseMessSend($uinf['id'], $its[$i]['item_id']);
			//$check_prom_send = !empty($check_prom_send) ? 1 : 0;
				
			// Показать отображение для проекта по сбору средств
			$req_stat_html = '';
			if( $its[$i]['amount_type'] == PROJ_TYPE_MONEY ) {
				$curr_name = $this->catmodel->Currency_Info($its[$i]['currency_id']);
				$targ_str = number_format($its[$i]['amount'], 0, ",", " ").' '.$curr_name['name'];
				$confirm_str = number_format($confirmed['sum'],  0, ",", " ").' '.$curr_name['name'];
				$progress_str = $confirm_str." / ".$targ_str;

				$wohpayed_item = $this->catmodel->getWohPayedItem($uinf['id']);

				foreach ($wohpayed_item as $k=>$itm_id) {
					$wohpayed_item[$k] = $itm_id['item_id'];
				}

				$sum_to_pay = (in_array($its[$i]['item_id'], $wohpayed_item) ? round($its[$i]['req_amount']-($its[$i]['req_amount']*(WOH_PERCENT_BY_PAY/100)), 2) : $its[$i]['req_amount']);

				$req_stat_html = 'Внес: <b>'.number_format($its[$i]['req_amount'], 2, ".", " ").'</b><br>';
			}
			else if( $its[$i]['amount_type'] == PROJ_TYPE_HUMAN )
			{
				$targ_str = round($its[$i]['amount'])." ".$this->localize->human;
				$confirm_str = round($confirmed['sum'])." ".$this->localize->human;
				$progress_str = $confirm_str." / ".$targ_str;
			}
			else
			{
				$progress_str = round($confirmed['sum'])." ".$this->localize->requests;
			}
						
			switch( $its[$i]['req_status'] ) {
				case REQ_STATUS_CONFIRM:
					if($its[$i]['is_promise'] == REQ_PROMISE && $its[$i]['promise_pay'] == REQ_PRIMISE_NOPAY) {
						/*if($check_prom_send) {
							$req_stat_html .= '<span class="p-stat-ok">'.$this->localize->get("cabdohelp", "stat-promisemess").'</span>' . '<abbr title="' . $this->localize->get("cabdohelp", "stat-promaddrmsg") . ': ' . $confirm_str . '">&nbsp;<span class="qmark qmark-cab"></span></abbr>';
						} else*/
							$req_stat_html .= '<span class="p-stat-ok">' . $this->localize->get("cabdohelp", "stat-promise") . '</span>' . '<abbr title="' . $this->localize->get("cabdohelp", "stat-promaddr") . ': ' . $targ_str . '">&nbsp;<span class="qmark qmark-cab"></span></abbr>';
					} else if($its[$i]['is_promise'] == REQ_PROMISE && $its[$i]['promise_pay'] == 2)
							$req_stat_html .= '<span class="p-stat-new">'.$this->localize->get("cabdohelp", "stat-promisepay").'</span>';
					else
						$req_stat_html .= '<span class="p-stat-ok">' . $this->localize->get("cabdohelp", "stat-confirm") . '</span>';
					break;
				case REQ_STATUS_NEW:
					$req_stat_html .= ($its[$i]['req_type'] == REQ_TYPE_MONEY && $its[$i]['show'] == 1) ? '<span class="p-stat-new" style="color:#ac4c52">' . $this->localize->get("cabdohelp", "stat-waitbyuser") . '</span>' : '<span class="p-stat-new">' . $this->localize->get("cabdohelp", "stat-wait") . '</span>';
					break;
				case REQ_STATUS_DECLINE:
					$req_stat_html .= '<span class="p-stat-decl">' . $this->localize->get("cabdohelp", "stat-decline") . '</span>';
					break;
				case REQ_STATUS_CANCEL:
					$req_stat_html .= '<span class="p-stat-cancel">' . $this->localize->get("cabdohelp", "stat-cancel") . '</span>';
					break;
			}
			
			$in_msg_new = $this->catmodel->Item_MessagesNum($this->UserId, $its[$i]['item_id'], "in", MSG_STATUS_NEW);

			$promise = $this->catmodel->checkPromisePays($its[$i]['item_id'], $this->UserId);

			if(!empty($promise[0]['sum'])) {
				$projinfo = $this->catmodel->Item_Info($its[$i]['item_id']);
				$promSum = $promise[0]['sum'];
				$promise = 1;
			} else
				$promise = 0;

			$currency_data = Array(
				0 => Array("name" => "UAH", "defval" => "100.0"),
				1 => Array("name" => "UAH", "defval" => "100.0"),
				2 => Array("name" => "RUB", "defval" => "200.0"),
				3 => Array("name" => "UAH", "defval" => "100.0"),
				//4 => Array("name" => "EUR", "defval" => "20.0")
				4 => Array("name" => "UAH", "defval" => "100.0")
			);

			echo '<tr>
				<td><img src="'.( WWWHOST.( $its[$i]['pic_sm'] != "" ? (file_exists($its[$i]['pic_sm']) ? $its[$i]['pic_sm'] : 'img/no-pic.png') : 'img/no-pic.png' ) ).'" width="80" height="80"></td>
				<td class="name"> 
					<div class="p-tit"><a href="'.$this->Page_BuildUrl("proj", "view/".$its[$i]['item_id']).'">'.$its[$i]['title2'].'</a></div>
					<div class="p-sums">'.$progress_str.'</div>
					'.( ($its[$i]['amount_type'] == PROJ_TYPE_MONEY) || ($its[$i]['amount_type'] == PROJ_TYPE_HUMAN) ? '<div class="p-progress"><img src="'.WWWHOST.'img/rateorg-'.(round(($confprog_percnt > 100 ? 100 : $confprog_percnt) / 10.0)).'.png" alt=""></div>' : '' ).'					
				</td>
				<td>'.$req_stat_html.'</td>			
				<td>'.( $in_msg_new > 0 ? '<span class="badge">+'.$in_msg_new.'</span>' : ' - ' ).'</td>
				<td>
					<div class="p-tm">
						<span><img src="'.WWWHOST.'img/progress-act.png" width="'.(ceil($tmprog_percnt * 1.2)).'" height="8" alt=""></span>
						<br>
						'.$tmleft_str.'
					</div>
				</td>
				<td>';
				
				$star_review = '';

			if($this->viewMode == "getlist") {

				switch ($its[$i]['req_status']) {
					case REQ_STATUS_CONFIRM:
						$myrevs = $this->catmodel->Req_ReviewList($its[$i]['id'], $this->UserId);
						if (count($myrevs) > 0) {
							$star_review = $this->localize->get("cabreq", "rate-val") . ": " . ($myrevs[0]['rate'] >= 8 ? '<b>' . $myrevs[0]['rate'] . '</b>' : $myrevs[0]['rate']) . '<br><span class="p-rev-dt">' . $myrevs[0]['add_date'] . '</span>';
						}
						break;
				}

				//$author_rate = $this->catmodel->Buyer_ReqStarsRate($its[$i]['author_id']);
				//echo $its[$i]['item_id'];

				if (($its[$i]['req_status'] == REQ_STATUS_CONFIRM)) {
					echo($star_review != "" ?
						$star_review :
						'<a href="#" data-proj="' . $its[$i]['item_id'] . '" data-reqit="' . $its[$i]['id'] . '" class="btn btn-info starlnk"><span class="glyphicon glyphicon-star"></span> ' . $this->localize->get("cabreq", "btn-rate") . '</a>'
					);
				}
			}
				
				
			 if($promise == 1) { ?>
				 <button class="btn btn-success btnmyhelp" onclick="popWnd('projpayment', 'projid=' + <?=$its[$i]['item_id']?>+'&sum='+ <?=$sum_to_pay?>+'&type=cab'+'&prom='+<?=($promise)?>);  yaCounter34223620.reachGoal('PushButton_fulfill_promise'); return true;">Выполнить обещание</button>

						<?php /*?>
					<form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
						<input type="hidden" name="ik_co_id" value="561555503d1eafe0518b4567"/>
						<input type="hidden" name="ik_pm_no" value="ID_1_<?= $its[$i]['item_id'] ?>_<?= $this->UserId ?>"/>
						<input type="hidden" name="ik_am" id="ik_am_inp"
							   value="<?= $promSum ?>"/><!--  100.0  -->
						<input type="hidden" name="ik_cur" id="ik_cur"
							   value="<?= ($currency_data[$projinfo['currency_id']]['name']); ?>"/><!-- UAH -->
						<input type="hidden" name="ik_desc" value="Благотворительный взнос"/>
						<input type="hidden" name="ik_exp" value="<?= date("Y-m-d", time() + 24 * 3600); ?>"/>
						<input type="hidden" name="ik_ia_u" value="http://wayofhelp.com/payscallback/ikinteract/"/>
						<input type="hidden" name="ik_ia_m" value="post"/>
						<input type="hidden" name="ik_suc_u" value="<?= $this->Page_BuildUrl("cabinet", "" ); ?>"/>
						<input type="hidden" name="ik_suc_m" value="get"/>
						<input type="hidden" name="ik_fal_u" value="<?= $this->Page_BuildUrl("cabinet", "" ); ?>"/>
						<input type="hidden" name="ik_fal_m" value="get"/>
						<!--<input type="hidden" name="ik_int" value="web" />
						<input type="hidden" name="ik_am_t" value="payway" />-->
						<input type="submit" class="btn btn-success" value="Оплатить">
					</form>
				<?php */ } else if(!$promise && $its[$i]['promise_pay'] == 2 ) { ?>
							<button class="btn btn-success btnmyhelp " onclick="popWnd('projpayment', 'projid=' + <?=$its[$i]['item_id']?>+'&sum='+ <?=$sum_to_pay?>+'&type=cab'+'&prom=1&dis=1');" ><?=$this->localize->get("myprojects", "waiting");?></button>
			<?php }
				 echo '</td>
			</tr>';
		}
		
		if( count($its) == 0 )
		{
			if( $this->filtby == "running" )		$noitems_str = $this->localize->get("cabdohelp", "tbl-noreq-running");
			else if( $this->filtby == "finished" )	$noitems_str = $this->localize->get("cabdohelp", "tbl-noreq-finished");
			else									$noitems_str = $this->localize->get("cabdohelp", "tbl-noreq");
			
			echo '<tr><td colspan="6" class="noitems">
				<div class="tbl-noitems">
					<div class="tbl-noitems-txt"><div><span>'.$noitems_str.'</span></div></div>
					<div class="tbl-noitems-face"></div>
				</div>
			</td></tr>';
		}
	?>
		</table>
	</div>
		
	<?php
		$PAGES_NUM = ceil($this->projlist_total / $this->pn);
		
		//echo $this->projlist_total.":".$this->pn.":".$PAGES_NUM."<br>";
		
		if( $PAGES_NUM > 1 )
		{		
	?>
	<div class="proj-pages">
		<nav class="navbar-right">
			<ul class="pagination">
	<?php
			for( $i=1; $i<=$PAGES_NUM; $i++)
			{
				echo '<li'.( $this->pi == ($i-1) ? ' class="active"' : '' ).'><a href="'.$this->Page_BuildUrl("cabinet", "dohelp").($this->filtby != "" ? 'filt_'.$this->filtby.'/' : '').($i>1 ? 'p_'.$i.'/' : '').'">'.$i.'</a></li>';
			}
			/*
	?>
			<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
			<li class="active"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
			*/
	?>
			</ul>
		</nav>
		<div class="clearfix"></div>
	</div>
<?php
		}
?>
<script>
$(document).ready(function(){
	$("a.starlnk").bind("click", function(){
		var dopreq = "projid=" + $(this).attr("data-proj") + "&reqid=" + $(this).attr("data-reqit");

		popWnd('starratedlg', dopreq);
	});
});
</script>
<?php
	}
	else if( $this->viewMode == "reqlist" )
	{
		$its = $this->reqlist;
?>
		<div class="proj-flt">
			<div class="row">
				<div class="col-xs-12 col-md-12 proj-flt-col">
					<div class="btn-group" role="group" aria-label="<?=$this->localize->get("cabreq", "flt-hdr");?>">
						<a href="<?=$this->Page_BuildUrl("cabinet", "reqlist/".$this->projinfo['id']);?>" class="btn btn-default<?=($this->filtby == "" ? ' active' : '');?>"><?=$this->localize->get("cabreq", "flt-all");?></a>
						<a href="<?=$this->Page_BuildUrl("cabinet", "reqlist/".$this->projinfo['id']);?>filt_new/" class="btn btn-default<?=($this->filtby == "new" ? ' active' : '');?>"><?=$this->localize->get("cabreq", "flt-new");?></a>
						<a href="<?=$this->Page_BuildUrl("cabinet", "reqlist/".$this->projinfo['id']);?>filt_confirmed/" class="btn btn-default<?=($this->filtby == "confirmed" ? ' active' : '');?>"><?=$this->localize->get("cabreq", "flt-confirm");?></a>
						<a href="<?=$this->Page_BuildUrl("cabinet", "reqlist/".$this->projinfo['id']);?>filt_declined/" class="btn btn-default<?=($this->filtby == "declined" ? ' active' : '');?>"><?=$this->localize->get("cabreq", "flt-decline");?></a>
					</div>
				</div>
			</div>
		</div>
<?php
		if( isset($this->msg) && ($this->msg != "") ) {
			echo '<div class="frm-notify-msg">'.$this->msg.'</div>';
		} 
		
		$tbl_h_lbl = $this->localize->get("cabreq", "tbl-pers");
		if( $this->projinfo['profile_id'] == PROJ_THINGS )
			$tbl_h_lbl = $this->localize->get("cabreq", "tbl-persget");
		else if( $this->projinfo['profile_id'] == PROJ_SENDHELP )
			$tbl_h_lbl = $this->localize->get("cabreq", "tbl-persmy");
?>		
		<div class="proj-tbl">
			<table>
			<tr>
				<th colspan="2" class="name"><?=$tbl_h_lbl;?></th>
				<th><?=( $this->projinfo['profile_id'] == PROJ_THINGS ? $this->localize->get("cabreq", "tbl-getthing") : $this->localize->get("cabreq", "tbl-help") );?></th>
				<th><?=$this->localize->get("cabreq", "tbl-status");?></th>
				<th><?=$this->localize->get("cabreq", "tbl-func");?></th>
			</tr>
		<?php
			for( $i=0; $i<count($its); ++$i ) {

				//$confirmed = $this->catmodel->Item_ReqCollected($its[$i]['item_id'], REQ_STATUS_CONFIRM);			
				//$confprog_percnt = $this->catmodel->PercentDone($its[$i]['amount'], $confirmed['sum']);
					
				// Показать отображение для проекта по сбору средств

				if( $its[$i]['req_type'] == PROJ_TYPE_MONEY ) {

					if($its[$i]['is_promise'] == REQ_PROMISE && $its[$i]['promise_pay'] == REQ_PRIMISE_NOPAY)
						$req_amount = $this->localize->get("cabreq", "help-moneyprom").': <b>'.number_format($its[$i]['req_amount'], 2, ".", " ").'</b><br>';

					else if($its[$i]['is_promise'] == REQ_PROMISE && $its[$i]['promise_pay'] == 2)
						$req_amount = $this->localize->get("cabreq", "help-moneypromcheck").': <b>'.number_format($its[$i]['req_amount'], 2, ".", " ").'</b><br>';

					else
						$req_amount = $this->localize->get("cabreq", "help-money").': <b>'.number_format($its[$i]['req_amount'], 2, ".", " ").'</b><br>';
				}

				else {
					$req_amount = $its[$i]['req_amount']." ".$this->localize->human;
				}			

				$star_review = ''; 
				
				$req_stat_html = '';

				$btn_type = "";

				switch( $its[$i]['req_status'] ) {
					case REQ_STATUS_CONFIRM:
						if($its[$i]['is_promise'] == REQ_PROMISE && $its[$i]['promise_pay'] == REQ_PRIMISE_NOPAY) {
							$btn_type = 1;
							$req_stat_html .= '<span class="p-stat-new">'.$this->localize->get("cabreq", "stat-wait").'</span>';
						} else if($its[$i]['is_promise'] == REQ_PROMISE && $its[$i]['promise_pay'] == 2) {
							$btn_type = 2;
							$req_stat_html .= '<span class="p-stat-ok woh-green">' . $this->localize->get("cabreq", "stat-wait") . '</span>';
						}else
							$req_stat_html .= '<span class="p-stat-ok">'.$this->localize->get("cabreq", "stat-confirm").'</span>';

						// Проверить, размещен ли отзыв на эту помощь
						$myrevs = $this->catmodel->Req_ReviewList($its[$i]['id'], $this->UserId);

						if( count($myrevs) > 0 ) {
							$star_review = $this->localize->get("cabreq", "rate-val").": ".($myrevs[0]['rate'] >= 8 ? '<b>'.$myrevs[0]['rate'].'</b>' : $myrevs[0]['rate']).'<br><span class="p-rev-dt">'.$myrevs[0]['add_date'].'</span>';
						}

						break;
					case REQ_STATUS_NEW:		$req_stat_html .= '<span class="p-stat-new">'.$this->localize->get("cabreq", "stat-wait").'</span>';	break;
					case REQ_STATUS_DECLINE:	$req_stat_html .= '<span class="p-stat-decl">'.$this->localize->get("cabreq", "stat-decline").'</span>';	break;
				}

				$author_rate = $this->catmodel->Buyer_ReqStarsRate($its[$i]['author_id']);

				//echo $its[$i]['item_id'];

				echo '<tr>
					<td><img src="'.( WWWHOST.( $its[$i]['pic_sm'] != "" ? (file_exists($its[$i]['pic_sm']) ? $its[$i]['pic_sm'] : 'img/no-pic.png') : 'img/no-pic.png' ) ).'" width="80" height="80"></td>
					<td class="name"> 
						<div class="p-tit"><a href="'.$this->Page_BuildUrl("users", "viewrev/".$its[$i]['author_id']).'">'.$its[$i]['name'].' '.$its[$i]['fname'].'</a></div>
						'.( false ? '<div class="p-sums">'.$progress_str.'</div>' : '' ).'
						<div class="p-progress"><img src="'.WWWHOST.'img/rateorg-'.round($author_rate['avgrate']).'.png" alt="'.$this->localize->get("myprojects", "rating").' '.number_format($author_rate['avgrate'], 2, ".", "").'"></div>
						<div class="p-comment">'.$its[$i]['comments'].'</div>					
					</td>
					<td>'.$req_amount.'<div class="p-reqdt">'.$its[$i]['dtpost'].'</div></td>
					<td>'.$req_stat_html.'</td>			
					<td>'.($btn_type == 1 ? 'Ожидает сбора средств' : ($btn_type == 2 ? '<form action="'.$this->Page_BuildUrl('cabinet','promaccept').'" method="post"><input type="hidden" name="pid" value="'.$its[$i]['item_id'].'" /><input type="hidden" name="uid" value="'.$its[$i]['sender_id'].'" /><input type="submit" value="'.$this->localize->get("myprojects", "accept").'" class="btn btn-primary" /></form>' : ( $its[$i]['req_status'] == REQ_STATUS_NEW ? '
						<a href="'.$this->Page_BuildUrl("cabinet", "reqaccept/".$this->projinfo['id']."/".$its[$i]['id']).'" class="btn btn-primary"><span class="glyphicon glyphicon-thumbs-up"></span> '.$this->localize->get("cabreq", "btn-accept").'</a> 
						<a href="'.$this->Page_BuildUrl("cabinet", "reqdecline/".$this->projinfo['id']."/".$its[$i]['id']).'" class="btn btn-danger"><span class="glyphicon glyphicon-thumbs-down"></span> '.$this->localize->get("cabreq", "btn-decline").'</a>
						' : ( ($its[$i]['req_status'] == REQ_STATUS_CONFIRM) && ($this->projinfo['profile_id'] != PROJ_SENDHELP) ? 
								( $star_review != "" ? 
										$star_review : ($its[$i]['sender_id'] > -1 ? '<a href="#" data-proj="'.$this->projinfo['id'].'" data-reqit="'.$its[$i]['id'].'" class="btn btn-info starlnk"><span class="glyphicon glyphicon-star"></span> '.$this->localize->get("cabreq", "btn-rate").'</a>' : '' )
								) : 
								''
							) ) ) ).' 
					</td>
				</tr>';
			}			
		?>	
			</table>
		</div>
		<?php
		/*
		?>
		<div class="proj-pages">
			<nav class="navbar-right">
				<ul class="pagination">
				<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
				<li class="active"><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
				</ul>
			</nav>
			<div class="clearfix"></div>
		</div>
		*/
		?>
<script>
$(document).ready(function(){
	$("a.starlnk").bind("click", function(){
		var dopreq = "projid=" + $(this).attr("data-proj") + "&reqid=" + $(this).attr("data-reqit");

		popWnd('starratedlg', dopreq);
	});

});
</script>
<?php	
	}
?>
	
	<br><br>
</div>
</div>

<script>
	$(document).ready(function () {

		$.ajax({
			type: "POST",
			url: req_ajx_host + "ajx/updtab/",
			data: "tab=" + '<?=$this->viewMode?>'
		});

		Share = {
			vkontakte: function(purl, ptitle, text) {
				url  = 'http://vkontakte.ru/share.php?';
				url += 'url='          + purl;
				url += '&title='       + encodeURIComponent(ptitle);
				url += '&description=' + encodeURIComponent(text);
				Share.popup(url);
			},
			odnoklassniki: function(purl, text) {
				url  = 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1';
				url += '&st._surl='    + encodeURIComponent(purl);
				url += '&st.comments=' + encodeURIComponent(text);
				Share.popup(url);
			},
			facebook: function(link, title, descr) {
				var facebook_appID = <?=FB_APP_ID?>;
				url = "https://www.facebook.com/dialog/feed?app_id="+ facebook_appID +    "&link=" + encodeURIComponent(link)+
					"&name=" + encodeURIComponent(title) +
					"&description=" + encodeURIComponent(descr) +
					"&redirect_uri=https://www.facebook.com";
				Share.popup(url);
			},

			popup: function(url, soc) {
				window.open(url,'','toolbar=0,status=0,width=626,height=436');
			}
		};

		var uId = "<?=$this->UserId?>";
		var uCount = "<?=$uinf['video_check']?>";
		var post_req_str = "id=" + uId + "&co=" + uCount;

		if(uCount > -1) {
			$.ajax({
				type: "GET",
				url: req_ajx_host + "ajx/addCounter/",
				data: post_req_str,
				dataType: "json",
				success: function (data) {
				}
			});
		}

	});
</script>