<?php
    $lnk = ( isset($_GET['lnk']) ? $_GET['lnk'] : '' );

    $UserId = UhCmsApp::getSesInstance()->UserId;
    $its = $this->catLib->getItemGame();
    $its_count = ceil(count($its)/5);
    $dateList = $this->catLib->getGameDateAll();
?>
<script type="text/javascript" src="<?=WWWHOST?>js/jquery.bootpag.min.js"></script>
    <select name="dat" id="sldat">
        <option disabled selected="selected" value="last">Дата</option>
        <option value="now">Сегодня</option>
        <?php foreach ($dateList as $date) {
            $datetime = new DateTime($date['date']);
            $datetime->modify('-1 day') ?>
            <option value="<?=$date['date']?>"><?=$datetime->format('Y-m-d')?></option>
        <?php } ?>
    </select>
<div id="results"></div>
<div id="post-results"></div>
<div class="pagination pagination-curr"></div>
<script>
    $(document).ready(function () {

        var lnk = '<?=$lnk?>';

        $("#results").prepend('<div class="loading-indication"><img src="<?=WWWHOST?>img/ajax-loader.gif" /> Loading...</div>');


        $("#results").load(req_ajx_host+"ajx/getgameproj/");  //initial page number to load
        
        $(".pagination").bootpag({
            total: <?=$its_count?>,
            page: 1,
            maxVisible: 5
        }).on("page", function(e, num){
            $("#results").prepend('<div class="loading-indication"><img src="<?=WWWHOST?>img/ajax-loader.gif" /> Loading...</div>');
            $("#results").load(req_ajx_host+"ajx/getgameproj/?num="+num);
        });

        if(lnk) {
            $(".wnd-close a").bind("click", function () {
                popWnd('infopopup', '');
                return 0;
            });
        }

            $("#sldat").change(function(){
                $("#results").prepend('<div class="loading-indication"><img src="<?=WWWHOST?>img/ajax-loader.gif" /> Loading...</div>');
                $("#results").load(req_ajx_host+"ajx/getgameproj/?date="+$(this).val());
                $(".pagination").bootpag({
                    total: <?=$its_count?>,
                    page: 1,
                    maxVisible: 5
                }).on("page", function(e, num){
                    $("#results").prepend('<div class="loading-indication"><img src="<?=WWWHOST?>img/ajax-loader.gif" /> Loading...</div>');
                    $("#results").load(req_ajx_host+"ajx/getgameproj/?date="+$( "#sldat option:selected" ).val()+"&num="+num);
                });
            });

    });
</script>