<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

?>
<div class="wnd-hdr"><?=$this->localize->get("pophelp2", "hdr");?></div>
<div class="pophelp2-cont"><div class="pophelp2-cont-human">
	<div class="pophelp2-txt">
		<?=( $PM->get_txtres()->poppromo3['text'] );?>
		<?php
			/* 
		Будь активным, помогай другим и тогда твое дерево добра будет расти!
		<br> 
		<img src="<?=IMGHOST;?>img/pop-promo-tree.gif" class="img-responsive" alt="Дерево взаимопомощи">
		<br>
		Самые активные пользователи попадут на:
		<br>
		<img src="<?=IMGHOST;?>img/pop-promo-icons.png" alt="">
		<br>
			*/
		?>
	</div>
	<div class="pophelp2-btns">
		<a href="<?=$this->Page_BuildUrl("cat", "fulfill-dream");?>" class="btn btn-border btn-big1"><?=$this->localize->get("pophelp2", "btn-dream");?></a>
		
		&nbsp;
		
		<a href="<?=$this->Page_BuildUrl("cat", "");?>" class="btn btn-dohelp btn-big1"><?=$this->localize->get("pophelp2", "btn-dohelp");?></a>
		
		&nbsp;
		
		<a id="popneedhelplnk2" href="<?=$this->Page_BuildUrl("proj", "add");?>" class="btn btn-needhelp btn-big1"><?=$this->localize->get("pophelp2", "btn-gethelp");?></a>
	</div>
</div></div>
<script>
$(document).ready(function(){
	$("#popneedhelplnk2").bind("click", function(){
		if( usr_logged == 1 )
			return true
		
		popWnd('logdlg', '');
		return false
	});	
});
</script>

