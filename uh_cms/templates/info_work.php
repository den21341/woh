<?php

$PM = $this->pageModel;

$page = $PM->get_page();

$PAGE_H1 = $page->title;

$BCHTML = $this->renderBreadcrumbs($PAGE_H1);
?>

<div class="row-head breacrumb-bgi">
    <div class="container">
        <ol class="breadcrumb">
            <?=$BCHTML;?>
        </ol>
        <h1><?=$this->localize->get("info-work", "how-it-work")?></h1>
    </div>
</div>
<div class="background-info-work">

    <div class="container">
        <div class="row">
            <p class="text-work-makehelp" >
                <?=$this->localize->get("info-work", "help-anyone")?>
            </p>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="work-line-l img-four-position">
                    <div class="col-md-3 col-lg-3 col-xs-3">
                        <div class="img-work-four-1 img-work-four-size img-work-padding"></div>
                        <div><?=$this->localize->get("info-work", "fizical")?></div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3">
                        <div class="img-work-four-2 img-work-four-size img-work-padding"></div>
                        <div><?=$this->localize->get("info-work", "fizical")?></div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3">
                        <div class="img-work-four-3 img-work-four-size img-work-padding"></div>
                        <div><?=$this->localize->get("info-work", "fizical")?></div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-3">
                        <div class="img-work-four-4 img-work-four-size img-work-padding"></div>
                        <div><?=$this->localize->get("info-work", "fizical")?></div>
                    </div>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="work-line-l">
                    <div class="img-work-arrows"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="work-line-l">
                    <div class="text-work-baz"><?=$this->localize->get("info-work", "db-requests")?></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="work-line-tv-l">
                    <div class="col-md-3 col-lg-4 col-xs-3">
                        <div class="img-work-arrow-left"></div>
                        <div class="text-work-right-1"><?=$this->localize->get("info-work", "im")?></div>
                        <div class="text-work-right-2"><?=$this->localize->get("info-work", "my-tree")?></div>
                    </div>

                    <div class="col-md-6 col-lg-4 col-xs-6">
                        <div class="img-work-monitor"></div>
                    </div>

                    <div class="col-md-3 col-lg-4 col-xs-3">
                        <div class="img-work-arrow-right"></div>
                        <div class="text-work-left-1"><?=$this->localize->get("info-work", "couch")?></div>
                        <div class="text-work-left-2"><?=$this->localize->get("info-work", "consult")?></div>
                        <div class="text-work-left-3"><?=$this->localize->get("info-work", "surgery")?></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="work-position-text">
                    <div class="work-line-l">
                        <div class="col-md-6 col-lg-6 col-xs-6">
                            <div class="btn work-btn-dohelp work-btn-def btn-info-work-do ">
                                <div class="col-md-3">
                                    <a href="<?=WWWHOST?>cat/">
                                        <img src="<?=WWWHOST?>img/work-button-hand.png" alt="<?=$this->localize->get("info-work", "make-help")?>" title="<?=$this->localize->get("info-work", "make-help")?>">
                                </div>
                                <div class="col-md-9">
                                    <p><?=$this->localize->get("info-work", "making-help")?></p>
                                </div></a>
                            </div>
                            <ul class="info-work-ul-green">
                                <p class="info-work-text-li"><li class="info-work-li"><?=$this->localize->get("info-work", "good-deeds")?></li></p>
                                <p class="info-work-text-li"> <li class="info-work-li"><?=$this->localize->get("info-work", "status-up")?> <img class="work-ang-img" src="<?=WWWHOST?>img/work-angel-gold.png" alt="<?=$this->localize->get("info-work", "angel")?>"></li></p>
                                <p class="info-work-text-li"><li class="info-work-li"><?=$this->localize->get("info-work", "shows-tree")?></li></p>
                            </ul>
                        </div>

                        <div class="col-md-6 col-lg-6 col-xs-6">
                            <div class="btn work-btn-needhelp work-btn-def btn-info-work-make">
                                <div class="col-md-3">
                                    <a href="<?=WWWHOST?>proj/add/">
                                        <img src="<?=WWWHOST?>img/work-button-ball.png" alt="<?=$this->localize->get("info-work", "need-help")?>" title="<?=$this->localize->get("info-work", "need-help")?>">
                                </div>
                                <div class="col-md-9">
                                    <p><?=$this->localize->get("info-work", "take-help")?></p>
                                </div></a>
                            </div>
                            <ul class="info-work-ul-red">
                                <p class="info-work-text-li"><li class="info-work-li"><?=$this->localize->get("info-work", "take-help-deeds")?></li></p>
                                <p class="info-work-text-li"><li class="info-work-li"><?=$this->localize->get("info-work", "make-dreams")?></li></p>
                                <p class="info-work-text-li"><li class="info-work-li"><?=$this->localize->get("info-work", "influence")?></li></p>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="background-info-work-down">

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="img-work-triangle"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="img-work-tree-down">
                    <div class="text-work-tree-me"><?=$this->localize->get("info-work", "im")?></div>
                    <div class="text-work-tree-mytree"><?=$this->localize->get("info-work", "mytree-help")?></div>
                </div>
                <div class="img-work-arrow-down">
                    <div class="text-work-arrow-good"><?=$this->localize->get("info-work", "potential-good")?></div>

                </div>

            </div>
        </div>

        <div class="col-md-12">

            <div class="txt-work-tree-nvg"><?=$this->localize->get("info-work", "want")?></div>

        </div>

    </div>
</div>