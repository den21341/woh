<?php

$PM = $this->pageModel;

$result = ( isset($_GET['res']) ? $_GET['res'] : 0 );
$hlp_co =  ( isset($_GET['reqco']) ? $_GET['reqco'] : 0 );
$hlp_co = ( ($hlp_co - 1) < 0 ? 0 : $hlp_co - 1 );

?>

<?php if($result == 1) { ?>

    <div class="wnd-tree" style="text-align: center; font-size: 16pt; margin-top: 59px; line-height: 1.4;">
        <div class="cab-graph cab-new-graph">
            <div class="bon-bonus-loose">

                <div id="parent">
                    <div id="child" class="child-1-loose"><img src="<?=WWWHOST?>img/bon-loose.png"></div>
                </div>

                <div id="parent">
                    <div id="child2" class="child-2-loose"><?=$this->localize->get('getbonus','no-more-try')?></div>
                </div>
            </div>
        </div>
    </div>

<?php } if($result == 2) { ?>

    <div class="wnd-tree" style="text-align: center; font-size: 16pt; margin-top: 59px; line-height: 1.4;">
        <div class="cab-graph cab-new-graph">
            <div class="bon-bonus-loose">

                <div id="parent">
                    <div id="child" class="child-1-loose"><img src="<?=WWWHOST?>img/bon-winner.png"></div>
                </div>

                <div id="parent">
                    <div id="child2" class="child-2-loose"><?=$this->localize->get('getbonus','winner')?></div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php if($result == 3) { ?>

    <div class="wnd-tree" style="text-align: center; font-size: 16pt; margin-top: 59px; line-height: 1.4;">
        <div class="cab-graph cab-new-graph">
            <div class="bon-bonus-loose">

                <div id="parent">
                    <div id="child" class="child-1-loose"><img src="<?=WWWHOST?>img/bon-loose.png"></div>
                </div>

                <div id="parent">
                    <div id="child2" class="child-2-loose"><?=$this->localize->get('getbonus','lose')?>
                        <span class="woh-blue"><?=$this->localize->get('getbonus','unused-chance')?><b><?=$hlp_co?></b></span><br>
                        <?=$this->localize->get('getbonus','more-goodjob')?>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>
