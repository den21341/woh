<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$nologlnk = ( isset($_GET['nologlnk']) ? $_GET['nologlnk'] : 0 );

	/*
	// Facebook
	$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);
		
	$FB_REDIRECT_URL = WWWHOST.'login/redirfb';
	$FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);
	
	// Vkontakte
	$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);
	
	$VK_REDIRECT_URL = WWWHOST.'login/redirvk';
	$VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);
	
	// Odnoklassniki
	$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);
	
	$OK_REDIRECT_URL = WWWHOST.'login/redirok';
	$OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);
	*/		
?>
<div class="wnd-hdr"><?=$this->localize->get("poprest", "hdr");?></div>
<p><?=( $PM->get_txtres()->restorepass['text'] );?></p>
<div id="restoreerror" class="wnd-error"></div>
<form id="passrestfrm" action="<?=( $this->Page_BuildUrl("login", "restorepass") );?>" method="POST">
	<div class="row form-horizontal">
		<div class="col-xs-12 col-md-12">
			<div class="form-group">
				<label for="ulogin" class="col-sm-3 control-label"><?=$this->localize->get("poprest", "lbl-login");?></label>
				<div class="col-sm-9"><input type="email" class="form-control" id="ulogin" name="ulogin" placeholder="Email"></div>
			</div>
		</div>
	</div>	
	<div class="row form-horizontal">
		<div class="col-xs-12 col-md-12 ">
			<div class="form-group">
				<div class="col-sm-3 col-sm-offset-3"><button id="restorebtn" class="btn btn-primary"><span class="glyphicon glyphicon-user"></span> <?=$this->localize->get("poprest", "btn-send");?></button></div>
				<div class="col-sm-6 text-right"><?=( $nologlnk == 1 ? '' : '<a id="goto-loglnk" href="#">'.$this->localize->get("poprest", "dolog").'</a>' );?></div>
			</div>
		</div>
	</div>		
</form>
<script>
$("#restorebtn").on("click", function(){
	checkRestoreForm(<?=$this->localize->get("basicjs", "checklog-err")?>);
	return false
});

$("#goto-loglnk").on("click", function(){
	popWnd('logdlg','');
	return false;
});
</script>