<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$show_send_msg_lnk = false;

	////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();
	
	// Set page head title (h1 tag value)
	$PAGE_H1 = ( isset($this->seo[SEO_H1]) ? $this->seo[SEO_H1] : $page->title );	
	
	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);
	
	//if( isset($this->title) )
	//	$PAGE_H1 = $this->title;
?>
	<div class="row">
		<div class="top-block top-block-pr col-md-12 col-xs-12 col-lg-12">
			<div class="top-block-txt top-block-pr-txt">
				<h1>FAQ</h1>
			</div>
		</div>
	</div>
<div class="container des-cont-all faq-cont">
	<div class="info info-minheight text-center">
<?php
	if( $this->seo[SEO_TEXT1] != "" )
		echo $this->seo[SEO_TEXT1]."<br>";
	
	//echo '<div class="ratingTblRow">'.$this->build_RateTable().'</div>';
	
	echo '<div class="faq-list">';
	
	$its = $this->faqs;


$group_id = 0;
	for( $i=0; $i<count($its); $i++ )
	{
		$its[$i] = preg_replace('~style="[^"]*"~i', '', $its[$i]);
		if( $group_id != $its[$i]['group_id'] )
		{
			if( $i > 0 )
				echo '</ul></div>';
			
			echo '<div class="faq-group"><span>'.$its[$i]['grname'].'</span>
			<ul>';
			$group_id = $its[$i]['group_id'];
		}
		
		echo '<li>
			<div class="faq-it"><a href="#" data-faq="'.$its[$i]['id'].'">'.$its[$i]['title'].'</a></div>
			<div id="fit'.$its[$i]['id'].'" class="faq-it-i">'.$its[$i]['content'].'</div>
		</li>';
	}
	
	echo '</ul></div>
	</div>';
	
	
	if( $this->seo[SEO_TEXT2] != "" )
		echo $this->seo[SEO_TEXT2]."<br>";
?>
	</div>
</div>		
<?php	
	//}
?>