
<?php
$BCHTML = $this->renderBreadcrumbs();
?>

<div class="row">
    <div class="top-block top-block-pr col-md-12 col-xs-12 col-lg-12">
        <div class="container">
            <ol class="breadcrumb">
                <?=$BCHTML;?>
            </ol>
        </div>
        <div class="top-block-txt top-block-pr-txt">
            <h1><?=$this->localize->get("agreement", "header")?></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="container des-cont-all">
        <div class="fees-about conf-about">
            <p><?=$this->localize->get("agreement", "you-agree")?></p>
            <p><?=$this->localize->get("agreement", "any-sum")?></p>
            <p><?=$this->localize->get("agreement", "help")?></p>
            <p><?=$this->localize->get("agreement", "example")?></p>
            <p><?=$this->localize->get("agreement", "instruction")?></p>
        </div>
    </div>
</div>
