<?php
$uid = $this->uid;
$projid = $this->projid;
$val = $this->val;

$txt = str_replace('_defsum_', $this->def_sum[0], $this->localize->get("projprom", "prom-txt"));
$txt = str_replace('_cur_', $this->def_sum[1], $txt);
$txt = str_replace('_defsum1_', $this->def_sum[2], $txt);
$txt = str_replace('_allsum_', $this->to_pay['val'], $txt);
$txt = str_replace('_perc_', WOH_PERCENT_BY_PAY, $txt);
$txt = str_replace('_hr_', $this->Page_BuildUrl("info", "rules"), $txt);

?>

<div class="<?=($this->viewMode == 'toomany' ? "" : "promise-background-top")?>">
    <div class="container">
        <div class="row">
            <div class="proj-promise">
                <?php if($this->viewMode == 'toomany') { ?>
                <div class="frm-mess"><?=$txt?><div>
                        <p class="promise-msg-send promise-msg-tpl"><?=$this->localize->get("projprom", "phone-msgsend")?></p>
                        <p class="promise-msg-bad promise-msg-tpl"><?=$this->localize->get("projprom", "phone-notdone")?></p>
                        <p class="promise-msg-bad promise-msg-tpl"><?=$this->localize->get("projprom", "phone-errcode")?></p>
                        <form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
                            <input type="hidden" name="ik_co_id" value="561555503d1eafe0518b4567"/>
                            <input type="hidden" name="ik_pm_no" value="ID_4_<?= $projid ?>_<?= $uid ?>_<?= $val ?>"/>
                            <input type="hidden" name="ik_am" id="ik_am_inp" value="<?=round($this->to_pay['val']*(WOH_PERCENT_BY_PAY / 100), 2)?>" />
                            <input type="hidden" name="ik_cur" id="ik_cur"
                                   value="<?=$this->to_pay['type']?>"/><!-- UAH -->
                            <input type="hidden" name="ik_desc" value="Подтверждение оплаты"/>
                            <input type="hidden" name="ik_exp" value="<?= date("Y-m-d", time() + 24 * 3600); ?>"/>
                            <input type="hidden" name="ik_ia_u" value="http://wayofhelp.com/payscallback/ikinteract/"/>
                            <input type="hidden" name="ik_ia_m" value="post"/>
                            <input type="hidden" name="ik_suc_u" value="<?= $this->Page_BuildUrl("proj", "view").$projid?>/"/>
                            <input type="hidden" name="ik_suc_m" value="get"/>
                            <input type="hidden" name="ik_fal_u" value="<?= $this->Page_BuildUrl("proj", "view").$projid?>/"/>
                            <input type="hidden" name="ik_fal_m" value="get"/>
                            <div>
                                <input type="submit" style="width: 50%" class="btn btn-success center-block" value="Оплатить">
                            </div>
                        </form>
                    </div>
                    <?php } else { ?>

                        <div class="col-xs-6 col-md-6 col-sm-6">
                            <img src="<?=WWWHOST?>/img/buildings.png" class="buildings-promise" alt="Спасибо за помощь" title="Спасибо за помощь">

                        </div>

                        <div class="col-xs-6 col-md-6 col-sm-6">
                            <p class="promise-s-text"><?=$this->localize->get("projprom", "def-msg-s")?></p>
                            <br>
                            <p class="promise-o-text"><?=$this->localize->get("projprom", "def-msg-o")?></p>
                            <br>

                            <!--<a href="<?=$this->page_BuildUrl('proj','view').$projid.'/'?>">
                                <div class="col-md-5">
                                    <div class="btn promise-o-btn promise-btn-left">
                                        <div class="col-md-12">
                                            <p>Вернуться к проекту</p>
                                        </div>
                                    </div>
                                </div>
                            </a>-->

                            <a href="<?=$this->page_BuildUrl('cat').($this->to_p != '' ? 'p_'.$this->to_p.'/' : '')?>">
                                <div class="col-md-7">
                                    <div class="col-md-6 btn promise-s-btn promise-btn-right">
                                        <div class="col-md-12">
                                            <p>Вернуться к каталогу проектов</p>
                                        </div>
                                    </div>
                                </div>
                            </a>

                            <a onclick="popWnd('projpayment', 'projid=' + <?=$projid?>+'&sum='+ <?=$val?>+'&type=cab'+'&prom=1')" href="#">
                                <div class="col-md-5">
                                    <div class="btn promise-o-btn promise-btn-left">
                                        <div class="col-md-12">
                                            <p>Оплатить</p>
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
