<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$PM = $this->pageModel;

$projid = ( isset($_GET['projid']) ? $_GET['projid'] : 0 );

$UserId = UhCmsApp::getSesInstance()->UserId;
$UserGroup = UhCmsApp::getSesInstance()->UserGroup;
$sesid = UhCmsApp::getSesInstance()->sesid();

$check_bonus = $this->catLib->Bonus_Info($projid);

if($check_bonus['amount_used'] < $check_bonus['amount']) {

	/*
    ?>
    <div class="wnd-hdr"><?=$this->localize->get("popbonus", "hdr");?></div>
    <p>
    <?php
        echo $PM->get_txtres()->popgetbonus['text'];
    ?>
    </p>
    <div id="doerror" class="wnd-error"></div>
        <?php */
	/*
    <form id="frmaddfund" action="<?=( $this->Page_BuildUrl("bonus", "getbonus") );?>" method="POST">
    <input type="hidden" id="hmonprojid" name="selprojid" value="<?=$projid;?>">
    <?php
    */

	/*
    <div class="row form-horizontal">
        <div class="col-xs-12 col-md-12">
            <div class="form-group">
                <label for="upass" class="col-sm-3 control-label"><?=$this->localize->get("pophelp", "lbl-msg");?></label>
                <div class="col-sm-9"><textarea class="form-control" id="helptxt" name="helptxt" rows="4" placeholder="<?=$this->localize->get("pophelp", "ph-msg");?>"></textarea></div>
            </div>
        </div>
    </div>
    */
	?>
	<div class="wnd-tree">
		<div class="cab-graph cab-new-graph">
			<div class="bon-bonus-tree">
				<div id="parent">
					<div id="child" class="child-1">Забрать бонус</div>
				</div>

				<div id="parent">
					<div id="child" class="child-2"><img src="<?= WWWHOST ?>img/face-try.png"></div>
				</div>

				<div id="parent">
					<div id="child" class="child-3"><?= $this->localize->get('getbonus', 'take-bonus') ?></div>
				</div>

				<div id="parent">
					<div id="child" class="child-4"><?= $this->localize->get('getbonus', 'trys') ?></div>
				</div>

				<div id="parent">
					<div id="child" class="child-5"><a href="#" class="bonustree-try" id="bonustry1"></a><a href="#"
																											class="bonustree-try"
																											id="bonustry2"></a><a
							href="#" class="bonustree-try" id="bonustry3"></a></div>
				</div>


				<div id="bonus-tree" class="bon-game-tree"></div>
			</div>
		</div>
	</div>

	<?php
} else { ?>
	<div class="wnd-tree">
		<div class="cab-graph cab-new-graph">
			<div class="bon-bonus-tree">
				<div id="">
					<div style="text-align:center; padding-top: 20%; " class="child-1">Упс, бонус уже выиграли :(</div>
				</div>
			</div>
		</div>
	</div>
<?php }
?>
<script>
	$("#applybonusbtn").on("click", function() {
		if( checkAddhelpForm(['<?=$this->localize->get('basicjs','checkhlp-err-1')?>'], ['<?=$this->localize->get('basicjs','checkhlp-err-2')?>']) )
			$("#frmaddfund").submit();

		return false
	});

	loadBonusTree('<?=$UserId;?>', '<?=$projid;?>', '<?=$sesid;?>', 'bonus-tree');
</script>