<?php $BCHTML = $this->renderBreadcrumbs();	 ?>

<div class="row">
    <div class="top-block top-block-pr col-md-12 col-xs-12 col-lg-12">
        <div class="top-block-txt top-block-pr-txt">
            <div class="container">
                <ol class="breadcrumb">
                    <?=$BCHTML;?>
                </ol>
            </div>
            <h1><?=$this->pageInfo[0]['page_text']?></h1>
        </div>
    </div>
</div>
<div class="row"></div>
    <div class="container des-cont-all tree-cont">
        <div class="about-uhelp-a">
            <a href="<?=WWWHOST?>tree/obj_graph.php"><div class="about-uhelp-btn tree-uhelp"><?=$this->localize->get("info-tree", "see-clearly")?></div></a>
        </div>
        <div class="tree-block col-xs-12">
            <img src="<?=WWWHOST?>img/tree-img-1.png" alt="<?=$this->localize->get("info-tree", "tree")?> 1">
            <p class="tree-txt-1"><?=$this->pageInfo[1]['page_text']?></p>
        </div>
        <div class="tree-block tree-al-bottom tree-block-2 col-xs-12">
            <img src="<?=WWWHOST?>img/tree-img-2.png" alt="<?=$this->localize->get("info-tree", "tree")?> 2">
            <p><?=$this->pageInfo[2]['page_text']?></p>
        </div>
        <div class="tree-block tree-al-bottom  tree-block-3 col-xs-12">
            <img src="<?=WWWHOST?>img/tree-img-3.png" alt="<?=$this->localize->get("info-tree", "tree")?> 3">
            <p><?=$this->pageInfo[3]['page_text']?></p>
        </div>
        <div class="tree-block tree-al-bottom  tree-block-4 col-xs-12">
            <img src="<?=WWWHOST?>img/tree-img-8.png" alt="<?=$this->localize->get("info-tree", "tree")?> 4">
            <p><?=$this->pageInfo[4]['page_text']?></p>
        </div>
        <div class="tree-block tree-al-bottom  tree-block-5 col-xs-12">
            <img src="<?=WWWHOST?>img/tree-img-4.png" alt="<?=$this->localize->get("info-tree", "tree")?> 5">
            <div><?=$this->pageInfo[5]['page_text']?></div>
        </div>
        <div class="tree-block tree-al-bottom  tree-block-6 col-xs-12">
            <img src="<?=WWWHOST?>img/tree-img-5.png" alt="<?=$this->localize->get("info-tree", "tree")?> 6">
            <p><?=$this->pageInfo[6]['page_text']?></p>
        </div>
        <div class="tree-block tree-al-bottom  tree-block-7 col-xs-12">
            <img src="<?=WWWHOST?>img/tree-img-6.png" alt="<?=$this->localize->get("info-tree", "tree")?> 7">
            <p><?=$this->pageInfo[7]['page_text']?></p>
        </div>
        <div class="tree-block tree-al-bottom tree-block-8 col-xs-12">
            <img src="<?=WWWHOST?>img/tree-img-7.png" alt="<?=$this->localize->get("info-tree", "tree")?> 8">
            <p><?=$this->pageInfo[8]['page_text']?></p>
        </div>
    </div>
</div>