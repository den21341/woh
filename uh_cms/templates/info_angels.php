<?php


?>

<div class="row">
    <div class="container top-block top-block-pr">
        <div class="col-md-12 col-xs-12 col-lg-12">

            <h2 class="angel-head"><?=$this->localize->get("info-angels", "angel-day-week")?></h2>

        </div>
    </div>
</div>

<div class="row">
    <div class="container">

            <div class="col-md-12 col-xs-12 col-lg-12 angel-divt">
                <div class="col-md-2 col-xs-2 col-lg-2">
                    <img class="" src="<?=WWWHOST?>img/angel-info-day.png" title="<?=$this->localize->get("view", "ang-day")?>" alt="<?=$this->localize->get("view", "ang-day")?>">
                </div>

                <div class="col-md-10 col-xs-10 col-lg-10">
                <p class="angel-text"><?=$this->localize->get("info-angels", "ang-day")?></p>
                </div>
            </div>

            <div class="col-md-12 col-xs-12 col-lg-12 angel-divb">
                <div class="col-md-2 col-xs-2 col-lg-2">
                    <img class="" src="<?=WWWHOST?>img/angel-info-week.png" title="<?=$this->localize->get("view", "ang-week")?>" alt="<?=$this->localize->get("view", "ang-week")?>">
                </div>

                <div class="col-md-10 col-xs-10 col-lg-10">
                    <p class="angel-text"><?=$this->localize->get("info-angels", "ang-week")?></p>
                </div>
            </div>
    </div>
</div>


