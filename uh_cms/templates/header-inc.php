<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////
//$start = microtime(true);

$PM = $this->pageModel;

$LangId = $PM->getLangId();
$pname = $PM->getPageName();

$page = $PM->get_page();

$UserId = $PM->ses->UserId;

$newActivities = $PM->checkNewActivities();

$dop_tip_msg = '';
$box_tip_mess = '';
$dream_tip_mess = '';
//$commentsBox = '';
$its_success = '';
$checkPays = '';
$showTip = false;

//$start = microtime(true);

if( $UserId != 0 ) {
	$uinf = $this->catmodel->Buyer_Info($UserId);
	$pits0 = $this->catmodel->Item_List(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, $UserId, 0, 1, 3, "rate", PROJ_STATUS_RUN);

	$diff_date = $this->catmodel->getDiffProjDate($UserId);

	foreach ($diff_date as $itmdt) {
		$dop_tip_msg = 'Ваш проект скоро заканчивается. <br> Он будет автоматически <br> остановлен <br> <b>'.$itmdt['end_date'].'</b>';
	}

	$its_success = $this->catmodel->getSuccessProj($UserId);
	$tip_medal = '';
	//$this->catmodel->checkHelp($UserId, 1, 10);

	$boxData = $this->catmodel->getDateBox($UserId);
	
	$newDream = $this->catmodel->getLastDream($UserId);

	if($newDream)
		$dream_tip_mess = ' У нас на сайте появилась новая мечта <a href='.WWWHOST.'proj/view/'.$newDream.'/> оцените </a> ее значимость и<br>заработайте дополнительную<br>силу.<br>';
	/*if($checkPays) {
		if(!$checkPays[0]['is_active'])
			$dream_tip_mess = 'Вчера вы заработали <b>'.$checkPays[0]['local_currency'].'</b> баллов внутренней валюты.<br>Это соответствует <b>'.$checkPays[0]['pays_send'].'</b> гривен. <br> Для того, чтобы перевести эти деньги на свой проект нужно в течении <br><b> 3 дней </b> <a href='.$this->page_BuildUrl('cabinet','').' > синхронизироваться </a> с одной из социальных сетей. <br> Полную таблицу <br> распределения денег <br> <a href="#" id="btnshowproj">смотри тут</a>';
		else
			$dream_tip_mess = 'Вчера вы заработали <b>'.$checkPays[0]['local_currency'].'</b> баллов внутренней валюты. <br> Это соответствует <b>'.$checkPays[0]['pays_send'].'</b> гривен. <br> Деньги были переведены на Ваш проект. <br> Полную таблицу <br> распределения денег <br> <a href="#" id="btnshowproj">смотри тут</a>';
	}*/

	if($this->statusBox->getBox($boxData[0]['status']) <= $boxData[0]['money']) {
		//var_dump($this->statusBox->getStatus(($boxData[0]['status']+1)));
		$this->catmodel->setStatusBox($UserId, $boxData[0]['status']+1);
		$box_tip_mess = 'Вам присвоен новый статус <b>'.$this->statusBox->getStatus(($boxData[0]['status']+1)).'</b>!';
		$tip_medal = true;
		$this->catmodel->sendUserMailMsg_ct($UserId, $this->localize->get("mess", "usr-new-stattit"), 'За Вашу активность и неуклонную тягу к добрым делам,<br>
						Вам присвоен новый статус <b>'.$this->statusBox->getStatus(($boxData[0]['status']+1)).'</b>!!!
						<br><br>Благодаря Вам добрых дел становится больше!!!<br><br>
						
						Перейдите по <a href='.WWWHOST."cabinet/".'>ссылке</a> и поделитесь этой замечательной<br>
						новостью с друзьями.  
						');
	} else {
		$box_tip_mess .= 'До статуса <b>' . ($this->statusBox->getStatus(($boxData[0]['status'] + 1))) . '</b> не хватает <b>' . ($this->statusBox->getBox($boxData[0]['status']) - $boxData[0]['money']) . '</b>' . $this->statusBox->adaptCoin($this->statusBox->getBox($boxData[0]['status']) - $boxData[0]['money']) . '.<br>';
		$box_tip_mess .= '<a href='.$this->Page_BuildUrl("info/power").'> Как заработать ангелиты </a>';
		$box_tip_mess .= '<br>';
	}
	if(count($its_success)) {
		$dop_tip_msg = "<b>Поздравляем!</b> Ваш проект собрал <br> необходимую сумму. <br> Для того, чтоб получить деньги, <br>нужно <a id='trgproj'>поделиться</a> проектом в <br> своих соц. сетях.<br>";
	}
}
/*******/
//$time = microtime(true) - $start;
//printf('Скрипт выполнялся %.4F сек.', $time);

$showTip = ($box_tip_mess || $dop_tip_msg || $dream_tip_mess) ? true : false;

?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php
	/*
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    */
	?>
	<title><?=( $this->seo[SEO_TITLE] != "" ? $this->seo[SEO_TITLE] : $page->seo_title );?></title>
	<meta name="keywords" content="<?=( $this->seo[SEO_KEYW] != ""  ? $this->seo[SEO_KEYW] : $page->seo_keyw);?>" />
	<meta name="description" content="<?=( $this->seo[SEO_DESCR] != "" ? $this->seo[SEO_DESCR] : $page->seo_descr);?>" />
	<?=( $this->meta[META_NOINDEX] ? '<meta name="robots" content="noindex,nofollow" />' : ( $this->meta[META_NOFOLLOW] ? '<meta name="robots" content="index,nofollow" />' : '' ) );?>
	<?=( $this->meta[META_PREV] != "" ? '<link rel="prev" href="'.$this->meta[META_PREV].'" />' : '' );?>
	<?=( $this->meta[META_NEXT] != "" ? '<link rel="next" href="'.$this->meta[META_NEXT].'" />' : '' );?>
	<?=( $this->meta[META_CANONICAL] != "" ? '<link rel="canonical" href="'.$this->meta[META_CANONICAL].'" />' : '' );?>
	<?=( $this->seo[SEO_SOCIAL_TAGS] != "" ? $this->seo[SEO_SOCIAL_TAGS] : '' );?>
	<meta name="webmoney" content="0D303893-71E4-4B81-881E-4C3124D4E5EC">
	<link href="<?=WWWHOST?>favicon.ico" rel="icon" type="image/x-icon"/>
	<link href="<?=WWWHOST?>favicon.ico" rel="shortcut icon" type="image/x-icon"/>

	<!-- Bootstrap -->
	<link href="<?=WWWHOST?>css/bootstrap_zero.css" rel="stylesheet">

	<!-- project local -->
	<link href="<?=WWWHOST?>css/all.css?v=2.9.2" rel="stylesheet">
	<link href="<?=WWWHOST?>css/_all.css?v=0.5" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script src="<?=WWWHOST?>js/jquery-1.11.1.min.js"></script>
	<?php
	/*
    <!--<script src="<?=WWWHOST;?>js/angular.js"></script>
    <script src="<?=WWWHOST;?>js/angtodo.js"></script>-->
    */
	?>
	<script src="<?=WWWHOST?>js/cufon-yui.js"></script>
	<script src="<?=WWWHOST?>js/jquery-ui.min.js"></script>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '107875862961701');
		fbq('track', "PageView");
	</script>
	<noscript><img src="https://www.facebook.com/tr?id=107875862961701&ev=PageView&noscript=1" height="1" width="1" style="display:none" alt="Without js" ></noscript>
	<!-- End Facebook Pixel Code -->
	<?php
	if( isset($this->with_magnific_popup) && $this->with_magnific_popup ) {
		?>
		<link rel="stylesheet" type="text/css" href="<?=WWWHOST;?>js/magnif/magnific-popup.css" />
		<script type="text/javascript" src="<?=WWWHOST;?>js/magnif/magnifgalery.js"></script>
		<?php
	}

	if( isset($this->with_fancybox) && $this->with_fancybox ) {
		?>
		<link rel="stylesheet" type="text/css" href="<?=WWWHOST;?>js/fancybox/jquery.fancybox-1.3.2.css" />
		<script type="text/javascript" src="<?=WWWHOST;?>js/fancybox/jquery.fancybox-1.3.2.pack.js"></script>
		<?php
	}
	?>
	<script type="text/javascript">
		//Cufon.replace("ul.mainmenu li .hmmdm a", {fontFamily: "Robbcond"});
		Cufon.replace(".hdr-cuf", {fontFamily: "Ubjh"});

		var req_ajx_host = '<?=WWWHOST?>';
		var usr_logged = <?=($UserId == 0 ? '0' : '1')?>;
		var adapt_top = <?=($this->top_is_adaptive ? 'true' : 'false');?>;

		$(document).ready(function() {

			<?php if( $UserId != 0 ) { ?>

				if(sessionStorage.getItem('dontLoad') == null) { //

					var ch = 0;

					$.ajax({
						type: "GET",
						url: req_ajx_host + "ajx/upddt/",
						data: "",
						dataType: "json",
						success: function (data) {

							if(data[1]) {
								$('#bx_list').html(data[1]);
								showTip('popnotify-boxlist');
								ch = 1;
							}

							$.ajax({
								type: "GET",
								url: req_ajx_host + "ajx/runScript/",
								data: "uid=" + <?=$UserId?> + "&sum=" + data[0],
								dataType: "json",
								success: function (data) {
									if(!ch) {
										showTip('popnotify-next');
									}
									sessionStorage.setItem('dontLoad', 'true');
								}
							});
						}
					});

				} else if(<?=$newActivities['msgnum']?> || <?=$newActivities['msgnum']?> || <?=$newActivities['thingreqnum']?>) {
					if(window.location.pathname.match(/cabinet/g) == null) {
						showTip('popnotify-msg');
					}
				}
			<?php
				} if( isset($this->with_magnific_popup) && $this->with_magnific_popup ) {
			?>
				$('a[rel=picgalerylist]').magnificPopup({type:'image'});
			<?php
				} if( isset($this->with_fancybox) && $this->with_fancybox ) {
			?>

				$("a[rel=picgalerylist]").fancybox({
					overlayOpacity: 0.8,
					overlayColor: '#000',
					transitionIn: 'none',
					transitionOut: 'none',
					titlePosition: 'over',
					titleFormat: function(title, currentArray, currentIndex, currentOpts){
						return '<div id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</div>';
					}
				});

			<?php } ?>

		});
		var redirbackurl = location.href;

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-64704503-1', 'auto');
		ga('send', 'pageview');
		ga('require', 'ec');

		if('<?=$UserId?>' != 0) {
			ga('set', 'userId', '<?=$UserId?>'); // Задание идентификатора пользователя с помощью параметра user_id (текущий пользователь).
		}

	</script>
</head>
<body>
	<?php if(!$this->top_is_adaptive) { ?>
			<script>
				$("body").addClass("minhead");
			</script>
	<?php } ?>
<header>
	<nav class="navbar navbar-cust">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?=WWWHOST?>"><img class="img-responsive" src="<?=IMGHOST;?>img/wayofhelp-logo.png" alt="<?=$this->localize->get("all", "logo-alt");?>" /></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse donsp">

				<?php
					$uname_str = '&nbsp;';
					if( $UserId != 0 ) {
						//print_r($uinf);
						$uname_str = ''.$this->localize->get("all", "top-logas").': <a href="'.$this->Page_BuildUrl("users", "viewrev/".$UserId).'">'.( $uinf['account_type'] == USR_TYPE_PERS ? $uinf['name'].' '.$uinf['fname'] : $uinf['orgname'] ).'</a>'; ?>
						<div class="top-bl-user">
							<div class="top-bl-user-msg col-lg-1 col-md-1 col-xs-1">
								<a href="<?=$this->Page_BuildUrl("cabinet", "")?>"><div class="msg-pic"><div class="msg-pic-co"><?=($newActivities['msgnum']+$newActivities['reqnum']+$newActivities['thingreqnum']) > 0 ? $newActivities['msgnum']+$newActivities['reqnum']+$newActivities['thingreqnum'] : '0'?></div></div></a>
							</div>
							<div class="top-bl-user-info col-lg-10 col-md-10 col-xs-10">
								<div class="top-bl-user-info-pic col-lg-3 col-md-3 col-xs-3">
									<img src="<?=WWWHOST.($uinf['pic_sm'] ? $uinf['pic_sm'] : 'img/no-pic.png')?>" alt="Default picture">
								</div>
								<div class="top-bl-user-info-block col-lg-8 col-md-9 col-xs-11">
									<div class="top-u-name"><a href="<?=$this->Page_BuildUrl("cabinet", "")?>"><?=( $uinf['account_type'] == USR_TYPE_PERS ? $uinf['name'].' '.$uinf['fname'] : $uinf['orgname'] )?></a></div>
									<div class="top-u-exit"><a id="logoutlnk" rel="nofollow" href="<?=$this->Page_BuildUrl("login", "logout")?>?action=dologout"><span><?=$this->localize->get("all", "top-logout")?></span></a></div>
								</div>
							</div>
						</div>

				<?php } else {
					echo '<div class="top-bl-user">';

					if( isset($this->hide_login_btn_top) && $this->hide_login_btn_top ) {
						//hide login link
					}
					else {
						echo '<li><a id="loginlnk" rel="nofollow" href="'.$this->Page_BuildUrl("login", "").'"><span>'.$this->localize->get("all", "top-login").'</span></a></li>';
					}
					//echo '<li><a id="signinlnk" href="'.$this->Page_BuildUrl("buyerreg", "").'"><span>'.$this->localize->get("all", "top-reg").'</span></a></li>';
					echo '<li><a id="signinlnk" href="'.$this->Page_BuildUrl("registration", "person").'"><span>'.$this->localize->get("all", "top-reg").'</span></a></li>';
					echo '</div>';
				} ?>

				<div class="top-btn">
					<a href="<?=$this->Page_BuildUrl("cat","");?>" id="toplnkdohelp" class="btn btn-dohelp top-text-btn"><?=$this->localize->get("all", "top-btn-dohelp");?></a>
					<a href="<?=$this->Page_BuildUrl("proj","add");?>" id="toplnkgethelp" class="btn btn-needhelp top-text-btn"><?=$this->localize->get("all", "top-btn-needhelp");?></a>
				</div>
				<!--noindex--><div class="top-btn top-soc-btn">
					<a rel="nofollow" href="https://www.facebook.com/WayofHelp" target="_blank"><div class="top-soc-fb btn-soc"><div class="head-img-soc head-img-posfb" alt="Facebook logo"></div></div></a>
					<a rel="nofollow" href="https://vk.com/wayofhelp" target="_blank"><div class="top-soc-vk btn-soc"><div class="head-img-soc head-img-posvk" alt="Vk logo"></div></div></a>
					<a rel="nofollow" href="http://ok.ru/group/53895621509237" target="_blank"><div class="top-soc-ok btn-soc"><div class="head-img-soc head-img-posok" alt="Ok logo"></div></div></a>
				</div><!--/noindex-->

					<?php
						if( $UserId != 0 ) {
						$m_rate = abs($boxData[0]['user_rank_diff']);
						$m_rate_day = abs($boxData[0]['user_rank_day_diff']);
					?>

					<div class="main-topbutton topbtnfix">
						<a id="toplshow" class="btn btn-dohelp top-text-btn main-topbutton-color"><p class="main-topbutton-txt"><?=$boxData[0]['user_rank']?><?=!empty($pits0) ? ('/'.$pits0[0]['item_view']) : ''?>&emsp;&#9660;</p></a>
						<a id="toplhide" style="display: none" class="btn main-topbutton-color-white  main-topbutton-size"><?=$boxData[0]['user_rank']?><?=!empty($pits0) ? ('/'.$pits0[0]['item_view']) : ''?>&emsp;<span class="proj-subs">&#9650;</span></a>

						<div id="test" style="display: none">
							<div class="main-spoiler-color main-spoiler-value rehr-fix" style="<?=(empty($pits0)? 'height: 115px;' : '')?>">
								<div style="<?=(empty($pits0)? 'margin-top: 10px;' : '')?>display: inline-block">
								<div><p class="main-text-spoiler">Ваша позиция в рейтинге: <span class="main-num-color-1"><?=$boxData[0]['user_rank']?>&nbsp;&nbsp;</span> <div class="main-backg-arrow <?=($boxData[0]['user_rank_diff'] > 0 ? 'main-backg-arrposr' : 'main-backg-arrposg')?> main-text-spoiler"></div><span class="main-text-spoiler <?=($boxData[0]['user_rank_diff'] > 0 ? 'main-num-color-3' : 'main-num-color-2')?>"><?=$m_rate?></span></p></div>
								<a href="#" onclick="popWndsm('rateinfo', 'type=0')" class="main-btn-spoiler"><p>Увеличить рейтинг</p></a>
								</div>
								<!--<hr>
									<div class=""><p class="main-text-spoiler">Ваша позиция в дневном рейтинге: <span class="main-num-color-1"><?=$boxData[0]['user_rank_day']?>&nbsp;&nbsp;</span> <div class="main-backg-arrow <?=($boxData[0]['user_rank_day_diff'] < 0 ? 'main-backg-arrposr' : 'main-backg-arrposg')?> main-text-spoiler"></div><span class="main-text-spoiler <?=($boxData[0]['user_rank_day_diff'] < 0 ? 'main-num-color-3' : 'main-num-color-2')?>"><?=$m_rate_day?></span></p></div>
									<a href="#" onclick="popWnd('rateinfo', 'type=uratebyday')" class="main-btn-spoiler"><p>Увеличить рейтинг</p></a>-->
								<hr>
								<?php if(!empty($pits0)) { ?>
									<div class=""><p class="main-text-spoiler">Колличество показов вашего проекта: <span class="main-num-color-1"><?=$pits0[0]['item_view']?>&nbsp;&nbsp;</span> <div class="main-backg-arrow <?=($pits0[0]['item_view_diff'] < 0 ? 'main-backg-arrposr' : 'main-backg-arrposg')?> main-text-spoiler"></div><span class="main-text-spoiler <?=($pits0[0]['item_view_diff'] < 0 ? 'main-num-color-3' : 'main-num-color-2')?>"><?=abs($pits0[0]['item_view_diff'])?></span></p></div>
									<a href="#" onclick="popWndsm('rateinfo', 'type=0')" class="main-btn-spoiler"><p>Поднять колличество показов</p></a>
								<?php } ?>
							</div>
						</div>

					</div>
				<?php } ?>

			</div><!--/.nav-collapse -->
		</div>
	</nav>
	<div id="popnotify" class="tip-notify">
		<div class="popnotify-close a-close" onclick="<?php if(strip_tags($dream_tip_mess) != "") { ?> closePopNotify('popnotify-dream') <?php } else { ?> closePopNotify(0) <?php } ?>" ></div>
		<div>
			<p><?= $dop_tip_msg ?></p>
		</div>
	</div>
	<div id="popnotify-msg" class="tip-notify">
		<div class="popnotify-close a-close"></div>
		<div>
			<?php
			if( $newActivities['msgnum'] > 0 )
				echo '<p><a href="'.$this->Page_BuildUrl("cabinet", "msgview").'">Новых сообщений</a>: <span>'.$newActivities['msgnum'].'</span></p>';

			if( $newActivities['reqnum'] > 0 )
				echo '<p><a href="'.$this->Page_BuildUrl("cabinet", "needhelp").'">Новых предложений</a>: <span>'.$newActivities['reqnum'].'</span></p>';

			if( $newActivities['thingreqnum'] > 0 )
				echo '<p><a href="'.$this->Page_BuildUrl("cabinet", "givehelp").'">Новых запросов на вещи</a>: <span>'.$newActivities['thingreqnum'].'</span></p>';
			/*if($commentsBox['count'] > 0) {
				echo '<p> К вашему <a href='.$this->Page_BuildUrl("proj/view").$commentsBox['item_id'].'>проекту</a> оставили комментарий. '.'<p>';
			}*/
			?>
		</div>
	</div>
	<div id="popnotify-next" class="tip-notify <?=(isset($tip_medal) && $tip_medal) ? 'tip-notify-medal' : ''?>">

		<?php if( ($UserId != 0) && (($newActivities['msgnum'] > 0)  || ($newActivities['reqnum'] > 0) || ($dop_tip_msg != "")) ) { ?>
			<div class="popnotify-close a-close" onclick="<?php if(strip_tags($dop_tip_msg) != '' ) { ?> closePopNotify('popnotify') <?php }  ?>" ></div>
		<?php } else { ?> <div class="popnotify-close a-close" onclick="<?php if($dream_tip_mess) { ?> closePopNotify('popnotify-dream') <?php } else { ?> closePopNotify(0) <?php } ?>"></div> <?php } ?>

		<div> <?php if(isset($box_tip_mess)) echo '<p>'.$box_tip_mess.'</p>' ?> </div>
	</div>

	<div id="popnotify-boxlist" class="tip-notify">

		<?php if( ($UserId != 0) && (($newActivities['msgnum'] > 0)  || ($newActivities['reqnum'] > 0) || ($dop_tip_msg != "")) ) { ?>
			<div class="popnotify-close a-close" onclick="<?php if(strip_tags($dop_tip_msg) != '' ) { ?> closePopNotify('popnotify') <?php }  ?>" ></div>
		<?php } else { ?> <div class="popnotify-close a-close" onclick="<?php if($dream_tip_mess) { ?> closePopNotify('popnotify-dream') <?php } else { ?> closePopNotify(0) <?php } ?>"></div> <?php } ?>

		<div id="bx_list"></div>
	</div>

	<div id="popnotify-adder" class="tip-notify">
		<div class="popnotify-close a-close" onclick="closePopNotify(0);"></div>
	</div>

	<div id="popnotify-dream" class="tip-notify">
		<div class="popnotify-close a-close" onclick="closePopNotify(0);"></div>
		<div>
			<?php echo '<p>'.$dream_tip_mess.'</p>' ?>
		</div>
	</div>
</header>
<?php
	unset($best0);
	unset($boxData);
	unset($diff_date);
	unset($pits0);
	unset($newActivities);
?>
<script>
	$(document).ready(function () {
		<?php if($its_success) { ?>

			$("#trgproj").bind('click', function () {
				var post_req_str = 'projid=' + '<?=$its_success[0]['id']?>' + '&none=1';

				$.ajax({
					type: "GET",
					url: req_ajx_host + "cabinet/projsuccess/",
					data: post_req_str,
					dataType: "json",
					success: function (data) {}
				});

				document.location.href = req_ajx_host+'proj/view/'+'<?=$its_success[0]['id']?>';
			});

		<?php } ?>

		$(".popnotify-close").bind("click", function() {
			$(this).parent().removeClass("tip-notify-show");
		});

	});
</script>
<!--<a style="font-size: 25px; color: #2b2929; font-family: 'UbuntuBold'; margin-top: 10px;"  class="navbar-brand" href="<?=WWWHOST?>">TEST SERVER</a>-->