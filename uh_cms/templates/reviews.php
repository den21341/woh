<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$show_send_msg_lnk = false;

	////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();
	
	// Set page head title (h1 tag value)
	$PAGE_H1 = $page->title;
	
	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);	
?>
<div class="row-head breacrumb-bgi">
	<div class="container">
		<ol class="breadcrumb">
			<?=$BCHTML;?>
		</ol>
		<h1><?=$PAGE_H1;?></h1>
	</div>
</div>
<div class="index-resp">
	<div class="container">
<?php
	$rlist = $this->revlist; 
	
	for( $i=0; $i<count($rlist); $i++ )
	{
		echo '<div class="col-xs-12 col-md-4">
			<div class="iresp">
				<div class="iresp-pic"><span class="center-block"><img src="'.( $rlist[$i]['pic'] != "" ? WWWHOST.$rlist[$i]['pic'] : IMGHOST."img/no-pic.png").'" class="img-responsive"></span></div>
				<div class="iresp-txt">
					'.$rlist[$i]['rate_comments'].'
					<div class="iresp-auth hdr-cuf">'.$rlist[$i]['name'].' '.$rlist[$i]['fname'].'</div>
				</div>				
				<div class="iresp-lnk">
					<span>'.$this->localize->get("rev", "gethelp").':</span>
					<p><a href="'.$this->Page_BuildUrl("proj", "view/".$rlist[$i]['item_id']).'">'.$rlist[$i]['title2'].'</a></p>
				</div>
				<div class="iresp-btn"><a href="'.$this->Page_BuildUrl("proj", "view/".$rlist[$i]['item_id']).'"><img src="'.WWWHOST.'img/btn-mytree.png" alt="'.$this->localize->get("rev", "tree").'"></a></div>
			</div>
		</div>';		
	}
?>
	</div>
</div>