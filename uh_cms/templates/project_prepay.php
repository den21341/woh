<?php

$PM = $this->pageModel;

$page = $PM->get_page();

// Set page head title (h1 tag value)
$PAGE_H1 = $page->title;

// Build bread crumbs
$BCHTML = $this->renderBreadcrumbs();
?>

<div class="row-head breacrumb-bgi">
    <div class="container">
        <ol class="breadcrumb">
            <?=$BCHTML;?>
        </ol>
        <h1><?='Оплатить проект'?></h1>
    </div>
</div>
<div class="prepay-txt">
    <div class="manyproj-notallowed prepay-allowed">
        <?php if($this->paysVal[0]['test_type'] == 1) { ?>
        <div class="wh-cntr"><?=$this->localize->get("project-prepay", "portal-terms")?></div> <?=$this->localize->get("project-prepay", "participate")?> <span class="prepay-dng" >30 000 </span> <?=$this->localize->get("project-prepay", "uah")?> (<span class="prepay-dng" > 1000 </span> <?=$this->localize->get("project-prepay", "every-day")?>
        <span class="prepay-mon"><?=$this->paysVal[0]['pays_val']?> </span> <?=$this->localize->get("project-prepay", "uah")?>. <?=$this->localize->get("project-prepay", "money")?>
        <ul>
            <li><?=$this->localize->get("project-prepay", "read-more")?> <a href="<?=$this->Page_BuildUrl('info','active')?>" target="_blank"><?=$this->localize->get("project-prepay", "here")?></a>.</li>
            <li><?=$this->localize->get("project-prepay", "money-table")?> <a href="#" id="btnpayproj"><?=$this->localize->get("project-prepay", "here")?></a>.</li>
            <li><?=$this->localize->get("project-prepay", "all-your-money")?></li>
            <li><?=$this->localize->get("project-prepay", "good-tree")?></li>
        </ul>
        <hr>
            <?=$this->localize->get("project-prepay", "good-job")?>
        <div class="prepay-frm">
            <form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
                <input type="hidden" name="ik_co_id" value="561555503d1eafe0518b4567" />
                <input type="hidden" name="ik_pm_no" value="ID_0_<?=$this->uid?>_<?=$this->projid?>" />
                <input type="hidden" name="ik_am" id="ik_am_inp" value="<?=$this->paysVal[0]['pays_val']?>" /><!--  100.0  -->
                <input type="hidden" name="ik_cur" id="ik_cur" value="UAH" /><!-- UAH -->
                <input type="hidden" name="ik_desc" value="<?=$this->localize->get("project-prepay", "updating-account")?>" />
                <input type="hidden" name="ik_exp" value="<?=date("Y-m-d", time()+24*3600);?>" />
                <input type="hidden" name="ik_ia_u" value="http://wayofhelp.com/payscallback/ikinteract/" />
                <input type="hidden" name="ik_ia_m" value="post" />
                <input type="hidden" name="ik_suc_u" value="<?=$this->Page_BuildUrl("proj", "view/".$this->projid);?>" />
                <input type="hidden" name="ik_suc_m" value="get" />
                <input type="hidden" name="ik_fal_u" value="<?=$this->Page_BuildUrl("proj", "view/".$this->projid);?>" />
                <input type="hidden" name="ik_fal_m" value="get" />
                <!--<input type="hidden" name="ik_int" value="web" />
                <input type="hidden" name="ik_am_t" value="payway" />-->
                <div class="prepay-frm">
                    <input type="submit" class="btn btn-success btn-prepay-ok" value="<?=$this->localize->get("project-prepay", "payed")?>">
                </div>
            </form>
        </div>
        <?php } else if ($this->paysVal[0]['test_type'] == 2) {
            if(isset($this->run)) { ?>
                <p><?=$this->localize->get("project-prepay", "suc-launched")?></p>
            <?php } else { ?>
            <p> <div class="wh-cntr"><?=$this->localize->get("project-prepay", "under-terms")?></div>
                <br><?=$this->localize->get("project-prepay", "to-participate")?> <span class="prepay-dng" >30 000 </span> <?=$this->localize->get("project-prepay", "uah")?> (<span class="prepay-dng" > 1000 </span> <?=$this->localize->get("project-prepay", "every-day")?> <span class="prepay-mon"><?=$this->paysVal[0]['pays_val']?> </span> <?=$this->localize->get("project-prepay", "uah-to-project")?>
                <ul>
                    <li><?=$this->localize->get("project-prepay", "read-more")?> <a href="<?=$this->Page_BuildUrl('info','active')?>" target="_blank">тут</a>.</li>
                    <li><?=$this->localize->get("project-prepay", "money-table")?> <a href="#" id="btnpayproj"><?=$this->localize->get("project-prepay", "here")?></a>.</li>
                    <li><?=$this->localize->get("project-prepay", "all-your-money")?></li>
                    <li><?=$this->localize->get("project-prepay", "good-tree")?></li>
                </ul>
                <hr>
                <?=$this->localize->get("project-prepay", "good-job")?></p>
        <?php } } ?>
    </div>
</div>

<script>
    $(document).ready(function () {
       $('#btnpayproj').bind('click', function () {
            popWnd('projcreate', '');
       });
        //popWnd('socialconn', 'uid='+'<?=$this->uid?>');
    });
</script>
