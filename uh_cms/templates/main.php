<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

?>
<script>/*<![CDATA[*/$(document).ready(function(){<?php
	/*
	if( $this->first_visit )
	{
?>
if( $(window).width() > 1000 )
	popWnd('promovideodlg', '');
<?php
	}
	*/

	if( !$PM->isPromoSeen() )
	{
		/*
?>
	$("#promonextlnk").bind("click", function(){
		$(".poptip-promo").removeClass("poptip-promo-show");
		popWnd('promohelpdlg2', '');
		return false
	});
		*/
?><?php
	/*
	$(".poptip-close a").bind("click", function(){
		$(".poptip-promo").removeClass("poptip-promo-show");
		return false
	});

	var pos = $("#dohelpmainbtn").offset();
	$(".poptip-promo").css("left", Math.round(pos.left - 50)+"px");
	$(".poptip-promo").css("top", Math.round(pos.top - 280)+"px");
	$(".poptip-promo").addClass("poptip-promo-show");
<?php
	*/
	}
?>});/*]]>*/</script>
<div class="index-molecul">
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-6 hidden-xs hidden-sm">
<img class="img-responsive" src="<?=WWWHOST?>img/ind-block1-bg.png" alt="Фон WayOfHelp">
<div class="ind-bl1-sub"></div>
</div>
<div class="col-xs-6 col-md-6">
<div class="splash">
<div class="splash-tit hdr-cuf"><?=$PM->get_txtres()->mainslogan['text'];?></div>
<div class="splash-txt hdr-cuf"><?=$PM->get_txtres()->mainslogantxt['text'];?></div>
<div class="splash-r-btn">
<a href="<?=$this->Page_BuildUrl("cat","");?>">
<div class="btn main-btn-def btn-info-work-do">
<div class="col-md-3">
<img src="<?=WWWHOST?>img/main-give.png" class="main-btnimg-pos" alt="<?=$this->localize->get("cabinet", "assist")?>" title="<?=$this->localize->get("cabinet", "assist")?>">
</div>
<div class="col-md-9">
<p><?=$this->localize->get("cabinet", "makeassist")?></p>
</div>
</div></a>
<a href="<?=$this->Page_BuildUrl("proj","add");?>">
<div class="btn main-btn-def btn-info-work-make">
<div class="col-md-3">
<img src="<?=WWWHOST?>img/main-take.png" class="main-btnimg-pos" alt="<?=$this->localize->get("cabinet", "needassist")?>" title="<?=$this->localize->get("cabinet", "needassist")?>">
</div>
<div class="col-md-9">
<p><?=$this->localize->get("cabinet", "takeassist")?></p>
</div>
</div>
</a>
</div>
</div>
<div class="ind-main-txt">
</div>
</div>
<div class="col-xs-6 col-md-6 hidden-md hidden-lg">
<img class="img-responsive" src="img/ind-block1-bg-min.png" alt="<?=$this->localize->get("main", "fontitle")?>">
</div>
</div>
</div>
<div class="index-helpother">
<div class="container">
<div class="indx-hlpothr-h col-lg-12 col-md-12 col-xs-12"><span><?=$this->localize->get("main", "helping-others")?></span><br>
<p><?=$this->localize->get("main", "help-myself")?></p></div>
<div class="indx-hlpothr-block-img col-lg-12 col-md-12 col-xs-12">
<div class="col-lg-4 col-md-4 col-xs-4 hlpothr-block htlpothr-block-pers">
<div class="ind-arr-l"></div>
<a href="<?=$this->Page_BuildUrl('info','project')?>"><div class="hlpothr-pers hlpothr-block-center">
<div class="hlpothr-pers-img"></div>
<div class="hlpothr-pers-txt">
«<?=$this->localize->get("main", "what-do")?>»
</div>
</div></a>
<a href="<?=$this->Page_BuildUrl('registration','person')?>"><div class="btn btn-primary main-btn-promise main-reg-btn"><?=$this->localize->get("main", "registered")?></div></a>
<div class="hlpothr-block-side-txt"><?=$this->localize->get("main", "start-your-tree")?></div>
</div>
<div class="col-lg-4 col-md-4 col-xs-4 hlpothr-block">
<div class="hlpothr-mon">
<div class="hlpother-mon-h"><?=$this->localize->get("main", "how-been")?></div>
<div class="hlpothr-mon-img hlpothr-block-center">
<a href="<?=WWWHOST?>users/viewrev/375/"><div class="hlpothr-mon-screen"></div></a>
</div>
</div>
</div>
<div class="col-lg-4 col-md-4 col-xs-4 hlpothr-block">
	
	<div class="hlpothr-soc">
			<a class="main-fix-successfull" href="<?=WWWHOST?>cat/only_success/"><div class="btn btn-primary main-btn-promise main-reg-btn"><?=$this->localize->get("main", "successful-proj")?></div></a>

		<div class="main-block-share main-share-back">
			<div class="main-blocktwo-div">
				<div class="col-lg-4 col-md-4 col-xs-4">
					<a onclick="Share.facebook('http://wayofhelp.com/proj/view/1422/')"><div class="line-ico-main-block ico-fb-pop"></div></a>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-4">
					<a onclick="Share.vkontakte('http://wayofhelp.com/proj/view/1422/')"><div class="line-ico-main-block ico-vk-pop"></div></a>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-4">
					<a onclick="Share.odnoklassniki('http://wayofhelp.com/proj/view/1422/')"><div class="line-ico-main-block ico-ok-pop"></div></a>
				</div>
			</div>
		</div>
		<div class="hlpothr-soc-txt"><?=$this->localize->get("main", "tell-your-friends")?></div>
		<div class="hlpothr-soc-next"><?=$this->localize->get("main", "and-then")?></div>
		<div class="ind-arr-r"></div>
	</div>
<div class="ind-arr-r"></div>
</div>
</div>
</div>
</div>
</div>
<div class="index-vid">
<div id="video">
<div class="video-main video-hover">
<div class="video-txt"><p class="video-txt-f"><?=$this->localize->get("main", "sec-h1")?></p><p class="video-txt-l"><?=$this->localize->get("main", "sec-h2")?></p></div>
<div class="video-people">
<div class="people-img"></div>
<div class="people-txt sample-txt">
<p class="txt-count"><?=$this->endCount?></p>
<p class="txt-desc"><?=$this->localize->get("main", "sec-dream")?></p>
</div>
</div>
<div class="video-tree video-hover">
<div class="tree-txt sample-txt">
<p class="txt-count txt-count-tree"><?=$this->treeCount?></p>
<p class="txt-desc"><?=$this->localize->get("main", "sec-tree").'<span class="sec-tree1">'.$this->localize->get("main", "sec-tree1").'</span>'?></p>
</div>
<div class="tree-img"></div>
</div>
<div class="video-loc video-hover">
<div class="loc-txt sample-txt">
<p class="loc-count txt-count">8392км</p>
<p class="txt-desc"><?=$this->localize->get("main", "sec-loc")?></p>
</div>
<div class="loc-img"></div>
</div>
<div class="video-smily video-hover">
<div class="smily-txt sample-txt">
<p class="txt-desc smily-desc"><?=$this->localize->get("main", "sec-smily")?> <a href="<?WWWHOST?>users/viewrev/375/">Лариса Попова</a></p>
</div>
<div class="smily-img"></div>
</div>
<div class="video-gift video-hover">
<div class="gift-img"></div>
<div class="gift-txt sample-txt">
<p class="txt-count">5 человек</p>
<p class="txt-desc"><?=$this->localize->get("main", "sec-bonus1")?><a href="<?=WWWHOST?>bonus/list/"> <?=$this->localize->get("main", "bonus")?> </a><?=$this->localize->get("main", "sec-bonus2")?></p>
</div>
</div>
<div class="video-help video-hover">
<div class="help-img"></div>
<div class="help-txt sample-txt">
<p class="txt-count v-help-count">5</p>
<p class="txt-desc v-help-desc"><?=$this->localize->get("main", "sec-help")?></p>
</div>
</div>
<div class="video-getvideo"></div>
<img id="videoplay" alt="<?=$this->localize->get("main", "video-btn")?>" src="<?=IMGHOST;?>img/play-btn.png">
<div id="vplayer"></div>
</div>
</div>
<div class="col-md-12 col-xs-12 col-lg-12 bl2-btn-ab">
<a href="<?=$this->Page_BuildUrl("cat","");?>">
<div class="btn main-btn-def-down btn-info-work-do">
<div class="col-md-3">
<img src="<?=WWWHOST?>img/work-button-hand.png" class="main-btn-true" alt="<?=$this->localize->get("cabinet", "assist")?>" title="<?=$this->localize->get("cabinet", "assist")?>">
</div>
<div class="col-md-9">
<p><?=$this->localize->get("cabinet", "makeassist")?></p>
</div>
</div>
</a>
<a href="<?=$this->Page_BuildUrl("proj","add");?>">
<div class="btn main-btn-def-down btn-info-work-make">
<div class="col-md-3">
<img src="<?=WWWHOST?>img/work-button-ball.png" class="main-btn-true" alt="<?=$this->localize->get("cabinet", "needassist")?>" title="<?=$this->localize->get("cabinet", "needassist")?>">
</div>
<div class="col-md-9">
<p><?=$this->localize->get("cabinet", "takeassist")?></p>
</div>
</div>
</a>
<a href="<?=$this->Page_BuildUrl("info","project");?>">
<div class="btn main-btn-us main-btn-ofus">
<div class="col-md-3">
<img src="<?=WWWHOST?>img/black-book.png" class="mini-fix-img" alt="<?=$this->localize->get("footer", "ofus")?>" title="<?=$this->localize->get("footer", "ofus")?>">
</div>
<div class="col-md-9">
<p><?=$this->localize->get("main", "more-ofus")?></p>
</div>
</div>
</a>
</div>
<?php
	/*
	<div class="container">
		<div class="col-xs-12 col-md-6">
			&nbsp;
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="splash">
				<div class="splash-btn">
					<a href="<?=$this->Page_BuildUrl("info","makehelp");?>"><img src="img/btn-dohelp.png" alt="Оказать помощь" /></a>
					<a href="<?=$this->Page_BuildUrl("proj","add");?>"  class="lnkgethelp"><img src="img/btn-gethelp.png" alt="Получить помощь" /></a>
				</div>
				<div class="splash-btn"><a href="<?=$this->Page_BuildUrl("info","about");?>"><img src="img/btn-aboutus.png" alt="Подробнее о нас" /></a></div>
			</div>
		</div>
	</div>
	*/
	?>
</div>
<script src="<?=WWWHOST.'js/owl.carousel.min.js'?>"></script>
<h4 class="ind-h4-hlp"><?=$this->localize->get("main", "reviews")?></h4>
<div class="row">
<div class="container">
<div class="owl-carousel owl-cntr">
<?php
            $rlist = $this->revlist;
	        for( $i=0; $i<count($rlist); $i++ ) { ?>
<div class="ind-car-hlp">
<div class="car-hlp-u-photo col-lg-5 col-md-5 col-xs-5">
<img src="<?=WWWHOST.$this->revlist[$i]['pic']?>" class="hlp-u-photo-resize" alt="<?=$this->localize->get("main", "user-photo")?> <?=$rlist[$i]['name'].' '.$rlist[$i]['fname']?>" title="<?=$this->localize->get("main", "user-photo")?> <?=$rlist[$i]['name'].' '.$rlist[$i]['fname']?>">
</div>
<div class="car-hlp-u-info col-lg-7 col-md-7 col-xs-7">
<div class="hlp-name"><?=$rlist[$i]['name'].' '.$rlist[$i]['fname']?></div>
<div class="hlp-comm"><?=mb_strlen($rlist[$i]['rate_comments']) > 50 ? mb_substr($rlist[$i]['rate_comments'], 0, 50)."..." : $rlist[$i]['rate_comments']?></div>
<div class="hlp-proj">
<p>Получена помощь:</p>
<a href="<?=$this->Page_BuildUrl("proj", "view/".$rlist[$i]['item_id'])?>"><?=mb_strlen($rlist[$i]['title2']) > 30 ? mb_substr($rlist[$i]['title2'], 0, 30)."..." : $rlist[$i]['title2']?></a>
<a href="<?=$this->Page_BuildUrl("users", "viewrev/".$rlist[$i]['author_id'])?>"><div class="ind-info-btn-tree">
<div class="btn-sm-img-tree"></div>
<span><?=$this->localize->get("info-work", "my-tree")?></span>
</div>
</a>
</div>
</div>
</div>
<?php } ?>
</div>
<script>$(".owl-carousel").owlCarousel({loop:true,margin:10,nav:true,center:true,responsive:{0:{items:2},1000:{items:2},1200:{items:3}},});$(".owl-prev").html("");$(".owl-prev").addClass("owl-hlp-l").addClass("my-owl-nav");$(".owl-next").html("");$(".owl-next").addClass("owl-hlp-r").addClass("my-owl-nav");$(".center").addClass("midle-item");$(".owl-cntr").on("translate.owl.carousel",function(a){idx=a.item.index;$(".owl-item.midle-item").removeClass("midle-item");$(".owl-item").eq(idx).addClass("midle-item")});</script>
</div>
</div>
<hr>
<h4 class="ind-h4-hlp"><?=$this->localize->get("main", "total-activity")?></h4>
<div class="row">
<div class="container">
<div class="owl-carousel-act">
<?php for($i=0; $i<count($this->actUser); $i++) { ?>
<div class="ind-car-act">
<a href="<?=$this->Page_BuildUrl("users", "viewrev/".$this->actUser[$i]['id'])?>"> <div class="car-act-u-photo">
<img src="<?=WWWHOST.($this->actUser[$i]['pic'] ? $this->actUser[$i]['pic'] : 'img/no-pic.png')?>" class="act-u-photo-resize" alt="<?=$this->localize->get("main", "active-user-photo")?> <?=$this->actUser[$i]['name'].' '.$this->actUser[$i]['fname']?>">
</div>
<div class="car-act-u-name"><?=$this->actUser[$i]['name'].' '.$this->actUser[$i]['fname']?></div></a>
</div>
<?php } ?>
</div>
<script>$(".owl-carousel-act").owlCarousel({loop:true,margin:10,nav:true,responsive:{0:{items:2},600:{items:4},1000:{items:5}}});$(".owl-prev").html("");$(".owl-prev").addClass("owl-hlp-l").addClass("my-owl-nav");$(".owl-next").html("");$(".owl-next").addClass("owl-hlp-r").addClass("my-owl-nav");</script>
</div>
</div>
<hr>
<h4 class="ind-h4-hlp"><?=$this->localize->get("main", "popular-project")?></h4>
<div class="row">
<div class="container">
<div class="owl-carousel-pr">
<?php for($i=0; $i<count($this->actProj); $i++) { ?>
<div class="ind-car-act">
<a href="<?=$this->Page_BuildUrl("proj", "view/".$this->actProj[$i]['id'])?>"> <div class="car-act-u-photo">
<img src="<?=WWWHOST.($this->actProj[$i]['filename_ico'] ? $this->actProj[$i]['filename_ico'] : 'img/no-pic.png')?>" class="act-u-photo-resize" alt="<?=str_replace('"',"'",$this->actProj[$i]['title'])?>" title="<?=str_replace('"',"'",$this->actProj[$i]['title'])?>">
</div>
<div class="car-act-u-name"><?=(strlen($this->actProj[$i]['title'])) > 50 ? mb_substr($this->actProj[$i]['title'], 0, 50).'...' : $this->actProj[$i]['title'] ?></div></a>
</div>
<?php } ?>
</div>
<script>$(".owl-carousel-pr").owlCarousel({loop:true,margin:10,nav:true,responsive:{0:{items:3},600:{items:4},1000:{items:5}}});$(".owl-prev").html("");$(".owl-prev").addClass("owl-hlp-l").addClass("my-owl-nav");$(".owl-next").html("");$(".owl-next").addClass("owl-hlp-r").addClass("my-owl-nav");</script>
</div>
</div>
</div>
<div class="row-sects">
<div class="container">
<?php
	$sects = $this->sects;
	for( $i=0; $i<count($sects); $i++ )
	{
		$SECTURL = $this->Page_BuildUrl("cat",$sects[$i]['url']);

		if( ($i+1) % 4 == 1 ) { ?>
<div class='row'>
<?php }
		$ico_file = ( $sects[$i]['filename'] != "" ? WWWHOST.FILE_DIR.stripslashes($sects[$i]['filename']) : WWWHOST.'img/br-1-build.png'); ?>
<div class="col-xs-3 col-md-3">
<div class="sect-it">
<div class="sect-inf"><span class="circ-green"><?=$sects[$i]['its_do_num']?></span> <span class="circ-org"><?=$sects[$i]['its_need_num']?></span></div>
<div class="sect-ico"><a href="<?=$SECTURL?>"><img src="<?=$ico_file?>" alt="<?=$sects[$i]['name']?>" title="<?=$sects[$i]['name']?>"></a></div>
<div class='sect-name <?=(strlen($sects[$i]['name']) > 34 ? 'sect-name-l' : '' )?>'><a href="<?=$SECTURL?>"><?=(strlen($sects[$i]['name']) > 34 ? implode('<br>', explode(' ',$sects[$i]['name'],2)) : $sects[$i]['name'])?></a></div>
</div>
</div>
<?php if( ($i+1) % 4 == 0 )
		{
			echo '</div>';
		}
	}

	if( $i % 4 != 0 )
		echo '</div>';
?>
</div>
</div>
<?php
	if( !$PM->isPromoSeen() )
	{
?>
<div class="poptip-promo">
<div class="poptip-promo-cont">
<div class="poptip-close"><a href="#" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-remove"></span></a></div>
<div class="poptip-info"><?=( $PM->get_txtres()->poppromoindex['text'] );?></div>
</div>
</div>
<?php
		//$PM->setPromoSeen();
	}
?>
<script>$(document).ready(function(){$("#btnpopup").bind("click",function(){popWnd("infopopup","")})});</script>
<script>
		$(document).ready(function(){
			$("#btnpopup").bind("click",function(){popWnd("infopopup","")});

			Share = {
				vkontakte: function(purl) {
					url  = 'http://vkontakte.ru/share.php?';
					url += 'url='          + purl;
					Share.popup(url);
				},
				odnoklassniki: function(purl) {
					url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
					url += '&st._surl='    + encodeURIComponent(purl);
					Share.popup(url);
				},
				facebook: function(purl) {
					url  = 'http://www.facebook.com/sharer.php?s=100';
					url += '&p[url]='       + encodeURIComponent(purl);
					Share.popup(url);
				},
				popup: function(url, soc) {
					window.open(url,'','toolbar=0,status=0,width=626,height=436');
				}
			};


		});
	</script>
<script type="text/javascript">(window.Image?(new Image()):document.createElement("img")).src=location.protocol+"//vk.com/rtrg?r=mdbydJYwR1MdwSqUAmOjTyssxgbn5z4vDkXBqtSNM1jYgh4WZaq3ysL57YEw*6qznAN59Bm6pc3qj*7U/z8SQ/KfZYzK07ClqtwpJuAZVk72BN3KW1bHTukh5fIWzeByz0cj15bHhM1b1tv6cybksynk9VMy6XF0OfBRbQF08IA-";</script>