<?php
$type = (isset($_GET['type']) ? $_GET['type'] : 'reg');
$redirurl = ( isset($_GET['redirto']) ? $_GET['redirto'] : '' );

$type = (isset($_GET['type']) ? $_GET['type'] : 'reg');
$redirurl = ( isset($_GET['redirto']) ? $_GET['redirto'] : '' );

$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);

$FB_REDIRECT_URL = WWWHOST.'login/ajxregFB?to='.urlencode($redirurl);
$FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);

$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);

$VK_REDIRECT_URL = WWWHOST.'login/ajxregVK?to='.urlencode($redirurl);
$VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);

$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);

$ok_redirurl = str_replace('/','_',$redirurl);

$OK_REDIRECT_URL = WWWHOST.'login/redirok?to='.urlencode($ok_redirurl);
$OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);
?>

<style>
    .wnd-in{
        background: none;
    }
    .wnd-wnd{
        margin-left: 150px;
        background: url(<?=WWWHOST?>img/postreg-back.png) no-repeat 100% 100%;
        background-size: cover;
        width: 40%!important;
    }
</style>

<!----------------------- LOGIN ----------------------->
<p class="req-text req-postreg-text" id="succ" style="display:none">На Ваш почтовый ящик было отправлено письмо с подтверждением регистрации. Пройдите по ссылке в письме, чтобы активировать учетную запись.</p>

<div id="hideall">
    <div class="col-md-6 col-xs-6 col-lg-6">
        <p id="text" class="req-text">Помогая этому проекту, Вы запускаете цепочку добрых дел. Поэтому очень важно войти под своим аккауном или зарегистрироваться.</p>

        <div id="text-big" class="req-text-big" style="display: none">
            <p class="req-text-big">Помогая этому проекту, Вы запускаете цепочку добрых дел.</p>
            <img src="<?=WWWHOST?>img/indx-btn-lg.png">
        </div>
    </div>

    <div class="col-md-6 col-xs-6 col-lg-6">

        <div id="shares" class="req-share req-millenium req-twelve" style="display: none">

            <a target="_blank" href="<?=$FB_LINK;?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"><img src="<?=WWWHOST?>img/btn-log-fb.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-fb");?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"></a>
            <p class="req-or">или</p>
            <a target="_blank" href="<?=$VK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"><img src="<?=WWWHOST?>img/btn-log-vk.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-vk");?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"></a>
            <p class="req-or">или</p>
            <a target="_blank" href="<?=$OK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"><img src="<?=WWWHOST?>img/btn-log-od.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-ok");?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"></a>
            <p class="req-or req-or-last">или</p>
        </div>

        <div id="shows" class="req-div" style="display: none">
            <div class="row form-horizontal">
                <div class="col-xs-12 col-md-12 col-lg-12 req-share req-millenium req-twelve">
                    <div class="form-group">
                        <label for="ulogin" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input type="email" class="form-control req-hor req-fix-emaillog" id="ulogin" name="upass" placeholder="Email"></div>
                    </div>
                    <div class="form-group">
                        <label for="upass" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input type="password" class="form-control req-hor" id="upass" name="upass" placeholder="Password"></div>
                    </div>
                </div>
            </div>
        </div>
        <!------------------------ REGISTRATION ----------------------------->

        <div id="tblock" class="req-div" style="display: none">
            <div class="row form-horizontal">
                <div class="col-xs-12 col-md-12 col-lg-12 req-share req-millenium req-twelve">
                    <div class="form-group">
                        <p class="req-p-id req-fix-sec"><b>Займет всего <span class="req-color-share">2 секунды</p></b></span>
                        <div id="shareg" style="display: none" class="req-reg-share">
                            <a target="_blank" onclick="yaCounter34223620.reachGoal('ProjectforNonRegistrationUserDoublePushButton_Registration'); return true;" href="<?=$FB_LINK;?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"><img src="<?=WWWHOST?>img/btn-log-fb.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-fb");?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"></a>
                            <p class="req-or">или</p>
                            <a target="_blank" onclick="yaCounter34223620.reachGoal('ProjectforNonRegistrationUserDoublePushButton_Registration'); return true;" href="<?=$VK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"><img src="<?=WWWHOST?>img/btn-log-vk.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-vk");?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"></a>
                            <p class="req-or">или</p>
                            <a target="_blank" onclick="yaCounter34223620.reachGoal('ProjectforNonRegistrationUserDoublePushButton_Registration'); return true;" href="<?=$OK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"><img src="<?=WWWHOST?>img/btn-log-od.png" width="180" height="35" alt="<?=$this->localize->get("poplog", "btn-ok");?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"></a>
                            <p class="req-or req-or-last">или</p>
                        </div>
                        <p class="req-p-id" id="errname" style="display: none">Вы ввели неправильно имя</p>
                        <label for="regname" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input id="regname" type="text" size="20" maxlength="20" name="regname" class="form-control req-hor req-fix-reqname" placeholder="Имя"></div>
                    </div>
                    <div class="form-group">
                        <p class="req-p-id" id="errfname" style="display: none">Вы ввели неправильно фамилию</p>
                        <label for="regfname" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input id="regfname" type="text" size="20" maxlength="20" name="regfname" class="form-control req-hor" placeholder="Фамилия"></div>
                    </div>
                    <div class="form-group">
                        <p class="req-p-id" id="errmail" style="display: none">Вы ввели неправильно почту или такая уже существует</p>
                        <label for="regmail" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input id="regmail" type="email" name="regmail" class="form-control req-hor" placeholder="Email"></div>
                    </div>
                    <div class="form-group">
                        <p class="req-p-id" id="errpass" style="display: none">Вы ввели неправильно пароль</p>
                        <label for="regpass" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9"><input id="regpass" name="regpass" type="password" class="form-control req-hor" placeholder="Пароль"></div>
                    </div>
                </div>
            </div>


        </div>
        <!------------------------ ---------- ----------------------------->

        <div class="req-top col-md-12 col-xs-12 col-lg-12">
            <div id="reg" style="display: none"><a id="btnajxreg" onclick="ajxReg()" class="btn btn-primary req-reg-btnfix req-millenium req-twelve"><span class="glyphicon glyphicon-pencil"></span> <?=$this->localize->get("poplog", "btn-reg");?></a></div>

            <div id="login" class="btn req-buttons req-log req-millenium req-twelve">
                <p>Войти</p>
            </div>
            <div id="fix">
                <div style="margin-top: 16px;"></div>
            </div>
            <div id="showbtn" style="display: none">
                <button onclick="ajxLog()" id="btnlog" class="btn req-buttons req-log req-btn-top req-millenium req-twelve"><span class="glyphicon glyphicon-user"></span> <?=$this->localize->get("poplog", "btn-enter");?></button>
            </div>
        </div>
        <div id="lostp" style="display: none" class="req-lostpass"><a id="goto-restorepass<?=(isset($passrest_nobacklink) && $passrest_nobacklink ? '2' : '');?>" href="#"><?=$this->localize->get("poplog", "restpass");?></a></div>

        <div class="req-bot">
            <div><a id="registr" class="btn btn-primary req-botto-fix req-millenium req-twelve"><span class="glyphicon glyphicon-pencil" style="visibility: hidden;"></span> <?=$this->localize->get("poplog", "btn-reg");?></a></div>
        </div>

    </div>
</div>
<script type="text/javascript">

    function ajxLog() {
        var has_err = 0;

        if (document.getElementById('ulogin').value!=0) {
            $('#ulogin').css('border-color', '');
        }else{
            $('#ulogin').css('border-color', 'red');
            has_err = 1;
        }

        if (document.getElementById('upass').value!=0) {
            $('#upass').css('border-color', '');
        }else{
            $('#upass').css('border-color', 'red');
            has_err = 1;
        }

    }

    function ajxReg() {

        var has_err = 0;

        var name = /[A-Za-zА-Яа-яЁё]/;

        if (document.getElementById('regname').value.search(name)==0) {
            $( "#errname" ).hide( "slow", function() {});
            $('#regname').css('border-color', '');
        }else{
            $( "#errname" ).show( "slow", function() {});
            $('#regname').css('border-color', 'red');
            has_err = 1;
        }


        if (document.getElementById('regfname').value.search(name)==0) {
            $( "#errfname" ).hide( "slow", function() {});
            $('#regfname').css('border-color', '');
        }else{
            $( "#errfname" ).show( "slow", function() {});
            $('#regfname').css('border-color', 'red');
            has_err = 1;
        }

        if ($('#regmail').val() == '' || !isEmail($('#regmail').val()) || checkEmailbyAjax($('#regmail').val()) == true) {
            $( "#errmail" ).show( "slow", function() {});
            $('#regmail').css('border-color', 'red');
            has_err = 1;
        } else {
            $( "#errmail" ).hide( "slow", function() {});
            $('#regmail').css('border-color', '');
        }

        var pass = /[A-Za-z-0-9]{6,}/;

        if (document.getElementById('regpass').value.search(pass)==0){
            $( "#errpass" ).hide( "slow", function() {});
            $('#regpass').css('border-color', '');

        }else{
            $( "#errpass" ).show( "slow", function() {});
            $('#regpass').css('border-color', 'red');
            has_err = 1;
        }

        if (!has_err) {

            yaCounter34223620.reachGoal('ProjectforNonRegistrationUserDoublePushButton_Registration');

            $( "#hideall" ).hide( "slow", function() {});

            $( "#succ" ).show( "slow", function() {});

            var post_req_str = 'redirto='+'<?=$redirurl?>'+'&regname=' + $('#regname').val() + '&regfname=' + $('#regfname').val() + '&regemail=' + $('#regmail').val() + '&regpass=' + $('#regpass').val();

            var el14 = new Image(); el14.src = '//pixel-conv.ru/conv/?t=c&id=262853&uid=52859&h=191706572c190b0f1aa61e89a0b9640c';
            var el15 = new Image(); el15.src = '//pixel-conv.ru/conv/?t=c&id=263516&uid=52859&h=d8c53be3a7f14ac4313dc33556b46ed8';
            var el16 = new Image(); el16.src = '//pixel-conv.ru/conv/?t=c&id=262846&uid=52859&h=971ad6656664c4cf1224d65d58deef35';
            var el17 = new Image(); el17.src = '//pixel-conv.ru/conv/?t=c&id=263518&uid=52859&h=a15b7315f4c6a310e94c4cc7e50e042d';
            var el18 = new Image(); el18.src = '//pixel-conv.ru/conv/?t=c&id=262706&uid=52859&h=104cd7ced5ddc1d914bddbbbac80a8b3';
            var el19 = new Image(); el19.src = '//pixel-conv.ru/conv/?t=c&id=263498&uid=52859&h=0e8975f480c99514e963c53d02413e4d';
            var el20 = new Image(); el20.src = '//pixel-conv.ru/conv/?t=c&id=264049&uid=52859&h=ed6961a702d75da23bfdbe5236ec777e';
            var el21 = new Image(); el21.src = '//pixel-conv.ru/conv/?t=c&id=264051&uid=52859&h=0af331a96cabf666633049c36611338e';
            var el22 = new Image(); el22.src = '//pixel-conv.ru/conv/?t=c&id=264058&uid=52859&h=5588328941e82adc66fb0096e4bcb7da';

            $.ajax({
                type: "POST",
                url: req_ajx_host + "ajx/ajxreg/",
                data: post_req_str,
                dataType: "json",
                success: function (data) {
                    if(data == 'true') {}
                    //smplLogIn($('#regmail').val(), $('#regpass').val(), '<?=$redirurl?>');
                }
            });
        }
    }

    $( "#login" ).click(function() {
        yaCounter34223620.reachGoal('ProjectforNonRegistrationUserPushButton_Enter');

        $( "#login" ).hide( "fast", function() {});
        $( "#text" ).hide( "slow", function() {});
        $( "#text-big" ).show( "slow", function() {});
        $( "#shares" ).show( "slow", function() {});
        $( "#shows" ).show( "slow", function() {});
        $( "#showbtn" ).show( "slow", function() {});
        $( "#lostp" ).show( "slow", function() {});
        $( "#fix" ).hide( "slow", function() {});

        $( "#registr" ).show( "slow", function() {});
        $( "#tblock" ).hide( "slow", function() {});
        $( "#reg" ).hide( "slow", function() {});
        $( "#shareg" ).hide( "slow", function() {});
    });

    $( "#registr" ).click(function() {
        yaCounter34223620.reachGoal('ProjectforNonRegistrationUserPushButton_Registration');

        $( "#login" ).show( "fast", function() {});
        $( "#text" ).hide( "slow", function() {});
        $( "#text-big" ).show( "slow", function() {});
        $( "#shares" ).hide( "slow", function() {});
        $( "#shows" ).hide( "slow", function() {});
        $( "#showbtn" ).hide( "slow", function() {});
        $( "#lostp" ).hide( "slow", function() {});

        $( "#registr" ).hide( "slow", function() {});
        $( "#tblock" ).show( "slow", function() {});
        $( "#reg" ).show( "slow", function() {});
        $( "#shareg" ).show( "slow", function() {});

    });

    $("#btnlog").on("click", function(){
        yaCounter34223620.reachGoal('ProjectforNonRegistrationUserPushButton_Enter2');
        checkLoginForm();
        return false;
    });

</script>