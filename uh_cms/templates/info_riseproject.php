
<?php
$BCHTML = $this->renderBreadcrumbs();
?>
<div class="row">
    <div class="top-block top-block-pr col-md-12 col-xs-12 col-lg-12">
        <div class="container">
            <ol class="breadcrumb">
                <?=$BCHTML;?>
            </ol>
        </div>
        <div class="top-block-txt top-block-pr-txt">
            <h1><?=$this->localize->get("info-riseproject", "instruction")?></h1>
        </div>
    </div>
</div>

<div class="row">
    <div class="container des-cont-all">
        <div class="fees-about conf-about">
        <p><?=$this->localize->get("info-riseproject", "many-questions")?></p>

        <ul class="rise-decimal">
            <li><?=$this->localize->get("info-riseproject", "repost")?></li>
            <li><?=$this->localize->get("info-riseproject", "first-stage")?></li>
            <li><?=$this->localize->get("info-riseproject", "self")?> <a href="<?=WWWHOST?>info/power/"><?=$this->localize->get("info-riseproject", "active-actions")?></a> <?=$this->localize->get("info-riseproject", "can-increase")?></li>
            <li><?=$this->localize->get("info-riseproject", "successful-projects")?></li>
            <li><?=$this->localize->get("info-riseproject", "strangers")?></li>
            <li><?=$this->localize->get("info-riseproject", "properly-designed")?></li>
         </ul>

        <p> <?=$this->localize->get("info-riseproject", "thus")?>
            <ul>
                <li><?=$this->localize->get("info-riseproject", "share-soc")?></li>
                <li><?=$this->localize->get("info-riseproject", "free-shows")?> <a href="<?=WWWHOST?>info/power/"><?=$this->localize->get("info-riseproject", "active-action")?></a></li>
            </ul>
        </p>

        <p><?=$this->localize->get("info-riseproject", "some-try")?></p>


        <p><strong><?=$this->localize->get("info-riseproject", "your-social")?></strong></p>

        <p><?=$this->localize->get("info-riseproject", "remember")?></p>

        <p><strong><?=$this->localize->get("info-riseproject", "say-thanks")?></strong></p>

        <p><?=$this->localize->get("info-riseproject", "public-thanks")?></p>

        <p><strong><?=$this->localize->get("info-riseproject", "pray")?></strong></p>

        <p><?=$this->localize->get("info-riseproject", "not-expect")?></p>

        <p><strong><?=$this->localize->get("info-riseproject", "mind-local")?></strong></p>

        <p><?=$this->localize->get("info-riseproject", "first-then")?></p>

        <p><strong><?=$this->localize->get("info-riseproject", "top-portal")?></strong></p>

        <p><?=$this->localize->get("info-riseproject", "many-visitors")?>
        <ul class="rise-decimal">
            <li><?=$this->localize->get("info-riseproject", "right")?> <a href="<?=WWWHOST?>info/projectinfo/"><?=$this->localize->get("info-riseproject", "issue-project")?></li>

            <li><?=$this->localize->get("info-riseproject", "enough-impressions")?></li>
        </ul>
        </p>
        <p><strong><?=$this->localize->get("info-riseproject", "step-by-step")?></strong></p>

        <p><?=$this->localize->get("info-riseproject", "above")?></p>

        <p><?=$this->localize->get("info-riseproject", "im-think")?></p>

        </div>
    </div>
</div>
