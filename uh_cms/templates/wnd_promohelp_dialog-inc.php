<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

?>
<div class="wnd-hdr"><?=$this->localize->get("pophelp", "hdr");?></div>
<div class="pophelp-cont">
	<div class="pophelp-cont-human">
		<?=( $PM->get_txtres()->poppromo2['text'] );?>
		<div class="row">
			<div class="col-md-4">
				<div class="pophelp-col pophelp-col-dream">
					<span></span>
					<div><a href="<?=$this->Page_BuildUrl("cat", "fulfill-dream");?>" class="btn btn-border btn-big1"><?=$this->localize->get("pophelp", "btn-dream");?></a></div>
				</div>			
			</div>
			<div class="col-md-4">
				<div class="pophelp-col pophelp-col-dohelp">
					<span></span>
					<div><a href="<?=$this->Page_BuildUrl("cat", "");?>" class="btn btn-dohelp btn-big1"><?=$this->localize->get("pophelp", "btn-dohelp");?></a></div>
				</div>			
			</div>
			<div class="col-md-4">
				<div class="pophelp-col pophelp-col-needhelp">
					<span></span>
					<div><a id="popneedhelplnk" href="<?=$this->Page_BuildUrl("proj", "add");?>" class="btn btn-needhelp btn-big1"><?=$this->localize->get("pophelp", "btn-gethelp");?></a></div>
				</div>			
			</div>
		</div>	
	</div>
	<div class="pophelp-next"><a id="pophelpnext1" href="#">Далее</a></div>
</div>
<script>
$(document).ready(function(){
	$("#popneedhelplnk").bind("click", function(){
		if( usr_logged == 1 )
			return true
		
		popWnd('logdlg', '');
		return false
	});

	$("#pophelpnext1").bind("click", function(){
		popWnd('promohelpdlg3', '');
		return false
	});
});
</script>

