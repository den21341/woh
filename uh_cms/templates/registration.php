<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$employee = Array(5 => "до 5", 10 => "до 10", 20 => "до 20", 50 => "до 50", 99 => "до 100", 100 => "100 и более");

	$ruleslnk = $this->Page_BuildUrl("info","rules");
	
	$countries = $this->catmodel->Loc_CountryList(0, true, 0, 0);
	
	////////////////////////////////////////////////////////////////////////////
	// Meta data
		
	$PAGE_H1 = $this->localize->get("registration", "reg-on-site");
	//if( isset($PM->get_page()->title) )
	//	$PAGE_H1 = $PM->get_page()->title;
	switch($this->regmode)
	{
		case "edit_person":
		case "edit_company":
		case "edit_organization":
			$PAGE_H1 = $this->localize->get("registration", "edit-profile");
			$this->addBreadcrumbs($this->Page_BuildUrl("cabinet",""), $this->localize->get("registration", "my-cabinet"));
			break;
	}
	
	// Build bread crumbs	
	$BCHTML = $this->renderBreadcrumbs("");

	//print_r($this->catmodel->Loc_CityList(0, $this->reqdata->robl));

?>
<div class="row-head breacrumb-bgi">
	<div class="container">	
		<ol class="breadcrumb">
			<?=$BCHTML;?>
		</ol>
		<h1><?=$PAGE_H1;?></h1>
	</div>
</div>
<?php
	if( ($this->regmode == "organization") || ($this->regmode == "company") || ($this->regmode == "person") || ($this->regmode == "personfb") || ($this->regmode == "personvk") || ($this->regmode == "personok") )
	{
		$person_reg_mode = (($this->regmode == "person") || ($this->regmode == "personfb") || ($this->regmode == "personvk") || ($this->regmode == "personok"));
?>
<div class="container" style="padding-top: 32px;">
	<ul class="nav nav-tabs">
		<li role="presentation"<?=($person_reg_mode ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("registration", "person");?>"><?=$this->localize->get("reg", "tab-person");?></a></li>
		<li role="presentation"<?=(($this->regmode == "company") ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("registration", "company");?>"><?=$this->localize->get("reg", "tab-comp");?></a></li>
		<li role="presentation"<?=(($this->regmode == "organization")  ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("registration", "organization");?>"><?=$this->localize->get("reg", "tab-org");?></a></li>
	</ul>
</div>
<?php 
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Активация учетной записи по ссылке в письме
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	if( $this->regmode == "activate" )
	{
?>
<div class="container">
	<div class="info">
	<div class="h2"><?=$this->localize->get("registration", "step-3")?></div>
	<?=( $PM->get_txtres()->regactivate['text'] );?>
	</div>
</div>		
<?php	
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Окончение регистрации пользователя из соц-сети
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if( ($this->regmode == "done_personfb") || ($this->regmode == "done_personvk") || ($this->regmode == "done_personok") )
	{
	?>
	<div class="container">
		<div class="info">
			<div class="reg-done">
				<div class="h2"><?=$this->localize->get("registration", "step-2")?></div>
				<?=( $PM->get_txtres()->regdonesc['text'] )?>
			</div>
			<script>
				$(document).ready(function () {
					popWnd('postregistr','');
				});
			</script>
		</div>
	</div>	
<script type="text/javascript">
var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=231684&uid=52859&h=f1c0774dbb21275d0e1cb79880828aaa';
var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=231071&uid=52859&h=6ac0689b138c748461d63ae046b3f41c';
var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=231686&uid=52859&h=57123c5bee3c11e4380400ed2fd3e8a2';
var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=231689&uid=52859&h=8d84849fc724581697a88990a70b6219';
var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=231688&uid=52859&h=6373d5df6297525f2cc28fb3ed8d8243';
var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=231697&uid=52859&h=f7dc867aca6a5d8e1e67602b8d7025ca';
var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=231701&uid=52859&h=f96ebdfc47b1786c6611eb88895c3972';
var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=242318&uid=52859&h=28af8f11fcd7bfd283be647aaf412c6e';
var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=242322&uid=52859&h=192f430603b03d6548648c9360281ccc';
var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=242852&uid=52859&h=99e71bdc2c329b97839efb97260a7a36';
var el11 = new Image(); el11.src = '//pixel-conv.ru/conv/?t=c&id=251153&uid=52859&h=770ee5d6a4c733ba0875718787657c60';
var el12 = new Image(); el12.src = '//pixel-conv.ru/conv/?t=c&id=252731&uid=52859&h=d8549fe304ffd0f1a883ea5644be000c';
var el13 = new Image(); el13.src = '//pixel-conv.ru/conv/?t=c&id=253927&uid=52859&h=f1a8f294f55cfc0d743ebba5d4a7c684';
var el14 = new Image(); el14.src = '//pixel-conv.ru/conv/?t=c&id=262853&uid=52859&h=191706572c190b0f1aa61e89a0b9640c';
var el15 = new Image(); el15.src = '//pixel-conv.ru/conv/?t=c&id=263516&uid=52859&h=d8c53be3a7f14ac4313dc33556b46ed8';
var el16 = new Image(); el16.src = '//pixel-conv.ru/conv/?t=c&id=262846&uid=52859&h=971ad6656664c4cf1224d65d58deef35';
var el17 = new Image(); el17.src = '//pixel-conv.ru/conv/?t=c&id=263518&uid=52859&h=a15b7315f4c6a310e94c4cc7e50e042d';
var el18 = new Image(); el18.src = '//pixel-conv.ru/conv/?t=c&id=262706&uid=52859&h=104cd7ced5ddc1d914bddbbbac80a8b3';
var el19 = new Image(); el19.src = '//pixel-conv.ru/conv/?t=c&id=263498&uid=52859&h=0e8975f480c99514e963c53d02413e4d';
var el20 = new Image(); el20.src = '//pixel-conv.ru/conv/?t=c&id=264049&uid=52859&h=ed6961a702d75da23bfdbe5236ec777e';
var el21 = new Image(); el21.src = '//pixel-conv.ru/conv/?t=c&id=264051&uid=52859&h=0af331a96cabf666633049c36611338e';
var el22 = new Image(); el22.src = '//pixel-conv.ru/conv/?t=c&id=264058&uid=52859&h=5588328941e82adc66fb0096e4bcb7da';
//
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262768&uid=52859&h=b40d1945564cd10737b6d99fb55ccf74';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264230&uid=52859&h=01cdf5a2b2f3569a471b2de735596d66';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264231&uid=52859&h=5b1f733b297c0cc845ae7b3be5b31ffd';
//22.09
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264404&uid=52859&h=81740547a1f9b20d7a30aae7b0587a57';
var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=264374&uid=52859&h=9da7701400268dceedf485097f62d950';
var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=264382&uid=52859&h=574c71dbce2ecf35ea557a12d357a63d';
var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=264385&uid=52859&h=0ec15de7ccd066ae617db7a83a333c57';
//var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=264388&uid=52859&h=e498c0e8047e3ce9aaba511f6e1ebab9';
var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=264389&uid=52859&h=1823007bb52317a20468c06b99283629';
var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=264390&uid=52859&h=72f4167370bcf999675db28b8dc1b1ec';
var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=264392&uid=52859&h=4662b70bb2ba47b985943ed3f512df26';
var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=264393&uid=52859&h=94ba8aa160cc3a9960eb1003e1970d7e';
var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=264394&uid=52859&h=6e471923c98ee5354f41417e4c02963c';
var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=264395&uid=52859&h=194d3e9e9a750581bdedfda941662774';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264407&uid=52859&h=258eae1bff037bef739272b6992f22b7';
//26.09
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264739&uid=52859&h=1ce6a292a0783326c6b417fe55602181';

</script>
	<?php
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Окончение регистрации на сайте обычного пользователя
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if( $this->regmode == "done_person" )
	{
?>
<div class="container">
	<div class="info">
		<div class="reg-done">
			<div class="h2"><?=$this->localize->get("registration", "step-2")?></div>
			<?=( $PM->get_txtres()->regdone['text'] )?>
		</div>
		<script>
			$(document).ready(function () {
				popWnd('postregistr','');
			});
		</script>
	</div>
</div>
<script type="text/javascript">
var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=231684&uid=52859&h=f1c0774dbb21275d0e1cb79880828aaa';
var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=231071&uid=52859&h=6ac0689b138c748461d63ae046b3f41c';
var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=231686&uid=52859&h=57123c5bee3c11e4380400ed2fd3e8a2';
var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=231689&uid=52859&h=8d84849fc724581697a88990a70b6219';
var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=231688&uid=52859&h=6373d5df6297525f2cc28fb3ed8d8243';
var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=231697&uid=52859&h=f7dc867aca6a5d8e1e67602b8d7025ca';
var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=231701&uid=52859&h=f96ebdfc47b1786c6611eb88895c3972';
var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=242318&uid=52859&h=28af8f11fcd7bfd283be647aaf412c6e';
var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=242322&uid=52859&h=192f430603b03d6548648c9360281ccc';
var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=242852&uid=52859&h=99e71bdc2c329b97839efb97260a7a36';
var el11 = new Image(); el11.src = '//pixel-conv.ru/conv/?t=c&id=251153&uid=52859&h=770ee5d6a4c733ba0875718787657c60';
var el12 = new Image(); el12.src = '//pixel-conv.ru/conv/?t=c&id=252731&uid=52859&h=d8549fe304ffd0f1a883ea5644be000c';
var el13 = new Image(); el13.src = '//pixel-conv.ru/conv/?t=c&id=253927&uid=52859&h=f1a8f294f55cfc0d743ebba5d4a7c684';
var el14 = new Image(); el14.src = '//pixel-conv.ru/conv/?t=c&id=262853&uid=52859&h=191706572c190b0f1aa61e89a0b9640c';
var el15 = new Image(); el15.src = '//pixel-conv.ru/conv/?t=c&id=263516&uid=52859&h=d8c53be3a7f14ac4313dc33556b46ed8';
var el16 = new Image(); el16.src = '//pixel-conv.ru/conv/?t=c&id=262846&uid=52859&h=971ad6656664c4cf1224d65d58deef35';
var el17 = new Image(); el17.src = '//pixel-conv.ru/conv/?t=c&id=263518&uid=52859&h=a15b7315f4c6a310e94c4cc7e50e042d';
var el18 = new Image(); el18.src = '//pixel-conv.ru/conv/?t=c&id=262706&uid=52859&h=104cd7ced5ddc1d914bddbbbac80a8b3';
var el19 = new Image(); el19.src = '//pixel-conv.ru/conv/?t=c&id=263498&uid=52859&h=0e8975f480c99514e963c53d02413e4d';
var el20 = new Image(); el20.src = '//pixel-conv.ru/conv/?t=c&id=264049&uid=52859&h=ed6961a702d75da23bfdbe5236ec777e';
var el21 = new Image(); el21.src = '//pixel-conv.ru/conv/?t=c&id=264051&uid=52859&h=0af331a96cabf666633049c36611338e';
var el22 = new Image(); el22.src = '//pixel-conv.ru/conv/?t=c&id=264058&uid=52859&h=5588328941e82adc66fb0096e4bcb7da';
//
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262768&uid=52859&h=b40d1945564cd10737b6d99fb55ccf74';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264230&uid=52859&h=01cdf5a2b2f3569a471b2de735596d66';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264231&uid=52859&h=5b1f733b297c0cc845ae7b3be5b31ffd';
//22.09
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264404&uid=52859&h=81740547a1f9b20d7a30aae7b0587a57';
var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=264374&uid=52859&h=9da7701400268dceedf485097f62d950';
var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=264382&uid=52859&h=574c71dbce2ecf35ea557a12d357a63d';
var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=264385&uid=52859&h=0ec15de7ccd066ae617db7a83a333c57';
//var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=264388&uid=52859&h=e498c0e8047e3ce9aaba511f6e1ebab9';
var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=264389&uid=52859&h=1823007bb52317a20468c06b99283629';
var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=264390&uid=52859&h=72f4167370bcf999675db28b8dc1b1ec';
var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=264392&uid=52859&h=4662b70bb2ba47b985943ed3f512df26';
var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=264393&uid=52859&h=94ba8aa160cc3a9960eb1003e1970d7e';
var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=264394&uid=52859&h=6e471923c98ee5354f41417e4c02963c';
var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=264395&uid=52859&h=194d3e9e9a750581bdedfda941662774';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264407&uid=52859&h=258eae1bff037bef739272b6992f22b7';
//26.09
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264739&uid=52859&h=1ce6a292a0783326c6b417fe55602181';
</script>
<?php
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Окончение регистрации компании на сайте 
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if( $this->regmode == "done_company" )
	{
?>
	<div class="container">
		<div class="info">
			<div class="reg-done">
				<div class="h2"><?=$this->localize->get("registration", "step-2")?></div>
				<?=( $PM->get_txtres()->regdone['text'] );?>
			</div>
			<script>
				$(document).ready(function () {
					popWnd('postregistr','');
				});
			</script>
		</div>
	</div>		
<script type="text/javascript">
var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=231684&uid=52859&h=f1c0774dbb21275d0e1cb79880828aaa';
var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=231071&uid=52859&h=6ac0689b138c748461d63ae046b3f41c';
var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=231686&uid=52859&h=57123c5bee3c11e4380400ed2fd3e8a2';
var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=231689&uid=52859&h=8d84849fc724581697a88990a70b6219';
var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=231688&uid=52859&h=6373d5df6297525f2cc28fb3ed8d8243';
var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=231697&uid=52859&h=f7dc867aca6a5d8e1e67602b8d7025ca';
var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=231701&uid=52859&h=f96ebdfc47b1786c6611eb88895c3972';
var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=242318&uid=52859&h=28af8f11fcd7bfd283be647aaf412c6e';
var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=242322&uid=52859&h=192f430603b03d6548648c9360281ccc';
var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=242852&uid=52859&h=99e71bdc2c329b97839efb97260a7a36';
var el11 = new Image(); el11.src = '//pixel-conv.ru/conv/?t=c&id=251153&uid=52859&h=770ee5d6a4c733ba0875718787657c60';
var el12 = new Image(); el12.src = '//pixel-conv.ru/conv/?t=c&id=252731&uid=52859&h=d8549fe304ffd0f1a883ea5644be000c';
var el13 = new Image(); el13.src = '//pixel-conv.ru/conv/?t=c&id=253927&uid=52859&h=f1a8f294f55cfc0d743ebba5d4a7c684';
var el14 = new Image(); el14.src = '//pixel-conv.ru/conv/?t=c&id=262853&uid=52859&h=191706572c190b0f1aa61e89a0b9640c';
var el15 = new Image(); el15.src = '//pixel-conv.ru/conv/?t=c&id=263516&uid=52859&h=d8c53be3a7f14ac4313dc33556b46ed8';
var el16 = new Image(); el16.src = '//pixel-conv.ru/conv/?t=c&id=262846&uid=52859&h=971ad6656664c4cf1224d65d58deef35';
var el17 = new Image(); el17.src = '//pixel-conv.ru/conv/?t=c&id=263518&uid=52859&h=a15b7315f4c6a310e94c4cc7e50e042d';
var el18 = new Image(); el18.src = '//pixel-conv.ru/conv/?t=c&id=262706&uid=52859&h=104cd7ced5ddc1d914bddbbbac80a8b3';
var el19 = new Image(); el19.src = '//pixel-conv.ru/conv/?t=c&id=263498&uid=52859&h=0e8975f480c99514e963c53d02413e4d';
var el20 = new Image(); el20.src = '//pixel-conv.ru/conv/?t=c&id=264049&uid=52859&h=ed6961a702d75da23bfdbe5236ec777e';
var el21 = new Image(); el21.src = '//pixel-conv.ru/conv/?t=c&id=264051&uid=52859&h=0af331a96cabf666633049c36611338e';
var el22 = new Image(); el22.src = '//pixel-conv.ru/conv/?t=c&id=264058&uid=52859&h=5588328941e82adc66fb0096e4bcb7da';
//
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262768&uid=52859&h=b40d1945564cd10737b6d99fb55ccf74';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264230&uid=52859&h=01cdf5a2b2f3569a471b2de735596d66';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264231&uid=52859&h=5b1f733b297c0cc845ae7b3be5b31ffd';
//22.09
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264404&uid=52859&h=81740547a1f9b20d7a30aae7b0587a57';
var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=264374&uid=52859&h=9da7701400268dceedf485097f62d950';
var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=264382&uid=52859&h=574c71dbce2ecf35ea557a12d357a63d';
var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=264385&uid=52859&h=0ec15de7ccd066ae617db7a83a333c57';
//var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=264388&uid=52859&h=e498c0e8047e3ce9aaba511f6e1ebab9';
var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=264389&uid=52859&h=1823007bb52317a20468c06b99283629';
var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=264390&uid=52859&h=72f4167370bcf999675db28b8dc1b1ec';
var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=264392&uid=52859&h=4662b70bb2ba47b985943ed3f512df26';
var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=264393&uid=52859&h=94ba8aa160cc3a9960eb1003e1970d7e';
var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=264394&uid=52859&h=6e471923c98ee5354f41417e4c02963c';
var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=264395&uid=52859&h=194d3e9e9a750581bdedfda941662774';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264407&uid=52859&h=258eae1bff037bef739272b6992f22b7';
//26.09
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264739&uid=52859&h=1ce6a292a0783326c6b417fe55602181';
</script>
<?php
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Окончение регистрации организации на сайте
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if( $this->regmode == "done_organization" )
	{
?>
	<div class="container">
		<div class="info">
			<div class="reg-done">
				<div class="h2"><?=$this->localize->get("registration", "step-2")?></div>
				<?=( $PM->get_txtres()->regdone['text'] )?>
			</div>
			<script>
				$(document).ready(function () {
					popWnd('postregistr','');
				});
			</script>
		</div>
	</div>	
<script type="text/javascript">
var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=231684&uid=52859&h=f1c0774dbb21275d0e1cb79880828aaa';
var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=231071&uid=52859&h=6ac0689b138c748461d63ae046b3f41c';
var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=231686&uid=52859&h=57123c5bee3c11e4380400ed2fd3e8a2';
var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=231689&uid=52859&h=8d84849fc724581697a88990a70b6219';
var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=231688&uid=52859&h=6373d5df6297525f2cc28fb3ed8d8243';
var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=231697&uid=52859&h=f7dc867aca6a5d8e1e67602b8d7025ca';
var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=231701&uid=52859&h=f96ebdfc47b1786c6611eb88895c3972';
var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=242318&uid=52859&h=28af8f11fcd7bfd283be647aaf412c6e';
var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=242322&uid=52859&h=192f430603b03d6548648c9360281ccc';
var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=242852&uid=52859&h=99e71bdc2c329b97839efb97260a7a36';
var el11 = new Image(); el11.src = '//pixel-conv.ru/conv/?t=c&id=251153&uid=52859&h=770ee5d6a4c733ba0875718787657c60';
var el12 = new Image(); el12.src = '//pixel-conv.ru/conv/?t=c&id=252731&uid=52859&h=d8549fe304ffd0f1a883ea5644be000c';
var el13 = new Image(); el13.src = '//pixel-conv.ru/conv/?t=c&id=253927&uid=52859&h=f1a8f294f55cfc0d743ebba5d4a7c684';
var el14 = new Image(); el14.src = '//pixel-conv.ru/conv/?t=c&id=262853&uid=52859&h=191706572c190b0f1aa61e89a0b9640c';
var el15 = new Image(); el15.src = '//pixel-conv.ru/conv/?t=c&id=263516&uid=52859&h=d8c53be3a7f14ac4313dc33556b46ed8';
var el16 = new Image(); el16.src = '//pixel-conv.ru/conv/?t=c&id=262846&uid=52859&h=971ad6656664c4cf1224d65d58deef35';
var el17 = new Image(); el17.src = '//pixel-conv.ru/conv/?t=c&id=263518&uid=52859&h=a15b7315f4c6a310e94c4cc7e50e042d';
var el18 = new Image(); el18.src = '//pixel-conv.ru/conv/?t=c&id=262706&uid=52859&h=104cd7ced5ddc1d914bddbbbac80a8b3';
var el19 = new Image(); el19.src = '//pixel-conv.ru/conv/?t=c&id=263498&uid=52859&h=0e8975f480c99514e963c53d02413e4d';
var el20 = new Image(); el20.src = '//pixel-conv.ru/conv/?t=c&id=264049&uid=52859&h=ed6961a702d75da23bfdbe5236ec777e';
var el21 = new Image(); el21.src = '//pixel-conv.ru/conv/?t=c&id=264051&uid=52859&h=0af331a96cabf666633049c36611338e';
var el22 = new Image(); el22.src = '//pixel-conv.ru/conv/?t=c&id=264058&uid=52859&h=5588328941e82adc66fb0096e4bcb7da';
//
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=262768&uid=52859&h=b40d1945564cd10737b6d99fb55ccf74';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264230&uid=52859&h=01cdf5a2b2f3569a471b2de735596d66';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264231&uid=52859&h=5b1f733b297c0cc845ae7b3be5b31ffd';
//22.09
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264404&uid=52859&h=81740547a1f9b20d7a30aae7b0587a57';
var el1 = new Image(); el1.src = '//pixel-conv.ru/conv/?t=c&id=264374&uid=52859&h=9da7701400268dceedf485097f62d950';
var el2 = new Image(); el2.src = '//pixel-conv.ru/conv/?t=c&id=264382&uid=52859&h=574c71dbce2ecf35ea557a12d357a63d';
var el3 = new Image(); el3.src = '//pixel-conv.ru/conv/?t=c&id=264385&uid=52859&h=0ec15de7ccd066ae617db7a83a333c57';
//var el4 = new Image(); el4.src = '//pixel-conv.ru/conv/?t=c&id=264388&uid=52859&h=e498c0e8047e3ce9aaba511f6e1ebab9';
var el5 = new Image(); el5.src = '//pixel-conv.ru/conv/?t=c&id=264389&uid=52859&h=1823007bb52317a20468c06b99283629';
var el6 = new Image(); el6.src = '//pixel-conv.ru/conv/?t=c&id=264390&uid=52859&h=72f4167370bcf999675db28b8dc1b1ec';
var el7 = new Image(); el7.src = '//pixel-conv.ru/conv/?t=c&id=264392&uid=52859&h=4662b70bb2ba47b985943ed3f512df26';
var el8 = new Image(); el8.src = '//pixel-conv.ru/conv/?t=c&id=264393&uid=52859&h=94ba8aa160cc3a9960eb1003e1970d7e';
var el9 = new Image(); el9.src = '//pixel-conv.ru/conv/?t=c&id=264394&uid=52859&h=6e471923c98ee5354f41417e4c02963c';
var el10 = new Image(); el10.src = '//pixel-conv.ru/conv/?t=c&id=264395&uid=52859&h=194d3e9e9a750581bdedfda941662774';
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264407&uid=52859&h=258eae1bff037bef739272b6992f22b7';
//26.09
var el = new Image(); el.src = '//pixel-conv.ru/conv/?t=c&id=264739&uid=52859&h=1ce6a292a0783326c6b417fe55602181';
</script>
<?php
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Установка разделов для информирования
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if( $this->regmode == "edittopics" )
	{
?>
<div class="container">
	<div class="info">
	<div class="h2"><?=$this->localize->get("registration", "way-of-help")?></div>
	<?=( $PM->get_txtres()->regtopicsel['text'] );?>
	</div>
</div>
<form id="topicselfrm" action="<?=( $this->regmode == "settopics" ? $this->Page_BuildUrl("registration", "dotopic") : $this->Page_BuildUrl("cabinet", "savetopic") );?>" method="POST">
<input type="hidden" name="action" value="topics">
<div class="row-gray">
	<div class="container">
		<a id="formstart" name="formstart"></a>
	<?php
		/*
		if( $this->reqdata->has_errors() )
		{			
			echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
		}
		if( isset($this->msg) && ($this->msg != "") )
		{
			echo '<div class="frm-notify-msg">'.$this->msg.'</div>';
		}
		*/
		
		echo '<div class="row form-horizontal">';
		
		$seltopics = $this->buyertopics;
		
		// Get all catalog sections
		$cat = $this->catmodel;
		
		$sgroup = 0;
		
		$sect0 = $cat->Catalog_SectLevel($sgroup, 0);
		for( $i=0; $i<count($sect0); $i++ )
		{
			//$isopen0 = $cat->Catalog_SectIsInPath($sect0[$i]['id'], $this->spath);
		
			//$caturl = $this->Catalog_BuildUrl($sect0[$i]['url']);
		
			$ico_file = ( $sect0[$i]['filename_thumb'] != "" ? WWWHOST.FILE_DIR.stripslashes($sect0[$i]['filename_thumb']) : WWWHOST.'img/br-br-1-build.png');
			//if( $isopen0 && ($sect0[$i]['filename_thumb_sel'] != "") )
			//	$ico_file = WWWHOST.FILE_DIR.stripslashes($sect0[$i]['filename_thumb_sel']);		
			
			echo '<div class="col-xs-6 col-sm-4 col-md-3"><div class="topic-it">
				<table>
				<tr><td><img src="'.$ico_file.'" alt=""></td><td><span>'.$sect0[$i]['name'].'</span></td></tr>
				</table>';
								
			$sect1 = $cat->Catalog_SectLevel($sgroup, $sect0[$i]['id']);
				
			echo '<div class="topic-list">';
			for( $j=0; $j<count($sect1); $j++ )
			{
				//$isopen1 = $cat->Catalog_SectIsInPath($sect1[$j]['id'], $this->spath);
	
				//$caturl = $this->Catalog_BuildUrl($sect1[$j]['url']);
				
				echo '<div class="checkbox"><label><input type="checkbox" name="rtopics[]" value="'.$sect1[$j]['id'].'"'.( in_array($sect1[$j]['id'], $seltopics) ? ' checked' : '' ).'> '.$sect1[$j]['name'].'</label></div>';
	
				/*
				if( $isopen1 )
				{
					$sect2 = $cat->Catalog_SectLevel($sgroup, $sect1[$j]['id']);
					echo '<ul>';
					for( $k=0; $k<count($sect2); $k++ )
					{
						$isopen2 = $cat->Catalog_SectIsInPath($sect2[$k]['id'], $this->spath);
	
						$caturl = $this->Catalog_BuildUrl($sect2[$k]['url']);
	
						echo '<li'.($isopen2 ? ' class="sel"' : '').'><a href="'.$caturl.'">'.$sect2[$k]['name'].'</a></li>';
					}
					echo '</ul>';
				}
				*/
			}
			echo '</div>';
		
			echo '</div></div>';
			
			$vis_xs = false;
			$vis_sm = false;
			$vis_md = false;
			$cls_name = "";
			if( ($i+1) % 2 == 0 ){	$cls_name .= ($cls_name != "" ? " " : "")."visible-xs";	$vis_xs = true; }
			if( ($i+1) % 3 == 0 ){	$cls_name .= ($cls_name != "" ? " " : "")."visible-sm";	$vis_sm = true;	}
			if( ($i+1) % 4 == 0 ){	$cls_name .= ($cls_name != "" ? " " : "")."visible-md visible-lg";	$vis_md = true; }
			
			if( $cls_name != "" )
			{
				echo '<div class="clearfix '.$cls_name.'"></div>';
			}
		}
		
		echo '</div>';
	?>									
	</div>	
</div>
<div class="container">
	<div class="text-center btn-form-pad"><input <?=$this->regmode == "settopics" ? 'onclick="ga(\'send\', \'event\', \'try_registration\', \'push_button_registration\')' : ''?> type="submit" class="btn btn-primary" value="<?=( $this->regmode == "settopics" ? $this->localize->get("poplog", "btn-reg") : $this->localize->get("addbonus", "btn-save") );?>"></div>
</div>
</form>		
<?php
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 После первого шага регистрации организации, показать форму прикрепления уставных документов
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if( $this->regmode == "attach" )
	{
?>
<div class="container">
	<div class="info">
	<?=( $PM->get_txtres()->regattach['text'] );?>
	</div>
</div>
<form id="regformatt" action="<?=( $this->regmode == "attach" ? $this->Page_BuildUrl("registration", "doattach") : $this->Page_BuildUrl("cabinet", "saveattach") );?>" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="makeattach">
<div class="row-gray">
	<div class="container">
		<a id="formstart" name="formstart"></a>
		<div class="h2"><?=$this->localize->get("registration", "reg-document")?></div>
	<?php
		if( $this->reqdata->has_errors() )
		{			
			echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
		}
		if( isset($this->msg) && ($this->msg != "") )
		{
			echo '<div class="frm-notify-msg">'.$this->msg.'</div>';
		}
	?>		
		<div class="row form-horizontal">			
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rsvid1') ? ' has-error' : '' );?>">
					<label for="rsvid1" class="col-sm-6 control-label"><?=$this->localize->get("registration", "certificate")?></label>
					<div class="col-sm-6"><input type="file" id="rsvid1" name="rsvid1"></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rsvid2') ? ' has-error' : '' );?>">
					<label for="rsvid2" class="col-sm-6 control-label"><?=$this->localize->get("registration", "certificate")?></label>
					<div class="col-sm-6"><input type="file" id="rsvid2" name="rsvid2"></div>
				</div>											
			</div>
			<?php
			/*
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rlogo') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label">Логотип:</label>
					<div class="col-sm-9"><input type="file" id="rlogo" name="rlogo"></div>
				</div>
			</div>
			*/
			?>				
		</div>						
	</div>	
</div>
<?php 
		if( $this->regmode == "attach" )
		{

?>
<div class="container">
	<div class="text-center"><input type="submit" class="btn btn-primary" value="<?=$this->localize->get("poplog", "btn-reg")?>" /></div>
</div>
<?php
		}
		else // edit
		{
?>
<div class="container">
	<div class="text-center"><input type="submit" class="btn btn-primary" value="<?=$this->localize->get("addbonus", "btn-save")?>" /></div>
</div>
<?php		
		} 
?>
</form>
<?php
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Форма добавления или редактирования организации
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if( ($this->regmode == "organization") || ($this->regmode == "edit_organization") )	
	{		
		$cryptcode = "";
		$h1_str = $this->localize->get("registration", "step-1");
		if( $this->regmode == "organization" )
		{
			$cryptcode = $this->captcha->generate_code();
?>
<div class="container">
	<div class="info">
	<?=( $PM->get_txtres()->regformorg['text'] );?>
	</div>
</div>
<?php
		} 
		else	// edit
		{
			$h1_str = $this->localize->get("registration", "reg-data");
		}
	
		$obls = $this->catmodel->Loc_OblList($this->reqdata->rcountry);
		//$cities = $this->catmodel->Loc_CityList(0, $this->reqdata->robl);
?>
<form class="regform" id="regformcomp" action="<?=( $this->regmode == "organization" ? $this->Page_BuildUrl("registration", "doorganization") : $this->Page_BuildUrl("cabinet", "saveme") );?>" method="POST">
	<input type="hidden" name="action" value="makeregorg">
	<input type="hidden" name="controlcode" value="<?=$cryptcode?>">
<div class="row-gray">
	<div class="container">
		<a id="formstart" name="formstart"></a>
		<div class="h2"><?=$h1_str;?></div>
	<?php
		if( $this->reqdata->has_errors() )
		{			
			echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
		}
		if( isset($this->msg) && ($this->msg != "") )
		{
			echo '<div class="frm-notify-msg">'.$this->msg.'</div>';
		}
	?>		
		<div class="row form-horizontal">			
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rorg') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-orgo");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rorg" name="rorg" placeholder="<?=$this->localize->get("reg", "ph-orgo");?>" value="<?=$this->reqdata->quote($this->reqdata->rorg);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-orgo");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rorgsphere') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-sphere");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rorgsphere" name="rorgsphere" placeholder="<?=$this->localize->get("reg", "ph-sphere-org");?>" value="<?=$this->reqdata->quote($this->reqdata->rorgsphere);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-sphere");?></div></div>
				</div>
				
				<div class="form-group<?=( !$this->reqdata->ok('rlogin') ? ' has-error' : '' );?>">
					<label for="rlogin" class="col-sm-3 control-label"><?=( $this->regmode == "organization" ? $this->localize->get("reg", "lbl-login") : $this->localize->get("reg", "lbl-email") );?>:</label>
					<div class="col-sm-9"><input type="email" class="form-control" id="rlogin" name="rlogin" placeholder="your_mailbox@somehost.com" value="<?=$this->reqdata->quote($this->reqdata->rlogin);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-email");?></div></div>
				</div>					
	<?php
		if( $this->regmode == "organization" )
		{
	?>
				<div class="form-group<?=( !$this->reqdata->ok('rpasswd1') ? ' has-error' : '' );?>">
					<label for="rpasswd1" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-pass");?>:</label>
					<div class="col-sm-9"><input type="password" class="form-control" id="rpasswd1" name="rpasswd1" placeholder=""><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-pass");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rpasswd1') ? ' has-error' : '' );?>">
					<label for="rpasswd2" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-pass1");?>:</label>
					<div class="col-sm-9"><input type="password" class="form-control" id="rpasswd2" name="rpasswd2" placeholder=""></div>
				</div>
	<?php
		}
	?>
				<div class="form-group<?=( !$this->reqdata->ok('rname') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-name");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rname" name="rname" placeholder="<?=$this->localize->get("reg", "ph-name");?>" value="<?=$this->reqdata->quote($this->reqdata->rname);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-name");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rfname') ? ' has-error' : '' );?>">
					<label for="rfname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-fname");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rfname" name="rfname" placeholder="<?=$this->localize->get("reg", "ph-fname");?>" value="<?=$this->reqdata->quote($this->reqdata->rfname);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-fname");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rtel') ? ' has-error' : '' );?>">
					<label for="rfname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-tel");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rtel" name="rtel" placeholder="<?=$this->localize->get("reg", "ph-tel");?>" value="<?=$this->reqdata->quote($this->reqdata->rtel);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-tel");?></div></div>
				</div>
	<?php 
		if( $this->regmode == "edit_organization" )
		{
				echo '<div class="backpic-select"><input type="hidden" name="backpicid" id="backpicid" value="'.$this->reqdata->backpicid.'">
					<label class="control-label">'.$this->localize->get("reg", "lbl-country").':</label>
					<div class="backpic-list">
						<div class="backpic-it'.( $this->reqdata->backpicid == 1 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon1" href="#" data-picid="1"></a></div>
						<div class="backpic-it'.( $this->reqdata->backpicid == 2 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon2" href="#" data-picid="2"></a></div>
						<div class="backpic-it'.( $this->reqdata->backpicid == 3 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon3" href="#" data-picid="3"></a></div>
						<div class="backpic-it'.( $this->reqdata->backpicid == 4 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon4" href="#" data-picid="4"></a></div>
						<div class="backpic-it'.( $this->reqdata->backpicid == 0 ? ' backpic-it-sel' : '' ).'"><a class="backpic-nofon" href="#" data-picid="0"></a></div>
						<div class="both"></div>
					</div>
				</div>';
		?>
	<script>
	$(document).ready(function(){
		$(".backpic-it a").bind("click", function(){
			$(".backpic-it").removeClass("backpic-it-sel");
			$(this).parent().addClass("backpic-it-sel");
			$("#backpicid").val( $(this).attr("data-picid") );
			return false;
		});
	});
	</script>
	<?php
		}
	?>		
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rcountry') ? ' has-error' : '' );?>">
					<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-country");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="rcountry" name="rcountry" onchange="loc_LoadRegion(this, 'robl', 'rcity', ['<?=$this->localize->get("addbonus", "load-region")?>', '<?=$this->localize->get("addbonus", "load-city")?>'])">
						option value="0">----- <?=$this->localize->get("reg", "lbl-oblsel");?> -----</option>
		<?php
			for( $i=0; $i<count($countries); $i++ )
			{
				echo '<option value="'.$countries[$i]['id'].'"'.($this->reqdata->rcountry == $countries[$i]['id'] ? ' selected' : '').'>'.$countries[$i]['name'].'</option>';
			} 
		?>
					</select></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('robl') ? ' has-error' : '' );?>">
					<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-obl");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="robl" name="robl" onchange="loc_LoadCity(this, 'rcity')">
						<option value="0">----- <?=$this->localize->get("reg", "lbl-oblsel");?> -----</option>
		<?php
			for( $i=0; $i<count($obls); $i++ )
			{
				echo '<option value="'.$obls[$i]['id'].'"'.($this->reqdata->robl == $obls[$i]['id'] ? ' selected' : '').'>'.$obls[$i]['name'].'</option>';
			} 
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-obl");?></div></div>
				</div>					
				<div class="form-group<?=( !$this->reqdata->ok('rcity') ? ' has-error' : '' );?>">
					<label for="rcity" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-city");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="rcity" name="rcity">
						<option value="0">----- <?=$this->localize->get("reg", "lbl-citysel");?> -----</option>
		<?php			
			//$cities = $this->catmodel->Loc_CityList(UKRAINE, $this->reqdata->robl);
			for( $i=0; $i<count($cities); $i++ )
			{
				echo '<option value="'.$cities[$i]['id'].'"'.($this->reqdata->rcity == $cities[$i]['id'] ? ' selected' : '').'>'.$cities[$i]['name'].'</option>';
			}
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-city");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('remplnum') ? ' has-error' : '' );?>">
					<label for="remplnum" class="col-sm-3 control-label"><?=$this->localize->get("registration", "employees")?></label>
					<div class="col-sm-9"><select class="form-control" id="remplnum" name="remplnum">
				<?php
					foreach($employee as $enum => $estr)
					{
						echo '<option value="'.$enum.'"'.( $enum == $this->reqdata->remplnum ? ' selected' : '').'>'.$estr.'</option>';
					} 
					/*
				?>
						<option value="5">до 5</option>
						<option value="10">до 10</option>
						<option value="20">до 20</option>
						<option value="50">до 50</option>
						<option value="100">более 100</option>
					*/
				?>
					</select></div>
				</div>
		<?php
			if( $this->regmode == "edit_organization" )
			{
				$avatar_pic = WWWHOST.'img/no-pic.png';
				
				if( $this->reqdata->pic != "" )
					$avatar_pic = WWWHOST.$this->reqdata->pic;
		?>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3"><img src="<?=$avatar_pic;?>" class="img-thumbnail" width="80" height="80" alt=""></div>
					</div>
					<div class="form-group">
						<label for="rfile" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-logo");?>:</label>
						<div class="col-sm-9">
							<input type="file" id="rfile" name="rfile">						
						</div>					
					</div>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
						<input type="submit" name="btnsetphoto" class="btn btn-primary" value="<?=$this->localize->get("reg", "lbl-loadphoto")?>">
						</div>
					</div>		
		<?php
			}
		?>				
			</div>				
		</div>				
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="form-group">
					<label for="rdescr" class="control-label"><?=$this->localize->get("reg", "lbl-descr");?></label>
					<textarea class="form-control" id="rdescr" name="rdescr" rows="4"><?=$this->reqdata->rdescr;?></textarea>
				</div>
			</div>
		</div>		
	</div>	
</div>
<?php 
		if( $this->regmode == "organization" )
		{
?>
<div class="container">
	<div class="row form-horizontal" style="padding: 18px 0 30px 0;">
		<div class="col-xs-6 col-md-6">
			<div class="form-group<?=( !$this->reqdata->ok('rseccode') ? ' has-error' : '' );?>">
				<label for="rseccode" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-captcha");?>:</label>
				<div class="col-sm-9"><input type="text" class="form-control" id="rseccode" name="rseccode" placeholder=""><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-captcha");?></div></div>
			</div>
		</div>
		<div class="col-xs-6 col-md-5 col-md-offset-1">
			<img src="<?=$this->Page_BuildUrl("captcha", "code", "inm=".$cryptcode);?>" width="100" height="30" alt="">
		</div>		
	</div>
	<div class="text-center">
		<div class="checkbox">
			<label>
			<input type="checkbox" id="rrule" name="rrule" value="1">
				<?=$this->localize->get("registration", "agree")?> <a href="<?=$ruleslnk;?>" target="_blank"><?=$this->localize->get("registration", "rules")?></a> <?=$this->localize->get("registration", "undertake")?>
			</label>
		</div>
	</div>
	<div class="text-center"><input type="submit" class="btn btn-primary" value="<?=$this->localize->get("popreg", "btn-reg")?>" /></div>
</div>
<?php
		}
		else // edit
		{
?>
<div class="container">
	<div class="text-center btn-form-pad"><input type="submit"  class="btn btn-primary" value="<?=$this->localize->get("addbonus", "btn-save")?>" /></div>
</div>
<?php		
		} 
?>
</form>
<?php		
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Форма добавления или редактирования компании
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else if( ($this->regmode == "company") || ($this->regmode == "edit_company") )
	{
		$cryptcode = "";
		$h1_str = $this->localize->get("registration", "step-1");
		if( $this->regmode == "company" )
		{
			$cryptcode = $this->captcha->generate_code();
?>
<div class="container">
	<div class="info">
	<?=( $PM->get_txtres()->regformcomp['text'] );?>
	</div>
</div>
<?php
		}
		else	// edit
		{
			$h1_str = $this->localize->get("registration", "reg-data");
		}
		
		$obls = $this->catmodel->Loc_OblList($this->reqdata->rcountry);
		//$cities = $this->catmodel->Loc_CityList(0, $this->reqdata->robl);
?>
<form class="regform" id="regformcomp" action="<?=( $this->regmode == "company" ? $this->Page_BuildUrl("registration", "docompany") : $this->Page_BuildUrl("cabinet", "saveme") );?>" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="makeregcomp">
<input type="hidden" name="controlcode" value="<?=$cryptcode;?>">
<div class="row-gray">
	<div class="container">
		<a id="formstart" name="formstart"></a>
		<div class="h2"><?=$h1_str;?></div>
	<?php
		if( $this->reqdata->has_errors() )
		{			
			echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
		}
		if( isset($this->msg) && ($this->msg != "") )
		{
			echo '<div class="frm-notify-msg">'.$this->msg.'</div>';
		}
	?>		
		<div class="row form-horizontal">			
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rorg') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-org");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rorg" name="rorg" placeholder="<?=$this->localize->get("reg", "ph-org");?>" value="<?=$this->reqdata->quote($this->reqdata->rorg);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-org");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rorgsphere') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-sphere");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rorgsphere" name="rorgsphere" placeholder="<?=$this->localize->get("reg", "ph-sphere");?>" value="<?=$this->reqdata->quote($this->reqdata->rorgsphere);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-sphere");?></div></div>
				</div>
				
				<div class="form-group<?=( !$this->reqdata->ok('rlogin') ? ' has-error' : '' );?>">
					<label for="rlogin" class="col-sm-3 control-label"><?=( $this->regmode == "company" ? $this->localize->get("reg", "lbl-login") : $this->localize->get("reg", "lbl-email") );?>:</label>
					<div class="col-sm-9"><input type="email" class="form-control" id="rlogin" name="rlogin" placeholder="your_mailbox@somehost.com" value="<?=$this->reqdata->quote($this->reqdata->rlogin);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-email");?></div></div>
				</div>					
	<?php
		if( $this->regmode == "company" )
		{
	?>
				<div class="form-group<?=( !$this->reqdata->ok('rpasswd1') ? ' has-error' : '' );?>">
					<label for="rpasswd1" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-pass");?>:</label>
					<div class="col-sm-9"><input type="password" class="form-control" id="rpasswd1" name="rpasswd1" placeholder=""><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-pass");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rpasswd1') ? ' has-error' : '' );?>">
					<label for="rpasswd2" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-pass1");?>:</label>
					<div class="col-sm-9"><input type="password" class="form-control" id="rpasswd2" name="rpasswd2" placeholder=""></div>
				</div>
	<?php
		}
	?>				
				<div class="form-group<?=( !$this->reqdata->ok('rname') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-name");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rname" name="rname" placeholder="<?=$this->localize->get("reg", "ph-name");?>" value="<?=$this->reqdata->quote($this->reqdata->rname);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-name");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rfname') ? ' has-error' : '' );?>">
					<label for="rfname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-fname");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rfname" name="rfname" placeholder="<?=$this->localize->get("reg", "ph-fname");?>" value="<?=$this->reqdata->quote($this->reqdata->rfname);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-fname");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rtel') ? ' has-error' : '' );?>">
					<label for="rfname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-tel");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rtel" name="rtel" placeholder="<?=$this->localize->get("reg", "ph-tel");?>" value="<?=$this->reqdata->quote($this->reqdata->rtel);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-tel");?></div></div>
				</div>
	<?php 
		if( $this->regmode == "edit_company" )
		{
				echo '<div class="backpic-select"><input type="hidden" name="backpicid" id="backpicid" value="'.$this->reqdata->backpicid.'">
					<label class="control-label">'.$this->localize->get("reg", "lbl-country").':</label>
					<div class="backpic-list">
						<div class="backpic-it'.( $this->reqdata->backpicid == 1 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon1" href="#" data-picid="1"></a></div>
						<div class="backpic-it'.( $this->reqdata->backpicid == 2 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon2" href="#" data-picid="2"></a></div>
						<div class="backpic-it'.( $this->reqdata->backpicid == 3 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon3" href="#" data-picid="3"></a></div>
						<div class="backpic-it'.( $this->reqdata->backpicid == 4 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon4" href="#" data-picid="4"></a></div>
						<div class="backpic-it'.( $this->reqdata->backpicid == 0 ? ' backpic-it-sel' : '' ).'"><a class="backpic-nofon" href="#" data-picid="0"></a></div>
						<div class="both"></div>
					</div>
				</div>';
		?>
	<script>
	$(document).ready(function(){
		$(".backpic-it a").bind("click", function(){
			$(".backpic-it").removeClass("backpic-it-sel");
			$(this).parent().addClass("backpic-it-sel");
			$("#backpicid").val( $(this).attr("data-picid") );
			return false;
		});
	});
	</script>
	<?php
		}
	?>			
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rcountry') ? ' has-error' : '' );?>">
					<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-country");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="rcountry" name="rcountry" onchange="loc_LoadRegion(this, 'robl', 'rcity', ['<?=$this->localize->get("addbonus", "load-region")?>', '<?=$this->localize->get("addbonus", "load-city")?>'])">
						option value="0">----- <?=$this->localize->get("reg", "lbl-oblsel");?> -----</option>
		<?php
			for( $i=0; $i<count($countries); $i++ )
			{
				echo '<option value="'.$countries[$i]['id'].'"'.($this->reqdata->rcountry == $countries[$i]['id'] ? ' selected' : '').'>'.$countries[$i]['name'].'</option>';
			} 
		?>
					</select></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('robl') ? ' has-error' : '' );?>">
					<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-obl");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="robl" name="robl" onchange="loc_LoadCity(this, 'rcity')">
		<?php
			for( $i=0; $i<count($obls); $i++ )
			{
				echo '<option value="'.$obls[$i]['id'].'"'.($this->reqdata->robl == $obls[$i]['id'] ? ' selected' : '').'>'.$obls[$i]['name'].'</option>';
			} 
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-obl");?></div></div>
				</div>					
				<div class="form-group<?=( !$this->reqdata->ok('rcity') ? ' has-error' : '' );?>">
					<label for="rcity" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-city");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="rcity" name="rcity">
						<option value="0">----- <?=$this->localize->get("reg", "lbl-citysel");?> -----</option>
		<?php			
			//$cities = $this->catmodel->Loc_CityList(UKRAINE, $this->reqdata->robl);
			for( $i=0; $i<count($cities); $i++ )
			{
				echo '<option value="'.$cities[$i]['id'].'"'.($this->reqdata->rcity == $cities[$i]['id'] ? ' selected' : '').'>'.$cities[$i]['name'].'</option>';
			} 
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-city");?></div></div>
				</div>
				
				<div class="form-group<?=( !$this->reqdata->ok('remplnum') ? ' has-error' : '' );?>">
					<label for="remplnum" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-empl");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="remplnum" name="remplnum">
				<?php
					foreach($employee as $enum => $estr)
					{
						echo '<option value="'.$enum.'"'.( $enum == $this->reqdata->remplnum ? ' selected' : '').'>'.$estr.'</option>';
					} 
					/*
				?>
						<option value="5">до 5</option>
						<option value="10">до 10</option>
						<option value="20">до 20</option>
						<option value="50">до 50</option>
						<option value="100">более 100</option>
					*/
				?>
					</select></div>
				</div>
	<?php
		if( $this->regmode == "edit_company" )
		{
			$avatar_pic = WWWHOST.'img/no-pic.png';
			
			if( $this->reqdata->pic != "" )
				$avatar_pic = WWWHOST.$this->reqdata->pic;
	?>
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3"><img src="<?=$avatar_pic;?>" class="img-thumbnail" width="80" height="80" alt=""></div>
				</div>
				<div class="form-group">
					<label for="rfile" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-logo");?>:</label>
					<div class="col-sm-9">
						<input type="file" id="rfile" name="rfile">						
					</div>					
				</div>
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
					<input type="submit" name="btnsetphoto" class="btn btn-primary" value="<?=$this->localize->get("reg", "lbl-loadphoto")?>">
					</div>
				</div>		
	<?php
		}
	?>				
			</div>				
		</div>				
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="form-group">
					<label for="rdescr" class="control-label"><?=$this->localize->get("reg", "lbl-descr");?></label>
					<textarea class="form-control" id="rdescr" name="rdescr" rows="4"><?=$this->reqdata->rdescr;?></textarea>
				</div>
			</div>
		</div>		
	</div>	
</div>
<?php 
		if( $this->regmode == "company" )
		{
?>
<div class="container">
	<div class="row form-horizontal" style="padding: 18px 0 30px 0;">
		<div class="col-xs-6 col-md-6">
			<div class="form-group<?=( !$this->reqdata->ok('rseccode') ? ' has-error' : '' );?>">
				<label for="rseccode" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-captcha");?>:</label>
				<div class="col-sm-9"><input type="text" class="form-control" id="rseccode" name="rseccode" placeholder=""><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-captcha");?></div></div>
			</div>
		</div>
		<div class="col-xs-6 col-md-5 col-md-offset-1">
			<img src="<?=$this->Page_BuildUrl("captcha", "code", "inm=".$cryptcode);?>" width="100" height="30" alt="">
		</div>		
	</div>
	<div class="text-center">
		<div class="checkbox">
			<label>
			<input type="checkbox" id="rrule" name="rrule" value="1">
				<?=$this->localize->get("registration", "agree")?> <a href="<?=$ruleslnk;?>" target="_blank"><?=$this->localize->get("registration", "rules")?></a> <?=$this->localize->get("registration", "undertake")?>
			</label>
		</div>
	</div>
	<div class="text-center"><input type="submit" class="btn btn-primary" value="<?=$this->localize->get("poplog", "btn-reg")?>" /></div>
</div>
<?php
		}
		else // edit
		{
?>
<div class="container">
	<div class="text-center btn-form-pad"><input type="submit" class="btn btn-primary" value="<?=$this->localize->get("addbonus", "btn-save")?>" /></div>
</div>
<?php		
		} 
?>
</form>
<?php		
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	 Форма добавления или редактирования обычного пользователя и пользователя соц.сети
	//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	else
	{
		$cryptcode = "";
		$h1_str = $this->localize->get("registration", "step-1");
		if( ($this->regmode == "person") || ($this->regmode == "personfb") || ($this->regmode == "personvk") || ($this->regmode == "personok") )
		{
			// Facebook
			$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);
			
			$FB_REDIRECT_URL = WWWHOST.'login/redirfb';
			$FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);
			
			// Vkontakte
			$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);
			
			$VK_REDIRECT_URL = WWWHOST.'login/redirvk';
			$VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);
			
			// Odnoklassniki
			$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);
			
			$OK_REDIRECT_URL = WWWHOST.'login/redirok';
			$OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);
			
			$cryptcode = $this->captcha->generate_code();
?>
<div class="container">
	<div class="info">
	<?=( $PM->get_txtres()->regformpers['text'] );?>
	</div>
	<div class="row reg-sc-login">
		<a target="_blank" href="<?=$FB_LINK;?>" title="<?=$this->localize->get("popreg", "btn-fb");?>"><img src="<?=WWWHOST?>img/btn-log-fb.png" width="170" height="34" alt="<?=$this->localize->get("popreg", "btn-fb");?>"></a>
		<a target="_blank" href="<?=$VK_LINK;?>" title="<?=$this->localize->get("popreg", "btn-vk");?>"><img src="<?=WWWHOST?>img/btn-log-vk.png" width="180" height="34" alt="<?=$this->localize->get("popreg", "btn-vk");?>"></a>
		<a target="_blank" href="<?=$OK_LINK;?>" title="<?=$this->localize->get("popreg", "btn-ok");?>"><img src="<?=WWWHOST?>img/btn-log-od.png" width="212" height="34" alt="<?=$this->localize->get("popreg", "btn-ok");?>"></a>
	</div>
</div>
			<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=Emp1CBwO*kVIL72Q8/PkAX3oXNWRyxHxLEPHoBbNxGNbrDCiHgnJdF1CEgr9LmeTc8eYKs7rfchxkJj5XXGfSEbtPdnojG2S6S84SWoyeDdvt7VIUK0HEn7zChRzlshi0/avWRMBhzTqhTppP2Yk9hdDT9OtMAPPvnRVfB0SxTE-';</script>
			<script>fbq('track', 'CompleteRegistration');</script>
			<?php
		}
		else	// edit
		{
			$h1_str = $this->localize->get("registration", "reg-data");
		}
		
		$FRM_URL = $this->Page_BuildUrl("registration", "doperson");
		switch($this->regmode)
		{
			case "personfb":	$FRM_URL = $this->Page_BuildUrl("registration", "dopersonfb");	break;
			case "personvk":	$FRM_URL = $this->Page_BuildUrl("registration", "dopersonvk");	break;
			case "personok":	$FRM_URL = $this->Page_BuildUrl("registration", "dopersonok");	break;
			case "edit_person":	$FRM_URL = $this->Page_BuildUrl("cabinet", "saveme");	break;
		}

		$obls = $this->catmodel->Loc_OblList($this->reqdata->rcountry);

		//$cities = $this->catmodel->Loc_CityList(UKRAINE, $this->reqdata->robl);
?>
<form class="regform" id="regformpers" action="<?=$FRM_URL;?>" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="action" value="makeregperson">
	<input type="hidden" name="controlcode" value="<?=$cryptcode;?>">
	<input type="hidden" name="uhash" value="<?=$this->reqdata->uhash?>" >
<div class="row-gray">
	<div class="container">
		<a id="formstart" name="formstart"></a>
		<div class="h2"><?=$h1_str;?></div>
	<?php
		if( $this->reqdata->has_errors() )
		{			
			echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
		}
		if( isset($this->msg) && ($this->msg != "") )
		{
			echo '<div class="frm-notify-msg">'.$this->msg.'</div>';
		}
		
		/*
	?>		
		<div class="row form-horizontal">			
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rname') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label">Ваше имя:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rname" name="rname" placeholder="Ваше имя" value="<?=$this->reqdata->quote($this->reqdata->rname);?>"></div>
				</div>					
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rfname') ? ' has-error' : '' );?>">
					<label for="rfname" class="col-sm-3 control-label">Фамилия:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rfname" name="rfname" placeholder="Ваша фамилия" value="<?=$this->reqdata->quote($this->reqdata->rfname);?>"></div>
				</div>
			</div>
		</div>
		*/
	?>
		<div class="row form-horizontal">			
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rname') ? ' has-error' : '' );?>">
					<label for="rname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-name");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rname" name="rname" placeholder="<?=$this->localize->get("reg", "ph-name");?>" value="<?=$this->reqdata->quote($this->reqdata->rname);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-name");?></div></div>
				</div>					
				<div class="form-group<?=( !$this->reqdata->ok('rfname') ? ' has-error' : '' );?>">
					<label for="rfname" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-fname");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rfname" name="rfname" placeholder="<?=$this->localize->get("reg", "ph-fname");?>" value="<?=$this->reqdata->quote($this->reqdata->rfname);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-fname");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rlogin') ? ' has-error' : '' );?>">
					<label for="rlogin" class="col-sm-3 control-label"><?=( $this->regmode == "person" ? $this->localize->get("reg", "lbl-login") : $this->localize->get("reg", "lbl-email") );?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rlogin" name="rlogin" placeholder="your_mailbox@somehost.com" value="<?=$this->reqdata->quote($this->reqdata->rlogin);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-email");?></div></div>
				</div>
		<?php
			if( ($this->regmode == "personfb") || ($this->regmode == "personvk") || ($this->regmode == "personok") || ($this->regmode == "edit_person") )
			{
				//
			}
			else
			{		
		?>			
				<div class="form-group<?=( !$this->reqdata->ok('rpasswd1') ? ' has-error' : '' );?>">
					<label for="rpasswd1" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-pass");?>:</label>
					<div class="col-sm-9"><input type="password" class="form-control" id="rpasswd1" name="rpasswd1" placeholder=""><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-pass");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('rpasswd1') ? ' has-error' : '' );?>">
					<label for="rpasswd2" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-pass1");?>:</label>
					<div class="col-sm-9"><input type="password" class="form-control" id="rpasswd2" name="rpasswd2" placeholder=""></div>
				</div>
		<?php
			}
		?>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rcountry') ? ' has-error' : '' );?>">
					<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-country");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="rcountry" name="rcountry" onchange="loc_LoadRegion(this, 'robl', 'rcity', ['<?=$this->localize->get("addbonus", "load-region")?>', '<?=$this->localize->get("addbonus", "load-city")?>'])">
		<?php
			for( $i=0; $i<count($countries); ++$i ) {
				echo '<option value="'.$countries[$i]['id'].'"'.($this->reqdata->rcountry == $countries[$i]['id'] ? ' selected' : '').'>'.$countries[$i]['name'].'</option>';
			} 
		?>
					</select></div>
				</div>				
				<div class="form-group<?=( !$this->reqdata->ok('robl') ? ' has-error' : '' );?>">
					<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-obl");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="robl" name="robl" onchange="loc_LoadCity(this, 'rcity')">
						<option value="0">----- <?=$this->localize->get("reg", "lbl-oblsel");?> -----</option>
		<?php
			$obls = $this->catmodel->Loc_OblList($this->reqdata->rcountry == 0 ? UKRAINE : $this->reqdata->rcountry);

			for( $i=0; $i<count($obls); ++$i ) {
				echo '<option value="'.$obls[$i]['id'].'"'.($this->reqdata->robl == $obls[$i]['id'] ? ' selected' : '').'>'.$obls[$i]['name'].'</option>';
			} 
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-obl");?></div></div>
				</div>					
				<div class="form-group<?=( !$this->reqdata->ok('rcity') ? ' has-error' : '' );?>">
					<label for="rcity" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-city");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="rcity" name="rcity">
						<option value="0">----- <?=$this->localize->get("reg", "lbl-citysel");?> -----</option>
		<?php
			//$cities = $this->catmodel->Loc_CityList($this->reqdata->rcountry, $this->reqdata->robl);
			$cities = (($this->reqdata->rcountry > 0 && $this->reqdata->robl > 0) ? $this->catmodel->Loc_CityList($this->reqdata->rcountry, $this->reqdata->robl) : []);

			for( $i=0; $i<count($cities); ++$i ) {
				echo '<option value="'.$cities[$i]['id'].'"'.($this->reqdata->rcity == $cities[$i]['id'] ? ' selected' : '').'>'.$cities[$i]['name'].'</option>';
			} 
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-city");?></div></div>
				</div>
				
				<div class="form-group<?=( !$this->reqdata->ok('rprof') ? ' has-error' : '' );?>">
					<label for="rprof" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-prof");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rprof" name="rprof" placeholder="<?=$this->localize->get("reg", "ph-prof");?>" value="<?=$this->reqdata->quote($this->reqdata->rprof);?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-prof");?></div></div>
				</div>
	<?php
		if( $this->regmode == "edit_person" )
		{
			$avatar_pic = WWWHOST.'img/no-pic.png';
			
			if( $this->reqdata->pic != "" )
				$avatar_pic = WWWHOST.$this->reqdata->pic;
	?>
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3"><img src="<?=$avatar_pic;?>" class="img-thumbnail" width="80" height="80" alt=""></div>
				</div>
				<div class="form-group">
					<label for="rfile" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-avatar");?>:</label>
					<div class="col-sm-9">
						<input type="file" id="rfile" name="rfile">
						<!-- <p class="help-block">Example block-level help text here.</p>-->
					</div>					
				</div>
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
					<input type="submit" name="btnsetphoto" class="btn btn-primary" value="<?=$this->localize->get("reg", "lbl-loadphoto");?>">
					</div>
				</div>		
	<?php
		}
		else if( ($this->regmode == "personfb") || ($this->regmode == "personvk")  )
		{
			if( $this->reqdata->pic != "" )
			{
				//$avatar_pic = WWWHOST.'img/no-pic.png';
				$avatar_pic = $this->reqdata->pic;
	?>
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3"><img src="<?=$avatar_pic;?>" class="img-thumbnail" width="140" height="140" alt=""></div>
				</div>
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<div class="checkbox">
							<label>
							<input type="checkbox" id="rusepic" name="rusepic" value="1" checked>
							<?=$this->localize->get("reg", "lbl-avatar-use");?>
							</label>
						</div>
					</div>
				</div>		
	<?php
			}
		}
		
	?>
			</div>			
		</div>
	<?php 
	/*
		<div class="row form-horizontal">			
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rprof') ? ' has-error' : '' );?>">
					<label for="rprof" class="col-sm-3 control-label">Профессия:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="rprof" name="rprof" placeholder="Например: адвокат" value="<?=$this->reqdata->quote($this->reqdata->rprof);?>"></div>
				</div>					
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('robl') ? ' has-error' : '' );?>">
					<label for="robl" class="col-sm-3 control-label">Регион:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="robl" name="robl" placeholder="Ваш регион" value="<?=$this->reqdata->quote($this->reqdata->robl);?>"></div>
				</div>
			</div>
		</div>
	*/
	?>			
			
	</div>	
</div>
<div class="container">
<?php
	if( $this->regmode != "edit_person" )
	{
		if( ($this->regmode == "personfb") || ($this->regmode == "personvk") || ($this->regmode == "personok") )
		{
			//
		}
		else
		{		
?>
	<div class="row form-horizontal" style="padding: 18px 0 30px 0;">
		<div class="col-xs-6 col-md-6">
			<div class="form-group<?=( !$this->reqdata->ok('rseccode') ? ' has-error' : '' );?>">
				<label for="rseccode" class="col-sm-3 control-label"><?=$this->localize->get("reg", "lbl-captcha");?>:</label>
				<div class="col-sm-9"><input type="text" class="form-control" id="rseccode" name="rseccode" placeholder=""><div class="alert alert-danger" role="alert"><?=$this->localize->get("reg", "err-captcha");?></div></div>
			</div>
		</div>
		<div class="col-xs-6 col-md-5 col-md-offset-1">
			<img src="<?=$this->Page_BuildUrl("captcha", "code", "inm=".$cryptcode);?>" width="100" height="30" alt="">
		</div>		
	</div>
<?php
		}
?>
	<div class="text-center">
		<div class="checkbox">
			<label>
			<input type="checkbox" id="rrule" name="rrule" value="1">
			<?=$this->localize->get("registration", "agree")?> <a href="<?=$ruleslnk;?>" target="_blank"><?=$this->localize->get("registration", "reles")?></a> <?=$this->localize->get("registration", "undertake")?>
			</label>
		</div>
	</div>
	<div class="text-center btn-form-pad"><input type="submit" onclick="ga('send', 'event', 'try_registration', 'push_button_registration')" class="btn btn-primary" value="<?=$this->localize->get("poplog", "btn-reg")?>" /></div>
<?php
	}
	else {
		echo '<div class="text-center btn-form-pad"><input type="submit" class="btn btn-primary" value="'.$this->localize->get("addbonus", "btn-save").'" /></div>';
	}
?>	
</div>
</form>
<?php
	}
?>
<br><br>