<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$show_send_msg_lnk = false;

	////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();
	
	// Set page head title (h1 tag value)
	$PAGE_H1 = ( isset($this->seo[SEO_H1]) ? $this->seo[SEO_H1] : $page->title );
	
	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);
	
	//if( isset($this->title) )
	//	$PAGE_H1 = $this->title;
?>
<!--<div class="row-head">
	<div class="container">
		<ol class="breadcrumb">
			<?=$BCHTML;?>
		</ol>
		<h1><?=$PAGE_H1;?></h1>
	</div>
</div>-->
<div class="row">
	<div class="top-block col-md-12 col-xs-12 col-lg-12">
		<div class="top-block-txt top-trate-block-txt">
			<div class="container">
				<ol class="breadcrumb">
					<?=$BCHTML;?>
				</ol>
			</div>
			<h1><?=$this->pageInfo[0]['page_text']?></h1>
		</div>
	</div>
</div>
	<div class="row">
		<div class="container des-cont-all des-trat-con">
			<div class="fees-about rat-about"><?=$this->pageInfo[1]['page_text']?></div>
<?php
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	if( $this->viewMode == "toptable" ) {
?>
	<div class="info info-minheight text-center">
<?php

	echo '<div class="ratingTblRow">'.$this->build_RateTable().'</div>';
	
	if( $this->seo[SEO_TEXT2] != "" )
		echo $this->seo[SEO_TEXT2]."<br>";
?>
	</div>
</div>
		</div>
<?php	
	}
?>