<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$show_send_msg_lnk = false;

	////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();
	
	// Set page head title (h1 tag value)
	$PAGE_H1 = $page->title;	
	
	// Build bread crumbs
	$this->addBreadcrumbs(WWWHOST.'bonus/list/','Каталог бонусов');
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);
	
	if( isset($this->projinfo) && ($this->viewMode == "") )
		$PAGE_H1 = $this->projinfo['title'];
	
	if( isset($this->title) )
		$PAGE_H1 = $this->title;
?>
	<div class="row-head breacrumb-bgi">
		<div class="container">
			<ol class="breadcrumb">
				<?=$BCHTML;?>
			</ol>
			<h1><?=$PAGE_H1;?></h1>
		</div>
	</div>
<?php
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	if( $this->viewMode == "notallowed" )
	{
?>
<div class="container">
	<div class="info info-minheight text-center">
<?php 
	echo $PM->get_txtres()->newprojnotallowed['text'].'<br>';
	echo '<a href="'.$this->page_BuildUrl("cabinet","needhelp").'">'.$this->localize->get("proj", "notallowed").'</a>';
?>
	</div>
</div>		
<?php	
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	else if( $this->viewMode == "bonusdone" )
	{
?>
<div class="container">
	<div class="info info-minheight text-center">
<?php 
	echo $PM->get_txtres()->prodreqsend['text'].'<br>';
	echo '<a href="'.$this->page_BuildUrl("proj","view/".$this->projinfo['id']).'">'.$this->localize->get("proj", "backtoproj").' &quot;'.$this->projinfo['title2'].'&quot;</a>';
?>
	</div>
</div>		
<?php	
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	
	else if( $this->viewMode == "saved" )
	{
		?>
	<div class="container">
		<div class="info info-minheight text-center">
	<?php 
		//echo $PM->get_txtres()->prodreqsend['text'].'<br>';
		echo '<a href="'.$this->page_BuildUrl("cabinet","needhelp").'">'.$this->localize->get("proj", "backtocab").'</a>';
	?>
		</div>
	</div>		
	<?php	
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	else if( $this->viewMode == "created" )		
	{
?>
<div class="container">
	<div class="info">
	<?=( $page->content );?>
		<div class="addproj-face">
			<div class="tbl-noitems">
				<div class="tbl-noitems-txt">
					<div><span><?=$this->localize->get("addbonus", "photo-promo");?></span></div>
				</div>
				<div class="tbl-noitems-face"></div>
				<div class="both"></div>			
			</div>
			<div class="both"></div>
		</div>
	</div>
</div>
<form id="projformpic" action="<?=$this->Page_BuildUrl("bonus", "addmedia");?>" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="addpublish">
<input type="hidden" name="projid" value="<?=$this->projid;?>">
<div class="row-gray">
	<div class="container">
		<a id="formstart" name="formstart"></a>
		<div class="h2 text-center"><?=$this->localize->get("addbonus", "photo-hdr");?></div>
	<?php
	/*
		<div class="row">
			<div class="col-xs-12 col-md-12">
	<?php
		echo '<div class="backpic-select"><input type="hidden" name="backpicid" id="backpicid" value="'.$this->backpicid.'">
			<label class="control-label">'.$this->localize->get("addproj", "lbl-background").':</label>
			<div class="backpic-list">
				<div class="backpic-it'.( $this->backpicid == 1 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon1" href="#" data-picid="1"></a></div>
				<div class="backpic-it'.( $this->backpicid == 2 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon2" href="#" data-picid="2"></a></div>
				<div class="backpic-it'.( $this->backpicid == 3 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon3" href="#" data-picid="3"></a></div>
				<div class="backpic-it'.( $this->backpicid == 4 ? ' backpic-it-sel' : '' ).'"><a class="backpic-fon4" href="#" data-picid="4"></a></div>
				<div class="backpic-it'.( $this->backpicid == 0 ? ' backpic-it-sel' : '' ).'"><a class="backpic-nofon" href="#" data-picid="0"></a></div>
				<div class="both"></div>
			</div>
		</div>';
	?>
<script>
$(document).ready(function(){
	$(".backpic-it a").bind("click", function(){
		$(".backpic-it").removeClass("backpic-it-sel");
		$(this).parent().addClass("backpic-it-sel");
		$("#backpicid").val( $(this).attr("data-picid") );
		return false;
	});
});
</script>
			</div>
		</div>
	<?php 
	*/
	
		if( $this->userinfo['pic'] == '' )
		{
	?>
		<div class="row row-form-pad">
			<div class="col-xs-3 col-md-3">
				<div class="form-group">
					<label for="fileuser" class="control-label"><?=$this->localize->get("addbonus", "photo-user-lbl");?>:</label>
					<div><input type="file" id="fileuser" name="pfileuser"></div>
				</div>
			</div>
			<div class="col-xs-3 col-md-2 col-btn-pad">
				<input type="submit" name="addavatarbtn" class="btn btn-primary" value="<?=$this->localize->get("addbonus", "photo-user-btn");?>" />
			</div>
		</div>
	<?php 
		}
	?>
		<div class="row row-form-pad">
	<?php
		$pics = $this->projpics;
		$pic_num = count($pics);
		
		for( $i=0; $i<count($pics); $i++ )
		{
			echo '<div class="col-xs-4 col-md-3">
				<img src="'.WWWHOST.stripslashes($pics[$i]['filename_ico']).'" class="img-thumbnail center-block" alt="">
				<div class="proj-pic-del"><a href="'.$this->Page_BuildUrl("bonus", "delmedia", "projid=".$this->projid."&picid=".$pics[$i]['id']).'">'.$this->localize->get("addbonus", "photo-del").'</a></div>
			</div>';
		}
	?>
			<div class="col-xs-3 col-md-3">
				<div class="form-group">
					<label for="file1" class="control-label"><?=$this->localize->get("addbonus", "photo-lbl");?>:</label>
					<div><input type="file" id="file1" name="pfile1"></div>
				</div>
			</div>
			<div class="col-xs-3 col-md-2 col-btn-pad">
				<input type="submit" id="addpicbtn" name="addpicbtn" class="btn btn-primary" value="<?=$this->localize->get("addbonus", "photo-btn");?>" />
			</div>
		</div>
	</div>	
</div>
<div class="container">
	<div class="text-center btn-form-pad"><input type="submit" id="btnprojfinish" class="btn btn-primary" value="<?=$this->localize->get("addbonus", "photo-btn-finish");?>" /></div>
</div>
</form>
<?php
		if( count($pics) == 0 )
		{
?>
<div class="poptip-promo">
	<div class="poptip-promo-cont">
		<div class="poptip-close"><a href="#" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-remove"></span></a></div>
		<div class="poptip-info"><?=( $PM->get_txtres()->newprojpopaddpic['text'] );?></div>
	</div>
</div>
<?php
		//$PM->setPromoSeen();
		}

?>
<script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
<script>
$(document).ready(function(){
	$("#btnprojfinish").bind("click", function() {
		popWnd('sharedlg', 'projid=<?=$this->projid;?>'+'&mode=bonus');
		return false;
	});

	$(".wnd-close a").on("click", function(){
		$("#projformpic").submit();
	});
	<?php
		if( count($pics) == 0 )
		{		
	?>
		$(".poptip-close a").bind("click", function(){
			$(".poptip-promo").hide();	//removeClass("poptip-promo-show");
			return false
		});
	
		var pos = $("#addpicbtn").offset();
		$(".poptip-promo").css("left", Math.round(pos.left - 50)+"px");
		$(".poptip-promo").css("top", Math.round(pos.top - 280)+"px");
		$(".poptip-promo").addClass("poptip-promo-show");
	<?php
		}
	?> 
});
</script>	
<?php	
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	else if( $this->viewMode == "create" || $this->viewMode == "edit" )
	{
		$countries = $this->catmodel->Loc_CountryList();
		$obls = $this->catmodel->Loc_OblList($this->reqdata->rcountry);
?>
<div class="container">
	<div class="info">
	<?=( $this->viewMode == "create" ? $page->header : "" );?>
	</div>
</div>
<form id="projformadd" class="regform" action="<?=( $this->viewMode == "edit" ? $this->Page_BuildUrl("bonus", "save") : $this->Page_BuildUrl("bonus", "publish") );?>" method="POST" enctype="multipart/form-data">
<?php
		if( $this->viewMode == "edit" )
		{
			echo '<input type="hidden" name="action" value="savepublish">
			<input type="hidden" name="projid" value="'.$this->reqdata->id.'">';
		}
		else
		{
			echo '<input type="hidden" name="action" value="makepublish">';			
		}
?>
<div class="row-gray">
	<div class="container">
		<a id="formstart" name="formstart"></a>
		<div class="h2"><?=$this->localize->get("addbonus", "hdr");?></div>
<?php
		if( $this->reqdata->has_errors() )
		{
			echo '<div class="frm-error-msg">'.$this->reqdata->get_msglist().'</div>';
		}
?>
		<div class="row">			
			<div class="col-xs-12 col-md-12">
				<div class="form-group<?=( !$this->reqdata->ok('tit') ? ' has-error' : '' );?>">
					<label for="rname" class="control-label"><?=$this->localize->get("addbonus", "lbl-tit");?>:</label>
					<input type="text" class="form-control" id="tit" name="tit" placeholder="<?=$this->localize->get("addbonus", "ph-tit");?>" value="<?=$this->reqdata->quote($this->reqdata->tit);?>">
					<div class="alert alert-danger" role="alert"><?=$this->localize->get("addbonus", "err-tit");?></div>
				</div>
			</div>
		</div>
		<div class="row form-horizontal jhscope">
			<div class="col-xs-12 col-md-6">
				<div id="amountrow" class="form-group<?=( !$this->reqdata->ok('pamount') ? ' has-error' : '' );?>">
					<label id="lblamount" for="pamount" class="col-sm-3 control-label"><?=$this->localize->get("addbonus", "lbl-amount");?>:</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="pamount" name="pamount" value="<?=$this->reqdata->pamount;?>">
						<div class="alert alert-danger" role="alert"><?=$this->localize->get("addbonus", "err-amount");?></div>
					</div>					
				</div>
				<div class="form-group">
					<label for="diff" class="col-sm-3 control-label"><?=$this->localize->get("addbonus", "lbl-difficulty");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="diff" name="diff" value="<?=$this->reqdata->stdt;?>">
						<option value="26"<?=($this->reqdata->diff == 26 ? ' selected' : '');?>>Легко</option>
						<option value="32"<?=($this->reqdata->diff == 32 ? ' selected' : '');?>>Средний</option>
						<option value="64"<?=($this->reqdata->diff == 64 ? ' selected' : '');?>>Сложный</option>
					</select></div>
				</div>		
				<div class="form-group<?=( !$this->reqdata->ok('stdt') ? ' has-error' : '' );?>">
					<label for="stdt" class="col-sm-3 control-label"><?=$this->localize->get("addbonus", "lbl-dtst");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="stdt" name="stdt" value="<?=$this->reqdata->stdt;?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("addbonus", "err-stdt");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('endt') ? ' has-error' : '' );?>">
					<label for="endt" class="col-sm-3 control-label"><?=$this->localize->get("addbonus", "lbl-dten");?>:</label>
					<div class="col-sm-9"><input type="text" class="form-control" id="endt" name="endt" value="<?=$this->reqdata->endt;?>"><div class="alert alert-danger" role="alert"><?=$this->localize->get("addbonus", "err-endt");?></div></div>
				</div>
<script>
$(document).ready(function() {
	$("#stdt").datepicker({
		dateFormat: "dd.mm.yy",
		dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
		dayNamesShort: [ "Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб" ],
		dayNames: [ "Воскрес.", "Понед.", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" ],
		monthNamesShort: [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
		monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
		minDate: new Date(<?=date("Y",time());?>, <?=date("m",time());?>-1, <?=date("d",time());?>)
	});

	$("#endt").datepicker({
		dateFormat: "dd.mm.yy",
		dayNamesMin: [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
		dayNamesShort: [ "Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб" ],
		dayNames: [ "Воскрес.", "Понед.", "Вторник", "Среда", "Четверг", "Пятница", "Суббота" ],
		monthNamesShort: [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
		monthNames: [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
		minDate: new Date(<?=date("Y",time()+3*3600);?>, <?=date("m",time()+3*3600);?>-1, <?=date("d",time()+3*3600);?>)
	});	
});
</script>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="form-group<?=( !$this->reqdata->ok('rcountry') ? ' has-error' : '' );?>">
					<label for="rcountry" class="col-sm-3 control-label"><?=$this->localize->get("addbonus", "lbl-country");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="rcountry" name="rcountry" onchange="loc_LoadRegion(this, 'robl', 'rcity', ['<?=$this->localize->get("addbonus", "load-region")?>', '<?=$this->localize->get("addbonus", "load-city")?>'])">
		<?php
			for( $i=0; $i<count($countries); $i++ )
			{
				echo '<option value="'.$countries[$i]['id'].'"'.($this->reqdata->rcountry == $countries[$i]['id'] ? ' selected' : '').'>'.$countries[$i]['name'].'</option>';
			} 
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("addbonus", "err-country");?></div></div>
				</div>
				<div class="form-group<?=( !$this->reqdata->ok('robl') ? ' has-error' : '' );?>">
					<label for="robl" class="col-sm-3 control-label"><?=$this->localize->get("addbonus", "lbl-obl");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="robl" name="robl" onchange="loc_LoadCity(this, 'rcity')">
						<option value="0">----- <?=$this->localize->get("addbonus", "lbl-obl-0");?> -----</option>
		<?php
			for( $i=0; $i<count($obls); $i++ )
			{
				echo '<option value="'.$obls[$i]['id'].'"'.($this->reqdata->robl == $obls[$i]['id'] ? ' selected' : '').'>'.$obls[$i]['name'].'</option>';
			} 
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("addbonus", "err-obl");?></div></div>
				</div>					
				<div class="form-group<?=( !$this->reqdata->ok('rcity') ? ' has-error' : '' );?>">
					<label for="rcity" class="col-sm-3 control-label"><?=$this->localize->get("addbonus", "lbl-city");?>:</label>
					<div class="col-sm-9"><select class="form-control" id="rcity" name="rcity">
						<option value="0">----- <?=$this->localize->get("addbonus", "lbl-city-0");?> -----</option>
		<?php			
			$cities = $this->catmodel->Loc_CityList(UKRAINE, $this->reqdata->robl);
			for( $i=0; $i<count($cities); $i++ )
			{
				echo '<option value="'.$cities[$i]['id'].'"'.($this->reqdata->rcity == $cities[$i]['id'] ? ' selected' : '').'>'.$cities[$i]['name'].'</option>';
			} 
		?>
					</select><div class="alert alert-danger" role="alert"><?=$this->localize->get("addbonus", "err-city");?></div></div>
				</div>
			</div>				
		</div>				
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="form-group<?=( !$this->reqdata->ok('descr') ? ' has-error' : '' );?>">
					<label for="rdescr" class="control-label"><?=$this->localize->get("addbonus", "lbl-descr");?></label>
					<textarea class="form-control" id="descr" name="descr" rows="4" onkeyup="updtTxtCount(this,'ltcount1',200)"><?=$this->reqdata->descr;?></textarea>

					<div class="alert alert-danger" role="alert"><?=$this->localize->get("addbonus", "err-descr");?></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="form-group">
					<!--<label for="rdescr" class="control-label">=$this->localize->get("addbonus", "lbl-descrfull");?></label>-->
					<!--<textarea class="form-control" id="descrfull" name="descrfull" rows="8">=$this->reqdata->descrfull;?></textarea> -->
				</div>
			</div>
		</div>
<?php
		if( $this->viewMode == "edit" )
		{
?>		
		<div class="row">
	<?php
			$pics = $this->catmodel->Item_Pics($this->reqdata->id);
			for($i=0; $i<count($pics); $i++)
			{
				echo '
				<div class="col-xs-3 col-md-2">
					<img src="'.WWWHOST.$pics[$i]['filename_ico'].'" alt="" class="img-thumbnail center-block">
					<div class="proj-pic-del"><a href="'.$this->Page_BuildUrl("proj", "picdel", "projid=".$this->reqdata->id."&picid=".$pics[$i]['id']."").'">'.$this->localize->get("addbonus", "photo-del").'</a></div>
				</div>
				';
			}
	?>
			<div class="col-xs-4 col-md-3">
				<div class="form-group">
					<label for="rfile" class="control-label"><?=$this->localize->get("addbonus", "lbl-photo");?></label>
					<input type="file" id="file1" name="file1">
					<div class="btn-form-pad-sm"></div><input type="submit" name="btnaddphoto" class="btn btn-primary" value="<?=$this->localize->get("addbonus", "btn-addphoto");?>"></div>
				</div>
			</div>
		</div>
<?php
		}
?>		
	</div>	
</div>
<div class="container">
	<?php	
	$btn_text = $this->localize->get("addbonus", "btn-addbonus");
	if( $this->viewMode == "edit" )
		$btn_text = $this->localize->get("addbonus", "btn-save");	
	?>
	<div class="text-center btn-form-pad"><input type="submit" class="btn btn-primary" value="<?=$btn_text;?>" /></div>
</div>
</form>
<?php		
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	else
	{
		//		
	}
?>