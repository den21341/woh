<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;
	
	$PAGE_H1 = $this->localize->get("login", "site-login");

	if( $this->viewMode == "login" )
	{
	}
	else if( $this->viewMode == "restorepass" )
	{
		$PAGE_H1 = $this->localize->get("login", "pass-reset");
	}
	else if( $this->viewMode == "logout" )
	{
		$PAGE_H1 = $this->localize->get("login", "session-lost");
	}
	
	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);
?>
	<div class="row-head breacrumb-bgi">
		<div class="container">
			<ol class="breadcrumb">
				<?=$BCHTML;?>
			</ol>
			<h1><?=$PAGE_H1;?></h1>
		</div>
	</div>
<?php
	if( $this->viewMode == "restoreok" )
	{
?>
<div class="container">
	<div class="info">
	<div class="h2">Спасибо</div>
	<?=( $PM->get_txtres()->logoutdone['text'] );?>
	</div>
</div>		
<?php
	}	
	else if( $this->viewMode == "restorepassnotify" )
	{
?>
<div class="container">
	<div class="info">
	<div class="h2"><?=$this->localize->get("login", "mail-msg")?></div>
	<div style="text-align: center; padding: 30px 0px 30px 0px;"><?=$this->localize->get("login", "toyour-mail")?></div>
	</div>
</div>		
<?php
	}
	else if( $this->viewMode == "passchanged" )
	{
?>
<div class="container">
	<div class="info">
	<div class="h2"><?=$this->localize->get("login", "pass-changed")?></div>
	<div style="text-align: center; padding: 30px 0px 30px 0px;"><?=$this->localize->get("login", "youare-changepass")?></div>
	</div>
</div>		
<?php
	}
	else if( $this->viewMode == "restorepass" )	
	{
?>
<form id="savepassfrm" action="<?=$this->Page_BuildUrl("login", "savepass");?>" method="POST">
<input type="hidden" name="action" value="restorepass">
<input type="hidden" name="guid" value="<?=$this->guid;?>">
<div class="row-gray">
	<div class="container">
		<a id="formstart" name="formstart"></a>
		<div class="h2"><?=$this->localize->get("login", "complete-data")?></div>
<?php
		if( isset($this->msg) )
		{
			echo '<div class="frm-error-msg">'.$this->msg.'</div>';
		}
?>		
		<div class="row form-horizontal">			
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<label for="rpass1" class="col-sm-3 control-label"><?=$this->localize->get("login", "new-pass")?></label>
					<div class="col-sm-9"><input type="password" class="form-control" id="rpass1" name="rpass1"></div>
				</div>					
			</div>				
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<label for="rpass2" class="col-sm-3 control-label"><?=$this->localize->get("login", "repeat-pass")?></label>
					<div class="col-sm-9"><input type="password" class="form-control" id="rpass2" name="rpass2"></div>
				</div>					
			</div>
		</div>		
	</div>	
</div>
<div class="container">
	<div class="text-center btn-form-pad"><input type="submit" class="btn btn-primary" value="<?=$this->localize->get("login", "restore-pass")?>" /></div>
</div>
</form>
<?php
	}
	else
	{
		$passrest_nobacklink = true;
		
		echo '<div class="container">';
		include $this->cfg['MVC_PATH']."templates/wnd_log_dialog-inc.php";
		echo '</div>';
	}
?>