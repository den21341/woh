<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$page = $PM->get_page();	
	
	$PAGE_H1 = $page->title;	
	
	// Build bread crumbs
	$BCHTML = $this->renderBreadcrumbs($PAGE_H1);

	$need_tab_sel = ($this->viewMode == "needlist");
	$do_tab_sel = ($this->viewMode == "dolist");
	$give_tab_sel = ($this->viewMode == "givelist");
	$my_tab_sel = ($this->viewMode == "sendlist");
	$myget_tab_sel = ($this->viewMode == "getlist");
	
	//$num_new_msg = $PM->msg_NumNew($this->UserId);
?>
<div class="row-head breacrumb-bgi cab-bgi">
	<div class="container">
		<ol class="breadcrumb">
			<?=$BCHTML?>
		</ol>
		<h1><?=$PAGE_H1?></h1>

		<div class="cab-nav bar-fix">
			<ul class="nav nav-tabs">
				<li role="presentation" class="cab-tabs-fix"><a href="<?=$this->Page_BuildUrl("cabinet");?>"><?=$this->localize->get("cab", "tab-profile");?></a></li>
				<li role="presentation"<?=($myget_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelpget");?>"><?=$this->localize->get("cabinet", "needhelp");?> <span class="<?=($this->need_services['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->need_services['tab_new'] > 0 ? '+'.$this->need_services['tab_new'] : $this->need_services['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($my_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","myhelp");?>"><?=$this->localize->get("cabinet", "givehelp");?> <span class="<?=($this->my_services['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->my_services['tab_new'] > 0 ? '+'.$this->my_services['tab_new'] : $this->my_services['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($need_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","needhelp");?>"><?=$this->localize->get("cab", "tab-needhelp");?> <span class="<?=($this->needhelp['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->needhelp['tab_new'] > 0 ? '+'.$this->needhelp['tab_new'] : $this->needhelp['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($do_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","dohelp");?>"><?=$this->localize->get("cab", "tab-dohelp");?> <span class="<?=($this->myhelp['tab_new'] > 0 ? 'badge-red' : 'badge')?>"><?=($this->myhelp['tab_new'] > 0 ? '+'.$this->myhelp['tab_new'] : $this->myhelp['tab_tot'])?></span></a></li>
				<li role="presentation"<?=($give_tab_sel ? ' class="active"' : '');?>><a href="<?=$this->Page_BuildUrl("cabinet","givehelp");?>"><?=$this->localize->get("cab", "tab-givehelp");?> <span class="badge"><?=$this->thinghelp['tab_tot']?></span></a></li>
				<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet", "msgview");?>"><?=$this->localize->get("cab", "tab-msg");?><span class="<?=($this->mymsg['tab_new'] > 0 ? 'badge-red' : 'badge' )?>"><?=($this->mymsg['tab_new'] > 0 ? '+'.$this->mymsg['tab_new'] : 0 )?></span></a></li>
				<li role="presentation"><a href="<?=$this->Page_BuildUrl("cabinet","mybonus");?>"><?=$this->localize->get("cab", "tab-bonus");?> <span class="badge"><?=$this->mybonus['tab_tot']?></span></a></li>
				<li role="presentation" class="needhelp-size fix-myface"><a href="<?=$this->Page_BuildUrl("users","viewrev").$this->UserId.'/'?>"><?=$this->localize->get("cab", "tab-onlooking");?> <span class="badge"><?=""?></span></a></li>
			</ul>
		</div>
	</div>
</div>

<div class="container">
<?php
	$its = $this->its;
	
	/*
?>
	<div class="proj-flt">
		<div class="row">
			<div class="col-xs-12 col-md-6 proj-flt-col">
				<div class="btn-group" role="group" aria-label="<?=$this->localize->get("cabdohelp", "flt-hdr");?>">
					<a href="<?=$this->Page_BuildUrl("cabinet", "dohelp");?>" class="btn btn-default<?=($this->filtby == "" ? ' active' : '');?>"><?=$this->localize->get("cabdohelp", "flt-all");?></a>
					<a href="<?=$this->Page_BuildUrl("cabinet", "dohelp");?>filt_running/" class="btn btn-default<?=($this->filtby == "running" ? ' active' : '');?>"><?=$this->localize->get("cabdohelp", "flt-cur");?></a>
					<a href="<?=$this->Page_BuildUrl("cabinet", "dohelp");?>filt_finished/" class="btn btn-default<?=($this->filtby == "finished" ? ' active' : '');?>"><?=$this->localize->get("cabdohelp", "flt-ended");?></a>
				</div>
			</div>						
		</div>
	</div>
	*/
?>
	
	<div class="proj-tbl">
		<table>
		<tr>
			<th colspan="2" class="name"><?=$this->localize->get("cabbonus", "tbl-bonus");?></th>
			<th><?=$this->localize->get("cabbonus", "tbl-luckind");?></th>
			<th><?=$this->localize->get("cabbonus", "tbl-get");?></th>
			<th><?=$this->localize->get("cabbonus", "tbl-func");?></th>
		</tr>
	<?php
		for( $i=0; $i<count($its); $i++ )
		{		
			$pic_src = WWWHOST.'img/no-pic.png';			
			if( count($its[$i]['pics'])>0 )
			{
				$pic_src = PICHOST.$its[$i]['pics'][0]['filename_ico'];
			}
			
			echo '<tr>
				<td><img src="'.$pic_src.'" width="80" height="80"></td>
				<td class="name"> 
					<div class="p-tit"><a href="'.$this->Page_BuildUrl("bonus", "view/".$its[$i]['id']).'">'.$its[$i]['title2'].'</a></div>
				</td>
				<td>'.$its[$i]['point_ind'].'</td>			
				<td>'.$its[$i]['windt'].'</td>
				<td>					
					'.( false ? '<a href="'.$this->Page_BuildUrl("cabinet", "reqcancel/".$its[$i]['id']).'" class="aplnk-del">'.$this->localize->get("cabdohelp", "func-cancel").'</a>' : '&nbsp;' ).'
				</td>
			</tr>';
		}
		
		if( count($its) == 0 )
		{
			$noitems_str = $this->localize->get("cabbonus", "tbl-noits");
			
			echo '<tr><td colspan="6" class="noitems">
				<div class="tbl-noitems">
					<div class="tbl-noitems-txt"><div><span>'.$noitems_str.'</span></div></div>
					<div class="tbl-noitems-face"></div>
				</div>
			</td></tr>';
		}		
	?>	
		</table>
	</div>
		
	<?php
		/*
		$PAGES_NUM = ceil($this->projlist_total / $this->pn);
		
		//echo $this->projlist_total.":".$this->pn.":".$PAGES_NUM."<br>";
		
		if( $PAGES_NUM > 1 )
		{		
	?>
	<div class="proj-pages">
		<nav class="navbar-right">
			<ul class="pagination">
	<?php
			for( $i=1; $i<=$PAGES_NUM; $i++)
			{
				echo '<li'.( $this->pi == ($i-1) ? ' class="active"' : '' ).'><a href="'.$this->Page_BuildUrl("cabinet", "dohelp").($this->filtby != "" ? 'filt_'.$this->filtby.'/' : '').($i>1 ? 'p_'.$i.'/' : '').'">'.$i.'</a></li>';
			}
			
	?>
			<li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
			<li class="active"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
			
	?>
			</ul>
		</nav>
		<div class="clearfix"></div>
	</div>
<?php
		}	
		*/
?>
	<br><br>
</div>