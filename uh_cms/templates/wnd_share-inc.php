<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$projid = ( isset($_GET['projid']) ? $_GET['projid'] : 0 );
	$mode = ( isset($_GET['mode']) ? $_GET['mode'] : 0 );

	$URLSHARE = ($mode == 'bonus' ? UhCmsUtils::Page_BuildUrl("bonus", "view/".$projid)
		: ($mode == 'proj' ? UhCmsUtils::Page_BuildUrl("proj", "view/".$projid) : ''));

		$proj_or_bon = ($mode == 'bonus' ? "popbon" : "popproj");
		
	//$URLSHARE = UhCmsUtils::Page_BuildUrl("proj", "view/".$projid);
?>
<style>
	.wnd-in{
		height: 260px;
	}
</style>
<div class="wnd-hdr"><?=$this->localize->get($proj_or_bon, "hdr");?></div>
<p></p>
<div>

<div class="pop-face">
	<div class="tbl-noitems">
		<div class="tbl-noitems-txt">
			<div><span class="share-txt">
				<?=$this->localize->get($proj_or_bon, "text");?><br>
			</span></div>
		</div>
			<div class="tbl-noitems-face <?=($proj_or_bon == "popproj" ? "tbl-fix-img" : "")?>"></div>
	</div>
</div>
<script>
	$(document).ready(function() {
		$("#fb-share").bind("click", function () {
			FB.ui({
				method: 'share',
				href: $(this).attr("data-share-url"),
			}, function (response) {
				if (response === null) {
					//console.log('was not shared');
				} else {
				}
			});
			return false
		});
	});
</script>
<?php if($proj_or_bon != "popproj") { ?>
	<div class="jh-sclike-row">
		<a href="#" id="fb-share" class="fb-share-button" data-layout="button_count" data-share-url="<?=$URLSHARE?>"></a>
			<?php
			/*
			<div class="fb-like" data-href="<?=$this->Page_BuildUrl("proj", "view/".$proj['id']."/");?>" data-layout="button" data-action="like" data-show-faces="true" data-share="false"></div>
			<div class="fb-like" data-href="http://wayofhelp.com/proj/view/1/" data-layout="button" data-action="recommend" data-show-faces="true" data-share="false"></div>
			*/
			?>
		<div class="jh-sclike jh-sclike-vk">
<?php
/*
			<!-- Put this script tag to the <head> of your page -->
			<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

			<script type="text/javascript">
			  VK.init({apiId: 4923828, onlyWidgets: true});
			</script>

			<script type="text/javascript" src="http://vk.com/js/api/share.js?90" charset="windows-1251"></script>
*/
?>
			<!-- Put this div tag to the place, where the Like block will be -->
			<div id="vk_poplike"></div>
			<script type="text/javascript">
			<?php
			/*
			?>
			VK.Share.button(false,{type: "round", text: "Поделиться"});
			VK.Widgets.Like("vk_like", {type: "button", verb: 1, height: 20});
			*/
			?>
			document.getElementById('vk_poplike').innerHTML = VK.Share.button('<?=$URLSHARE;?>', {type: 'button', text: 'Поделиться', height: '20px'});
			</script>
		</div>
		<div class="jh-sclike jh-sclike-od-share">
			<div id="ok_shareWidget"></div>
			<script>
			!function (d, id, did, st) {
			  var js = d.createElement("script");
			  js.src = "http://connect.ok.ru/connect.js";
			  js.onload = js.onreadystatechange = function () {
			  if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
				if (!this.executed) {
				  this.executed = true;
				  setTimeout(function () {
					OK.CONNECT.insertShareWidget(id,did,st);
				  }, 0);
				}
			  }};
			  d.documentElement.appendChild(js);
			}(document,"ok_shareWidget","<?=$URLSHARE;?>","{width:115,height:30,st:'straight',sz:20,ck:2,nc:1}");
			</script>
		</div>
	<div class="both"></div>
</div>
	<?php } ?>
</div>
<script>

</script>
