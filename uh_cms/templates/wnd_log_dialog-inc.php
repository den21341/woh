<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	// Facebook
	$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);
		
	$FB_REDIRECT_URL = WWWHOST.'login/redirfb';
	$FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);
	
	// Vkontakte
	$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);
	
	$VK_REDIRECT_URL = WWWHOST.'login/redirvk';
	$VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);
	
	// Odnoklassniki
	$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);
	
	$OK_REDIRECT_URL = WWWHOST.'login/redirok';
	$OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);
	
	//////////////////////////////////////////////////////
	
	$redirurl = ( isset($_GET['redirto']) ? $_GET['redirto'] : '' );
	
	
?>
<div class="wnd-hdr"><?=$this->localize->get("poplog", "hdr");?></div>
<p><?=( $PM->get_txtres()->poplog['text'] );?></p>
<div id="loginerror" class="wnd-error"></div>
<form action="<?=( $this->Page_BuildUrl("login", "makelogin") );?>" method="POST" onsubmit="return logFrmNone(this)">
<?php
if( $redirurl != '' )
{
	echo '<input type="hidden" name="redirto" value="'.$redirurl.'">';
} 
?>
	<div class="row form-horizontal">
		<div class="col-xs-12 col-md-12">
			<div class="form-group">
				<label for="ulogin" class="col-sm-3 control-label"><?=$this->localize->get("poplog", "lbl-login");?></label>
				<div class="col-sm-9"><input type="text" class="form-control" id="ulogin" name="ulogin" placeholder="Email" value=""></div>
			</div>
			<div class="form-group">
				<label for="upass" class="col-sm-3 control-label"><?=$this->localize->get("poplog", "lbl-pass");?></label>
				<div class="col-sm-9"><input type="password" class="form-control" id="upass" name="upass" placeholder="Password" value=""></div>
			</div>
		</div>
	</div>	
	<div class="row form-horizontal">
		<div class="col-xs-12 col-md-12 ">
			<div class="form-group">
				<div class="col-sm-3 col-sm-offset-3"><button id="loginbtn" class="btn btn-primary"><span class="glyphicon glyphicon-user"></span> <?=$this->localize->get("poplog", "btn-enter");?></button></div>
				<div class="col-sm-6 text-right"><a id="goto-restorepass<?=(isset($passrest_nobacklink) && $passrest_nobacklink ? '2' : '');?>" href="#"><?=$this->localize->get("poplog", "restpass");?></a></div>
			</div>
		</div>
	</div>		
</form>
<div class="row reg-sc-login">
	<a target="_blank" href="<?=$FB_LINK;?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"><img src="<?=WWWHOST?>img/btn-log-fb.png" width="170" height="34" alt="<?=$this->localize->get("poplog", "btn-fb");?>" title="<?=$this->localize->get("poplog", "btn-fb");?>"></a>
	<a target="_blank" href="<?=$VK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"><img src="<?=WWWHOST?>img/btn-log-vk.png" width="180" height="34" alt="<?=$this->localize->get("poplog", "btn-vk");?>" title="<?=$this->localize->get("poplog", "btn-vk");?>"></a>
	<a target="_blank" href="<?=$OK_LINK;?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"><img src="<?=WWWHOST?>img/btn-log-od.png" width="212" height="34" alt="<?=$this->localize->get("poplog", "btn-ok");?>" title="<?=$this->localize->get("poplog", "btn-ok");?>"></a>
</div>
<div class="row reg-login">
	<?=$this->localize->get("poplog", "gotoreg");?>
	<div><a href="<?=$this->Page_BuildUrl("registration", "person");?>" id="goto-regdlg" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> <?=$this->localize->get("poplog", "btn-reg");?></a></div>
</div>
<script type="text/javascript">
function logFrmNone(frm)
{
	console.log("form submitted");
	
	return false
}

<?php
/*
$("#goto-regdlg").on("click",function(){
	popWnd('regdlg', '');
	return false
});
*/
?>

$("#loginbtn").on("click", function(){
	checkLoginForm();
	return false
});
<?php
	if( isset($passrest_nobacklink) && $passrest_nobacklink )
	{
?>
$("#goto-restorepass2").on("click", function(){
	popWnd('restorepdlg','nologlnk=1');
	return false;
});
<?php
	}
	else
	{
?>
$("#goto-restorepass").on("click", function(){
	popWnd('restorepdlg','');
	return false;
});
<?php
	} 
?>
</script>