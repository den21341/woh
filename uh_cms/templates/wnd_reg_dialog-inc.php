<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	// Facebook
	$FbApi = new UhCmsFacebook(FB_APP_ID, FB_APP_SECRET);
		
	$FB_REDIRECT_URL = WWWHOST.'login/redirfb';
	$FB_LINK = $FbApi->makeLoginLink($FB_REDIRECT_URL);
	
	// Vkontakte
	$VkApi = new UhCmsVk(VK_APP_ID, VK_APP_SECRET);
	
	$VK_REDIRECT_URL = WWWHOST.'login/redirvk';
	$VK_LINK = $VkApi->makeLoginLink($VK_REDIRECT_URL);
		
	// Odnoklassniki
	$OkApi = new UhCmsOdnoklassniki(OK_APP_ID, OK_APP_SECRET, OK_APP_KEY);
	
	$OK_REDIRECT_URL = WWWHOST.'login/redirok';
	$OK_LINK = $OkApi->makeLoginLink($OK_REDIRECT_URL);
?>
<div class="wnd-hdr"><?=$this->localize->get("popreg", "hdr");?></div>
<p><?=(
 $PM->get_txtres()->popreg['text'] );?></p>
<div class="row">
	<div class="col-xs-12 col-md-4">
		<div class="reg-col">
			<div class="reg-col-who">
				<div class="reg-col-pers hidden-xs"></div>
				<div class="reg-col-tit"><?=$this->localize->get("popreg", "h-pers");?></div>
			</div>
			<a href="<?=$this->Page_BuildUrl("registration", "person");?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> <?=$this->localize->get("popreg", "btn-reg");?></a>			
			<span class="hidden-xs hidden-sm"><?=( $PM->get_txtres()->popregpers['text'] );?></span>			
		</div>
	</div>
	<div class="col-xs-12 col-md-4">
		<div class="reg-col">
			<div class="reg-col-who">
				<div class="reg-col-firm hidden-xs"></div>
				<div class="reg-col-tit"><?=$this->localize->get("popreg", "h-comp");?></div>
			</div>
			<a href="<?=$this->Page_BuildUrl("registration", "company");?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> <?=$this->localize->get("popreg", "btn-reg");?></a>
			<span class="hidden-xs hidden-sm"><?=( $PM->get_txtres()->popregcomp['text'] );?></span>
		</div>
	</div>
	<div class="col-xs-12 col-md-4">
		<div class="reg-col">
			<div class="reg-col-who">			
				<div class="reg-col-org hidden-xs"></div>
				<div class="reg-col-tit"><?=$this->localize->get("popreg", "h-org");?></div>
			</div>
			<a href="<?=$this->Page_BuildUrl("registration", "organization");?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> <?=$this->localize->get("popreg", "btn-reg");?></a>
			<span class="hidden-xs hidden-sm"><?=( $PM->get_txtres()->popregorg['text'] );?></span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="row reg-sc-login">
	<a target="_blank" href="<?=$FB_LINK;?>" title="<?=$this->localize->get("popreg", "btn-fb");?>"><img src="<?=WWWHOST?>img/btn-log-fb.png" width="170" height="34" alt="<?=$this->localize->get("popreg", "btn-fb");?>"></a>
	<a target="_blank" href="<?=$FB_LINK;?>" title="<?=$this->localize->get("popreg", "btn-vk");?>"><img src="<?=WWWHOST?>img/btn-log-vk.png" width="180" height="34" alt="<?=$this->localize->get("popreg", "btn-vk");?>"></a>
	<a target="_blank" href="<?=$OK_LINK;?>" title="<?=$this->localize->get("popreg", "btn-ok");?>"><img src="<?=WWWHOST?>img/btn-log-od.png" width="212" height="34" alt="<?=$this->localize->get("popreg", "btn-ok");?>"></a>
</div>
<?php
/*
<div class="row reg-login">
	Если вы уже являетесь зарегистрированным пользователем то нажмите кнопку войти, чтобы выполнить авторизацию				
	<div><button id="goto-logdlg" class="btn btn-primary"><span class="glyphicon glyphicon-user"></span> Войти</button></div>
</div>
<script>
//$(document).ready(function(){
	$("#goto-logdlg").bind("click",function(){
		popWnd('logdlg');
		return false
	});
//});
</script>
*/
?>