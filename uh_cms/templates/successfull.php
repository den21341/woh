<?php

$PM = $this->pageModel;

$page = $PM->get_page();

$PAGE_H1 = $page->title;

$BCHTML = $this->renderBreadcrumbs($PAGE_H1);

?>

<div class="row-head breacrumb-bgi">
    <div class="container">
        <h2 class="suc-color-head suc-pos-head"><?=$this->localize->get("footer", "good-proj")?></h2>
    </div>
</div>

<div class="row">
    <div class="container">

        <div class="col-xs-12 col-md-12 col-sm-12">
            <div class="col-xs-2 col-md-2 col-sm-2"><img src="<?=WWWHOST?>img/cat-head.png" title="" alt="" class="suc-img-head"></div>
            <div class="col-xs-10 col-md-10 col-sm-10"><span><p class="suc-text-head"><?=$this->localize->get("successfull", "success-project")?></p></span></div>

            <style type="text/css">
                @import url("<?=WWWHOST?>css/graph.css");
                @import url("<?=WWWHOST?>css/vis.css");
            </style>
            <script type="text/javascript" src="<?=WWWHOST?>js_vsgraph/my_vis.js"></script>
            <script type="text/javascript">
            </script>
            <script type="text/javascript" src="http://numberimage.ru/graph/js/visgraph/obj_graph_baloon.js"></script>

            <?php

            $projitem = $this->its_new;

            //echo '<pre>';
            //print_r($projitem); die();
            
            $i = 0;
            //$arr =array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
            //echo '<pre>';
            //print_r($arr);
            //echo '</pre>';

            $checkdata = $projitem[0]['stdat'];

            foreach ($projitem as $allinfo){

            $confirmed = $this->catmodel->Item_ReqCollected($allinfo['id'], REQ_STATUS_CONFIRM);

            $countS = $allinfo['stdat'];

            switch ($countS){
                case '01': $countS = 'Январь'; break;
                case '02': $countS = 'Февраль'; break;
                case '03': $countS = 'Март'; break;
                case '04': $countS = 'Апрель'; break;
                case '05': $countS = 'Май'; break;
                case '06': $countS = 'Июнь'; break;
                case '07': $countS = 'Июль'; break;
                case '08': $countS = 'Август'; break;
                case '09': $countS = 'Сентябрь'; break;
                case '10': $countS = 'Октябрь'; break;
                case '11': $countS = 'Ноябрь'; break;
                case '12': $countS = 'Декабрь'; break;}

            $AUTHOR_LNK = $this->Page_BuildUrl('users', 'viewrev/' . $allinfo['author_id']);
            $PROJ_LNK = $this->Page_BuildUrl('proj', "view/" . $allinfo['id']);
            $proj_avatar = WWWHOST . ($allinfo['pic_sm'] != "" ? $allinfo['filename_thumb'] : 'img/no-pic.png');
            $person_avatar = WWWHOST . ($allinfo['pic'] != "" ? $allinfo['pic_sm'] : 'img/no-pic.png');

            $title = strip_tags($allinfo['title2'], '');

            if (mb_strlen($title) > 32) {
                $title = mb_substr($title, 0, 32) . "...";
            }

            $short = strip_tags($allinfo['descr'], '');


            if (mb_strlen($short) > 60) {
                $short = mb_substr($short, 0, 60) . "...";
            }

            $start = $allinfo['stdt'];
            $name = $allinfo['name'];

            if($checkdata != $allinfo['stdat'] || $i == 0){

            ?>

            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 suc-img-catalog soc-div-posrel soc-global-s">
                <div class="suc-green-1 soc-div-zindex" ><?=$countS?></div>
            </div>
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 suc-img-catalog soc-div-posrel">
                <img src="<?=WWWHOST?>img/cat-green-2.png" class="suc-green-2" >

                <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 suc-img-catalog soc-global-hr">

                    <?php
                    }

                    ?>

                    <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4 suc-position-block soc-height-all">
                        <div class="suc-div-img">
                            <a href="<?=$PROJ_LNK?>" target="_blank"><img src="<?=$proj_avatar?>" class="suc-img-table"></a>
                        <?php if($confirmed['avg']!=1){?>
                            <div class="suc-money-num"><div class="col-md-12">
                                    <p class="suc-money-text">Собрано: <?=($confirmed['avg']!=1? $confirmed['sum']: "").($allinfo['currency_id']==1? $this->localize->get("successfull", "uah") : $this->localize->get("successfull", "rub") )?></p>
                                </div>
                                </div>
                            <?php }?>

                            </a>

                            <span class="soc-folder"><div><a class="soc-con-tree soc-con-basic" href="<?=$AUTHOR_LNK?>"><img class="soc-con-basics <?=($confirmed['avg']==1 ? "soc-smallfix" : "")?>" src="<?=$person_avatar?>"></a></div></span>

                        </div>
            <div class="soc-gedfer">
                        <div class="suc-p soc-height-text">
                            <p class="suc-text-block">
                                <a href="<?=$PROJ_LNK?>"<span class="suc-text-block"><?=$title?></span></a>

                            </p>
                        </div>

                        <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6 suc-img-catalog">
                            <div class="suc-left-rating suc-left-right-size">
                                <p class="soc-text-randh soc-color-ratg"><b><?=$this->localize->get("projview", "usr-rating")?></b></p>
                                <p class="soc-size-rating soc-color-ratg"><b><?=$allinfo['user_rate']?></b></p>
                            </div>
                        </div>

                        <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6 suc-img-catalog">
                            <div class="suc-right-help suc-left-right-size">
                                <p class="soc-text-randh soc-color-help"><?=($allinfo['amount_type'] == PROJ_THINGS) ? $this->localize->get("successfull", "take-think") : $this->localize->get("successfull", "people-help")?></p>
                                <p class="soc-size-help soc-color-help"><b><?=$allinfo['helpcount']?></b><span><?=($allinfo['parent_id'] == 77) ? $this->localize->get("successfull", "people") : ''?></span></p>
                            </div>
                        </div>

                        <div>
                            <p class="suc-text-underblock soc-height-underblock-text"><?=$short?></p>
                        </div>

                        <hr class="suc-hr">

                        <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
                            <div class="soc-downt-text-left">
                                <p class="">Запущен:<?=$start?></p>
                            </div>
                        </div>

                        <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
                            <div class="soc-downt-text-right">
                                <a href="<?=$AUTHOR_LNK?>" class="soc-usr-a"><img src="<?=WWWHOST?>img/cat-blackman.png" alt="<?=$this->localize->get("successfull", "user")?>" title="<?=$this->localize->get("successfull", "user")?>"><span><?=$name?></span></a>

                            </div>
                        </div>
                    </div>
                    </div>

                    <?php



                    $checkdata = $allinfo['stdat'];

                    if(isset($projitem[$i+1]['stdat']) && $projitem[$i+1]['stdat'] != $allinfo['stdat']) {
                        echo '</div>';
                        echo '</div>';

                    } else if(!isset($projitem[$i+1]['stdat'])) {
                        echo '</div>';
                    }



                    $i++;}?><div></div></div></div></div></div>
