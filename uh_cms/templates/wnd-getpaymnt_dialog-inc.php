<?php

$projid = ( isset($_GET['projid']) ? $_GET['projid'] : 0 );
$sum = ( isset($_GET['sum']) ? $_GET['sum'] : 0 );
$type = ( isset($_GET['type']) ? $_GET['type'] : '' );
$UserId = UhCmsApp::getSesInstance()->UserId;
$promise = ( isset($_GET['prom']) ? $_GET['prom'] : 0 );

$projinfo = $this->catLib->Item_Info(addslashes($projid));
$pymntList = $this->catLib->getPaymentProjList($projid);

$currency_data = Array(
    0 => Array("name" => "UAH", "defval" => "100.0"),
    1 => Array("name" => "UAH", "defval" => "100.0"),
    2 => Array("name" => "RUB", "defval" => "200.0"),
    3 => Array("name" => "UAH", "defval" => "100.0"),
    //4 => Array("name" => "EUR", "defval" => "20.0")
    4 => Array("name" => "UAH", "defval" => "100.0")
);

?>

<div class="row form-horizontal getpymnt-cont">
    <div class="col-xs-12 col-md-12">
        <?php if(count($pymntList) != 0) { ?>
            <div class="getpymnt-txt">
                <b class="woh-blue"><?=$this->localize->get("getpaymnt", "payed")?></b>. <br>
            <span><?=$this->localize->get("getpaymnt", "steps")?></span>
            </div>
            <table>
                <tr>
                    <th><?=$this->localize->get("getpaymnt", "payment")?></th>
                    <th class="tbl-leftal"><?=$this->localize->get("getpaymnt", "req")?></th>
                </tr>
                <tr>
                    <?php foreach ($pymntList as $payment) { ?>
                    <td class=""><label for="p_<?= $payment['payment_id'] ?>"><?= $payment['name'] ?></label></td>
                    <td class="tbl-leftal"><input type="text" value="<?= $payment['account'] ?>" readonly="readonly">
                    </td>
                </tr>
                <?php } ?>
            </table>
			<p class="wnd-gmnt-txt woh-orange"><?=$this->localize->get("getpaymnt", "pay-sum")?></p>
            <?php if ($type != 'cab') { ?>
                <form action="<?= $this->page_BuildUrl('proj', 'addhelp') ?>" method="post">
                    <input type="hidden" name="sum" value="<?= $sum ?>">
                    <input type="hidden" name="pid" value="<?= $projid ?>">
                    <button type="submit" class="btn btn-success"><?=$this->localize->get("getpaymnt", "agree")?></button>
                </form>
            <?php } else if ($type == 'cab') { ?>
                 <p><?=$this->localize->get("getpaymnt", "push-btn")?></p>
                <form action="<?= $this->page_BuildUrl('cabinet', 'sendhlp') ?>" method="post">
                    <input type="hidden" name="sum" value="<?= $sum ?>">
                    <input type="hidden" name="pid" value="<?= $projid ?>">
                    <button type="submit" class="btn btn-success"><?=$this->localize->get("getpaymnt", "agree")?></button>
                </form>
            <?php }
        } else { ?>
            <div class="getpymnt-txt">
                <br>
                <b class="woh-blue"><?=$this->localize->get("getpaymnt", "help-user")?></b>.<br>
                <span><?=$this->localize->get("getpaymnt", "help-tree")?></span>
                <br>
                <table>
                    <tr>
                        <th><?=$this->localize->get("getpaymnt", "payment")?></th>
                        <th class="tbl-leftal"><?=$this->localize->get("getpaymnt", "req")?></th>
                    </tr>
                    <tr>
                        <td class="tbl-pymnt"><label for="idwoh">WayOfHelp</label></td>
                        <td>
                            <form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
                                <input type="hidden" name="ik_co_id" value="561555503d1eafe0518b4567"/>
                                <?php if($promise == 1) { ?>
                                    <input type="hidden" name="ik_pm_no" value="<?=$type == 'cab' ? 'ID_1_'.$projid.'_'. $UserId : 'ID_'.$projid.'_'.$UserId ?>"/>
                                <?php } else {  ?>
                                    <input type="hidden" name="ik_pm_no" value="<?=$type == 'cab' ? 'ID_2_'.$projid.'_'. $UserId : 'ID_'.$projid.'_'.$UserId ?>"/>
                                <?php } ?>
                                <input type="hidden" name="ik_am" id="ik_am_inp"
                                       value="<?= $sum ?>"/><!--  100.0  -->
                                <input type="hidden" name="ik_cur" id="ik_cur"
                                       value="<?= ($currency_data[$projinfo['currency_id']]['name']); ?>"/><!-- UAH -->
                                <input type="hidden" name="ik_desc" value="Благотворительный взнос"/>
                                <input type="hidden" name="ik_exp" value="<?= date("Y-m-d", time() + 24 * 3600); ?>"/>
                                <input type="hidden" name="ik_ia_u" value="http://wayofhelp.com/payscallback/ikinteract/"/>
                                <input type="hidden" name="ik_ia_m" value="post"/>
                                <input type="hidden" name="ik_suc_u" value="<?= $this->Page_BuildUrl("cabinet", "")?>"/>
                                <input type="hidden" name="ik_suc_m" value="get"/>
                                <input type="hidden" name="ik_fal_u" value="<?= $this->Page_BuildUrl("cabinet", "")?>"/>
                                <input type="hidden" name="ik_fal_m" value="get"/>
                                <!--<input type="hidden" name="ik_int" value="web" />
                                <input type="hidden" name="ik_am_t" value="payway" />-->
                                <a href="#" class="tbl-leftal"><input type="submit" class="btn btn-success" value=" <?=$this->localize->get("myprojects", "payed")?> "></a>
                            </form>
                    </tr>
                </table>
            </div>
        <?php } ?>
    </div>
</div>
