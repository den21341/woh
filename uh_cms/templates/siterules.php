<?php $BCHTML = $this->renderBreadcrumbs();	 ?>
<div class="top-block col-md-12 col-xs-12 col-lg-12">
    <div class="top-block-txt">
        <div class="container">
            <ol class="breadcrumb">
                <?=$BCHTML;?>
            </ol>
        </div>
        <h1><p><?=$this->localize->get("siterules", "rule-concept")?></p>
            <p><?=$this->localize->get("siterules", "independent")?></p></h1>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="rule-block">
                <div class="rule-block-img"></div>
                <div class="rule-block-txt">
                    <p><?=$this->localize->get("siterules", "rules-govern")?></p><p><?=$this->localize->get("siterules", "platform")?></p>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="info-rule-block container-fluid">
                <div class="info-rule-img"></div>
                <div class="info-rule-txt"><p><?=$this->localize->get("siterules", "in-this-page")?></p>
                    <p><?=$this->localize->get("siterules", "learn-the-rules")?></p>
                    <p><?=$this->localize->get("siterules", "between-platform")?></p></div>
            </div>
        </div>
    </div>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="rule-cat-block">
                <div class="rule-row">
                    <a href="<?=$this->Page_BuildUrl('info','project')?>"><div class="cat-block ">
                        <div class="cat-blick-soimg cat-block-ab"></div>
                        <div class="cat-block-txt bl-tx-2"><p><?=$this->localize->get("siterules", "about-project")?></p>
                            <p><?=$this->localize->get("siterules", "in-outline")?></p><p style="visibility: collapse">.</p></div>
                    </div></a>
                    <a href="<?=$this->Page_BuildUrl('info','concepts')?>"><div class="cat-block">
                        <div class="cat-blick-soimg cat-block-mi"></div>
                        <div class="cat-block-txt bl-tx-1"><p><?=$this->localize->get("siterules", "concept")?></p><p style="visibility: collapse">.</p><p style="visibility: collapse">.</p>
                        </div>
                    </div></a>
                    <a href="<?=$this->Page_BuildUrl('info','users')?>"><div class="cat-block">
                        <div class="cat-blick-soimg cat-block-usr"></div>
                        <div class="cat-block-txt bl-tx-3"><p><?=$this->localize->get("siterules", "category")?></p>
                            <p><?=$this->localize->get("siterules", "users")?></p>
                            <p><?=$this->localize->get("siterules", "less")?></p></div>
                    </div></a>
                    <a href="<?=$this->Page_BuildUrl('info','tree')?>"><div class="cat-block">
                        <div class="cat-blick-soimg cat-block-tree"></div>
                        <div class="cat-block-txt bl-tx-3"><p><?=$this->localize->get("siterules", "principles")?></p>
                            <p><?=$this->localize->get("siterules", "visual")?></p>
                            <p><?=$this->localize->get("siterules", "good-job")?></p></div>
                    </div></a>
                    <a href="<?=$this->Page_BuildUrl('info','rules')?>"><div class="cat-block">
                        <div class="cat-blick-soimg cat-block-rul"></div>
                        <div class="cat-block-txt bl-tx-2"><p><?=$this->localize->get("siterules", "work-rule")?></p>
                            <p><?=$this->localize->get("siterules", "platforms")?></p><p style="visibility: collapse">.</p></div>
                    </div></a>
                    <a href="<?=$this->Page_BuildUrl('info','donors')?>"><div class="cat-block">
                        <div class="cat-blick-soimg cat-block-don"></div>
                        <div class="cat-block-txt bl-tx-1"><p><?=$this->localize->get("siterules", "donor")?></p><p style="visibility: collapse">.</p><p style="visibility: collapse">.</p></div>
                    </div></a>
                    <a href="<?=$this->Page_BuildUrl('info','report')?>"><div class="cat-block">
                        <div class="cat-blick-soimg cat-block-req"></div>
                        <div class="cat-block-txt bl-tx-3"><p><?=$this->localize->get("siterules", "stat")?></p>
                                <p><?=$this->localize->get("siterules", "charity")?></p>
                                <p><?=$this->localize->get("siterules", "proj")?></p></div>
                    </div></a>
                    <a href="<?=$this->Page_BuildUrl('info','conf')?>"><div class="cat-block">
                        <div class="cat-blick-soimg cat-block-pol"></div>
                        <div class="cat-block-txt bl-tx-2"><p><?=$this->localize->get("siterules", "pol")?></p>
                            <p><?=$this->localize->get("siterules", "confident")?></p><p style="visibility: collapse">.</p></div>
                    </div></a>
                    <a href="<?=$this->Page_BuildUrl('info','fees')?>"><div class="cat-block">
                        <div class="cat-blick-soimg cat-block-mon"></div>
                        <div class="cat-block-txt bl-tx-1"><p><?=$this->localize->get("siterules", "fees-pay")?></p><p style="visibility: collapse">.</p><p style="visibility: collapse">.</p></div>
                    </div></a>
                </div>
            </div>
        </div>
    </div>
</div>