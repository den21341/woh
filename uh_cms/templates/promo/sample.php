<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////
?>
<link media="all" rel="stylesheet" type="text/css" href="/promo/sample/css/fonts.css" />
<link media="all" rel="stylesheet" type="text/css" href="/promo/sample/css/style.css" />
<script type="text/javascript" src="/promo/sample/js/control-page.js"></script>
<div class="total-container">
	<div class="peopleNeed">           
		<div class="peopleNeed__container"></div>
	</div>
	<div class="goodDeeds">
		<div class="goodDeeds__container"></div>
	</div>
	<div class="daydream">
		<div class="daydream__container">
			<div class="butt-play"></div>
		</div>
	</div>
       
       
       
       <div class="blockInfo">
            
            <div class="blockInfo__container">
                 
                 <ul class="blockInfo__buttCont">
                      <li><a class="blockInfo__give" href="http://wayofhelp.com/cat/thing-market/sort_new/">Отдам вещи</a></li>
                      <li><a class="blockInfo__necessary" href="http://wayofhelp.com/cat/goods/sort_new/">Нужны вещи</a></li>
                 </ul>
                 
                 <div class="socNet">
                      <span class="socNet__text">Подписывайся на нас в социальных сетях и ежедневно заряжайся позитивом!</span>
                      <ul class="socNet__list">
                           <li><a class="socNet-ok" href="http://ok.ru/group/53534055989344" target="_blank"></a></li>
                           <li><a class="socNet-fb" href="https://www.facebook.com/WayofHelp" target="_blank"></a></li>
                           <li><a class="socNet-vk" href="https://vk.com/wayofhelp" target="_blank"></a></li>
                      </ul>
                 </div>
                 
            </div>
            
       </div>
       
       
       
       <div class="hFooter"></div> 
</div>