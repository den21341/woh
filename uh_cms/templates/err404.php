<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error 404 - Not found</title>
    <!-- Bootstrap -->
    <link href="<?=WWWHOST?>css/bootstrap_zero.css" rel="stylesheet">

    <!-- project local -->
    <link rel="stylesheet" href="<?=WWWHOST.'css/owl.carousel.css'?>">
    <link href="<?=WWWHOST?>css/all.css" rel="stylesheet">
    <link href="<?=WWWHOST?>css/_all.css" rel="stylesheet">
</head>
<body>
<div class="row">
    <div class="container-fluid err-container">
        <div class="bg">
            <img src="<?=WWWHOST?>img/face-noreviews.png" alt="Подсказчик" height="136px" width="136px">
            <h3>К сожалению, запрашиваемая Вами страница не найдена.</h3>
            <div class="sub-info">
                <p>Чтобы найти интересующую Вас информацию, предлагаем следующие пути:</p>
                <ul>
                    <li><a href="<?=WWWHOST?>">перейти на </a></li>
                    <li><a href="<?=WWWHOST?>">создать проект на <a href="<?=WWWHOST.'cat/'?>">Оказание помощи</a></li>
                    <li><a href="<?=WWWHOST?>">создать проект на <a href="<?=WWWHOST.'proj/add/'?>">Получение помощи</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>