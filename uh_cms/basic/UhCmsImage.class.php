<?php
class UhCmsImage {
    public $img;

    public $transparent;

    public $width;

    public $height;

    public function __construct($img = null) {
        if ($img != null) {
            if(exif_imagetype($img) == 2)
                $this->img = imagecreatefromjpeg($img);
            else if(exif_imagetype($img) == 3)
                $this->img = imagecreatefrompng($img);

            $this->width = imagesx($this->img);
            $this->height = imagesy($this->img);
            $this->setTransparentColour();
        }
    }

    public function create($width, $height, $transparent) {
        $this->img = imagecreatetruecolor($width, $height);
        $this->width = $width;
        $this->height =$height;

        $this->setTransparentColour();

        if (true === $transparent) {
            imagefill($this->img, 0, 0, $this->transparent);
        }
    }

    public function setTransparentColour($red = 255, $green = 0, $blue = 255) {
        $this->transparent = imagecolorallocate($this->img, $red, $green, $blue);
        imagecolortransparent($this->img, $this->transparent);
    }

    public function circleCrop() {
        $mask = imagecreatetruecolor($this->width, $this->height);
        $black = imagecolorallocate($mask, 0, 0, 0);
        $magenta = imagecolorallocate($mask, 255, 0, 255);

        imagefill($mask, 0, 0, $magenta);

        imagefilledellipse(
            $mask,
            ($this->width / 2),
            ($this->height / 2),
            $this->width,
            $this->height,
            $black
        );

        imagecolortransparent($mask, $black);

        imagecopymerge($this->img, $mask, 0, 0, 0, 0, $this->width, $this->height, 100);

        imagedestroy($mask);
    }

    public function merge(UhCmsImage $in, $dst_x = 0, $dst_y = 0) {
        imagecopymerge(
            $this->img,
            $in->img,
            $dst_x,
            $dst_y,
            0,
            0,
            $in->width,
            $in->height,
            100
        );
    }

    public function render() {
        header('Content-type: image/png');

        imagepng($this->img);
    }

    public function imageresize($percent = 0.5) {
        $newwidth = $this->width * $percent;
        $newheight = $this->height * $percent;

        $thumb = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresized($thumb, $this->img, 0, 0, 0, 0, $newwidth, $newheight, $this->width, $this->height);

        $this->img = $thumb;
        $this->width = $newwidth;
        $this->height = $newheight;
        $this->setTransparentColour();
    }

    public function cut($m_width, $m_height) {
        $offset_x = 0;
        $offset_y = 0;

        $new_height = $this->height - $m_height;
        $new_width = $this->width - $m_width;

        $thumb = imagecreatetruecolor($new_width, $new_height);
        imagecopy($thumb, $this->img, 0, 0, $offset_x, $offset_y, $this->width, $this->height);

        $this->img = $thumb;
        $this->width = $new_width;
        $this->height = $new_height;
        $this->setTransparentColour();
    }

    public function addText($text, $font_url, $size, $angle, array $color_arr, array $pos_arr) {
        imagettftext($this->img, $size, $angle, $pos_arr[0], $pos_arr[1], imagecolorallocate($this->img, $color_arr[0], $color_arr[1], $color_arr[2]), $font_url, $text);
    }

    public function toBite() {
        ob_start();
        imagepng($this->img);
        return ob_get_clean();
    }

    public function saveImage($src, $uid) {
        $path = 'img/user_share/'.$src.'_'.$uid.'.png';

        if(file_exists($path))
            unlink($path);

        return imagepng($this->img, $path);
    }

    // STATIC FUNCTION FOR THIS CLASS TO CREATE IMAGE FOR SHARE

    static public function getAdaptTit($numb) {
        $numb = $numb % 10;

        if($numb == 1)
            return array('Доброе', 'дело');
        else if($numb >= 2 && $numb <= 4)
            return array('Добрых', 'дела');
        else if(($numb >= 5 && $numb <= 9) || $numb == 0)
            return array('Добрых', 'дел');

        return array('', '');
    }

    static public function renderShareImgVK($user_info, $hlpco) {
        $img = new UhCmsImage();
        $img->create(600, 268, true);

        $img2 = new UhCmsImage(WWWHOST.'images/woh_vk2.jpg');
        $img2->addText($user_info['name'].' '.$user_info['fname'], 'fonts/UbuntuBold.ttf', 11, 0, array(141, 247, 151), array(425, 30));
        $img2->addText($hlpco, 'fonts/UbuntuBold.ttf', 30, 0, array(255, 255, 255), array(
            strlen($hlpco) == 1 ? 545 : (strlen($hlpco) == 3 ? 520 : 530) ,
            140));

        $img2->addText(UhCmsImage::getAdaptTit($hlpco)[0], 'fonts/UbuntuRegular.ttf', 14, 0, array(255, 255, 255), array(520, 170));
        $img2->addText(UhCmsImage::getAdaptTit($hlpco)[1], 'fonts/UbuntuRegular.ttf', 14, 0, array(255, 255, 255), array(537, 190));
        $img->merge($img2, 0, 0);

        $img3 = new UhCmsImage(($user_info['pic'] && file_exists($user_info['pic']) ? WWWHOST.$user_info['pic'] : WWWHOST.'img/no-pic.png'));
        $img3->imageresize(0.8);
        $img3->circleCrop();
        $img3->cut(0, 22);

        $img->merge($img3, 56, 134);

        //$img->render();
        $img->saveImage('vk', $user_info['id']);
    }

    static public function renderShareImgFB($user_info, $hlpco) {
        $img = new UhCmsImage();
        $img->create(486, 255, true);

        $img2 = new UhCmsImage(WWWHOST.'images/woh_fb.jpg');
        $img2->addText($user_info['name'].' '.$user_info['fname'], 'fonts/UbuntuBold.ttf', 10, 0, array(141, 247, 151), array(340, 25));
        $img2->addText($hlpco, 'fonts/UbuntuBold.ttf', 26, 0, array(255, 255, 255), array(
            strlen($hlpco) == 1 ? 440 : (strlen($hlpco) == 3 ? 420 : 430) ,
            145));
        $img2->addText(UhCmsImage::getAdaptTit($hlpco)[0], 'fonts/UbuntuBold.ttf', 12, 0, array(255, 255, 255), array(420, 170));
        $img2->addText(UhCmsImage::getAdaptTit($hlpco)[1], 'fonts/UbuntuBold.ttf', 12, 0, array(255, 255, 255), array(435, 190));
        $img->merge($img2, 0, 0);

        $img3 = new UhCmsImage(($user_info['pic'] && file_exists($user_info['pic']) ? WWWHOST.$user_info['pic'] : WWWHOST.'img/no-pic.png'));
        $img3->imageresize(0.66);
        $img3->circleCrop();

        $img->merge($img3, 50, 122);

        //$img->render();
        $img->saveImage('fb', $user_info['id']);
    }

    static public function renderShareImgOK($user_info, $hlpco) {
        $img = new UhCmsImage();
        $img->create(176, 172, true);

        $img2 = new UhCmsImage(WWWHOST.'images/woh_ok.jpg');
        $img2->addText($user_info['name'].' '.$user_info['fname'], 'fonts/UbuntuBold.ttf', 6, 0, array(141, 247, 151), array(74, 14));
        $img2->addText($hlpco, 'fonts/UbuntuBold.ttf', 16, 0, array(255, 255, 255), array(
            strlen($hlpco) == 1 ? 151 : (strlen($hlpco) == 3 ? 138 : 146) ,
            110));
        $img2->addText(UhCmsImage::getAdaptTit($hlpco)[0], 'fonts/UbuntuRegular.ttf', 7, 0, array(255, 255, 255), array(140, 122));
        $img2->addText(UhCmsImage::getAdaptTit($hlpco)[1], 'fonts/UbuntuRegular.ttf', 7, 0, array(255, 255, 255), array(144, 135));
        $img->merge($img2, 0, 0);

        $img3 = new UhCmsImage(($user_info['pic_sm'] && file_exists($user_info['pic_sm']) ? WWWHOST.$user_info['pic_sm'] : WWWHOST.'img/no-pic.png'));
        $img3->imageresize(0.80);
        $img3->circleCrop();
        $img3->cut(0, 10);

        $img->merge($img3, 11, 118);

        //$img->render();
        $img->saveImage('ok', $user_info['id']);
    }
}