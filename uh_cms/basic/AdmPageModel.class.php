<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class AdmPageModel extends Model{
	protected $Langs;
	protected $LangId;
	protected $page;
	protected $view_lang;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->view_lang = "ru";
		$this->Langs = $this->loadLangs();
		$this->LangId = $LangId;
		$this->ses = UhCmsAdminApp::getSesInstance();
		
		//$this->loadAdminMenu();
	}
	
	public function getViewLang()
	{
		return $this->view_lang;
	}
	
	protected function loadLangs()
	{
		$query = "SELECT * FROM ".TABLE_LANGS." ORDER BY id";
		$langs = $this->db->query($query);
		return $langs;
	}			
	
	public function getLangId()
	{
		return $this->LangId;
	}
	
	public function getLangs()
	{
		return $this->Langs;
	}
	
	
	public function initElements()
	{
		//global $tag_name, $menu_items, $menu_sections_num, $menu_items_num, $menu_itgroup, $menu_items2gr;
		
		$this->tag_name = "";

		$this->menu_sections = array();
		$this->menu_sections_num = 0;

		$this->menu_items = array();
		$this->menu_items_num = 0;

		$this->menu_items2gr = array();
		$this->menu_itgroup = 0;
	}

	public function startElement($parser, $name, $attrs)
	{
		//global $tag_name, $menu_items, $menu_sections_num, $menu_items_num, $menu_itgroup, $menu_items2gr;
					
		$this->tag_name = $name;
		
		if( $this->tag_name == "MENUSECTION" )
		{
			if( empty( $this->menu_items2gr[$attrs['GROUP']] ) )
			{
				$this->menu_items2gr[$attrs['GROUP']] = Array();
			}			
			
			$this->menu_itgroup = $attrs['GROUP'];
		}

		if( $this->tag_name == "MENUITEM" )
		{
			if( empty($this->menu_items[$this->menu_sections_num]) )
				$this->menu_items[$this->menu_sections_num] = Array();
			
			$this->menu_items[$this->menu_sections_num][$this->menu_items_num]["link"] = $attrs["LINKURL"];

			$this->menu_items2gr[$this->menu_itgroup][] = Array("link" => $attrs["LINKURL"], "name" => "");
		}
	}

	public function endElement($parser, $name)
	{
		//global $tag_name, $menu_sections_num, $menu_items_num;

		if( $name == "MENUSECTION" )
		{
			$this->menu_sections_num++;
			$this->menu_items_num = 0;
		}

		$this->tag_name = "";
	}

	public function dataElement($parser, $data)
	{
		//global	$lang, $tag_name, $menu_sections, $menu_sections_num, $menu_items, $menu_items_num, $menu_itgroup, $menu_items2gr;						

		switch( $this->tag_name )
		{
			case "MENUSECTION":
				break;

			case "MENUTITLE":
				if( trim($data) != "" )
					$this->menu_sections[$this->menu_sections_num] = $data;
				break;

			case "MENUITEM":
				if( trim($data) != "" )
				{
					$this->menu_items[$this->menu_sections_num][$this->menu_items_num]["name"] = $data;
					$this->menu_items2gr[$this->menu_itgroup][count($this->menu_items2gr[$this->menu_itgroup])-1]['name'] = $this->menu_items[$this->menu_sections_num][$this->menu_items_num]["name"];
					$this->menu_items_num++;
				}
				break;

			case "LANGUAGE":
				$lang = $data;
				break;
		}
	}
	
	public function loadAdminMenu()
	{
		////////////////////////////////////////////////////////////////////////////
		//		Build menu from xml file
		//switch( $UserGroup )
		switch( $this->ses->UserGroup )
		{
			case GROUP_ADMIN:
				$file = "templates/config.xml";
				break;

			case GROUP_MANAGER:
				$file = "templates/config_manager.xml";
				break;

			case GROUP_OPERATOR:
				$file = "templates/config_operator.xml";
				break;

			case GROUP_SEO:
				$file = "templates/config_seo.xml";
				break;

			default:
				$file = "templates/config_empty.xml";
		}		
			
		$this->initElements();
		
		$xml_parser = xml_parser_create();
		xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
		//xml_parser_set_option($xml_parser, XML_OPTION_TARGET_ENCODING, "1251");
		xml_set_element_handler($xml_parser, array($this, "startElement"), array($this, "endElement"));
		xml_set_character_data_handler($xml_parser, array($this, "dataElement"));
		if (!($fp = fopen($file, "r"))) {
			die("could not open XML input");
		}

		//echo "start";
		while ($data = fread($fp, 32096))
		{
			//echo "<pre>".$data."</pre>";
			if (!xml_parse($xml_parser, $data, feof($fp)))
			{
				die(sprintf("XML error: %s at line %d",
						xml_error_string(xml_get_error_code($xml_parser)),
						xml_get_current_line_number($xml_parser)));
			}
		}
		//echo "end";
		xml_parser_free($xml_parser);
						
		return $this->getAdminMenu();
	}
	
	public function getAdminMenu()
	{
		return array("menu_sections" => $this->menu_sections, "menu_items" => $this->menu_items, "menu_2groups" => $this->menu_items2gr );
	}
}
?>