<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsAuthUserSes extends UhCmsSes
{
	public function AuthUser()
	{
		$query = "SELECT u1.login, u1.account_type as group_id, u1.id, u1.name, u1.guid_uhash, a1.sc_token, a1.sc_uid  
        	FROM ".TABLE_SHOP_BUYER_AUTH." a1, ".TABLE_SHOP_BUYERS." u1
        	WHERE a1.ses_id='".$this->sesid()."' AND a1.user_login=u1.login AND a1.user_passwd=u1.passwd";

		//echo $query."<br>";
		
		$authits = $this->db->query( $query );
		for( $i=0; $i<count($authits); $i++ )
		{
			//var_dump($authits);
			//echo "<br>";
			if( $i == 0 )
			{
				$this->ses_params['UserId'] = $authits[$i]['id'];
				$this->ses_params['UserHash'] = $authits[$i]['guid_uhash'];
				$this->ses_params['UserGroup'] = $authits[$i]['group_id'];
				$this->ses_params['UserLogin'] = stripslashes($authits[$i]['login']);
				$this->ses_params['UserName'] = stripslashes($authits[$i]['name']);
				$this->ses_params['ScToken'] = stripslashes($authits[$i]['sc_token']);
				$this->ses_params['ScUid'] = stripslashes($authits[$i]['sc_uid']);
			}
			else
			{
				//echo "Dub auth<br>";
			}
		}
		
		unset($authits);
	}
	
	public function AuthMake($ulogin, $upass)
	{		
		//echo $this->session_id."<br>";
		if( $this->session_id == "" )
		{
			return false;
		}
		
        $query = "SELECT *, account_type as group_id FROM ".TABLE_SHOP_BUYERS."         		
        		WHERE login='".addslashes($ulogin)."' AND passwd=PASSWORD('".addslashes($upass)."') AND isactive=1 AND isactive_web=1";        
		$res = $this->db->query( $query );
		if( count($res) > 0 )
		{
			$already_signedin = false;
			
			$query1 = "SELECT * FROM ".TABLE_SHOP_BUYER_AUTH."
                	WHERE ses_id='".$this->sesid()."' AND user_login='".addslashes($ulogin)."'
                	AND user_passwd=PASSWORD('".addslashes($upass)."')";
			$res1 = $this->db->query( $query1 );
			if( count($res1) > 0 )
			{
				$already_signedin = true;						
			}
			
			if( !$already_signedin )
			{
				$query = "INSERT INTO ".TABLE_SHOP_BUYER_AUTH." (ses_id, user_login, user_passwd, time_added) 
					VALUES ('".$this->sesid()."', '".addslashes($ulogin)."', PASSWORD('".addslashes($upass)."'), NOW() )";
				if( !$this->db->exec($query) )
				{
					return false;
				}
			}			
			
			$this->ses_params['UserId'] = $res[0]['id'];
			$this->ses_params['UserHash'] = $res[0]['guid_uhash'];
			$this->ses_params['UserGroup'] = $res[0]['group_id'];
			$this->ses_params['UserLogin'] = $ulogin;
			$this->ses_params['UserName'] = stripslashes($res[0]['name']);
			$this->ses_params['ScToken'] = '';
			$this->ses_params['ScUid'] = '';
		}
		
		if( $this->ses_params['UserId'] == 0 )
			return false;
		
		return true;
	}
	
	public function AuthClear()
	{
		$query = "DELETE FROM ".TABLE_SHOP_BUYER_AUTH." WHERE ses_id='".$this->sesid()."'";
		if( !$this->db->exec($query) )
		{
			return false;
		}
		
		$this->ses_params['UserId'] = 0;
		$this->ses_params['UserHash'] = "";
		$this->ses_params['UserGroup'] = 0;
		$this->ses_params['UserLogin'] = "";
		$this->ses_params['UserName'] = "";
		$this->ses_params['ScToken'] = "";
		$this->ses_params['ScUid'] = "";
		
		return true;
	}
	
	public function AuthMakeAutoByActGuid($guid)
	{
		$query = "DELETE FROM ".TABLE_SHOP_BUYER_AUTH." WHERE ses_id='".$this->sesid()."' ";
		$this->db->exec($query);
		
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE guid_act='".addslashes($guid)."'";
		$res = $this->db->query( $query );
		if( count($res) > 0 )
		{
			$this->ses_params['UserId'] = $res[0]['id'];
			$this->ses_params['UserHash'] = $res[0]['guid_uhash'];
			$this->ses_params['UserGroup'] = $res[0]['group_id'];
			$this->ses_params['UserLogin'] = stripslashes($res[0]['login']);
			$this->ses_params['UserName'] = stripslashes($res[0]['name']);
			$this->ses_params['ScToken'] = $token;
			$this->ses_params['ScUid'] = $scuser_id;
			
			$query = "INSERT INTO ".TABLE_SHOP_BUYER_AUTH." (ses_id, user_login, user_passwd, time_added)
					VALUES ('".$this->sesid()."', '".addslashes($res[0]['login'])."', '".addslashes($res[0]['passwd'])."', NOW() )";
			if( !$this->db->exec($query) )
			{
				return false;
			}
				
			return true;
		}
		
		return false;
	}
	
	public function AuthMakeSocial($uid, $ulogin, $upass, $token, $scuser_id='', $inregmode = false)
	{
		$query = "DELETE FROM ".TABLE_SHOP_BUYER_AUTH." WHERE ses_id='".$this->sesid()."' ";
		$this->db->exec($query);
		
		$query = "INSERT INTO ".TABLE_SHOP_BUYER_AUTH." (ses_id, user_login, user_passwd, time_added, sc_uid, sc_token)
			VALUES ('".addslashes($this->sesid())."', '".addslashes($ulogin)."', 
			".($inregmode ? " PASSWORD('".addslashes($upass)."')" : "'".addslashes($upass)."'").", NOW(), '".addslashes($scuser_id)."','".addslashes($token)."')";
		if( !$this->db->exec($query) )
			return false;
		
		$query = "SELECT *, account_type as group_id FROM ".TABLE_SHOP_BUYERS." WHERE id='$uid'";
		$res = $this->db->query( $query );
		if( count($res) > 0 )
		{
			$this->ses_params['UserId'] = $res[0]['id'];
			$this->ses_params['UserHash'] = $res[0]['guid_uhash'];
			$this->ses_params['UserGroup'] = $res[0]['group_id'];
			$this->ses_params['UserLogin'] = $ulogin;
			$this->ses_params['UserName'] = stripslashes($res[0]['name']);
			$this->ses_params['ScToken'] = $token;
			$this->ses_params['ScUid'] = $scuser_id;
			
			return true;
		}
		
		return false;		
	}
		
	
	public function AddSocialSession($token, $scuser_id='')
	{
		$query = "SELECT * FROM ".TABLE_SHOP_BUYER_AUTH." WHERE ses_id='".$this->sesid()."' ";
		$res = $this->db->query( $query );
		if( count($res) > 0 )
		{
			// Already auhtorized
			$query = "UPDATE ".TABLE_SHOP_BUYER_AUTH." SET sc_uid='".addslashes($scuser_id)."', sc_token='".addslashes($token)."' WHERE id=".$res[0]['id'];
			$this->db->exec($query);
		}
		else
		{
			// Add new session record
			$query = "INSERT INTO ".TABLE_SHOP_BUYER_AUTH." (ses_id, user_login, user_passwd, time_added, sc_uid, sc_token) 
					VALUES ('".addslashes($this->sesid())."', '', '', NOW(), '".addslashes($scuser_id)."', '".addslashes($token)."')";
			$this->db->exec($query);
		}
		
		$this->ses_params['ScToken'] = $token;
	}
	
	public function GetSessionToken()
	{
		$query = "SELECT * FROM ".TABLE_SHOP_BUYER_AUTH." WHERE ses_id='".$this->sesid()."' ";
		$res = $this->db->query( $query );
		if( count($res) > 0 )
		{
			$this->ses_params['ScToken'] = stripslashes($res[0]['sc_token']);
			
			return $this->ses_params['ScToken'];
		}
		
		return '';
	}
	
	public function GetSessionScuserid()
	{
		$query = "SELECT * FROM ".TABLE_SHOP_BUYER_AUTH." WHERE ses_id='".$this->sesid()."' ";
		$res = $this->db->query( $query );
		if( count($res) > 0 )
		{
			$this->ses_params['ScUid'] = stripslashes($res[0]['sc_uid']);
				
			return $this->ses_params['ScUid'];
		}
	
		return '';
	}
}
?>