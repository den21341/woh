<?php
/**
 * Created by PhpStorm.
 * User: Денис
 * Date: 11.03.2016
 * Time: 10:17
 */

class iPageView extends PageView {

    public function __construct($config, $pModel) {
        parent::__construct($config, $pModel);
    }

    public function renderLanding($page_tpl_file) {
        include $this->cfg['MVC_PATH']."landing/".$page_tpl_file;
    }
}