<?php

class UhCmsAdwords {

    public function __construct() {
        require_once 'adwords/examples/AdWords/v201609/init.php';
    }

    public function AddOflineConversion(AdWordsUser $user, $conversionName, $gclid, $conversionTime, $conversionValue) {

        $offlineConversionService = $user->GetService('OfflineConversionFeedService', ADWORDS_VERSION);

        // Associate offline conversions with the existing named conversion tracker.
        // If this tracker was newly created, it may be a few hours before it can
        // accept conversions.

        $feed = new OfflineConversionFeed();
        $feed->conversionName = $conversionName;
        $feed->conversionTime = $conversionTime;
        $feed->conversionValue = $conversionValue;
        $feed->googleClickId = $gclid;

        $offlineConversionOperation = new OfflineConversionFeedOperation();
        $offlineConversionOperation->operator = 'ADD';
        $offlineConversionOperation->operand = $feed;

        $offlineConversionOperations = array($offlineConversionOperation);
        $result = $offlineConversionService->mutate($offlineConversionOperations);

        $feed = $result->value[0];

        printf('Uploaded offline conversion value of %d for Google Click ID = ' .
            "'%s' to '%s'.", $feed->conversionValue, $feed->googleClickId,
            $feed->conversionName);
    }

    public function getUserObj() {
        require_once 'adwords/src/Google/Api/Ads/AdWords/Lib/AdWordsUser.php';

        return new AdWordsUser();
    }
}