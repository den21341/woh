<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsAuthSes extends UhCmsSes
{
	public function AuthUser()
	{
		$query = "SELECT u1.login, u1.group_id, u1.id, u1.name 
        	FROM ".TABLE_USER_AUTH." a1, ".TABLE_USERS." u1
        	WHERE a1.ses_id='".$this->sesid()."' AND a1.user_login=u1.login AND a1.user_passwd=u1.passwd";
			
		$authits = $this->db->query( $query );
		for( $i=0; $i<count($authits); $i++ )
		{
			if( $i == 0 )
			{
				$this->ses_params['UserId'] = $authits[$i]['id'];
				$this->ses_params['UserGroup'] = $authits[$i]['group_id'];
				$this->ses_params['UserLogin'] = stripslashes($authits[$i]['login']);
				$this->ses_params['UserName'] = stripslashes($authits[$i]['name']);
			}
			else
			{
				echo "Dub auth<br>";
			}
		}
		
		unset($authits);
	}
	
	public function AuthMake($ulogin, $upass)
	{		
		//echo $this->session_id."<br>";
		if( $this->session_id == "" )
		{
			return false;
		}
		
		//echo "Make auth<br>";
		
        $query = "SELECT * FROM ".TABLE_USERS." WHERE login='".addslashes($ulogin)."' AND passwd=PASSWORD('".addslashes($upass)."')";
		$res = $this->db->query( $query );
		if( count($res) > 0 )
		{
			$already_signedin = false;
			
			$query1 = "SELECT * FROM ".TABLE_USER_AUTH."
                	WHERE ses_id='".$this->sesid()."' AND user_login='".addslashes($ulogin)."'
                	AND user_passwd=PASSWORD('".addslashes($upass)."')";
			$res1 = $this->db->query( $query1 );
			if( count($res1) > 0 )
			{
				$already_signedin = true;						
			}
			
			if( !$already_signedin )
			{
				$query = "INSERT INTO ".TABLE_USER_AUTH." (ses_id, user_login, user_passwd, time_added) 
					VALUES ('".$this->sesid()."', '".addslashes($ulogin)."', PASSWORD('".addslashes($upass)."'), NOW() )";
				if( !$this->db->exec($query) )
				{
					return false;
				}
			}			
			
			$this->ses_params['UserId'] = $res[0]['id'];
			$this->ses_params['UserGroup'] = $res[0]['group_id'];
			$this->ses_params['UserLogin'] = $ulogin;
			$this->ses_params['UserName'] = stripslashes($res[0]['name']);
		}
		
		if( $this->ses_params['UserId'] == 0 )
			return false;
		
		return true;
	}
	
	public function AuthClear()
	{
		$query = "DELETE FROM ".TABLE_USER_AUTH." WHERE ses_id='".$this->sesid()."'";
		if( !$this->db->exec($query) )
		{
			return false;
		}
		
		$this->ses_params['UserId'] = 0;
		$this->ses_params['UserGroup'] = 0;
		$this->ses_params['UserLogin'] = "";
		$this->ses_params['UserName'] = "";
		
		return true;
	}
}
?>