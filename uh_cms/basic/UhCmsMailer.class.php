<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsMailer{
	protected	$cfg;
	protected	$from_mail;
	protected	$from_name;
	
	function __construct($cfg)
	{
		$this->from_mail = "";
		$this->from_name = "Служба поддержки ".mb_substr(WWWHOST, mb_strlen("http://"), mb_strlen(WWWHOST)-mb_strlen("http://")-1);
		$this->cfg = $cfg;
	}	
	
	public function setFrom($email, $name)
	{
		$this->from_mail = $email;
		$this->from_name = $name;
	}
	
	public function setFromName($name)
	{
		$this->from_name = $name;
	}
	
	public function sendMail($to, $subject, $txt)
	{
		$this->sendPlain($to, $subject, $txt);
	}

	public function sendHTMLmail($to, $sub, $txt){
		//$this->sendHTMLmailGate($to, $sub, $txt);
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

// Дополнительные заголовки
		$headers .= 'From: Служба WayOfHelp <wayofhelp.com>' . "\r\n";
		$headers .= 'Cc: wayofhelp.com' . "\r\n";
		$headers .= 'Bcc: wayofhelp.com' . "\r\n";

		mail($to, $sub, $txt, $headers);
	}
	
	protected function sendPlain($to, $subject, $txt)
	{
		UhCmsUtils::sendmail($this->from_mail, $this->from_name, $to, $subject, $txt);
	}
	
	protected function sendHtml($to, $subject, $txt)
	{
		UhCmsUtils::sendmail($this->from_mail, $this->from_name, $to, $subject, $txt);
	}

	protected function sendHTMLmailGate($to, $sub, $txt){
		UhCmsUtils::sendHTML($this->from_mail, $this->from_name, $to, $sub, $txt);
	}
}
?>