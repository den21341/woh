<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsNews{
	protected	$db;
	protected	$LangId;
	protected	$newsGroups;
	
	function __construct($db, $LangId)
	{		
		$this->db = $db;
		$this->LangId = $LangId;
		
		$this->newsGroups = Array(0 => "Новости", 1 => "Добрая стена", 2 => "Обзоры");
	}	
		
	public function News_GroupList()
	{
		return $this->newsGroups;
	}
	
	public function News_GroupInfo($group_id)
	{
		if( isset($this->newsGroups[$group_id]) )
			return $this->newsGroups[$group_id];
		
		return "";
	}
	
	/*
	function News_BuildUrl_PHP($LangId, $group=-1, $pi=0, $pn=20, $nid=0, $mod="", $y=0, $m=0)
	{
		global $PHP_SELF;
		// sample link styles
		//   news.php
		//   news.php?pagei=2
		//   news.php?id=10
		//   news.php?mode=arc
		//   news.php?mode=arc&dm=01&dy=2010
		//   news.php?mode=arc&dm=01&dy=2010&id=8
		$url = "news.php";

		$groupurl = "";
		if( $group != -1 )
		{
			$groupurl = "group=".$group."&";
		}

		if( $nid == 0 )
		{
			// The news item not specified
			if( $mod == "arc" )
				$url .= "?".$groupurl."mode=arc".($y != 0 ? "&dy=".$y : "").($m != 0 ? "&dm=".$m : "");
			else
				$url .= ($pi == 0 ? "?".$groupurl : "?".$groupurl."pagei=".$pi);
		}
		else
		{
			// News item is specified
			if( $mod == "arc" )
				$url .= "?".$groupurl."mode=".$mod."&dy=".$y."&dm=".$m."&id=".$nid;
			else
				$url .= "?".$groupurl."id=".$nid;
		}

		return $url;
	}

	function News_BuildUrl_HTML($LangId, $group=-1, $pi=0, $pn=20, $nid=0, $mod="", $y=0, $m=0)
	{
		// sample link styles
		//   news/index.html
		//   news/page_2_20.html
		//   news/01_2010/10.html
		//   news/archive.html
		//   news/01_2010/index.html
		//   news/01_2010/8.html

		$url = "news/";

		if( $group == 1 )
		{
			$url = "article/";
		}
		else if( $group == 2 )
		{
			$url = "obzor/";
		}

		if( $nid == 0 )
		{
			// The news item not specified
			if( $mod == "arc" )
			{
				$url .= ( $y != 0 ? "archive.html" : sprintf("%02d", $m)."_".sprintf("%04d", $y)."/index.html" );
			}
			else
			{
				$url .= ( $pi > 0 ? "page_".$pi."_".$pn.".html" : "index.html" );
			}
		}
		else
		{
			// News item is specified
			$url .= sprintf("%02d", $m)."_".sprintf("%04d", $y)."/".$nid.".html";
		}

		return $url;
	}

	function News_BuildUrl($LangId, $group=-1, $pi=0, $pn=20, $nid=0, $mod="", $y=0, $m=0)
	{
		global $UHCMS_LINK_MODE, $WWWHOST;

		//if( $UHCMS_LINK_MODE == UH_LINKMODE_PHP )
		//	$wwwlink = substr( $WWWHOST, 0, strpos($WWWHOST, "/", 7) );
		$wwwlink = $WWWHOST;

		return (
			$UHCMS_LINK_MODE == UH_LINKMODE_PHP ?
				$wwwlink.News_BuildUrl_PHP($LangId, $group, $pi, $pn, $nid, $mod, $y, $m) :
				$WWWHOST.News_BuildUrl_HTML($LangId, $group, $pi, $pn, $nid, $mod, $y, $m)
			);
	}
	*/

	public function News_ItemsNum($ngroup=-1)
	{
		//global $TABLE_NEWS;

		$totnews = 0;

		$cond = "";
		if( $ngroup != -1 )
		{
			$cond = " WHERE ngroup='".$ngroup."' ";
		}

		$query = "SELECT count(*) as totnews FROM ".TABLE_NEWS." $cond";
		$its = $this->db->query( $query );
		if( count($its) > 0 )
		{
			$totnews = $its[0]['totnews'];
		}
		/*
		if( $res = mysql_query( $query ) )
		{
			if( $row = mysql_fetch_object( $res ) )
			{
				$totnews = $row->totnews;
			}
			mysql_free_result( $res );
		}
		*/

		return $totnews;
	}

	public function News_Items($ngroup=-1, $sortby="add", $anons_len=300, $pi=-1, $pn=20, $year=0, $month=0, $sectid=0)
	{
		//global $TABLE_NEWS, $TABLE_NEWS_LANGS, $WWWHOST, $FILE_DIR, $UHCMS_DEF_ENC;

		$sort_cond = "m1.dtime DESC";
		switch( $sortby )
		{
			case "add":
				$sort_cond = "m1.dtime DESC";
				break;

			case "add_asc":
				$sort_cond = "m1.dtime";
				break;
		}

		$sel_cond = "";
		if( ($month != 0) && ($year != 0) )
		{
			if( $year != 0 )
				$sel_cond .= " AND YEAR(dtime)='".$year."' ";

			if( $month != 0 )
				$sel_cond .= " AND MONTH(dtime)='".$month."' ";
		}

		if( $sectid != 0 )
		{
			$sel_cond .= " AND m1.sect_id='".$sectid."' ";
		}

		$limit_cond = "";
		if( $pi != -1 )
		{
			$limit_cond = " LIMIT ".($pi*$pn).",$pn";
		}

		$typecond = "";
		if( $ngroup != -1 )
		{
			$typecond = " m1.ngroup='".$ngroup."' AND ";
		}
		
		$query = "SELECT m1.*, m2.title, m2.content, DAYOFMONTH(dtime) as day, MONTH(dtime) as month, YEAR(dtime) as year
				FROM ".TABLE_NEWS." m1, ".TABLE_NEWS_LANGS." m2
				WHERE $typecond m1.id=m2.news_id AND m2.lang_id='".$this->LangId."' $sel_cond
				ORDER BY $sort_cond
				$limit_cond";
		$its = $this->db->query( $query );
		
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			$its[$i]['title'] = stripslashes($row['title']);
			$its[$i]['content'] = stripslashes($row['content']);
			$its[$i]['date'] = sprintf("%02d.%02d.%04d", $row['day'], $row['month'], $row['year']);
			$its[$i]['img_src'] = ( stripslashes($row['filename_src']) != "" ? WWWHOST.FILE_DIR.stripslashes($row['filename_src']) : "" );
			$its[$i]['short'] = strip_tags($row['content']);
			if( mb_strlen( $its[$i]['short'], UHCMS_DEF_ENC ) > $anons_len )
				$its[$i]['short'] = mb_substr($its[$i]['short'], 0, $anons_len, UHCMS_DEF_ENC)."...";
		}
		
		return $its;
		
		/*
		$news = Array();
		$query = "SELECT m1.*, m2.title, m2.content, DAYOFMONTH(dtime) as day, MONTH(dtime) as month, YEAR(dtime) as year
				FROM $TABLE_NEWS m1, $TABLE_NEWS_LANGS m2
				WHERE $typecond m1.id=m2.news_id AND m2.lang_id='$LangId' $sel_cond
				ORDER BY $sort_cond
				$limit_cond";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object($res) )
			{
				$ni = Array();
				$ni['id'] = $row->id;
				$ni['group'] = $row->ngroup;
				$ni['title'] = stripslashes($row->title);
				$ni['text'] = stripslashes($row->content);
				$ni['date'] = sprintf("%02d.%02d.%04d",$row->day, $row->month, $row->year);
				$ni['date_d'] = $row->day;
				$ni['date_m'] = $row->month;
				$ni['date_y'] = $row->year;
				$ni['img_src'] = ( stripslashes($row->filename_src) != "" ? $WWWHOST.$FILE_DIR.stripslashes($row->filename_src) : '' );

				$shorttxt = strip_tags($ni['text']);
				//if( strlen( $shorttxt ) > $anons_len )
				//	$shorttxt = substr($shorttxt, 0, $anons_len)."...";
				if( mb_strlen( $shorttxt, $UHCMS_DEF_ENC ) > $anons_len )
					$shorttxt = mb_substr($shorttxt, 0, $anons_len, $UHCMS_DEF_ENC)."...";

				$ni['short'] = $shorttxt;

				$news[] = $ni;
			}
			mysql_free_result($res);
		}

		return $news;
		*/
	}

	public function News_ItemInfo($id)
	{
		//global $TABLE_NEWS, $TABLE_NEWS_LANGS, $WWWHOST, $FILE_DIR;
		
		$query = "SELECT m1.*, m2.title, m2.content, DAYOFMONTH(dtime) as day, MONTH(dtime) as month, YEAR(dtime) as year
			FROM ".TABLE_NEWS." m1 
			INNER JOIN ".TABLE_NEWS_LANGS." m2 ON m1.id=m2.news_id AND m2.lang_id='".$this->LangId."' 
			WHERE m1.id='$id'";
		$its = $this->db->query( $query );
		
		if( count($its) > 0 )
		{
			$row = $its[0];
			$its[0]['title'] = stripslashes($row['title']);
			$its[0]['content'] = stripslashes($row['content']);
			$its[0]['date'] = sprintf("%02d.%02d.%04d", $row['day'], $row['month'], $row['year']);
			$its[0]['img_src'] = ( stripslashes($row['filename_src']) != "" ? WWWHOST.FILE_DIR.stripslashes($row['filename_src']) : "" );
		}
		
		return ( count($its) > 0 ? $its[0] : $its );
		
		/*
		$ninfo = Array();

		$query = "SELECT m1.*, m2.title, m2.content, DAYOFMONTH(dtime) as day, MONTH(dtime) as month, YEAR(dtime) as year
			FROM $TABLE_NEWS m1, $TABLE_NEWS_LANGS m2
			WHERE m1.id='$id' AND m1.id=m2.news_id AND m2.lang_id='$LangId'";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object($res) )
			{
				$ninfo['id'] = $row->id;
				$ninfo['group'] = $row->ngroup;
				$ninfo['title'] = stripslashes($row->title);
				$ninfo['text'] = stripslashes($row->content);
				$ninfo['date'] = sprintf("%02d.%02d.%04d", $row->day, $row->month, $row->year);
				$ninfo['date_d'] = $row->day;
				$ninfo['date_m'] = $row->month;
				$ninfo['date_y'] = $row->year;
				$ninfo['img_src'] = ( stripslashes($row->filename_src) != "" ? $WWWHOST.$FILE_DIR.stripslashes($row->filename_src) : '' );
			}
			mysql_free_result($res);
		}		

		return $ninfo;
		*/
	}

	public function News_GetFirstItemDate($ngroup=-1)
	{
		//global $TABLE_NEWS;

		$dt_info = Array();
		$dt_info['d'] = 1;
		$dt_info['m'] = 1;
		$dt_info['y'] = date("Y", time());

		$cond = "";
		if( $ngroup != -1 )
		{
			$cond = " WHERE m1.ngroup='".$ngroup."' ";
		}

		$query = "SELECT DAYOFMONTH(dtime) as day, MONTH(dtime) as month, YEAR(dtime) as year
			FROM ".TABLE_NEWS." m1
			$cond
			ORDER BY m1.dtime LIMIT 0,1";
		$its = $this->db->query( $query );
		
		if( count($its) > 0 )	
		{
			$row = $its[0];
			$dt_info['m'] = $row['month'];
			$dt_info['y'] = $row['year'];
		}
		/*
		if( $res = mysql_query( $query ) )
		{
			if( $row = mysql_fetch_object($res) )
			{
				$dt_info['m'] = $row->month;
				$dt_info['y'] = $row->year;
			}
			mysql_free_result($res);
		}
		*/
		
		return $dt_info;
	}
}
?>