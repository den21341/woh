<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsAdminApp{
	private static $db = null;
	private static $ses = null;
	private $full_req_url = "";
	private $router;
	private $config;	
	
	function __construct($config){
		$this->config = $config;	

		self::$db = new UhCmsDb();
		self::$ses = new UhCmsAuthSes(self::$db);
	}	
	
	public function run()
	{
		$db_host 		= $this->config['DB_HOST'];
		$db_user 		= $this->config['DB_USER'];
		$db_password 	= $this->config['DB_PASSWORD'];
		$db_database 	= $this->config['DB_NAME'];
		
		$UHCMS_DEF_ENC 	= $this->config['DEF_ENC'];
		
		////////////////////////////////////////////////////////////////////////////
		// Make database connections		
		if( !self::$db->connect($db_host,$db_user,$db_password,$db_database, $UHCMS_DEF_ENC) )
		{
			//echo $this->db->getError();
			die(self::$db->getError());
		}
		
		if( $this->config['DEBUG_ON'] )
			self::$db->setDebug(true);
		
		////////////////////////////////////////////////////////////////////////
		// Initialize session
		self::$ses->startSession(SESSION_PATH);	

		// If supports user sessions
		if( $this->config['WITH_AUTH'] ){
			// Authentication here...
			self::$ses->AuthUser();
		}		
		$this->showLog("SES id: ".self::$ses->sesid().", UserId = ".self::$ses->UserId);
						
		////////////////////////////////////////////////////////////////////////
		// Start router to process requests
		$this->full_req_url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

		$this->showLog("Full path: ".$this->full_req_url);

		$routeurl = "";
		if( strncmp($this->full_req_url, WWWHOST, strlen(WWWHOST)) == 0 )
		{
			$routeurl = substr($this->full_req_url, strlen(WWWHOST));
		}

		$this->router = new UhCmsAdminRouter( $routeurl, "", $this->config, self::$db, self::$ses );
	}
	
	public static function getDbInstance(){
		return self::$db;
	}
	
	public static function getSesInstance(){
		return self::$ses;
	}
	
	private function showLog($str){
		if( $this->config['DEBUG_ON'] ){
			echo "App log: ".$str."<br>";
		}
	}
}
?>