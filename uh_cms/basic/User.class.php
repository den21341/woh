<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class User{
	protected	$db;
	
	function __construct($db)
	{		
		$this->db = $db;
	}	
		
	public function groupList()
	{
		$its = $this->db->query("SELECT * FROM ".TABLE_USER_GROUPS." ORDER BY id ");
		
		return $its;
	}
	
	public function groupInfo($group_id)
	{
		$its = $this->db->query("SELECT * FROM ".TABLE_USER_GROUPS." WHERE id='$group_id'");
		if( count($its) > 0 )
			return $its[0];
		
		return $its;
	}
	
	public function userInfo($uid)
	{
		$query = "SELECT g.id as group_id, g.group_name, u.id as id, u.login as login, u.isactive, u.name, u.address, u.city_id, u.country, u.zip_code,
			u.telephone, u.office_phone, u.cell_phone, u.email1, u.email2, u.email3, u.web_url 
			FROM ".TABLE_USERS." u, ".TABLE_USER_GROUPS." g
			WHERE u.group_id=g.id AND u.id='$uid'
			ORDER BY group_id, login";
			
		$its = $this->db->query( $query );
		
		return ( count($its) > 0 ? $its[0] : $its );
	}
	
	public function userList($sqlcond = "")	
	{
		$query = "SELECT g.id as group_id, g.group_name, u.id as id, u.login as login, u.isactive, u.name, u.address, u.city_id, u.country, u.zip_code,
			u.telephone, u.office_phone, u.cell_phone, u.email1, u.email2, u.email3, u.web_url 
			FROM ".TABLE_USERS." u, ".TABLE_USER_GROUPS." g
			WHERE u.group_id=g.id $sqlcond
			ORDER BY group_id, login";
			
		$its = $this->db->query( $query );
		
		return $its;
	}
}
?>