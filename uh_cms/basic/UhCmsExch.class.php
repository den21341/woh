<?php

class UhCmsExch {
    public static function conver($from = 'RUB', $to = 'UAH') {
        $to = ($to == 'GRN' ? 'UAH' : $to);
        $from = ($from == 'GRN' ? 'UAH' : $from);

       // $url = 'http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='. $from . $to .'=X';

        /*$handle = @fopen($url, 'r');
        if ($handle) {
            $result = fgets($handle, 4096);
            fclose($handle);
        }

        $allData = explode(',', $result);
        $currencyValue = $allData[1];*/

        $res = file_get_contents('http://free.currencyconverterapi.com/api/v3/convert?q='.$from.'_'.$to.'&compact=ultra');
        $res =  json_decode($res, true);

        return round($res[$from.'_'.$to], 2);
    }

}