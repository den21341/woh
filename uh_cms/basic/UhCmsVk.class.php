<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsVk{
	protected	$data;
	
	protected	$app_id;
	protected	$secret_id;
	protected	$user_id;
	
	function __construct($appid, $secretid)
	{
		$this->app_id = $appid;
		$this->secret_id = $secretid;
	}		
	
	function __set($pname, $val){
		$this->data[$pname] = $val;
	}
	
	function __get($pname){
		return (isset($this->data[$pname]) ? $this->data[$pname] : 0);
	}

	function __isset($pname){
		return isset($this->data[$pname]);
	}	
	
	public function makeLoginLink($redirurl)
	{		
		$VK_LOGIN_URL = 'https://oauth.vk.com/authorize';
		
		$params = array(
			'client_id'     => $this->app_id, //FB_APP_ID,
			'redirect_uri'  => $redirurl,
			'response_type' => 'code',
			'v' => '5.32',
			'state' => UhCmsApp::getSesInstance()->sesid(),
			//'scope'         => 'id,email,name,first_name,last_name,link,gender,user_birthday,verified'
			'scope'         => 'email,photos'
		);
		
		
		$VK_LINK = $VK_LOGIN_URL."?".urldecode(http_build_query($params));
		
		return $VK_LINK;
	}
	
	public function setSessionToken($token)
	{
		$this->access_token = $token;
	}
	
	public function setUserId($user_id)
	{
		$this->user_id = $user_id;
	}
	
	public function getSessionToken($redirurl, $code)
	{
		$VK_TOKEN_URL = 'https://oauth.vk.com/access_token';
		
		$params = array(
			'client_id'     => $this->app_id, //FB_APP_ID,
			'client_secret' => $this->secret_id,
			'redirect_uri'  => $redirurl,
			//'granted_scopes'=> 'public_profile,user_photos',
			'code'          => $code
		);		
		
		$vk_res = file_get_contents($VK_TOKEN_URL.'?'.http_build_query($params));
		
		//echo $vk_res."<br>";
		
		$tokenObj = null;
		//parse_str($vk_res, $tokenInfo);
		
		$tokenObj = json_decode($vk_res);
		
		$token_data = Array();
		$token_data['access_token'] = ( isset($tokenObj->access_token) ? $tokenObj->access_token : null );
		$token_data['user_id'] = ( isset($tokenObj->access_token) ? $tokenObj->user_id : 0 );
		$token_data['expires_in'] = ( isset($tokenObj->access_token) ? $tokenObj->expires_in : 0 );
		
		
		//var_dump($token_data);
		//echo "<br>";
		
		$this->access_token = $token_data['access_token']; //$tokenInfo['access_token'];
		
		return $token_data;
	}
	
	private function getMe($params)
	{
		$VK_ME_URL = 'https://api.vk.com/method/users.get';
		
		$url_req = $VK_ME_URL.'?'.urldecode(http_build_query($params));
		
		$userInfo = json_decode(file_get_contents($url_req), true);
		
		return $userInfo;
	}
	
	public function getUserPermissions()
	{
		$params = array(
			'fields' => 'permissions',
			'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		return $this->getMe($params);
	}
	
	public function getUserInfo($token = "", $user_id = 0)
	{
		$params = array(
			'user_ids' => $user_id,
			'fields' => 'photo_200,city,bdate,domain,site,verified',
			'name_case' => 'Nom',
			'v' => '5.32',
			'access_token' => ( $token != "" ? $token : $this->access_token )			
		);
		
		$res = $this->getMe($params);
		
		if( isset($res['response']) && count($res['response'])>0 )
		{
			$uInf = $res['response'][0];

			if( isset($uInf['id']) )
			{
				return $uInf;
			}			
		}
		
		return 	$res;
	}
	
	public function getUserAvatar($token = "", $user_id = 0)
	{
		$params = array(
			'user_ids' => $user_id,
			'fields' => 'photo_200',
			'name_case' => 'Nom',
			'v' => '5.32',
			'access_token' => ( $token != "" ? $token : $this->access_token )			
		);
		
		return $this->getMe($params);
	}
	
	/*
	public function getUserPhotos($token = "")
	{
		$params = array(
				'fields' => 'albums',
				'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		return $this->getMe($params);		
	}
	
	public function getUserFriends($token = "")
	{
		//$FB_ME_URL = 'https://graph.facebook.com/me/friend-list';
	
		//$params = array('access_token' => ( $token != "" ? $token : $this->access_token ));
		
		$params = array(
				'fields' => 'friend-list',
				'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		return $this->getMe($params);
	
		//$url_req = $FB_ME_URL.'?'.urldecode(http_build_query($params));	
		//echo "<br>".$url_req."<br>";
	
		//$userInfo = file_get_contents($url_req);	
		//$userInfo = json_decode(file_get_contents($FB_ME_URL.'?'.urldecode(http_build_query($params))), true);
	
		//return $userInfo;
	}
	*/
}
?>