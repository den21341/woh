<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class PageModelAuth extends PageModel
{
	public $ses;	
	protected $promoseen;
	
	function __construct($config, $db, $LangId, $pname)
	{
		parent::__construct($config, $db, $LangId, $pname);
		
		$this->ses = UhCmsApp::getSesInstance();
		$this->promoseen = $this->checkPromoSeen();		
	}
	
	public function checkNewActivities() {
		$num_msg = ( $this->ses->UserId != 0 ? $this->msg_NumNew($this->ses->UserId) : 0 );
		$num_req = ( $this->ses->UserId != 0 ? $this->req_NumNew($this->ses->UserId) : 0 );
		$num_thingreq = ( $this->ses->UserId != 0 ? $this->req_NumNew($this->ses->UserId, PROJ_THINGS) : 0 );
		
		return Array("msgnum" => $num_msg, "reqnum" => $num_req, "thingreqnum" => $num_thingreq);
	}
	
	public function req_NumNew($uid, $proj_type = PROJ_NEEDHELP) {
		//$status_cond = "";
		//if( $req_status != -10 )
		//$status_cond = " AND r1.req_status='".REQ_STATUS_NEW."' ";
		
		$query = "SELECT avg(r1.req_amount) as avgcollect, sum(r1.req_amount) as totalcollect, count(r1.sender_id) as totalnum 
			FROM ".TABLE_CAT_ITEMS_HELPREQ." r1 
			INNER JOIN ".TABLE_CAT_ITEMS." i1 ON r1.item_id=i1.id AND i1.author_id='$uid' AND i1.profile_id='".$proj_type."'    
			WHERE r1.req_status='".REQ_STATUS_NEW."' AND r1.show = 1 ";
		$res = $this->db->query($query);		
		if( count($res)>0 )
		{
			//return Array("sum" => $res[0]['totalcollect'], "avg" => $res[0]['avgcollect'], "num" => $res[0]['totalnum']);
			return $res[0]['totalnum'];
		}
		
		return 0; //Array("sum" => 0, "avg" => 0, "num" => 0);	
	}
	
	public function msg_NumNew($toid, $projid=0)
	{
		$sql_cond = "";
		if( $projid != 0 )
			$sql_cond = " AND m1.item_id='$projid' ";
	
		$query = "SELECT count(m1.id) as totmsg
			FROM ".TABLE_CAT_MSG." m1
				WHERE m1.to_id='$toid' AND m1.msg_status = 1 $sql_cond";
		//echo $query."<br>";
		$res = $this->db->query($query);
		if( count($res)>0 )
			return $res[0]['totmsg'];
	
		return 0;
	}
	
	public function checkPromoSeen()
	{
		$promo_shown = false;
		
		if( isset($_SESSION['wayofhelpses']) && ($_SESSION['wayofhelpses']=="promoseen") )
		{
			$promo_shown = true;
		}
	
		//setcookie("wayofhelpses","washere", time()+3600);	// Set cookie for 30 mins
		$_SESSION['wayofhelpses'] = ( $promo_shown ? "promoseen" : "promoshow" );
	
		return $promo_shown;
	}
	
	public function isPromoSeen()
	{
		return $this->promoseen;
	}
	
	public function setPromoSeen($seen = true)
	{
		$_SESSION['wayofhelpses'] = "promoseen";
		$this->promoseen = true;
	}
}