<?php

class UhCmsSMS {
    public function __construct() {
        include_once "smsc_api.php";
    }

    public function sendSms($phone, $message) {
        send_sms($phone, $message);
    }

    public function okCode($secret) {
        return hexdec(substr(md5($secret."<секретная строка>"), 7, 5));
    }

}