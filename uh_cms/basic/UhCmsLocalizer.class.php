<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsLocalizer {
	private $LangId;
	private $texts;

	function __construct($LangId = 1) {
		$this->LangId = $LangId;

		include "inc/localizer-inc.php";

		$this->texts = $txts;
		$this->texts_loc = $this->texts[$this->LangId];
	}

	function selectLang($LangId) {

		if( isset($this->texts[$LangId]) ) {
			$this->LangId = $LangId;
			$this->texts_loc = $this->texts[$this->LangId];
			//print_r($this->texts_loc); die();
			return true;
		}

		return false;
	}

	function __get($fname) {
		return (isset($this->texts_loc[$fname]) ? $this->texts_loc[$fname] : "");
	}

	function __isset($fname) {
		return isset($this->texts_loc[$fname]);
	}

	public function get($viewname, $strname) {
		return (isset($this->texts_loc[$viewname."_".$strname]) ? $this->texts_loc[$viewname."_".$strname] : "");
	}
}