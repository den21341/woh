<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsFormData
{
	private $req;
	private $req_ok;
	private $req_msg;
	
	function __construct($req)
	{
		$this->req = $req;
		$this->req_ok = Array();
		$this->req_msg = Array();
		foreach( $req as $k => $v )
		{
			$this->req_ok[$k] = true;
			$this->req_msg[$k] = "";
		}
	}
	
	function __set($fname, $val){
		$this->req[$fname] = $val;
	}
	
	function __get($fname){
		return (isset($this->req[$fname]) ? $this->req[$fname] : "");
	}

	function __isset($fname){
		return isset($this->req[$fname]);
	}
	
	public function quote($val)
	{
		return str_replace("\"", "&quot;", $val);
	}
	
	public function ok($fname)
	{
		if( isset($this->req_ok[$fname]) )
			return $this->req_ok[$fname];
		
		return false;
	}
	
	public function msg($fname)
	{
		if( isset($this->req_msg[$fname]) )
			return $this->req_msg[$fname];
		
		return "";
	}
	
	public function setok($fname, $status = false)
	{
		$this->req_ok[$fname] = $status;
	}
	
	public function setmsg($fname, $msg)
	{
		$this->req_msg[$fname] = $msg;
	}
	
	public function has_errors()
	{
		foreach($this->req_ok as $k => $v)
		{
			if( !$v )
				return true;
		}
		
		return false;
	}
	
	public function get_msglist()
	{
		$res = '';
		
		foreach($this->req_ok as $k => $v)
		{
			if( !$v )
			{
				$res .= ($res != "" ? "<br>" : "").$this->msg($k);
			}
		}
		
		return $res;
	}
}

?>