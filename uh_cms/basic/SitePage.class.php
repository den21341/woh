<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class SitePage
{
	private $data;
	private $db;
	private $pagename;
	private $LangId;
	
	function __construct($db, $LangId, $pagename="")
	{
		$this->db = $db;
		$this->LangId = $LangId;
		$this->pagename = $pagename;
		$this->data = $this->Page_GetInfo($pagename);
	}
	
	public function __set($key, $value)
	{
		$this->data[$name] = $value;
	}
	
	public function __get($key)
	{	
		return $this->data[$key];
	}
	
	public function length()
	{
		return count($this->data);
	}
	
	/*
	private function Page_BuildUrl_PHP($pname="", $ipage="")
	{
		// sample URLS
		//	somepage.php
		//	info.php?page=anotherpage
		$url = "";

		if( $ipage == "" )
		{
			$url = $pname.".php";
		}
		else
		{
			$url = ($pname == "" ? "info" : $pname).".php?page=".$ipage;
		}

		return $url;
	}

	private function Page_BuildUrl_HTML($pname="", $ipage="")
	{
		// sample URLS
		//	somepage.html
		//	info/anotherpage.html
		$url = "";

		if( $ipage == "" )
		{
			$url = $pname.".html";
		}
		else
		{
			$url = ($pname == "" ? "info" : $pname)."/".$ipage.".html";
		}

		return $url;
	}

	//public function Page_BuildUrl($LangId, $pname="", $ipage="")
	public function Page_BuildUrl($pname="", $ipage="")
	{
		return ( UHCMS_LINK_MODE == UH_LINKMODE_PHP ?  WWWHOST.$this->Page_BuildUrl_PHP($this->LangId, $pname, $ipage) : WWWHOST.$this->Page_BuildUrl_HTML($this->LangId, $pname, $ipage) );
	}
	*/
	public function Page_BuildUrl($pname="", $ipage="")
	{				
		$url = UhCmsUtils::Page_BuildUrl($pname, $ipage);
		return $url;
	}

	function Page_GetResource($pid, $disp=-1)
	{
		$sql_cond = "";
		if( $disp != -1 )
		{
			$sql_cond = " AND pr1.display_type='$disp' ";
		}
		
		$query = "SELECT pr1.id as assignid, pr1.display_type, pr1.sort_num, r1.*, r2.content
			FROM ".TABLE_PAGE_RESOURCES." pr1
			INNER JOIN ".TABLE_RESOURCE." r1 ON pr1.item_id=r1.id
			INNER JOIN ".TABLE_RESOURCE_LANGS." r2 ON r1.id=r2.item_id AND r2.lang_id='".$this->LangId."'
			WHERE pr1.page_id='$pid' $sql_cond 
			ORDER BY pr1.display_type, pr1.sort_num";
			
		$pageres = $this->db->query($query);
		return $pageres;
		
		/*
		global $TABLE_RESOURCE, $TABLE_RESOURCE_LANGS, $TABLE_PAGE_RESOURCES;
		$pageres = Array();
		$query = "SELECT pr1.id as assignid, pr1.display_type as disp, pr1.sort_num, r1.*, r2.content
			FROM $TABLE_PAGE_RESOURCES pr1
			INNER JOIN $TABLE_RESOURCE r1 ON pr1.item_id=r1.id
			INNER JOIN $TABLE_RESOURCE_LANGS r2 ON r1.id=r2.item_id AND r2.lang_id='$LangId'
			WHERE pr1.page_id='$pid' AND pr1.display_type='$disp'
			ORDER BY pr1.sort_num";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object( $res ) )
			{
				$ri = Array();
				$ri['id'] = $row->id;
				$ri['assignid'] = $row->assid;
				$ri['disp'] = $row->display_type;
				$ri['sort'] = $row->sort_num;
				$ri['name'] = stripslashes($row->name);
				$ri['title'] = stripslashes($row->title);
				$ri['text'] = stripslashes($row->content);
				$pageres[] = $ri;
			}
			mysql_free_result( $res );
		}

		return $pageres;
		*/
	}

	function Page_GetIdByName($pname)
	{
		//global $TABLE_PAGES;

		$pid = 0;

		$query = "SELECT * FROM ".TABLE_PAGES." WHERE page_name='".addslashes($pname)."'";
		$res = $this->db->query( $query );
		if( count($res) > 0 )
			$pid = $res[0]['id'];
		
		/*
		
		if( $res = mysql_query( $query ) )
		{
			if( $row = mysql_fetch_object( $res ) )
			{
				$pid = $row->id;
			}
			mysql_free_result( $res );
		}
		*/

		return $pid;
	}
	
	function Page_Subpages($parentid)
	{
		$itemcond = "";
	
		$query = "SELECT p1.*, p2.page_mean, DATE_FORMAT(create_date, '%d.%m.%Y') as dd, DATE_FORMAT(create_date, '%H.%i') as tm
			FROM ".TABLE_PAGES." p1
			INNER JOIN ".TABLE_PAGES_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
			WHERE p1.parent_id=".$parentid." $itemcond
				ORDER BY p1.sort_num,p1.id";
	
		$res = $this->db->query( $query );
	
		for( $i=0; $i<count($res); $i++ )
		{
			$row = $res[$i];
				
			$link_file = "";
			//$is_selected = false;
			switch( $row['page_record_type'] )
			{
				case 1:	// Basic script
					$link_file = $this->Page_BuildUrl(stripslashes($row['page_name']),"");
					if( stripslashes($row['page_name']) == "index" )
						$link_file = WWWHOST;
					//if( stripslashes($row['page_name']) == $curpname )
					//	$is_selected = true;
					break;
				case 2:	// Folder
					$link_file = stripslashes($row['page_name']).'/';
					break;
				case 3:	// Info page from database
					$link_file = $this->Page_BuildUrl("info", stripslashes($row['page_name']));
					//if( ($curpname == "info") && ($page == stripslashes($row['page_name'])) )
					//{
					//	$is_selected = true;
					//}
					break;
				case 4:	// Direct link to url
					$link_file = stripslashes($row['page_name']);
					break;
			}
				
			$res[$i]['link'] = $link_file;
			//$res[$i]['selected'] = $is_selected;
			$res[$i]['name'] = stripslashes($row['page_mean']);
			$res[$i]['pname'] = stripslashes($row['page_name']);
		}
	
		return $res;		
	}

	public function Menu_GetLevel($parentid, $menutype, $curpname)
	{
		//global $TABLE_PAGES, $TABLE_PAGES_LANGS;
		//global $page;

		$mitems = Array();

		$itemcond = "";
		if ( $menutype != null )
			$itemcond = " AND p1.show_in_menu=".$menutype." ";

		$query = "SELECT p1.*, p2.page_mean FROM ".TABLE_PAGES." p1
			INNER JOIN ".TABLE_PAGES_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
			WHERE p1.parent_id=".$parentid." $itemcond
			ORDER BY p1.sort_num,p1.id";
			
		$res = $this->db->query( $query );
		for( $i=0; $i<count($res); $i++ )
		{
			$row = $res[$i];
			
			$link_file = "";
			$is_selected = false;
			
			switch( $row['page_record_type'] )
			{
				case 1:	// Basic script
					$link_file = $this->Page_BuildUrl(stripslashes($row['page_name']),"");
					if( stripslashes($row['page_name']) == "index" )
						$link_file = WWWHOST;
					if( stripslashes($row['page_name']) == $curpname )
						$is_selected = true;
					break;
				case 2:	// Folder
					$link_file = stripslashes($row['page_name']).'/';
					break;
				case 3:	// Info page from database
					$link_file = $this->Page_BuildUrl("info", stripslashes($row['page_name']));
					if( ($curpname == "info") && ($page == stripslashes($row['page_name'])) )
					{
						$is_selected = true;
					}
					break;
				case 4:	// Direct link to url
					$link_file = stripslashes($row['page_name']);
					break;
			}		

			$res[$i]['link'] = $link_file;
			$res[$i]['selected'] = $is_selected;
			$res[$i]['name'] = stripslashes($row['page_mean']);
			$res[$i]['pname'] = stripslashes($row['page_name']);
		}
		
		return $res;		
	}

	public function Page_GetPath($iid)
	{
		//global $WWWHOST;
		//global $TABLE_PAGES, $TABLE_PAGES_LANGS;

		$pits = Array();

		$pageid = $iid;

		if( $pageid != 0 )
		{
			do
			{
				$found = false;
				$query1 = "SELECT c1.*, c2.page_mean as name
					FROM ".TABLE_PAGES." c1, ".TABLE_PAGES_LANGS." c2
					WHERE c1.id='$pageid' AND c1.id=c2.item_id AND c2.lang_id='$this->LangId'";
				$its = $this->db->query($query);
				
				//if( $res1 = mysql_query( $query1 ) )
				//{
				//	if( $row1 = mysql_fetch_object( $res1 ) )
				//	{
				for( $i=0; $i<count($its); $i++ )
				{
					$row1 = $its[$i];
					
						$si = Array();
						$si['id'] = $row1['id'];
						$si['name'] = stripslashes($row1['name']);

						$link_file = "";
						switch( $row1->page_record_type )
						{
							case 1:
								$link_file = $this->Page_BuildUrl(stripslashes($row1['page_name']),"");
								if( stripslashes($row1['page_name']) == "index" )
									$link_file = WWWHOST;
								break;
							case 2:
								$link_file = stripslashes($row1['page_name']).'/';
								break;
							case 3:
								$link_file = $this->Page_BuildUrl("info", stripslashes($row1['page_name']));
								break;
							case 4:
								$link_file = stripslashes($row1['page_name']);
								break;
						}

						$si['url'] = $link_file;


						$pits[] = $si;

						$pageid = $row1->parent_id;
						$found = true;
				}
					//}
					//mysql_free_result( $res1 );
				//}
				//else
				//	echo mysql_error();

				if( !$found )
					break;
			}
			while( $pageid != 0 );
		}

		return $pits;
	}

	function Page_VideoNum($page_id)
	{
		$num = 0;
		
		$query = "SELECT count(*) as totvid FROM ".TABLE_PAGE_VIDEO." WHERE item_id=".$page_id." ";
		$res = $this->db->query( $query );
		if( isset($res[0]) )
			$num = $res[0]['totvid'];
		
		/*
		global $TABLE_PAGE_VIDEO;

		$num = 0;

		$query1 = "SELECT count(*) as totvid FROM $TABLE_PAGE_VIDEO WHERE item_id=".$page_id." ";
		if( $res1 = mysql_query( $query1 ) )
		{
			while( $row1 = mysql_fetch_object( $res1 ) )
			{
				$num = $row1->totvid;
			}
			mysql_free_result( $res1 );
		}
		*/

		return $num;
	}

	function Page_Video($page_id)
	{
		$query = "SELECT p1.*, p1.src_w as clip_w, p1.src_h as clip_h, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM ".TABLE_PAGE_VIDEO." p1
			INNER JOIN ".TABLE_PAGE_VIDEO_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
			WHERE p1.item_id=".$page_id."
			ORDER BY p1.sort_num,p1.add_date";
		$videos = $this->db->query( $query );
		
		return $videos;
		/*
		global $TABLE_PAGE_VIDEO, $TABLE_PAGE_VIDEO_LANGS;

		$videos = Array();
		$query1 = "SELECT p1.*, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM $TABLE_PAGE_VIDEO p1
			INNER JOIN $TABLE_PAGE_VIDEO_LANGS p2 ON p1.id=p2.item_id AND p2.lang_id='$LangId'
			WHERE p1.item_id=".$page_id."
			ORDER BY p1.sort_num,p1.add_date";

		if( $res1 = mysql_query( $query1 ) )
		{
			while( $row1 = mysql_fetch_object( $res1 ) )
			{
				$pi = Array();
				$pi['id'] = $row1->id;
				$pi['alt'] = stripslashes($row1->title);
				$pi['descr'] = stripslashes($row1->descr);
				$pi['snum'] = $row1->sort_num;
				$pi['clip'] = stripslashes($row1->filename);
				$pi['clip_w'] = $row1->src_w;
				$pi['clip_h'] = $row1->src_h;
				$pi['ico'] = stripslashes($row1->filename_ico);
				$pi['ico_w'] = $row1->ico_w;
				$pi['ico_h'] = $row1->ico_h;
				$pi['tubecode'] = stripslashes($row1->tube_code);
				$pi['date'] = sprintf("%02d.%02d.%04d", $row1->dd, $row1->dm, $row1->dy);

				$videos[] = $pi;
			}
			mysql_free_result( $res1 );
		}

		return $videos;
		*/
	}
	
	function Page_VideoInfo($video_id)
	{
		$query = "SELECT p1.*, p1.src_w as clip_w, p1.src_h as clip_h, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM ".TABLE_PAGE_VIDEO." p1
			INNER JOIN ".TABLE_PAGE_VIDEO_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
			WHERE p1.id=".$video_id." ";
		$videos = $this->db->query( $query );
	
		return $videos;
		/*
			global $TABLE_PAGE_VIDEO, $TABLE_PAGE_VIDEO_LANGS;
	
			$videos = Array();
			$query1 = "SELECT p1.*, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM $TABLE_PAGE_VIDEO p1
			INNER JOIN $TABLE_PAGE_VIDEO_LANGS p2 ON p1.id=p2.item_id AND p2.lang_id='$LangId'
			WHERE p1.item_id=".$page_id."
			ORDER BY p1.sort_num,p1.add_date";
	
			if( $res1 = mysql_query( $query1 ) )
			{
			while( $row1 = mysql_fetch_object( $res1 ) )
			{
			$pi = Array();
			$pi['id'] = $row1->id;
			$pi['alt'] = stripslashes($row1->title);
			$pi['descr'] = stripslashes($row1->descr);
			$pi['snum'] = $row1->sort_num;
			$pi['clip'] = stripslashes($row1->filename);
			$pi['clip_w'] = $row1->src_w;
			$pi['clip_h'] = $row1->src_h;
			$pi['ico'] = stripslashes($row1->filename_ico);
			$pi['ico_w'] = $row1->ico_w;
			$pi['ico_h'] = $row1->ico_h;
			$pi['tubecode'] = stripslashes($row1->tube_code);
			$pi['date'] = sprintf("%02d.%02d.%04d", $row1->dd, $row1->dm, $row1->dy);
	
			$videos[] = $pi;
			}
			mysql_free_result( $res1 );
			}
	
			return $videos;
			*/
	}

	function Page_PhotoNum($page_id)
	{
		$num = 0;
		
		$query = "SELECT count(*) as totpic FROM ".TABLE_PAGE_PHOTO." WHERE item_id=".$page_id." ";
		$res = $this->db->query( $query );
		if( isset($res[0]) )
			$num = $res[0]['totpic'];
		
		return $num;
		/*
		global $TABLE_PAGE_PHOTO;

		$num = 0;

		$query1 = "SELECT count(*) as totpic FROM $TABLE_PAGE_PHOTO WHERE item_id=".$page_id." ";
		if( $res1 = mysql_query( $query1 ) )
		{
			while( $row1 = mysql_fetch_object( $res1 ) )
			{
				$num = $row1->totpic;
			}
			mysql_free_result( $res1 );
		}

		return $num;
		*/
	}

	function Page_Photo($page_id)
	{
		$query = "SELECT p1.*, p1.src_w as pic_w, p1.src_h as pic_h, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM ".TABLE_PAGE_PHOTO." p1
			INNER JOIN ".TABLE_PAGE_PHOTO_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
			WHERE p1.item_id=".$page_id."
			ORDER BY p1.sort_num,p1.add_date";
			
		$photos = $this->db->query($query);
		for( $i=0; $i<count($photos); $i++ )
		{
			if( $photos[$i]['filename'] != "" )
				$photos[$i]['pic'] = WWWHOST.stripslashes($photos[$i]['filename']);
			
			if( $photos[$i]['filename_thumb'] != "" )
				$photos[$i]['thumb'] = WWWHOST.stripslashes($photos[$i]['filename_thumb']);
			
			if( $photos[$i]['filename_ico'] != "" )
				$photos[$i]['ico'] = WWWHOST.stripslashes($photos[$i]['filename_ico']);
			
			$photos[$i]['date'] = sprintf("%02d.%02d.%04d", $photos[$i]['dd'], $photos[$i]['dm'], $photos[$i]['dy']);
		}

		return $photos;
		/*
		global $TABLE_PAGE_PHOTO, $TABLE_PAGE_PHOTO_LANGS;

		$photos = Array();
		$query1 = "SELECT p1.*, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM $TABLE_PAGE_PHOTO p1
			INNER JOIN $TABLE_PAGE_PHOTO_LANGS p2 ON p1.id=p2.item_id AND p2.lang_id='$LangId'
			WHERE p1.item_id=".$page_id."
			ORDER BY p1.sort_num,p1.add_date";
		if( $res1 = mysql_query( $query1 ) )
		{
			while( $row1 = mysql_fetch_object( $res1 ) )
			{
				$pi = Array();
				$pi['id'] = $row1->id;
				$pi['alt'] = stripslashes($row1->title);
				$pi['snum'] = $row1->sort_num;
				$pi['pic'] = $WWWHOST.stripslashes($row1->filename);
				$pi['pic_w'] = $row1->src_w;
				$pi['pic_h'] = $row1->src_h;
				$pi['thumb'] = $WWWHOST.stripslashes($row1->filename_thumb);
				$pi['thumb_w'] = $row1->thumb_w;
				$pi['thumb_h'] = $row1->thumb_h;
				$pi['ico'] = $WWWHOST.stripslashes($row1->filename_ico);
				$pi['ico_w'] = $row1->ico_w;
				$pi['ico_h'] = $row1->ico_h;
				$pi['date'] = sprintf("%02d.%02d.%04d", $row1->dd, $row1->dm, $row1->dy);

				$photos[] = $pi;
			}
			mysql_free_result( $res1 );
		}

		return $photos;
		*/
	}
	
	function Page_PhotoInfo($photo_id)
	{
		$query = "SELECT p1.*, p1.src_w as pic_w, p1.src_h as pic_h, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM ".TABLE_PAGE_PHOTO." p1
			INNER JOIN ".TABLE_PAGE_PHOTO_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
			WHERE p1.id=".$photo_id." ";
			
		$photos = $this->db->query($query);
		for( $i=0; $i<count($photos); $i++ )
		{
			if( $photos[$i]['filename'] != "" )
				$photos[$i]['pic'] = WWWHOST.stripslashes($photos[$i]['filename']);
				
			if( $photos[$i]['filename_thumb'] != "" )
				$photos[$i]['thumb'] = WWWHOST.stripslashes($photos[$i]['filename_thumb']);
				
			if( $photos[$i]['filename_ico'] != "" )
				$photos[$i]['ico'] = WWWHOST.stripslashes($photos[$i]['filename_ico']);
				
			$photos[$i]['date'] = sprintf("%02d.%02d.%04d", $photos[$i]['dd'], $photos[$i]['dm'], $photos[$i]['dy']);
		}
	
		return $photos;		
	}

	private function _Page_ScanInfo($sql_query)
	{
		//echo $sql_query."<br>";
		$res = $this->db->query( $sql_query );		
		$page1 = ( isset($res[0]) ? $res[0] : $res );
		if( isset($page1['id']) )
		{
			$page1['pmean'] = stripslashes($page1['pmean']);
			$page1['pfile'] = stripslashes($page1['page_name']);			
			$page1['pagename'] = $page1['pfile'];
			
			$page1['title'] = stripslashes($page1['title']);
			$page1['header'] = stripslashes($page1['header']);
			$page1['content'] = stripslashes($page1['content']);
			
			$page1['seo_title'] = stripslashes($page1['page_title']);
			$page1['seo_keyw'] = stripslashes($page1['page_keywords']);
			$page1['seo_descr'] = stripslashes($page1['page_descr']);
			
			$page1['pics'] = $this->Page_Photo($page1['id']);
			$page1['clips'] = $this->Page_Video($page1['id']);
		}
		
		return $page1;
	}
	
	public function Page_GetInfoById($pageid)
	{
		$query = "SELECT p1.*, p1.parent_id as pid, p2.page_mean as pmean, p2.page_title, p2.page_keywords, p2.page_descr, p2.title, p2.header, p2.content
			FROM ".TABLE_PAGES." p1
			INNER JOIN ".TABLE_PAGES_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
			WHERE p1.id='".$pageid."'";
			
		$page1 = $this->_Page_ScanInfo($query);
		
		return $page1;
	}

	public function Page_GetInfo($pagename)
	{
		//global $TABLE_PAGES, $TABLE_PAGES_LANGS;
		
		$query = "SELECT p1.*, p1.parent_id as pid, p2.page_mean as pmean, p2.page_title, p2.page_keywords, p2.page_descr, p2.title, p2.header, p2.content
			FROM ".TABLE_PAGES." p1
			INNER JOIN ".TABLE_PAGES_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
			WHERE p1.page_name='".$pagename."'";

		//echo $query; die();
		
		$page1 = $this->_Page_ScanInfo($query);
		
		return $page1;
		
		/*

		$page1 = Array();

		if( $result = mysql_query("SELECT p1.*, p2.page_mean, p2.page_title, p2.page_keywords, p2.page_descr,
				p2.title, p2.header, p2.content
				FROM ".TABLE_PAGES." p1
				INNER JOIN ".TABLE_PAGES_LANGS." p2 ON p1.id=p2.item_id AND p2.lang_id='".$this->LangId."'
				WHERE p1.page_name='".$pagename."'") )
		{
			if( $row = mysql_fetch_object($result) )
			{
				$page1['id'] = $row->id;
				$page1['pid'] = $row->parent_id;
				$page1['pfile'] = stripslashes($row->page_name);
				$page1['pmean'] = stripslashes($row->page_mean);
				$page1['seo_title'] = stripslashes($row->page_title);
				$page1['seo_keywords'] = stripslashes($row->page_keywords);
				$page1['seo_descr'] = stripslashes($row->page_descr);
				$page1['pcreated'] = $row->create_date;
				$page1['pmodified'] = $row->modify_date;
				$page1['pagename'] = stripslashes($row->page_name);
				$page1['title'] = stripslashes($row->title);
				$page1['header'] = stripslashes($row->header);
				$page1['content'] = stripslashes($row->content);
				$page1['show'] = $row->show_in_menu;
				$page1['type'] = $row->page_record_type;
				$page1['sort'] = $row->sort_num;

				$page1['pics'] = Page_Video($LangId, $row->id);
				$page1['clips'] = Page_Video($LangId, $row->id);
			}
			mysql_free_result( $result );
		}

		return $page1;
		*/
	}

	
}
