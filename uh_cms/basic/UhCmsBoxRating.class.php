<?php

class UhCmsBoxRating {

    protected $boxRating;
    protected $boxAssocStatus;
    protected $boxAdaptStatus;

    public function __construct() {

        include 'inc/boxRating.php';

        $this->boxRating = $boxStatus;
        $this->boxAssocStatus = $boxAssoc;
        $this->boxAdaptStatus = $boxAdapt;
    }

    public function setBox($status, $rating) {
        $this->boxRating[$status] = $rating;
    }

    public function getBox($status) {
        return (isset($this->boxRating[$status]) ? $this->boxRating[$status] : '');
    }

    public function getStatus($id) {
        return (isset($this->boxAssocStatus[$id]) ? $this->boxAssocStatus[$id] : '');
    }
    public function getBoxStatus($id) {
        return (isset($this->boxRating[$id]) ? $this->boxRating[$id] : '');
    }

    public function adaptCoin($val) {
        if($val == 11 || $val == 12 || $val == 13 || $val == 14)
            return ' ангелитов';
        if(isset($this->boxAdaptStatus[substr($val, -1)]))
            return ' ' . $this->boxAdaptStatus[substr($val, -1)];
        else
            return ' ангелитов';
    }
}