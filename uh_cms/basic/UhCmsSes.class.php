<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsSes{
	protected	$ses_params;
	protected	$session_id;
	protected	$db;
	
	function __construct($db)
	{
		$this->ses_params = Array();
		$this->ses_params['UserId'] = 0;
		$this->ses_params['UserGroup'] = 0;
		$this->ses_params['UserLogin'] = 0;
		$this->ses_params['UserName'] = 0;
		//$this->ses_params['UserDiscountLevel'] = 0;		
		$this->ses_params['SESID'] = "";
		
		//$this->UserId = 0;
		//$this->UserGroup = 0;
		//$this->UserLogin = "";
		//$this->UserName = "";
		//$this->UserDiscountLevel = 0;
		$this->db = $db;
		$this->session_id = "";
		
		//$this->startSession(SESSION_PATH);
	}	
	
	public function startSession($sespath){
		@session_save_path( $sespath );
		@session_name("uhcms");
		$this->session_id = @session_id();
		if( $this->session_id == "" )
		{
			@session_start();
			$this->session_id = @session_id();
		}
		
		$this->ses_params['SESID'] = $this->session_id;
	}
	
	function __set($pname, $val){
		$this->ses_params[$pname] = $val;
	}
	
	function __get($pname){
		return (isset($this->ses_params[$pname]) ? $this->ses_params[$pname] : 0);
	}

	function __isset($pname){
		return isset($this->ses_params[$pname]);
	}
	
	public function sesid(){
		return $this->session_id;
	}
}
?>