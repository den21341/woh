<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsFaqs{
	protected	$db;
	protected	$LangId;
	//protected	$newsGroups;
	
	function __construct($db, $LangId)
	{		
		$this->db = $db;
		$this->LangId = $LangId;
		
		//$this->newsGroups = Array(0 => "Новости", 1 => "Статьи", 2 => "Обзоры");
	}	
		
	public function Faqs_GroupList($sortby="")
	{
		$cond = "";
		//if( $group_id > 0 )
		//	$cond = " WHERE g1.id='$group_id' ";
		
		$sort_cond = " g1.sort_num ";
		switch($sort_cond)
		{
			case "name":
				$sort_cond = " g2.name ";
				break;
		}
		
		$query = "SELECT g1.*, g2.name, g2.descr FROM ".TABLE_FAQ_GROUP." g1
			INNER JOIN ".TABLE_FAQ_GROUP_LANGS." g2 ON g1.id=g2.group_id AND g2.lang_id='".$this->LangId."' 
			ORDER BY $sort_cond";
		$its = $this->db->query( $query );
		if( count($its) > 0 )
		{
			return $its;
		}
		
		return $its;
	}
	
	public function Faqs_GroupInfo($group_id)
	{
		$query = "SELECT g1.*, g2.name, g2.descr FROM ".TABLE_FAQ_GROUP." g1
			INNER JOIN ".TABLE_FAQ_GROUP_LANGS." g2 ON g1.id=g2.group_id AND g2.lang_id='".$this->LangId."'
			WHERE g1.id='".addslashes($group_id)."'";
		$its = $this->db->query( $query );
		if( count($its) > 0 )
		{
			return $its[0];
		}		
		
		return Array("id"=>0);
	}
	
	
	public function Faqs_ItemsNum($group_id=0)
	{
		$tot = 0;

		$cond = "";
		if( $group_id > 0 )
		{
			$cond = " WHERE group_id='".$group_id."' ";
		}

		$query = "SELECT count(*) as totfaqs FROM ".TABLE_FAQ." $cond";
		$its = $this->db->query( $query );
		if( count($its) > 0 )
		{
			$tot = $its[0]['totfaqs'];
		}

		return $tot;
	}

	public function Faqs_Items($group_id=0, $sortby="", $anons_len=300, $pi=-1, $pn=20)
	{
		$sort_cond = "m1.sort_num";
		switch( $sortby )
		{
			case "name":
				$sort_cond = "m2.title";
				break;
		}

		$sel_cond = "";
		if( $group_id != 0 )
			$sel_cond = " WHERE m1.group_id='".addslashes($group_id)."' ";
		
		$limit_cond = "";
		if( $pi != -1 )
		{
			$limit_cond = " LIMIT ".($pi*$pn).",$pn";
		}

		$query = "SELECT m1.*, m2.title, m2.content 
				FROM ".TABLE_FAQ." m1 
				INNER JOIN ".TABLE_FAQ_LANGS." m2 ON m1.id=m2.item_id AND m2.lang_id='".$this->LangId."' 
				$sel_cond 
				ORDER BY $sort_cond
				$limit_cond";
		$its = $this->db->query( $query );
		
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			$its[$i]['title'] = stripslashes($row['title']);
			$its[$i]['content'] = stripslashes($row['content']);
			//$its[$i]['date'] = sprintf("%02d.%02d.%04d", $row['day'], $row['month'], $row['year']);
			//$its[$i]['img_src'] = ( stripslashes($row['filename_src']) != "" ? WWWHOST.FILE_DIR.stripslashes($row['filename_src']) : "" );
			$its[$i]['short'] = strip_tags($row['content']);
			if( mb_strlen( $its[$i]['short'], UHCMS_DEF_ENC ) > $anons_len )
				$its[$i]['short'] = mb_substr($its[$i]['short'], 0, $anons_len, UHCMS_DEF_ENC)."...";
		}
		
		return $its;		
	}

	public function Faqs_ItemInfo($id)
	{
		$query = "SELECT m1.*, m2.title, m2.content 
			FROM ".TABLE_FAQ." m1 
			INNER JOIN ".TABLE_FAQ_LANGS." m2 ON m1.id=m2.item_id AND m2.lang_id='".$this->LangId."' 
			WHERE m1.id='$id'";
		$its = $this->db->query( $query );
		
		if( count($its) > 0 )
		{
			$row = $its[0];
			$its[0]['title'] = stripslashes($row['title']);
			$its[0]['content'] = stripslashes($row['content']);
			//$its[0]['date'] = sprintf("%02d.%02d.%04d", $row['day'], $row['month'], $row['year']);
			//$its[0]['img_src'] = ( stripslashes($row['filename_src']) != "" ? WWWHOST.FILE_DIR.stripslashes($row['filename_src']) : "" );
		}
		
		return ( count($its) > 0 ? $its[0] : $its );		
	}
	
	public function Faqs_ItemsAll($sortby="", $anons_len=300, $pi=-1, $pn=20)
	{
		$sort_cond = "g1.sort_num, g2.name, m1.sort_num, m2.title";
		switch( $sortby )
		{
			case "name":
				$sort_cond = "m2.title";
				break;
		}
	
		$sel_cond = "";
		//if( $group_id != 0 )
		//	$sel_cond = " WHERE m1.group_id='".addslashes($group_id)."' ";
	
		$limit_cond = "";
		if( $pi != -1 )
		{
			$limit_cond = " LIMIT ".($pi*$pn).",$pn";
		}
	
		$query = "SELECT m1.*, m2.title, m2.content, g1.sort_num as grsort, g2.name as grname, g2.descr 
				FROM ".TABLE_FAQ." m1
				INNER JOIN ".TABLE_FAQ_LANGS." m2 ON m1.id=m2.item_id AND m2.lang_id='".$this->LangId."' 
				INNER JOIN ".TABLE_FAQ_GROUP." g1 ON m1.group_id=g1.id 
				INNER JOIN ".TABLE_FAQ_GROUP_LANGS." g2 ON g1.id=g2.group_id AND g2.lang_id='".$this->LangId."' 
				$sel_cond
				ORDER BY $sort_cond
				$limit_cond";
		
		$its = $this->db->query( $query );
	
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			$its[$i]['grname'] = stripslashes($row['grname']);
			$its[$i]['descr'] = stripslashes($row['descr']);
				
			$its[$i]['title'] = stripslashes($row['title']);			
			$its[$i]['content'] = stripslashes($row['content']);
			//$its[$i]['date'] = sprintf("%02d.%02d.%04d", $row['day'], $row['month'], $row['year']);
			//$its[$i]['img_src'] = ( stripslashes($row['filename_src']) != "" ? WWWHOST.FILE_DIR.stripslashes($row['filename_src']) : "" );
			$its[$i]['short'] = strip_tags($row['content']);
			if( mb_strlen( $its[$i]['short'], UHCMS_DEF_ENC ) > $anons_len )
				$its[$i]['short'] = mb_substr($its[$i]['short'], 0, $anons_len, UHCMS_DEF_ENC)."...";
		}		
	
		return $its;
	}
	
}
?>