<?php

class UhCmsGEO {
    public function __construct() {
        require_once('geo/SxGeo.php');
    }

    public function getUserLocation() {
        $ip = $this->getUserIP();
        $SxGeo = new SxGeo('geo/SxGeoCity.dat', SXGEO_BATCH | SXGEO_MEMORY);
        $region = $SxGeo->getCityFull($ip);

        return $region;
    }

    public function getUserIP() {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = '';
        return $ipaddress;
    }

    public function locationStrCut($name) {
        return($strpos=mb_strpos($name,' '))!==false?mb_substr($name,0,$strpos,'utf8'):$name;
    }

}