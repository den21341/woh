<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class AdmPageView extends View{
	
	protected $tpl_folder;
	protected $pageModel;
	protected $breadcrumbs;
	protected $PAGE_HEADER;
	protected $data;	
	
	function __construct($config, $pModel)
	{
		parent::__construct($config);
		
		$this->tpl_folder = "templates/";
		
		$this->breadcrumbs = Array();
		$this->data = Array();
		
		$this->PAGE_HEADER = Array();
		$this->PAGE_HEADER['ru'] = "Добро пожаловать";
		$this->PAGE_HEADER['en'] = "Welcome";
		
		$this->error_msg = "";
		
		$this->pageModel = $pModel;
	}	
	
	public function __set($pname, $val){
		$this->data[$pname] = $val;
	}
	
	public function __get($pname){
		return (isset($this->data[$pname]) ? $this->data[$pname] : 0);
	}

	public function __isset($pname){
		return isset($this->data[$pname]);
	}
		
	public function renderHeader()
	{		
		include $this->tpl_folder."header-inc.php";
	}
	
	public function renderFooter()
	{
		include $this->tpl_folder."footer-inc.php";
	}

	public function renderHeaderPopup()
	{
		include $this->tpl_folder."popup-header-inc.php";
	}
	
	public function renderFooterPopup()
	{
		include $this->tpl_folder."popup-footer-inc.php";
	}
	
	public function renderPage($page_tpl_file, $popup_mode = 0)
	{
		// Local variable for fast access to PageModel
		if( $popup_mode )
			$this->renderHeaderPopup();
		else		
			$this->renderHeader();
		
		include $this->tpl_folder.$page_tpl_file;
		
		if( $popup_mode )
			$this->renderFooterPopup();
		else
			$this->renderFooter();
	}	
	
	public function render404()
	{
		$this->renderPage("404.php");
	}
	
	public function page_BuildUrl($contr_name, $action_name="", $other="")
	{
		$url = "";
		if( $contr_name == "" )
		{
			//;
		}
		else
		{
			$url = $contr_name."/";
		}
		
		if( $action_name != "" )
			$url .= $action_name."/";
		
		return WWWHOST."admin/".$url.($other != "" ? "?".$other : "");
	}
	
	public function page_BreadcrumbsAdd($url, $title)
	{
		$this->breadcrumbs[] = Array("url" => $url, "name" => $title);
	}
}
?>