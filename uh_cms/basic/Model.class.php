<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

abstract class Model{
	protected $cfg;
	protected $db;
	protected $lasterr;
	
	function __construct($config, $db)
	{
		$this->resetLastErr();
		
		$this->cfg = $config;		
		$this->db = $db;
	}

	public function getCfg(){
		return $this->cfg;
	}
	
	public function getLastErr(){
		return $this->lasterr;
	}
	
	protected function resetLastErr(){
		$this->lasterr = 0;
	}
	
	protected function setLastErr($code){
		$this->lasterr = $code;
	}
}
?>