<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

abstract class Controller{
	protected $cfg;
	protected $db;
	protected $self_contolled;
	protected $urlparts;
	
	function __construct($config, $db)
	{
		$this->cfg = $config;		
		$this->db = $db;
		$this->self_contolled = false;
		$this->urlparts = Array();
	}	
	
	public function putUrls($urlarr){
		$this->urlparts = $urlarr;
	}
	
	public function getDb(){
		return $this->db;
	}
	
	public function getCfg(){
		return $this->cfg;
	}
	
	public function selfControlled(){
		return $this->self_contolled;
	}
	
	protected function showLog($str){
		if( isset($this->cfg['DEBUG_ON']) && $this->cfg['DEBUG_ON'] ){
			echo "Controller log: ".$str."<br>";
		}
	}
	
	public function getReqParam($pname, $pdef="", $replace_quotes = false, $injectclear = false)
	{
		$ret = $pdef;
		if( isset($_GET[$pname]) )
			$ret = $_GET[$pname];
		else if( isset($_POST[$pname]) )
			$ret = $_POST[$pname];
		
		if( $replace_quotes )
		{
			if( !is_array($ret) && ($ret != "") )
				$ret = str_replace ("\"", "&quot;", $ret);
		}
		
		if( $injectclear )
		{
			if( !is_array($ret) && ($ret != "") )
				$ret = UhCmsUtils::ClearSQLInject($ret);
		}
		
		return $ret;
	}
	
	public function goToUrl($url)
	{
		header("Location: ".$url);
		exit();
	}
	
	public function handle_action($urlparts){
		// by default do nothing, should be overidden in child controllers
	}
	
	abstract public function action_default();
}
?>