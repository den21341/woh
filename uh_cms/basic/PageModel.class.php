<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Class for storing basic contact information
class SiteContacts
{
	private $data;
	private $db;
	
	public function __construct($db, $LangId)
	{
		$this->db = $db;
		$this->data = $this->Contacts_Get($LangId);
	}
	
	public function __set($key, $value)
	{
		$this->data[$name] = $value;
	}
	
	public function __get($key)
	{	
		return $this->data[$name];
	}
	
	// Get website contact information
	private function Contacts_Get($LangId)
	{
		$continfo = Array();
		
		$query = "SELECT * FROM ".TABLE_SITE_OPTIONS." WHERE lang_id=".$LangId." ORDER BY id";
		$contdat = $this->db->query( $query );
		foreach( $contdat as $row )
		{
			if( ($row['id'] % 10) == 1 )		$continfo['infomail'] = stripslashes($row['value']);
			else if( ($row['id'] % 10) == 2 )	$continfo['supmail'] = stripslashes($row['value']);
			else if( ($row['id'] % 10) == 3 )	$continfo['phone1'] = stripslashes($row['value']);
			else if( ($row['id'] % 10) == 4 )	$continfo['phone2'] = stripslashes($row['value']);
			else if( ($row['id'] % 10) == 5 )	$continfo['phone3'] = stripslashes($row['value']);
			else if( ($row['id'] % 10) == 6 )	$continfo['phone4'] = stripslashes($row['value']);
			else if( ($row['id'] % 10) == 7 )	$continfo['address'] = stripslashes($row['value']);
			else if( ($row['id'] % 10) == 8 )	$continfo['skype'] = stripslashes($row['value']);
			else if( ($row['id'] % 10) == 9 )	$continfo['icq'] = stripslashes($row['value']);
		}
		
		unset($contdat);
		
		return $continfo;
	}
}

////////////////////////////////////////////////////////////////////////
// Class for storing text resources 
class SiteResources
{
	private $data;
	private $db;
	private $LangId;
	
	public function __construct($db, $LangId)
	{
		$this->LangId = $LangId;
		$this->db = $db;
		$this->data = $this->Resources_Get();
	}
	
	public function __set($key, $value)
	{
		$this->data[$key] = $value;
	}
	
	public function __get($key)
	{	
		return $this->data[$key];
	}
	
	public function Resources_List()
	{
		return $this->data;
	}
	
	// Extract text resources for website
	//public function Resources_Get($langid)
	private function Resources_Get()
	{
		$txt_res = Array();

		// Now we should extract all text resources to display on page
		$query = "SELECT r1.*, r2.content as text FROM ".TABLE_RESOURCE." r1
			INNER JOIN ".TABLE_RESOURCE_LANGS." r2 ON r1.id=r2.item_id AND r2.lang_id='".$this->LangId."'";
		
		$txt_res0 = $this->db->query( $query );
		foreach($txt_res0 as $row)
		{
			$txt_res[stripslashes($row['name'])] = $row;
		}
		
		/*
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object( $res ) )
			{
				$ri = Array();
				$ri['id'] = $row->id;
				$ri['name'] = stripslashes($row->name);
				$ri['title'] = stripslashes($row->title);
				$ri['text'] = stripslashes($row->content);
				$txt_res[$row->name] = $ri;
			}
			mysql_free_result( $res );
		}
		*/

		return $txt_res;
	}	
	
	/*
	public function Resources_ForPageList($pageid)
	{
		// Now we should extract all text resources assigned to page
		$query = "SELECT pr1.id as assignid, pr1.display_type, pr1.sort_num, r1.*, r2.content
			FROM ".TABLE_PAGE_RESOURCES." pr1
			INNER JOIN ".TABLE_RESOURCE." r1 ON pr1.item_id=r1.id
			INNER JOIN ".TABLE_RESOURCE_LANGS." r2 ON r1.id=r2.item_id AND r2.lang_id='".$this->LangId."'
			WHERE pr1.page_id='$pageid'
			ORDER BY pr1.display_type,pr1.sort_num";
		
		$res = $this->db->query( $query );
		
		return $res;
	}
	*/
}

///////////////////////////////////////////////////////////////////////////
// Class for storing Splash slides on the start page
class SiteSplashSlides
{
	private $data;
	private $db;
	
	public function __construct($db, $LangId)
	{
		$this->db = $db;
		$this->data = $this->Slides_Get($LangId);
	}
	
	public function __set($key, $value)
	{
		$this->data[$name] = $value;
	}
	
	public function __get($key)
	{	
		return $this->data[$name];
	}
	
	public function length()
	{
		return count($this->data);
	}
	
	// Get slides for splash banner
	//public function Slides_Get($LangId)
	private function Slides_Get($LangId)
	{
		$slides = $this->db->query("SELECT * FROM ".TABLE_SLIDES." ORDER BY sort_num");
		for( $i=0; $i<count($slides); $i++ )
		{
			if( $slides[$i]['filename'] != "" )
				$slides[$i]['filename'] = WWWHOST.FILE_DIR.$slides[$i]['filename'];
		}

		/*
		$slides = Array();
		if( $res = mysql_query("SELECT * FROM $TABLE_SLIDES ORDER BY sort_num") )
		{
			while( $row = mysql_fetch_object($res) )
			{
				$item['url'] = stripslashes($row->url);
				$item['filename'] = $WWWHOST.$FILE_DIR.stripslashes($row->filename);
				$item['alt'] = stripslashes($row->title);
				$item['comment'] = stripslashes($row->comment);

				$slides[] = $item;
			}
			mysql_free_result($res);
		}
		*/

		return $slides;
	}	
}

////////////////////////////////////////////////////////////////////////////////////////////
// Basic Page Model
class PageModel extends Model{
	protected $LangId;
	protected $continfo;
	protected $txt_res;
	protected $slides;
	protected $pagename;
	protected $page;
	
	function __construct($config, $db, $LangId, $pagename="")
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->pagename = $pagename;
		
		$this->loadCmsData();
	}
	
	public function getLangId()
	{
		return $this->LangId;
	}
	
	public function getPageName()
	{
		return $this->pagename;
	}
	
	public function get_page()
	{
		return $this->page;
	}
	
	public function get_continfo()
	{
		return $this->continfo;
	}
	
	public function get_txtres()
	{
		return $this->txt_res;
	}
	
	public function get_slides()
	{
		return $this->slides;
	}
	
	public function loadCmsData()
	{	
		$this->continfo = new SiteContacts($this->db, $this->LangId);
		$this->txt_res = new SiteResources($this->db, $this->LangId);
		$this->slides = new SiteSplashSlides($this->db, $this->LangId);
		$this->page = new SitePage($this->db, $this->LangId, $this->pagename);
	}
	
		
	// Get website contact information
	function Contacts_Filiali($langid)
	{
		global $TABLE_OFFICES, $TABLE_OFFICES_LANG;

		$its = Array();
		$query = "SELECT o1.id as filid, o1.map_file, o1.page_link, o2.*
			FROM $TABLE_OFFICES o1
			INNER JOIN $TABLE_OFFICES_LANG o2 ON o1.id=o2.office_id AND o2.lang_id='$langid'
			ORDER BY o1.sort_num";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object($res) )
			{
				$it = Array();
				$it['id'] = $row->filid;
				$it['name'] = stripslashes($row->title);
				$it['city'] = stripslashes($row->city);
				$it['phone'] = stripslashes($row->phone);
				$it['address'] = stripslashes($row->address);
				$it['fax'] = stripslashes($row->fax);
				$it['email'] = stripslashes($row->email);

				$its[] = $it;
			}
			mysql_free_result($res);
		}
		else
			echo mysql_error();

		return $its;
	}

	// Get banners
	function Banners_Get($LangId, $pagepos=-1, $pageurl="", $usemask=false )
	{
		global $WWWHOST, $FILE_DIR, $TABLE_BANNERS, $TABLE_BANNERS_LANGS;

		$cond = "b1.managetype=1";
		$sort = "b1.id";
		if( $pageurl != "" )
		{
			$cond = " b1.managetype=0 AND b1.page_url LIKE '".addslashes($pageurl).($usemask ? "%" : "")."' ";
			$sort = "b1.sort_num";
		}

		if( $pagepos != -1 )
		{
			$cond .= " AND b1.disppos='".$pagepos."' ";
			$sort = "b1.sort_num";
		}

		$bans = Array();

		$query = "SELECT b1.*, bl1.alttext FROM $TABLE_BANNERS b1
			INNER JOIN $TABLE_BANNERS_LANGS bl1 ON b1.id=bl1.banner_id AND bl1.lang_id='$LangId'
			WHERE $cond
			ORDER BY $sort";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object( $res ) )
			{
				$bi = Array();
				$bi['id'] = $row->id;
				$bi['link'] = stripslashes($row->linkurl);
				$bi['alt'] = stripslashes($row->alttext);
				$bi['file'] = ( trim(stripslashes($row->filename)) != "" ? $WWWHOST.$FILE_DIR.stripslashes($row->filename) : "" );
				$bi['type'] = $row->disptype;
				$bi['pos'] = $row->disppos;
				$bi['w'] = $row->width;
				$bi['h'] = $row->height;

				$bans[] = $bi;
			}
			mysql_free_result( $res );
		}

		return $bans;
	}

	function Banners_Show($banobj)
	{
		global $BANNER_TYPE_FLASH;

		if( !$banobj )
			return "";

		$bancode = "";
		if( isset($banobj) && isset($banobj['file']) && ($banobj['file'] != "") )
		{
			if( $banobj['type'] == $BANNER_TYPE_FLASH )
			{
				$bancode = '<OBJECT classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
	 codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
	 WIDTH="'.$banobj['w'].'" HEIGHT="'.$banobj['h'].'" id="BANNER'.$banobj['id'].'" ALIGN="">
	 <PARAM NAME=movie VALUE="'.$banobj['file'].'"> <PARAM NAME=quality VALUE=high> <PARAM NAME=bgcolor VALUE=#FFFFFF> <EMBED src="'.$banobj['file'].'" quality=high bgcolor=#FFFFFF  WIDTH="'.$banobj['w'].'" HEIGHT="'.$banobj['h'].'" NAME="BANNER'.$banobj['id'].'" ALIGN=""
	 TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED>
	</OBJECT>';
			}
			else
			{
				$bancode = "<a href=\"".$banobj['link']."\"><img src=\"".$banobj['file']."\" width=\"".$banobj['w']."\" height=\"".$banobj['h']."\" alt=\"".$banobj['alt']."\" border=\"0\" /></a>";
			}
		}

		return $bancode;
	}

	function Breadcrumbs_Add($url, $title)
	{
		global $BREADCRUMBS;

		if( empty($BREADCRUMBS) )
			$BREADCRUMBS = Array();

		$BREADCRUMBS[] = Array("url" => $url, "name" => $title);
	}
	
	function Seo_LoadData()
	{
		$url = $_SERVER['REQUEST_URI'];
		
		$query = "SELECT * FROM ".TABLE_SEO_TITLES." WHERE lang_id=".$this->LangId." AND url LIKE '".addslashes($url)."' ";
		$res = $this->db->query($query);
		if( count($res)>0 )
			return $res[0];
		
		return null;
	}

	function Seo_LoadData_imp($id, $table) {
		switch ($table) {
			case 'helpwall' : $query = "SELECT title FROM ".TABLE_NEWS_LANGS." WHERE news_id = ".addslashes($id); break;
			case 'user' : $query = "SELECT b1.name, b1.fname, c1.name as city FROM ".TABLE_SHOP_BUYERS." b1 
															LEFT JOIN ".TABLE_CITY_LANG." c1 ON b1.city_id = c1.city_id 
															WHERE b1.id = ".addslashes($id); break;
			default: return false;
		}
		return $this->db->query($query);
	}
}
?>