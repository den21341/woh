<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsAdminApp extends UhCmsApp{
	
	protected function startSession()
	{
		self::$ses = new UhCmsAuthSes(self::$db);
		self::$ses->startSession(SESSION_PATH);
	
		// If supports user sessions
		if( $this->config['WITH_AUTH'] ){
			// Authentication here...
			self::$ses->AuthUser();
		}
	
		$this->showLog("Ses id: ".self::$ses->sesid()."; UserID = ".self::$ses->UserId);
	}
	
	protected function startRouting()
	{
		$this->full_req_url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	
		$this->showLog("Full path: ".$this->full_req_url);
	
		$routeurl = "";
		if( strncmp($this->full_req_url, WWWHOST, strlen(WWWHOST)) == 0 )
		{
			$routeurl = substr($this->full_req_url, strlen(WWWHOST));
		}
	
		$this->router = new UhCmsAdminRouter( $routeurl, "", $this->config, self::$db, self::$ses );
	}	
}
?>