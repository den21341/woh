<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Catalog
{
	protected $db;
	protected $LangId;
	protected $clientGroups;

	function __construct($db, $LangId)
	{
		$this->db = $db;
		$this->LangId = $LangId;

		$this->clientGroups = Array(0 => "Физическое лицо", 1 => "Юридическое лицо", 2 => "Общественная организация");
	}

	////////////////////////////////////////////////////////////////////////////
	//
	// Currency utils
	//
	////////////////////////////////////////////////////////////////////////////

	public function Currency_List()
	{
		$query = "SELECT c1.*, c2.name FROM " . TABLE_CAT_CURRENCY . " c1
INNER JOIN " . TABLE_CAT_CURRENCY_LANGS . " c2 ON c1.id=c2.item_id AND c2.lang_id='" . $this->LangId . "'
ORDER BY c1.sort_num, c2.name";
		$res = $this->db->query($query);

		return $res;
	}

	public function Currency_Info($id)
	{
		$query = "SELECT c1.*, c2.name FROM " . TABLE_CAT_CURRENCY . " c1
INNER JOIN " . TABLE_CAT_CURRENCY_LANGS . " c2 ON c1.id=c2.item_id AND c2.lang_id='" . $this->LangId . "'
WHERE c1.id='" . $id . "'";
//echo $query."<br>";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0];

		return $res;
	}

////////////////////////////////////////////////////////////////////////////
//
// Country/City utils
//
////////////////////////////////////////////////////////////////////////////

	private function _cat_FilterCond($sid)
	{
		$sql_cond = "";

		$slev = 3;
		if ($sid == 0) {
			$slev = -1;
		} else {
			$spath = $this->Catalog_SectPath($sid);

			if (count($spath) == 1)
				$slev = 2;
		}

		$sql_cond = " INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id ";
		if ($slev == 3) {
			$sql_cond .= " AND cc1.sect_id='$sid' ";
		} else if ($slev == 2) {
			$sql_cond .= " INNER JOIN " . TABLE_CAT_CATALOG . " cat1 ON cc1.sect_id=cat1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON cat1.parent_id=cp1.id AND cp1.id='$sid' ";
		} else {
			$sql_cond .= " INNER JOIN " . TABLE_CAT_CATALOG . " cat1 ON cc1.sect_id=cat1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON cat1.parent_id=cp1.id AND cp1.parent_id='$sid' ";
		}

		return $sql_cond;
	}

	public function Loc_CountryList($cont_id = 0, $onlyvis = true, $sid = 0, $chech_if_item_exist = 1)
	{
		$cond = "";
		$innercond = "";
		$distinct = "";

		if ($onlyvis && $this->LangId == 1) {
			$cond = ($cond != "" ? " AND " : " WHERE ") . " r1.visible = 1 ";
		}

		if ($cont_id != 0)
			$cond = ($cond != "" ? " AND " : " WHERE ") . " r1.continent_id = '" . $cont_id . "' ";
		if (($sid == "all") || ($sid != 0)) {
			$distinct = " DISTINCT ";

			if ($sid == "all")
				$sect_cond = $this->_cat_FilterCond(0);
			else
				$sect_cond = $this->_cat_FilterCond($sid);

			if ($chech_if_item_exist == 1) {
				$innercond = "INNER JOIN " . TABLE_CITY . " c1 ON r1.id=c1.country_id
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON c1.id=i1.city_id AND i1.start_date<=NOW() AND i1.end_date>=NOW() AND i1.archive=0 " . $sect_cond;
			}
		}

		$query = "SELECT $distinct r1.*, r2.name FROM " . TABLE_COUNTRY . " r1
INNER JOIN " . TABLE_COUNTRY_LANG . " r2 ON r1.id=r2.country_id AND r2.lang_id='" . $this->LangId . "'
$innercond
$cond
ORDER BY r1.priority DESC, r2.name";
		$res = $this->db->query($query);

		return $res;
	}

	public function Loc_OblList($country_id, $sid = 0)
	{
		$innercond = "";
		$distinct = "";

		if ($sid == "0") {
//$query = "SELECT $distinct r1.*, r2.name FROM ".TABLE_REGION." r1
//	INNER JOIN ".TABLE_REGION_LANG." r2 ON r1.id=r2.region_id AND r2.lang_id='".$this->LangId."'
//	$innercond
//	WHERE r1.country_id='".$country_id."'
//	ORDER BY r1.sort_num, r2.name";
		} else if (($sid == "all") || ($sid != 0)) {
			$distinct = " DISTINCT ";

			if ($sid == "all")
				$sect_cond = $this->_cat_FilterCond(0);
			else
				$sect_cond = $this->_cat_FilterCond($sid);

			$innercond = "INNER JOIN " . TABLE_CITY . " c1 ON r1.id=c1.obl_id
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON c1.id=i1.city_id AND i1.start_date<=NOW() AND i1.end_date>=NOW() AND i1.archive=0 " . $sect_cond;
		}

		$query = "SELECT $distinct r1.*, r2.name FROM " . TABLE_REGION . " r1
INNER JOIN " . TABLE_REGION_LANG . " r2 ON r1.id=r2.region_id AND r2.lang_id='" . $this->LangId . "'
$innercond
WHERE r1.country_id='" . $country_id . "'
ORDER BY r1.sort_num, r2.name";
//echo $query."<br>";
		$res = $this->db->query($query);

		return $res;
	}

	public function Loc_CityList($country_id, $obl_id = 0, $sid = 0) {
		$innercond = "";
		$distinct = "";
		$obl_cond = "";

		if ($country_id != 0)
			$obl_cond = ($obl_cond != "" ? " AND " : " WHERE ") . " r1.country_id='$country_id' ";
		if ($obl_id != 0)
			$obl_cond = ($obl_cond != "" ? " AND " : " WHERE ") . " r1.obl_id='$obl_id' ";

		if ($sid == "0") {
// Do nothing
		} else if (($sid == "all") || ($sid != 0)) {
			if ($sid == "all")
				$sect_cond = $this->_cat_FilterCond(0);
			else
				$sect_cond = $this->_cat_FilterCond($sid);

			$distinct = " DISTINCT ";
			$innercond = " INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.id=i1.city_id AND i1.start_date<=NOW() AND i1.end_date>=NOW() AND i1.archive=0 " . $sect_cond;
		}

		$query = "SELECT $distinct r1.*, r2.name FROM " . TABLE_CITY . " r1
INNER JOIN " . TABLE_CITY_LANG . " r2 ON r1.id=r2.city_id AND r2.lang_id='" . $this->LangId . "'
$innercond
$obl_cond
ORDER BY r2.name";
		$res = $this->db->query($query);
		return $res;
	}

	public function Loc_City($city_id)
	{
		$query = "SELECT r1.*, r2.name FROM " . TABLE_CITY . " r1
INNER JOIN " . TABLE_CITY_LANG . " r2 ON r1.id=r2.city_id AND r2.lang_id='" . $this->LangId . "'
WHERE r1.id='" . $city_id . "'";
		$res = $this->db->query($query);

		if (count($res) > 0)
			return $res[0];

		return null;
	}

	public function Loc_InfoByCity($city_id)
	{
		$query = "SELECT c1.*, c2.name, r2.name as regname, cc2.name as countryname, cc1.code, c2.lat, c2.lng, c2.city_id FROM " . TABLE_CITY . " c1
INNER JOIN " . TABLE_CITY_LANG . " c2 ON c1.id=c2.city_id AND c2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_REGION . " r1 ON c1.obl_id=r1.id
INNER JOIN " . TABLE_REGION_LANG . " r2 ON r1.id=r2.region_id AND r2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_COUNTRY . " cc1 ON c1.country_id=cc1.id
INNER JOIN " . TABLE_COUNTRY_LANG . " cc2 ON cc1.id=cc2.country_id AND cc2.lang_id='" . $this->LangId . "'
WHERE c1.id='" . $city_id . "'";
		$res = $this->db->query($query);

		if (count($res) > 0)
			return $res[0];

		return Array("id" => 0);
	}

	public function Loc_GetCountryIdByCity($city_id)
	{
		$query = "SELECT r1.*, r2.name FROM " . TABLE_CITY . " r1
INNER JOIN " . TABLE_CITY_LANG . " r2 ON r1.id=r2.city_id AND r2.lang_id='" . $this->LangId . "'
WHERE r1.id='" . $city_id . "'";
		$res = $this->db->query($query);

		if (count($res) > 0)
			return $res[0]['country_id'];

		return 0;
	}

////////////////////////////////////////////////////////////////////////////
//
// Buyer utils
//
////////////////////////////////////////////////////////////////////////////
	public function Buyer_GroupList()
	{
//$its = $this->db->query("SELECT * FROM ".TABLE_USER_GROUPS." ORDER BY id ");

		return $this->clientGroups;
	}

	public function Buyer_GroupInfo($group_id)
	{
//$its = $this->db->query("SELECT * FROM ".TABLE_USER_GROUPS." WHERE id='$group_id'");
//if( count($its) > 0 )
//	return $its[0];

		if (isset($this->clientGroups[$group_id]))
			return $this->clientGroups[$group_id];

		return "";
//return $its;
	}

	public function Buyer_Info($buyer_id)
	{
		$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='$buyer_id'";
		$its = $this->db->query($query);

		return (count($its) > 0 ? $its[0] : Array("id" => 0));
	}

	public function Buyer_Sects($buyer_id, $withsectinfo = false)
	{
		$query = "SELECT * FROM " . TABLE_SHOP_BUYER_SECTS . " WHERE buyer_id='$buyer_id'";
		if ($withsectinfo) {
			$query = "SELECT c1.*, c2.name, pc1.filename_thumb as ppic, pc2.name as pname
FROM " . TABLE_SHOP_BUYER_SECTS . " b1
INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON b1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_CAT_CATALOG . " pc1 ON c1.parent_id=pc1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " pc2 ON pc1.id=pc2.sect_id AND pc2.lang_id='" . $this->LangId . "'
WHERE b1.buyer_id='$buyer_id'
ORDER BY pc1.sort_num, pc1.id, pc2.name, c1.sort_num, c2.name";
//echo $query."<br>";
		}
		$its = $this->db->query($query);

		return $its;
	}

	public function Buyer_SectCheck($buyer_id, $sect_id)
	{
		$query = "SELECT * FROM " . TABLE_SHOP_BUYER_SECTS . " WHERE buyer_id='$buyer_id' AND sect_id='$sect_id'";
		$its = $this->db->query($query);

		return (count($its) > 0);
	}

	public function Buyer_List($buyer_group = -1, $pi = -1, $pn = 20, $sortby = "", $sqlcond = "")
	{
		$sql_cond = "";
		if ($buyer_group >= 0) {
			$sql_cond .= ($sql_cond != "" ? " AND " : " WHERE ") . " u1.account_type='$buyer_group' ";
		}

		$sql_sort = " u1.id ";
		switch ($sortby) {
			case "name":
				$sql_sort = " u1.fname, u1.name ";
				break;

			case "login":
				$sql_sort = " u1.login ";
				break;

			case "adddate":
				$sql_sort = " u1.add_date ";
				break;

			case "rand":
				$sql_sort = " RAND() ";
				break;
		}

		$limit_cond = "";
		if ($pi >= 0) {
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";
		}

		$query = "SELECT u1.* FROM " . TABLE_SHOP_BUYERS . " u1 $sql_cond ORDER BY $sql_sort $limit_cond";
		$its = $this->db->query($query);

		return $its;
	}

	public function Buyer_ItemSects($uid, $help_type, $amout_type = -1)
	{
		$res = Array();

		if ($help_type == PROJ_NEEDHELP) {
			$query = "SELECT c1.*, c2.name, count(cc1.id) as itnum FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id
INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id='" . $this->LangId . "'
WHERE author_id='$uid'
GROUP BY c1.id
ORDER BY c1.parent_id, c1.sort_num, c2.name";
			$res = $this->db->query($query);
		}

		return $res;
	}

	public function Buyer_ReqIsSend($uid, $projid, $onlystatus = 0)
	{
		$query = "SELECT * FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1 WHERE r1.item_id='$projid' AND r1.sender_id='$uid'
AND (r1.req_status='" . REQ_STATUS_NEW . "' OR r1.req_status='" . REQ_STATUS_CONFIRM . "')";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return Array("id" => $res[0]['id'], "status" => $res[0]['req_status']);

		return false;
	}

	public function Buyer_MyhelpReqNum($uid, $req_type, $req_status = REQ_STATUS_ALL, $proj_status = PROJ_STATUS_ALL, $proj_help_type = -1, $withamount = false, $show = -1, $group = 0) {
		$req_stat_cond = "";

		$sql_cond = "";
		if ($proj_help_type != -1)
			$sql_cond .= " AND i1.amount_type='$proj_help_type' ";
		if ($proj_status != PROJ_STATUS_ALL)
			$sql_cond = " AND i1.status='" . $proj_status . "' ";

		$show_cond = "";
		if($show != -1){
			$show_cond = " AND `show` = ".addslashes($show).' ';
		}

		$group_cond = "";
		if($group > 0) {
			$group_cond = " GROUP BY r1.item_id ";
		}

		if ($req_type == PROJ_NEEDHELP)
		{
			$query = "SELECT count(*) totnum FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 AND i1.author_id='$uid' $sql_cond
WHERE r1.req_status=" . $req_status . " ".$show_cond. $group_cond;
		}
		else
		{
			if ($req_status != REQ_STATUS_ALL)
				$req_stat_cond = " AND r1.req_status=" . $req_status . " ";

			$query = "SELECT count(*) totnum FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 AND i1.profile_id='".PROJ_SENDHELP."' $sql_cond
WHERE r1.sender_id='$uid' $req_stat_cond ".$show_cond.$group_cond;
		}

		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Buyer_ReqNum($uid, $req_type, $req_status = REQ_STATUS_ALL, $proj_status = PROJ_STATUS_ALL, $proj_help_type = -1, $withamount = false, $show = -1, $group = 0) {
		$req_stat_cond = "";

		$sql_cond = "";
		if ($proj_help_type != -1)
			$sql_cond .= " AND i1.amount_type='$proj_help_type' ";
		if ($proj_status != PROJ_STATUS_ALL)
			$sql_cond = " AND i1.status='" . $proj_status . "' ";

		$show_cond = "";
		if($show != -1){
			$show_cond = " AND `show` = ".addslashes($show).' ';
		}

		$group_cond = "";
		if($group > 0) {
			$group_cond = " GROUP BY r1.item_id ";
		}

		if ($req_type == PROJ_NEEDHELP)
		{
			$query = "SELECT count(*) totnum FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 AND i1.author_id='$uid' $sql_cond
WHERE r1.req_status=" . $req_status . " ".$show_cond. $group_cond;
		}
		else
		{
			if ($req_status != REQ_STATUS_ALL)
				$req_stat_cond = " AND r1.req_status=" . $req_status . " ";

			$query = "SELECT count(*) totnum FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 AND i1.profile_id<>'".PROJ_SENDHELP."' $sql_cond
WHERE r1.sender_id='$uid' $req_stat_cond ".$show_cond.$group_cond;
		}

		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Buyer_ReqAmountTotal($uid, $req_type, $req_status = REQ_STATUS_ALL, $proj_status = PROJ_STATUS_ALL, $proj_help_type = -1)
	{
		$req_stat_cond = "";

		$sql_cond = "";
		if ($proj_help_type != -1)
			$sql_cond .= " AND i1.amount_type='$proj_help_type' ";
		if ($proj_status != PROJ_STATUS_ALL)
			$sql_cond = " AND i1.status='" . $proj_status . "' ";

		if ($req_type == PROJ_NEEDHELP) {
			$query = "SELECT count(r1.id) totnum, sum(r1.req_amount) as totsum FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 AND i1.author_id='$uid' $sql_cond
WHERE r1.req_status=" . $req_status . " ";
		} else {
			if ($req_status != REQ_STATUS_ALL)
				$req_stat_cond = " AND r1.req_status=" . $req_status . " ";

			$query = "SELECT count(*) totnum, sum(r1.req_amount) as totsum FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 $sql_cond
WHERE r1.sender_id='$uid' $req_stat_cond ";
		}

		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0];
		return Array("totnum" => 0, "totsum" => 0);
	}


	/*public function Buyer_ReqList($uid, $req_type, $req_status = REQ_STATUS_ALL, $sortby = "", $pi = -1, $pn = 20, $proj_status = PROJ_STATUS_ALL)
	{
		$req_stat_cond = "";
		if ($req_status != REQ_STATUS_ALL)
			$req_stat_cond = " AND r1.req_status=" . $req_status . " ";

		$proj_cond = "";
		if ($proj_status != PROJ_STATUS_ALL)
			$proj_cond = " AND i1.status='" . $proj_status . "' ";

		$sort_cond = " r1.add_date DESC ";

		$limit_cond = "";
		if ($pi >= 0)
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";

		if ($req_type == PROJ_SENDHELP) {
			$query = "SELECT r1.*, i1.title, i1.amount_type, i1.amount, i1.status, i2.title2, b1.id as reciever_id, b1.account_type, b1.id as uid, b1.name, b1.fname, b1.angel_day, b1.angel_week, b1.orgname, b1.pic, b1.pic_sm,
case when b1.account_type=" . USR_TYPE_PERS . " then CONCAT(b1.name, ' ', b1.fname) else b1.orgname end as uname,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall,
TIMESTAMPDIFF(MINUTE, r1.add_date, NOW()) as minago
FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 $proj_cond
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON i1.author_id=b1.id
WHERE r1.sender_id='$uid' " . $req_stat_cond . "
ORDER BY $sort_cond
$limit_cond";
		} else {
			$query = "SELECT r1.*, i1.title, i1.amount_type, i1.amount, i1.status, i2.title2, b1.account_type, b1.id as uid,  b1.name, b1.fname, b1.angel_day, b1.angel_week, b1.orgname, b1.pic, b1.pic_sm,
case when b1.account_type=" . USR_TYPE_PERS . " then CONCAT(b1.name, ' ', b1.fname) else b1.orgname end as uname,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall,
TIMESTAMPDIFF(MINUTE, r1.add_date, NOW()) as minago
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_HELPREQ . " r1 ON r1.item_id=i1.id $req_stat_cond
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.sender_id=b1.id
WHERE i1.author_id='$uid' $proj_cond
ORDER BY $sort_cond
$limit_cond";
		}
		$res = $this->db->query($query);
		return $res;
	}*/

	public function Buyer_ReqList($uid, $req_type, $req_status = REQ_STATUS_ALL, $sortby = "", $pi = -1, $pn = 20, $proj_status = PROJ_STATUS_ALL, $curr = 0, $amount = -10, $count = 0, $group = 0) {
		$amount_cond = "";
		if($amount != -10) {
			$amount_cond = " AND i1.amount_type = ".addslashes($amount)." ";
		}

		$req_stat_cond = "";
		if ($req_status != REQ_STATUS_ALL)
			$req_stat_cond = " AND r1.req_status=" . $req_status . " ";

		$proj_cond = "";
		if ($proj_status != PROJ_STATUS_ALL)
			$proj_cond = " AND i1.status='" . $proj_status . "' ";

		$sort_cond = " r1.add_date DESC ";

		$limit_cond = "";
		if ($pi >= 0)
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";

		$crr_join = "";
		if($curr != 0) {
			$crr_join = " INNER JOIN ". TABLE_CAT_CURRENCY_LANGS ." c1 ON c1.item_id=i1.currency_id" ;
		}

		$group_cond = "";
		if($group != 0) {
			$group_cond = " GROUP BY r1.item_id ";
		}

		if ($req_type == PROJ_SENDHELP) {
			$query = "SELECT ".(!$count ? " r1.*, ".($group != 0 ? " SUM(r1.req_amount) req_amount, " : "" )." i1.title, i1.amount_type, i1.amount, i1.status, i1.currency_id, i2.title2, b1.id as reciever_id, b1.account_type, b1.id as uid, b1.name, b1.fname, b1.angel_day, b1.angel_week, b1.orgname, b1.pic, b1.pic_sm, ".($curr != 0 ? ' c1.name as names, ' : '')."
case when b1.account_type=" . USR_TYPE_PERS . " then CONCAT(b1.name, ' ', b1.fname) else b1.orgname end as uname,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall,
TIMESTAMPDIFF(MINUTE, r1.add_date, NOW()) as minago " : "count(*) count "). " 
FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 AND i1.profile_id<>'".PROJ_SENDHELP."' $proj_cond
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON i1.author_id=b1.id
".$crr_join."
WHERE r1.sender_id='$uid' " . $req_stat_cond . " ".$amount_cond ." $group_cond
ORDER BY $sort_cond 
$limit_cond";
		} else {
			$query = "SELECT ".(!$count ? " r1.*, ".($group != 0 ? " SUM(r1.req_amount) req_amount, " : "" )." i1.title, i1.amount_type, i1.amount, i1.status, i1.currency_id, i2.title2, b1.account_type, b1.id as uid,  b1.name, b1.fname, b1.angel_day, b1.angel_week, b1.orgname, b1.pic, b1.pic_sm, ".($curr != 0 ? ' c1.name as names, ' : '')."
case when b1.account_type=" . USR_TYPE_PERS . " then CONCAT(b1.name, ' ', b1.fname) else b1.orgname end as uname,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall,
TIMESTAMPDIFF(MINUTE, r1.add_date, NOW()) as minago " : "count(*) count "). " 
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_HELPREQ . " r1 ON r1.item_id=i1.id $req_stat_cond
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.sender_id=b1.id
".$crr_join."
WHERE i1.author_id='$uid' AND i1.profile_id<>'".PROJ_SENDHELP."' $proj_cond ".$amount_cond ." $group_cond
ORDER BY $sort_cond
$limit_cond";
		}

		//if( $uid == 80 )
		//	echo $query."<br>";

		$res = $this->db->query($query);
		return $res;
	}

	public function Buyer_MyhelpReqList($uid, $req_type, $req_status = REQ_STATUS_ALL, $sortby = "", $pi = -1, $pn = 20, $proj_status = PROJ_STATUS_ALL, $curr = 0, $amount = -10, $count = 0, $group = 0) {
		$amount_cond = "";
		if($amount != -10) {
			$amount_cond = " AND i1.amount_type = ".addslashes($amount)." ";
		}

		$req_stat_cond = "";
		if ($req_status != REQ_STATUS_ALL)
			$req_stat_cond = " AND r1.req_status=" . $req_status . " ";

		$proj_cond = "";
		if ($proj_status != PROJ_STATUS_ALL)
			$proj_cond = " AND i1.status='" . $proj_status . "' ";

		$sort_cond = " r1.add_date DESC ";

		$limit_cond = "";
		if ($pi >= 0)
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";

		$crr_join = "";
		if($curr != 0) {
			$crr_join = " INNER JOIN ". TABLE_CAT_CURRENCY_LANGS ." c1 ON c1.item_id=i1.currency_id" ;
		}

		$group_cond = "";
		if($group != 0) {
			$group_cond = " GROUP BY r1.item_id ";
		}

		if ($req_type == PROJ_SENDHELP) {
			$query = "SELECT ".(!$count ? " r1.*, ".($group != 0 ? " SUM(r1.req_amount) req_amount, " : "" )." i1.title, i1.amount_type, i1.amount, i1.status, i1.currency_id, i2.title2, b1.id as reciever_id, b1.account_type, b1.id as uid, b1.name, b1.fname, b1.angel_day, b1.angel_week, b1.orgname, b1.pic, b1.pic_sm, ".($curr != 0 ? ' c1.name as names, ' : '')."
case when b1.account_type=" . USR_TYPE_PERS . " then CONCAT(b1.name, ' ', b1.fname) else b1.orgname end as uname,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall,
TIMESTAMPDIFF(MINUTE, r1.add_date, NOW()) as minago " : "count(*) count "). " 
FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id AND i1.archive=0 AND i1.profile_id='".PROJ_SENDHELP."' $proj_cond
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON i1.author_id=b1.id
".$crr_join."
WHERE r1.sender_id='$uid' " . $req_stat_cond . " ".$amount_cond ." $group_cond
ORDER BY $sort_cond 
$limit_cond";
		} else {
			$query = "SELECT ".(!$count ? " r1.*, ".($group != 0 ? " SUM(r1.req_amount) req_amount, " : "" )." i1.title, i1.amount_type, i1.amount, i1.status, i1.currency_id, i2.title2, b1.account_type, b1.id as uid,  b1.name, b1.fname, b1.angel_day, b1.angel_week, b1.orgname, b1.pic, b1.pic_sm, ".($curr != 0 ? ' c1.name as names, ' : '')."
case when b1.account_type=" . USR_TYPE_PERS . " then CONCAT(b1.name, ' ', b1.fname) else b1.orgname end as uname,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall,
TIMESTAMPDIFF(MINUTE, r1.add_date, NOW()) as minago " : "count(*) count "). " 
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_HELPREQ . " r1 ON r1.item_id=i1.id $req_stat_cond
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.sender_id=b1.id
".$crr_join."
WHERE i1.author_id='$uid' AND i1.profile_id='".PROJ_SENDHELP."' $proj_cond ".$amount_cond ." $group_cond
ORDER BY $sort_cond
$limit_cond";
		}

		//if( $uid == 80 )
		//	echo $query."<br>";

		$res = $this->db->query($query);
		return $res;
	}

	public function Buyer_ReqStarsRate($uid, $req_type = -1, $period_months = -1)
	{
		$sql_cond = "";
		if ($period_months > 0)
			$sql_cond = " AND add_date>DATE_SUB(NOW(), 'INTERVAL " . $period_months . " MONTH') ";

		$type_cond = "";
		if ($req_type != -1)
			$type_cond = " INNER JOIN " . TABLE_CAT_ITEMS_HELPREQ . " h1 ON h1.id=r1.id AND h1.req_type='$req_type' ";

		$query = "SELECT avg(r1.rate) as avgrate, count(r1.id) as totrate FROM " . TABLE_CAT_ITEMS_HELPREQ_RATE . " r1
$type_cond
WHERE r1.user_id='$uid' $sql_cond ";
		$res = $this->db->query($query);

		return (count($res) > 0 ? $res[0] : Array("avgrate" => 0, "totrate" => 0));
	}

	public function Buyer_ReqStarsList($uid, $req_type = -1, $period_months = -1, $amount = -1) {
		$sql_cond = "";
		if ($period_months > 0)
			$sql_cond = " AND add_date>DATE_SUB(NOW(), 'INTERVAL " . $period_months . " MONTH') ";

		$type_cond = "";
		if ($req_type != -1)
			$type_cond = " AND h1.req_type='$req_type' ";

		$mon_type = "";
		if($amount == PROJ_TYPE_MONEY) {
			$mon_type = " AND i1.amount_type = ".PROJ_TYPE_MONEY;
		}

		$query = "SELECT r1.*, b1.account_type, b1.orgname, b1.name, b1.fname, b1.pic, b1.pic_sm, i1.amount, i1.amount_type, i2.title2
FROM " . TABLE_CAT_ITEMS_HELPREQ_RATE . " r1
INNER JOIN " . TABLE_CAT_ITEMS_HELPREQ . " h1 ON h1.id=r1.id $type_cond
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.author_id=b1.id
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i2.item_id=i1.id AND i2.lang_id='" . $this->LangId . "'
WHERE r1.user_id='$uid' $sql_cond ".$mon_type;
		$res = $this->db->query($query);
		return $res;
	}

	public function Buyer_ReqStarsListCount($uid, $req_type = -1, $period_months = -1, $amount = -1) {
		$sql_cond = "";
		if ($period_months > 0)
			$sql_cond = " AND add_date>DATE_SUB(NOW(), 'INTERVAL " . $period_months . " MONTH') ";

		$type_cond = "";
		if ($req_type != -1)
			$type_cond = " AND h1.req_type='$req_type' ";

		$mon_type = "";
		if($amount == PROJ_TYPE_MONEY) {
			$mon_type = " AND i1.amount_type = ".PROJ_TYPE_MONEY;
		}

		$query = "SELECT count(r1.id) as count
							FROM " . TABLE_CAT_ITEMS_HELPREQ_RATE . " r1
							INNER JOIN " . TABLE_CAT_ITEMS_HELPREQ . " h1 ON h1.id=r1.id $type_cond
							INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.author_id=b1.id
							INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id
							INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i2.item_id=i1.id AND i2.lang_id='" . $this->LangId . "'
							WHERE r1.user_id='$uid' $sql_cond ".$mon_type;
		$res = $this->db->query($query);
		return $res;
	}

//////////////////////////////////////////////////////////////////////////
//
// Catalog utils
//
//////////////////////////////////////////////////////////////////////////

	public function Catalog_SectIdByUrl($sect_url)
	{
//global $TABLE_CAT_CATALOG;

		$sect_id = 0;

		$query = "SELECT id FROM " . TABLE_CAT_CATALOG . " WHERE url='" . addslashes($sect_url) . "'";
		$res = $this->db->query($query);
		if (count($res) > 0)
			$sect_id = $res[0]['id'];
		/*
$query = "SELECT id FROM $TABLE_CAT_CATALOG WHERE url='".addslashes($sect_url)."'";
if( $res = mysql_query( $query ) )
{
if( $row = mysql_fetch_object( $res ) )
{
$sect_id = $row->id;
}
mysql_free_result( $res );
}
*/
		return $sect_id;
	}

	public function Catalog_SectSubNum($sgroup, $sid, $visible = 1)
	{
		$query = "SELECT count(*) as totnum FROM " . TABLE_CAT_CATALOG . " WHERE mode='$sgroup' AND parent_id='" . $sid . "'";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Catalog_SectLevel($sgroup, $sid, $with_prod_num = false, $orderby = "", $pi = -1, $pn = 10)
	{
//global $TABLE_CAT_CATALOG, $TABLE_CAT_CATALOG_LANGS, $TABLE_CAT_CATITEMS, $sid;

		$sort_cond = "c1.sort_num, c2.name";
		$firstp_cond = "";
		switch ($orderby) {
			case "rate":
				$sort_cond = "c1.sect_rate DESC, c1.sort_num";
				break;

			case "show":
				$sort_cond = "c1.show_first DESC, c1.sort_num";
				break;

			case "firstpage":
				$sort_cond = "c1.sect_rate DESC, c1.sort_num";
				$firstp_cond = " AND c1.show_first=1 ";
				break;
		}

		$limit_cond = "";
		if ($pi >= 0) {
			$limit_cond = " LIMIT " . ($pi * $pn) . "," . ($pn) . " ";
		}

		$query = "SELECT c1.*, c2.name, c2.descr
FROM " . TABLE_CAT_CATALOG . " c1
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id='" . $this->LangId . "'
WHERE c1.mode='$sgroup' AND c1.parent_id='$sid' AND c1.visible=1 $firstp_cond
ORDER BY $sort_cond
$limit_cond";

		if ($with_prod_num) {
			$query = "SELECT c1.*, c2.name, c2.descr, count(cc1.id) as totprods
FROM " . TABLE_CAT_CATALOG . " c1
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id='" . $this->LangId . "'
LEFT JOIN " . TABLE_CAT_CATITEMS . " cc1 ON c1.id=cc1.sect_id
WHERE c1.mode='$sgroup' AND c1.parent_id='$sid' AND c1.visible=1 $firstp_cond
GROUP BY c1.id
ORDER BY $sort_cond
$limit_cond";
		}

		$sects = $this->db->query($query);

		/*
$sects = Array();
if( $res1 = mysql_query( $query1 ) )
{
while( $row1 = mysql_fetch_object( $res1 ) )
{
$si = Array();
$is_selected = false;
$si['id'] = $row1->id;
$si['is_link'] = $row1->is_link;
$si['name'] = stripslashes($row1->name);
$si['descr'] = stripslashes($row1->descr);
$si['ico'] = stripslashes($row1->filename);
$si['url'] = stripslashes($row1->url);
//if($sid==$si['id']){
//	$is_selected = true;
//}
//$si['selected'] = $is_selected;

$si['prod_num'] = 0;
if( $with_prod_num )
{
$si['prod_num'] = $row1->totprods;
}

$si['banfile'] = stripslashes($row1->banner_file);
$si['banlink'] = stripslashes($row1->banner_link);

$sects[] = $si;
}
mysql_free_result( $res1 );
}
else
echo mysql_error();
*/

		return $sects;
	}

	public function Catalog_SectPath($sid)
	{
//global $TABLE_CAT_CATITEMS, $TABLE_CAT_CATALOG, $TABLE_CAT_CATALOG_LANGS;

		$sits = Array();
		$sectid = $sid;

		if ($sectid != 0) {
			do {
				$found = false;
				$query1 = "SELECT c1.id, c1.parent_id, c1.url, c2.name
FROM " . TABLE_CAT_CATALOG . " c1, " . TABLE_CAT_CATALOG_LANGS . " c2
WHERE c1.id='$sectid' AND c1.id=c2.sect_id AND c2.lang_id='" . $this->LangId . "'";
				$res1 = $this->db->query($query1);
				if (count($res1) > 0) {
					$sits[] = $res1[0];
					$sectid = $res1[0]['parent_id'];
					$found = true;
				}
				/*
if( $res1 = mysql_query( $query1 ) )
{
if( $row1 = mysql_fetch_object( $res1 ) )
{
$si = Array();
$si['id'] = $row1->id;
$si['name'] = stripslashes($row1->name);
$si['url'] = stripslashes($row1->url);
$sits[] = $si;
$sectid = $row1->parent_id;
$found = true;
}
mysql_free_result( $res1 );
}
else
echo mysql_error();
*/

				if (!$found)
					break;
			} while ($sectid != 0);
		}

		return $sits;
	}

	public function Catalog_SectIsInPath($sid, $path)
	{
		for ($i = 0; $i < count($path); $i++) {
			if ($sid == $path[$i]['id'])
				return true;
		}

		return false;
	}

	public function Catalog_SectInfo($sid)
	{
//global $TABLE_CAT_CATITEMS, $TABLE_CAT_CATALOG, $TABLE_CAT_CATALOG_LANGS;

		$it = Array();

		$query1 = "SELECT c1.*, c2.name, c2.descr, c2.descr0, c2.meta_title, meta_keyw, meta_descr
FROM " . TABLE_CAT_CATALOG . " c1, " . TABLE_CAT_CATALOG_LANGS . " c2
WHERE c1.id='$sid' AND c1.id=c2.sect_id AND c2.lang_id='" . $this->LangId . "'";
		$res1 = $this->db->query($query1);
		if (count($res1) > 0) {
			$it = $res1[0];
		}
		/*
if( $res1 = mysql_query( $query1 ) )
{
if( $row1 = mysql_fetch_object( $res1 ) )
{
$it['id'] = $row1->id;
$it['pid'] = $row1->parent_id;
$it['name'] = stripslashes($row1->name);
$it['descr'] = stripslashes($row1->descr);
$it['descr0'] = stripslashes($row1->descr0);
$it['layout'] = $row1->product_layout;
$it['slayout'] = $row1->section_layout;
$it['pic'] = stripslashes($row1->filename);
$it['url'] = stripslashes($row1->url);

$it['p_title'] = stripslashes($row1->meta_title);
$it['p_keyw'] = stripslashes($row1->meta_keyw);
$it['p_descr'] = stripslashes($row1->meta_descr);

$it['showmakefilt'] = $row1->make_filter;
$it['makeview'] = $row1->make_view;

$it['banfile'] = stripslashes($row1->banner_file);
$it['banlink'] = stripslashes($row1->banner_link);
}
mysql_free_result( $res1 );
}
else
echo mysql_error();
*/

		return $it;
	}

	public function Catalog_ItemsNum($sid, $level = 3, $status = "all")
	{
		$sql_cond = "";
		if ($status == "active") {
			$sql_cond = " AND i1.status='" . PROJ_STATUS_RUN . "' AND i1.end_date>NOW() ";
		}

		if ($level == 3) {
			$sect_cond = " INNER JOIN  " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id AND c1.id='$sid' ";
		} else if ($level == 2) {
			$sect_cond = " INNER JOIN  " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id AND c1.parent_id='$sid' ";
		} else if ($level == 1) {
			$sect_cond = " INNER JOIN  " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN  " . TABLE_CAT_CATALOG . " c2 ON c1.parent_id=c2.id AND c2.parent_id='$sid'";
		}

		$query = "SELECT count(i1.id) as totnum FROM " . TABLE_CAT_CATITEMS . " cc1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON cc1.item_id=i1.id $sql_cond
$sect_cond";

		$res = $this->db->query($query);

		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Catalog_ItemsReqNum($sid, $level = 3, $status = "all", $req_status = REQ_STATUS_ALL)
	{
		$sql_cond = "";
		$req_cond = "";
		if ($status == "active") {
			$sql_cond = " AND i1.status='" . PROJ_STATUS_RUN . "' AND i1.end_date>NOW() ";
		}
		if ($req_status != REQ_STATUS_ALL) {
			$req_cond = " AND r1.req_status='$req_status' ";
		}

		if ($level == 3) {
			$sect_cond = " INNER JOIN  " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id AND c1.id='$sid' ";
		} else if ($level == 2) {
			$sect_cond = " INNER JOIN  " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id AND c1.parent_id='$sid' ";
		} else if ($level == 1) {
			$sect_cond = " INNER JOIN  " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN  " . TABLE_CAT_CATALOG . " c2 ON c1.parent_id=c2.id AND c2.parent_id='$sid'";
		}

		$query = "SELECT count(r1.id) as totnum FROM " . TABLE_CAT_CATITEMS . " cc1
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON cc1.item_id=i1.id $sql_cond
$sect_cond
INNER JOIN " . TABLE_CAT_ITEMS_HELPREQ . " r1 ON i1.id=r1.item_id " . $req_cond;

		$res = $this->db->query($query);

		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Catalog_ItemRootSect($item_id)
	{
		//global $TABLE_CAT_CATITEMS, $TABLE_CAT_CATALOG, $TABLE_CAT_CATALOG_LANGS;

		$sectid = 0;

		$query = "SELECT * FROM ".TABLE_CAT_CATITEMS." WHERE item_id='$item_id'";
		$res = $this->db->query($query);
		if (count($res) > 0)
		{
			$sectid = $res[0]['sect_id'];
		}

		$sits = Array();

		$rootid = $sectid;

		if ($sectid != 0) {
			do {
				$found = false;
				$query1 = "SELECT c1.id, c1.parent_id, c1.url, c2.name
FROM " . TABLE_CAT_CATALOG . " c1, " . TABLE_CAT_CATALOG_LANGS . " c2
	WHERE c1.id='$sectid' AND c1.id=c2.sect_id AND c2.lang_id='" . $this->LangId . "'";
				$res1 = $this->db->query($query1);
				if (count($res1) > 0) {
					if( $res1[0]['parent_id'] == 0 )
						$rootid = $sectid;

					$sits[] = $res1[0];
					$sectid = $res1[0]['parent_id'];
					$found = true;
				}
				/*
				 if( $res1 = mysql_query( $query1 ) )
				 {
				 if( $row1 = mysql_fetch_object( $res1 ) )
				 {
				 $si = Array();
				 $si['id'] = $row1->id;
				 $si['name'] = stripslashes($row1->name);
				 $si['url'] = stripslashes($row1->url);
				 $sits[] = $si;
				 $sectid = $row1->parent_id;
				 $found = true;
				 }
				 mysql_free_result( $res1 );
				 }
				 else
				 	echo mysql_error();
				 	*/

				if (!$found)
					break;
			}
			while ($sectid != 0);
		}

		return $rootid;
	}

//////////////////////////////////////////////////////////////////////////////////////////////
	public function Item_UpdateRate($id)
	{
// Calculate daily item rate
		$daily_rate_id = 0;

		$query = "SELECT * FROM " . TABLE_CAT_ITEMS_RATE . " WHERE item_id='$id' AND dt=CURDATE()";
		$res = $this->db->query($query);
		if (count($res) > 0)
			$daily_rate_id = $res[0]['id'];

		if ($daily_rate_id == 0) {
			$query = "INSERT INTO " . TABLE_CAT_ITEMS_RATE . " (item_id, dt, amount) VALUES ('$id', CURDATE(), 1)";
			$this->db->exec($query);
		} else {
			$query = "UPDATE " . TABLE_CAT_ITEMS_RATE . " SET amount=amount+1 WHERE id=$daily_rate_id";
			$this->db->exec($query);
		}

		$query = "UPDATE " . TABLE_CAT_ITEMS . " SET item_rate=item_rate+1 WHERE id=$id";
		$this->db->exec($query);
	}

	public function Item_UpdateScRate($id, $referer_id, $ip)
	{
// Calculate daily sc item rate
		$daily_rate_id = 0;

		$query = "SELECT * FROM " . TABLE_CAT_ITEMS_SC_RATE . " WHERE item_id='$id' AND dt=CURDATE() AND user_referer_id='" . addslashes($referer_id) . "' AND ip='" . addslashes($ip) . "'";
		$res = $this->db->query($query);
		if (count($res) > 0)
			$daily_rate_id = $res[0]['id'];

		if ($daily_rate_id == 0) {
			$query = "INSERT INTO " . TABLE_CAT_ITEMS_SC_RATE . " (item_id, user_referer_id, dt, add_date, ip) VALUES ('$id', '$referer_id', CURDATE(), NOW(), '" . addslashes($ip) . "')";
			$this->db->exec($query);
		} else {
//$query = "UPDATE ".TABLE_CAT_ITEMS_SC_RATE." SET amount=amount+1 WHERE id=$daily_rate_id";
//$this->db->exec($query);
			return false;
		}

//$query = "UPDATE ".TABLE_CAT_ITEMS." SET item_rate=item_rate+1 WHERE id=$id";
//$this->db->exec($query);

		return true;
	}

	public function Item_Info($id, $sid = 0) {
		$sect_cond = "";

		if ($sid != 0)
			$sect_cond = " AND cc1.sect_id='$sid' ";

		$query = "SELECT i1.*, i2.title2, i2.descr, i2.descr0, c1.id as sectid, c1.url as secturl, c2.name as sectname,
u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
YEAR(i1.end_date) as edy, MONTH(i1.end_date) as edm, DAYOFMONTH(i1.end_date) as edd,
YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id $sect_cond
INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
WHERE i1.id='" . addslashes($id) . "'";

		$res = $this->db->query($query);

		return (count($res) > 0 ? $res[0] : null);
	}

//DataBase Select:

	public function Item_ListCount($profid = -1, $sid, $uid = 0, $slev = 3, $proj_status = PROJ_STATUS_ALL, $fltby = "", $cityby = "", $onlyactive = true, $coId = -1, $oblId = 0, $is_play = 1, $moder = 1, $noLoc = 0, $help_type = 0) {
		$cond = "";
		if ($uid != 0)
			$cond .= " AND i1.author_id='$uid' ";
		if ($proj_status != PROJ_STATUS_ALL)
			$cond .= " AND i1.status='$proj_status' ";

		if (is_array($profid) && (count($profid) > 1)) {
			$cond .= " AND i1.profile_id IN (" . implode(",", $profid) . ") ";
		} else if ($profid != -1)
			$cond .= " AND i1.profile_id='" . $profid . "' ";

		$sect_cond = "";
		if ($sid != 0)
			$sect_cond = " AND cc1.sect_id='$sid' ";

		if (($cityby != 0) && is_numeric($cityby)) {
			$cond .= " AND i1.city_id='" . addslashes($cityby) . "' ";
		}

		if( $oblId != 0 && is_numeric($oblId)) {
			$cond .= " AND i1.obl_id='" . addslashes($oblId) . "' ";
		}

		$play_cond = '';
		if($is_play) {
			//$play_cond = " AND i1.is_play = 1 ";
		}

		$noLoc_cond = "";
		if(is_array($noLoc)) {
			if(isset($noLoc['country_id']) && isset($noLoc['reg_id']) && $noLoc['country_id'] > -1 && $noLoc['reg_id'] > 0) {
				$subCity = "SELECT obl_id obl_f, (SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".addslashes($noLoc['country_id'])." GROUP BY obl_id DESC LIMIT 1) obl_l FROM ".TABLE_CITY." WHERE country_id = ".addslashes($noLoc['country_id'])." GROUP BY obl_id ASC LIMIT 1";
				//echo $subCity; die();//
				$obl_loc = $this->db->query($subCity);
				$obl_loc = isset($obl_loc[0]) ? $obl_loc[0] : 0;

				// SELECT OBL_ID IN USER COUNTRY AND DONT SHOW ITEM WHICH SET LOCATION TYPE = 1

				$subCity = "SELECT id FROM ".TABLE_CAT_ITEMS." WHERE (obl_id BETWEEN ".$obl_loc['obl_f']." AND ".$obl_loc['obl_l']." AND location_type = ".ITEM_LOCATION_COUNTRY.") OR (obl_id <> ".$noLoc['reg_id']." AND location_type = ".ITEM_LOCATION_REGION.") ";
				$noLoc_itm = $this->db->query($subCity);

				if(!empty($noLoc_itm)) {
					foreach ($noLoc_itm as $itmid) {
						$noLoc_cond .= $itmid['id'].',';
					}
					$noLoc_cond = " AND i1.id NOT IN( ".rtrim($noLoc_cond, ',').' ) ';
				}

			}
		}

		switch ($fltby) {
			case "money":
				$cond .= " AND i1.amount_type='" . PROJ_TYPE_MONEY . "' ";
				break;

			case "work":
				$cond .= " AND i1.amount_type<>'" . PROJ_TYPE_MONEY . "' ";
				break;

			case "easy":
				$cond .= " AND i1.help_type = 1";
				break;
			case "offers":
				$cond .= " AND i1.amount_type = ".PROJ_TYPE_MONEY." AND i1.is_arbitr = 1 ";
		}

		if($coId > -1) {
			$subCity = "SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".$coId." GROUP BY obl_id ASC LIMIT 1";
			$obl[0] = $this->db->query($subCity)[0];
			$subCity = "SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".$coId." GROUP BY obl_id DESC LIMIT 1";
			$obl[1] = $this->db->query($subCity)[0];

			$query = "SELECT count(i1.id) as count
				FROM " . TABLE_CAT_ITEMS . " i1
				INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
				INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id $sect_cond
				INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
				INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
				INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
				WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "").$cond.$play_cond.$noLoc_cond." AND i1.help_type = ".addslashes($help_type)." AND i1.obl_id BETWEEN ".$obl[0]['obl_id']." AND ".$obl[1]['obl_id']." AND i1.moderated = $moder ";

		} else {
			$query = "SELECT count(i1.id) as count
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id $sect_cond
INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "") . $cond.$play_cond.$noLoc_cond." AND i1.help_type = ".addslashes($help_type)." AND i1.moderated = $moder ";
		}

		$query_part = "";
		if ($slev == 2)    // The sid is the parent section for project sections
		{
			$query_part = " INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON c1.parent_id=cp1.id AND cp1.id='$sid' ";
		} else if ($slev == 1) {
			$query_part = " INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp2 ON c1.parent_id=cp2.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON cp2.parent_id=cp1.id AND cp1.id='$sid' ";
		}

		if ($query_part != "") {
			if(isset($obl))
				$oblq = " AND i1.obl_id BETWEEN ".$obl[0]['obl_id']." AND ".$obl[1]['obl_id'];
			$query = "SELECT count(i1.id) as count
			FROM " . TABLE_CAT_ITEMS . " i1
			INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
			INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id
			$query_part
			INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
			INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
			WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "") .$cond.$play_cond.$noLoc_cond." AND i1.help_type = ".addslashes($help_type)." AND i1.moderated = $moder ".(isset($oblq) ? $oblq : '');
		}
		//echo $query.'<br>';
		$res = $this->db->query($query);
		return $res;
	}

	public function Item_List($profid = -1, $sid, $uid = 0, $pi = -1, $pn = 20, $slev = 3, $sortby = "", $proj_status = PROJ_STATUS_ALL, $fltby = "", $cityby = "", $onlyactive = true, $coId = -1, $oblId = 0, $moder = -1, $noLoc = 0, $help_type = 0) {
		$cond = "";
		if ($uid != 0)
			$cond .= " AND i1.author_id='$uid' ";
		if ($proj_status != PROJ_STATUS_ALL)
			$cond .= " AND i1.status='$proj_status' ";

		if (is_array($profid) && (count($profid) > 1)) {
			$cond .= " AND i1.profile_id IN (" . implode(",", $profid) . ") ";
		} else if ($profid != -1)
			$cond .= " AND i1.profile_id='" . $profid . "' ";

		$sect_cond = "";
		if ($sid != 0)
			$sect_cond = " AND cc1.sect_id='$sid' ";
//else if( $profid == -1 )
//	$cond .= " AND i1.profile_id<>'".PROJ_THINGS."' ";

		if (($cityby != 0) && is_numeric($cityby)) {
			$cond .= " AND i1.city_id='" . addslashes($cityby) . "' ";
		}

		if( $oblId != 0 && is_numeric($oblId)) {
			$cond .= " AND i1.obl_id='" . addslashes($oblId) . "' ";
		}

		$sort_cond = "i1.modify_date DESC, i1.item_priority_rate DESC, i1.item_rate DESC, i1.add_date DESC ";
//if( $slev == -1 )
//	$sort_cond = " i1.add_date DESC ";

		$limit_cond = "";
		if ($pi >= 0) {
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";
		}

		$moder_cond = "";
		if($moder != -1) {
			$moder_cond = " AND i1.moderated = $moder ";
		}

		$noLoc_cond = "";
		if(is_array($noLoc)) {
			if(isset($noLoc['country_id']) && isset($noLoc['reg_id']) && $noLoc['country_id'] > -1 && $noLoc['reg_id'] > 0) {
				$subCity = "SELECT obl_id obl_f, (SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".addslashes($noLoc['country_id'])." GROUP BY obl_id DESC LIMIT 1) obl_l FROM ".TABLE_CITY." WHERE country_id = ".addslashes($noLoc['country_id'])." GROUP BY obl_id ASC LIMIT 1";
				//echo $subCity; die();//
				$obl = $this->db->query($subCity);
				$obl = isset($obl[0]) ? $obl[0] : 0;
			}

			// SELECT OBL_ID IN USER COUNTRY AND DONT SHOW ITEM WHICH SET LOCATION TYPE = 1

			$subCity = "SELECT id FROM ".TABLE_CAT_ITEMS." WHERE (obl_id BETWEEN ".$obl['obl_f']." AND ".$obl['obl_l']." AND location_type = ".ITEM_LOCATION_COUNTRY.") OR (obl_id <> ".$noLoc['reg_id']." AND location_type = ".ITEM_LOCATION_REGION.") ";
			$noLoc_itm = $this->db->query($subCity);

			if(!empty($noLoc_itm)) {
				foreach ($noLoc_itm as $itmid) {
					$noLoc_cond .= $itmid['id'].',';
				}
				$noLoc_cond = " AND i1.id NOT IN( ".rtrim($noLoc_cond, ',').' ) ';
			}
		}

		$helptype_cond = '';
		if($help_type > -1) {
			$helptype_cond = " AND i1.help_type = ".addslashes($help_type);
		}


		switch ($sortby) {
			case "add":
			case "new":
				$sort_cond = " i1.add_date DESC ";
				break;

			case "popular":
				$sort_cond = " i1.item_rate DESC ";
				break;

			case "ending":
				$sort_cond = " i1.end_date ";
				$cond .= " AND i1.end_date>NOW() ";
				break;
			case "stop":
				$sort_cond = " i1.stop_date ASC ";
				break;
			case "rate" :
				$sort_cond = " i1.item_priority_rate DESC ";
				break;


		}

		switch ($fltby) {
			case "money":
				$cond .= " AND i1.amount_type='" . PROJ_TYPE_MONEY . "' ";
				break;

			case "work":
				$cond .= " AND i1.amount_type<>'" . PROJ_TYPE_MONEY . "' ";
				break;

			case "money_done":
				$cond .= " AND i1.amount ";
				break;

			case "easy":
				$cond .= " AND i1.help_type = 1";
				break;
		}

		if($coId > -1 && $oblId == 0 && $cityby == 0) {
			$subCity = "SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".addslashes($coId)." GROUP BY obl_id ASC LIMIT 1";
			$obl[0] = $this->db->query($subCity)[0];
			$subCity = "SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".addslashes($coId)." GROUP BY obl_id DESC LIMIT 1";
			$obl[1] = $this->db->query($subCity)[0];

			$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, c1.id as sectid, c1.url as secturl, c2.name as sectname,
						u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
						DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
						DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
						TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
						FROM " . TABLE_CAT_ITEMS . " i1
						INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
						INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id $sect_cond
						INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
						INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
						INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
						WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "").$cond.$noLoc_cond.$helptype_cond." AND i1.obl_id BETWEEN ".$obl[0]['obl_id']." AND ".$obl[1]['obl_id']." $moder_cond GROUP BY i1.id ORDER BY i1.item_priority_rate DESC ".$limit_cond;
		}
		else {
			$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, c1.id as sectid, c1.url as secturl, c2.name as sectname,
u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id $sect_cond
INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "") . "$cond $moder_cond $noLoc_cond $helptype_cond
ORDER BY $sort_cond
$limit_cond";
		}

		$query_part = "";
		if ($slev == 2)    // The sid is the parent section for project sections
		{
			$query_part = " INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON c1.parent_id=cp1.id AND cp1.id='$sid' ";
		} else if ($slev == 1) {
			$query_part = " INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp2 ON c1.parent_id=cp2.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON cp2.parent_id=cp1.id AND cp1.id='$sid' ";
		}

		if ($query_part != "") {
			if(isset($obl))
				$oblq = " AND i1.obl_id BETWEEN ".$obl[0]['obl_id']." AND ".$obl[1]['obl_id'];
			$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, c1.id as sectid, c1.url as secturl, c2.name as sectname,
u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id
$query_part
INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "") . " $cond $noLoc_cond $moder_cond $helptype_cond ".(isset($oblq) ? $oblq : '')." $moder_cond
ORDER BY $sort_cond
$limit_cond";
		}

		$res = $this->db->query($query);
		return $res;
	}

// Количество проектов

	public function itemCount($type = 1, $status = 1)
	{
		$query_pt = '';
		if ($type == 0 || $type == 1 || $type == 2) {
			$query_pt = "SELECT count(*) as count
FROM " . TABLE_CAT_ITEMS . " WHERE `amount_type` = " . $type . " and `status` = " . $status;
			if ($status == 0 || $status == 1 || $status == 2) {
				$query_pt . " AND status = " . $status;
			}
		}
		$res = $this->db->query($query_pt);
		return $res[0]['count'];
	}

	public function Item_Num($profid = -1, $sid, $uid = 0, $slev = 3, $sortby = "", $proj_status = PROJ_STATUS_ALL, $fltby = "", $onlyactive = true)
	{
		$cond = "";
		if ($uid != 0)
			$cond .= " AND i1.author_id='$uid' ";
		if ($proj_status != PROJ_STATUS_ALL)
			$cond .= " AND i1.status='$proj_status' ";

		$sect_cond = "";
		if ($sid != 0)
			$sect_cond = " AND cc1.sect_id='$sid' ";

		if (is_array($profid) && (count($profid) > 1)) {
			$cond .= " AND i1.profile_id IN (" . implode(",", $profid) . ") ";
		} else if ($profid != -1)
			$cond .= " AND i1.profile_id='" . $profid . "' ";

		switch ($sortby) {
			case "ending":
				$cond .= " AND i1.end_date>NOW() ";
				break;
		}

		switch ($fltby) {
			case "money":
				$cond .= " AND i1.amount_type='" . PROJ_TYPE_MONEY . "' ";
				break;

			case "work":
				$cond .= " AND i1.amount_type<>'" . PROJ_TYPE_MONEY . "' ";
				break;
		}

		$query = "SELECT count(i1.id) as totnum
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id $sect_cond
INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "") . " $cond";

		$query_part = "";
		if ($slev == 2)    // The sid is the parent section for project sections
		{
			$query_part = " INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON c1.parent_id=cp1.id AND cp1.id='$sid' ";
		} else if ($slev == 1) {
			$query_part = " INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp2 ON c1.parent_id=cp2.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON cp2.parent_id=cp1.id AND cp1.id='$sid' ";
		}

		if ($query_part != "") {
			$query = "SELECT count(i1.id) as totnum
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id
$query_part
INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "") . " $cond";
		}

		$res = $this->db->query($query);

		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Item_PicsNum($id)
	{
		$query = "SELECT count(*) as totpics FROM " . TABLE_CAT_ITEMS_PICS . " WHERE item_id='$id' ";
		$res = $this->db->query($query);

		if (count($res) > 0)
			return $res[0]['totpics'];

		return 0;
	}

	public function Item_Pics($id, $num = 0)
	{
		$limit_cond = "";
		if ($num > 0)
			$limit_cond = "LIMIT 0,$num";

		$query = "SELECT * FROM " . TABLE_CAT_ITEMS_PICS . " WHERE item_id='$id' ORDER BY sort_num $limit_cond";
		$pics = $this->db->query($query);

//for($i=0; $i<count($pics); $i++)
//{
//	if($)
//}

		return $pics;
	}

	public function Item_ReqCollected($id, $req_status = -10, $promShow = 1, $prom_pay = -1, $is_check_tab = -1) {
		$status_cond = "";
		if ($req_status != -10)
			$status_cond = " AND r1.req_status='$req_status' ";

		$prom_cond = "";
		if($prom_pay > -1)
			$prom_cond = " AND r1.promise_pay = ".addslashes($prom_pay);

		$check_cond = "";
		if($is_check_tab > -1) {
			$check_cond = " AND tab_check = ".addslashes($is_check_tab);
		}

		$query = "SELECT avg(r1.req_amount) as avgcollect, sum(r1.req_amount) as totalcollect, count(r1.sender_id) as totalnum
FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1 WHERE r1.item_id='$id' $status_cond AND r1.`show` = $promShow ".$prom_cond.$check_cond;
		$res = $this->db->query($query);
		//echo $query.'<br>';
		if (count($res) > 0) {
			return Array("sum" => $res[0]['totalcollect'], "avg" => $res[0]['avgcollect'], "num" => $res[0]['totalnum']);
		}

		return Array("sum" => 0, "avg" => 0, "num" => 0);
	}

	public function Item_ReqList($id, $req_status = -10, $pi = -1, $pn = 100, $promShow = 1) {
		$status_cond = "";
		if ($req_status != -10)
			$status_cond = " AND r1.req_status='$req_status' ";

		$limit_cond = "";
		if ($pi >= 0)
			$limit_cond = " LIMIT " . addslashes($pi) . ",$pn ";

		$order_cond = " r1.add_date DESC ";

		$query = "SELECT r1.*, DATE_FORMAT(r1.add_date, '%d.%m.%Y %H:%i') as dtpost, DATEDIFF(NOW(), r1.add_date) as daysbefore,
b1.id as author_id, b1.name, b1.fname, b1.account_type, b1.orgname, b1.pic, b1.pic_sm
FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
LEFT JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.sender_id=b1.id
WHERE r1.item_id='$id' AND r1.sender_id <> ".FROM_WOH_ID."  $status_cond AND r1.`show` = $promShow
ORDER BY $order_cond $limit_cond";
		$res = $this->db->query($query);
		return $res;
	}

	public function Item_ReqInfo($rid)
	{
		$query = "SELECT r1.*, DATE_FORMAT(r1.add_date, '%d.%m.%Y %H:%i') as dtpost, DATEDIFF(NOW(), r1.add_date) as daysbefore,
b1.id as author_id, b1.name, b1.fname, b1.account_type, b1.orgname, b1.pic, b1.pic_sm
FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.sender_id=b1.id
WHERE r1.id='$rid'";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0];

		return null;
	}

	public function Item_CommentsNum($id)
	{
		$query = "SELECT count(c1.id) as totnum FROM " . TABLE_CAT_ITEMS_COMMENT . " c1
INNER JOIN " . TABLE_CAT_ITEMS_COMMENT_LANGS . " c2 ON c1.id=c2.item_id AND c2.lang_id='" . $this->LangId . "'
WHERE c1.item_id='" . $id . "'";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Item_Comments($id, $onlyvis = false, $pi = -1, $pn = 20, $withproj = false)
	{
		$limit_cond = "";
		if ($pi >= 0)
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";

		$cond = "";
		if ($onlyvis)
			$cond = ($id != 0 ? " AND " : " WHERE ") . "c1.visible=1 ";

		$proj_flds = "";
		$proj_cond = "";
		if ($withproj) {
			$proj_flds = ", i2.title2 ";
			$proj_cond = " INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON c1.item_id=i1.id
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "' ";
		}

		$query = "SELECT c1.*, c2.content, b1.account_type, b1.orgname, b1.name, b1.fname, b1.pic_sm as pic, DATE_FORMAT(c1.add_date, '%d.%m.%Y %H:%i') as add_dt  $proj_flds
FROM " . TABLE_CAT_ITEMS_COMMENT . " c1
INNER JOIN " . TABLE_CAT_ITEMS_COMMENT_LANGS . " c2 ON c1.id=c2.item_id AND c2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON c1.author_id=b1.id
$proj_cond
" . ($id != 0 ? " WHERE c1.item_id='" . $id . "' " : "") . " $cond
ORDER BY c1.add_date DESC
$limit_cond";

		$res = $this->db->query($query);
		return $res;
	}

	public function Item_CommentInfo($cid)
	{
		$query = "SELECT c1.*, c2.content, b1.account_type, b1.orgname, b1.name, b1.fname, b1.pic, DATE_FORMAT(c1.add_date, '%d.%m.%Y %H:%i') as add_dt
FROM " . TABLE_CAT_ITEMS_COMMENT . " c1
INNER JOIN " . TABLE_CAT_ITEMS_COMMENT_LANGS . " c2 ON c1.id=c2.item_id AND c2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON c1.author_id=b1.id
WHERE c1.id='" . $cid . "'";

		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0];
		return null;
	}

//////////////////////////////////////////////////////////////////
// Bonus
	public function Bonus_UpdateRate($id)
	{
// Calculate daily item rate
		$daily_rate_id = 0;

		$query = "SELECT * FROM " . TABLE_CAT_BONUS_RATE . " WHERE item_id='$id' AND dt=CURDATE()";
		$res = $this->db->query($query);
		if (count($res) > 0)
			$daily_rate_id = $res[0]['id'];

		if ($daily_rate_id == 0) {
			$query = "INSERT INTO " . TABLE_CAT_BONUS_RATE . " (item_id, dt, amount) VALUES ('$id', CURDATE(), 1)";
			$this->db->exec($query);
		} else {
			$query = "UPDATE " . TABLE_CAT_BONUS_RATE . " SET amount=amount+1 WHERE id=$daily_rate_id";
			$this->db->exec($query);
		}

		$query = "UPDATE " . TABLE_CAT_BONUS . " SET item_rate=item_rate+1 WHERE id=$id";
		$this->db->exec($query);
	}

	public function Bonus_Info($id)
	{
		$sect_cond = "";

		$query = "SELECT i1.*, i2.title2, i2.descr, i2.descr0, u1.name, u1.fname, u1.orgname, u1.account_type, u1.pic, u1.pic_sm,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
YEAR(i1.end_date) as edy, MONTH(i1.end_date) as edm, DAYOFMONTH(i1.end_date) as edd,
YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
FROM " . TABLE_CAT_BONUS . " i1
INNER JOIN " . TABLE_CAT_BONUS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
WHERE i1.id='" . addslashes($id) . "'";
		$res = $this->db->query($query);

		return (count($res) > 0 ? $res[0] : null);
	}

	public function Bonus_List($uid = 0, $pi = -1, $pn = 20, $sortby = "", $proj_status = PROJ_STATUS_ALL, $union_sort = 1)
	{
		$cond = "";
		if ($uid != 0)
			$cond .= " AND i1.author_id='$uid' ";
		if ($proj_status != PROJ_STATUS_ALL)
			$cond .= " AND i1.status='$proj_status' ";

		$sort_cond = "i1.item_rate DESC, i1.add_date DESC ";
//if( $slev == -1 )
//	$sort_cond = " i1.add_date DESC ";

		$limit_cond = "";
		if ($pi >= 0) {
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";
		}

		switch ($sortby) {
			case "add":
			case "new":
				$sort_cond = " i1.add_date DESC, i1.amount_used ASC ";
				break;

			case "popular":
				$sort_cond = " i1.item_rate DESC ";
				break;

			case "ending":
				$sort_cond = " i1.end_date ";
				$cond .= " AND i1.end_date>NOW() ";
				break;

			default:
				$sort_cond = " i1.add_date DESC ";
		}

		if($union_sort == 1) {
			$query = "(
							SELECT i1.*, i2.title2, i2.descr0 as descr, u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
								DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
								DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
								TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
								FROM " . TABLE_CAT_BONUS . " i1
								INNER JOIN " . TABLE_CAT_BONUS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
								INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
								WHERE i1.archive=0 $cond AND i1.premoderation=1
								HAVING (i1.amount > i1.amount_used)
								ORDER BY $sort_cond
								$limit_cond
							)
							UNION
							(
							SELECT i1.*, i2.title2, i2.descr0 as descr, u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
								DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
								DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
								TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
								FROM " . TABLE_CAT_BONUS . " i1
								INNER JOIN " . TABLE_CAT_BONUS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
								INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
								WHERE i1.archive=0 $cond AND i1.premoderation=1
								HAVING (i1.amount <= i1.amount_used)
								ORDER BY $sort_cond
								$limit_cond
							)";
		} else {
			$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
							DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
							DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
							TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
							FROM " . TABLE_CAT_BONUS . " i1
							INNER JOIN " . TABLE_CAT_BONUS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
							INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
							WHERE i1.archive=0 $cond AND i1.premoderation=1
							ORDER BY $sort_cond
							$limit_cond";
		}
		//echo $query;
		$res = $this->db->query($query);

		return $res;
	}

	public function Bonus_Num($uid = 0, $sortby = "", $proj_status = PROJ_STATUS_ALL)
	{
		$cond = "";
		if ($uid != 0)
			$cond .= " AND i1.author_id='$uid' ";
		if ($proj_status != PROJ_STATUS_ALL)
			$cond .= " AND i1.status='$proj_status' ";

		switch ($sortby) {
			case "ending":
				$cond .= " AND i1.end_date>NOW() ";
				break;
		}

		$query = "SELECT count(i1.id) as totnum
FROM " . TABLE_CAT_BONUS . " i1
INNER JOIN " . TABLE_CAT_BONUS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
WHERE i1.archive=0 $cond";
		$res = $this->db->query($query);

		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Bonus_Pics($id, $num = 0)
	{
		$limit_cond = "";
		if ($num > 0)
			$limit_cond = "LIMIT 0,$num";

		$query = "SELECT * FROM " . TABLE_CAT_BONUS_PICS . " WHERE item_id='$id' ORDER BY sort_num $limit_cond";
		$pics = $this->db->query($query);

//for($i=0; $i<count($pics); $i++)
//{
//	if($)
//}

		return $pics;
	}

	/*
public function Item_ReqCollected($id, $req_status=-10)
{
$status_cond = "";
if( $req_status != -10 )
$status_cond = " AND r1.req_status='$req_status' ";

$query = "SELECT avg(r1.req_amount) as avgcollect, sum(r1.req_amount) as totalcollect, count(r1.sender_id) as totalnum
FROM ".TABLE_CAT_ITEMS_HELPREQ." r1 WHERE r1.item_id='$id' $status_cond";
$res = $this->db->query($query);
if( count($res)>0 )
{
return Array("sum" => $res[0]['totalcollect'], "avg" => $res[0]['avgcollect'], "num" => $res[0]['totalnum']);
}

return Array("sum" => 0, "avg" => 0, "num" => 0);
}

public function Item_ReqList($id, $req_status=-10, $pi=-1, $pn=100)
{
$status_cond = "";
if( $req_status != -10 )
$status_cond = " AND r1.req_status='$req_status' ";

$limit_cond = "";
if( $pi >= 0 )
$limit_cond = " LIMIT ".addslashes($pi).",$pn ";

$order_cond = " r1.add_date DESC ";

$query = "SELECT r1.*, DATE_FORMAT(r1.add_date, '%d.%m.%Y %H:%i') as dtpost, DATEDIFF(NOW(), r1.add_date) as daysbefore,
b1.id as author_id, b1.name, b1.fname, b1.account_type, b1.orgname, b1.pic, b1.pic_sm
FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
INNER JOIN ".TABLE_SHOP_BUYERS." b1 ON r1.sender_id=b1.id
WHERE r1.item_id='$id' $status_cond
ORDER BY $order_cond $limit_cond";
$res = $this->db->query($query);

return $res;
}

public function Item_ReqInfo($rid)
{
$query = "SELECT r1.*, DATE_FORMAT(r1.add_date, '%d.%m.%Y %H:%i') as dtpost, DATEDIFF(NOW(), r1.add_date) as daysbefore,
b1.id as author_id, b1.name, b1.fname, b1.account_type, b1.orgname, b1.pic, b1.pic_sm
FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
INNER JOIN ".TABLE_SHOP_BUYERS." b1 ON r1.sender_id=b1.id
WHERE r1.id='$rid'";
$res = $this->db->query($query);
if( count($res)>0 )
return $res[0];

return null;
}
*/

	public function Bonus_CommentsNum($id)
	{
		$query = "SELECT count(c1.id) as totnum FROM " . TABLE_CAT_BONUS_COMMENT . " c1
INNER JOIN " . TABLE_CAT_BONUS_COMMENT_LANGS . " c2 ON c1.id=c2.item_id AND c2.lang_id='" . $this->LangId . "'
WHERE c1.item_id='" . $id . "'";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0]['totnum'];

		return 0;
	}

	public function Bonus_Comments($id, $onlyvis = false, $pi = -1, $pn = 20, $withproj = false)
	{
		$limit_cond = "";
		if ($pi >= 0)
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";

		$cond = "";
		if ($onlyvis)
			$cond = ($id != 0 ? " AND " : " WHERE ") . "c1.visible=1 ";

		$proj_flds = "";
		$proj_cond = "";
		if ($withproj) {
			$proj_flds = ", i2.title2 ";
			$proj_cond = " INNER JOIN " . TABLE_CAT_BONUS . " i1 ON c1.item_id=i1.id
INNER JOIN " . TABLE_CAT_BONUS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "' ";
		}

		$query = "SELECT c1.*, c2.content, b1.account_type, b1.orgname, b1.name, b1.fname, b1.pic, DATE_FORMAT(c1.add_date, '%d.%m.%Y %H:%i') as add_dt  $proj_flds
FROM " . TABLE_CAT_BONUS_COMMENT . " c1
INNER JOIN " . TABLE_CAT_BONUS_COMMENT_LANGS . " c2 ON c1.id=c2.item_id AND c2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON c1.author_id=b1.id
$proj_cond
" . ($id != 0 ? " WHERE c1.item_id='" . $id . "' " : "") . " $cond
ORDER BY c1.add_date DESC
$limit_cond";

		$res = $this->db->query($query);
		return $res;
	}

	public function Bonus_CommentInfo($cid)
	{
		$query = "SELECT c1.*, c2.content, b1.account_type, b1.orgname, b1.name, b1.fname, b1.pic, DATE_FORMAT(c1.add_date, '%d.%m.%Y %H:%i') as add_dt
FROM " . TABLE_CAT_BONUS_COMMENT . " c1
INNER JOIN " . TABLE_CAT_BONUS_COMMENT_LANGS . " c2 ON c1.id=c2.item_id AND c2.lang_id='" . $this->LangId . "'
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON c1.author_id=b1.id
WHERE c1.id='" . $cid . "'";

		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0];
		return null;
	}

//////////////////////////////////////////////////////////////////
// Request reviews with rates
	public function Req_ReviewList($reqid, $author_id = 0, $orderby = "")
	{
		$sort_cond = " add_date DESC ";

		$sql_cond = "";
		if ($author_id != 0) {
			$sql_cond = " AND author_id='$author_id' ";
		}

		$query = "SELECT * FROM " . TABLE_CAT_ITEMS_HELPREQ_RATE . " WHERE req_id='" . $reqid . "' ORDER BY $sort_cond";
		$res = $this->db->query($query);

		return $res;
	}

	public function Req_List($minrate = 10, $sortby = "", $sectid = 0, $isparentsect = false, $period_months = -1, $pi = -1, $pn = 3)
	{
		$sort_cond = " r1.add_date DESC ";
		switch ($sortby) {
			case "rand":
				$sort_cond = " RAND() ";
				break;
			case "index":
				$sort_cond = " RAND() ";
				break;
		}

		$limit_cond = "";
		if ($pi >= 0)
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";

		$sql_cond = "";
		if ($period_months > 0)
			$sql_cond = " AND r1.add_date>DATE_SUB(NOW(), 'INTERVAL " . $period_months . " MONTH') ";
		if ($sortby == "index")
			$sql_cond .= " AND r1.show_first=1 ";

		$query = "SELECT r1.*, b1.account_type, b1.name, b1.fname, b1.pic, i1.title, i2.title2, i1.url
FROM " . TABLE_CAT_ITEMS_HELPREQ_RATE . " r1
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.author_id=b1.id
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
WHERE r1.rate>=$minrate $sql_cond
ORDER BY $sort_cond $limit_cond";
		$res = $this->db->query($query);

		return $res;
	}

	public function Req_ReviewInfo($revid)
	{
		$query = "SELECT r1.*, ri1.req_amount, ri1.req_status, b1.account_type, b1.name, b1.fname, b1.pic, i1.title, i2.title2, i1.url
FROM " . TABLE_CAT_ITEMS_HELPREQ_RATE . " r1
INNER JOIN " . TABLE_CAT_ITEMS_HELPREQ . " ri1 ON r1.req_id=ri1.id
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.author_id=b1.id
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON ri1.item_id=i1.id
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
WHERE r1.id='$revid'";
//echo $query."<br>";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0];

		return null;
	}

	public function Req_Info($reqid)
	{
		$query = "SELECT r1.*, b1.account_type, b1.name, b1.fname, b1.pic, i1.title, i2.title2, i1.url
FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
INNER JOIN " . TABLE_SHOP_BUYERS . " b1 ON r1.sender_id=b1.id
INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON r1.item_id=i1.id
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id='" . $this->LangId . "'
WHERE r1.id='$reqid'";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0];

		return null;
	}

//////////////////////////////////////////////////////////////////
// Messages utils
	public function Item_MessagesNum($uid, $id = 0, $inout = "in", $msg_status = -1)
	{
		$status_cond = "";
		if ($msg_status == 0)    // New only
		{
			$status_cond = " AND pp1.is_read=0 ";
		}

		$it_cond = "";
		if ($id != 0) {
			$it_cond = " INNER JOIN " . TABLE_CAT_ITEMS . " i1 ON m1.item_id=i1.id AND i1.id='$id' ";
		}

		$query = "SELECT count(pp1.id) as totmsg FROM " . TABLE_CAT_MSG_P2P . " pp1
INNER JOIN " . TABLE_CAT_MSG . " m1 ON pp1.msg_id=m1.id AND m1." . ($inout == "in" ? "to_id" : "from_id") . "=pp1.user_id
$it_cond
WHERE m1." . ($inout == "in" ? "to_id" : "from_id") . "='$uid' $status_cond ";
		$res = $this->db->query($query);
		if (count($res) > 0)
			return $res[0]['totmsg'];

		return 0;
	}

	public function Msg_List($senderid, $recieverid, $pi = -1, $pn = 20)
	{
		$sql_cond = "";
		$from_cond = "";
		if ($senderid != 0) {
			$sql_cond = ($sql_cond != "" ? " AND " : " WHERE ") . " m1.from_id='$senderid' ";
//$from_cond = " INNER JOIN  ";
		}

		if ($recieverid != 0) {
			$sql_cond = ($sql_cond != "" ? " AND " : " WHERE ") . " m1.to_id='$recieverid' ";
//$from_cond = " INNER JOIN  ";
		}

		$query = "SELECT m1.* FROM " . TABLE_CAT_MSG . " m1
$sql_cond
ORDER BY m1.add_date DESC";

		$res = $this->db->query($query);

		return $res;
	}

//////////////////////////////////////////////////////////////////
// Other utils
	public function PercentDone($minute, $minute_all, $presize = 0, $type = 'time') {
		if ($minute_all == 0)
			return 0;

		if($type == 'time')
			return ($minute_all > $minute ? number_format((100.0 * $minute) / $minute_all, $presize, ".", "") : 0);
		else
			return (number_format((100.0 * $minute) / $minute_all, $presize, ".", ""));
	}

	public function Time_LeftFromMinutes($minute, $txtmode = "normal")
	{
		$localize = UhCmsApp::getLocalizerInstance();

		if ($minute < 0)
			return $localize->time_ended; //"Срок действия истек";

		$daysarr_ru = Array("дней", "день", "дня", "дня", "дня", "дней", "дней", "дней", "дней", "дней", "дней", "дней", "дней", "дней", "дней");
		$hsarr_ru = Array("часов", "час", "часа", "часа", "часа", "часов", "часов", "часов", "часов", "часов", "часов", "часов", "часов", "часов", "часов", "часов");
		$minsarr_ru = Array("минут", "минута", "минуты", "минуты", "минуты", "минут", "минут", "минут", "минут", "минут", "минут", "минут", "минут", "минут", "минут", "минут");

		$daysarr_en = Array("day", "days", "days", "days", "days", "days", "days", "days", "days", "days", "days", "days", "days", "days", "days");
		$hsarr_en = Array("hour", "hours", "hours", "hours", "hours", "hours", "hours", "hours", "hours", "hours", "hours", "hours", "hours", "hours", "hours");
		$minsarr_en = Array("minute", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes", "minutes");

		$days_lang = Array(1 => $daysarr_ru, 2 => $daysarr_en);
		$hs_lang = Array(1 => $hsarr_ru, 2 => $hsarr_en);
		$mins_lang = Array(1 => $minsarr_ru, 2 => $minsarr_en);

		$daysarr = $days_lang[$this->LangId];
		$hsarr = $hs_lang[$this->LangId];
		$minsarr = $mins_lang[$this->LangId];

		$daysleft = $dd = floor($minute / (60 * 24));
		$hoursleft = $dh = floor(($minute - $dd * (60 * 24)) / 60);
		$minsleft = $dm = $minute - $dd * (60 * 24) - $dh * 60;

		$less3hour = false;
		$fromnow = false;

		if ($txtmode == "short") {
			if ($daysleft > 0) {
				return (($less3hour && $fromnow) ? '<span style="color: red;">' : '') . sprintf("%d %s %d %s %d %s",
					$daysleft, "дн.",
					$hoursleft, "ч.",
					$minsleft, "мин.") . ($less3hour ? '</span>' : '');
			} else {
				return (($less3hour && $fromnow) ? '<span style="color: red;">' : '') . sprintf("%d %s %d %s",
					$hoursleft, "ч.",
					$minsleft, "мин.") . ($less3hour ? '</span>' : '');
			}
		}

		/*
return (($less3hour && $fromnow) ? '<span style="color: red;">' : '').sprintf("%d %s %d %s %d %s",
			$daysleft, ($daysleft < 15 ? $daysarr[$daysleft] : $daysarr[$daysleft%10]),
			$hoursleft, ($hoursleft < 15 ? $hsarr[$hoursleft] : $hsarr[$hoursleft%10]),
			$minsleft, ($minsleft < 15 ? $minsarr[$minsleft] : $minsarr[$minsleft%10])).($less3hour ? '</span>' : '');
*/

		$leftnum = $daysleft;
		$leftstr = ($daysleft < 15 ? $daysarr[$daysleft] : $daysarr[$daysleft % 10]);
		if ($daysleft == 0) {
			$leftnum = $hoursleft;
			$leftstr = ($hoursleft < 15 ? $hsarr[$hoursleft] : $hsarr[$hoursleft % 10]);

			if ($hoursleft == 0) {
				$leftnum = $minsleft;
				$leftstr = ($minsleft < 15 ? $minsarr[$minsleft] : $minsarr[$minsleft % 10]);
			}
		}

		return (($less3hour && $fromnow) ? '<span style="color: red;">' : '') . sprintf("%s %d %s", $localize->avail, $leftnum, $leftstr) . ($less3hour ? '</span>' : '');
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/******** Обновление видео счетчика *********/
	public function Set_Video_Count($uid, $num)
	{
		if (is_numeric($num)) {
			$query = "UPDATE " . TABLE_SHOP_BUYERS . " SET video_check = " . $num . " WHERE id = " . $uid;
			$its = $this->db->exec($query);
		}
		return $its;
	}

	/****** UPDATE DATE IN buyer_box *******/

	public function updDateBox($uid, $date) {
		
		$res = $this->getDateBox($uid);

		$dateNew = DateTime::createFromFormat('Y/m/d', $date);
		$dateNew->modify('+7 day');
		$dateNew = $dateNew->format('Y/m/d');

		$queryIns = "UPDATE " . TABLE_SHOP_BUYERS_BOX . "";

		if ($res[0]['prev_date'] == null) {
			$queryIns .= " SET prev_date = " . "'{$date}'";
			//$queryIns .= " WHERE buyer_id = " . $uid;
			//$this->db->exec($queryIns);

			if ($res[0]['next_date'] == null) {
				$queryIns .= ", next_date = " . "'{$dateNew}'";
				$queryIns .= " WHERE buyer_id = " . $uid;
				//print_r($queryIns);
				//die();
				$its = $this->db->exec($queryIns);
			} else {
				$queryIns .= " WHERE buyer_id = " . $uid;
				//print_r($queryIns);
				//die();
				$its = $this->db->exec($queryIns);
			}
		} else if ($res[0]['prev_date'] != null) {
			$queryIns .= " SET curr_date = " . "'{$date}'";
			$queryIns .= " WHERE buyer_id = " . $uid;
			//print_r($queryIns);
			//die();
			$its = $this->db->exec($queryIns);
		}
		$this->updDateCount($uid, $date, $dateNew);
	}

	public function updDateCount($uid, $date, $dateNew) {

		$res = $this->getDateBox($uid);

		$query_set_day = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET prev_date = "."'{$res[0]['curr_date']}'"." WHERE buyer_id = ".$uid;

		$dStart = new DateTime($res[0]['prev_date']);
		$dEnd  = new DateTime($res[0]['curr_date']);
		$dDiff = $dStart->diff($dEnd);

		$datetime2 = new DateTime($res[0]['next_date']);
		$interval = $dEnd->diff($datetime2)->format('%R%a');

		$is_portal = true;

		if($interval[0] == '-') {
			$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET curr_date = "."'{$date}'".", prev_date = "."'{$date}'".", next_date = "."'{$dateNew}'".", date_count = 0";
			$query .= " WHERE buyer_id = " . $uid;
			$this->db->exec($query);
			//$this->setMoneyBox($uid, 1, 'portal');
		}
		if($res[0]['curr_date'] == $res[0]['next_date'] && $res[0]['date_count'] == 6) {
			$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET daily_money = 10, prev_date = null, curr_date = null, next_date = null, date_count = 0";
			$query .= " WHERE buyer_id = " . $uid;
			$this->db->exec($query);
			$this->setMoneyBox($uid, 10, 'daily');
			//$this->setMoneyBox($uid, 1, 'portal');
			$this->setMoneyBox($uid, 10, 'money');
			//$this->resetMoneyBox($uid, 'repost_count');
			$is_portal = false;
			return;
		}
		if($dDiff->days == 1) {
			$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET date_count = ".($res[0]['date_count']+1);
			$query_upd = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET  daily_money = 2 WHERE buyer_id = ".$uid; //CHANGE ON ARRAY
			$query .= " WHERE buyer_id = " . $uid;
			$this->db->exec($query);
			$this->db->exec($query_upd);
			$this->db->exec($query_set_day);
			$this->setMoneyBox($uid, 2, 'daily');
			//$this->setMoneyBox($uid, 1, 'portal');
			$this->setMoneyBox($uid, 2, 'money');
			//$this->resetMoneyBox($uid, 'repost_count');
			$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET star_count = 0, comment_count = 0 WHERE buyer_id = ".$uid;
			$this->db->exec($query);
			$is_portal = false;
		}
		else {
			$query_upd = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET daily_money = 0 WHERE buyer_id = ".$uid;
			$this->db->exec($query_upd);
			$this->db->exec($query_set_day);
		}
		if($dDiff->days > 0 && $is_portal) {
			$this->setMoneyBox($uid, 1, 'portal');
			//$this->resetMoneyBox($uid, 'repost_count');
		}
		//CHANGE COSE THIS SHIT NOT WORKING CORRECTLY --- DONT FORGET
		/*if(!$res[0]['portal_check']) {
			$this->setMoneyBox($uid, 1, 'portal');
			$query = " UPDATE ".TABLE_SHOP_BUYERS_BOX." SET portal_check = 1 WHERE buyer_id = ".$uid;
			$this->db->exec($query);
		}*/

	}

	public function getDateBox($uid, $tbl='') {
		if($tbl) {
			$query = "SELECT ".addslashes($tbl)." FROM ".TABLE_SHOP_BUYERS_BOX . " WHERE buyer_id = " . addslashes($uid);
		} else
			$query = "SELECT * FROM " . TABLE_SHOP_BUYERS_BOX . " WHERE buyer_id = " . addslashes($uid);
		return $this->db->query($query);
	}

	/**************/

	/**** SET STAR RATE ****/
	public function setRatingStars($uid, $itemId, $rating, $oldRate) {
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS_STAR_RATE." WHERE item_id = ".$itemId." AND rater_id = ".$uid;
		$res = $this->db->query($query);
		if(empty($res)) {
			$query = "INSERT INTO ".TABLE_CAT_ITEMS_STAR_RATE." (item_id, rater_id, rating, add_date) VALUES (".$itemId.",".$uid.",".$rating.", NOW())";
			$this->db->exec($query);
			$this->updRatingStars($uid, $itemId, $rating, $oldRate);
			$query = "SELECT star_count FROM ".TABLE_SHOP_BUYERS_BOX." WHERE buyer_id = ".addslashes($uid);
			$ch = $this->db->query($query);
			if($ch[0]['star_count'] < 3) {
				$this->setMoneyBox($uid, 1, 'star');
				$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET star_count = star_count + 1 WHERE buyer_id = ".$uid;
				$this->db->exec($query);
				return false;
			} return true;
		} else {
			$query = "UPDATE ".TABLE_CAT_ITEMS_STAR_RATE." SET rating = ".$rating.", add_date = NOW() WHERE rater_id = ".$uid." AND item_id = ".$itemId;
			$this->db->exec($query);
			return $this->updRatingStars($uid, $itemId, $rating, $oldRate);
		}
	}

	public function updRatingStars($uid, $itemId, $rating, $oldRate) {
		$query = "SELECT COUNT(item_id) as count, SUM(rating) as sum FROM ".TABLE_CAT_ITEMS_STAR_RATE." WHERE item_id = ".$itemId;
		$res = $this->db->query($query);
		//print_r($res);

		$newRate = $res[0]['sum']/$res[0]['count'];
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET star_rate = ".$newRate." WHERE id = ".$itemId;
		$this->db->exec($query);

		return true; // Потому что идет проверка поствил ли оценку пользователь первый раз или это был апдейт
	}
	/**********************/

	/**** SET USER RATE ****/

	public function setUserItemRate($uid, $rate) {
		$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET user_rate = ".$rate." WHERE buyer_id = ".$uid;
		return $this->db->exec($query);
	}

	/**** SET MONEY BOX RATE ****/
	// Add to array boxRating[] all money rate
	public function setMoneyBox($uid, $money, $tableCol) {
		$select = "SELECT * FROM ".TABLE_SHOP_BUYERS_BOX." WHERE buyer_id = ".$uid;
		$res = $this->db->query($select);
		$query = "";

		switch($tableCol) {
			//case 'daily' : $colmn = $tableCol.'_money'; break;
			case 'star' : $colmn = $tableCol.'_money'; break;
			case 'portal' : $colmn = $tableCol.'_money'; break;
			case 'comment' : $colmn = $tableCol.'_money'; break;
			case 'repost' : if($res[0]['repost_count'] < 3) {
				$colmn = $tableCol . '_money';
				$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX. " SET ".$colmn." = ".($res[0]['repost_money']+$money).", repost_count = ".($res[0]['repost_count']+1)." WHERE buyer_id = ".$uid;
				$this->db->exec($query);
				return true;
			} else return false;
				break;
			case 'money' : 	$query = "UPDATE " . TABLE_SHOP_BUYERS_BOX . " SET money = " . ($res[0]['money'] += $money)." WHERE buyer_id = ".$uid;
				$this->db->exec($query); break;
			case 'bonus': $colmn = $tableCol.'_money'; break;
			case 'friend' : $colmn = $tableCol.'_money'; break;
			default : return false;
		}

		if(isset($colmn) && $colmn == 'portal_money') {
			$query = "UPDATE " . TABLE_SHOP_BUYERS_BOX . " SET money = " . ($res[0]['money'] += $money) . ", ".$colmn." = ".$money." WHERE buyer_id = ".$uid;
			return $this->db->exec($query);
		}
		if(isset($colmn)) {
			$query = "UPDATE " . TABLE_SHOP_BUYERS_BOX . " SET money = " . ($res[0]['money'] += $money) . ", ".$colmn." = " . ($res[0][$colmn] += $money)." WHERE buyer_id = ".$uid;
			return ($this->db->exec($query));
		}
		return false;
	}

	public function resetMoneyBox($uid, $tableCol) {
		$tableCol = $tableCol.'_money';
		if(is_numeric($uid)) {
			$box = $this->getDateBox($uid, $tableCol);
			$lc = $this->checkLocalCurrency($uid);

			if (!empty($lc))
				$query_pre = "UPDATE " . TABLE_LOCAL_CURRENCY . " SET " . addslashes($tableCol) . " = " . addslashes($tableCol) . " + " . $box[0][$tableCol] . " WHERE user_id = " . $uid;
			else
				$query_pre = "INSERT INTO " . TABLE_LOCAL_CURRENCY . "(user_id, " . $tableCol . ") VALUES(" . $uid . ", " . $box[0][$tableCol] . ")";
			$this->db->exec($query_pre);

			$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET " . addslashes($tableCol) . " = 0 WHERE buyer_id = " . $uid;
			return $this->db->exec($query);
		}
	}

	public function checkLocalCurrency($uid){
		$query = "SELECT id FROM ".TABLE_LOCAL_CURRENCY." WHERE user_id = ".addslashes($uid);
		return $this->db->query($query);
	}

	/**** SET STATUS BOX ****/
	public function setStatusBox($uid, $status) {
		$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET status = ".$status." WHERE buyer_id = ".$uid;
		return $this->db->exec($query);
	}
	/***********************/

	/**** SET POPTIP COUNT ****/

	public function setPoptipCount($res, $date, $day, $popcount) {
		if(!$res['last_date']) {
			$query = "UPDATE " . TABLE_SHOP_BUYERS . " SET poptip_check = " . ($res['poptip_check'] + 1) . ", last_date = " . "'{$date}'" . " WHERE id = " . $res['id'];
			$this->db->exec($query);
		} else {
			$dateNew = DateTime::createFromFormat('Y/m/d', str_replace('-','/',$res['last_date']));
			$dateNew->modify('+'.$day.' day');
			$dateNew = $dateNew->format('Y/m/d');

			$dStart = new DateTime($res['last_date']);
			$dEnd  = new DateTime($dateNew);
			$dDiff = $dStart->diff($dEnd);

			if($dDiff->days < $day) {
				$query = "UPDATE " . TABLE_SHOP_BUYERS . " f poptip_check = 0, last_date = " . "'{$date}'" . " WHERE id = " . $res['id'];
			} else {
				$query = "UPDATE " . TABLE_SHOP_BUYERS . " SET poptip_check = ".($res['poptip_check'] + 1)." WHERE id = " . $res['id'];
			}
			$this->db->exec($query);
		}
		if($res['poptip_check'] >= $popcount) {
			return false;
		} else return true;
	}


	/**** CHECK HELP ****/
	public function checkHelp($uid, $status, $money) {
		//$res = $this->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_ALL, "", -1, 10, PROJ_STATUS_ALL);
		$res = $this->getCountReqStatus($uid, $status);
		//print_r($res);
		if(isset($res['count'])) {
			if(is_numeric($money) && ($res['help_count'] < $res['count'])) {
				$query = "UPDATE " . TABLE_SHOP_BUYERS_BOX . " SET help_count = ".$res['count'].", help_money = help_money + " . (($res['count']-$res['help_count']) * $money) ." WHERE buyer_id = ".$uid;
				$this->db->exec($query);
				$this->setMoneyBox($uid, (($res['count']-$res['help_count']) * $money), 'money');
			}
		} return false;
	}

	public function getCountReqStatus($uid, $status) {
		if(is_numeric($uid) && is_numeric($status)) {
			$query = "SELECT count(hp.req_status) as count, bx.help_count FROM " . TABLE_CAT_ITEMS_HELPREQ ." as hp"
				." JOIN ".TABLE_SHOP_BUYERS_BOX." as bx ON hp.sender_id = bx.buyer_id"
				." WHERE hp.req_status = ".$status." AND hp.sender_id = ".$uid." AND hp.is_promise = ".REQ_NOPROMISE;
			return $this->db->query($query)[0];
		}
		return false;
	}

	/**** SET COMMET_MONEY ****/

	public function setCommentMoney($uid, $itid, $money) {
		$res = $this->getAutorComment($uid, $itid);
		$box = $this->getDateBox($uid);

		if($res['count'] && $box[0]['comment_count'] <= 11) {
			$this->setMoneyBox($uid, $money, 'comment');
			$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET comment_count = comment_count + 1 WHERE buyer_id = ".addslashes($uid);
			$this->db->exec($query);
			return 1;
		}
		return 0;
	}

	/***** SET DAILY MONEY *****/

	public function setDailyWeekMoney($id, $sum) {
		$res = $this->getUserBox($id);
		$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET money_day = ".( $res['money_day']+=$sum ).", money_week = ".( $res['money_week']+=$sum )." WHERE buyer_id = ".$id;
		$this->db->exec($query);
	}

	/**** GET USER/ITEM INFO ****/

	public function getAutorComment($uid, $itid) {
		if(is_numeric($uid) && is_numeric($itid)) {
			$query = "SELECT count(*) as count FROM " . TABLE_CAT_ITEMS_COMMENT . " WHERE author_id = " . $uid." AND item_id = ".$itid;
			return $this->db->query($query)[0];
		} return false;
	}

	public function geStarCounter($pid) {
		if(is_numeric($pid)) {
			$query = "SELECT count(item_id) as count FROM " . TABLE_CAT_ITEMS_STAR_RATE . " WHERE item_id = " . $pid;
			return $this->db->query($query)[0];
		} else return false;

	}

	public function getLastDream($uid) {
		$query = "
		SELECT tbl.item_id, i1.id FROM ".TABLE_CAT_CATITEMS." as tbl
		JOIN ".TABLE_CAT_ITEMS." AS i1 ON tbl.item_id = i1.id
		WHERE i1.status = 0 AND sect_id = 76 AND i1.moderated = 1 ORDER BY tbl.id DESC LIMIT 0, 1
		";
		$res = $this->db->query($query)[0]['item_id'];

		if(isset($res) && is_numeric($uid)) {
			$query = "SELECT item_id FROM " . TABLE_CAT_ITEMS_STAR_RATE . " WHERE item_id = " . $res . " AND rater_id = " . $uid;
			$ch = $this->db->query($query);
			//print_r($ch);
			if(empty($ch))
				return $res;
			else return false;
		}
		return false;
	}

	public function getUserItemPics($item_id) {
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS_PICS." WHERE item_id = ".$item_id;
		return $this->db->query($query);
	}

	public function getUserBox($id) {
		if(is_numeric($id)) {
			$query = "SELECT * FROM " . TABLE_SHOP_BUYERS_BOX . " WHERE buyer_id = " . $id;
			$res = $this->db->query($query);
			return $res[0];
		}
		else
			return false;
	}

	/**** SET VIDEO VIEW TO ITEM_ID ****/

	public function checkVideoView($uid, $item_id) {
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS_REQ_VIDEO." WHERE user_watch_id = ".$uid." AND item_id = ".$item_id;
		$res = $this->db->query($query);

		if(empty($res)) {
			$query = "INSERT INTO ".TABLE_CAT_ITEMS_REQ_VIDEO."(item_id, user_watch_id) VALUES(".$item_id.", ".$uid.")";
			$this->db->exec($query);
			return false;
		} return true;
	}

	/* GET POPULAR USER FOR MAIN PAGE */

	public function getUserWithAngStat() {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE angel_day = 1 OR angel_week = 1 LIMIT 0, 10";
		return $this->db->query($query);
	}

	/* GET POPULAR PROJECT FOR MAIN PAGE */

	public function getPopularProject() {
		$query = "SELECT i1.id, i1.title, ip1.filename_ico, ip1.filename_thumb FROM ".TABLE_CAT_ITEMS." as i1 
				  JOIN ".TABLE_CAT_ITEMS_PICS." as ip1 ON i1.`id` = ip1.item_id
				  WHERE archive = 0 AND status = 0 GROUP BY i1.id  ORDER BY item_priority_rate DESC LIMIT 0, 10";
		return $this->db->query($query);
	}

	/* GET MOST VIEWED PROJECT */

	public function getViewedProject($id, $l1 = 0, $l2 = 6) {
		$query = "SELECT sect_id FROM ".TABLE_CAT_CATITEMS." WHERE item_id =  ".$id;
		$res = $this->db->query($query);
		if(!empty($res)) {
			$query = "SELECT i1.*, p1.filename_thumb, p1.filename_ico, p1.filename_big FROM ".TABLE_CAT_ITEMS." as i1 
			JOIN ".TABLE_CAT_CATITEMS." as s1 ON s1.item_id = i1.id
			JOIN ".TABLE_CAT_ITEMS_PICS." as p1 ON p1.item_id = i1.id
			WHERE s1.sect_id = ".$res[0]['sect_id']." AND status = 0 AND archive = 0 GROUP BY i1.id ORDER BY i1.item_rate DESC LIMIT ".$l1.", ".$l2;
			return $this->db->query($query);
		} else return false;
	}

	/* SMART CATALOG SYSTEM */

	public function sm_Item_List($profid = -1, $sid, $uid = 0, $pi = -1, $pn = 20, $slev = 3, $sortby = "", $proj_status = PROJ_STATUS_ALL, $fltby = "", $cityby = "", $onlyactive = true, $coId = -1, $oblId = 0, $currUsrID = 0, $loc_reg = 0, $loc_cou = 0, $is_play = 1, $moder = 1, $noLoc = 0, $help_type = 0, $rotation = 1) {
		$cond = "";
		if ($uid != 0)
			$cond .= " AND i1.author_id='$uid' ";

		if ($proj_status === 'success') {
			$cond .= " AND i1.is_success = 1 OR (((amount_done*100)/amount) > 60)";
		}
		else if ($proj_status != PROJ_STATUS_ALL)
			$cond .= " AND i1.status='$proj_status' ";

		if (is_array($profid) && (count($profid) > 1)) {
			$cond .= " AND i1.profile_id IN (" . implode(",", $profid) . ") ";
		} else if ($profid != -1)
			$cond .= " AND i1.profile_id='" . $profid . "' ";

		$sect_cond = "";
		if ($sid != 0)
			$sect_cond = " AND cc1.sect_id='$sid' ";

		if (($cityby != 0) && is_numeric($cityby))
			$cond .= " AND i1.city_id='" . addslashes($cityby) . "' ";

		if( $oblId != 0 && is_numeric($oblId))
			$cond .= " AND i1.obl_id='" . addslashes($oblId) . "' ";

		$sort_cond = "i1.item_priority_rate DESC, i1.item_rate DESC, i1.add_date DESC ";

		$limit_cond = "";
		if ($pi >= 0)
			$limit_cond = " LIMIT " . ($pi * $pn) . ",$pn ";

		$or1 = "";
		if($currUsrID != 0) {
			$or1 = " cc1.sect_id IN ( 
			SELECT sect_id FROM  ".TABLE_SHOP_BUYER_SECTS."
			WHERE  `buyer_id` = ".$currUsrID."
			) DESC , ";
		}

		$or3 = "";
		if($loc_reg > 0) {
			$or3 = " i1.obl_id = ".addslashes($loc_reg)." DESC , ";
		}

		$or2 = "";
		if($loc_cou > -1 && $or3 == '') {
			$or2 = " i1.obl_id IN ( 
			SELECT id FROM ".TABLE_REGION."  
			WHERE country_id = ".addslashes($loc_cou)."
			) DESC , ";
		}

		$play_cond = '';
		if($is_play) {
			//$play_cond = " AND i1.is_play = 1";
		}

		$moder_cond = "";
		if($moder != -1) {
			//$moder_cond = " AND i1.moderated = ".$moder;
		}

		$noLoc_cond = "";
		if(is_array($noLoc)) {
			if(isset($noLoc['country_id']) && isset($noLoc['reg_id']) && $noLoc['country_id'] > -1 && $noLoc['reg_id'] > 0) {
				$subCity = "SELECT obl_id obl_f, (SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".addslashes($noLoc['country_id'])." GROUP BY obl_id DESC LIMIT 1) obl_l FROM ".TABLE_CITY." WHERE country_id = ".addslashes($noLoc['country_id'])." GROUP BY obl_id ASC LIMIT 1";
				//echo $subCity; die();//
				$obl_loc = $this->db->query($subCity);
				$obl_loc = isset($obl_loc[0]) ? $obl_loc[0] : 0;

				// SELECT OBL_ID IN USER COUNTRY AND DONT SHOW ITEM WHICH SET LOCATION TYPE = 1

				$subCity = "SELECT id FROM ".TABLE_CAT_ITEMS." WHERE (obl_id BETWEEN ".$obl_loc['obl_f']." AND ".$obl_loc['obl_l']." AND location_type = ".ITEM_LOCATION_COUNTRY.") OR (obl_id <> ".$noLoc['reg_id']." AND location_type = ".ITEM_LOCATION_REGION.") ";
				$noLoc_itm = $this->db->query($subCity);

				if(!empty($noLoc_itm)) {
					foreach ($noLoc_itm as $itmid) {
						$noLoc_cond .= $itmid['id'].',';
					}
					$noLoc_cond = " AND i1.id NOT IN( ".rtrim($noLoc_cond, ',').' ) ';
				}

			}
		}

		switch ($sortby) {
			case "add":
			case "new":
				$sort_cond = " i1.add_date DESC ";
				break;

			case "popular":
				$sort_cond = " i1.item_rate DESC ";
				break;

			case "ending":
				$sort_cond = " i1.end_date ";
				$cond .= " AND i1.end_date>NOW() ";
				break;
		}

		switch ($fltby) {
			case "money":
				$cond .= " AND i1.amount_type='" . PROJ_TYPE_MONEY . "' ";
				break;

			case "work":
				$cond .= " AND i1.amount_type<>'" . PROJ_TYPE_MONEY . "' ";
				break;

			case "easy":
				$help_type = 1;
				break;

			case "offers":
				$cond .= " AND i1.amount_type = ".PROJ_TYPE_MONEY." AND i1.is_arbitr = 1 ";
		}

		$or4 = "";
		if($rotation == 1 && $pi == 0 && $fltby == 'money' && $sortby == '') {

			$query = "SELECT id FROM " . TABLE_CAT_ITEMS . " i1 WHERE 1 ".$cond.$play_cond.$noLoc_cond." AND i1.help_type = ".addslashes($help_type)." AND i1.moderated = $moder ".
				" AND i1.cat_item_type = 0 ORDER BY i1.cat_view_rate DESC LIMIT 0, " . CAT_LIMIT_NEW_PROJ_ROTATION;

			//echo $query;
			
			$rotatiton_pids = $this->db->query($query);

			//echo $query; die();

			if (!empty($rotatiton_pids)) {

				foreach ($rotatiton_pids as $r_pid) {
					$or4 .= $r_pid['id'].',';
				}

				$or4 = " i1.id IN( ".rtrim($or4, ',').' ) ';
				//$cond = " AND (i1.cat_item_type = 0 AND ".$or4.") OR (i1.cat_item_type <> 0) ";
				$or4 .= "DESC, ";
				//$limit_cond = " LIMIT 3, " . (($pn - CAT_LIMIT_NEW_PROJ_ROTATION) < 0 ? 0 : ($pn - CAT_LIMIT_NEW_PROJ_ROTATION));
			}
		}

		if($coId > -1 && $oblId == 0 && $cityby == 0 && is_numeric($coId)) {
			$subCity = "SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".addslashes($coId)." GROUP BY obl_id ASC LIMIT 1";
			$obl[0] = $this->db->query($subCity)[0];
			$subCity = "SELECT obl_id FROM ".TABLE_CITY." WHERE country_id = ".addslashes($coId)." GROUP BY obl_id DESC LIMIT 1";
			$obl[1] = $this->db->query($subCity)[0];

			$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, c1.id as sectid, c1.url as secturl, c2.name as sectname,
						u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
						DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
						DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
						TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
						FROM " . TABLE_CAT_ITEMS . " i1
						INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
						INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id $sect_cond
						INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
						INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
						INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
						WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "").$cond.$play_cond.$noLoc_cond." AND i1.help_type = ".addslashes($help_type)." AND i1.obl_id BETWEEN ".$obl[0]['obl_id']." $moder_cond AND ".$obl[1]['obl_id']." AND i1.moderated = $moder GROUP BY i1.id ORDER BY ".$or1.$or3.$or2." i1.item_priority_rate DESC ".$limit_cond;
		}
		else {
			$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, c1.id as sectid, c1.url as secturl, c2.name as sectname,
u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id $sect_cond
INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "") . "$cond $play_cond $noLoc_cond AND i1.help_type = ".addslashes($help_type)." AND i1.moderated = $moder
ORDER BY ".$or4.$or1.$or3.$or2." $sort_cond
$limit_cond";
		}

		$query_part = "";
		if ($slev == 2)    // The sid is the parent section for project sections
		{
			$query_part = " INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON c1.parent_id=cp1.id AND cp1.id='$sid' ";
		} else if ($slev == 1) {
			$query_part = " INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp2 ON c1.parent_id=cp2.id
INNER JOIN " . TABLE_CAT_CATALOG . " cp1 ON cp2.parent_id=cp1.id AND cp1.id='$sid' ";
		}

		//print_r($obl); die();

		if ($query_part != "") {
			if(isset($obl))
				$oblq = " AND i1.obl_id BETWEEN ".$obl[0]['obl_id']." AND ".$obl[1]['obl_id'];

			$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, c1.id as sectid, c1.url as secturl, c2.name as sectname,
u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
FROM " . TABLE_CAT_ITEMS . " i1
INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=" . $this->LangId . "
INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id
$query_part
INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=" . $this->LangId . "
WHERE i1.archive=0 " . ($onlyactive ? " AND i1.active=1 " : "") . " $cond $play_cond $noLoc_cond AND i1.help_type = ".addslashes($help_type)." ".(isset($oblq) ? $oblq : '')." AND i1.moderated = $moder
ORDER BY ".$or4.$or1.$or3.$or2." $sort_cond
$limit_cond";
		}
		//echo $query; die();
		$res = $this->db->query($query);
		return $res;
	}

	/**** GET SUCCESS PROJECT ****/

	public function getSuccessProj($uid, $l1=0, $l2=1) {
		$query = "SELECT i1.id FROM ".TABLE_CAT_ITEMS." i1 WHERE amount_donepercent >= 99.99 AND author_id = ".addslashes($uid)." AND is_success = 0 LIMIT ".addslashes($l1).", ".addslashes($l2);
		return $this->db->query($query);
	}

	/**** GET COMMENT TO POPTIP ****/

	public function getPoptipItemComment($id) {
		$query = "SELECT count(c1.id) as count, c1.item_id 
							FROM ".TABLE_CAT_ITEMS_COMMENT." c1
							WHERE is_read = 0 AND c1.item_id IN (SELECT id FROM ".TABLE_CAT_ITEMS." WHERE author_id = ".addslashes($id).") ORDER BY item_id";
		return $this->db->query($query);
	}

	/**** RESET COMMENT FROM POPTIP ****/

	public function resetPoptipItemComment($itemId) {
		$query = "UPDATE ".TABLE_CAT_ITEMS_COMMENT." SET is_read = 1 WHERE item_id = ".addslashes($itemId);
		return $this->db->exec($query);
	}

	/**** SEND MAIL MESSAGE TO  ****/

	protected function getUserEmail_ct($id) {
		$query = "SELECT email FROM ".TABLE_SHOP_BUYERS."
							WHERE id = ".addslashes($id);
		$res = $this->db->query($query);

		if (count($res) > 0) {
			return $res[0]['email'];
		} return 0;
	}

	public function sendUserMailMsg_ct($uid, $subject, $txt, $shar=1, $unsub = 0) {

		$tomail = $this->getUserEmail_ct($uid);

		if (!$tomail)
			return;

		$shtext = '<br>Не забывайте подписываться на наши социальные сети <a href="https://vk.com/wayofhelp">ВК</a>, <a href="https://www.facebook.com/WayofHelp">ФБ</a> и <a href="https://ok.ru/group/53895621509237">ОК</a>';
		$test = ($shar == 1? $shtext : "");
		$support = '<br>'.$txt.'<br><br>Служба поддержки <a href="'.WWWHOST.'" >WayOfHelp</a>
		'.$test.'';
		$main_text = '
		<br><br><br>
		<p style="color: #ababab;">Вы получили это письмо, потому что подписались<br>
		на новости от сайта <a href="https://wayofhelp.com">WayOfHelp</a>.<br><br>

		Вы всегда можете <a href="'.WWWHOST.'users/unsubscribe/?uid='.$uid.'">отказаться</a> от получения наших<br>
		писем, для этого просто перейдите по ссылке</p>';

		if($unsub == 0) {
			UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $support . $main_text);
			UhCmsApp::getMailerInstance()->sendHTMLmail('qwe100com@mail.ru', $subject, $support . $main_text);
		}
	}

	/**** GET ITEM FROM GAME-MONEY PROJECT ****/

	public function getItemGame($date = 0, $moder = 1) {
		$date_cond = "";
		if($date) {
			if($date === 'now') {
				/*$query = "SELECT g1. * , g1.id item_id, b1.pic_sm AS user_pic, b1.name, b1.fname, b1.account_type, b1.orgname, l1.descr, p1.filename_ico AS pic, (
						c1.portal_money + c1.daily_money + c1.star_money + c1.help_money + c1.comment_money + c1.repost_money + c1.ad_money + c1.aw_money + c1.bonus_money + c1.admin_money
						) AS lc_count,  pg1.pays_send pays_send_all
						FROM  `jhlp_cat_item` g1
						LEFT JOIN jhlp_cat_item_pics p1 ON g1.id = p1.item_id
						JOIN jhlp_cat_buyer b1 ON b1.id = g1.author_id
						JOIN jhlp_cat_item_lang l1 ON l1.item_id = g1.id
						JOIN jhlp_local_currency c1 ON c1.user_id = g1.author_id
						JOIN jhlp_pays_game pg1 ON pg1.proj_id = g1.id
						WHERE g1.status = 0
						AND g1.is_play = 1
						AND g1.amount_type = 0
						GROUP BY g1.id
						ORDER BY lc_count DESC ";*/
				$query = "SELECT g1. * , g1.id item_id, b1.pic_sm AS user_pic, b1.name, b1.fname, b1.account_type, b1.orgname, l1.descr, p1.filename_ico AS pic, (
						c1.portal_money + c1.daily_money + c1.star_money + c1.help_money + c1.comment_money + c1.repost_money + c1.ad_money + c1.aw_money + c1.bonus_money + c1.admin_money
						) AS lc_count,  pg1.pays_send pays_send_all, (SELECT SUM( req_amount ) 
FROM  `jhlp_cat_requests` 
WHERE  `item_id` = g1.id
AND  `sender_id` =1992) pays_all, l1.title2
						FROM  `jhlp_cat_item` g1
						LEFT JOIN jhlp_cat_item_pics p1 ON g1.id = p1.item_id
						JOIN jhlp_cat_buyer b1 ON b1.id = g1.author_id
						JOIN jhlp_cat_item_lang l1 ON l1.item_id = g1.id
						JOIN jhlp_local_currency c1 ON c1.user_id = g1.author_id
						JOIN jhlp_pays_game pg1 ON pg1.proj_id = g1.id
						WHERE g1.status =0
						AND g1.is_play =1
						AND g1.amount_type =0
						AND g1.moderated = $moder
				
			
						GROUP BY b1.id
						ORDER BY lc_count DESC ";
				return $this->db->query($query);
			}
			else
				$date_cond = " AND d1.date LIKE '".addslashes($date)."%'";
		} else
			$date_cond = " AND DAY(d1.date) = DAY(NOW()) ";

		$query = "SELECT g1. * , g1.id item_id, d1.pays_send, d1.pays_coef, d1.local_currency, b1.pic_sm AS user_pic, b1.name, b1.fname, b1.account_type, b1.orgname, l1.descr, p1.filename_ico AS pic, (d1.date - INTERVAL 1 DAY) date, pg1.pays_send pays_send_all
					, (SELECT SUM( req_amount ) 
FROM  `jhlp_cat_requests` 
WHERE  `item_id` = g1.id
AND  `sender_id` =1992) pays_all, l1.title2
					FROM  `jhlp_cat_item` g1
					LEFT JOIN jhlp_cat_item_pics p1 ON g1.id = p1.item_id
					JOIN jhlp_cat_buyer b1 ON b1.id = g1.author_id
					JOIN jhlp_cat_item_lang l1 ON l1.item_id = g1.id
					JOIN jhlp_local_currency c1 ON c1.user_id = g1.author_id
					LEFT JOIN jhlp_pays_game_dump d1 ON d1.proj_id = g1.id
					JOIN jhlp_pays_game pg1 ON pg1.proj_id = d1.proj_id
					WHERE g1.status =0
					AND g1.is_play =1
					AND g1.moderated = $moder
			
					AND g1.amount_type =0
					".$date_cond."
					GROUP BY b1.id
					ORDER BY d1.local_currency DESC";
		$res = $this->db->query($query);

		/*foreach ($res as $k => $item) {
			$res[$k]['lc_count'] = $item['portal_money']+
													$item['daily_money']+
													$item['star_money']+
													$item['help_money']+
													$item['comment_money']+
													$item['repost_money']+
													$item['ad_money']+
													$item['aw_money']+
													$item['bonus_money']+
													$item['admin_money'];
		}

		function cmp($a, $b) {
			return $b['lc_count'] - $a['lc_count'];
		}

		usort($res, "cmp");*/
		return $res;
	}

	public function getItemGameCount() {
		//$query = "SELECT($)";
	}

	/**** CHECK IF USER PAY FOR OWN PROJECT ****/

	public function checkUserPayProj($uid, $projid) {
		if(is_numeric($uid) && is_numeric($projid)) {
			$query = "SELECT * FROM " . TABLE_PAYS_CURRENCY . " WHERE user_id = " . addslashes($uid) . " AND proj_id = " . addslashes($projid);
			return $this->db->query($query);
		} return false;
	}

	/**** SHOW/HIDE POPUP ****/

	public function checkShowPopup($uid, $val=1) {
		if($val) {
			if (is_numeric($uid)) {
				$query = "SELECT show_popup FROM " . TABLE_SHOP_BUYERS . " WHERE id = " . addslashes($uid);
				return $this->db->query($query);
			} return false;
		} else if($val == 0){
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET show_popup = ".addslashes($val)." WHERE id = ".addslashes($uid);
			return $this->db->exec($query);
		}
	}

	/**** CHECK WOH PAYS TO PROJ ****/

	public function  checkWohPays($projid) {
		$query = "SELECT add_date FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE `item_id` = ".addslashes($projid)." AND `sender_id` = ".FROM_WOH_ID." AND `req_type` = ".PROJ_TYPE_MONEY." ORDER BY add_date DESC LIMIT 0, 1";
		$subq = $this->db->query($query);

		if(!empty($subq)) {
			$query = "SELECT sum(r1.req_amount) as req_amount, b1.*, r1.req_type, r1.sender_id, DATEDIFF(NOW(), '".$subq[0]['add_date']."') as daysbefore  FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
			JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = r1.sender_id
			WHERE `item_id` = ".addslashes($projid)." AND `sender_id` = ".FROM_WOH_ID." AND `req_type` = ".PROJ_TYPE_MONEY;

			return $this->db->query($query);
		}
	}

	public function getGameDateAll() {
		$query = "SELECT date(`date`) as date FROM ".TABLE_PAYS_GAME_DUMP." WHERE 1
					GROUP BY DAY(`date`) ORDER BY date DESC";
		return $this->db->query($query);
	}

	public function getUserPaysGameDumpDate($uid, $date = 'now') {
		$date_cond = "";
		if($date === 'now')
			$date_cond = " AND DATE(date) = DATE(NOW()) ";
		else
			$date_cond = " AND DATE(date) = DATE(".addslashes($date).") ";

		$query = "SELECT d1.*, g1.is_active FROM ".TABLE_PAYS_GAME_DUMP." d1
		 JOIN ".TABLE_PAYS_GAME." g1 ON g1.proj_id = d1.proj_id
		 WHERE d1.is_check = 0 AND d1.user_id = ".addslashes($uid).$date_cond;
		return $this->db->query($query);
	}

	public function setUserPaysGameCheck($uid, $date = 'now') {
		$date_cond = "";
		if($date === 'now')
			$date_cond = " AND date = DATE(NOW()) ";

		$query = "UPDATE ".TABLE_PAYS_GAME_DUMP." SET is_check = 1 WHERE user_id = ".addslashes($uid).$date_cond;
		return $this->db->exec($query);
	}

	public function getTestQuestions() {
		$query = "SELECT * FROM ".TABLE_PROJ_TEST." WHERE 1";
		return $this->db->query($query);
	}

	public function getTestSubQuestions($id) {
		$query = "SELECT * FROM ".TABLE_PROJ_TEST_SUB." WHERE q_id = ".addslashes($id);
		return $this->db->query($query);
	}

	public function checkPromisePays($projid, $uid, $prom = REQ_PROMISE, $promType = REQ_PRIMISE_NOPAY) {
		$query = "SELECT SUM(req_amount) sum FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE `item_id` = ".addslashes($projid)." AND `sender_id` = ".addslashes($uid)." AND `is_promise` = ".addslashes($prom)." AND `promise_pay` = ".addslashes($promType)." AND `req_type` = ".REQ_TYPE_MONEY;
		return $this->db->query($query);
	}

	/**** ADD USER BOX MONEY ****/

	public function updUserMoney($uid, $money = 0) {
		if(is_numeric($uid) && is_numeric($money)) {
			$query = "UPDATE " . TABLE_SHOP_BUYERS_BOX . " SET money = money + ".addslashes($money).", help_money = help_money + ".addslashes($money)." WHERE buyer_id = ".addslashes($uid);
			return $this->db->exec($query);
		} else return false;
	}

	public function getPaymentList($lang = 1) {
		$query = "SELECT * FROM ".TABLE_ITEM_PAYMENT." WHERE lang = ".addslashes($lang)." AND payment_id <> 10 ";
		return $this->db->query($query);
	}

	public function getPaymentProjList($pid) {
		$query = "SELECT ipl1.*, ip1.name FROM ".TABLE_ITEM_PAYMENT_LIST." ipl1
		 		  	JOIN ".TABLE_ITEM_PAYMENT." ip1 ON ip1.payment_id = ipl1.payment_id WHERE item_id = ".addslashes($pid)." ORDER BY ipl1.payment_id ASC";
		return $this->db->query($query);
	}

	/**** GET HELPREQ ****/
	public function getHelpreq($uid)
	{
		$query = "SELECT h1.item_id as count_help, i1.author_id, h1.add_date as sender_date FROM " . TABLE_CAT_ITEMS_HELPREQ . " h1  
INNER JOIN " . TABLE_CAT_ITEMS. " i1 ON h1.item_id=i1.id 
WHERE h1.req_status=1 AND h1.sender_id=$uid
GROUP BY h1.item_id";

		$res = $this->db->query($query);

		return $res;
	}

	/**** GET NAME AND SURNAME ****/

	public function getName($uid)
	{
		$query = "SELECT name, fname FROM ".TABLE_SHOP_BUYERS." WHERE id=".$uid."";

		$res = $this->db->query($query);

		return $res;
	}

	/**** GET HELPERS HELP ****/

	public function getHelpersInfo($uid)
	{
		$query = "SELECT COUNT(DISTINCT item_id) as count, sender_id FROM `jhlp_cat_requests` WHERE `sender_id`=".$uid." AND `req_status`=1";

		$res = $this->db->query($query);

		return $res;
	}

	/**** GET SUCCESSFULL ITEM ****/

	public function getSuccessItem(){
		$query ="SELECT i1.*, i2.title2, i2.descr0 as descr, c1.id as sectid, c1.parent_id, c1.url as secturl, c2.name as sectname,
		u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
		DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
		DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt, DATE_FORMAT(i1.stop_date, '%m') as stdat, DATE_FORMAT(i1.start_date, '%Y') as stdat2,
		TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall, 
		(SELECT SUM(req_amount) 
		FROM ".TABLE_CAT_ITEMS_HELPREQ." 
		WHERE item_id = i1.id AND req_type = ".REQ_TYPE_MONEY." AND req_status = ". REQ_STATUS_CONFIRM .")sum, p1.filename_thumb, b1.user_rate,
		(SELECT COUNT(DISTINCT sender_id) FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE req_status = ".REQ_STATUS_CONFIRM." AND item_id = i1.id) helpcount
		FROM " . TABLE_CAT_ITEMS . " i1
		INNER JOIN " . TABLE_CAT_ITEMS_LANGS . " i2 ON i1.id=i2.item_id AND i2.lang_id=1
		INNER JOIN " . TABLE_CAT_CATITEMS . " cc1 ON i1.id=cc1.item_id 
		INNER JOIN " . TABLE_SHOP_BUYERS . " u1 ON i1.author_id=u1.id
		INNER JOIN " . TABLE_CAT_CATALOG . " c1 ON cc1.sect_id=c1.id
		INNER JOIN " . TABLE_CAT_CATALOG_LANGS . " c2 ON c1.id=c2.sect_id AND c2.lang_id=1
		INNER JOIN ".TABLE_CAT_ITEMS_PICS." p1 ON i1.id = p1.item_id
		INNER JOIN ".TABLE_SHOP_BUYERS_BOX." b1 ON i1.author_id = b1.buyer_id
		WHERE i1.archive = 0 AND i1.active = 1 AND i1.stop_date is not null
		GROUP BY i1.id 
		HAVING((sum/i1.amount)*100) > 60 OR i1.is_success = 1
		ORDER BY stop_date DESC ";

		$res = $this->db->query($query);

		return $res;
	}

	// GET SUM REQUEST AMOUNT, IF ITEM TYPE MONEY AND COUNT REQUEST, IF TYPE OTHER

	public function getProjReq($pid) {
		$query = "SELECT amount_type FROM ".TABLE_CAT_ITEMS." WHERE id = ".addslashes($pid);
		$type = $this->db->query($query);
		$pinfo = [];

		if(!empty($type)) {
			$type = $type[0]['amount_type'];

			if($type == PROJ_TYPE_MONEY) {
				$query = "SELECT SUM(r1.req_amount) req, amount, i1.status FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
				 			JOIN ". TABLE_CAT_ITEMS ." i1 ON i1.id = r1.item_id WHERE r1.item_id = " . addslashes($pid);
			} else {
				$query = "SELECT COUNT(r1.id) req, amount, i1.status FROM " . TABLE_CAT_ITEMS_HELPREQ . " r1
				 			JOIN ". TABLE_CAT_ITEMS ." i1 ON i1.id = r1.item_id WHERE r1.item_id = " . addslashes($pid);
			}
			$res = $this->db->query($query);
			$pinfo['type'] = $type;
			$pinfo['req'] = $res[0]['req'];
			$pinfo['amount'] = $res[0]['amount'];
			$pinfo['status'] = $res[0]['status'];

			return $pinfo;
		} return false;

	}

	//RETURN USERS AVATAR

	public function getUserPic($uid) {
		$query = "SELECT pic, pic_sm FROM ".TABLE_SHOP_BUYERS." WHERE id = ".addslashes($uid);
		return $this->db->query($query);
	}

	public function getDiffProjDate($uid) {
		$query = "SELECT id, end_date, title FROM ".TABLE_CAT_ITEMS." WHERE status = ".PROJ_STATUS_RUN." AND active = 1 AND archive = 0 AND DATEDIFF(end_date, CURDATE()) > 0 AND DATEDIFF(end_date, CURDATE()) < 10 AND author_id = ".addslashes($uid);
		return $this->db->query($query);
	}

	//CHECK IF USER NEED TO PAY PROMISE AND IF HE PAYED THIS SHIT

	public function checkPromiseMessSend($uid, $pid, $pay = REQ_PRIMISE_NOPAY) {
		$query = "SELECT * FROM ".TABLE_PAYS_PROMISE." WHERE user_id = ".addslashes($uid)." AND item_id = ".addslashes($pid)." AND is_pay =  ".addslashes($pay);
		return $this->db->query($query);
	}

	public function getBuyerReqRate($reqid) {
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS_HELPREQ_RATE." WHERE req_id = ".addslashes($reqid);
		return $this->db->query($query);
	}

	public function setReqComment($reqid, $uid, $comm, $rate = 10) {
		if(is_numeric($reqid)) {
			$query = "SELECT * FROM " . TABLE_CAT_ITEMS_HELPREQ_RATE . " WHERE req_id = " . addslashes($reqid);
			$res = $this->db->query($query);

			if(empty($res)) {
				$query = "SELECT r1.*, i1.author_id FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
				JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
				WHERE r1.id = ".addslashes($reqid);
				$citms = $this->db->query($query);

				if(empty($citms)) {
					return;
				}

				$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ_RATE." (`id`, `item_id`, `req_id`, `author_id`, `user_id`, `rate`, `add_date`, `rate_comments`) 
							VALUES (NULL, '".$citms[0]['item_id']."', '".addslashes($reqid)."', '".$uid."', '".$citms[0]['author_id']."', '".addslashes($rate)."', NOW(), '".addslashes($comm)."')";
			} else {
				$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ_RATE." SET rate = ".addslashes($rate).", rate_comments = '".addslashes($comm)."' WHERE req_id = ".addslashes($reqid).' AND user_id = '.addslashes($uid);
			}

			return $this->db->exec($query);
		}

	}

	public function getBuyerReqNum($uid, $status = 1, $type = -10, $is_prom = -10, $prom_pay = -10, $show = -10 ) {
		$type_cond = '';
		$prom_cond = '';

		if($type != -10)
			$type_cond = " AND req_type = ".addslashes($type);

		if($is_prom != -10)
			$prom_cond = " AND is_promise = ".addslashes($is_prom).($prom_pay != -10 ? '' : ' AND promise_pay = '.$prom_pay.($show != -10 ? '' : ' AND `show` = '.addslashes($show)));

		$query = "SELECT COUNT(*) as hlpco FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE sender_id = ".addslashes($uid)." AND req_status = ".addslashes($status).$prom_cond.$type_cond;
		return $this->db->query($query);
	}

	public function getCronsQuery($next = 0) {
		if(!$next) {
			$query = "SELECT * FROM " . TABLE_CRONS_QUERY . " WHERE 1";
			return $this->db->query($query);
		} else {
			$query = "UPDATE ".TABLE_CRONS_QUERY." SET crons_next = ".addslashes($next);
			return $this->db->exec($query);
		}
	}

	// GET AND SET POPUP WHICH LOAD IN SOME PAGE

	public function getPagesPopUp($page) {
		$query = "SELECT * FROM ".TABLE_PAGES_POPUP." WHERE page = '".addslashes($page)."'";
		return $this->db->query($query);
	}

	public function setPagesPopUp($page, $next = 1) {
		$query = "UPDATE ".TABLE_PAGES_POPUP." SET popup_cur = ".addslashes($next)." WHERE page = '".addslashes($page)."'";
		echo $query;
		return $this->db->exec($query);
	}

	public function setPopUpInfo($id, $uid = 0, $date = '') {
		if($date) {
			$date_cond = ", ".addslashes($date)." ";
		} else {
			$date_cond = ", NOW() ";
		}
		$query = "INSERT INTO ".TABLE_PAGES_POPUP_INFO."(popup_id, data) VALUES(".addslashes($id).$date_cond.")";
		return $this->db->exec($query);
	}

	public function msgPulseMail($uid, $subject, $txt) {
		if(is_numeric($uid)) {
			$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id=" . addslashes($uid);
			$res = $this->db->query($query);
		} else {
			$res[0]['email'] = $uid;
			$res[0]['name'] = $res[0]['fname'] = '';
		}

		$html = '<hr><br>'.$txt.'<br><br><hr>Служба поддержки <a href="'.WWWHOST.'" >WayOfHelp</a>
		<br>Не забывайте подписываться на наши социальные сети <a href="https://vk.com/wayofhelp">ВК</a>, <a href="https://www.facebook.com/WayofHelp">ФБ</a> и <a href="https://ok.ru/group/53895621509237">ОК</a>';

		if (count($res) > 0) {
			$tomail = $res[0]['email'];
			$name = $res[0]['name']+' '+$res[0]['fname'];
		}


		if( $tomail == "" )
			return;

		$text = strip_tags($html);
		$SPApiProxy = new SendpulseApi( SENDPULSE_ID, SENDPULSE_KEY, 'file' );
		$email = array(
			'html'    => $html,
			'text'    => $text,
			'subject' => $subject,
			'from'    => array(
				'name'  => 'WayOfHelp',
				'email' => 'info@wayofhelp.com'
			),
			'to'      => array(
				array(
					'name'  => $name,
					'email' => $tomail
				)
			)
		);
		$SPApiProxy->smtpSendMail( $email );
	}

	// ADD ITEMS WHICH PAYED 10 percent

	public function addWohPayedItem($uid) {
		$query = "SELECT item_id FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE sender_id = ".addslashes($uid)." 
		AND is_promise = ".REQ_PROMISE." AND promise_pay = ".REQ_PRIMISE_NOPAY." AND item_id NOT IN(SELECT item_id FROM ".TABLE_BUYER_WOHPAY.") ";
		$res = $this->db->query($query);

		foreach ($res as $itm) {
			$query = "INSERT INTO ".TABLE_BUYER_WOHPAY." (user_id, item_id, woh_perc) VALUES(".$uid.", ".$itm['item_id'].", ".WOH_PERCENT_BY_PAY.") ";
			$this->db->exec($query);
		}
	}

	//GET ITEMS WHERE USER PAYED 10 PERC BY SUM

	public function getWohPayedItem($uid) {
		$query = "SELECT item_id FROM ".TABLE_BUYER_WOHPAY." WHERE user_id = ".addslashes($uid);
		return $this->db->query($query);
	}

	// GET GEO LOCATION IN DB

	public function searchLocationDB($name, $loc = 'reg') {
		switch ($loc) {
			case 'reg': $query = "SELECT * FROM ".TABLE_REGION_LANG." WHERE name LIKE '".addslashes($name.'%')."'"; break;
			case 'country': $query = "SELECT * FROM ".TABLE_COUNTRY_LANG." WHERE name LIKE '".addslashes($name.'%')."'"; break;
			default: return '';
		}
		return $this->db->query($query);
	}

	public function getCountry($obl_id) {
		$query = "SELECT * FROM ".TABLE_CITY." WHERE obl_id = ".addslashes($obl_id)." LIMIT 0,1";
		return $this->db->query($query);
	}

	public function getItemCurrency($pid) {
		$query = "SELECT * FROM ".TABLE_CAT_CURRENCY." WHERE id = (SELECT currency_id FROM ".TABLE_CAT_ITEMS." WHERE id = ".addslashes($pid).") ";
		return $this->db->query($query);
	}
	// UNSUBSCRIBE

	public function Buyer_Unsubscribe($usr){
		$query = "INSERT INTO `jhlp_cat_buyer_unsubscribe`(buyer_id, date) VALUES (".addslashes($usr).", CURDATE())";
		return $this->db->exec($query);
	}

	// ADD HELP REQUEST FROM USERS

	public function addHelpReq($uid, $projid, $helpReq = REQ_TYPE_OTHER, $reqStatus = 1, $amount = 1, $is_promise = REQ_PRIMISE_NOPAY, $show = 1) {
		$isReqSent = ( ($this->Buyer_ReqIsSend($uid, $projid)) !== false );

		$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ." (item_id, sender_id, req_type, req_status, is_second, show_first, req_amount, add_date, modify_date, comments, is_promise, `show`)
		 			VALUES(".$projid.", ".$uid.", ".$helpReq.", ".addslashes($reqStatus).", ".($isReqSent ? 1 : 0).", 0, ".$amount.", NOW(), NOW(), '', ".$is_promise.", ".addslashes($show).")";
		return $this->db->exec($query);
	}

	// GET AND SET USER IP WITH DB AND GET LOCATION

	public function getUserIpInfo($ip) {
		$query = "SELECT * FROM ".TABLE_USER_IP." WHERE user_ip = '".addslashes($ip)."'";
		return $this->db->query($query);
	}

	public function setUserIpInfo($uid, $ip, $co, $reg) {
		$query = "INSERT INTO ".TABLE_USER_IP."(user_id, user_ip, user_country, user_reg, add_date)
					VALUES(".addslashes($uid).", '".addslashes($ip)."', ".addslashes($co).", ".addslashes($reg).", NOW())";
		return $this->db->exec($query);
	}

	// GET USER RANK FROM ALL USER RATE

	public function getUserRankPosition($uid, $by = 'rate') {
		$query = "SELECT `buyer_id`, `user_rate`, ";
		switch ($by) {
			case 'rate' : $query .= " (SELECT COUNT(*)+1 FROM ".TABLE_SHOP_BUYERS_BOX." WHERE user_rate > bb1.user_rate) AS rank_upper, 
									  (SELECT COUNT(*) FROM ".TABLE_SHOP_BUYERS_BOX." WHERE user_rate >= bb1.user_rate) AS rank_lower 
									  FROM ".TABLE_SHOP_BUYERS_BOX." bb1 WHERE bb1.buyer_id = ".addslashes($uid);
				break;
			case 'day' : $query .= " (SELECT COUNT(*) FROM ".TABLE_SHOP_BUYERS_BOX." WHERE money_day >= bb1.money_day) AS rank_day_lower 
									 FROM ".TABLE_SHOP_BUYERS_BOX." bb1 WHERE bb1.buyer_id = ".addslashes($uid);
				break;
			case 'city' :
				$uinfo = $this->Buyer_Info($uid);
				$query .= " (SELECT COUNT( * ) FROM ".TABLE_SHOP_BUYERS_BOX." bb2
    								  JOIN jhlp_cat_buyer b2 ON b2.id = bb2.buyer_id
									  WHERE bb2.user_rate >= bb1.user_rate AND b2.city_id = ".$uinfo['city_id'].") AS rank_city_lower
										FROM ".TABLE_SHOP_BUYERS_BOX." bb1
										JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = bb1.buyer_id
										WHERE b1.city_id = ".$uinfo['city_id']." AND b1.id = ".addslashes($uid);
		}
		$res = $this->db->query($query);

		if(!empty($res)) {
			return $res[0];
		} else return 0;
	}

	public function getPositionByRate($uid, $rate) {
		$query = "SELECT `buyer_id`, `user_rate` cur_user_rate, `user_rate` + ".addslashes($rate)." new_user_rate, 
					(SELECT COUNT(*) FROM ".TABLE_SHOP_BUYERS_BOX." WHERE user_rate >= bb1.user_rate + ".addslashes($rate).") AS upd_rank,
					(SELECT COUNT(*) FROM ".TABLE_SHOP_BUYERS_BOX." WHERE money_day >= bb1.money_day + ".addslashes($rate).") AS upd_rank_day
					FROM ".TABLE_SHOP_BUYERS_BOX." bb1 WHERE bb1.buyer_id = ".addslashes($uid);

		//(SELECT COUNT(*) FROM ".TABLE_SHOP_BUYERS_BOX." WHERE money_day >= bb1.money_day + ".addslashes($rate).") AS upd_rank_day - FROM RANK IN CURRENT DAY

		return $this->db->query($query);
	}

	public function getTableUserRate($uid, $urate, $type = 'rate', $to = 30) {
		$query = '';

		switch($type) {
			case 'rate' :
				$query = "SELECT bb1.`buyer_id`, bb1.`user_rate`, bb1.user_rank, bb1.user_rank_diff, b1.name, b1.fname, b1.pic_sm, 
(SELECT count(id) FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE req_status = 1 AND sender_id = bb1.buyer_id) hlpco
					FROM (SELECT * FROM ".TABLE_SHOP_BUYERS_BOX." ORDER BY user_rank ASC) bb1 
					JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = bb1.buyer_id 
					WHERE bb1.user_rank BETWEEN ".(($urate-$to) < 0 ? 1 : ($urate-$to)) ." AND ".($urate+$to);
				break;

			case 'city' :
				$uinfo = $this->Buyer_Info($uid);

				$query = "SELECT bb1.`buyer_id`, bb1.`user_rate`, bb1.user_rank_city user_rank, bb1.user_rank_city_diff user_rank_diff, b1.name, b1.fname, b1.pic_sm,
						(SELECT count(id) FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE req_status = 1 AND sender_id = bb1.buyer_id) hlpco
					FROM (SELECT bb2.* FROM ".TABLE_SHOP_BUYERS_BOX." bb2
								JOIN ".TABLE_SHOP_BUYERS." b2 ON b2.id = bb2.buyer_id 
								WHERE b2.city_id = ".$uinfo['city_id']." 
								ORDER BY bb2.user_rank_city ASC) bb1 
					JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = bb1.buyer_id 
					WHERE bb1.user_rank_city BETWEEN ".(($urate-$to) < 0 ? 1 : ($urate-$to)) ." AND ".($urate+$to);
				break;
			default: return 0;
		}
		//echo $query;
		/*
			SELECT bb1.`buyer_id`, bb1.`user_rate`, (SELECT COUNT( * ) FROM jhlp_cat_buyer_box bb3
    								  JOIN jhlp_cat_buyer b3 ON b3.id = bb3.buyer_id
									  WHERE bb3.user_rate >= bb1.user_rate AND b3.city_id = 9905
                                        ) AS rank_city_lower, bb1.user_rank_diff, b1.name, b1.fname, b1.pic_sm FROM
(
    SELECT bb2.* FROM jhlp_cat_buyer_box bb2
    JOIN jhlp_cat_buyer b2 ON b2.id = bb2.buyer_id
    WHERE b2.city_id = 9905
    ORDER BY bb2.user_rank ASC
) bb1
JOIN jhlp_cat_buyer b1 ON b1.id = bb1.buyer_id
WHERE b1.city_id = 9905 AND bb1.user_rank BETWEEN 5 AND 120
		*/
		return $this->db->query($query);
	}

	public function addProjViews($pid, $co) {
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET cat_views = cat_views + ".addslashes($co)." WHERE id = ".addslashes($pid);
		return $this->db->exec($query);
	}

	public function recalcItemViewRate($pid) {
		$itm_info = $this->Item_Info($pid);

		if(!empty($itm_info)) {

			$upower = $this->getDateBox($itm_info['author_id'])[0]['money'];
			$upower = ($upower > 300 ? 300 : $upower);    // сила пользователя за все время
			$item_cat_views = $itm_info['cat_views'];            // просмотры с каталога
			$newrat = ($upower - $item_cat_views);

			$query = "UPDATE " . TABLE_CAT_ITEMS . " SET cat_view_rate = " . addslashes($newrat) . " WHERE id = " . addslashes($itm_info['id']);

			$this->db->exec($query);

			return $newrat;
		} return 0;
	}

	public function sendUserMsg($fromid, $toid, $txt, $projid = 0, $replyto = 0, $send_mail = 1, $subject = '') {
		$query = "INSERT INTO " . TABLE_CAT_MSG . " (item_id, from_id, to_id, reply_msg_id, add_date, modify_date, message) 
			VALUES ('$projid', '$fromid', '$toid', '$replyto', NOW(), NOW(), '" . addslashes($txt) . "')";
		if (!$this->db->exec($query))
			return false;

		$msg_id = $this->db->insert_id();
		if ($msg_id == 0)
			return false;

		$query = "INSERT INTO " . TABLE_CAT_MSG_P2P . " (msg_id, user_id, add_date, modify_date) 
			VALUES ('$msg_id', '$fromid', NOW(), NOW() )";
		if (!$this->db->exec($query))
			return false;

		$query = "INSERT INTO " . TABLE_CAT_MSG_P2P . " (msg_id, user_id, add_date, modify_date)
		VALUES ('$msg_id', '$toid', NOW(), NOW() )";
		if (!$this->db->exec($query))
			return false;

		$to_email = "";
		$from_name = "";

		$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='" . $toid . "'";
		$res = $this->db->query($query);
		if (count($res) > 0) {
			$to_email = $res[0]['email'];
		}


		$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='" . $fromid . "'";
		$res = $this->db->query($query);
		if (count($res) > 0) {
			$from_name = ($res[0]['account_type'] == USR_TYPE_PERS ? $res[0]['name'] . ' ' . $res[0]['fname'] : $res[0]['orgname']);
		}


		// Send notify mail
		if($send_mail == 1) {
			if (($to_email != "") && ($from_name != ""))
				$this->sendUserMailMsg_ct($toid, $subject, $txt);
		}	return true;
	}

}