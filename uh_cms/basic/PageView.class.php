<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("SEO_TITLE", "title");
define("SEO_KEYW", "keyw");
define("SEO_DESCR", "descr");
define("SEO_H1", "h1");
define("SEO_TEXT1", "text1");
define("SEO_TEXT2", "text2");
define("SEO_SOCIAL_TAGS", "sc_tags");

define("META_NOINDEX", "noindex");
define("META_NOFOLLOW", "nofollow");
define("META_CANONICAL", "canonicalurl");
define("META_NEXT", "nextpage");
define("META_PREV", "prevpage");

class PageView extends View {
	
	protected $pageModel;
	protected $breadcrumbs;
	protected $localize;
	protected $statusBox;
	
	protected $seo;
	protected $meta;
	
	function __construct($config, $pModel)
	{
		parent::__construct($config);
		
		$this->breadcrumbs = Array();
		$this->seo = Array("title" => "", "keyw" => "", "descr" => "", "h1" => "", "text1" => "", "text2" => "", "sc_tags" => "");
		$this->meta = Array("noindex" => false, "nofollow" => false, "canonicalurl" => "", "nextpage" => "", "prevpage" => "");
		$this->pageModel = $pModel;
		$this->localize = UhCmsApp::getLocalizerInstance();
		$this->top_is_adaptive = false;
		$this->statusBox = UhCmsApp::getStatusBox();
	}	
	
	public function setSeo($option, $value)
	{
		if( isset($this->seo[$option]) )
			$this->seo[$option] = $value;
	}
	
	public function setMeta($option, $value)
	{
		if( isset($this->meta[$option]) )
			$this->meta[$option] = $value;
	}
	
	public function getLocalizer()
	{
		return $this->localize;
	}
	
	public function setPageModel($pModel)
	{
		$this->pageModel = $pModel;
	}
	
	public function renderHeader()
	{		
		include $this->cfg['MVC_PATH']."templates/header-inc.php";
	}
	
	public function renderFooter()
	{
		include $this->cfg['MVC_PATH']."templates/footer-inc.php";
	}	
	
	public function renderPage($page_tpl_file) {
		$seodata = $this->pageModel->Seo_LoadData();
		$req_url = strtok($_SERVER["REQUEST_URI"], '?');

		//echo $_SERVER["REQUEST_URI"].'<br>';
		//echo $req_url.'<br>'; die();

		if(empty($seodata)) {
			if(preg_match('/helpwall\/[0-9]+.html/', $req_url, $matches)) {
				$id = explode('/', $matches[0])[1];
				$id = explode('.', $id)[0];
				$seodata['page_title'] = $this->pageModel->Seo_LoadData_imp($id, 'helpwall')[0]['title'];
			}
			if(preg_match('/viewrev\/[0-9]+\//', $req_url, $matches)) {
				$id = explode('/', $matches[0])[1];
				$title = $this->pageModel->Seo_LoadData_imp($id, 'user')[0];
				$seodata['page_title'] = $title['name'].' '.$title['fname'].', '.$title['city'].' - информация о пользователе WayOfHelp.';
			}
			if(preg_match('/cat\/.*/', $req_url, $matches)) {
				$catURL = explode('/', $matches[0]);

				if((strpos($catURL[1], 'p_')) !== false) {

				} else {
					$cat = $catURL[1];

					$page = (isset($catURL[2]) && $catURL[2]) ? explode('_', $catURL[2])[1] : '';

					if ($cat == 'thing-market') {
						$seoTitle = $this->setSeoCategories($page, 'Получить помощь');
					} else
						$seoTitle = $this->setSeoCategories($page);

					$seodata['page_title'] = $seoTitle['title'];
					$seodata['page_descr'] = $seoTitle['descr'];
				}
			}
		}

		//var_dump($seodata);
		
		if( $seodata != null ) {
			if( isset($seodata['page_title']) && $seodata['page_title'] != "" )
				$this->setSeo(SEO_TITLE, $seodata['page_title']);
			if( isset($seodata['page_keywords']) && $seodata['page_keywords'] != "" )
				$this->setSeo(SEO_KEYW, $seodata['page_keywords']);
			if( isset($seodata['page_descr'])&& $seodata['page_descr'] != "" )
				$this->setSeo(SEO_DESCR, $seodata['page_descr']);			
			
			if( isset($seodata['page_h1']) && $seodata['page_h1'] != "" )
				$this->setSeo(SEO_H1, $seodata['page_h1']);
			if( isset($seodata['content_text']) &&$seodata['content_text'] != "" )
				$this->setSeo(SEO_TEXT1, $seodata['content_text']);
			if( isset($seodata['content_words']) &&$seodata['content_words'] != "" )
				$this->setSeo(SEO_TEXT2, $seodata['content_words']);
		}
		// Render page
		$this->renderHeader();

		include $this->cfg['MVC_PATH']."templates/".$page_tpl_file;
		
		$this->renderFooter();
	}
	// own render function for lp

	public function renderLanding($page_tpl_file) {
		include $this->cfg['MVC_PATH']."landing/".$page_tpl_file;
	}
	
	public function render404()
	{
		//$this->renderPage("err404.php");
		header("HTTP/1.1 404 Not Found");
		include "/uh_cms/templates/err404.php";
		exit();
	}
	
	public function page_BuildUrl($contr_name, $action_name="", $other="")
	{
		return UhCmsUtils::Page_BuildUrl($contr_name, $action_name, $other);
	}
	
	public function addBreadcrumbs($url, $title)
	{
		$this->breadcrumbs[] = Array("url" => $url, "name" => $title);
	}
	
	public function renderBreadcrumbs($curpage_name = "") {
		// Build bread crumbs
		$BCROOTNAME = "Главная";
		$BCROORURL = WWWHOST;
		
		$BCHTML = '<li><a href="'.$BCROORURL.'">'.$BCROOTNAME.'</a></li>';
		if( isset($this->breadcrumbs) && (count($this->breadcrumbs)>0) ) {
			for( $i=0; $i<count($this->breadcrumbs); $i++ ) {
				$BCHTML .= '<li><a href="'.$this->breadcrumbs[$i]['url'].'">'.$this->breadcrumbs[$i]['name'].'</a></li>';
			}
		}
		
		if( $curpage_name != "" )
			$BCHTML .= '<li class="active">'.$curpage_name.'</li>';
		
		return $BCHTML;
	}
	
	public function setAdaptiveTop($isadapt=true) {
		$this->top_is_adaptive = $isadapt;
	}

	private function setSeoCategories($page = '', $endStr = 'Оказание помощи') {
		$seoCat = ['title' => '', 'descr' => ''];

		if( isset($this->breadcrumbs) && (count($this->breadcrumbs)>0) ) {

			if(count($this->breadcrumbs) == 1) {
				$seoCat['title'] = $this->breadcrumbs[0]['name'] . ' | ' . ($page ? 'Page №' . $page . ' | ' : '');
				$seoCat['descr'] = '1000 проектов об оказании или получении помощи в разделе '.$this->breadcrumbs[0]['name'].'. Смотрите все объявления на сайте WayOfHelp.';
			}
			else {
				for ($i = count($this->breadcrumbs) - 1; $i > 0; --$i) {
					$seoCat['title'] .= $this->breadcrumbs[$i]['name'] . ' | ';
					$seoCat['descr'] .= $this->breadcrumbs[$i]['name'].' ';
				}
				$seoCat['descr'] .= '. Помоги другому или получи помощь сам! Смотрите все объявления в разделе '.$this->breadcrumbs[0]['name'].' на сайте WayOfHelp.';
				$seoCat['title'] .= ($page ? 'Page №'.$page.' | ' : '');
				$seoCat['title'] .= $this->breadcrumbs[0]['name'].' | ';
			}
			$seoCat['title'] .= $endStr . ' - WayOfHelp';
		}
		return $seoCat;
	}
}
?>