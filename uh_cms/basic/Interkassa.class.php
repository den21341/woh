<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("INTERKASSA_URL", "http://sci.interkassa.com/");
define("IK_CALLBACK_URL_INTERACT", "payscallback/ikinteract");

class Interkassa{
	
	protected $data;
	protected $interact_url;	
	
	protected $secret_key;
	
	function __construct($config)
	{
		//parent::__construct($config);
		
		$this->secret_key = "aeGUCWlwPv9JHCnC";	// Реальный ключ
		//$this->secret_key = "HcXGP2DmYSTbjcGZ";		// Тестовый ключ
				
		$this->interact_url = $config['WWWHOST'].IK_CALLBACK_URL_INTERACT;
		
		$this->data = Array();
		
		$this->error_msg = "";	
	}	
	
	public function __set($pname, $val){
		$this->data[$pname] = $val;
	}
	
	public function __get($pname){
		return (isset($this->data[$pname]) ? $this->data[$pname] : 0);
	}

	public function __isset($pname){
		return isset($this->data[$pname]);
	}	
	
	public function getInteractUrl()
	{
		return $this->interact_url;
	}
	
	public function makeSign($post_arr, $with_secret=true)
	{
		$dataSet = $post_arr;
		
		unset($dataSet['ik_sign']);						// удаляем из данных строку подписи
		
		ksort($dataSet, SORT_STRING);					// сортируем по ключам в алфавитном порядке элементы массива
		array_push($dataSet, $this->secret_key);		// добавляем в конец массива "секретный ключ"
		$signString = implode(':', $dataSet);			// конкатенируем значения через символ ":"
		$sign = base64_encode(md5($signString, true));	// берем MD5 хэш в бинарном виде по сформированной строке и кодируем в BASE64
		
		return $sign; // возвращаем результат
	}
}

/*
<form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
	<input type="hidden" name="ik_co_id" value="561555503d1eafe0518b4567" />
	<input type="hidden" name="ik_pm_no" value="ID_4233" />
	<input type="hidden" name="ik_am" value="100.00" />
	<input type="hidden" name="ik_cur" value="RUB" />
	<input type="hidden" name="ik_desc" value="Благотворительный взнос" />
	<input type="hidden" name="ik_exp" value="2015-12-26" />
	<input type="hidden" name="ik_int" value="web" />
	<input type="hidden" name="ik_am_t" value="payway" />
	<input type="submit" value="Pay">
</form>
 */
?>