<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class PageController extends Controller{
	
	protected $pageModel;
	protected $pageView;
	protected $catLib;
	protected $LangId;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		$this->LangId = UhCmsApp::getLangId();
						
		// Make include for View
		// echo "__CLASS__ = ".__CLASS__."<br />";		// Class name of current method = PageController
		// echo get_class()."<br>";						// Class name of current method = PageController
		// echo get_called_class()."<br />";			// Class name of decendant class = Main, ...
		if( get_class() != get_called_class() )
		{
			// Load page model source code
			$fileModel = $config['MVC_PATH']."models/".strtolower(get_called_class())."Model.class.php";
		
			//$this->showLog("Load model: ".$fileModel);
		
			if( !file_exists($fileModel) )
			{
				//die('404 Not Found - Model '.strtolower(get_called_class()));
				$this->go404();
				return;
			}
			
			include_once $fileModel;
			
			// Load page view class source code
			$fileView = $config['MVC_PATH']."views/".strtolower(get_called_class())."View.class.php";
		
			//$this->showLog("Load view: ".$fileView);
		
			if( !file_exists($fileView) )
			{
				//die('404 Not Found - View '.strtolower(get_called_class()));
				$this->go404();
				return;
			}
			
			include_once $fileView;			
		}	

		$this->initPage();		
	}	
	
	public function initPage(){
		// Load all data to PageModel object
		// 1. Load contacts
		// 2. Load text resources
		// 3. Load top menu
		// 4. Load catalog menu		
				
		try{
			$model_class = get_called_class()."Model";
			$view_class = get_called_class()."View";
			//$this->pageModel = new PageModel($this->cfg, $this->db, 1);
			
			//$this->showLog("Create page model: ".$model_class.", page view: ".$view_class);
			
			$this->pageModel = new $model_class($this->cfg, $this->db, $this->LangId);
			$this->pageView = new $view_class($this->cfg, $this->pageModel);
			
			$this->pageView->catmodel = $this->catLib = new Catalog($this->db, $this->LangId);
		}
		catch(Exception $e)
		{
			echo "ERROR Creating basic PageModel: ".$e->getMessage();
		}
	}
	
	public function getPageModel(){
		return $this->pageModel;
	}
	
	public function go301($togo = WWWHOST){
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$togo);
		exit();
	}
	
	public function go404($togo = ""){
		$gourl = $togo;
		if( $gourl == "" )
			WWWHOST.'info/err404';
		header("HTTP/1.1 404 Not Found");
		header("Location: ".$gourl);
		exit();
	}
	
	public function action_default(){
	}
	
	
}
?>