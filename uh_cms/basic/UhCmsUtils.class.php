<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

// Resource utils
// Contacts utils
// 

class UhCmsUtils
{	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Basic functions
	public static function make_seed()
	{
		list($usec, $sec) = explode(' ', microtime());
		return (float) $sec + ((float) $usec * 100000);
	}

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// GUID generation
	public static function makeUuid()
	{
		//list($usec, $sec) = explode(' ', microtime());
		//$seed = (float) $sec + ((float) $usec * 100000);

		//srand($seed);

		$uuid = sprintf( "%08X-%04X-%04X-%04X-%06X%06X", rand(1000, (1024*1024*1023*2-1)), rand(0,65535), time() % 65535, rand(0,65535), rand(0,16777215), rand(0,16777215));
		return $uuid;
	}
	
	public static function makeUpass()
	{
		$carr = "1234567890ABCDEFGHJIKLMNOPQRSTUVWXYZabcdefgjiklmnopqrstuvwxyz";
		
		$pass = '';
		for( $i=0; $i<10; $i++ )
			$pass .= mb_substr($carr, rand(0,strlen($carr)-1), 1);
		
		return $pass;
	}
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	public static function sendmail($from_email, $from_name, $to_email, $subject, $body, $charset = "cp1251")	
	{
		if( $charset == "cp1251" )
		{
			self::send1251mail($from_email, $from_name, $to_email, $subject, $body);
		}
	}
	
	 
	public static function send1251mail($from, $from_name, $to, $subject, $body)
	{
		$mailto = $to;
		$mailsub = iconv("UTF-8", "cp1251", "=?windows-1251?b?").base64_encode(iconv("UTF-8", "cp1251", $subject)).iconv("UTF-8", "cp1251","?=");
		$mailbody = iconv("UTF-8", "cp1251", $body);
		$mailfrom = $from_name; //"Служба поддержки ".substr($WWWHOST, 7, strlen($WWWHOST)-8);
		$mailhdr = iconv("UTF-8", "cp1251", "From: =?windows-1251?b?").base64_encode(iconv("UTF-8", "cp1251",$mailfrom)).iconv("UTF-8", "cp1251","?= <".$from.">\r\nReply-To: =?windows-1251?b?").base64_encode(iconv("UTF-8", "cp1251",$mailfrom)).iconv("UTF-8", "cp1251","?= <".$from.">\r\nContent-Type: text/plain; charset=\"windows-1251\"");
		mail( $mailto, $mailsub, $mailbody, $mailhdr );
	}

	public static function sendHTML($from, $from_name, $to, $sub, $txt){
		$mailfrom = $from_name;
		$mailhdr = iconv("UTF-8", "cp1251", "From: =?windows-1251?b?").base64_encode(iconv("UTF-8", "cp1251",$mailfrom)).iconv("UTF-8", "cp1251","?= <".$from.">\r\nReply-To: =?windows-1251?b?").base64_encode(iconv("UTF-8", "cp1251",$mailfrom)).iconv("UTF-8", "cp1251","?= <".$from.">\r\nContent-Type: text/plain; charset=\"windows-1251\"");
		mail($to, $sub, $txt, "Content-type:text/html; charset = utf-8", $mailhdr);
	}

	/*
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Send windows 1251 email message (only for cp1251 page encoding)
	public static function send1251mail($to, $subject, $body)
	{
		global $continfo, $WWWHOST;

		$mailto = $to;
		$mailsub = iconv("UTF-8", "cp1251", "=?windows-1251?b?").base64_encode(iconv("UTF-8", "cp1251", $subject)).iconv("UTF-8", "cp1251","?=");
		$mailbody = iconv("UTF-8", "cp1251", $body);
		$mailfrom = "Служба поддержки ".substr($WWWHOST, 7, strlen($WWWHOST)-8);
		$mailhdr = iconv("UTF-8", "cp1251", "From: =?windows-1251?b?").base64_encode(iconv("UTF-8", "cp1251",$mailfrom)).iconv("UTF-8", "cp1251","?= <".$continfo['infomail'].">\r\nReply-To: =?windows-1251?b?").base64_encode(iconv("UTF-8", "cp1251",$mailfrom)).iconv("UTF-8", "cp1251","?= <".$continfo['infomail'].">\r\nContent-Type: text/plain; charset=\"windows-1251\"");
		mail( $mailto, $mailsub, $mailbody, $mailhdr );
	}
	*/
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Trim string to the required length and add '...' to the end
	public static function TrimTo($string, $len)
	{
		$str = $string;
		if( strlen($string) > $len )
		{
			$str = substr($string, 0, $len)."...";
		}

		return $str;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	//			URL processing methods
	//
	////////////////////////////////////////////////////////////////////////////////
	
	// GET method query processing methods
	//    trims dangerous chars and siquences
	public static function ClearSQLInject( $str )
	{
		$strval = $str;

		$strval = str_replace("\'",'',$strval);
		$strval = str_replace('"','',$strval);
		$strval = str_replace("'",'',$strval);
		$strval = str_replace('\"','',$strval);
		$strval = str_replace('%27','',$strval);
		$strval = str_replace("%22",'',$strval);

		return $strval;
	}

	// Delete Query param from Query string
	public static function StripParamFromQuery( $querystr, $stripname )
	{
		$params = explode( "&", $querystr );
		$out_query = "";
		for($i=0; $i<count($params); $i++)
		{
			$it = explode( "=", $params[$i] );
			if( count($it) == 2 )
			{
				if( $it[0] != $stripname )
				{
					$out_query .= ($out_query == "" ? "" : "&").$params[$i];
				}
			}
		}
		return $out_query;
	}

	// Translate the cyrilic string to enlglish letters
	public static function TranslitEncode($string)
	{
		$converter = array(
		'а' => 'a',   'б' => 'b',   'в' => 'v',
		'г' => 'g',   'д' => 'd',   'е' => 'e',
		'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
		'и' => 'i',   'й' => 'y',   'к' => 'k',
		'л' => 'l',   'м' => 'm',   'н' => 'n',
		'о' => 'o',   'п' => 'p',   'р' => 'r',
		'с' => 's',   'т' => 't',   'у' => 'u',
		'ф' => 'f',   'х' => 'h',   'ц' => 'c',
		'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
		'ь' => "",   'ы' => 'y',   'ъ' => "",
		'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
		' ' => '_',   '%' => '_',
		'А' => 'A',   'Б' => 'B',   'В' => 'V',
		'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
		'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
		'И' => 'I',   'Й' => 'Y',   'К' => 'K',
		'Л' => 'L',   'М' => 'M',   'Н' => 'N',
		'О' => 'O',   'П' => 'P',   'Р' => 'R',
		'С' => 'S',   'Т' => 'T',   'У' => 'U',
		'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
		'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
		'Ь' => "",  'Ы' => 'Y',   'Ъ' => "",
		'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
		'Ї' => 'j', 'ї' => 'j', 'є' => 'e', 'Є' => 'E', 'і' => 'i', 'І' => 'I',
		'!' => '', '#' => '_', '№' => '_', '$' => '_', '%' => '_', '*' => '_', '?' => '_', '@' => '_',
		'/' => '_', '\\' => '_', '"' => '', '\'' => '', '(' => '_', ')' => '_', '[' => '_', ']' => '_', '.' => '_', ',' => '', '&' => '_', '+' => '_'
		);

		$conv_double = array('_________' => '_', '________' => '_', '_______' => '_', '______' => '_',
		'_____' => '_', '____' => '_', '___' => '_', '__' => '_', '_' => '_');

		//$string = ereg_replace("&quot;", "", $string);
		$string = str_replace("&quot;", "", $string);
		$tmp1 = strtr($string, $converter);
		//$tmp1 = ereg_replace("[^a-zA-Z0-9_-]+", "", $tmp1);
		$tmp1 = preg_replace("/[^a-zA-Z0-9_-]+/i", "", $tmp1);
		//$tmp1 = ereg_replace("[я]+", "", $tmp1);

		return trim( strtr($tmp1, $conv_double), "_" );
	}	
	
	////////////////////////////////////////////////////////////////////////////////
	//
	// Load formats
	//
	////////////////////////////////////////////////////////////////////////////////
	
	public static function Load_CsvFileAsArray($file_name, $in_enc = "UTF-8")
	{
		$csv = Array();
		
		if( !file_exists($file_name) )
			return $csv;
		
		$csv_lines = file($file_name);		
		for($i=0;$i<count($csv_lines);$i++)
		{
			$line = $csv_lines[$i];
			if( $in_enc != "UTF-8" )
			{
				$line = iconv($in_enc, "UTF-8", $line);
			}
			
			$row = explode(";", $line);
			for($j=0;$j<count($row);$j++)
			{
				$row[$j] = trim($row[$j], "\"\r\n ");
			}
			
			$csv[] = $row;
		}
		
		return $csv;
	}
	
	////////////////////////////////////////////////////////////////////////////////
	//
	//		Build basic page's urls
	//
	////////////////////////////////////////////////////////////////////////////////
	
	public static function Page_BuildUrl($controller, $action="", $queryparams="")
	{	
		$url = ($controller == "index" ? "" : $controller)."/";
		
		if( $action != "" )
			$url .= $action."/";
		
		if( $queryparams != "" )
			$url .= "?".$queryparams;
		
		return WWWHOST.$url;
	}

	////////////////////////////////////////////////////////////////////////////////
	//
	//		Image processing utilities
	//
	////////////////////////////////////////////////////////////////////////////////

	// Recalc sizes with saving proportions from source $w,$h to the max rectangle of $dw,$dh
	public static function Image_RecalcSize($w, $h, $dw, $dh)
	{
		$ow = 0;
		$oh = 0;
		if( ($w <= $dw) && ($h<=$dh) )
		{
			$ow = $w;
			$oh = $h;
		}
		else
		{
			if( ($w/$dw) > ($h/$dh) )
			{
				$oh = floor( ($dw*$h)/$w );
				$ow = $dw;
			}
			else
			{
				$ow = floor( ($dh*$w)/$h );
				$oh = $dh;
			}
		}

		return Array("w" => $ow, "h" => $oh);
	}
	
	// Get the image sizes by its path
	public static function GetImageSizeAll( $srcimg, $srcext="" )
	{
		$size = getimagesize($srcimg);
		if( $size === FALSE )
			return null;
		
		$ret = Array('w' => $size[0], 'h' => $size[1]);
		
		return $ret;
		
		/*
		$ret = null;

		$img_ext = strtolower( $srcext );

		switch($img_ext)
		{
			case ".jpg":
			case ".jpeg":
				 $im = @imagecreatefromjpeg( $srcimg );
				 break;
			case ".png":
				 $im = @imagecreatefrompng( $srcimg );
				 break;
			case ".gif":
				 $im = @imagecreatefromgif( $srcimg );
				 break;
		}

		if( empty($im) || ($im == null) )
			return $ret;

		$img_w = @imagesx( $im );
		$img_h = @imagesy( $im );

		if( ($img_w > 0) && ($img_w < 3000) && ($img_h > 0) && ($img_h < 3000) )
		{
			$ret = Array('w' => $img_w, 'h' => $img_h);
		}

		return $ret;
		*/
	}
	
	// Resize image and save to new location
	public static function ResizeImage( $srcimg, $dstimg, $outformat, $max_w, $max_h, $truesize = false, $jpg_quality = 90, $truecolor = true, $with_copyright = false, $copyright_pic = "", $copyright_str = "Копирайт" )
	{
		//$img_ext = $srcext;
		$extform = strtolower($outformat); 	// Could be '.jpg', '.png', '.gif'
		$THUMB_W = $max_w;								// Destination image width
		$THUMB_H = $max_h;								// Destination image hight

		//$namefiles = $FILE_DIR.$namefiles;
		//$img_ext = strtolower(substr( $srcimg, strrpos($srcimg, ".") ));
		
		$sz = getimagesize($srcimg);
		
		if( empty($sz['mime']) )
			return false;

		//switch($img_ext)
		switch($sz['mime'])
		{
			//case ".jpg":
			//case ".jpeg":
			case "image/jpeg":
				 $im = imagecreatefromjpeg( $srcimg );
				 break;
			//case ".png":
			case "image/png":
				 $im = imagecreatefrompng( $srcimg );
				 break;
			//case ".gif":
			case "image/gif":
				 $im = imagecreatefromgif( $srcimg );
				 break;
			default:
				$im = NULL;
		}


		// If error occure while opening file then return ERROR
		if( $im == NULL )
			return false;

		// Get the source image size
		$src_w = imagesx( $im );
		$src_h = imagesy( $im );

		// Calculate destination image size
		if( $truesize )
		{
			$dst_w = $THUMB_W;
			$dst_h = $THUMB_H;
			
			$k = $dst_w/$dst_h; 
			$sk = $src_w/$src_h;
			
			if( $k > $sk )
			{
				$src_cw = $src_w;		
				$src_ch = ($src_w / $k);
			}
			else
			{
				$src_ch = $src_h;
				$src_cw = ($src_h * $k);
			}
			
			//echo $src_w.":".$src_h." => ".$dst_w.":".$dst_h." == ".$src_cw.":".$src_ch."<br>";
			
			//$src_ch = $src_cw = ($src_w > $src_h ? $src_h : $src_w);		// Для прямоугольника на віходе работает
			
			//$src_ch = ($src_w > ceil($src_h*$k) ? $src_h : ceil($src_h/$k));
			//$src_cw = ( $src_ch < $src_h ? $src_w : ceil($src_w/$k) )
			
			//if( $dst_w > $dst_h )			
		}
		else
		{
			if( ($src_w > $THUMB_W) || ($src_h > $THUMB_H) )
			{
				$dst_w = $THUMB_W;
				$dst_h = ceil($dst_w * ($src_h / $src_w));

				if( $dst_h > $THUMB_H )
				{
					$dst_h = $THUMB_H;
					$dst_w = ceil($dst_h * ($src_w / $src_h));
				}
			}
			else
			{
				$dst_w = $src_w;
				$dst_h = $src_h;
			}
		}

		// Create new image and put resized picture to it
		if( $truecolor )
			$dst_im = imagecreatetruecolor($dst_w, $dst_h);
		else
			$dst_im = imagecreate($dst_w, $dst_h);

		imagealphablending( $dst_im, true );

		//if( $img_ext == ".png" )
		//{
			// Fill white rectangle to avoid the transparent pixels to be black
			imagefilledrectangle($dst_im, 0, 0, $dst_w, $dst_h, imagecolorallocate($dst_im, 255, 255, 255));
		//}

		if( $truesize )
			//imagecopyresized( $dst_im, $im, 0, 0, floor( ($src_w - $src_cw) / 2 ), floor( ($src_h - $src_ch) / 2 ), $dst_w, $dst_h, $src_cw, $src_ch );
			imagecopyresampled( $dst_im, $im, 0, 0, floor( ($src_w - $src_cw) / 2 ), floor( ($src_h - $src_ch) / 2 ), $dst_w, $dst_h, $src_cw, $src_ch );
		else
			//imagecopyresized( $dst_im, $im, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h );
			imagecopyresampled( $dst_im, $im, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h );


		if( $with_copyright )
		{
			if( $copyright_pic != "" )
			{
				// make copyright watermark logo from picture
				$im_logo = imagecreatefrompng( $copyright_pic );
				if( $im_logo )
				{
					$logo_w = imagesx( $im_logo );
					$logo_h = imagesy( $im_logo );

					imagecopy( $dst_im, $im_logo, 5, $dst_h-$logo_h-5, 0, 0, $logo_w, $logo_h ) ;
				}
			}
			else
			{
				// make copyright watermark text
				$conv_from = "CP1251";
				$conv_to = "UTF-8";
				//$font_file_name = "font/verdana.ttf";
				//$font_file_logo = "font/futura.ttf";
				$font_file_name = "font/verdana.ttf";
				$font_size = "12";
				$txt_x = $dst_w - 110;
				$txt_y = $dst_h - 12;
				// No parameters given for drawing so display error message for end user
				$txt_color = imagecolorallocate($im, 0xFF,0xFF, 0xFF);
				$txt_color_black = imagecolorallocate($im, 0x33,0x33, 0x33);
				imagettftext($dst_im, $font_size, 0, $txt_x, $txt_y, $txt_color_black, $font_file_name, iconv($conv_from, $conv_to, $copyright_str));
				imagettftext($dst_im, $font_size, 0, $txt_x+1, $txt_y+1, $txt_color, $font_file_name, iconv($conv_from, $conv_to, $copyright_str));
				//imagettftext($dst_im, $font_size, 0, $txt_x, $txt_y, $txt_color_black, $font_file_name, $copiright_str);
				//imagettftext($dst_im, $font_size, 0, $txt_x+1, $txt_y+1, $txt_color, $font_file_name, $copiright_str);
			}
		}

		// Check if output file already exists then delete it.
		if( file_exists( $dstimg ) )
		{
			unlink( $dstimg );
		}

		// Write destination image to disk
		switch( $extform )
		{
			case ".jpg":
				imagejpeg( $dst_im, $dstimg, $jpg_quality );
				break;
			case ".png":
				imagepng( $dst_im, $dstimg );
				break;
			case ".gif":
				imagegif( $dst_im, $dstimg );
				break;
		}

		return true;
	}
}
?>