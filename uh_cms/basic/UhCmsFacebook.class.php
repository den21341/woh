<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsFacebook{
	protected	$data;
	
	protected	$app_id;
	protected	$secret_id;
	
	function __construct($appid, $secretid)
	{
		$this->app_id = $appid;
		$this->secret_id = $secretid;
	}		
	
	function __set($pname, $val){
		$this->data[$pname] = $val;
	}
	
	function __get($pname){
		return (isset($this->data[$pname]) ? $this->data[$pname] : 0);
	}

	function __isset($pname){
		return isset($this->data[$pname]);
	}	
	
	public function makeLoginLink($redirurl)
	{		
		$FB_LOGIN_URL = 'http://www.facebook.com/v2.0/dialog/oauth/';
		
		$params = array(
			'client_id'     => $this->app_id, //FB_APP_ID,
			'redirect_uri'  => $redirurl,
			'response_type' => 'code',
			//'scope'         => 'id,email,name,first_name,last_name,link,gender,user_birthday,verified'
				'scope'         => 'email,user_birthday'
		);
		
		
		$FB_LINK = $FB_LOGIN_URL."?".urldecode(http_build_query($params));
		
		return $FB_LINK;
	}
	
	public function setSessionToken($token)
	{
		$this->access_token = $token;
	}
	
	public function getSessionToken($redirurl, $code)
	{
		$FB_TOKEN_URL = 'https://graph.facebook.com/oauth/access_token';
		
		$params = array(
			'client_id'     => $this->app_id, //FB_APP_ID,
			'redirect_uri'  => $redirurl,
			'client_secret' => $this->secret_id,
			'granted_scopes'=> 'public_profile,user_photos',
			'code'          => $code
		);		
		
		$fb_res = file_get_contents($FB_TOKEN_URL.'?'.http_build_query($params));
		
		$tokenInfo = null;
		parse_str($fb_res, $tokenInfo);
		
		$this->access_token = $tokenInfo['access_token'];
		
		return $tokenInfo;
	}
	
	private function getMe($params)
	{
		$FB_ME_URL = 'https://graph.facebook.com/me';
		
		$url_req = $FB_ME_URL.'?'.urldecode(http_build_query($params));
		
		$userInfo = json_decode(file_get_contents($url_req), true);
		
		return $userInfo;
	}
	
	public function getUserPermissions()
	{
		$params = array(
			'fields' => 'permissions',
			'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		return $this->getMe($params);
	}
	
	public function getUserInfo($token = "")
	{
		$params = array(
			'fields' => 'id,birthday,email,first_name,last_name,name,gender,website,cover,link',
			'access_token' => ( $token != "" ? $token : $this->access_token )			
		);
		
		return $this->getMe($params);				
	}
	
	public function getUserAvatar($token = "")
	{
		/*
		$params = array(
			'fields' => 'picture',
			'type' => 'large',
			'redirect' => 'false',
			'access_token' => ( $token != "" ? $token : $this->access_token )		
		);
		
		return $this->getMe($params);
		*/
		$params = array(
				'type' => 'large',
				'redirect' => 'false',
				'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		$FB_ME_URL = 'https://graph.facebook.com/me/picture';
		
		$url_req = $FB_ME_URL.'?'.urldecode(http_build_query($params));
		
		$picInfo = json_decode(file_get_contents($url_req), true);
		
		return $picInfo;
	}
	
	public function getUserPhotos($token = "")
	{
		$params = array(
				'fields' => 'albums',
				'access_token' => ( $token != "" ? $token : $this->access_token )
		);
	
		return $this->getMe($params);
	}
	
	public function getPhoto($token = "", $photoid)
	{
		$params = array(
				'type' => 'large',
				'redirect' => 'false',
				'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		$FB_ME_URL = 'https://graph.facebook.com/me/picture';
		
		$url_req = $FB_ME_URL.'?'.urldecode(http_build_query($params));
		
		$picInfo = json_decode(file_get_contents($url_req), true);
		
		return $picInfo;
	}
	
	public function getUserFriends($token = "")
	{
		//$FB_ME_URL = 'https://graph.facebook.com/me/friend-list';
	
		//$params = array('access_token' => ( $token != "" ? $token : $this->access_token ));
		
		$params = array(
				'fields' => 'friend-list',
				'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		return $this->getMe($params);
	
		//$url_req = $FB_ME_URL.'?'.urldecode(http_build_query($params));	
		//echo "<br>".$url_req."<br>";
	
		//$userInfo = file_get_contents($url_req);	
		//$userInfo = json_decode(file_get_contents($FB_ME_URL.'?'.urldecode(http_build_query($params))), true);
	
		//return $userInfo;
	}
}
?>