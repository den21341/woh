<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsAdminRouter{
	private $url;
	private $controller_name;
	private $action_name;
	
	function __construct($requrl, $mvc_path, $config, $db, $ses){
		$this->url = $requrl;		
		
		//echo "Route admin url: ".$requrl."<br>";
		
		$controller_name = "Main";
		$action_name = "action_default";
		
		$urlarr = explode("/", $this->url);
		
		//var_dump($urlarr);
		
		// For admin router the 0th element will always be 'admin', because all routes points to /admin folder
		
		if( isset($urlarr[1]) && strncmp("?", trim($urlarr[1]), 1) == 0 )
		{
			// Request with empty controller but with parameters
			// /admin/?action=...
		}		
		else
		{		
			if( isset($urlarr[1]) && trim($urlarr[1]) != "" )
				$controller_name = $urlarr[1];
			
			if( isset($urlarr[2]) && strncmp("?", trim($urlarr[2]), 1) == 0 )
			{
				// Request with empty actions (default action) but with parameters
			}
			else
			{
				if( isset($urlarr[2]) && trim($urlarr[2]) != "" )
					$action_name = "action_".$urlarr[2];
			}
		}
		
		if( $controller_name != "Main" && $ses->UserId == 0 )
		{
			header("Location: ".WWWHOST."admin/");
			exit();
		}
			
				
		//echo "<br>Controller: ".$controller_name."; Action: ".$action_name."<br>";
		
		$this->controller_name = $controller_name;
		$this->action_name = $action_name;		
		
		$fileController = $mvc_path."controllers/".strtolower($this->controller_name)."Controller.class.php";
		
		//echo "Load controller: ".$fileController."<br />";
		
		if( !file_exists($fileController) )
		{
			die('404 Not Found - Controller');
		}
				
		include_once $fileController;
		
		$controller = new $this->controller_name($config, $db);
		
		//echo "Call.. ".$this->action_name."<br >";
		
		if (is_callable(array($controller, $this->action_name)) == false) {
            die ('404 Not Found');
        }
		else
		{
			$controller->$action_name();
		}
	}	
}
?>