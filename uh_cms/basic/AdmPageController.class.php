<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class AdmPageController extends Controller{
	
	protected $pageModel;
	protected $pageView;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		$this->initPage();
		
		// Make include for View
		//echo "__CLASS__ = ".__CLASS__."<br />";		
		//echo get_class()."<br>";
		//echo get_called_class()."<br />";
		if( get_class() != get_called_class() )
		{
			// Load page model class source code
			$fileModel = "models/".strtolower(get_called_class())."Model.class.php";
			
			//echo "Load model: ".$fileModel."<br />";
			
			if( !file_exists($fileModel) )
			{
				die('404 Not Found - Model');
			}
			
			include_once $fileModel;
			
			
			// Load page view class source code
			$fileView = "views/".strtolower(get_called_class())."View.class.php";
		
			//echo "Load view: ".$fileView."<br />";
		
			if( !file_exists($fileView) )
			{
				die('404 Not Found - Controller');
			}
			
			include_once $fileView;
		}		
	}	
	
	public function initPage(){
		// Load all data to AdmPageModel object
		// 1. Load left menu
		// 2. Load languages		
		try{
			$this->pageModel = new AdmPageModel($this->cfg, $this->db, 1);
		}
		catch(Exception $e)
		{
			echo "ERROR Creating basic PageModel: ".$e->getMessage();
		}
	}
	
	public function getPageModel(){
		return $this->pageModel;
	}
	
	public function action_default(){
	}
}
?>