<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsRouter {
	private $url;
	private $controller_name;
	private $action_name;

	function __construct($requrl, $mvc_path, $config) {
		$this->url = $requrl;

		//echo "Route url: ".$requrl."<br>";

		$controller_name = "Main";
		$action_name = "action_default";
		$page_url = "";

		$urlstring = $this->url;
		$urlhash = "";

		// Find HASH in url string url#hashparam

		if( ($pos = strrpos($urlstring, "#")) !== false ) {
			$urlstring = mb_substr($urlstring, 0, $pos);
			$urlhash = mb_substr($urlstring, $pos);
		}

		// first split query string into 2 parts: URL and QueryParams
		$urlparts = explode("?", $urlstring, 2);

		$req_url = $urlparts[0];
		$req_params = "";

		if( count($urlparts)>1 ) {
			$req_params = $urlparts[1];
		}

		$urlarr = explode("/", $req_url);

		// Check if the language is passed in request url  (Example: /en/news/view/)
		// If not, then set the first language as default
		//
		if( isset($urlarr[0]) && trim($urlarr[0]) != "" ) {
			$langs = UhCmsApp::getLangs();

			for($i=0; $i<count($langs); $i++) {

				if( $urlarr[0] == $langs[$i]['lang_code'] ) {
					// Language is passed in url as first folder
					UhCmsApp::setLangId($langs[$i]['id']);
					UhCmsApp::getLocalizerInstance()->selectLang($langs[$i]['id']);

					array_shift($urlarr);
					break;
				}
			}
		}

		if( isset($urlarr[0]) && trim($urlarr[0]) != "" ) {
			$controller_name = $urlarr[0];

			if( ($controller_name == "info") || ($controller_name == "promo") ) {
				//echo "!!!";
				if( $urlarr[1] != "" ) {
					$action_name = "action_runpage";
					$page_url = $urlarr[1];
				}
			}
			else {
				if( isset($urlarr[1]) && trim($urlarr[1]) != "" )
					$action_name = "action_".$urlarr[1];
			}
		}

		/*
		if( isset($urlarr[0]) && strncmp("?", trim($urlarr[0]), 1) == 0 )
		{
			// Request with empty controller but with parameters
			// /?getvar=...
		}
		else
		{
			if( isset($urlarr[0]) && trim($urlarr[0]) != "" )
				$controller_name = $urlarr[0];

			if( $controller_name == "info" )
			{
				if( $urlarr[1] != "" )
				{
					$action_name = "action_runpage";
					$page_url = $urlarr[1];
				}
			}
			else
			{
				if( isset($urlarr[1]) && strncmp("?", trim($urlarr[1]), 1) == 0 )
				{
					// Request with empty actions (default action) but with parameters
				}
				else
				{
					if( isset($urlarr[1]) && trim($urlarr[1]) != "" )
						$action_name = "action_".$urlarr[1];
				}
			}
		}
		*/

		//echo "<br>Controller: ".$controller_name."; Action: ".$action_name."<br>";

		$this->controller_name = $controller_name;
		$this->action_name = $action_name;

		$fileController = $mvc_path."controllers/".strtolower($this->controller_name)."Controller.class.php";

		//echo "Load controller: ".$fileController."<br />";

		if( !file_exists($fileController) ) {
			//die('404 Not Found - Controller');
			$this->goto404();
			return;
		}

		include_once $fileController;

		$controller = new $this->controller_name($config, UhCmsApp::getDbInstance());

		array_shift($urlarr);

		// If the controller will process routing operation by itself, then pass all url parts to default method 'handle_action'

		if( $controller->selfControlled() ) {
			$controller->handle_action($urlarr);
		} else {

			if( $this->action_name != "action_default" ) {
				array_shift($urlarr);
			}

			//print_r($urlarr); die();
			$controller->putUrls($urlarr);

			//echo "Call.. ".$this->action_name."<br >";

			if (is_callable(array($controller, $this->action_name)) == false) {
				$this->goto404();
				return;
			} else {

				switch( $controller_name ) {
					// v1.0 call
					//case "info":
					//	$controller->$action_name($page_url);
					//	break;

					default:
						$controller->$action_name();
						break;
				}
			}
		}
	}

	public function goto404() {
		header("HTTP/1.1 404 Not Found");
		include "uh_cms/templates/err404.php";
		exit();
	}
}