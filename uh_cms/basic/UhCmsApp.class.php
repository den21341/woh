<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsApp {
	protected static $mailer = null;
	protected static $db = null;
	protected static $ses = null;
	protected static $langs = null;
	protected static $LangId = 1;
	protected static $localize = null;
	protected static $statusBox = null;
	protected $full_req_url = "";
	protected $router;
	protected $config;

	function __construct($config) {
		$this->config = $config;

		self::$db = new UhCmsDb();

		self::$langs = [
			["id" => 1, "lang_code" => "ru", "lang_name" => "Русский"],
			["id" => 2, "lang_code" => "en", "lang_name" => "English"],
		];

		self::$LangId = 1;

		if( get_class() == get_called_class() ) {
			// Load only for main site, but not admin area
			self::$localize = new UhCmsLocalizer(self::$LangId);
			self::$statusBox = new UhCmsBoxRating();
		}
	}

	public function run() {
		$db_host 		= $this->config['DB_HOST'];
		$db_user 		= $this->config['DB_USER'];
		$db_password 	= $this->config['DB_PASSWORD'];
		$db_database 	= $this->config['DB_NAME'];

		$UHCMS_DEF_ENC 	= $this->config['DEF_ENC'];

		////////////////////////////////////////////////////////////////////////////
		// Make database connections
		if( !self::$db->connect($db_host,$db_user,$db_password,$db_database, $UHCMS_DEF_ENC) ) {
			//echo $this->db->getError();
			die(self::$db->getError());
		}

		if( $this->config['DEBUG_ON'] )
			self::$db->setDebug(true);

		////////////////////////////////////////////////////////////////////////
		// Load multilanguage support
		// Get cms language list
		self::$langs = self::$db->query("SELECT * FROM ".TABLE_LANGS." ORDER BY id");

		////////////////////////////////////////////////////////////////////////
		// Initialize session
		$this->startSession();

		////////////////////////////////////////////////////////////////////////
		// Create mailer object to send all mails
		self::$mailer = new UhCmsMailer($this->config);

		////////////////////////////////////////////////////////////////////////
		// Start router to process requests
		$this->startRouting();
	}

	protected function startSession() {
		self::$ses = new UhCmsAuthUserSes(self::$db);
		self::$ses->startSession(SESSION_PATH);
		//self::$ses->UserId = '2440';
		// If supports user sessions

		if( $this->config['WITH_AUTH'] ) {
			// Authentication here...
			self::$ses->AuthUser();
		}

		$this->showLog("Ses id: ".self::$ses->sesid()."; UserID = ".self::$ses->UserId);
	}

	protected function startRouting() {
		$this->full_req_url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

		/*$last_url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
		echo $last_url;
		echo '<br>';
		echo $last_url = str_replace(WWWHOST, '', $last_url);
		echo '<br>';
		$last_url = explode('/', $last_url);



		foreach (self::$langs as $lang_type) {

			if(mb_strtolower($last_url[0]) == $lang_type['lang_code']) {
				echo 'WOW'; die();
			}
		}
		die('NOPE');*/

		$this->showLog("Full path: ".$this->full_req_url);

		//setcookie("lang", 'ru', time() + 60 * 60 * 24 * 365, '/');

		$routeurl = "";

		if( strncmp($this->full_req_url, WWWHOST, strlen(WWWHOST)) == 0 ) {
			$routeurl = substr($this->full_req_url, strlen(WWWHOST));
		}

		$lang_type_url = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : '';

		//print_r(self::$langs);

		// TEST VERSION
		// DELETE BEFORE LOADING TO HOST

		$route_arr = explode('/', $routeurl);

		foreach (self::$langs as $lang_type) {

			if($lang_type['lang_code'] == $route_arr[0]) {
				$lang_type_url = '';
				setcookie("lang", $route_arr[0], time() + 60 * 60 * 24 * 365, '/');
				//$routeurl = substr($routeurl, strlen($route_arr[0]));
			}
		}

		//

		if($lang_type_url != '') {
			foreach (self::$langs as $lang_type) {

				if($lang_type['lang_code'] == $lang_type_url) {
					$routeurl = $lang_type_url.'/'.$routeurl;
				}
			}
		}

		$this->router = new UhCmsRouter( $routeurl, MVC_PATH, $this->config );
	}

	public static function getDbInstance() {
		return self::$db;
	}

	public static function getSesInstance() {
		return self::$ses;
	}

	public static function getMailerInstance() {
		return self::$mailer;
	}

	public static function getLocalizerInstance() {
		return self::$localize;
	}

	public static function getLangs() {
		return self::$langs;
	}

	public static function getLangId() {
		return self::$LangId;
	}

	public static function setLangId($id) {
		self::$LangId = $id;
	}

	public static function getLangCode() {
		$lang_code = "ru";

		for($i=0; $i<count(self::$langs); $i++) {

			if( self::$langs[$i]['id'] == self::$LangId ) {
				$lang_code = self::$langs[$i]['lang_code'];
				break;
			}
		}

		return $lang_code;
	}

	protected function showLog($str) {
		if( $this->config['DEBUG_ON'] ){
			//echo "App log: ".$str."<br>";
		}
	}

	public static function getStatusBox() {
		return self::$statusBox;
	}
}