<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UhCmsOdnoklassniki{
	protected	$data;
	
	protected	$app_id;
	protected	$secret_id;
	protected	$app_key;
	protected	$user_id;
	
	function __construct($appid, $secretid, $appkey)
	{
		$this->app_id = $appid;
		$this->secret_id = $secretid;
		$this->app_key = $appkey;
	}		
	
	function __set($pname, $val){
		$this->data[$pname] = $val;
	}
	
	function __get($pname){
		return (isset($this->data[$pname]) ? $this->data[$pname] : 0);
	}

	function __isset($pname){
		return isset($this->data[$pname]);
	}	
	
	public function calc_signature($params, $token)
	{
		$strpar = '';
		foreach($params as $pk => $pv)
		{
			if( $pk == 'sig' || $pk == 'access_token' )
				continue;
			
			$strpar .= $pk.'='.$pv;
		}
		
		//echo "<br>Par string: <br>".$strpar."<br><br>";
				
		//$strpar = 'application_key='.$this->app_key.'method='.$method;
		$strtoken = md5($token.$this->secret_id);
		
		return strtolower(md5($strpar.$strtoken));
	}
	
	public function makeLoginLink($redirurl)
	{		
		$OK_LOGIN_URL = 'http://www.odnoklassniki.ru/oauth/authorize';
		
		$params = array(
			'client_id'     => $this->app_id, //FB_APP_ID,
			'redirect_uri'  => $redirurl,
			'response_type' => 'code',			
			'state' => UhCmsApp::getSesInstance()->sesid(),
			//'scope'         => 'id,email,name,first_name,last_name,link,gender,user_birthday,verified'
			'scope'         => 'VALUABLE_ACCESS'
		);
		
		
		$OK_LINK = $OK_LOGIN_URL."?".urldecode(http_build_query($params));
		
		return $OK_LINK;
	}
	
	public function setSessionToken($token)
	{
		$this->access_token = $token;
	}
	
	public function setUserId($user_id)
	{
		$this->user_id = $user_id;
	}
	
	public function getSessionToken($redirurl, $code)
	{
		$OK_TOKEN_URL = 'https://api.odnoklassniki.ru/oauth/token.do';
		
		$params = array(
			'client_id'     => $this->app_id, //FB_APP_ID,
			'client_secret' => $this->secret_id,
			'redirect_uri'  => $redirurl,
			'grant_type'	=> 'authorization_code',
			'code'          => $code
		);		
		
		// Создать контекст и инициализировать POST запрос
		$context = stream_context_create(array(
			'http' => array(
					'method' => 'POST',
					'header' => 'Content-Type: application/x-www-form-urlencoded' . PHP_EOL,
					'content' => http_build_query($params),
			),
		));
		
		$ok_res = file_get_contents($OK_TOKEN_URL, false, $context);
		
		// $ok_res."<br>";
		
		$tokenObj = null;
		//parse_str($vk_res, $tokenInfo);
		
		$tokenObj = json_decode($ok_res);
		
		//echo "<br><b>TokenObj</b><br>";
		//var_dump($tokenObj);		
		
		$token_data = Array();
		$token_data['error'] = ( isset($tokenObj->error) ? $tokenObj->error : null );
		$token_data['error_description'] = ( isset($tokenObj->error_description) ? $tokenObj->error_description : '' );
		$token_data['access_token'] = ( isset($tokenObj->access_token) ? $tokenObj->access_token : null );
		//$token_data['user_id'] = ( isset($tokenObj->access_token) ? $tokenObj->user_id : 0 );
		$token_data['token_type'] = ( isset($tokenObj->token_type) ? $tokenObj->token_type : '' );
		
		//echo "<br><b>Token data</b><br>";
		//var_dump($token_data);
		//echo "<br>";
		
		$this->access_token = $token_data['access_token']; //$tokenInfo['access_token'];
		
		return $token_data;
	}
	
	private function getMe($params)
	{
		$OK_ME_URL = 'http://api.ok.ru/fb.do';
		
		$url_req = $OK_ME_URL.'?'.urldecode(http_build_query($params));
		
		$userInfo = json_decode(file_get_contents($url_req), true);
		
		return $userInfo;
	}
	
	public function getUserPermissions()
	{
		$params = array(
			'application_key' => $this->app_key,
			'fields' => 'permissions',
			'format' => 'JSON',
			'method' => 'users.hasAppPermission',					
			'access_token' => ( $token != "" ? $token : $this->access_token ),
			'sig' => ''
		);
		
		$sig = $this->calc_signature($params, $token);
		$params['sig'] = $sig;
		
		return $this->getMe($params);
	}
	
	public function getCurUserInfo($token)
	{			
		$params = array(
			'application_key' => $this->app_key,
			//'fields' => 'uid,first_name,last_name,name,gender,birthday,location,pic240min,city,url_profile,photo_id',
			'fields' => 'UID,FIRST_NAME,LAST_NAME,NAME,GENDER,BIRTHDAY,LOCATION,PIC_1,PIC_2,PHOTO_ID',
			'format' => 'JSON',
			//'session_key' => '',
			'method' => 'users.getCurrentUser',					
			'access_token' => ( $token != "" ? $token : $this->access_token ),
			'sig' => ''
		);
		
		$sig = $this->calc_signature($params, $token);
		
		//echo "<br><b>My signatute:</b> ".$sig."<br>";		
		$params['sig'] = $sig;
		
		return $this->getMe($params);
	}
		
	public function getUserInfo($token = "", $user_id = 0)
	{
		$params = array(
			'application_key' => '',
			'fields' => 'UID,FIRST_NAME,LAST_NAME,NAME,GENDER,BIRTHDAY,LOCATION,PIC_1,PIC_2,PHOTO_ID',
			'format' => 'JSON',
			'method' => 'users.getInfo',			
			'uids' => $user_id,
			'access_token' => ( $token != "" ? $token : $this->access_token ),
			'sig' => ''
		);
		
		$sig = $this->calc_signature($params, $token);
		$params['sig'] = $sig;
		
		return $this->getMe($params);				
	}
	
	/*
	public function getUserAvatar($token = "")
	{
		$params = array(
			'user_ids' => $user_id,
			'fields' => 'photo_200',
			'name_case' => 'Nom',
			'v' => '5.32',
			'access_token' => ( $token != "" ? $token : $this->access_token )			
		);
		
		return $this->getMe($params);
	}
		
	public function getUserPhotos($token = "")
	{
		$params = array(
				'fields' => 'albums',
				'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		return $this->getMe($params);		
	}
	
	public function getUserFriends($token = "")
	{
		//$FB_ME_URL = 'https://graph.facebook.com/me/friend-list';
	
		//$params = array('access_token' => ( $token != "" ? $token : $this->access_token ));
		
		$params = array(
				'fields' => 'friend-list',
				'access_token' => ( $token != "" ? $token : $this->access_token )
		);
		
		return $this->getMe($params);
	
		//$url_req = $FB_ME_URL.'?'.urldecode(http_build_query($params));	
		//echo "<br>".$url_req."<br>";
	
		//$userInfo = file_get_contents($url_req);	
		//$userInfo = json_decode(file_get_contents($FB_ME_URL.'?'.urldecode(http_build_query($params))), true);
	
		//return $userInfo;
	}
	*/
}
?>