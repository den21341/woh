<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

abstract class FactoryAbstract
{
    /**
     * @var array
     */
    protected static $instances = array();

    /**
     * Возвращает экземпляр класса, из которого вызван
     *
     * @return static
     */
    public static function getInstance()
    {
		$className = static::getClassName();
        if (!(self::$instances[$className] instanceof $className)) {
            self::$instances[$className] = new $className();
        }
        return self::$instances[$className];
    }

    /**
     * Удаляет экземпляр класса, из которого вызван
     *
     * @return void
     */
    public static function removeInstance()
    {
        $className = static::getClassName();
        if (array_key_exists($className, self::$instances)) {
            unset(self::$instances[$className]);
        }
    }

    /**
     * Возвращает имя экземпляра класса
     *
     * @return string
     */
    final protected static function getClassName()
    {
        return get_called_class();
    }

    /**
     * Конструктор закрыт
     */
    protected function __construct()
    {
    }

    /**
     * Клонирование запрещено
     */
    final protected function __clone()
    {
    }

    /**
     * Сериализация запрещена
     */
    final protected function __sleep()
    {
    }

    /**
     * Десериализация запрещена
     */
    final protected function __wakeup()
    {
    }
}

/**
 * Интерфейс пула одиночек
 */
abstract class Factory extends FactoryAbstract
{

    /**
     * Возвращает экземпляр класса, из которого вызван
     *
     * @return static
     */
    final public static function getInstance()
    {
        return parent::getInstance();
    }

    /**
     * Удаляет экземпляр класса, из которого вызван
     *
     * @return void
     */
    final public static function removeInstance()
    {
        parent::removeInstance();
    }
}

class UhCmsDb{

	/**
     * Used to store and provide instances for the getInstance() method
     */
	private $res;
	private $db_login;
	private $db_pass;
	private $db_database;
	private $db_post;
	private $db_host;
	private $is_connected;

	private $db;
	private $debug;
	private $last_error;
	private $last_error_str;

	function __construct(){
		$this->db_login = "";	//$login;
		$this->db_pass = "";	//$pass;
		$this->db_host = "";	//$host;
		$this->db_database = "";//$database;

		$this->is_connected = false;
		$this->db = null;
		$this->debug = false;
		$this->last_error = 0;
		$this->last_error_str = '';
	}

	function __destruct(){
		if( $this->is_connected && ($this->db != null) )
			$this->db->close();
	}


///// NOT USED NOW ///////////
	/**
     * Hidden magic clone method, make sure no instances of this class
     * can be cloned using the clone keyword
     */
    private function __clone() { }

	/**
     * Create a new instance if one doesn't exist with the key provided.
     * Once an instance has been created, or if it was already created, return it.
     * @param $key the key which the instance should be stored/retrieved
     * @return self
     */
    public static function getInstance($key) {

        // Check if an instance exists with this key already
        if(!array_key_exists($key, self::$instances)) {
            // instance doesn't exist yet, so create it
            self::$instances[$key] = new self();
        }

        // Return the correct instance of this class
        return self::$instances[$key];
    }
//////////////////////////////


	public function connect($host, $login, $pass, $database, $def_enc){
		$this->db_login = $login;
		$this->db_pass = $pass;
		$this->db_host = $host;
		$this->db_database = $database;

		$this->db = new mysqli($this->db_host, $this->db_login, $this->db_pass, $this->db_database);
		if ($this->db->connect_errno){
			$this->logConnectError("Не удалось подключиться к MySQL: ");
			return false;
		}

		$mysql_enc = "cp1251";
		if( $def_enc == "UTF-8" )
		{
			$mysql_enc = "utf8";
		}

		$this->db->query("SET character_set_client = '$mysql_enc'");
		$this->db->query("SET character_set_results = '$mysql_enc'");
		$this->db->query("SET character_set_connection = '$mysql_enc'");

		return ($this->is_connected = true);
	}

	private function logConnectError($err_text = ''){
		$this->last_error = $this->db->connect_errno;
		$this->last_error_str = $this->db->connect_error;

		if( $this->debug )
		{
			echo "<br>".$err_text.$this->last_error_str."<br>";
		}
	}

	private function logError($err_text = ''){
		$this->last_error = $this->db->errno;
		$this->last_error_str = $this->db->error;

		if( $this->debug )
		{
			echo "<br>".$err_text.$this->last_error_str."<br>";
		}
	}

	public function setDebug($debug_on = false){
		$this->debug = $debug_on;
	}

	public function getError(){
		return $this->last_error_str;
	}

	public function query($sql)
	{
		$out = Array();
		if($res = $this->db->query($sql))
		{
			//while( $row = $res->fetch_array(MYSQLI_ASSOC) )
			while( $row = $res->fetch_assoc() )
			{
				$out[] = $row;
			}
			$res->free();
		}
		else
		{
			$this->logError("Ошибка запроса MySQL: ");
		}

		return $out;
	}

	public function exec($sql)
	{
		if( $this->db->query($sql) )
		{
			// ok
			return true;
		}
		else
		{
			$this->logError("Ошибка запроса MySQL: ");
			return false;
		}
	}

	public function insert_id()
	{
		return $this->db->insert_id;
	}
}