<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class PromoModel extends PageModelAuth
{
	function __construct($config, $db, $LangId, $pagename="index")
	{
		parent::__construct($config, $db, $LangId, $pagename);		
	}
	
	public function pageLib()
	{
		return $this->page;
	}	
}
?>