<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class BonusModel extends PageModelAuth
{
	private $langs;

	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db, $LangId, "bonus");

		$this->langs = UhCmsApp::getLangs();
	}

	public function pageLib()
	{
		return $this->page;
	}

	protected function checkAuthor($author_id, $projid)
	{
		$query = "SELECT * FROM ".TABLE_CAT_BONUS." WHERE author_id='$author_id' AND id='$projid'";
		$res = $this->db->query($query);

		if( count($res) == 0 )
			return false;

		return true;
	}

	public function saveBonus($author_id, $projid, $reqd)
	{
		$is_author = $this->checkAuthor($author_id, $projid);
		if( !$is_author )
			return false;

		$stdt_parts = explode(".", $reqd->stdt);
		$stdt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $stdt_parts[2], $stdt_parts[1], $stdt_parts[0], 1, 0, 0);

		$endt_parts = explode(".", $reqd->endt);
		$endt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $endt_parts[2], $endt_parts[1], $endt_parts[0], 1, 0, 0);

		$query = "UPDATE ".TABLE_CAT_BONUS." SET obl_id='".$reqd->robl."', city_id='".$reqd->rcity."', modify_date=NOW(), 
			start_date='".$stdt_sql."', end_date='".$endt_sql."', difficulty='".$reqd->diff."', amount='".$reqd->pamount."' WHERE id='$projid'";
		$this->db->exec($query);

		$query = "UPDATE ".TABLE_CAT_BONUS_LANGS." SET title2='".addslashes($reqd->tit)."', descr0='".addslashes($reqd->descr)."', 
			descr='".addslashes($reqd->descrfull)."' WHERE item_id='$projid' AND lang_id='".$this->LangId."'";
		$this->db->exec($query);

		//if( $reqd->sect != 0 )
		//{
		//	$query = "UPDATE ".TABLE_CAT_CATITEMS." SET sect_id='".$reqd->sect."' WHERE item_id='".$projid."'";
		//	$this->db->exec($query);
		//}

		return true;
	}

	public function addBonus($author_id, $reqd)
	{
		$newpid = 0;

		$stdt_parts = explode(".", $reqd->stdt);
		$stdt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $stdt_parts[2], $stdt_parts[1], $stdt_parts[0], 1, 0, 0);

		$endt_parts = explode(".", $reqd->endt);
		$endt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $endt_parts[2], $endt_parts[1], $endt_parts[0], 1, 0, 0);

		//$aval = ($reqd->ptype == PROJ_TYPE_HUMAN ? $reqd->pamount : 0 );
		//$acost = ($reqd->ptype == PROJ_TYPE_MONEY ? $reqd->pamount : 0 );		
		//$currency_id = ( $reqd->ptype == PROJ_TYPE_MONEY ? $reqd->pamountizm : 0 );

		//$prof_id = PROJ_NEEDHELP;
		//if( $reqd->eventtype == 1 )		$prof_id = PROJ_EVENT;	
		//if( $reqd->thingtype == 1 )		$prof_id = PROJ_THINGS;

		$query = "INSERT INTO ".TABLE_CAT_BONUS." (author_id, obl_id, city_id, add_date, modify_date, start_date, end_date, 
				difficulty, amount, amount_val, title, url) 
				VALUES ('$author_id', '".$reqd->robl."', '".$reqd->rcity."', NOW(), NOW(), '".$stdt_sql."', '".$endt_sql."', 
				'".$reqd->diff."', '".$reqd->pamount."', '".$reqd->pamount."', '".addslashes($reqd->tit)."', '')";
		if( $this->db->exec($query) )
		{
			$newpid = $this->db->insert_id();
			for($i=0; $i<count($this->langs); $i++)
			{
				$query = "INSERT INTO ".TABLE_CAT_BONUS_LANGS." (item_id, lang_id, title2, descr0, descr, page_title, page_keywords, page_descr) VALUES 
				('$newpid', '".$this->langs[$i]['id']."', '".addslashes($reqd->tit)."', '".addslashes($reqd->descr)."', '".addslashes($reqd->descrfull)."', '', '', '')";
				if( !$this->db->exec($query) )
				{
					return 0;
				}
			}

			//$query = "INSERT INTO ".TABLE_CAT_CATITEMS." (item_id, sect_id) VALUES ('$newpid', '".$reqd->sect."')";
			//$this->db->exec($query);
		}
		//else 
		//	return $newpid;

		return $newpid;
	}

	public function delBonusPhoto($picid, $projid)
	{
		$query = "SELECT * FROM ".TABLE_CAT_BONUS_PICS." WHERE id='$picid'";
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			$pic = $res[0];

			if( $pic['item_id'] != $projid )
				return false;

			if( file_exists(stripslashes($pic['filename'])) )
				unlink(stripslashes($pic['filename']));
			if( file_exists(stripslashes($pic['filename_big'])) )
				unlink(stripslashes($pic['filename_big']));
			if( file_exists(stripslashes($pic['filename_thumb'])) )
				unlink(stripslashes($pic['filename_thumb']));
			if( file_exists(stripslashes($pic['filename_ico'])) )
				unlink(stripslashes($pic['filename_ico']));

			$query = "DELETE FROM ".TABLE_CAT_BONUS_PICS." WHERE id='$picid'";
			$this->db->exec($query);

			return true;
		}
		return false;
	}

	public function getAngelDay($id){
		$query = "SELECT ad_count, aw_count FROM ".TABLE_SHOP_BUYERS." WHERE id='$id'";

		$this->db->query($query);

		return false;
	}

	public function addBonusPhoto($author_id, $projid, $pfile, $ind=0)
	{
		$is_author = $this->checkAuthor($author_id, $projid);
		if( !$is_author )
			return false;

		$newpname = $projid."_".rand(10000,40000);

		$pext = ".jpg";
		$ppos = strrpos($pfile['name'], ".");
		if( $ppos !== FALSE )
			$pext = mb_substr($pfile['name'], $ppos);

		$src_path = BONUS_PHOTO_DIR.$newpname.mb_strtolower($pext);

		if( !move_uploaded_file($pfile['tmp_name'], $src_path) )
		{
			return false;
		}

		// Add record to database
		$psz = UhCmsUtils::GetImageSizeAll($src_path);
		$pw = 190;
		$ph = 190;
		if( $psz != null )
		{
			$pw = $psz['w'];
			$ph = $psz['h'];
		}

		$query = "INSERT INTO ".TABLE_CAT_BONUS_PICS." (item_id, filename, title, sort_num, src_w, src_h)
		VALUES ('$projid', '".addslashes($src_path)."', '', '$ind', '$pw', '$ph')";
		if( !$this->db->exec($query) )
			return false;

		$file_id = $this->db->insert_id();

		// Make filename for resized pics
		$newfname = $newpname.".jpg";

		// Make big image with watermark
		if( UhCmsUtils::ResizeImage($src_path, BONUS_BIG_DIR.$newfname, ".jpg", BBIGPIC_W, BBIGPIC_H, true) )
		{
			$psz = UhCmsUtils::GetImageSizeAll(BONUS_BIG_DIR.$newfname);
			$pw = BBIGPIC_W;
			$ph = BBIGPIC_H;
			if( $psz != null )
			{
				$pw = $psz['w'];
				$ph = $psz['h'];
			}

			$query = "UPDATE ".TABLE_CAT_BONUS_PICS." SET filename_big='".addslashes(BONUS_BIG_DIR.$newfname)."', big_w='$pw', big_h='$ph' WHERE id='".$file_id."'";
			$this->db->exec($query);
		}

		// Make thumb image with watermark
		if( UhCmsUtils::ResizeImage($src_path, BONUS_THUMB_DIR.$newfname, ".jpg", BTHUMB_W, BTHUMB_H, true) )
		{
			$psz = UhCmsUtils::GetImageSizeAll(BONUS_THUMB_DIR.$newfname);
			$pw = BTHUMB_W;
			$ph = BTHUMB_H;
			if( $psz != null )
			{
				$pw = $psz['w'];
				$ph = $psz['h'];
			}

			$query = "UPDATE ".TABLE_CAT_BONUS_PICS." SET filename_thumb='".addslashes(BONUS_THUMB_DIR.$newfname)."', thumb_w='$pw', thumb_h='$ph' WHERE id='".$file_id."'";
			$this->db->exec($query);
		}

		// Make ico image with watermark
		if( UhCmsUtils::ResizeImage($src_path, BONUS_SMTHUMB_DIR.$newfname, ".jpg", BSMTHUMB_W, BSMTHUMB_H, true) )
		{
			$psz = UhCmsUtils::GetImageSizeAll(BONUS_SMTHUMB_DIR.$newfname);
			$pw = BSMTHUMB_W;
			$ph = BSMTHUMB_H;
			if( $psz != null )
			{
				$pw = $psz['w'];
				$ph = $psz['h'];
			}

			$query = "UPDATE ".TABLE_CAT_BONUS_PICS." SET filename_ico='".addslashes(BONUS_SMTHUMB_DIR.$newfname)."', ico_w='$pw', ico_h='$ph' WHERE id='".$file_id."'";
			$this->db->exec($query);
		}

		return true;
	}

	public function add_BonusComment($author_id, $reqd)
	{
		$query = "INSERT INTO ".TABLE_CAT_BONUS_COMMENT." (item_id, author_id, add_date, author, author_email)
			VALUES ('".$reqd['projid']."', '$author_id', NOW(), '', '')";
		if( $this->db->exec($query) )
		{
			$newpid = $this->db->insert_id();
			for($i=0; $i<count($this->langs); $i++)
			{
				$query = "INSERT INTO ".TABLE_CAT_BONUS_COMMENT_LANGS." (item_id, lang_id, content) VALUES
					('$newpid', '".$this->langs[$i]['id']."', '".addslashes($reqd['comment'])."')";
				if( !$this->db->exec($query) )
				{
					return false;
				}
			}
		}

		return true;
	}

	public function set_UserAvatar($uinf, $fname, $ftmp)
	{
		if( $uinf['pic'] != "" )
		{
			//echo "Del: ".$uinf['pic']."<br>";
			@unlink($uinf['pic']);
		}

		if( $uinf['pic_sm'] != "" )
		{
			//echo "Del: ".$uinf['pic_sm']."<br>";
			@unlink($uinf['pic_sm']);
		}

		$newfname0 = $uinf['id']."_".rand(0,1000);

		$newfname = $newfname0.".jpg";
		$newfname_sm = $newfname0."_sm.jpg";

		if( UhCmsUtils::ResizeImage($ftmp, AVATAR_DIR.$newfname, ".jpg", AVATAR_W, AVATAR_H, true) )
		{
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic='".addslashes(AVATAR_DIR.$newfname)."' WHERE id='".$uinf['id']."'";
			$this->db->exec($query);
		}

		if( UhCmsUtils::ResizeImage($ftmp, AVATAR_DIR.$newfname_sm, ".jpg", AVATAR_SM_W, AVATAR_SM_H, true) )
		{
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic_sm='".addslashes(AVATAR_DIR.$newfname_sm)."' WHERE id='".$uinf['id']."'";
			$this->db->exec($query);
		}

		unlink($ftmp);
	}
}
?>