<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class AjxModel extends Model
{
	protected $LangId;	
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
	}
	
	public function get_UserInfo($uid, $catLib)
	{
		return $catLib->Buyer_Info($uid);	
	}		
	
	public function make_TreeData($uid, $catLib) {
		//$myprojlist = $catLib->Buyer_ReqList($uid, PROJ_NEEDHELP, REQ_STATUS_CONFIRM);	//$catLib->Item_List(0, $uid);
		$myhelplist = $catLib->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM);
		//print_r($myhelplist); die();
		/*$mylist = Array();
		/*for( $i=0; $i<count($myprojlist); ++$i ) {

			$mylist[] = Array(
					"type" => PROJ_NEEDHELP,
					"uname" => $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'],
					"utype" => $myprojlist[$i]['account_type'],
					"pt" => $myprojlist[$i]['amount_type'],
					"pamount" => $myprojlist[$i]['amount'],
					"ramount" => $myprojlist[$i]['req_amount']
			);
		}*/
		//print_r($myhelplist);
		for( $i=0; $i<count($myhelplist); ++$i ) {

			$pics = $catLib->getUserPic($myhelplist[$i]['uid']);

			$mylist[] = Array(
					"type" => PROJ_SENDHELP,
					"uname" => $myhelplist[$i]['name'].' '.$myhelplist[$i]['fname'],
					"utype" => $myhelplist[$i]['account_type'],
					"pt" => $myhelplist[$i]['amount_type'],
					"pamount" => $myhelplist[$i]['amount'],
					"ramount" => $myhelplist[$i]['req_amount'],
					"pic" => $pics[0]['pic_sm'],
					"uid" => $myhelplist[$i]['reciever_id']
			);
		}

		//shuffle($mylist);

		return $mylist;
	}
	
	public function make_GraphData($uid, $catLib)
	{
		$myprojlist = $catLib->Buyer_ReqList($uid, PROJ_NEEDHELP, REQ_STATUS_CONFIRM);
		$inlist = Array();
		for( $i=0; $i<count($myprojlist); ++$i )
		{
			$inlist[] = Array(
					"type" => PROJ_NEEDHELP,
					"uid" => $myprojlist[$i]['uid'],
					"uname" => ( $myprojlist[$i]['account_type'] == USR_TYPE_PERS ? $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'] : $myprojlist[$i]['orgname'] ),
					//"uname" => $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'],
					"utype" => $myprojlist[$i]['account_type'],
					"pt" => $myprojlist[$i]['amount_type'],
					"pamount" => $myprojlist[$i]['amount'],
					"ramount" => $myprojlist[$i]['req_amount']
			);
		}
		
		$myhelplist = $catLib->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM);
		$outlist = Array();
		for( $i=0; $i<count($myhelplist); $i++ )
		{
			$myhelplist1 = $catLib->Buyer_ReqList($myhelplist[$i]['reciever_id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);
			
			$outlist[] = Array(
					"type" => PROJ_SENDHELP,
					"uid" => $myhelplist[$i]['uid'],
					"uname" => ( $myhelplist[$i]['account_type'] == USR_TYPE_PERS ? $myhelplist[$i]['name'].' '.$myhelplist[$i]['fname'] : $myhelplist[$i]['orgname'] ),
					"utype" => $myhelplist[$i]['account_type'],
					"pt" => $myhelplist[$i]['amount_type'],
					"pamount" => $myhelplist[$i]['amount'],
					"ramount" => $myhelplist[$i]['req_amount'],
					"outdata" => $myhelplist1
			);
					
		}
		
		$res = Array("in" => $inlist, "out" => $outlist);
		
		return $res;
	}
	
	private function _pack_udata($uowner, $uid, $utype, $uname, $uorg, $upic, $level=-2, $skip=0, $sind=1, $debug=false)
	{
		$inf = Array();
		$inf['gid'] = "u".$uid;
		$inf['label'] = $uname;
		$inf['title'] = ( $utype == USR_TYPE_PERS ? $uname : $uorg );
		//$inf['group'] = ( $uowner ? "owner" : "user" );
		$inf['nd_type'] = ( $uowner ? 'mnusr' : 'usr' ); //$utype;
		$inf['lvl'] = ( $uowner ? '0' : ($level > 0 ? $level : '-2') );
		$inf['size'] = ( $uowner ? '32' : '24' );
		$inf['p_type'] = "t".$sind;
		//$inf['child_skip'] = 0;
		$inf['info'] = Array( 
				"type" => 1, 
				"id" => $uid, 
				"child_skip" => $skip, //0,
				"url_ava" => ( $upic != "" ? PICHOST.$upic : "" ),
				"url_view" => WWWHOST."users/viewrev/".$uid."/",
				"utype" => $utype,				
				"uname" => ( $utype == USR_TYPE_PERS ? $uname : $uorg ), 
				"title" => "accceptor",
				"more_show" => ( $uowner ? false : ($level > 0 ? true : false) ),
				"more_load" => false 
		);
		
		if( $debug )
			echo $inf['gid'].":".$sind.":".$inf['p_type'].", ";
		
		return $inf;
	}
	
	private function _pack_pdata($pid, $ptype, $ptitle, $pamount_type, $pamount, $level = -1, $sind=1)
	{
		$inf = Array();
		$inf['gid'] = "p".$pid;
		$inf['label'] = $ptitle;
		$inf['title'] = $ptitle;
		//$inf['group'] = "project";
		$inf['nd_type'] = 'prj'; //$ptype;
		$inf['p_type'] = "t".$sind; //(($level > 0 ? 5 : 0) + 2+$pamount_type); //$ptype;
		//$inf['p_type'] = "t2";
		$inf['lvl'] = $level;
		//$inf['child_skip'] = 0;
		$inf['info'] = Array( 
			"type" => $ptype, 
			"id" => $pid, 
			"child_skip" => 0,
			"ptype" => $pamount_type, 
			"amount" => $pamount, 
			"name" => $ptitle,
			"title" => $ptitle,
			"content" => "",
			"url_view" => WWWHOST."proj/view/".$pid."/",
			"more_show" => true
		);
		
		return $inf;
	}
	
	private function _pack_edge($from, $to, $projid, $title, $req_amount_type=-1, $req_amount=0, $dttime, $sind = 1)
	{
		$inf = Array();
		$inf['id'] = "e-".$from."-".$to;
		$inf['from'] = $from;
		$inf['to'] = $to;
		$inf['projid'] = $projid;
		$inf['label'] = $title;
		$inf['title'] = $title;
		//$inf['group'] = "edge";
		$inf['projid'] = $projid;
		$inf['type'] = "";
		$inf['p_type'] = "t".$sind;
		$inf['info'] = Array("type" => 0, "id" => 0, "amount_type" => $req_amount_type, "amount" => $req_amount, "time_date" => $dttime /*date("Y-m-d H:i:s")*/);
		
		return $inf;
	}
	
	public function make_GraphData2($uid, $pid, $catLib, $umaxnum, $pmaxnum )
	{
		$ulist = Array();
		$ulist_by_id = Array();
		
		$plist = Array();
		$plist_by_id = Array();
		$plist_by_id_ind = Array();
		
		$plistall = Array();
		$plistall_by_id = Array();
		
		$elist = Array();
		
		if( $uid != 0 )
		{		
			//
			$sect_ind = 1;
			$sect_ind_by_id = Array();
			$sect0 = $catLib->Catalog_SectLevel(0, 0);
			for( $i=0; $i<count($sect0); $i++ )
			{
				$sect_ind_by_id[$sect0[$i]['id']] = $sect_ind++;
			}
			
			// Owner info
			$oinf = $catLib->Buyer_Info($uid);
			
			if( empty($oinf['id']) )
			{
				// Возможно сделать віход раньше
			}
			else
			{						
				if( empty($ulist_by_id[$oinf['id']]) )
				{
					$uinf = $this->_pack_udata(
							true,
							$oinf['id'],
							$oinf['account_type'],
							$oinf['name'].' '.$oinf['fname'],
							( $oinf['account_type'] == USR_TYPE_PERS ? $oinf['name'].' '.$oinf['fname'] : $oinf['orgname'] ),
							$oinf['pic_sm']
					);
						
					$ulist[] = $uinf;
						
					$ulist_by_id[$oinf['id']] = $uinf; //count($ulist)-1;
				}
				
				// Извлечь все оказанные помощи (со статусом "подтверждено") по всем моим проектам. 
				// Позвращается информация по проекту, объем оказанной помощи и кто её оказал
				
				$totpreqnum = $catLib->Buyer_ReqNum($uid, PROJ_NEEDHELP, REQ_STATUS_CONFIRM);
				
				$myprojlist = $catLib->Buyer_ReqList($uid, PROJ_NEEDHELP, REQ_STATUS_CONFIRM, "", 0, $pmaxnum);
				$inlist = Array();
				for( $i=0; $i<count($myprojlist); $i++ )
				{
					$rootsid = $catLib->Catalog_ItemRootSect($myprojlist[$i]['item_id']);
					
					$tcolor = ( ( isset($sect_ind_by_id[$rootsid]) ? $sect_ind_by_id[$rootsid] : 1 ) % 12) + 1;
					
					if( empty($ulist_by_id[$myprojlist[$i]['uid']]) )
					{
						$uinf = $this->_pack_udata(
								( $uid == $myprojlist[$i]['uid'] ),
								$myprojlist[$i]['uid'], 
								$myprojlist[$i]['account_type'], 
								$myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'], 
								( $myprojlist[$i]['account_type'] == USR_TYPE_PERS ? $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'] : $myprojlist[$i]['orgname'] ), 
								$myprojlist[$i]['pic_sm'],
								-2, 0, $tcolor
								);
						
						$ulist[] = $uinf;
						
						$ulist_by_id[$myprojlist[$i]['uid']] = $uinf; //count($ulist)-1;
					}
					
					if( empty($plist_by_id[$myprojlist[$i]['item_id']]) )
					{											
						$pinf = $this->_pack_pdata(
								$myprojlist[$i]['item_id'], 
								PROJ_NEEDHELP, 
								$myprojlist[$i]['title'], 
								$myprojlist[$i]['amount_type'], 
								$myprojlist[$i]['amount'],
								-1,
								$tcolor
								);
						
						$plist[] = $pinf;
						$plistall[] = $pinf;
						
						$plist_by_id[$myprojlist[$i]['item_id']] = $pinf; //count($plist)-1;
						$plist_by_id_ind[$myprojlist[$i]['item_id']] = (count($plist)-1);
						$plistall_by_id[$myprojlist[$i]['item_id']] = $pinf;
					}
					
					
					$einf = $this->_pack_edge(							
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							$ulist_by_id[$oinf['id']]['gid'],
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							"Получил помощь",
							$myprojlist[$i]['req_type'], $myprojlist[$i]['req_amount'], $myprojlist[$i]['add_date'],
							$tcolor);				
					$elist[] = $einf;
					
					$einf = $this->_pack_edge(
							$ulist_by_id[$myprojlist[$i]['uid']]['gid'],
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							"Оказал помощь", 
							$myprojlist[$i]['req_type'], $myprojlist[$i]['req_amount'], $myprojlist[$i]['add_date'],
							$tcolor);					
					$elist[] = $einf;
					
					$plist[$plist_by_id_ind[$myprojlist[$i]['item_id']]]['info']['child_skip']++;
					
					// Єтот массив уже не нужен
					$inlist[] = Array(
							"type" => PROJ_NEEDHELP,
							"uid" => $myprojlist[$i]['uid'],
							"uname" => ( $myprojlist[$i]['account_type'] == USR_TYPE_PERS ? $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'] : $myprojlist[$i]['orgname'] ),
							//"uname" => $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'],
							"utype" => $myprojlist[$i]['account_type'],
							"pt" => $myprojlist[$i]['amount_type'],
							"pamount" => $myprojlist[$i]['amount'],
							"ramount" => $myprojlist[$i]['req_amount']
					);
				}
			
				// Извлечь вче помощи, что оказал сам пользователь по чужим проектам
				$tothreqnum = $catLib->Buyer_ReqNum($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM);
				
				$myhelplist = $catLib->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM, "", 0, $umaxnum);
				$outlist = Array();
				for( $i=0; $i<count($myhelplist); $i++ )
				{
					/*
					$myhelplist1 = $catLib->Buyer_ReqList($myhelplist[$i]['reciever_id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);
						
					$outlist[] = Array(
							"type" => PROJ_SENDHELP,
							"uid" => $myhelplist[$i]['uid'],
							"uname" => ( $myhelplist[$i]['account_type'] == USR_TYPE_PERS ? $myhelplist[$i]['name'].' '.$myhelplist[$i]['fname'] : $myhelplist[$i]['orgname'] ),
							"utype" => $myhelplist[$i]['account_type'],
							"pt" => $myhelplist[$i]['amount_type'],
							"pamount" => $myhelplist[$i]['amount'],
							"ramount" => $myhelplist[$i]['req_amount'],
							"outdata" => $myhelplist1
					);
					*/

					/////////////////////////////////////////////////////////////////////////////////////////////////
					// Go to one level deeper
					$uid2 = $myhelplist[$i]['uid'];
					
					$tothreqnum2 = $catLib->Buyer_ReqNum($uid2, PROJ_SENDHELP, REQ_STATUS_CONFIRM);
				
					$myhelplist2 = $catLib->Buyer_ReqList($uid2, PROJ_SENDHELP, REQ_STATUS_CONFIRM, "", 0, $umaxnum);
					//////////////////////////////////////////////////////////////////////////////////////////////////
					
					$rootsid = $catLib->Catalog_ItemRootSect($myhelplist[$i]['item_id']);				
					
					$tcolor = ( ( isset($sect_ind_by_id[$rootsid]) ? $sect_ind_by_id[$rootsid] : 1 ) % 12) + 1;

					if( empty($ulist_by_id[$myhelplist[$i]['uid']]) )
					{											
						$uinf = $this->_pack_udata(
								( $uid == $myhelplist[$i]['uid'] ),
								$myhelplist[$i]['uid'],
								$myhelplist[$i]['account_type'],
								$myhelplist[$i]['name'].' '.$myhelplist[$i]['fname'],
								( $myhelplist[$i]['account_type'] == USR_TYPE_PERS ? $myhelplist[$i]['name'].' '.$myhelplist[$i]['fname'] : $myhelplist[$i]['orgname'] ),
								$myhelplist[$i]['pic_sm'],
								2, ($tothreqnum2 > count($myhelplist2) ? $umaxnum : -1) /*0*/, $tcolor
						);
					
						$ulist[] = $uinf;
					
						$ulist_by_id[$myhelplist[$i]['uid']] = $uinf; //count($ulist)-1;
					}
						
					if( empty($plist_by_id[$myhelplist[$i]['item_id']]) )
					{											
						$pinf = $this->_pack_pdata(
								$myhelplist[$i]['item_id'],
								PROJ_NEEDHELP,
								$myhelplist[$i]['title'],
								$myhelplist[$i]['amount_type'],
								$myhelplist[$i]['amount'],
								1,
								$tcolor
						);
					
						// Add project to list
						$plistall[] = $pinf;
						$plistall_by_id[$myhelplist[$i]['item_id']] = $pinf; //count($plist)-1;
						
						//$plist[] = $pinf;					
						//$plist_by_id[$myhelplist[$i]['item_id']] = $pinf; //count($plist)-1;
					}
						
						
					/*
					$einf = $this->_pack_edge(
							$plist_by_id[$myhelplist[$i]['item_id']]['gid'],
							$ulist_by_id[$myhelplist[$i]['uid']]['gid'],						
							$plist_by_id[$myhelplist[$i]['item_id']]['gid'],
							"Получил помощь 2",
							$myhelplist[$i]['req_type'], $myhelplist[$i]['req_amount'], $myhelplist[$i]['add_date']);
					$elist[] = $einf;
						
					$einf = $this->_pack_edge(
							$ulist_by_id[$oinf['id']]['gid'],
							$plist_by_id[$myhelplist[$i]['item_id']]['gid'],
							$plist_by_id[$myhelplist[$i]['item_id']]['gid'],
							"Оказал помощь 2", 
							$myhelplist[$i]['req_type'], $myhelplist[$i]['req_amount'], $myhelplist[$i]['add_date']);
					$elist[] = $einf;
					*/
					
					$einf = $this->_pack_edge(
							$ulist_by_id[$oinf['id']]['gid'],
							$ulist_by_id[$myhelplist[$i]['uid']]['gid'],
							$plistall_by_id[$myhelplist[$i]['item_id']]['gid'],
							"Оказал помощь 2",
							$myhelplist[$i]['req_type'], $myhelplist[$i]['req_amount'], $myhelplist[$i]['add_date'],
							$tcolor);										
					$elist[] = $einf;
					
					
					/////////////////////////////////////////////////////////////////////////////////////////////////
					// Go to one level deeper
					$uid2 = $myhelplist[$i]['uid'];
					
					$tothreqnum2 = $catLib->Buyer_ReqNum($uid2, PROJ_SENDHELP, REQ_STATUS_CONFIRM);
				
					$myhelplist2 = $catLib->Buyer_ReqList($uid2, PROJ_SENDHELP, REQ_STATUS_CONFIRM, "", 0, $umaxnum);
					
					for( $i2=0; $i2<count($myhelplist2); $i2++ )
					{											
						$rootsid = $catLib->Catalog_ItemRootSect($myhelplist2[$i2]['item_id']);				
						
						$tcolor = ( ( isset($sect_ind_by_id[$rootsid]) ? $sect_ind_by_id[$rootsid] : 1 ) % 12) + 1;

						if( empty($ulist_by_id[$myhelplist2[$i2]['uid']]) )
						{											
							$uinf = $this->_pack_udata(
									( $uid == $myhelplist2[$i2]['uid'] ),
									$myhelplist2[$i2]['uid'],
									$myhelplist2[$i2]['account_type'],
									$myhelplist2[$i2]['name'].' '.$myhelplist2[$i2]['fname'],
									( $myhelplist2[$i2]['account_type'] == USR_TYPE_PERS ? $myhelplist2[$i2]['name'].' '.$myhelplist2[$i2]['fname'] : $myhelplist2[$i2]['orgname'] ),
									$myhelplist2[$i2]['pic_sm'],
									3, 0, $tcolor
							);
						
							$ulist[] = $uinf;
						
							$ulist_by_id[$myhelplist2[$i2]['uid']] = $uinf; //count($ulist)-1;
						}
							
						if( empty($plist_by_id[$myhelplist2[$i2]['item_id']]) )
						{											
							$pinf = $this->_pack_pdata(
									$myhelplist2[$i2]['item_id'],
									PROJ_NEEDHELP,
									$myhelplist2[$i2]['title'],
									$myhelplist2[$i2]['amount_type'],
									$myhelplist2[$i2]['amount'],
									1,
									$tcolor
							);
						
							// Add project to list
							$plistall[] = $pinf;
							$plistall_by_id[$myhelplist2[$i2]['item_id']] = $pinf; //count($plist)-1;														
						}																			
						
						$einf = $this->_pack_edge(
								$ulist_by_id[$myhelplist[$i]['uid']]['gid'],
								$ulist_by_id[$myhelplist2[$i2]['uid']]['gid'],
								$plistall_by_id[$myhelplist2[$i2]['item_id']]['gid'],
								"Оказал помощь 2",
								$myhelplist2[$i2]['req_type'], $myhelplist2[$i2]['req_amount'], $myhelplist2[$i2]['add_date'],
								$tcolor);										
						$elist[] = $einf;
					}
				}
			
				//$res = Array("in" => $inlist, "out" => $outlist);
			}
		}
		
		$res = Array("proj_req_tot" => $totpreqnum, "sendhelp_req_tot" => $tothreqnum,  "nodes" => array_merge($ulist, $plist), "edges" => $elist);
	
		return $res;
	}
	
	public function make_GraphDataRight($uid, $pid, $catLib, $skip=0, $count=5, $level=2)
	{
		$ulist = Array();
		$ulist_by_id = Array();
	
		$plist = Array();
		$plist_by_id = Array();
	
		$plistall = Array();
		$plistall_by_id = Array();
	
		$elist = Array();
	
		if( $uid != 0 )
		{
			$sect_ind = 1;
			$sect_ind_by_id = Array();
			$sect0 = $catLib->Catalog_SectLevel(0, 0);
			for( $i=0; $i<count($sect0); $i++ )
			{
				$sect_ind_by_id[$sect0[$i]['id']] = $sect_ind++;
			}
			
			// Owner info
			$oinf = $catLib->Buyer_Info($uid);
				
			if( empty($oinf['id']) )
			{
				// Возможно сделать віход раньше
			}
			else
			{
				if( empty($ulist_by_id[$oinf['id']]) )
				{
					$uinf = $this->_pack_udata(
							( $level != 0 ? false : true ),
							$oinf['id'],
							$oinf['account_type'],
							$oinf['name'].' '.$oinf['fname'],
							( $oinf['account_type'] == USR_TYPE_PERS ? $oinf['name'].' '.$oinf['fname'] : $oinf['orgname'] ),
							$oinf['pic_sm'],
							$level
					);
	
					$ulist[] = $uinf;
	
					$ulist_by_id[$oinf['id']] = $uinf; //count($ulist)-1;
				}
	
				$tothreqnum = $catLib->Buyer_ReqNum($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM);
				
				// Извлечь вcе помощи, что оказал сам пользователь по чужим проектам
				$myhelplist = $catLib->Buyer_ReqList($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM, "", ($skip/$count), $count);
				$outlist = Array();
				for( $i=0; $i<count($myhelplist); $i++ )
				{
					/*
						$myhelplist1 = $catLib->Buyer_ReqList($myhelplist[$i]['reciever_id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);
	
						$outlist[] = Array(
						"type" => PROJ_SENDHELP,
						"uid" => $myhelplist[$i]['uid'],
						"uname" => ( $myhelplist[$i]['account_type'] == USR_TYPE_PERS ? $myhelplist[$i]['name'].' '.$myhelplist[$i]['fname'] : $myhelplist[$i]['orgname'] ),
						"utype" => $myhelplist[$i]['account_type'],
						"pt" => $myhelplist[$i]['amount_type'],
						"pamount" => $myhelplist[$i]['amount'],
						"ramount" => $myhelplist[$i]['req_amount'],
						"outdata" => $myhelplist1
						);
						*/
					
					$rootsid = $catLib->Catalog_ItemRootSect($myhelplist[$i]['item_id']);
					
					$tcolor = ( ( isset($sect_ind_by_id[$rootsid]) ? $sect_ind_by_id[$rootsid] : 1 ) % 12) + 1;
	
					if( empty($ulist_by_id[$myhelplist[$i]['uid']]) )
					{
						$uinf = $this->_pack_udata(
								( $uid == $myhelplist[$i]['uid'] ),
								$myhelplist[$i]['uid'],
								$myhelplist[$i]['account_type'],
								$myhelplist[$i]['name'].' '.$myhelplist[$i]['fname'],
								( $myhelplist[$i]['account_type'] == USR_TYPE_PERS ? $myhelplist[$i]['name'].' '.$myhelplist[$i]['fname'] : $myhelplist[$i]['orgname'] ),
								$myhelplist[$i]['pic_sm'],
								$level+1, 0, $tcolor
						);
							
						$ulist[] = $uinf;
							
						$ulist_by_id[$myhelplist[$i]['uid']] = $uinf; //count($ulist)-1;
					}
	
					if( empty($plist_by_id[$myhelplist[$i]['item_id']]) )
					{												
						$pinf = $this->_pack_pdata(
								$myhelplist[$i]['item_id'],
								PROJ_NEEDHELP,
								$myhelplist[$i]['title'],
								$myhelplist[$i]['amount_type'],
								$myhelplist[$i]['amount'],
								1,
								$tcolor
						);
							
						// Add project to list
						$plistall[] = $pinf;
						$plistall_by_id[$myhelplist[$i]['item_id']] = $pinf; //count($plist)-1;
	
						//$plist[] = $pinf;
						//$plist_by_id[$myhelplist[$i]['item_id']] = $pinf; //count($plist)-1;
					}
	
	
					/*
						$einf = $this->_pack_edge(
						$plist_by_id[$myhelplist[$i]['item_id']]['gid'],
						$ulist_by_id[$myhelplist[$i]['uid']]['gid'],
						$plist_by_id[$myhelplist[$i]['item_id']]['gid'],
						"Получил помощь 2",
						$myhelplist[$i]['req_type'], $myhelplist[$i]['req_amount'], $myhelplist[$i]['add_date']);
						$elist[] = $einf;
	
						$einf = $this->_pack_edge(
						$ulist_by_id[$oinf['id']]['gid'],
						$plist_by_id[$myhelplist[$i]['item_id']]['gid'],
						$plist_by_id[$myhelplist[$i]['item_id']]['gid'],
						"Оказал помощь 2",
						$myhelplist[$i]['req_type'], $myhelplist[$i]['req_amount'], $myhelplist[$i]['add_date']);
						$elist[] = $einf;
						*/
						
					$einf = $this->_pack_edge(
							$ulist_by_id[$oinf['id']]['gid'],
							$ulist_by_id[$myhelplist[$i]['uid']]['gid'],
							$plistall_by_id[$myhelplist[$i]['item_id']]['gid'],
							"Оказал помощь 3",
							$myhelplist[$i]['req_type'], $myhelplist[$i]['req_amount'], $myhelplist[$i]['add_date'],
							$tcolor);
					$elist[] = $einf;
				}
					
				//$res = Array("in" => $inlist, "out" => $outlist);
			}
		}
	
		$res = Array("tot_req_num" => $tothreqnum, "nodes" => array_merge($ulist, $plist), "edges" => $elist);
	
		return $res;
	}
	
	public function make_GraphDataProj($uid, $pid, $catLib, $skip=0, $count=5, $level=2 )
	{
		$ulist = Array();
		$ulist_by_id = Array();
		
		$plist = Array();
		$plist_by_id = Array();
		
		$plistall = Array();
		$plistall_by_id = Array();
		
		$elist = Array();
		
		$totpreqnum = 0;
		
		if( $uid != 0 )
		{		
			$sect_ind = 1;
			$sect_ind_by_id = Array();
			$sect0 = $catLib->Catalog_SectLevel(0, 0);
			for( $i=0; $i<count($sect0); $i++ )
			{
				$sect_ind_by_id[$sect0[$i]['id']] = $sect_ind++;
			}
			
			// Owner info
			$oinf = $catLib->Buyer_Info($uid);
			
			if( empty($oinf['id']) )
			{
				// Возможно сделать віход раньше
			}
			else
			{						
				if( empty($ulist_by_id[$oinf['id']]) )
				{
					$uinf = $this->_pack_udata(
							true,
							$oinf['id'],
							$oinf['account_type'],
							$oinf['name'].' '.$oinf['fname'],
							( $oinf['account_type'] == USR_TYPE_PERS ? $oinf['name'].' '.$oinf['fname'] : $oinf['orgname'] ),
							$oinf['pic_sm']
					);
						
					$ulist[] = $uinf;
						
					$ulist_by_id[$oinf['id']] = $uinf; //count($ulist)-1;
				}
				
				// Извлечь все оказанные помощи (со статусом "подтверждено") по всем моим проектам. 
				// Позвращается информация по проекту, объем оказанной помощи и кто её оказал
				
				$totpreqnum = $catLib->Buyer_ReqNum($uid, PROJ_NEEDHELP, REQ_STATUS_CONFIRM);
				
				$myprojlist = $catLib->Buyer_ReqList($uid, PROJ_NEEDHELP, REQ_STATUS_CONFIRM, "", ($skip/$count), $count);
				$inlist = Array();
				for( $i=0; $i<count($myprojlist); $i++ )
				{
					$rootsid = $catLib->Catalog_ItemRootSect($myprojlist[$i]['item_id']);
					
					$tcolor = (( isset($sect_ind_by_id[$rootsid]) ? $sect_ind_by_id[$rootsid] : 1 ) % 12) + 1;
					
					if( empty($ulist_by_id[$myprojlist[$i]['uid']]) )
					{
						$uinf = $this->_pack_udata(
								( $uid == $myprojlist[$i]['uid'] ),
								$myprojlist[$i]['uid'], 
								$myprojlist[$i]['account_type'], 
								$myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'], 
								( $myprojlist[$i]['account_type'] == USR_TYPE_PERS ? $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'] : $myprojlist[$i]['orgname'] ), 
								$myprojlist[$i]['pic_sm']
								-2, 0, $tcolor);
						
						$ulist[] = $uinf;
						
						$ulist_by_id[$myprojlist[$i]['uid']] = $uinf; //count($ulist)-1;
					}
					
					if( empty($plist_by_id[$myprojlist[$i]['item_id']]) )
					{											
						$pinf = $this->_pack_pdata(
								$myprojlist[$i]['item_id'], 
								PROJ_NEEDHELP, 
								$myprojlist[$i]['title'], 
								$myprojlist[$i]['amount_type'], 
								$myprojlist[$i]['amount'],
								-1,
								$tcolor
								);
						
						$plist[] = $pinf;
						$plistall[] = $pinf;
						
						$plist_by_id[$myprojlist[$i]['item_id']] = $pinf; //count($plist)-1;
						$plistall_by_id[$myprojlist[$i]['item_id']] = $pinf;
					}
					
					
					$einf = $this->_pack_edge(							
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							$ulist_by_id[$oinf['id']]['gid'],
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							"Получил помощь",
							$myprojlist[$i]['req_type'], $myprojlist[$i]['req_amount'], $myprojlist[$i]['add_date'],
							$tcolor);				
					$elist[] = $einf;
					
					$einf = $this->_pack_edge(
							$ulist_by_id[$myprojlist[$i]['uid']]['gid'],
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							"Оказал помощь", 
							$myprojlist[$i]['req_type'], $myprojlist[$i]['req_amount'], $myprojlist[$i]['add_date'],
							$tcolor);
					$elist[] = $einf;
					
					// Єтот массив уже не нужен
					$inlist[] = Array(
							"type" => PROJ_NEEDHELP,
							"uid" => $myprojlist[$i]['uid'],
							"uname" => ( $myprojlist[$i]['account_type'] == USR_TYPE_PERS ? $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'] : $myprojlist[$i]['orgname'] ),
							//"uname" => $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'],
							"utype" => $myprojlist[$i]['account_type'],
							"pt" => $myprojlist[$i]['amount_type'],
							"pamount" => $myprojlist[$i]['amount'],
							"ramount" => $myprojlist[$i]['req_amount']
					);
				}
										
				//$res = Array("in" => $inlist, "out" => $outlist);
			}
		}
		
		$res = Array("proj_req_tot" => $totpreqnum, "nodes" => array_merge($ulist, $plist), "edges" => $elist);
	
		return $res;
	}
	
	public function make_GraphDataProjItems($pid, $catLib, $skip=0, $count=5, $level=2 )
	{
		$ulist = Array();
		$ulist_by_id = Array();
	
		$plist = Array();
		$plist_by_id = Array();
	
		$plistall = Array();
		$plistall_by_id = Array();
	
		$elist = Array();
		
		$totpreqnum = 0;
	
		if( $pid != 0 )
		{
			$sect_ind = 1;
			$sect_ind_by_id = Array();
			$sect0 = $catLib->Catalog_SectLevel(0, 0);
			for( $i=0; $i<count($sect0); $i++ )
			{
				$sect_ind_by_id[$sect0[$i]['id']] = $sect_ind++;
			}
			
			$pinfo = $catLib->Item_Info($pid);
				
			// Owner info
			$oinf = $catLib->Buyer_Info($pinfo['author_id']);
				
			if( empty($oinf['id']) )
			{
				// Возможно сделать віход раньше
			}
			else
			{
				if( empty($ulist_by_id[$oinf['id']]) )
				{
					$uinf = $this->_pack_udata(
							true,
							$oinf['id'],
							$oinf['account_type'],
							$oinf['name'].' '.$oinf['fname'],
							( $oinf['account_type'] == USR_TYPE_PERS ? $oinf['name'].' '.$oinf['fname'] : $oinf['orgname'] ),
							$oinf['pic_sm']
					);
	
					$ulist[] = $uinf;
	
					$ulist_by_id[$oinf['id']] = $uinf; //count($ulist)-1;
				}
				
				$rootsid = $catLib->Catalog_ItemRootSect($pid);
												
				$tcolor = (( isset($sect_ind_by_id[$rootsid]) ? $sect_ind_by_id[$rootsid] : 1 ) % 12) + 1;
				
				//var_dump($sect_ind_by_id);
				//echo "<br>";
				//
				//echo $rootsid.":".$tcolor;
	
				// Извлечь все оказанные помощи (со статусом "подтверждено") по всем моим проектам.
				// Позвращается информация по проекту, объем оказанной помощи и кто её оказал
	
				//$totpreqnum = $catLib->Buyer_ReqNum($uid, PROJ_NEEDHELP, REQ_STATUS_CONFIRM);
				$totdata = $catLib->Item_ReqCollected($pid, REQ_STATUS_CONFIRM);
				$totpreqnum = $totdata['num'];
	
				//$myprojlist = $catLib->Buyer_ReqList($uid, PROJ_NEEDHELP, REQ_STATUS_CONFIRM, "", ($skip/$count), $count);
				
				$myhelp = $catLib->Item_ReqList($pid, REQ_STATUS_CONFIRM, $skip, $count);
				
				// Add project item
				$pinf = $this->_pack_pdata(
						$pid,
						PROJ_NEEDHELP,
						$pinfo['title2'],
						$pinfo['amount_type'],
						$pinfo['amount'],
						-1,
						$tcolor
				);
				
				$plist[] = $pinf;
				$plistall[] = $pinf;
				
				$plist_by_id[$pid] = $pinf; //count($plist)-1;
				$plistall_by_id[$pid] = $pinf;
				
				$inlist = Array();
				for( $i=0; $i<count($myhelp); $i++ )
				{											
					if( empty($ulist_by_id[$myhelp[$i]['author_id']]) )
					{
						$uinf = $this->_pack_udata(
								( $pinfo['author_id'] == $myhelp[$i]['author_id'] ),
								$myhelp[$i]['author_id'],
								$myhelp[$i]['account_type'],
								$myhelp[$i]['name'].' '.$myhelp[$i]['fname'],
								( $myhelp[$i]['account_type'] == USR_TYPE_PERS ? $myhelp[$i]['name'].' '.$myhelp[$i]['fname'] : $myhelp[$i]['orgname'] ),
								$myhelp[$i]['pic_sm'],
								-2, 0, $tcolor);
						
						//echo "u".$myhelp[$i]['author_id'].":".$tcolor.", ";
	
						$ulist[] = $uinf;
	
						$ulist_by_id[$myhelp[$i]['author_id']] = $uinf; //count($ulist)-1;
					}
						
					/*
					if( empty($plist_by_id[$myprojlist[$i]['item_id']]) )
					{
						$pinf = $this->_pack_pdata(
								$myprojlist[$i]['item_id'],
								PROJ_NEEDHELP,
								$myprojlist[$i]['title'],
								$myprojlist[$i]['amount_type'],
								$myprojlist[$i]['amount'],
								-1,
								$tcolor
						);
	
						$plist[] = $pinf;
						$plistall[] = $pinf;
	
						$plist_by_id[$myprojlist[$i]['item_id']] = $pinf; //count($plist)-1;
						$plistall_by_id[$myprojlist[$i]['item_id']] = $pinf;
					}
					*/
						
					/*						
					$einf = $this->_pack_edge(
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							$ulist_by_id[$oinf['id']]['gid'],
							$plist_by_id[$myprojlist[$i]['item_id']]['gid'],
							"Получил помощь",
							$myprojlist[$i]['req_type'], $myprojlist[$i]['req_amount'], $myprojlist[$i]['add_date'],
							$tcolor);
					$elist[] = $einf;
					*/
						
					$einf = $this->_pack_edge(
							$ulist_by_id[$myhelp[$i]['author_id']]['gid'],
							$plist_by_id[$pid]['gid'],
							$plist_by_id[$pid]['gid'],
							"Оказал помощь",
							$myhelp[$i]['req_type'], $myhelp[$i]['req_amount'], $myhelp[$i]['add_date'],
							$tcolor);
					$elist[] = $einf;
						
					// Єтот массив уже не нужен
					/*
					$inlist[] = Array(
							"type" => PROJ_NEEDHELP,
							"uid" => $myprojlist[$i]['uid'],
							"uname" => ( $myprojlist[$i]['account_type'] == USR_TYPE_PERS ? $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'] : $myprojlist[$i]['orgname'] ),
							//"uname" => $myprojlist[$i]['name'].' '.$myprojlist[$i]['fname'],
							"utype" => $myprojlist[$i]['account_type'],
							"pt" => $myprojlist[$i]['amount_type'],
							"pamount" => $myprojlist[$i]['amount'],
							"ramount" => $myprojlist[$i]['req_amount']
					);
					*/
				}
	
				//$res = Array("in" => $inlist, "out" => $outlist);
			}
		}
	
		$res = Array("proj_req_tot" => $totpreqnum, "nodes" => array_merge($ulist, $plist), "edges" => $elist);
	
		return $res;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function treeapi_treeMain($musrId = 0, $catLib)
	{
		$prjTop = 5;
		$ndsTop = 5;
		$usrIdTop = 100000;
		
		////////////////////////////////////////
		// New tree
		$answ = Array();
		$answ["status"] = "ok";
		
		$ansData = Array();
		$ansData["settings"] = array();
		
		//if (isset($star["graph_layout"]))
		//{
		//	$ansData["settings"]["graph_layout"] = $star["graph_layout"];
		//}
		//else
		//$ansData["settings"]["graph_layout"] = "binary_tree";
		$ansData["settings"]["graph_layout"] = "physics";
		
		//if (isset($star["show_avas"]))
		//{
		//	$ansData["settings"]["show_avas"] = $star["show_avas"];
		//}
		//else
		$ansData["settings"]["show_avas"] = false;
		$ansData["settings"]["show_labels"] = false;
		
		$ansData["settings"]["debug_config"] = array(
				"cnt_prj" => $prjTop,
				"cnt_usr" => $ndsTop,
				"max_uid" => $usrIdTop);
		
		
		$ansData ["request"] = "tree_main";
		
		// main start node
		$ansData ["source_node"] = array();
		
		
		// data for user interaction and any other stuff
		$ansData ["source_node"]["info"] = array();
		
		//$lprj = mt_rand ( 1 , $prjTop );
		
		$ansData["nodes"] = Array();
		$ansData["edges"] = Array();
		
		//$resp = file_get_contents("http://wayofhelp.com/ajx/graphsrv/?uid=".$musrId);
		//echo $resp;
		
		//$resobj = json_decode($resp, true);
		
		$resobj = $this->make_GraphData2($musrId, 0, $catLib, $ndsTop, $prjTop);
		
		//var_dump($resobj['data']['nodes']);
		
		// data for vis.js
		$ansData["source_node"]["gid"] = "u".$musrId;
		$ansData["source_node"]["info"]["child_skip_usr"] = ( $resobj['sendhelp_req_tot'] > $ndsTop ? $ndsTop : -1 );
		$ansData["source_node"]["info"]["child_skip_prj"] = ( $resobj['proj_req_tot'] > $prjTop ? $prjTop : -1 );
		// data for graph showing
		$ansData["source_node"]["info"]["more_prj"] = true;
		$ansData["source_node"]["info"]["more_usr"] = true;
		
		$ansData["source_node"]["info"]["uname"] = $resobj['nodes'][0]['info']['uname'];		//$resobj['data']['nodes'][0]['info']['uname'];
		$ansData["source_node"]["info"]["url_ava"] = $resobj['nodes'][0]['info']['url_ava'];	//$resobj['data']['nodes'][0]['info']['url_ava'];
		
		//$ansData ["source_node"]["info"]["uname"]	= "Лариса Попова";
		//$ansData ["source_node"]["info"]["url_ava"] = "http://wayofhelp.com/pics/avatar/375_686.jpg";
		
		
		$ansData["nodes"] = $resobj['nodes'];	//$resobj['data']['nodes'];
		$ansData["edges"] = $resobj['edges'];	//$resobj['data']['edges'];
		
		$answ["data"] = $ansData;
		
		//$json = json_encode($answ);
		//echo $json;
		
		return $answ;
	}
	
	public function treeapi_projLoadMore($musrId = 0, $ismainusr=false, $catLib, $skip, $count, $level)
	{
		$prjTop = 5;
		$ndsTop = 5;
		$usrIdTop = 10000;
		
		////////////////////////////////////////
		// New tree
		$answ = Array();
		$answ["status"] = "ok";
		
		$ansData = Array();
		$ansData["request"] = ( $ismainusr ? "unfold_mn_prj" : "unfold_prj" );
		//$ansData["data"] = Array("request" => $ansData["request"]);		
		
		// main start node
		$ansData["source_node"] = array();
		
		$ansData["nodes"] = Array();
		$ansData["edges"] = Array();
		
		if( $ismainusr )
			$resobj = $this->make_GraphDataProj($musrId, 0, $catLib, $skip, $count, $level);
		else
			$resobj = $this->make_GraphDataProjItems($musrId, $catLib, $skip, $count, $level);
		
		//var_dump($resobj['data']['nodes']);
		
		// data for vis.js
		$ansData["source_node"]["gid"] = ( $ismainusr ? "u" : "p" ).$musrId;
		// data for graph showing
		$ansData["source_node"]["info"] = Array();
		
		if( $ismainusr )
		{
			//$ansData["source_node"]["child_skip_usr"] = ($skip + $count);
			//$ansData["source_node"]["info"]["child_skip_prj"] = ($skip + $count);
			$ansData["source_node"]["info"]["child_skip_prj"] = ($resobj['proj_req_tot'] > ($skip + $count) ? ($skip + $count) : -1);
			$ansData["source_node"]["child_skip_prj"] = $ansData["source_node"]["info"]["child_skip_prj"]; 
			//$ansData["source_node"]["more_show"] = false;
			//$ansData["source_node"]["more_load"] = false;
		}
		else
		{
			//$ansData["source_node"]["child_skip"] = ($skip + $count);
			$ansData["source_node"]["info"]["child_skip"] = ($resobj['proj_req_tot'] > ($skip + $count) ? ($skip + $count) : -1);
			$ansData["source_node"]["child_skip"] = $ansData["source_node"]["info"]["child_skip"];
			//$ansData["source_node"]["more_show"] = false;
			//$ansData["source_node"]["more_load"] = false;
		}				
		
		$ansData["nodes"] = $resobj['nodes'];	//$resobj['data']['nodes'];
		$ansData["edges"] = $resobj['edges'];	//$resobj['data']['edges'];
		
		$answ["data"] = $ansData;
		
		//$json = json_encode($answ);
		//echo $json;
		
		return $answ;
	}
	
	public function treeapi_userLoadMore($musrId = 0, $ismainusr=false, $catLib, $skip, $count, $level)
	{
		$prjTop = 5;
		$ndsTop = 5;
		$usrIdTop = 10000;
		
		////////////////////////////////////////
		// New tree
		$answ = Array();
		$answ["status"] = "ok";
		
		$ansData = Array();
		$ansData["request"] = ( $ismainusr ? "unfold_mn_usr" : "unfold_usr" );
		//$ansData["data"] = Array("request" => $ansData["request"]);		
		
		// main start node
		$ansData["source_node"] = array();
		
		$ansData["nodes"] = Array();
		$ansData["edges"] = Array();
		
		$resobj = $this->make_GraphDataRight($musrId, 0, $catLib, $skip, $count, $level);
		
		//var_dump($resobj['data']['nodes']);
		
		// data for vis.js
		$ansData["source_node"]["gid"] = "u".$musrId;
		// data for graph showing
		$ansData["source_node"]["info"] = Array();
		
		if( $ismainusr )
		{
			//$ansData["source_node"]["child_skip_usr"] = ($skip + $count);
			$ansData["source_node"]["info"]["child_skip_usr"] = ( $resobj['tot_req_num'] > ($skip + $count) ? ($skip + $count) : -1 );
			$ansData["source_node"]["child_skip_usr"] = $ansData["source_node"]["info"]["child_skip_usr"];
			//$ansData["source_node"]["more_show"] = false;
			//$ansData["source_node"]["more_load"] = false;
		}
		else
		{
			//$ansData["source_node"]["child_skip"] = ($skip + $count);
			$ansData["source_node"]["info"]["child_skip"] = ( $resobj['tot_req_num'] > ($skip + $count) ? ($skip + $count) : -1 );
			$ansData["source_node"]["child_skip"] = $ansData["source_node"]["info"]["child_skip"];
			//$ansData["source_node"]["more_show"] = false;
			//$ansData["source_node"]["more_load"] = false;
		}				
		
		$ansData["nodes"] = $resobj['nodes'];	//$resobj['data']['nodes'];
		$ansData["edges"] = $resobj['edges'];	//$resobj['data']['edges'];
		
		$answ["data"] = $ansData;
		
		//$json = json_encode($answ);
		//echo $json;
		
		return $answ;
	}
	
	public function treeapi_treeUser($musrId = 0, $catLib)
	{
		$prjTop = 5;
		$ndsTop = 5;
		$usrIdTop = 10000;
	
		////////////////////////////////////////
		// New tree
		$answ = Array();
		$answ["status"] = "ok";
	
		$ansData = Array();
		$ansData ["request"] = "tree_user_show";
	
		// main start node
		$ansData ["source_node"] = array();
		
		$ansData["nodes"] = Array();
		$ansData["edges"] = Array();
	
		//$resp = file_get_contents("http://wayofhelp.com/ajx/graphsrv/?uid=".$musrId);
		//echo $resp;
	
		//$resobj = json_decode($resp, true);
	
		//$resobj = $this->make_GraphData2($musrId, 0, $catLib);
		$resobj = $this->make_GraphDataRight($musrId, 0, $catLib);
	
		//var_dump($resobj['data']['nodes']);
	
		// data for vis.js
		$ansData["source_node"]["gid"] = "u".$musrId;
		// data for graph showing
		$ansData["source_node"]["more_show"] = false;
		$ansData["source_node"]["more_load"] = false;
		
		$ansData["nodes"] = $resobj['nodes'];	//$resobj['data']['nodes'];
		$ansData["edges"] = $resobj['edges'];	//$resobj['data']['edges'];
	
		$answ["data"] = $ansData;
	
		//$json = json_encode($answ);
		//echo $json;
	
		return $answ;
	}
	
	public function treeapi_projInfo($projid, $catLib)
	{
		$answ = array("status"=>"ok");		
		
		$ansData ["request"] = "node_project_info";
		
		$ansData["gid"] = "p".$projid;
		$ansData["content"] = "No data loaded";
		
		$pinf = $catLib->Item_Info($projid);
		if( isset($pinf['id']) )
		{
			$ansData["content"] = $pinf['title2'];
		}
		
		$answ["data"] = $ansData;
		
		return $answ;
	}
	
	public function treeapi_userInfo($uid, $catLib, $mainuser = false)
	{
		$answ = array("status"=>"ok");
	
		$ansData["request"] = ( $mainuser ? "node_mainuser_info" : "node_user_info" );

		$ansData["source_node"] = Array();
		$ansData["source_node"]["gid"] = ( $mainuser ? "u" : "u").$uid;
		$ansData["content"] = "No data loaded for user";
	
		$oinf = $catLib->Buyer_Info($uid);
		if( isset($oinf['id']) )
		{
			$ansData["content"] = ( $oinf['account_type'] == USR_TYPE_PERS ? $oinf['name'].' '.$oinf['fname'] : $oinf['orgname'] );
		}
	
		$answ["data"] = $ansData;
	
		return $answ;
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public function load_Res($resname)
	{
		$query = "SELECT r1.*, r2.content
			FROM ".TABLE_RESOURCE." r1 
			INNER JOIN ".TABLE_RESOURCE_LANGS." r2 ON r1.id=r2.item_id AND r2.lang_id='".$this->LangId."'
			WHERE r1.name='".addslashes($resname)."'";
		$res = $this->db->query($query);
		if( count($res) > 0 )
			return $res[0]['content'];
		
		return "";
	}
	
	public function findBonusQuizNum($uid, $bid, $sesid='', $status="all") {
		$sql_cond = '';
		//if($sesid != '')
		//	$sql_cond .= " AND ses_id='".addslashes($sesid)."' ";
	
		//if( $status == "finished" )
		//	$sql_cond .= " AND ( (status=".BONUS_QUIZ_STATUS_WON.") OR (status=".BONUS_QUIZ_STATUS_USED.") ) ";
		//else if( $status == "run" )
		//	$sql_cond .= " AND status=".BONUS_QUIZ_STATUS_RUN." ";
	
		$query = "SELECT count(*) totnum FROM ".TABLE_CAT_BONUS_QUIZ." WHERE user_id='$uid' AND item_id='$bid' $sql_cond AND add_date>DATE_SUB(NOW(), INTERVAL 1 DAY)";
		//echo $query."<br>";
		$res = $this->db->query($query);
		if( count($res) > 0 ) {
			return $res[0]['totnum'];
		}
						
		return 0;
	}
	
	public function findBonusQuiz($uid, $bid, $sesid='', $status="all") {
		$sql_cond = '';
		if($sesid != '')
			$sql_cond .= " AND ses_id='".addslashes($sesid)."' ";
		
		if( $status == "finished" )
			$sql_cond .= " AND ( (status=".BONUS_QUIZ_STATUS_WON.") OR (status=".BONUS_QUIZ_STATUS_USED.") ) "; 
		else if( $status == "run" )
			$sql_cond .= " AND status=".BONUS_QUIZ_STATUS_RUN." ";
		
		$query = "SELECT * FROM ".TABLE_CAT_BONUS_QUIZ." WHERE user_id='$uid' AND item_id='$bid' $sql_cond AND add_date>DATE_SUB(NOW(), INTERVAL 1 DAY)";		
		$res = $this->db->query($query);
		if( count($res) > 0 )
			return $res[0]['id'];
			
		return false;
	}
	
	public function readBonusQuiz($qid)
	{
		$quiz = Array("id" => 0, "status" => 0,  "pointsnum" => 0, "points" => Array());
		
		$query = "SELECT * FROM ".TABLE_CAT_BONUS_QUIZ." WHERE id='$qid'";
		$res = $this->db->query($query);
		if( count($res) > 0 )
		{
			$quiz['id'] = $res[0]['id'];
			$quiz['status'] = $res[0]['status'];
			$quiz['pointsnum'] = $res[0]['point_total'];
			
			$query1 = "SELECT * FROM ".TABLE_CAT_BONUS_QUIZ_POINTS." WHERE quiz_id='$qid' AND opened=1";
			$res1 = $this->db->query($query1);
			for( $i=0; $i<count($res1); $i++ )
			{
				$quiz['points'][] = $res1[$i]['point_ind'];
			}
		}
		
		return $quiz;
	}
	
	public function buildNewQuiz($catLib, $uid, $bid, $sesid)
	{
		$quiz = Array("id" => 0, "status" => 0,  "pointsnum" => 0, "points" => Array());
		
		$binf = $catLib->Bonus_Info($bid);
		if( $binf != null )
		{
			$points_num = $binf['difficulty'];
			
			$query = "INSERT INTO ".TABLE_CAT_BONUS_QUIZ." (item_id, user_id, ses_id, point_total, add_date) 
				VALUES ('$bid', '$uid', '".addslashes($sesid)."', '$points_num', NOW())";
			if( $this->db->exec($query) )
			{
				$newquizid = $this->db->insert_id();				
				
				// Add points
				$randpoint1 = rand(1,$points_num);
				//$randpoint2 = rand(1,$points_num);
				//$randpoint3 = rand(1,$points_num);
				//$randpoints = Array($randpoint1 => true, $randpoint2 => true, $randpoint3 => true );
				
				for( $i=1; $i<=$points_num; $i++ )
				{
					$query = "INSERT INTO ".TABLE_CAT_BONUS_QUIZ_POINTS." (item_id, quiz_id, point_ind, point_total, value, add_date)
						VALUES ('$bid', '$newquizid', '".$i."', '$points_num', '".( $randpoint1 == $i ? 1 : 0 )."', NOW())";
					if( $this->db->exec($query) )
					{
						// ok
					}
				}
				
				$quiz['id'] = $newquizid;
				$quiz['pointsnum'] = $points_num;
			}		
		}
		
		return $quiz;
	}
	
	public function checkBonusPoint($uid, $bid, $quizid, $pointind, $sesid) {
		$MAX_CHECKS = 3;
		$ajxres = Array("found" => 0, "nomore" => 0, "avail" => 0, "txt" => "");
		
		$checks_done = 0;
		
		$query = "SELECT count(*) as totnum FROM ".TABLE_CAT_BONUS_QUIZ_POINTS." WHERE item_id='$bid' AND quiz_id='$quizid' AND opened=1";
		$res = $this->db->query($query);
		if( count($res) > 0 ) {
			$checks_done = $res[0]['totnum'];					
		}
		
		if( $checks_done < $MAX_CHECKS ) {
			$ajxres['avail'] = 1;
			
			$query = "SELECT * FROM ".TABLE_CAT_BONUS_QUIZ_POINTS." WHERE item_id='$bid' AND quiz_id='$quizid' AND point_ind='$pointind' AND opened=0";
			$res = $this->db->query($query);
			if( count($res) > 0 ) {
				if( $res[0]['value'] == 1 ) {
					// The prise is found
					$ajxres['found'] = 1;					
					$ajxres['txt'] = UhCmsApp::getLocalizerInstance()->get("popbonus", "win");
					
					
					$query = "UPDATE ".TABLE_CAT_BONUS." SET amount_used=amount_used+1 WHERE id='".$bid."'";
					$this->db->exec($query);
					
					$query = "UPDATE ".TABLE_CAT_BONUS_QUIZ." SET status=".BONUS_QUIZ_STATUS_WON." WHERE id='".$quizid."'";
					$this->db->exec($query);
					
					$query = "INSERT INTO ".TABLE_CAT_BONUS_WINS." (item_id, user_id, quiz_id, point_ind, add_date) 
						VALUES ('".$bid."', '".$uid."', '".$quizid."', '".$pointind."', NOW())";
					$this->db->exec($query);
				}
				else {
					if( ($checks_done+1) >= $MAX_CHECKS ) {
						$ajxres['nomore'] = 1;
						$ajxres['txt'] = UhCmsApp::getLocalizerInstance()->get("popbonus", "loose");
						
						// Close quiz
						$query = "UPDATE ".TABLE_CAT_BONUS_QUIZ." SET status=".BONUS_QUIZ_STATUS_USED." WHERE id='".$quizid."'";
						$this->db->exec($query);											
					}
				}
				
				$query = "UPDATE ".TABLE_CAT_BONUS_QUIZ_POINTS." SET opened=1 WHERE id='".$res[0]['id']."'";
				if( $this->db->exec($query) ) {
					// ok
				}
			}
		}
		else {
			$ajxres['nomore'] = 1;
			$ajxres['txt'] = UhCmsApp::getLocalizerInstance()->get("popbonus", "loose");
		}
		
		return $ajxres;
	}

	public function smplAddPerson($name, $fname, $email, $pass, $catLib, $redir = '') {
		$uhash = UhCmsUtils::makeUuid();
		$actuid = UhCmsUtils::makeUuid();
		$deactuid = UhCmsUtils::makeUuid();

		$query = "INSERT INTO ".TABLE_SHOP_BUYERS." (account_type, isactive_web, login, passwd, name, fname, email, obl_id, city_id,  guid_uhash, guid_act, guid_deact, add_date, profession, comments) 
			VALUES (".USR_TYPE_PERS.", '0', '".addslashes($email)."', PASSWORD('".addslashes($pass)."'), '".addslashes($name)."', '".addslashes($fname)."', 
			'".addslashes($email)."', '', '', 
			'".addslashes($uhash)."', '".addslashes($actuid)."', '".addslashes($deactuid)."', NOW(), '', '')";
		//echo $query."<br>";
		if( !$this->db->exec($query) ) {
			return false;
		}

		$newusrid = $this->db->insert_id();
		/* ADD TO TABLE buyer_box */
		$query = "INSERT INTO ".TABLE_SHOP_BUYERS_BOX."(buyer_id) VALUES('".$newusrid."')";
		$this->db->exec($query);
		/*************************/

		// Send confirm email
		if( preg_match(EMAIL_REGEXP, trim($email)) == 0 ) {
			// Это не email, т.е. телефон
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET isactive_web=1 WHERE id='".$newusrid."'";
			if( !$this->db->exec($query) ) {
				//return false;
			}
		}
		else {
			$redir .= '?utm_source=direct&utm_medium=referral&utm_campaign=regfromproject';
			//$this->sendRegConfirmMail($email, $pass, $actuid);
			$tit =  UhCmsApp::getLocalizerInstance()->get("reg", "messtit");
			$tit = str_replace('_hr_', 'WayOfHelp', $tit);
			$descr =  UhCmsApp::getLocalizerInstance()->get("reg", "messdescr");
			$descr = str_replace('_hr_', 'WayOfHelp', $descr);
			$descr = str_replace('_log_', $email, $descr);
			$descr = str_replace('_pass_', $pass, $descr);
			$descr = str_replace('_hract_', UhCmsUtils::Page_BuildUrl("registration", "activate", "guid=".$actuid.'&redirto='.$redir), $descr);

			$catLib->sendUserMailMsg_ct($newusrid, $tit, $descr);
		}

		return true;
	}

	public function checkLogin($email) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE login='".addslashes($email)."'";
		$res = $this->db->query($query);

		if( count($res) > 0 )
			return true;

		return false;
	}

	public function checkHelp($uid, $data='2016-08-12') {
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE req_status = ".REQ_STATUS_CONFIRM." AND `show` = 1 AND sender_id = ".addslashes($uid)." 
		AND DATE(add_date) >= '".addslashes($data)."' AND id NOT IN(SELECT req_id FROM ".TABLE_BONUS_HELP.") ";
		return $this->db->query($query);
	}

	public function getBonusHelp($uid, $bid) {
		$query = "SELECT * FROM ".TABLE_BONUS_HELP." WHERE user_id = ".addslashes($uid)." AND bonus_id = ".addslashes($bid)." AND use_quiz < 3";
		return $this->db->query($query);
	}

	public function addBonusHelp($uid, $bid, $req_id, $type = 'upd') {
		if($type == 'upd')
			$query = "UPDATE " . TABLE_BONUS_HELP . " SET use_quiz = use_quiz + 1 WHERE user_id = " . addslashes($uid) . " AND bonus_id = " . addslashes($bid)." AND req_id = ".addslashes($req_id);
		else if($type == 'add' && $req_id) {
			$query = "INSERT INTO ".TABLE_BONUS_HELP." (`user_id`, `req_id`, `bonus_id`, `add_date`) VALUES (".addslashes($uid).", ".addslashes($req_id).", ".addslashes($bid).", NOW())";
		} else {
			return false;
		}

		return $this->db->exec($query);
	}

	public function addUserMoney($uid, $money, $soc_type) {
		$query = "";
		$res = "";

		switch ($soc_type) {
			case 'vk':
						$query = "SELECT vk_group FROM ".TABLE_SHOP_BUYERS_BOX." WHERE buyer_id = ".addslashes($uid);

						$res = $this->db->query($query);

						if(!empty($res) && $res[0]['vk_group'] == 0) {
							$res = 1;
						} else $res = 0;

						$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET money = money + ".addslashes($money).", vk_group = 1 WHERE buyer_id = ".addslashes($uid)." AND vk_group = 0";
						break;

			case 'fb':
						$query = "SELECT fb_group FROM ".TABLE_SHOP_BUYERS_BOX." WHERE buyer_id = ".addslashes($uid);

						$res = $this->db->query($query);

						if(!empty($res) && $res[0]['fb_group'] == 0) {
							$res = 1;
						} else $res = 0;

						$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET money = money + ".addslashes($money).", fb_group = 1 WHERE buyer_id = ".addslashes($uid)." AND fb_group = 0";
						break;
			case 'ok':
						$query = "SELECT ok_group FROM ".TABLE_SHOP_BUYERS_BOX." WHERE buyer_id = ".addslashes($uid);

						$res = $this->db->query($query);

						if(!empty($res) && $res[0]['ok_group'] == 0) {
							$res = 1;
						} else $res = 0;

						$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET money = money + ".addslashes($money).", ok_group = 1 WHERE buyer_id = ".addslashes($uid)." AND ok_group = 0";
						break;
			default: return -1;
		}

		$this->db->exec($query);

		return $res;
	}

	public function addProjClick($pid) {
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET cat_item_click = cat_item_click + 1 WHERE id = ".addslashes($pid);

		return $this->db->exec($query);
	}
	public function getShareCount($itemid) {
		$query = "SELECT count(distinct `user_referer_id`) as sco FROM ".TABLE_CAT_ITEMS_SC_RATE." WHERE  `item_id` = '".addslashes($itemid)."' ";

		return $this->db->query($query);
	}

	public function setInpSum($pid, $sum) {
		$query = "INSERT INTO ".TABLE_CAT_ITEM_DIAGRAM." (item_id, inp_req_sum, add_date) VALUES(".addslashes($pid).", ".$sum.", NOW()) ";

		return $this->db->exec($query);
	}

	public function updTabCounter($tab, $uid) {
		$query = "";

		switch ($tab) {
			case 'getlist' :
				$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." r1 
							JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
						  SET r1.tab_check = 1
						  WHERE i1.archive=0 AND i1.active=1 AND i1.profile_id = 1 AND r1.sender_id = ".addslashes($uid)."  AND r1.tab_check = 0";
			break;
			case 'sendlist' :
				$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." r1 
							JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
						  SET r1.tab_check = 1
						  WHERE i1.archive=0 AND i1.active=1 AND i1.profile_id = 1 AND i1.author_id = ".addslashes($uid)." AND req_status = ".REQ_STATUS_NEW." AND tab_check = 0";
			break;
			case 'needlist' :
				$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." r1 
							JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
						  SET r1.tab_check = 1
						  WHERE i1.archive=0 AND i1.active=1 AND i1.profile_id = 0 AND i1.author_id = ".addslashes($uid)." AND tab_check = 0";
			break;
			case 'dolist' :
				$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." r1 
							JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
						  SET r1.tab_check = 1
						  WHERE i1.archive=0 AND i1.active=1 AND r1.`sender_id` = ".addslashes($uid)." AND i1.profile_id = 0 AND r1.`req_status` = ".REQ_STATUS_CONFIRM." AND r1.`tab_check` = 0";
			break;
		}

		return $this->db->exec($query);

	}

	public function loadMsgBord($usrid, $myid, $type = 'all') {

		$query_in = "";
		$query_limit = "";
		$query_order_type = " ASC ";

		switch ($type) {
			case 'all' : $query_in = " (b1.from_id = ".addslashes($usrid)." AND b1.to_id = ".addslashes($myid).") OR (b1.from_id = ".addslashes($myid)." AND b1.to_id = ".addslashes($usrid).")";
						 break;

			case 'last_u' : $query_in = " b1.from_id = ".addslashes($myid)." AND b1.to_id = ".addslashes($usrid);
							$query_limit = " LIMIT 0, 1 ";
							$query_order_type = " DESC ";
							break;
		}

		$query = "SELECT DISTINCT b1.*, s1.account_type, s1.name, s1.fname, s1.orgname, s1.pic_sm
					FROM ".TABLE_CAT_MSG." b1
					JOIN ".TABLE_SHOP_BUYERS." s1 ON b1.from_id = s1.id
					WHERE ".$query_in."
					ORDER BY b1.add_date ".$query_order_type.$query_limit;

		//$query = "SELECT * FROM ".TABLE_CAT_MSG." b1 WHERE 1 LIMIT 0, 20 ";

		return $this->db->query($query);
	}

	public function updMsgBord($usrid, $myid) {

		$query = "SELECT DISTINCT b1.*, s1.account_type, s1.name, s1.fname, s1.orgname, s1.pic_sm
					FROM ".TABLE_CAT_MSG." b1
					JOIN ".TABLE_SHOP_BUYERS." s1 ON b1.from_id = s1.id
					WHERE b1.from_id = ".addslashes($usrid)."
					AND b1.to_id = ".addslashes($myid)."
					AND msg_status = 1
					ORDER BY b1.add_date ASC";

		return $this->db->query($query);
	}

	public function readMsgBord($usrid, $myid) {
		$query = "UPDATE ".TABLE_CAT_MSG." SET msg_status = 0 WHERE msg_status = 1 AND from_id = ".addslashes($usrid)."
					AND to_id = ".addslashes($myid);

		return $this->db->exec($query);
	}

	public function msg_ListByProj($toid, $pi = -1, $pn = 12) {

		$limit_cond = "";

		if( $pi >= 0 )
			$limit_cond = " LIMIT ".$pn.", $pi ";

		$query = "SELECT m1.to_id, sum(m1.msg_status <> 0 AND m1.to_id = ".$toid.") new_msg, 
					max(m1.add_date) as latestdt, 
					(CASE WHEN m1.to_id <> ".$toid." THEN m1.to_id ELSE m1.from_id END) user_id
					FROM ".TABLE_CAT_MSG." m1 
					LEFT JOIN ".TABLE_SHOP_BUYERS." s1 ON m1.from_id = s1.id 
					WHERE m1.to_id = ".$toid." OR m1.from_id = ".$toid."
					GROUP BY user_id
					ORDER BY latestdt DESC ".$limit_cond;

		$res = $this->db->query($query);

		foreach ($res as $k => $mess_list) {
			//echo $mess_list['user_id'].'<br>';
			$query = "SELECT message FROM ".TABLE_CAT_MSG." WHERE from_id IN(".$toid.", ".$mess_list['user_id'].") AND to_id IN (".$toid.", ".$mess_list['user_id'].") ORDER BY add_date DESC LIMIT 0, 1";

			$last = $this->db->query($query);

			if(isset($last[0]['message'])) {
				$res[$k]['last_mess'] = $last[0]['message'];

			} else {
				$res[$k]['last_mess'] = '';
			}
		}

		return $res;
	}

	public function msg_Send($fromid, $toid, $txt, $projid = 0, $replyto = 0, $status = 1) {

		$query = "INSERT INTO ".TABLE_CAT_MSG." (item_id, from_id, to_id, msg_status, reply_msg_id, add_date, modify_date, message) 
			VALUES (".addslashes($projid).", ".addslashes($fromid).", ".addslashes($toid).", ".$status.", ".addslashes($replyto).", NOW(), NOW(), '".addslashes($txt)."')";

		if( !$this->db->exec($query) )
			return false;

		$msg_id = $this->db->insert_id();
		if( $msg_id == 0 )
			return false;

		$query = "INSERT INTO ".TABLE_CAT_MSG_P2P." (msg_id, user_id, add_date, modify_date) 
			VALUES (".addslashes($msg_id).", ".addslashes($fromid).", NOW(), NOW() )";

		if( !$this->db->exec($query) )
			return false;

		$query = "INSERT INTO ".TABLE_CAT_MSG_P2P." (msg_id, user_id, add_date, modify_date)
		VALUES (".addslashes($msg_id).", ".addslashes($toid).", NOW(), NOW() )";

		if( !$this->db->exec($query) )
			return false;


		return true;
	}


	public function getLastMsg($myid, $usrid){
		$query = "SELECT DISTINCT b1.add_date, b1.message, b1.to_id, b1.from_id
					FROM jhlp_msg_board b1
					JOIN jhlp_msg_board_p2p b2 ON b1.to_id = b2.user_id
					WHERE b1.from_id IN(".addslashes($myid).", ".addslashes($usrid).")
					AND b1.to_id IN(".addslashes($usrid).",".addslashes($myid).")
					ORDER BY b1.add_date DESC 
					LIMIT 0 , 1";

		return $this->db->query($query);
	}
	public function globLoad_helpProjData($uid) {
		$query = "SELECT r1.item_id, b1.id as user_id, b1.name, b1.fname, b1.pic, b1.pic_sm, b1.city_id, b1.obl_id FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
					JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
					JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = i1.author_id
					WHERE r1.`sender_id` = ".addslashes($uid)."
					GROUP BY i1.author_id";

		return $this->db->query($query);
	}

	public function globLoad_getnames(array $data) {
		$data_names = [];

		foreach ($data as $val) {
			$data_names[]['names'] = $val['name'].' '.$val['fname'];
		}

		return $data_names;
	}

	public function globLoad_getavas(array $data) {
		$data_avas = [];

		foreach ($data as $val) {
			$data_avas[] = $val['pic_sm'];
		}

		return $data_avas;
	}

	public function globLoad_getloation(array $data, $lang) {
		$data_loc = [];
		$user_loc = [];

		foreach ($data as $val) {
			$query = "SELECT r1.country_id, cl1.*, c1.code FROM ".TABLE_REGION." r1
									JOIN ".TABLE_REGION_LANG." rl1 ON rl1.id = r1.id
									JOIN ".TABLE_COUNTRY_LANG." cl1 ON cl1.country_id = r1.country_id
									JOIN ".TABLE_COUNTRY." c1 ON c1.id = cl1.id
									WHERE rl1.`region_id` = ".addslashes($val['obl_id']);

			$res = $this->db->query($query);

			if(!empty($res)) {

				/*foreach ($res as $loc_val) {

					if(!$loc_val['lat'] || !$loc_val['lng']) {
						$location = $this->getGoogleLoc($loc_val['name']);

						if($location['lat'] && $location['lng']) {

							$loc_val['lat'] = $location['lat'];
							$loc_val['lng'] = $location['lng'];

							$query = "UPDATE " . TABLE_COUNTRY_LANG . " SET lat = " . addslashes($location['lat']) .", lng = ". addslashes($location['lng']) ." WHERE country_id = ".$loc_val['country_id'];
							$this->db->exec($query);
						}
					}

					$user_loc[$val['user_id']]['country'] = $loc_val;
				}*/

				$user_loc['name']['country'] = $res;
			}


			$query = "SELECT *, name as ru FROM ".TABLE_CITY_LANG." WHERE city_id = ".addslashes($val['city_id'])." AND lang_id = ".addslashes($lang);

			$res = $this->db->query($query);

			if(!empty($res)) {

				foreach ($res as $loc_val) {

					if(!$loc_val['lat'] || !$loc_val['lng']) {
						$location = $this->getGoogleLoc($loc_val['name']);

						if($location['lat'] && $location['lng']) {

							$loc_val['lat'] = $location['lat'];
							$loc_val['lng'] = $location['lng'];

							$query = "UPDATE " . TABLE_CITY_LANG . " SET lat = " . addslashes($location['lat']) .", lng = ". addslashes($location['lng']) ." WHERE city_id = ".$loc_val['city_id'];
							$this->db->exec($query);
						}
					}

					$user_loc['cities'][$val['user_id']] = $loc_val['name'];
					$user_loc['cities'][$val['user_id']] = $loc_val['lng'];
					$user_loc['cities'][$val['user_id']] = $loc_val['lat'];
				}

			}

		}

		return ['data' => $user_loc];
	}

	public function globLoad_getMainUserLoc($city_id, $cat) {
		$info = $cat->Loc_InfoByCity($city_id);

		if(!$info['lat'] || !$info['lng']) {
			$location = $this->getGoogleLoc($info['name'], $info['countryname']);

			if($location['lat'] && $location['lng']) {
				
				$info['lat'] = $location['lat']; 
				$info['lng'] = $location['lng'];
				
				$query = "UPDATE " . TABLE_CITY_LANG . " SET lat = " . addslashes($location['lat']) .", lng = ". addslashes($location['lng']) ." WHERE city_id = ".$info['city_id'];

				$this->db->exec($query);
			}
		}

		return $info;
	}

	private function getGoogleLoc($city, $country = '') {
		$latitude = 0;
		$longitude = 0;

		if($city) {
			$m_url = "http://maps.googleapis.com/maps/api/geocode/json?address=";
			$url = $m_url.urlencode($city);

			$json_data = file_get_contents($url);

			$result = json_decode($json_data, TRUE);

			if($result['status'] == 'OK') {

				$latitude = $result['results'][0]['geometry']['location']['lat'];
				$longitude = $result['results'][0]['geometry']['location']['lng'];

			} else if($result['status'] == 'ZERO_RESULTS' && $country) {

				return $this->getGoogleLoc($country);
			}
		}

		return ['lat' => $latitude, 'lng' => $longitude];
	}

	public function globLoad_userdata($uid, $cat, $type = '') {
		$uinfo = [];
		$uinfo['user_info'] = $cat->Buyer_Info($uid);

		if($uinfo['user_info']['id'] > 0) {
			$uinfo['loc_info'] = $cat->Loc_InfoByCity($uinfo['user_info']['city_id']);
		}

		if($type == 'help') {
			$uinfo['user_help'] = $this->loadCountryHelpByUser($uid)[0];
			$uinfo['user_help']['all_country'] = 197;
		}

		return $uinfo;
	}

	public function globLoad_useritem($sender_id, $uid) {
		$query = "SELECT r1.item_id, b1.author_id, il1.title2, il1.descr0, p1.filename_big, p1.filename_thumb FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
		 		  JOIN ".TABLE_CAT_ITEMS." b1 ON b1.id = r1.item_id 
		 		  JOIN ".TABLE_CAT_ITEMS_LANGS." il1 ON b1.id = il1.item_id 
		 		  LEFT JOIN ".TABLE_CAT_ITEMS_PICS." p1 ON p1.item_id = r1.item_id
		 		  WHERE r1.sender_id = ".addslashes($sender_id)." AND b1.author_id = ".addslashes($uid)."
		 		  ORDER BY r1.add_date DESC ";

		return $this->db->query($query);
	}

	public function globLoad_countryinfo($code, $uid) {
		$query = "SELECT r1.id, cl1.name FROM ".TABLE_COUNTRY." c1
					JOIN ".TABLE_COUNTRY_LANG." cl1 ON cl1.id = c1.id
					JOIN ".TABLE_REGION." r1 ON r1.country_id = c1.id
					WHERE c1.code = "."'".addslashes($code)."'";

		$country_help = 0;
		$country_u_help = 0;
		$country_name = '';

		$regs = $this->db->query($query);

		if(!empty($regs)) {

			$country_name = $regs[0]['name'];

			$obls = '';

			foreach ($regs as $region) {
				$obls .= $region['id'].',';
			}

			$obls = "(".substr($obls, 0, -1).")";

			$query = "SELECT count(*) as co FROM ".TABLE_CAT_ITEMS." i1
			 		  JOIN ".TABLE_CAT_ITEMS_HELPREQ." r1 ON r1.item_id = i1.id
					  WHERE i1.obl_id IN ".$obls;

			$country_help = $this->db->query($query)[0]['co'];
			$country_u_help = $this->db->query($query." AND r1.sender_id = ".addslashes($uid))[0]['co'];
		}

		return ['name' => $country_name, 'co_help' => $country_help, 'co_u_help' => $country_u_help];

	}

	private function loadCountryHelpByUser($uid) {
		$query = "SELECT count(DISTINCT re1.country_id) as country_count FROM ".TABLE_CAT_ITEMS_HELPREQ." r1 
					JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
					JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = i1.author_id
					JOIN ".TABLE_REGION." re1 ON re1.id = b1.obl_id
					WHERE r1.`sender_id` = ".addslashes($uid);

		return $this->db->query($query);
	}

	public function globLoad_projects() {
		$query = "";
	}
}