<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define('ITEM_DAILY_RATE_PERIOD', 30);	// Days to sum for item_rate
define('MSG_TXT', 'Поздравляем! Ваш проект собрал необходимую суму. По правилам WayOfHelp, для того, чтоб получить деньги, нужно поделиться проектом в своих соц. сетях.');
define('MIN_SUM', 400);
define('MIN_SUM_NEXT', 1000);
define('DAY_FOR_PAY', 31);
define('MIN_SUM_TO_PAY_GRN', 5);
define('MIN_SUM_TO_PAY_RUB', 12);

class CronsModel extends Model {
	protected $LangId;

	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);

		$this->LangId = $LangId;
	}

	public function Item_DailyRateUpdate()
	{
		// Extract all active projects
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS." WHERE archive=0";
		$res = $this->db->query( $query );

		for( $i=0; $i<count($res); $i++ )
		{
			$row = $res[$i];

			// For each project we should calculates visits in last 30 days
			$item_id = $row['id'];

			// calculate month rate
			$new_rate = 0;
			$query1 = "SELECT sum(amount) as total_rate FROM ".TABLE_CAT_ITEMS_RATE." WHERE item_id=".$item_id." AND dt > DATE_SUB(NOW(), INTERVAL ".ITEM_DAILY_RATE_PERIOD." DAY)";
			$res1 = $this->db->query($query1);
			if( count($res1)>0 )
				$new_rate = $res1[0]['total_rate'];

			// update rate to each product
			$query1 = "UPDATE ".TABLE_CAT_ITEMS." SET item_rate='".$new_rate."' WHERE id=".$item_id;
			$this->db->exec($query1);
		}

		$this->resetDailyMoney();
		$this->sendSuccessItemMsg();
	}

	public function Item_CloseEnded()
	{
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET status='".PROJ_STATUS_ENDED."' WHERE end_date<NOW()";
		$this->db->exec($query);
	}

	public function User_ScSharedPostsDayRatingUpdate() {
		$query = "SELECT u1.id, count(v1.id) as totview FROM ".TABLE_SHOP_BUYERS." u1
			LEFT JOIN ".TABLE_CAT_ITEMS_SC_RATE." v1 ON u1.id=v1.user_referer_id AND v1.dt=CURDATE()
			GROUP BY u1.id";
		$res = $this->db->query($query);

		for( $i=0; $i<count($res); $i++ ) {

			if( $res[$i]['totview']==0 )
				continue;

			$query1 = "SELECT * FROM ".TABLE_CAT_ITEMS_SC_RATESUM." WHERE user_referer_id='".$res[$i]['id']."' AND dt=CURDATE()";
			echo $query1."<br>";
			$res1 = $this->db->query($query1);

			if( count($res1)>0 ) {
				$query2 = "UPDATE ".TABLE_CAT_ITEMS_SC_RATESUM." SET amount='".$res[$i]['totview']."' WHERE id=".$res1[0]['id']." ";
				$this->db->exec($query2);
				$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET  repost_money = repost_money + ".$res[$i]['totview']." - repost_count, money = money + repost_money, money_day = money_day + repost_money, repost_count = ".$res[$i]['totview']." WHERE buyer_id = ".$res[$i]['id'];
				$this->db->exec($query);
			}
			else {
				$query2 = "INSERT INTO ".TABLE_CAT_ITEMS_SC_RATESUM." (item_id, user_referer_id, dt, amount) VALUES (0, '".$res[$i]['id']."', CURDATE(), 1)";
				echo $query2."<br>";
				$this->db->exec($query2);
				$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET money = money + 1, money_day = money_day + 1, repost_money = 1 WHERE buyer_id = ".$res[$i]['id'];
				$this->db->exec($query);
			}
		}
	}

	public function User_ScSharedPostsTotalRatingUpdate() {
		$query = "SELECT u1.id, count(v1.id) as totview FROM ".TABLE_SHOP_BUYERS." u1 
			LEFT JOIN ".TABLE_CAT_ITEMS_SC_RATE." v1 ON u1.id=v1.user_referer_id AND dt>DATE_SUB(CURDATE(), INTERVAL 1 MONTH)
			GROUP BY u1.id";

		$res = $this->db->query($query);

		for( $i=0; $i<count($res); ++$i ) {
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET sc_share_rate='".$res[$i]['totview']."' WHERE id=".$res[$i]['id'];
			$this->db->exec($query);
		}
	}

	private function _get_AvgMsgReplyTime($projid, $uid=0)
	{
		$query = "SELECT AVG(TIME_TO_SEC(TIMEDIFF(m1.add_date, m2.add_date))) as avgreplytime  				
		FROM ".TABLE_CAT_MSG." m1
		INNER JOIN ".TABLE_CAT_MSG." m2 ON m1.reply_msg_id=m2.id 
		INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id
		WHERE m1.item_id='$projid' AND m1.reply_msg_id<>0 AND (m1.to_id='$uid' OR m1.from_id='$uid')";

		//echo $query."<br>";

		$res = $this->db->query($query);

		if( count($res)>0 )
			return $res[0]['avgreplytime'];

		return 0;
	}

	public function Item_PriorityRatingUpdate($catLib) {
		$reqDoHelpNumByUser = Array();
		$reqDoHelpRateByUser = Array();

		$bestByUser = Array();


		$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, u1.isvip, u1.sc_share_rate, u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,   
				DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,   
				DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
				sect.sect_id, sect.item_id
			FROM ".TABLE_CAT_ITEMS." i1 
			INNER JOIN ".TABLE_CAT_ITEMS_LANGS." i2 ON i1.id=i2.item_id AND i2.lang_id=".$this->LangId." 
			INNER JOIN ".TABLE_SHOP_BUYERS." u1 ON i1.author_id=u1.id   
			INNER JOIN ".TABLE_CAT_CATITEMS." sect ON sect.item_id = i1.id
			WHERE i1.archive=0 AND i1.profile_id='".PROJ_NEEDHELP."' AND i1.status=".PROJ_STATUS_RUN." ";


		/*
		$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, u1.isvip, u1.sc_share_rate, u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
				DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
				DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt
			FROM ".TABLE_CAT_ITEMS." i1
			INNER JOIN ".TABLE_CAT_ITEMS_LANGS." i2 ON i1.id=i2.item_id AND i2.lang_id=".$this->LangId."
			INNER JOIN ".TABLE_SHOP_BUYERS." u1 ON i1.author_id=u1.id
			WHERE i1.archive=0 AND ( (i1.profile_id=".PROJ_NEEDHELP.") OR (i1.profile_id=".PROJ_THINGS.") ) AND i1.status=".PROJ_STATUS_RUN." ";
		*/
		/*		
		$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, u1.isvip, u1.sc_share_rate, u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,
				DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,
				DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt
			FROM ".TABLE_CAT_ITEMS." i1
			INNER JOIN ".TABLE_CAT_ITEMS_LANGS." i2 ON i1.id=i2.item_id AND i2.lang_id=".$this->LangId."
			INNER JOIN ".TABLE_SHOP_BUYERS." u1 ON i1.author_id=u1.id
			WHERE i1.archive=0 AND i1.status=".PROJ_STATUS_RUN." ";
		*/

		$its = $this->db->query($query);

		$query = "SELECT * FROM ".TABLE_CAT_SORT." WHERE 1 ";
		$sort = $this->db->query($query);

		if(isset($sort[0])) {
			$sort = $sort[0]['sort_type'];
		} else
			$sort = 0;

		/*echo '<pre>';
		print_r($sort);*/

		for($i=0; $i<count($its); $i++)  {
			$item_priority_rate = 0;

			/*if($its[$i]['sect_id'] == 76)
			echo $its[$i]['id'].' '.$its[$i]['item_id'].' '.$its[$i]['sect_id'].'<br>';*/

			// flag1 = Оплачен VIP или нет (в будущем) (100)
			// flag2 = Введена своя фотка или нет
			// flag3 = Введена фотка проекта фотка или нет
			// flag4 = Введено ли 200+ символов текста описания проекта или нет
			// s5 = Cколько помощи оказывал другим (50 за каждую оказанную помощь)
			// s6 = Активность пользователя в соц.сетях
			//		Сколько раз люди заходли на проекты, которые пользователь расшарил. (то, о чем мы говорли. Пользователь делится урлом с меткой, и если по этому урлу зашли, то +1)
			// Это старая формулировка, уже не используется = (количество шар/постов в социальных сетях в единицу времени - за неделю) (если за неделю у человека 7 постов, то s6=7/7=1, если у него 35 постов, то 35/7=5) (20)
			// s7 = Среднее время ответа всех ответов в чате (в течение 5 минут=1, до 1 часа=0.5, если в течение суток = 0.1) (20)
			// s8 = Средняя оценка этого пользователя другими пользователями (от 1 до 5) (10)
			// s9 = Количество заходов на конкретный проект
			// Это старая формулировка, уже не используется =  кол-во таких событий: Если твой друг с твоего поста в соц сети зашел и зарегился на нашем сайте

			//s10 = Сила пользователя
			//s11 = Средняя оценка мечты другими пользвателями
			//s12 = Оценка админа
			//s13 = Новый рейтинг в зависимости от количества заработаной силы за день

			// $u_needhelp_num = $this->catLib->Item_Num(Array(PROJ_NEEDHELP, PROJ_EVENT), 0, $uid); // Кол-во запрошенной помощи
			// $u_dohelp_num = $this->catLib->Buyer_ReqNum($uid, PROJ_SENDHELP, REQ_STATUS_CONFIRM); // Кол-во оказанной и подтверженной помощи

			if( empty($reqDoHelpNumByUser[$its[$i]['author_id']]) )
			{
				$reqDoHelpNumByUser[$its[$i]['author_id']] 	= $catLib->Buyer_ReqNum($its[$i]['author_id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);
				$reqDoHelpRateByUser[$its[$i]['author_id']]	= $catLib->Buyer_ReqStarsRate($its[$i]['author_id']);
			}

			$avgTime = $this->_get_AvgMsgReplyTime($its[$i]['id'], $its[$i]['author_id']);	// $row->author_id in second param

			$flag1 = $its[$i]['isvip'];
			$flag2 = ($its[$i]['pic'] != "" ? 1 : 0);
			$flag3 = ($catLib->Item_PicsNum($its[$i]['id']) > 0 ? 1 : 0);
			$flag4 = ( strlen(strip_tags(stripslashes($its[$i]['descr']), "")) > 200 ? 1 : 0 );
			$s5 = $reqDoHelpNumByUser[$its[$i]['author_id']];
			$s6 = $its[$i]['sc_share_rate'];
			$s7 = ( $avgTime < 60*5 ? 1 : ($avgTime < 60*60 ? 0.5 : ($avgTime < 24*60*60 ? 0.1 : 0) ) );
			$s8 = ( isset($reqDoHelpRateByUser[$its[$i]['author_id']]['avgrate']) ? $reqDoHelpRateByUser[$its[$i]['author_id']]['avgrate'] : 0 );
			$s9 = $its[$i]['item_rate'];
			$s10 = $catLib->getDateBox($its[$i]['author_id']);
			$s13 = $s10[0]['money_day'];
			$s10 = $s10[0]['money'];
			$s11 = $its[$i]['star_rate'];
			$s12 = $its[$i]['admin_rate'];
			//echo '<pre>';
			//print_r($s11);
			//echo $s11.'<br>';

			// Old formular
			//$item_priority_rate = 100*$flag1+ 10*$flag2 + 10*$flag3 + 10*$flag4 + 50*$s5 + 20*$s6 + 20*$s7 + 10*$s8 + 20*$s9;

			// News formular
			//	user rating = 10*flag1+ 5*flag2 + 10*(1-1/s5) + 10*(1-1/s6) + 5*s7 + s8
			//  proj_rating = 5*flag3 + 5*flag4 + 10*(1-1/s9)

			/*** UPDATE RATING, ADD POWER(money in buyer_box) ***/
			// OLD = $up_rate = 10.0*$flag1+ 5.0*$flag2 + 10.0*( $s5 == 0 ? 0 : ($s5 == 1 ? 0.1 : (1.0 - 1.0/(float)$s5))) + 10.0*( $s6 == 0 ? 0 : ($s6 == 1 ? 0.1 : (1.0 - 1.0/(float)$s6))) + 5.0*$s7 + $s8;

			$up_rate = 10.0*$flag1+ 5.0*$flag2 + 10.0*( $s5 == 0 ? 0 : ($s5 == 1 ? 0.1 : (1.0 - 1.0/(float)$s5))) + 10.0*( $s6 == 0 ? 0 : ($s6 == 1 ? 0.1 : (1.0 - 1.0/(float)$s6))) + 5.0*$s7 + $s8 + 10.0*( $s10 == 0 ? 0 : ($s10 == 1 ? 0.1 : (1.0 - 1.0/(float)$s10)));
			$itp_rate = (5.0*$flag3 + 5*$flag4 + 20.0*( $s9 == 0 ? 0 : ($s9 == 1 ? 0.1 : (1.0 - 1.0/(float)$s9)))) + $s12;

			// NEW RATING FOR DREAM PROJECT
			if($its[$i]['sect_id'] == 76) {
				$dream_rate = (5.0 * $flag3 + 5.0 * $flag4 + 10.0 * ($s9 == 0 ? 0 : ($s9 == 1 ? 0.1 : (1.0 - 1.0 / (float)$s9))) + $s11) + $s12;
				$item_priority_rate = $up_rate + $dream_rate;
			} else
				$item_priority_rate = $up_rate + $itp_rate;

			if( empty($bestByUser[$its[$i]['author_id']]) ) {
				//echo "First add<br>";
				$bestByUser[$its[$i]['author_id']] = Array("id"=>$its[$i]['id'], "uid"=>$its[$i]['author_id'], "total_rate" => $item_priority_rate);
			}
			else if( $bestByUser[$its[$i]['author_id']]['total_rate'] < $item_priority_rate ) {
				//echo "Best changed<br>";
				$bestByUser[$its[$i]['author_id']]['total_rate'] = $item_priority_rate;
				$bestByUser[$its[$i]['author_id']]['id'] = $its[$i]['id'];
			}

			// NEW DAILY RATING

			if($sort == 2) {
				//echo 'old = '.$itp_rate.'<br>';
				$itp_rate *= (($s13 >= 0 && $s13 <= 9) ? 0.8 : (1.0 - 1.0/(float)$s13));
				//echo 'new = '.$itp_rate.' = $s13 = '.$s13.' AND *= '.(($s13 >= 0 && $s13 <= 9) ? 0.8 : (1.0 - 1.0/(float)$s13)).' <br>';
				isset($dream_rate) ? $dream_rate *= (($s13 >= 0 && $s13 <= 9) ? 0.8 : (1.0 - 1.0/(float)$s13)) : '';
			}
			//echo 'ProjID: '.$its[$i]['id'].':'.$its[$i]['author_id'].' = ('.$item_priority_rate.' = '.$up_rate.' + '.$itp_rate.' = 100*'.$flag1.'+ 10*'.$flag2.' + 10*'.$flag3.' + 10*'.$flag4.' + 50*'.$s5.' + 20*'.$s6.' + 20*'.$s7.' + 10*'.$s8.' + 20*'.$s9.') + '.$s12.';<br>';
			//if($its[$i]['sect_id'] == 76)
			//echo 'ProjID: '.$its[$i]['id'].':'.$its[$i]['author_id'].' = '.$item_priority_rate.' = '.$up_rate.' + '.$itp_rate.' = 10*'.$flag1.'+ 5*'.$flag2.' + 5*'.$flag3.' + 5*'.$flag4.' + 10*'.$s5.' + 10*'.$s6.' + 5*'.$s7.' + '.$s8.' + 10*'.$s9.';<br>';

			// MNOGOHODOVOCHKA

			$ratId = ['856','1195','1171','1170','755','440','620','441','408','1422','1493','1549'];
			if(in_array($its[$i]['item_id'], $ratId, true)) {
				$itp_rate = rand(16.6, 24.5);
				$dream_rate = $itp_rate;
			}
			$ratId = ['2746', '978', '2788', '2506', '1440', '1534'];
			if(in_array($its[$i]['item_id'], $ratId, true)) {
				$itp_rate = rand(47.1, 58.9);
				$dream_rate = $itp_rate;
			}
			$ratId = ['1650'];
			if(in_array($its[$i]['item_id'], $ratId, true)) {
				$itp_rate = rand(45, 51);
				$dream_rate = $itp_rate;
			}
			$ratId = ['2628', '2604', '1994', '2562', '1650', '2401', '2659', '2587', '2634', '1534', '2401', '1534', '1916'];
			if(in_array($its[$i]['item_id'], $ratId, true)) {
				$itp_rate = rand(33.2, 40.5);
				$dream_rate = $itp_rate;
			}
			$ratId = ['1591', '1605', '1493', '2015', '1769', '1606', '1171'];
			if(in_array($its[$i]['item_id'], $ratId, true)) {
				$itp_rate = rand(8.3, 12.8);
				$dream_rate = $itp_rate;
			}

			//$query1 = "UPDATE ".TABLE_CAT_ITEMS." SET
			//	item_priority_rate='".number_format($item_priority_rate, 2, ".", "")."',
			//	item_priority_urate='".number_format($up_rate, 2, ".", "")."',
			//	item_priority_prate='".number_format($itp_rate, 2, ".", "")."'
			//	WHERE id='".$its[$i]['id']."'";


			if($its[$i]['sect_id'] == 76) {
				$query1 = "UPDATE " . TABLE_CAT_ITEMS . " SET
					item_priority_rate='" . number_format($dream_rate, 2, ".", "") . "',
					item_priority_urate='" . number_format($up_rate, 2, ".", "") . "',
					item_priority_prate='" . number_format($dream_rate, 2, ".", "") . "'
					WHERE id='" . $its[$i]['id'] . "'";
			} else {
				$query1 = "UPDATE " . TABLE_CAT_ITEMS . " SET
					item_priority_rate='" . number_format($itp_rate, 2, ".", "") . "',
					item_priority_urate='" . number_format($up_rate, 2, ".", "") . "',
					item_priority_prate='" . number_format($itp_rate, 2, ".", "") . "'
					WHERE id='" . $its[$i]['id'] . "'";
			}

			$this->db->exec($query1);
			$amount = $catLib->Item_ReqCollected($its[$i]['id'], REQ_STATUS_CONFIRM);
			$query = "UPDATE ".TABLE_CAT_ITEMS." SET amount_done = ".($amount['sum'] ? $amount['sum'] : 0.00).", amount_donepercent = ".($its[$i]['amount']>0 ? (($amount['sum']*100)/($its[$i]['amount'])) : 0)." WHERE id = ".$its[$i]['id'];

			$this->db->exec($query);
		}

		//echo "<br>";
		//var_dump($bestByUser);
		//echo "<br>";

		// Update rating only for one best project of each user as UserRate+ProjRate
		$bestids = Array();
		foreach($bestByUser as $aid => $bi)
		{
			$bestids[] = $bi['id'];
		}

		//echo implode($bestids)


		$query = "UPDATE ".TABLE_CAT_ITEMS." SET item_priority_rate=(item_priority_urate+item_priority_prate) WHERE id IN (".implode(",", $bestids).")";
		$this->db->exec($query);

		if($sort >= 2) {
			$query = "UPDATE ".TABLE_CAT_SORT." SET sort_type = 1";
		} else if($sort == 1)
			$query = "UPDATE ".TABLE_CAT_SORT." SET sort_type = sort_type + 1";
		$this->db->exec($query);
	}

	public function resetDailyMoney() {
		$select = "SELECT * FROM ".TABLE_SHOP_BUYERS_BOX;
		$res = $this->db->query($select);

		$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET money_day = 0";
		$this->db->exec($query);
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET angel_day = 0, angel_day_date = null WHERE angel_day_date <> curdate()";
		$this->db->exec($query);

		//echo '<br>'.date('Y-m-d').'<br>'; die();

		$count = count($res);
		for($i=0; $i<$count; ++$i) {
			//$res[$i]['mw_count'] += 1;
			if($res[$i]['mw_count'] >= 7) {
				$updq = "UPDATE ".TABLE_SHOP_BUYERS." SET angel_week = 0, angel_week_date = null WHERE angel_week_date <> curdate()";
				$this->db->exec($updq);
				$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET money_week = 0, mw_count = 0 WHERE buyer_id = ".$res[$i]['buyer_id'];
			} else {
				$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET mw_count = ".($res[$i]['mw_count']+1).' WHERE buyer_id = '.$res[$i]['buyer_id'];
			}
			$this->db->exec($query);
		}
	}

	public function sendSuccessItemMsg() {
		$query = "SELECT i1.id, i1.author_id, b1.email
							FROM ".TABLE_CAT_ITEMS." i1
							JOIN ".TABLE_SHOP_BUYERS." b1 ON i1.author_id = b1.id
							WHERE (((i1.amount_done *100) / i1.amount) >= 99.98)
							AND is_success = 0";

		$res = $this->db->query($query);
		$count = count($res);

		for($i=0; $i<$count; ++$i) {
			$this->sendUserMsg(FROM_WOH_ID, $res[$i]['author_id'], MSG_TXT);
		}
	}

	protected function sendUserMsg($fromid, $toid, $txt, $projid=0, $replyto=0, $test=1) {
		$query = "INSERT INTO " . TABLE_CAT_MSG . " (item_id, from_id, to_id, reply_msg_id, add_date, modify_date, message) 
			VALUES ('$projid', '$fromid', '$toid', '$replyto', NOW(), NOW(), '" . addslashes($txt) . "')";
		if (!$this->db->exec($query))
			return false;

		$msg_id = $this->db->insert_id();
		if ($msg_id == 0)
			return false;

		$query = "INSERT INTO " . TABLE_CAT_MSG_P2P . " (msg_id, user_id, add_date, modify_date) 
			VALUES ('$msg_id', '$fromid', NOW(), NOW() )";
		if (!$this->db->exec($query))
			return false;

		$query = "INSERT INTO " . TABLE_CAT_MSG_P2P . " (msg_id, user_id, add_date, modify_date)
		VALUES ('$msg_id', '$toid', NOW(), NOW() )";
		if (!$this->db->exec($query))
			return false;

		$to_email = "";
		$from_name = "";

		$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='" . $toid . "'";
		$res = $this->db->query($query);
		if (count($res) > 0) {
			$to_email = $res[0]['email'];
		}


		$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='" . $fromid . "'";
		$res = $this->db->query($query);
		if (count($res) > 0) {
			$from_name = ($res[0]['account_type'] == USR_TYPE_PERS ? $res[0]['name'] . ' ' . $res[0]['fname'] : $res[0]['orgname']);
		}


		// Send notify mail
		if($test==1) {
			if (($to_email != "") && ($from_name != ""))
				$this->sendMsgNotifyMail($to_email, $from_name, $txt);
		}	return true;
	}

	protected function sendMsgNotifyMail($tomail, $sender_name, $msgtext) {
		if( $tomail == "" )
			return;

		$subject = "Новое сообщение на сайте WayOfHelp.com";
		$txt = "<hr><div><h3>Вам пришло новое сообщение на сайте <a href=".WWWHOST.">".$this->cfg['NAME_RU']."</h3></a></div>

<br>Сообщение от <b>".$sender_name."</b>:
".$msgtext."
<br><br>
<a href=".WWWHOST.">Заходи</a>, логинься и смотри что тебе пишут.
<br><br>
Это письмо отправлено в автоматическом
режиме и на него не нужно отвечать. 
<br><br>
Чтобы написать ответ на полученное сообщение
вам необходимо войти в свой кабинет на <a href=".WWWHOST."cabinet/".">wayofhelp.com</a>. 

<br><br>Это письмо отправлено в автоматическом режиме и на него не нужно отвечать. Чтобы написать ответ на полученное сообщение вам необходимо войти в свой кабинет.
	
<br><br><hr>Служба поддержки ".$this->cfg['NAME_RU']."
";
//echo $txt;
		UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $txt);
		UhCmsApp::getMailerInstance()->sendHTMLmail('qwe100com@mail.ru', $subject, $txt);
	}

	public function currencySendPerDay() {
		//$query = "INSERT INTO jhlp_cron_test(val) VALUES(1)";
		//$this->db->exec($query);

		$test_money = 100;
		$allLC = 0;

		$query = "SELECT g1. * , g1.id as item_id, gg1.pays_send, b1.pic_sm AS user_pic, b1.name, b1.fname, b1.account_type, b1.orgname, l1.descr, p1.filename_ico AS pic, c1. * 
							FROM  `jhlp_cat_item` g1
							LEFT JOIN jhlp_cat_item_pics p1 ON g1.id = p1.item_id
							JOIN jhlp_cat_buyer b1 ON b1.id = g1.author_id
							JOIN jhlp_cat_item_lang l1 ON l1.item_id = g1.id
							JOIN jhlp_local_currency c1 ON c1.user_id = g1.author_id
							LEFT JOIN jhlp_pays_game gg1 ON g1.id = gg1.proj_id
							WHERE g1.status = 0
							AND g1.is_play = 1
							AND g1.amount_type = 0
							AND g1.moderated = 1
							GROUP BY b1.id";
		$res = $this->db->query($query);

		foreach ($res as $k => $item) {
			$res[$k]['lc_count'] = $item['portal_money']+
				$item['daily_money']+
				$item['star_money']+
				$item['help_money']+
				$item['comment_money']+
				$item['repost_money']+
				$item['ad_money']+
				$item['aw_money']+
				$item['bonus_money']+
				$item['admin_money'];
			$allLC += $res[$k]['lc_count'];
		}

		function cmp($a, $b) {
			return $b['lc_count'] - $a['lc_count'];
		}

		usort($res, "cmp");
		//print_r($res); die();
		//echo '<pre>';
		//print_r($query); die();

		//echo '<br>';
		//echo $allLC;

		if(!$allLC) return;

		foreach ($res as $proj) {
			$coeff = $proj['lc_count']/$allLC;
			$pr_money = round(($coeff * $test_money), 1);
			echo '<br>'.$pr_money;
			echo '<pre>';

			$query = "SELECT * FROM ".TABLE_BUYER_SOCIAL." WHERE buyer_id = ".$proj['author_id']." AND is_check = ".SOCIAL_CHECK_DONE;
			$is_soc = $this->db->query($query);

			$query = "SELECT * FROM " . TABLE_PAYS_GAME . " WHERE proj_id = " . $proj['item_id'];
			$ch = $this->db->query($query);
			$test_bug = 0;
			if (empty($ch)) {
				if(!empty($is_soc)) {
					if($pr_money > 0) {
						$query = "INSERT INTO " . TABLE_CAT_ITEMS_HELPREQ . " (item_id, sender_id, is_second, req_type, req_status, req_amount, add_date, modify_date, comments)
									VALUES (" . $proj['item_id'] . ", " . FROM_WOH_ID . ", '" . '1' . "', '" . REQ_TYPE_MONEY . "', '" . REQ_STATUS_CONFIRM . "', '" . $pr_money . "',
									NOW(), NOW(), '" . addslashes("") . "')";
						$this->db->exec($query);
						//echo $query.'<br>';
						//echo 'ADD ' . $pr_money . " TO " . $proj['item_id'];
						//$this->setGameDump($proj['item_id'], $proj['author_id'], $pr_money, $coeff, $proj['lc_count']);
					}
					$query = "INSERT INTO " . TABLE_PAYS_GAME . " (proj_id, user_id, pays_send, pays_coef, is_active) VALUES(" . $proj['item_id'] . "," . $proj['author_id'] . "," . $pr_money . "," . $coeff . "," . SOCIAL_CHECK_DONE . ") ";
				} else
					$query = "INSERT INTO " . TABLE_PAYS_GAME . " (proj_id, user_id, pays_send, pays_coef, is_active) VALUES(" . $proj['item_id'] . "," . $proj['author_id'] . "," . $pr_money . "," . $coeff . ",".SOCIAL_CHECK_NONE.") ";
				$this->db->exec($query);
				$test_bug = 1;
			} else {
				if(!empty($is_soc) && !$test_bug) {
					if($pr_money > 0) {
						$query = "INSERT INTO " . TABLE_CAT_ITEMS_HELPREQ . " (item_id, sender_id, is_second, req_type, req_status, req_amount, add_date, modify_date, comments)
									VALUES (" . $proj['item_id'] . ", " . FROM_WOH_ID . ", '" . '1' . "', '" . REQ_TYPE_MONEY . "', '" . REQ_STATUS_CONFIRM . "', '" . $pr_money . "',
									NOW(), NOW(), '" . addslashes("") . "')";

						$this->db->exec($query);
						//echo $query.'<br>';
						//echo 'ADD ' . $pr_money . " TO " . $proj['item_id'];
						//$this->setGameDump($proj['item_id'], $proj['author_id'], $pr_money, $coeff, $proj['lc_count']);
					}
					$query = "UPDATE " . TABLE_PAYS_GAME . " SET pays_send = pays_send + " . $pr_money . ", pays_coef = " . $coeff . ", day_count = day_count+1, is_active = 1  WHERE proj_id = " . $proj['item_id'];
				} else
					$query = "UPDATE " . TABLE_PAYS_GAME . " SET pays_send = pays_send + " . $pr_money . ", pays_coef = " . $coeff . ", day_count = day_count+1, pay_wait = pay_wait + 1 WHERE proj_id = " . $proj['item_id'];
				//print_r($query);
				$this->db->exec($query);
			}

			// ADD PAYS GAME DUMP
			$this->setGameDump($proj['item_id'], $proj['author_id'], $pr_money, $coeff, $proj['lc_count']);

			if ($proj['lc_count'] > 0 && $pr_money > 0){
				$head = UhCmsApp::getLocalizerInstance()->get("crons", "message-accrualstop");
				$head = str_replace('_sum_', $pr_money, $head);
				$txt = UhCmsApp::getLocalizerInstance()->get("crons", "message-accruals");
				$txt = str_replace('_name_', $proj['name'], $txt);
				$txt = str_replace('_sum_', $proj['lc_count'], $txt);
				$this->sendCronsMail( $proj['author_id'], $head, $txt);
			}

		}

		$query = "UPDATE " . TABLE_LOCAL_CURRENCY . " 
								SET 
									admin_money = 0, 
									daily_money = 0,
									star_money = 0,
									help_money = 0,
									comment_money = 0,
									repost_money = 0, 
									ad_money = 0,
									aw_money = 0,
									bonus_money = 0,
									portal_money = 0 
								WHERE 1";
		$this->db->exec($query);
	}

	public function setGameDump($projid, $uid, $pays, $coef, $lc) {
		$query = "INSERT INTO ".TABLE_PAYS_GAME_DUMP." (proj_id, user_id, local_currency, pays_send, pays_coef, date) 
					VALUES(".$projid.",".$uid.",".$lc.",".$pays.",".$coef.", NOW() )";
		return $this->db->exec($query);
	}

	public function stopUserProject() {
		$query = "SELECT id FROM ".TABLE_CAT_ITEMS." i1 WHERE `status` = ".PROJ_STATUS_RUN." AND `amount_type` = ".PROJ_TYPE_MONEY." GROUP BY author_id HAVING count(*) > 1";
		$res = $this->db->query($query);
		if(!empty($res)) {
			foreach ($res as $item) {
				$query = "UPDATE " . TABLE_CAT_ITEMS . " SET status = " . PROJ_STATUS_STOP . " WHERE id = ".$item['id'];
				$this->db->exec($query);
			}
		}
	}

	public function checkUserSocial() {

		$query = "SELECT * FROM ".TABLE_PAYS_GAME." WHERE day_count >= 30 ";
		$res = $this->db->query($query);
		foreach($res as $val) {
			$query = "UPDATE ".TABLE_CAT_ITEMS." SET is_play = 0 WHERE id = ".$val['proj_id'];
			$this->db->exec($query);
		}
		$query = "UPDATE " .TABLE_PAYS_GAME." SET day_count = 0 WHERE day_count >= 30";
		$this->db->exec($query);

		$query = "SELECT g1.*, s1.* FROM ".TABLE_PAYS_GAME." g1
					LEFT JOIN ".TABLE_BUYER_SOCIAL." s1 ON g1.user_id = s1.buyer_id
					WHERE g1.is_active = ".SOCIAL_CHECK_NONE;
		$res = $this->db->query($query);
		/*echo '<pre>';
		print_r($res);*/

		foreach ($res as $val) {
			if($val['pay_wait'] <= 3 && isset($val['is_check']) && $val['is_check']) {
				if($val['pays_send'] > 0) {
					$query = "INSERT INTO " . TABLE_CAT_ITEMS_HELPREQ . " (item_id, sender_id, is_second, req_type, req_status, req_amount, add_date, modify_date, comments)
									VALUES (" . $val['proj_id'] . ", " . FROM_WOH_ID . ", '" . '1' . "', '" . REQ_TYPE_MONEY . "', '" . REQ_STATUS_CONFIRM . "', '" . $val['pays_send'] . "',
									NOW(), NOW(), '" . addslashes("") . "')";
					echo 'ADD ' . $val['pays_send'] . " TO " . $val['proj_id'];
					$this->db->exec($query);
					//$this->setGameDump($val['proj_id'], $val['user_id'], $val['pays_send'], $val['pays_coef']);
				}
				$query = "UPDATE " . TABLE_PAYS_GAME . " SET is_active = " . SOCIAL_CHECK_DONE . ", pay_wait = 0 WHERE user_id = " . $val['user_id'];
			} else if ($val['pay_wait'] > 3) {
				$query = "UPDATE ".TABLE_PAYS_GAME." SET pays_send = 0, pays_coef = 0, pay_wait = 0 WHERE user_id = " . $val['user_id'];
			}
			$this->db->exec($query);
		}

	}

	public function checkSuccessItems() {
		$query = "SELECT r1.item_id, i1.amount, SUM( req_amount ) sum_req 
					FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
					JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id 
					WHERE i1.status = ".PROJ_STATUS_RUN." AND i1.archive = 0 AND i1.active = 1 AND r1.`show` = 1  
					GROUP BY r1.item_id
					HAVING(((sum_req/i1.amount)*100) >= 100)";

		$res = $this->db->query($query);

		foreach ($res as $itm) {
			$query = "UPDATE ".TABLE_CAT_ITEMS." SET status = ".PROJ_STATUS_ENDED.", is_success = 1, stop_date = NOW() WHERE id = ".$itm['item_id'];
			$this->db->query($query);
		}

		$query = "SELECT r1.item_id, i1.amount, SUM( req_amount ) sum_req, (
						SELECT SUM( req_amount ) 
						FROM  ".TABLE_CAT_ITEMS_HELPREQ."
						WHERE item_id = r1.item_id
						AND is_promise =".REQ_PROMISE."
						AND promise_pay =".REQ_PRIMISE_NOPAY."
					) prom
					FROM  ".TABLE_CAT_ITEMS_HELPREQ." r1
					JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
					WHERE i1.is_success = 1
					AND i1.active = 1
					AND i1.archive = 0
					AND i1.status = ".PROJ_STATUS_PAYED."
					AND r1.req_type = ".PROJ_TYPE_MONEY."
					AND i1.amount_type = ".PROJ_TYPE_MONEY."
					GROUP BY i1.id
					HAVING(((sum_req/i1.amount)*100) >= 60)";

		$res = $this->db->query($query);

		foreach ($res as $item) {
			if($item['prom'] == 0) {
				$query = "UPDATE ".TABLE_CAT_ITEMS." SET status = 2, is_success = 1, stop_date = NOW() WHERE id = ".$item['item_id'];
				$this->db->exec($query);
			} else {
				$query = "SELECT * FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE item_id = ".$item['item_id']." AND is_promise = ".REQ_PROMISE." AND promise_pay = ".REQ_PRIMISE_NOPAY." AND `show` = 1";
				$reqitem = $this->db->query($query);

				//print_r($reqitem);

				foreach ($reqitem as $reqits) {
					$date = date('d');
					if($date >= 4 && $date <= 10)
						$days = 7;
					else if($date >= 1 && $date <= 3)
						$days = 10 - $date;
					else {
						$days = date('Y-m-', strtotime('+1 month'));
						$days .= 10;

						$dStart = new DateTime(date('Y-m-d'));
						$dEnd  = new DateTime($days);
						$dDiff = $dStart->diff($dEnd);
						$dDiff->format('%R');

						$days = $dDiff->days;
					}
					$query = "SELECT * FROM ".TABLE_PAYS_PROMISE." WHERE item_id = ".$reqits['item_id']." AND user_id = ".$reqits['sender_id'];
					$check = $this->db->query($query);

					if(empty($check)) {
						$query = "INSERT INTO " . TABLE_PAYS_PROMISE . " (item_id, user_id, date_end, is_pay, day_count)
					 			VALUES(" . $reqits['item_id'] . "," . $reqits['sender_id'] . ", NOW() + INTERVAL ".$days." DAY , ".REQ_PRIMISE_NOPAY.", 0)";

						$this->db->exec($query);

						$txt = UhCmsApp::getLocalizerInstance()->get("crons", "succesmess-paybody");
						$txt = str_replace('_hr_', WWWHOST.'proj/view/'.$reqits['item_id'].'/', $txt);
						$txt = str_replace('_hr2_', WWWHOST.'cabinet/'.$reqits['item_id'].'/', $txt);

						$this->sendUserMsg(FROM_ADMIN_ID, $reqits['sender_id'], $txt, 0, 0, 0);
						$this->sendCronsMail($reqits['sender_id'], UhCmsApp::getLocalizerInstance()->get("crons", "succesmess-paytit"), $txt, 0);
						//$this->sendPulseMail($reqits['sender_id'], UhCmsApp::getLocalizerInstance()->get("crons", "succesmess-paytit"), $txt);
					}

				}

			}
			//$query = "UPDATE " . TABLE_CAT_ITEMS . " SET status = 2 WHERE id = " . $reqits['item_id'];
			//$this->db->exec($query);
		}

		$query = "SELECT r1.item_id, SUM(r1.req_amount)  sum, i1.amount FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
					JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id WHERE r1.req_type = ".PROJ_TYPE_MONEY." AND r1.req_status = ".REQ_STATUS_CONFIRM." AND stop_date IS NULL GROUP BY r1.item_id HAVING((sum/i1.amount)*100)>=60";
		$newIt = $this->db->query($query);
		//print_r($newIt);
		foreach ($newIt as $its) {
			$sq = "UPDATE ".TABLE_CAT_ITEMS." SET stop_date = NOW() WHERE id = ".$its['item_id'];
			$this->db->exec($sq);
		}

	}

	public function checkPromisePays() {
		$query = "SELECT *, DATEDIFF(date_end, NOW()) datadiff, i1.amount FROM ".TABLE_PAYS_PROMISE." p1
		 			JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = p1.user_id
		 			JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = p1.item_id
		 			WHERE is_pay = ".REQ_PRIMISE_NOPAY." AND b1.isactive = 1";
		$res = $this->db->query($query);

		//echo 'run';

		foreach ($res as $item) {
			if($item['datadiff'] <= 0) {
				//echo 'lock - '.$item['user_id'];
				//$query = "UPDATE ".TABLE_SHOP_BUYERS." SET isactive = 0 WHERE id = ".$item['user_id'];
				//	$this->db->exec($query);

				//$query = "UPDATE ".TABLE_CAT_ITEMS." SET status = ".PROJ_STATUS_ENDED." WHERE author_id = ".$item['user_id'];
				//$this->db->exec($query);

				$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." SET `show` = 0 WHERE sender_id = ".$item['user_id']." AND item_id = ".$item['item_id']." AND is_promise = 1";
				$this->db->exec($query);

				$query = "UPDATE ".TABLE_CAT_ITEMS." SET status = ".PROJ_STATUS_RUN." WHERE id = ".$item['item_id']." AND paydone = ".PROJ_PAYED_NONE;
				$this->db->exec($query);
			} else {
				//echo 'mail - '.$item['user_id'];
				// SEND MAIL

				//echo $item['user_id']."<br>";

				$txt = UhCmsApp::getLocalizerInstance()->get("crons", "succesmess-paybody");
				$txt = str_replace('_hr_', WWWHOST.'proj/view/'.$item['item_id'].'/', $txt);
				$txt = str_replace('_hr2_', WWWHOST.'cabinet/'.$item['item_id'].'/', $txt);

				$this->sendUserMsg(FROM_ADMIN_ID, $item['user_id'], $txt, 0, 0, 0);
				$this->sendCronsMail($item['user_id'], UhCmsApp::getLocalizerInstance()->get("crons", "succesmess-paytit"), $txt);
				//$this->sendMsgNotifyMail($to_email, 'WayOfHelp', $this->renderSuccessMess(2, $item['item_id'], $item['user_id']));
				//$this->sendPulseMail($item['user_id'], UhCmsApp::getLocalizerInstance()->get("crons", "succesmess-paytit"), $txt);
			}

		}

	}

	protected function renderSuccessMess($type = 1, $pid, $uid, $sum = 0) {
		if($type === 1) {
			$query = "SELECT SUM(req_amount) FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE item_id = ".$pid." AND is_promise = 1 AND pom_pay = 0 AND `show` = 1";
			$prom = $query[0]['sum'];

			return 'Ваш проект собрал '.$sum.' грн (руб). Из них Вам обещали помочь на сумму '.$prom.'. <br> В данный момент ваш проект приостановлен, а пользователям, которые обещали помочь разосланы уведомления. <br> По правилам WOH, у пользователей есть 1 неделя, чтоб выполнить обещание.<br><br> Если пользователь не переведет деньги, то ваш баланс уменьшится на эту сумму, а к человеку, сделавшему обещание будут применены штрафные санкции';

		} else if($type === 2) {
			$query = "SELECT b1.name FROM ".TABLE_SHOP_BUYERS." b1 WHERE b1.id = ".$uid;
			$name = $this->db->query($query);

			$name = !empty($name) ? $name[0]['name'] : '';
			$query = "SELECT SUM(req_amount) sum FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE `item_id` = ".addslashes($pid)." AND `sender_id` = ".addslashes($uid)." AND `is_promise` = ".REQ_PROMISE." AND `promise_pay` = ".REQ_PRIMISE_NOPAY." AND `req_type` = ".REQ_TYPE_MONEY." AND `show` = 1";
			$pays = $this->db->query($query)[0]['sum'];

			//return '<b>У нас хорошая новость.</b> Ваша помощь помогла закрыть еще один проект. Очередная мечта осуществилась: Чтобы пользователь '.$name.' смог забрать собранные средства, <br> Вам необходимо выполнить свое обещание, данное этому пользователю. Напоминаем, что Вы обещали помочь данному проекту в случае успешного завершения на сумму '.$pays.' грн (руб). <br> Согласно правилам портала, необходимо внести эти средства в течении недели. Просим отнестись с пониманием к этой необходимости, ведь этим взносом вы дарите человеку веру в Доброту и тем самым являетесь катализатором добрых дел!!! ';
			return UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-titstar");
		}
	}

	public function checkDoneData() {
		$query = "SELECT id, author_id, amount_type, end_date, title FROM `jhlp_cat_item` WHERE status = 0 AND active = 1 AND archive = 0 AND DATEDIFF(end_date, CURDATE()) > 0 AND DATEDIFF(end_date, CURDATE()) < 10";
		$itms = $this->db->query($query);

		foreach ($itms as $itm) {
			// SEND MAIL
			$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id=" . $itm['author_id'];
			$res = $this->db->query($query);

			$to_email = "";

			if (count($res) > 0) {
				$to_email = $res[0]['email'];
			}

			$this->sendUserMsg(FROM_ADMIN_ID, $itm['author_id'], $this->renderMsgDoneData($itm['end_date'], $itm['amount_type']), 0, 0, 0);

			if (($to_email != ""))
				$mess = mb_substr($itm['title'], 0, 25) . '...';
			$tit = UhCmsApp::getLocalizerInstance()->get("crons", "message-closeproject");
			$tit = str_replace('_proj_', WWWHOST."proj/view/".$itm['id'], $tit);
			$tit = str_replace('_timestop_', $itm['end_date'], $tit);
			$tit = str_replace('_title_', $mess, $tit);
			//$this->sendMsgNotifyMail($to_email, 'WayOfHelp', $this->renderMsgDoneData($itm['end_date'], $itm['amount_type']));
			$this->sendCronsMail($itm['author_id'], "Внимание! Срок Вашего проекта истекает!", $tit);
		}

	}

	public function renderMsgDoneData($data, $amount = -1) {
		$txt = "Ваш проект скоро <b>заканчивается</b>. <br> Он будет автоматически остановлен <b>".$data."</b> <br> Если вы хотите продлить проект, то можете это сделать из своего личного кабинета, зайдя на вкладку 'Нуждаюсь в помощи'";
		if($amount == PROJ_TYPE_MONEY) {
			$txt .= "<br><br> Напоминаем, если проект не собрал 60% небходимой суммы, то при его закрытии, согласно правил портала, деньги будут переведены на другие проекты. ";
		}
		return $txt;
	}

	public function dailyUserMail() {
		$query = "SELECT b1.id, bb1.status, bb1.money FROM ".TABLE_SHOP_BUYERS." b1
		 JOIN ".TABLE_SHOP_BUYERS_BOX." bb1 ON bb1.buyer_id=b1.id WHERE isactive = 1 AND isactive_web = 1 AND is_bot = 1 ";
		$users = $this->db->query($query);


		//echo '<pre>'; print_r($users); die();

		foreach ($users as $usr) {

			$query = "SELECT count(c1.id) comm, i1.id, i1.title FROM ".TABLE_CAT_ITEMS_COMMENT." c1	
						JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = c1.`item_id`	
						JOIN ".TABLE_CAT_ITEMS_LANGS." il1 ON il1.item_id = i1.id
						WHERE is_read = 0 AND i1.author_id = ".$usr['id']."
						GROUP BY c1.item_id ";

			//echo $query.'<br>';
			//die();
			$comms = $this->db->query($query);

			$tit = '';
			$txt = '';
			$txt_comm = '';
			$cou_co  = 0;
			$yourproj ="";
			$totnum=0;

			if(!empty($comms)) {
				foreach ($comms as $comm) {
					$txt_comm .= '<br><a href="'.WWWHOST.'proj/view/'.$comm['id'].'/'.'" >'.$comm['title'].'</a> -- '.$comm['comm'];
					$yourproj .= '<a href="'.WWWHOST.'proj/view/'.$comm['id'].'/'.'" >'.$comm['title'].'</a>';
					$cou_co += $comm['comm'];

				}
			}

			$query = "SELECT count(*) rates FROM ".TABLE_CAT_ITEMS_STAR_RATE." r1
						JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.`item_id` WHERE i1.author_id = ".$usr['id']." AND i1.status = ".PROJ_STATUS_RUN." AND DATE(r1.add_date) = CURDATE() ";
			//echo $query.'<br>';

			$res['rates'] = $this->db->query($query)[0]['rates'];

			if($cou_co && $res['rates']) {
				$tit = UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-titall");
				$txt = UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-bodyall1");
				$txt .= '<a href="'.WWWHOST.'cabinet/">'.UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-bodyall1").'</a>';
			} else if($cou_co && !$res['rates']) {
				$tit = UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-titcomm");
				$tit = str_replace('#', $cou_co, $tit);
				$txt = UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-bodycomm1");
				$txt = str_replace('_title_', $yourproj, $txt);
				$txtend = UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-bodycomm3");
				$txts = UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-bodycomm2");
				//$txt .= '<a href="'.WWWHOST.'cabinet/">'.UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-bodycomm2").'</a>';
				//$txt.=$txt_comm;

			} else if(!$cou_co && $res['rates']) {
				$tit = UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-titstar");
				$txt = UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-bodystar1");
				$txt .= '<a href="'.WWWHOST.'cabinet/">'.UhCmsApp::getLocalizerInstance()->get("crons", "dailymess-bodystar2").'</a>';
			}

			$nextrang = $usr['status'] + 1;

			$nextang = UhCmsApp::getStatusBox()->getStatus($nextrang);
			$curnum = UhCmsApp::getStatusBox()->getBoxStatus($usr['status']);
			$totnum = $curnum - $usr['money'];

			$angt = '';
			if ($totnum < 50) {
				$txt1 = UhCmsApp::getLocalizerInstance()->get("crons", "message-money");
				$txt1 = str_replace('_hr1_', $nextang, $txt1);
				$txt1 = str_replace('_hr2_', $totnum, $txt1);
				$txt1 = str_replace('_hr3_', WWWHOST . "info/power/", $txt1);
				$angt .= $txt1;
				//echo $txt.'<br>';
				//$this->sendMsgNotifyMail($res[0]['email'], "WayOfHelp", strip_tags($txt));
				//$this->sendPulseMail($res['id'], UhCmsApp::getLocalizerInstance()->get("crons", "message-newbontit"), $txt);
				//$this->sendCronsMail($res['id'], UhCmsApp::getLocalizerInstance()->get("crons", "message-newbontit"), $txt);
			}

			if($txt && $tit) {
				//echo $usr['id'].' -- '.$txt.'<br>';
								$this->sendCronsMail($usr['id'], $tit, $txt.$angt.$txtend, 0);
				//$this->sendPulseMail($usr['id'], $tit, $txt);
			}

		}

	}

	protected function sendCronsMail($uid, $subject, $txt, $share = 1, $unsub = 0) {
		$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id=" . addslashes($uid);
		$res = $this->db->query($query);

		if (count($res) > 0) {
			$tomail = $res[0]['email'];
		}

		if( $tomail == "" )
			return;

		//echo $tomail.'<br>';

		//$render .= "<br><br><hr>Служба поддержки <a href='http://wayofhelp.com/'>".$this->cfg['NAME_RU']."</a>";
		$shtext = '<br>Не забывайте подписываться на наши социальные сети <a href="https://vk.com/wayofhelp">ВК</a>, <a href="https://www.facebook.com/WayofHelp">ФБ</a> и <a href="https://ok.ru/group/53895621509237">ОК</a>';
		$test = ($share == 1 ? $shtext : "");
		$support = '<br>'.$txt.'<br><br>Служба поддержки <a href="'.WWWHOST.'" >WayOfHelp</a>
		'.$test.' ';
		$render = '
		<br><br><br>
		<p style="color: #ababab;">Вы получили это письмо, потому что подписались<br>
		на новости от сайта <a href="https://wayofhelp.com">WayOfHelp</a>.<br><br>

		Вы всегда можете <a href="'.WWWHOST.'users/unsubscribe/?uid='.$uid.'">отказаться</a> от получения наших<br>
		писем, для этого просто перейдите по ссылке</p>
		';

		if($unsub == 0) {
			UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $support);
			UhCmsApp::getMailerInstance()->sendHTMLmail('qwe100com@mail.ru', $subject, $support);
			//$this->sendPulseMail($subject);
		}
	}

	public function sendPulseTest() {
		$SPApiProxy = new SendpulseApi( SENDPULSE_ID, SENDPULSE_KEY, 'file' );
		$email = array(
			'html'    => '<hr>Blablablabla<hr><br><b>This is b</b>',
			'text'    => 'Your email text version goes here',
			'subject' => 'Testing SendPulse API',
			'from'    => array(
				'name'  => 'WayOfHelp',
				'email' => 'lora-tanita@i.ua'
			),
			'to'      => array(
				array(
					'name'  => 'Denys Ruslanovich',
					'email' => 'qwe100com@mail.ru'
				)
			)
		);
		var_dump( $SPApiProxy->smtpSendMail( $email ) );
	}

	public function sendPulseMail($uid, $subject, $html) {
		$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id=" . addslashes($uid);
		$res = $this->db->query($query);

		if (count($res) > 0) {
			$tomail = $res[0]['email'];
			$name = $res[0]['name']+' '+$res[0]['fname'];
		}


		if( $tomail == "" )
			return;

		$text = strip_tags($html);
		$SPApiProxy = new SendpulseApi( SENDPULSE_ID, SENDPULSE_KEY, 'file' );
		$email = array(
			'html'    => $html,
			'text'    => $text,
			'subject' => $subject,
			'from'    => array(
				'name'  => 'WayOfHelp',
				'email' => 'info@wayofhelp.com'
			),
			'to'      => array(
				array(
					'name'  => $name,
					'email' => $tomail
				)
			)
		);
		var_dump($SPApiProxy->smtpSendMail( $email ));
	}

	public function sendUserPowerMsg() {
		$query = "SELECT i1.id, b1.money, b1.status, i1.email
				  FROM ".TABLE_SHOP_BUYERS." i1
				  JOIN ".TABLE_SHOP_BUYERS_BOX." b1 ON i1.id = b1.buyer_id 
				  WHERE i1.is_bot = 1";

		$usrs = $this->db->query($query);

		//$currang = $res[0]['status'];

		foreach ($usrs as $res) {

			$nextrang = $res['status'] + 1;

			$nextang = UhCmsApp::getStatusBox()->getStatus($nextrang);
			$curnum = UhCmsApp::getStatusBox()->getBoxStatus($res['status']);

			$totnum = $curnum - $res['money'];

			echo $res['id'].' = '.$totnum.'<br>';

			if ($totnum) {
				$txt = UhCmsApp::getLocalizerInstance()->get("crons", "message-money");
				$txt = str_replace('_hr1_', $nextang, $txt);
				$txt = str_replace('_hr2_', $totnum, $txt);
				$txt = str_replace('_hr3_', WWWHOST . "info/power/", $txt);
				//echo $txt.'<br>';
				//$this->sendMsgNotifyMail($res[0]['email'], "WayOfHelp", strip_tags($txt));
				//$this->sendPulseMail($res['id'], UhCmsApp::getLocalizerInstance()->get("crons", "message-newbontit"), $txt);
				$this->sendCronsMail($res['id'], UhCmsApp::getLocalizerInstance()->get("crons", "message-newbontit"), $txt);
			}
		}
	}

	public function sendUserNewBon() {
		$query = "SELECT b1.title, b1.id
					FROM  ".TABLE_CAT_BONUS." b1
					WHERE b1.amount>b1.amount_used
					AND b1.premoderation=1 LIMIT 0, 5";

		$res = $this->db->query($query);

		$txt1 = UhCmsApp::getLocalizerInstance()->get("crons", "message-newbon");
		$txt2 = UhCmsApp::getLocalizerInstance()->get("crons", "message-newbon2");
		$txt2 = str_replace('_lora_', WWWHOST."users/viewrev/375/", $txt2);

		$test = "";
		foreach ($res as $raid){
			$count = $raid['title'];

			$txt = UhCmsApp::getLocalizerInstance()->get("crons", "message-newbon1");
			$txt = str_replace('_hr1_', $count, $txt);
			$txt = str_replace('_hr2_', WWWHOST."bonus/view/".$raid['id'], $txt);

			$test .= "$txt";
		}

		$txt = UhCmsApp::getLocalizerInstance()->get("crons", "message-newbon1");
		$txt = str_replace('_hr1_', '...', $txt);
		$txt = str_replace('_hr2_', WWWHOST, $txt);

		$test .= $txt;

		$query = "SELECT id FROM  ".TABLE_SHOP_BUYERS." WHERE is_bot = 1";

		$res = $this->db->query($query);

		foreach ($res as $raid2) {
			//echo $txt1.$test.'<br>';
			$this->sendCronsMail($raid2['id'], UhCmsApp::getLocalizerInstance()->get("crons", "message-newbontit"), $txt1.$test.$txt2, 0);
			//$this->sendPulseMail($raid2['id'], UhCmsApp::getLocalizerInstance()->get("crons", "message-newbontit"), $txt1.$test.$txt2);
		}
	}

	public function sendNewItemMsg($limit = 10) {
		$query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE is_bot = 1";
		$byrs = $this->db->query($query);

		foreach ($byrs as $usr) {
			$query = "SELECT buyer_id, sect_id FROM " . TABLE_SHOP_BUYER_SECTS . " WHERE buyer_id = " . $usr['id'];
			$sect = $this->db->query($query);
			$i = 0;

			$cond_in = "";

			if (!empty($sect)) {
				$cond_in = " AND ci1.sect_id IN ( ";
				for ($j = 0; $j < count($sect); ++$j) {
					$cond_in .= (isset($sect[$j + 1]) ? $sect[$j]['sect_id'] . ',' : $sect[$j]['sect_id']);
				}
				$cond_in .= " ) ";
			}

			//if($i % 3)
			//	echo '$i % 3 = '.($i % 3);

			$query = "SELECT ci1.sect_id, ci1.item_id, il1.title2, i1.add_date, i1.amount_type, i1.obl_id, i1.city_id FROM " . TABLE_CAT_CATITEMS . " ci1 
					 JOIN " . TABLE_CAT_ITEMS_LANGS . " il1 ON ci1.item_id = il1.item_id 
					 JOIN " . TABLE_CAT_ITEMS . " i1 ON i1.id = ci1.item_id
					 WHERE i1.status = " . PROJ_STATUS_RUN . " AND i1.active = 1 AND i1.archive = 0
					  ".$cond_in."
					  ORDER BY i1.add_date DESC, i1.obl_id = ".$usr['obl_id'].", i1.city_id = ".$usr['city_id']."
						LIMIT 0, " . addslashes($limit);

			$res = $this->db->query($query);

			$txt = '<ul>';

			foreach ($res as $itm) {
				$txt .= '<li><a href="'.WWWHOST.'proj/view/'.$itm['item_id'].'/" >'.$itm['title2'].'</a></li>';
			}

			$txt .= '</ul>';

			//echo $txt;

			//echo UhCmsApp::getLocalizerInstance()->get("crons", "newitem-desc").$txt;
						$this->sendCronsMail($usr['id'], UhCmsApp::getLocalizerInstance()->get("crons", "newitem-tit"), UhCmsApp::getLocalizerInstance()->get("crons", "newitem-desc").$txt, 0);
			//$this->sendPulseMail($usr['id'], UhCmsApp::getLocalizerInstance()->get("crons", "newitem-tit"), UhCmsApp::getLocalizerInstance()->get("crons", "newitem-desc").$txt);

		}
	}

	public function sendTopNews(){
		$query = "SELECT l1.title, n1.id
		FROM ".TABLE_NEWS." n1
		INNER JOIN ".TABLE_NEWS_LANGS." l1 ON n1.id=l1.news_id
		WHERE WEEKOFYEAR(NOW()) - WEEKOFYEAR(dtime) = 1
		AND YEAR(dtime) = YEAR(NOW())
		GROUP BY dtime DESC LIMIT 0, 3";

		$res = $this->db->query($query);

		$txt1 = UhCmsApp::getLocalizerInstance()->get("crons", "message-newstext");
		//$txt2 = UhCmsApp::getLocalizerInstance()->get("crons", "message-newsshare");
		$txt = UhCmsApp::getLocalizerInstance()->get("crons", "message-topnews");
		//$txt2 = str_replace('_VK_', "https://vk.com/wayofhelp", $txt2);
		//$txt2 = str_replace('_FB_', "https://www.facebook.com/WayofHelp", $txt2);
		//$txt2 = str_replace('_OK_', "https://ok.ru/group/53895621509237", $txt2);

		$totnews = "";
		foreach ($res as $raid) {
			$count = $raid['title'];

			$txt = UhCmsApp::getLocalizerInstance()->get("crons", "message-topnews");
			$txt = str_replace('_hr1_', $count, $txt);
			$txt = str_replace('_hr2_', WWWHOST."publ/helpwall/".$raid['id'], $txt);

			$totnews .= "$txt";

		}

		$query2 = "SELECT id
		FROM  ".TABLE_SHOP_BUYERS." WHERE is_bot = 1";

		$res2 = $this->db->query($query2);

		foreach ($res2 as $raid2) {
			//$this->sendCronsMail($raid2['id'], UhCmsApp::getLocalizerInstance()->get("crons", "message-newshead"), UhCmsApp::getLocalizerInstance()->get("crons", "message-newsdesc").$txt1 . $totnews);
						$this->sendCronsMail($raid2['id'], UhCmsApp::getLocalizerInstance()->get("crons", "message-newshead"), $txt1.$totnews.$shar, 0);
		}
	}

	public function sendGoodJob(){
		$query = "SELECT add_date, req_amount, req_status, req_type
		FROM `jhlp_cat_requests`
		WHERE WEEKOFYEAR(NOW()) - WEEKOFYEAR(add_date) = 1
		AND YEAR(add_date) = YEAR(NOW()) AND req_status=1
		GROUP BY add_date DESC";

		$res = $this->db->query($query);

		$mproj = 0;
		$totmproj = 0;
		$nomproj = 0;
		$totallproj = 0;
		foreach ($res as $obj){
			if($obj['req_type']==0){
				$mproj++;
				$totmproj = $mproj;
			}

			if($obj['req_type']!=0){
				$nomproj++;
				$totallproj = $nomproj;
			}
		}

		$query = "SELECT id, name, fname
		FROM ".TABLE_SHOP_BUYERS."
		WHERE angel_week>0 ";
		$res2 = $this->db->query($query);
		$usrweek="";

		foreach ($res2 as $usrw) {
			$weekuser = UhCmsApp::getLocalizerInstance()->get("crons", "message-weekuser");
			$weekuser = str_replace('_week_', $usrw['name']." ".$usrw['fname'], $weekuser);
			$weekuser = str_replace('_href_', WWWHOST."users/viewrev/".$usrw['id'], $weekuser);

			$usrweek .= "$weekuser";
		}

		$query = "SELECT bx.*, b.name, b.fname  FROM ".TABLE_SHOP_BUYERS_BOX." bx
				INNER JOIN ".TABLE_SHOP_BUYERS." b ON bx.buyer_id=b.id
				WHERE bx.money_week >0
				ORDER BY bx.money_week DESC limit 0,5";
		$res3 = $this->db->query($query);
		$usract = "";

		foreach ($res3 as $active) {
			$txt2 = UhCmsApp::getLocalizerInstance()->get("crons", "message-active");
			$txt2 = str_replace('_active_', $active['name']." ".$active['fname'], $txt2);
			$txt2 = str_replace('_hrefact_', WWWHOST."users/viewrev/".$active['buyer_id'], $txt2);

			$usract .= "$txt2";
		}

		$query = "SELECT r.sender_id, count(r.id) as sum, b.name, b.fname
		FROM `jhlp_cat_requests` r
		INNER JOIN ".TABLE_SHOP_BUYERS." b ON r.sender_id=b.id
		WHERE req_status=1 AND sender_id<>1992
		GROUP BY r.sender_id ORDER BY count(r.id) DESC limit 0,1";

		$res4 = $this->db->query($query);
		$toptree = "";
		$topid = "";
		foreach ($res4 as $tree) {
			$toptree .= $tree['name'].$tree['fname'];
			$topid .= $tree['sender_id'];
		}
		$sum = $mproj+$nomproj;

		$rating = UhCmsApp::getLocalizerInstance()->get("crons", "message-ratinghere");
		$rating = str_replace('_here_', "http://wayofhelp.com/rating/", $rating);

		$top = UhCmsApp::getLocalizerInstance()->get("crons", "message-topjob");
		$top = str_replace('_alljob_', $sum, $top);

		$alljob = UhCmsApp::getLocalizerInstance()->get("crons", "message-alljob");
		$alljob = str_replace('_alljob_', $sum, $alljob);
		$alljob = str_replace('_moneyjob_', $totmproj, $alljob);

		$txt5 = UhCmsApp::getLocalizerInstance()->get("crons", "message-active1");

		$topusrtree = UhCmsApp::getLocalizerInstance()->get("crons", "message-topusrtree");
		$topusrtree = str_replace('_toptree_', $toptree, $topusrtree);
		$topusrtree = str_replace('_hreftree_', WWWHOST."users/viewrev/".$topid, $topusrtree);

		$topprovoc = UhCmsApp::getLocalizerInstance()->get("crons", "message-topprovoc");
		$topprovoc = str_replace('_topprov_', $toptree, $topprovoc);
		$topprovoc = str_replace('_hrefprov_', WWWHOST."users/viewrev/.$topid", $topprovoc);

		//echo $alljob.$usrweek.$txt5.$usract.$topusrtree.$topprovoc;

		$query = "SELECT id
		FROM  ".TABLE_SHOP_BUYERS." WHERE is_bot = 1";

		$sender = $this->db->query($query);

		foreach ($sender as $raid) {
			$this->sendCronsMail($raid['id'], $top, $alljob.$txt5.$usract.$rating.$topusrtree.$topprovoc, 0);
		}
	}

	// ADD PAYMENT FROM BOTS TO USER PROJECTS

	public function run_BotsAddToProj($cat, $data = '2016-08-13', $amount = MIN_SUM) {
		$query = "SELECT *, TIMESTAMPDIFF(HOUR, start_date, NOW()) diffhour
				 	FROM ".TABLE_CAT_ITEMS." WHERE status = ".PROJ_STATUS_RUN." AND amount_type = ".PROJ_TYPE_MONEY."
		 			AND moderated = 1 AND archive = 0 AND active = 1 AND add_date > '".addslashes($data)."'
		 			AND amount >= ".addslashes($amount)." 
		 			HAVING(diffhour) >= 12 ";

		$res = $this->db->query($query);

		/*echo '<pre>';
		print_r($res); die();*/

		foreach ($res as $item) {
			$min_am = $item['currency_id'] == 2 ? MIN_SUM_TO_PAY_RUB : MIN_SUM_TO_PAY_GRN;
												// NOW()
			$query = "SELECT *, TIMESTAMPDIFF(DAY, NOW(), next_date) datastamp, TIMESTAMPDIFF(DAY, add_date, next_date) diffbydata  
						  FROM " . TABLE_BOTS_PROJPAY . " 
						  WHERE item_id = " . $item['id']." 
						  ORDER BY next_date ASC ";

			$ch = $this->db->query($query);

			if (empty($ch)) {

				if ($item['amount'] >= MIN_SUM && $item['amount'] <= MIN_SUM_NEXT)
					$first_pay = rand($min_am, ($min_am*2)+2);
				else
					$first_pay = rand(($min_am*2), ($min_am*3));

				$bot_id = $this->getRandomBot($item['id']);

				if (!empty($bot_id)) {
					$this->addPayFromBot($item['id'], $bot_id[0]['id'], $first_pay, rand(2, 5));
					$cat->addHelpReq($bot_id[0]['id'], $item['id'], REQ_TYPE_MONEY, REQ_STATUS_CONFIRM, $first_pay);

					/*echo 'Proj id = ' . $item['id'] . '<br>';
					echo 'Bot id = ' . $bot_id[0]['id'] . '<br>';
					echo 'First pay = ' . $first_pay . '<br>';
					echo 'Diff data = ' . $item['diffhour'] . '<br>';
					echo 'Amount = ' . $item['amount'] . '<br>';
					echo 'Sum perc = ' . $sum_perc . '<br>';
					echo 'Sum per day = ' . $sum_per_day . '<br>';
					echo 'Rand day = ' . $rnd_day . '<br>';
					echo 'To pay = ' . $to_pay . '<br><hr>';*/
				}
			} else {
				if($ch[0]['datastamp'] == 0) {
					$bot_id = $this->getRandomBot($item['id']);

					if (!empty($bot_id)) {

						if($ch[0]['next_date'] == null)
							continue;

						end($ch); // MOVE INDEX TO THE LAST ELEMENT IN ARRAY

						$item['amount'] = $item['amount'] - $ch[key($ch)]['req_amount'];
						$sum_perc = round(($item['amount'] * 0.03), 1);

						$sum_per_day = round(($sum_perc / (DAY_FOR_PAY - $ch[key($ch)]['diffbydata'])), 2);

						$to_pay = 0;

						if (($sum_per_day * 5) < $min_am) {
							$rnd_day = rand(2, 5);
							$to_pay = $sum_perc;
						}

						while ($to_pay < $min_am) {
							$rnd_day = rand(2, 5);
							$to_pay = round($sum_per_day * $rnd_day); // SUM * RANDOM DAY NUMBER (2-5)
						}

						$this->addPayFromBot($item['id'], $bot_id[0]['id'], $to_pay, $rnd_day);
						$cat->addHelpReq($bot_id[0]['id'], $item['id'], REQ_TYPE_MONEY, REQ_STATUS_CONFIRM, $to_pay);
					}
				}
			}
		}
	}

	protected function addPayFromBot($pid, $bid, $amount, $days = NULL) {
		$query = "INSERT INTO ".TABLE_BOTS_PROJPAY." (bot_id, item_id, req_amount, add_date, next_date)
		 			VALUES(".$bid.", ".$pid.", ".$amount.", NOW(), ".($days != NULL ? " DATE_ADD(NOW(), INTERVAL ".$days." DAY " : 'NULL')."))";

		return $this->db->exec($query);
	}

	protected function getRandomBot($pid, $limit = 2) {
		$query = "SELECT b1.id FROM ".TABLE_SHOP_BUYERS." b1 
				  WHERE b1.is_bot = 1 AND b1.id NOT IN( SELECT bot_id FROM ".TABLE_BOTS_PROJPAY.")
				  ORDER BY RAND() LIMIT ".$limit;

		$bots = $this->db->query($query);

		if(empty($bots)) {
			$query = "SELECT b1.id FROM ".TABLE_SHOP_BUYERS." b1 
				  WHERE b1.is_bot = 1
				  ORDER BY RAND() LIMIT ".$limit;
			$bots = $this->db->query($query);
		}

		return $bots;
	}

	public function calcUserRateRank() {
		$query = "SELECT buyer_id, user_rate, user_rank, user_rank_diff, user_rank_day, user_rank_day_diff, DATEDIFF(NOW(), bb1.curr_date) diff, 
					(SELECT COUNT(*) FROM ".TABLE_SHOP_BUYERS_BOX." WHERE user_rate >= bb1.user_rate) AS rank_lower,
					(SELECT COUNT(*) FROM ".TABLE_SHOP_BUYERS_BOX." WHERE money_day >= bb1.money_day) AS rank_day_lower 
					FROM ".TABLE_SHOP_BUYERS_BOX." bb1 WHERE 1"; //HAVING(diff <= 30)
		$res = $this->db->query($query);

		foreach ($res as $user) {
			$query = "SELECT city_id FROM ".TABLE_SHOP_BUYERS." WHERE id = ".$user['buyer_id'];
			$sub_res = $this->db->query($query)[0];

			$query = "SELECT `buyer_id`, (SELECT COUNT( * ) FROM " . TABLE_SHOP_BUYERS_BOX . " bb2
    								  JOIN jhlp_cat_buyer b2 ON b2.id = bb2.buyer_id
									  WHERE bb2.user_rate >= bb1.user_rate AND b2.city_id = " . $sub_res['city_id'] . ") AS rank_city_lower
										FROM " . TABLE_SHOP_BUYERS_BOX . " bb1
										JOIN " . TABLE_SHOP_BUYERS . " b1 ON b1.id = bb1.buyer_id
										WHERE b1.city_id = " . $sub_res['city_id'] . " AND b1.id = " . addslashes($user['buyer_id']);

			$sub_res = $this->db->query($query);

			$query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET user_rank_diff = ".($user['rank_lower'] - $user['user_rank'] == 0 ? 'user_rank_diff' : $user['rank_lower'] - $user['user_rank'] ).", user_rank = ".$user['rank_lower'].", 
															user_rank_day_diff = ".$user['rank_day_lower']." - user_rank_day, user_rank_day = ".$user['rank_day_lower'].",
															 user_rank_city_diff = ".$sub_res[0]['rank_city_lower']." - user_rank_city, user_rank_city = ".$sub_res[0]['rank_city_lower']."
															 WHERE buyer_id = ".$user['buyer_id'];
			$this->db->exec($query);

		}

	}

	// CALCULATING USER RATING AND USER RATING PER CURRENT DAY

	public function calcUserRate($catLib) {
		$query = "SELECT b1.*, DATEDIFF(NOW(), bb1.curr_date) diff, bb1.money_day FROM ".TABLE_SHOP_BUYERS." b1
		 JOIN ".TABLE_SHOP_BUYERS_BOX." bb1 ON bb1.buyer_id = b1.id
		 WHERE b1.isactive = 1 AND b1.isactive_web = 1 ";

		$res = $this->db->query($query);

		//echo $query;

		foreach ($res as $user) {
			$reqDoHelpNumByUser =[];
			$reqDoHelpRateByUser = [];

			if( empty($reqDoHelpNumByUser[$user['id']]) ) {
				$reqDoHelpNumByUser[$user['id']] 	= $catLib->Buyer_ReqNum($user['id'], PROJ_SENDHELP, REQ_STATUS_CONFIRM);
				$reqDoHelpRateByUser[$user['id']]	= $catLib->Buyer_ReqStarsRate($user['id']);
			}

			$avgTime = $this->_get_AvgMsgReplyTime($user['id'], $user['id']);	// $row->author_id in second param

			$flag1 = $user['isvip'];
			$flag2 = ($user['pic'] != "" ? 1 : 0);
			$s5 = $reqDoHelpNumByUser[$user['id']];
			$s6 = $user['sc_share_rate'];
			$s7 = ( $avgTime < 60*5 ? 1 : ($avgTime < 60*60 ? 0.5 : ($avgTime < 24*60*60 ? 0.1 : 0) ) );
			$s8 = ( isset($reqDoHelpRateByUser[$user['id']]['avgrate']) ? $reqDoHelpRateByUser[$user['id']]['avgrate'] : 0 );
			$s10 = $catLib->getDateBox($user['id'])[0]['money'];

			$up_rate = 10.0*$flag1+ 5.0*$flag2 + 10.0*( $s5 == 0 ? 0 : ($s5 == 1 ? 0.1 : (1.0 - 1.0/(float)$s5))) + 10.0*( $s6 == 0 ? 0 : ($s6 == 1 ? 0.1 : (1.0 - 1.0/(float)$s6))) + 5.0*$s7 + $s8 + 10.0*( $s10 == 0 ? 0 : ($s10 == 1 ? 0.1 : (1.0 - 1.0/(float)$s10)));

			$catLib->setUserItemRate($user['id'], $up_rate);
		}
	}

	public function calcItemViews($catLib) {

			$query = "SELECT * FROM ".TABLE_CAT_ITEMS." i1 
					WHERE i1.status = ".PROJ_STATUS_RUN." AND i1.archive = 0 AND i1.active = 1 AND i1.moderated = 1 ";

			$res = $this->db->query($query);
			
			foreach ($res as $item) {
				//$new_set = ($item['item_priority_rate'] * $test_views ) / $lt_item;
				$query = "UPDATE ".TABLE_CAT_ITEMS." SET ".( (ceil($item['cat_views']) - $item['item_view']) > 0 ? 'item_view_diff = '.(ceil($item['cat_views']) - $item['item_view']).',' : '' )." item_view = ".ceil($item['cat_views'])." WHERE id = ".$item['id'];
				$this->db->exec($query);
			}

	}

	protected function getActiveUser($days = 30) {
		$query = "SELECT b1.id, DATEDIFF(NOW(), bb1.curr_date) diff FROM ".TABLE_SHOP_BUYERS." b1
		 JOIN ".TABLE_SHOP_BUYERS_BOX." bb1 ON bb1.buyer_id = b1.id
		 WHERE b1.isactive = 1 AND b1.isactive_web = 1 
		 HAVING(diff <= ".$days.") ";

		return $this->db->query($query);
	}

	// NEW ITEM OUTPUT IN CATALOG SYSTEM

	public function new_Item_PriorityRatingUpdate($catLib) {

		$query = "SELECT i1.*, i2.title2, i2.descr0 as descr, u1.isvip, u1.sc_share_rate, u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm,   
				DATE_FORMAT(i1.add_date, '%d.%m.%Y') as adddt,   
				DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
				sect.sect_id, sect.item_id
			FROM ".TABLE_CAT_ITEMS." i1 
			INNER JOIN ".TABLE_CAT_ITEMS_LANGS." i2 ON i1.id=i2.item_id AND i2.lang_id=".$this->LangId." 
			INNER JOIN ".TABLE_SHOP_BUYERS." u1 ON i1.author_id=u1.id   
			INNER JOIN ".TABLE_CAT_CATITEMS." sect ON sect.item_id = i1.id
			WHERE i1.archive=0 AND i1.profile_id='".PROJ_NEEDHELP."' AND i1.status=".PROJ_STATUS_RUN." ";

		$its = $this->db->query($query);

		foreach ($its as $item) {

			$item_rate = $item['item_priority_rate'];
			$item_cat_type = $item['cat_item_type'];    	// Вид проекта, который попадает под сортировку(необученый, обученый, основной)
			$item_cat_click = $item['cat_item_click'];      // Кол-во кликов по проекту из каталога
			$item_cat_views = $item['cat_views'];            // просмотры с каталога
			$upower = $catLib->getDateBox($item['author_id'])[0]['money'];
			$upower = ($upower > 300 ? 300 : $upower);    // сила пользователя за все время

			/* ДЛЯ ТЕСТА */

			$AVGcc = 40;
			$AVGts = 43;
			$AVGcost = 1;

			/* ---------- */

			$query = "SELECT count(DISTINCT r1.sender_id) count FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
					  JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = r1.sender_id
					  WHERE item_id = ".$item['id']." AND b1.is_bot = 0 AND b1.id <> ".FROM_WOH_ID." AND r1.req_byconv = 1 AND r1.`show` = 1";

			$item_cat_conv = $this->db->query($query); 		// Количество конверсий на проекте
			$item_cat_conv = (isset($item_cat_conv[0]) ? $item_cat_conv[0]['count'] : 0);

			if($item_cat_type == 0) {					  	// необученые проекты

				if($upower > 0) {
					
					if ($item_cat_conv > 0) {
						$item_cat_type = 2;
					} else {

						if($item_cat_views >= 300) {
							$item_cat_type = 1;
						} else {
							$newrat = $catLib->recalcItemViewRate($item['id']);
						}
					}
				}

			}
			if($item_cat_type == 1 ) { 						// Обученые проекты
				$CTR = $item_cat_click / $item_cat_views;   // Кол-во кликов по проекту разделен на количество показов в каталоге

				if($item_cat_conv > 0) {
					$item_cat_type = 2;
				} else {

					if ($item_cat_click > (2 * $AVGcc))
						$item_rate = rand(0.05, 0.1);
					else {
						$item_rate = $AVGcost * $CTR;
					}
				}
			}

			if($item_cat_type == 2) {						// Основные проекты
				$item_rate = (($item_cat_conv + 1) / ($item_cat_views + 1)) * $AVGts;
			}

			/*echo 'Item = '.$item['id'].'<br>';
    echo 'Views = '.$item_cat_views.'<br>';
    echo 'Rate = '.$item_rate.'<br>';
    echo 'Type = '.$item_cat_type.'<br>';
    echo 'Conv = '.$item_cat_conv.'<br>';
    echo '<br>'; die();*/

			$query = "UPDATE ".TABLE_CAT_ITEMS." SET item_priority_rate = ".$item_rate.", cat_item_type = ".$item_cat_type." WHERE id = ".$item['id'];
			$this->db->exec($query);

		}

	}

}