<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class MainModel extends PageModelAuth
{
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db, $LangId, "index");		
	}
	
	public function pageLib()
	{
		return $this->page;
	}
	
	public function isFirstVisit()
	{
		if( isset($_COOKIE['wayofhelp']) && ($_COOKIE['wayofhelp']=="washere") )
		{
			return false;
		}
	
		setcookie("wayofhelp","washere", time()+30*24*3600);	// Set cookie for 30 days
	
		return true;
	}

	public function getUsersTree() {
		$query = "SELECT count(*) as count FROM ".TABLE_SHOP_BUYERS;
		return $this->db->query($query)[0];
	}

	//public function get
}
?>