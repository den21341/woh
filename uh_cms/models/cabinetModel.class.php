<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CabinetModel extends PageModelAuth {
	function __construct($config, $db, $LangId) {
		parent::__construct($config, $db, $LangId, "index");		
	}
	
	public function pageLib() {
		return $this->page;
	}
	
	public function restorePass($reqdata) {
		return true;
	}
	
	public function checkLoginExists($email, $uid) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE login='".addslashes($email)."' AND id<>'$uid'";
		$res = $this->db->query($query);
		if( count($res) > 0 )
			return true;
	
		return false;
	}

	public function set_ItemReqStatus($reqid, $status) {
		$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." SET req_status='$status', modify_date=NOW() WHERE id='$reqid'";
		return $this->db->exec($query);
	}
	
	public function del_Item($author_id, $projid) {
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET status='".PROJ_STATUS_STOP."', modify_date=NOW() WHERE id='$projid' AND author_id='$author_id'";
		return $this->db->exec($query);
	}
	
	public function start_Item($author_id, $projid) {
		//$query = "UPDATE ".TABLE_CAT_ITEMS." SET status =  ".PROJ_STATUS_STOP.", modify_date=NOW() WHERE status = ".PROJ_STATUS_RUN." AND author_id = ".$author_id." AND amount_type = ".PROJ_TYPE_MONEY;
		//$this->db->exec($query);
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET status='".PROJ_STATUS_RUN."', modify_date=NOW() WHERE id='$projid' AND author_id='$author_id' AND is_success=0 ";
		return $this->db->exec($query);
	}
	
	public function save_UserInfo($uid, $req) {
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET name='".addslashes($req['rname'])."', fname='".addslashes($req['rfname'])."', 
			phone='".addslashes($req['rtel'])."', email='".addslashes($req['rlogin'])."', comments='".addslashes($req['rdescr'])."',
			obl_id='".$req['robl']."', city_id='".$req['rcity']."', background='".$req['backpicid']."', profession='".addslashes($req['rprof'])."' 
			WHERE id='$uid'";
		return $this->db->exec($query);
	}
	
	public function save_CompInfo($uid, $req) {
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET orgname='".addslashes($req['rorg'])."', orgsphere='".addslashes($req['rorgsphere'])."', 
			employee_num='".addslashes($req['remplnum'])."', name='".addslashes($req['rname'])."', fname='".addslashes($req['rfname'])."',
			phone='".addslashes($req['rtel'])."', email='".addslashes($req['rlogin'])."', comments='".addslashes($req['rdescr'])."',
			obl_id='".$req['robl']."', city_id='".$req['rcity']."', background='".$req['backpicid']."'  WHERE id='$uid'";
		return $this->db->exec($query);
	}
	
	public function save_OrgInfo($uid, $req) {
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET orgname='".addslashes($req['rorg'])."', orgsphere='".addslashes($req['rorgsphere'])."',
			employee_num='".addslashes($req['remplnum'])."', name='".addslashes($req['rname'])."', fname='".addslashes($req['rfname'])."',
			phone='".addslashes($req['rtel'])."', email='".addslashes($req['rlogin'])."', comments='".addslashes($req['rdescr'])."',
			obl_id='".$req['robl']."', city_id='".$req['rcity']."', background='".$req['backpicid']."' WHERE id='$uid'";
		return $this->db->exec($query);
	}
	
	public function set_UserAvatar($uinf, $fname, $ftmp) {
		if( $uinf['pic'] != "" )  {
			//echo "Del: ".$uinf['pic']."<br>";
			@unlink($uinf['pic']);
		}
		
		if( $uinf['pic_sm'] != "" )  {
			//echo "Del: ".$uinf['pic_sm']."<br>";
			@unlink($uinf['pic_sm']);
		}
		
		$newfname0 = $uinf['id']."_".rand(0,1000);

		$newfname = $newfname0.".jpg";
		$newfname_sm = $newfname0."_sm.jpg";
		
		if( UhCmsUtils::ResizeImage($ftmp, AVATAR_DIR.$newfname, ".jpg", AVATAR_W, AVATAR_H, true) ) {
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic='".addslashes(AVATAR_DIR.$newfname)."' WHERE id='".$uinf['id']."'";
			$this->db->exec($query);
		}

		if( UhCmsUtils::ResizeImage($ftmp, AVATAR_DIR.$newfname_sm, ".jpg", AVATAR_SM_W, AVATAR_SM_H, true) ) {
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic_sm='".addslashes(AVATAR_DIR.$newfname_sm)."' WHERE id='".$uinf['id']."'";
			$this->db->exec($query);
		}
		
		unlink($ftmp);
	}
	
	public function save_UserTopics($uid, $topics) {
		$query = "DELETE FROM ".TABLE_SHOP_BUYER_SECTS." WHERE buyer_id='$uid'";
		$this->db->exec($query);
		
		for($i=0; $i<count($topics); $i++)
		{
			$query = "INSERT INTO ".TABLE_SHOP_BUYER_SECTS." (buyer_id, sect_id, add_date) VALUES ('$uid', '".$topics[$i]."', NOW())";
			$this->db->exec($query);
		}
	}
	
	public function save_UserReview($authorid, $uid, $projid, $reqid, $stars, $txt) {
		if( ($stars<1) || ($stars>10) )
			return false; 
		
		$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ_RATE." (author_id, user_id, item_id, req_id, rate, add_date, rate_comments) VALUES 
			('$authorid', '$uid', '$projid', '$reqid', '$stars', NOW(), '".addslashes($txt)."')";
		return $this->db->exec($query);
	}	
	
	public function msg_Send($fromid, $toid, $txt, $projid=0, $replyto=0) {
		$query = "INSERT INTO ".TABLE_CAT_MSG." (item_id, from_id, to_id, reply_msg_id, add_date, modify_date, message) 
			VALUES ('$projid', '$fromid', '$toid', '$replyto', NOW(), NOW(), '".addslashes($txt)."')";
		if( !$this->db->exec($query) )
			return false;
		
		$msg_id = $this->db->insert_id();
		if( $msg_id == 0 )
			return false;
		
		$query = "INSERT INTO ".TABLE_CAT_MSG_P2P." (msg_id, user_id, add_date, modify_date) 
			VALUES ('$msg_id', '$fromid', NOW(), NOW() )";
		if( !$this->db->exec($query) )
			return false;
		
		$query = "INSERT INTO ".TABLE_CAT_MSG_P2P." (msg_id, user_id, add_date, modify_date)
		VALUES ('$msg_id', '$toid', NOW(), NOW() )";
		if( !$this->db->exec($query) )
			return false;
		
		$to_email = "";
		$from_name = "";
		
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE id='".$toid."'";
		$res = $this->db->query($query);
		if( count($res)>0 ) {
			$to_email = $res[0]['email'];					
		}

		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE id='".$fromid."'";
		$res = $this->db->query($query);
		if( count($res)>0 ) {
			$from_name = ( $res[0]['account_type'] == USR_TYPE_PERS ? $res[0]['name'].' '.$res[0]['fname'] : $res[0]['orgname'] );
		}
		
		// Send notify mail
		if( ($to_email != "") && ($from_name != "") )
			$this->sendMsgNotifyMail( $to_email, $from_name, $txt);
		
		return true;
	}
	
	public function msg_SetRead($toid, $projid=0) {
		$query = "SELECT m1.id, mp1.id as ppid  
			FROM ".TABLE_CAT_MSG." m1
			INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id AND mp1.is_read=0
			WHERE m1.to_id='$toid' AND m1.item_id='$projid'";
		//echo $query."<br>";
		$res = $this->db->query($query);
		
		for($i=0; $i<count($res); $i++)
		{
			//echo $res[$i]['id']."<br>";
			$query = "UPDATE ".TABLE_CAT_MSG_P2P." SET is_read=1 WHERE id='".$res[$i]['ppid']."'";
			$this->db->exec($query);
		}
	}
	
	public function msg_SetReadUsrbrd($toid, $fromid) {
		$query = "SELECT m1.id, mp1.id as ppid
			FROM ".TABLE_CAT_MSG." m1
			INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id AND mp1.is_read=0
			WHERE m1.to_id='$toid' AND m1.item_id=0 AND m1.from_id='$fromid'";
		//echo $query."<br>";
		$res = $this->db->query($query);
	
		for($i=0; $i<count($res); $i++)
		{
			echo "clear: ".$res[$i]['id']."<br>";
			$query = "UPDATE ".TABLE_CAT_MSG_P2P." SET is_read=1 WHERE id='".$res[$i]['ppid']."'";
			$this->db->exec($query);
		}
	}
	
	public function msg_NumNew($toid, $projid=0)
	{
		$sql_cond = "";
		if( $projid != 0 )
			$sql_cond = " AND m1.item_id='$projid' ";

		$query = "SELECT count(m1.id) as totmsg
			FROM ".TABLE_CAT_MSG." m1
				WHERE m1.to_id='$toid' AND m1.msg_status = 1 $sql_cond";
		
		//if( $toid == 481 )
		//	echo $query."<br>";
		
		$res = $this->db->query($query);
		if( count($res)>0 )
			return $res[0]['totmsg'];
		
		return 0;
	}
	
	public function msg_NumByProj($userid, $projid, $msgdir = "in")
	{
		$sql_cond = "";
		switch($msgdir)
		{
			case "in":
				$sql_cond = " AND m1.to_id='$userid' ";
				break;
			case "out":
				$sql_cond = " AND m1.from_id='$userid' ";
				break;
		}
		
		if( $msgdir == "new" )
		{
			$query = "SELECT count(m1.id) as msgnum 
			FROM ".TABLE_CAT_MSG." m1
			INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id AND mp1.is_read=0
			WHERE m1.to_id='$userid' AND m1.item_id='$projid'";
		}
		else 
		{
			$query = "SELECT count(m1.id) as msgnum FROM ".TABLE_CAT_MSG." m1 
			WHERE m1.item_id='$projid' $sql_cond";
		}
		
		//if( $userid == 481 )
		//	echo $query."<br>";
		
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			return $res[0]['msgnum'];
		}
		
		return 0;
	}
	
	public function msg_NumFromUser($userid, $fromid, $msgdir = "in", $projid=0)
	{
		$sql_cond = "";
		$sql_cond2 = "";
		switch($msgdir)
		{
			case "in":
				$sql_cond = " AND m1.to_id='$userid' AND m1.from_id='$fromid'";
				$sql_cond2 = " AND m1.to_id=mp1.user_id ";
				break;
			case "innew":
				$sql_cond = " AND m1.to_id='$userid' AND m1.from_id='$fromid'";
				$sql_cond2 = " AND m1.to_id=mp1.user_id ";
				break;
			case "out":
				$sql_cond = " AND m1.from_id='$userid' AND m1.to_id='$fromid' ";
				$sql_cond2 = " AND m1.from_id=mp1.user_id ";
				break;
			case "outnew":
				$sql_cond = " AND m1.from_id='$userid' AND m1.to_id='$fromid' ";
				$sql_cond2 = " AND m1.from_id=mp1.user_id ";
				break;
		}
	
		if( ($msgdir == "innew") || ($msgdir == "outnew") )
		{
			$query = "SELECT count(m1.id) as msgnum
			FROM ".TABLE_CAT_MSG." m1
			INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id $sql_cond2 AND mp1.is_read=0
			WHERE m1.item_id='$projid' $sql_cond";
		}
		else
		{
			$query = "SELECT count(m1.id) as msgnum FROM ".TABLE_CAT_MSG." m1
			WHERE m1.item_id='$projid' $sql_cond";
		}
	
		//if( $userid == 481 )
		//	echo $query."<br>";
	
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			return $res[0]['msgnum'];
		}

		return 0;
	}
	
	public function msg_InList($toid)
	{
		$query = "SELECT m1.*, mp1.is_read, case when i1.id IS NOT NULL then 1 else 0 end as withproj, i1.author_id, 
				s1.account_type, s1.name, s1.fname, s1.orgname, s1.pic_sm as upic_sm, 
				b1.account_type as autype, b1.pic, b1.pic_sm, b1.name as aname, b1.fname as afname, b1.orgname as aorgname   
			FROM ".TABLE_CAT_MSG." m1 
			INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id 
			INNER JOIN ".TABLE_SHOP_BUYERS." s1 ON m1.from_id=s1.id  
			LEFT JOIN ".TABLE_CAT_ITEMS." i1 ON m1.item_id=i1.id 
			LEFT JOIN ".TABLE_SHOP_BUYERS." b1 ON i1.author_id=b1.id 
			WHERE m1.to_id='$toid' 
			ORDER BY mp1.is_read, m1.add_date DESC";		
		$res = $this->db->query($query);
		
		return $res;
	}
	
	public function msg_ListByProjNum($toid) {

		$query = "SELECT count( DISTINCT m1.item_id, m1.from_id, m1.to_id)  as totmsggrp  
			FROM ".TABLE_CAT_MSG." m1
			WHERE m1.to_id='$toid' OR m1.from_id='$toid'";
		
		//if( $toid == 481 )
		//	echo $query."<br>";
		
		$res = $this->db->query($query);
		
		if( count($res)>0 )
			return $res[0]['totmsggrp'];
		
		return 0;
	}
	
	public function msg_ListByProj($toid, $pi=-1, $pn=12) {

		$limit_cond = "";

		if( $pi >= 0 )
			$limit_cond = " LIMIT ".($pi*$pn).",$pn ";
		
		/*
		$query = "SELECT count(m1.id) as totmsg, count(mp1.is_read) as totmsgread, max(m1.add_date) as latestdt, (count(m1.id) - count(mp1.is_read)) as unreadnum, 
				case when (count(m1.id) - count(mp1.is_read))>0 then DATE_ADD(max(m1.add_date), INTERVAL 1 YEAR) else max(m1.add_date) end as lassortdt,   
				case when i1.id IS NOT NULL then CONCAT('puid', b1.id) else CONCAT('uid', s1.id) end as useridgroupby,
				case when i1.id IS NOT NULL then 1 else 0 end as withproj, 
				i1.*, i1.id as item_id, DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
				YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
				TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall    				
			FROM ".TABLE_CAT_MSG." m1
			INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id
			INNER JOIN ".TABLE_SHOP_BUYERS." s1 ON m1.from_id=s1.id
			LEFT JOIN ".TABLE_CAT_ITEMS." i1 ON m1.item_id=i1.id
			LEFT JOIN ".TABLE_SHOP_BUYERS." b1 ON i1.author_id=b1.id
			WHERE m1.to_id='$toid' OR m1.from_id='$toid' 
			GROUP BY m1.item_id, useridgroupby 
			ORDER BY unreadnum DESC, ".( false ? ' mp1.is_read, m1.add_date ' : '' )." lassortdt DESC";
		*/

		/*$query = "SELECT count(m1.id) as totmsg, count(mp1.is_read) as totmsgread, max(m1.add_date) as latestdt, DATE_FORMAT(max(m1.add_date), '%d.%m.%Y %H:%i') as lastdt, (count(m1.id) - count(mp1.is_read)) as unreadnum,
				case when i1.id IS NOT NULL then CONCAT('puid', b1.id) else CONCAT('uid', s1.id) end as useridgroupby,
				case when i1.id IS NOT NULL then 1 else 0 end as withproj,
				i1.*, i1.id as item_id, DATE_FORMAT(i1.start_date, '%d.%m.%Y') as stdt, DATE_FORMAT(i1.end_date, '%d.%m.%Y') as endt,
				YEAR(i1.add_date) as dy, MONTH(i1.add_date) as dm, DAYOFMONTH(i1.add_date) as dd, HOUR(i1.add_date) as dh, MINUTE(i1.add_date) as dmin,
				TIMESTAMPDIFF(MINUTE, NOW(), i1.end_date) as tmleft, TIMESTAMPDIFF(MINUTE, i1.start_date, i1.end_date) as tmall
			FROM ".TABLE_CAT_MSG." m1
			INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id
			INNER JOIN ".TABLE_SHOP_BUYERS." s1 ON m1.from_id=s1.id
			LEFT JOIN ".TABLE_CAT_ITEMS." i1 ON m1.item_id=i1.id
			LEFT JOIN ".TABLE_SHOP_BUYERS." b1 ON i1.author_id=b1.id
			WHERE m1.to_id='$toid' OR m1.from_id='$toid'
			GROUP BY m1.item_id, useridgroupby
			ORDER BY unreadnum DESC, ".( false ? ' mp1.is_read, m1.add_date ' : '' )." latestdt DESC
			$limit_cond";*/

		$query = "SELECT m1.to_id, sum(m1.msg_status <> 0 AND m1.to_id = ".$toid.") new_msg, 
					max(m1.add_date) as latestdt, 
					(CASE WHEN m1.to_id <> ".$toid." THEN m1.to_id ELSE m1.from_id END) user_id
					FROM ".TABLE_CAT_MSG." m1 
					LEFT JOIN ".TABLE_SHOP_BUYERS." s1 ON m1.from_id = s1.id 
					WHERE m1.to_id = ".$toid." OR m1.from_id = ".$toid."
					GROUP BY user_id
					ORDER BY latestdt DESC, new_msg DESC ".$limit_cond;


		//'(SELECT message FROM jhlp_msg_board WHERE from_id IN(".$toid.", user_id) AND to_id IN (".$toid.", user_id) ORDER BY add_date DESC LIMIT 0, 1) last_mess';
		
		//if( $toid == 481 )
			//echo $query."<br>";

		//echo $query; die();

		$res = $this->db->query($query);

		foreach ($res as $k => $mess_list) {
			//echo $mess_list['user_id'].'<br>';
			$query = "SELECT message FROM ".TABLE_CAT_MSG." WHERE from_id IN(".$toid.", ".$mess_list['user_id'].") AND to_id IN (".$toid.", ".$mess_list['user_id'].") ORDER BY add_date DESC LIMIT 0, 1";

			$last = $this->db->query($query);

			if(isset($last[0]['message'])) {
				$res[$k]['last_mess'] = $last[0]['message'];

			} else {
				$res[$k]['last_mess'] = '';
			}
			//print_r($mess_list['last']);
		}

		return $res;
	}
	
	public function msg_ListProj($userid, $projid=0)
	{
		$query = "SELECT m1.*, mp1.is_read, 
				s1.account_type, s1.name, s1.fname, s1.orgname, s1.pic_sm, 
				r1.account_type as r_account_type, r1.name as r_name, r1.fname as r_fname, r1.orgname as r_orgname, r1.pic_sm as r_pic_sm 
		FROM ".TABLE_CAT_MSG." m1
		INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id
		INNER JOIN ".TABLE_SHOP_BUYERS." s1 ON m1.from_id=s1.id 
		INNER JOIN ".TABLE_SHOP_BUYERS." r1 ON m1.to_id=r1.id
		WHERE m1.item_id='$projid' AND (m1.to_id='$userid' OR m1.from_id='$userid')
		ORDER BY m1.add_date DESC";
		$res = $this->db->query($query);
		return $res;
	}
	
	public function msg_ListUser($userid, $senderid=0)
	{
		$query = "SELECT m1.*, mp1.is_read, s1.account_type, s1.name, s1.fname, s1.orgname, s1.pic_sm
		FROM ".TABLE_CAT_MSG." m1
		INNER JOIN ".TABLE_CAT_MSG_P2P." mp1 ON m1.id=mp1.msg_id AND m1.to_id=mp1.user_id
		INNER JOIN ".TABLE_SHOP_BUYERS." s1 ON m1.from_id=s1.id
			WHERE m1.item_id='0' AND ((m1.to_id='$userid' AND m1.from_id='$senderid') OR (m1.from_id='$userid' AND m1.to_id='$senderid'))
			ORDER BY m1.add_date DESC";

		$res = $this->db->query($query);
		return $res;
	}
	
	
	protected function sendMsgNotifyMail($tomail, $sender_name, $msgtext) {
		if( $tomail == "" )
			return;
	
		//$subject = "Новое сообщение на сайте ".WWWHOST."";
		/*$txt = "<hr><div><h3>Вам пришло новое сообщение на сайте <a href=".WWWHOST.">".$this->cfg['NAME_RU']."</h3></a></div>

<br>Сообщение от <b>".$sender_name."</b>:
".$msgtext."

<br><br>Это письмо отправлено в автоматическом режиме и на него не нужно отвечать. Чтобы написать ответ на полученное сообщение вам необходимо войти в свой кабинет.
	
<br><br><hr>Служба поддержки ".$this->cfg['NAME_RU']."
";*/
		$subject = "Новое сообщение на сайте WayOfHelp";
		$txt = "
<br>Сообщение от <b>".$sender_name."</b>:
".$msgtext."
<br><br>
<a href=".WWWHOST.">Заходите</a>, авторизуйтесь и узнайте, что Вам пишут!
<br><br>
Это письмо отправлено в автоматическом<br>
режиме и на него не нужно отвечать. 
<br><br>
Чтобы написать ответ на полученное сообщение<br>
вам необходимо войти в свой кабинет на <a href=".WWWHOST."cabinet/".">wayofhelp.com</a>. 
";
		$render = '<br><br><br>Служба поддержки WayOfHelp
		<br><br>
		<p style="color: #ababab;">Вы получили это письмо, потому что подписались<br>
		на новости от сайта <a href="https://wayofhelp.com">WayOfHelp</a>.<br><br>

		Вы всегда можете отказаться от получения наших<br>
		писем, для этого просто перейдите по ссылке</p>';

		UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $txt.$render);
		//UhCmsApp::getMailerInstance()->sendHTMLmail('qwe100com@mail.ru', $subject, $txt);
	}
	
	
	public function bonus_ListBonus($userid, $sortby="", $pi=-1, $pn=20)
	{
		$sort_cond = "w1.add_date DESC";
		switch($sortby)
		{
			case "add":
				$sort_cond = "w1.add_date";
				break;
		}
		
		$limit_cond = "";
		if( $pi >= 0 )
		{
			$limit_cond = " LIMIT ".($pi*$pn).",".$pn." ";
		}
		
		$query = "SELECT b1.*, b2.title2, b2.descr, w1.point_ind, DATE_FORMAT(w1.add_date, '%d.%m.%Y %H:%i') as windt   
			FROM ".TABLE_CAT_BONUS_WINS." w1 
			INNER JOIN ".TABLE_CAT_BONUS." b1 ON w1.item_id=b1.id 
			INNER JOIN ".TABLE_CAT_BONUS_LANGS." b2 ON b1.id=b2.item_id AND b2.lang_id='".$this->LangId."'
			WHERE w1.user_id='".$userid."' 
			ORDER BY ".$sort_cond." ".$limit_cond;
		$res = $this->db->query($query);
		
		return $res;
	}
	
	public function bonus_Num($userid) {
		$query = "SELECT count(b1.id) as totnum 
			FROM ".TABLE_CAT_BONUS_WINS." w1
			INNER JOIN ".TABLE_CAT_BONUS." b1 ON w1.item_id=b1.id
			INNER JOIN ".TABLE_CAT_BONUS_LANGS." b2 ON b1.id=b2.item_id AND b2.lang_id='".$this->LangId."'
			WHERE w1.user_id='".$userid."'";
		$res = $this->db->query($query);
		if( count($res)>0  ) {
			return $res[0]['totnum'];
		}
		
		return 0;
	}

	public function success_Item($uid, $projid, $cat = '') {
		$query = "UPDATE ".TABLE_CAT_ITEMS." i1
						 JOIN ".TABLE_CAT_ITEMS_HELPREQ." r1 ON r1.item_id = i1.id 
						 SET i1.status='".PROJ_STATUS_ENDED."', i1.is_success=1, i1.modify_date=NOW(), stop_date = NOW() 
						 WHERE r1.item_id = ".$projid." AND i1.author_id='$uid'";
		
		if($cat) {
						$txt = UhCmsApp::getLocalizerInstance()->get("own", "proj-succdesc");
			$txt = str_replace('_perc_', '5', $txt);
			$txt = str_replace('_hr_', WWWHOST.'info/rules/', $txt);
			$txt = str_replace('_cab_', WWWHOST.'cabinet/', $txt);
			$cat->sendUserMailMsg_ct($uid, UhCmsApp::getLocalizerInstance()->get("own", "proj-succtit"), $txt, 0);
		}

		return $this->db->exec($query);
	}

	public function getUserBox($id) {
		if(is_numeric($id)) {
			$query = "SELECT * FROM " . TABLE_SHOP_BUYERS_BOX . " WHERE buyer_id = " . $id;
			$res = $this->db->query($query);
			return $res[0];
		}
		else
			return false;
	}

	public function sendUserIsPromise($pid) {
		$query = "SELECT sender_id FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE item_id = ".addslashes($pid);
		$res = $this->db->query($query);

		foreach ($res as $sender) {
			$query = "SELECT * FROM ".TABLE_PAYS_PROMISE." WHERE item_id = ".addslashes($pid)." AND user_id = ".$sender;
			$check = $this->db->query($query);

			/*if(empty($check)) {
				$query = "INSERT INTO " . TABLE_PAYS_PROMISE . " (item_id, user_id, date_end, is_pay, day_count)
					 			VALUES(" . $pid . "," . $sender . ", NOW() + INTERVAL ".$days." DAY , ".REQ_PRIMISE_NOPAY.", 0)";

				$this->db->exec($query);
			}*/
		}
	}

	public function updUserHelp($uid, $pid, $sum) {
		//$query = "SELECT * FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE sender_id = ".addslashes($uid)." AND item_id = ".addslashes($pid)." AND req_amount = ".addslashes($sum);
		//$ch = $this->db->query($query);

		//if(!empty($ch[0]['id'])) {
		$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." SET promise_pay = 2 WHERE sender_id = ".addslashes($uid)." AND item_id = ".addslashes($pid);
		return $this->db->exec($query);
		//} return false;
	}

	public function acceptPromPay($uid, $pid) {
		if($pid && $uid) {
			$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." SET promise_pay = ".REQ_PRIMISE_PAY." WHERE sender_id = ".addslashes($uid)." AND item_id = ".addslashes($pid);
				$this->db->exec($query);
			$query = "UPDATE ".TABLE_PAYS_PROMISE." SET is_pay = 1 WHERE item_id = ".addslashes($pid)." AND user_id = ".addslashes($uid);
				$this->db->exec($query);
			
			return true;
		} return false;
	}

	public function getTabCount($tab, $uid) {
		$query_tot = "";
		$query_new = "";
		$res = [];

		switch ($tab) {
			case 'needservices' :
				$query_tot = "SELECT count(*) tab_tot FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
						JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id 
						WHERE i1.archive=0 AND i1.active=1 AND i1.profile_id = 1 AND r1.sender_id = ".addslashes($uid);

				$query_new = "SELECT count(*) tab_new FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
						JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id 
						WHERE i1.archive=0 AND i1.active=1 AND i1.profile_id = 1 AND r1.sender_id = ".addslashes($uid)." AND r1.tab_check = 0";
				break;
			case 'myservices' :
				$query_tot = "SELECT count(*) tab_tot FROM ".TABLE_CAT_ITEMS." i1 WHERE i1.archive=0 AND i1.active=1 AND i1.`profile_id` = 1 AND i1.`author_id` = ".addslashes($uid);

				$query_new = "SELECT count(*) tab_new FROM ".TABLE_CAT_ITEMS_HELPREQ." r1 
								JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
								WHERE i1.archive=0 AND i1.active=1 AND i1.profile_id = 1 AND i1.author_id = ".addslashes($uid)." AND req_status = ".REQ_STATUS_NEW." AND tab_check = 0";

				break;
			case 'needhelp' :
				$query_tot = "SELECT count(*) tab_tot FROM ".TABLE_CAT_ITEMS." i1 WHERE i1.author_id  = ".addslashes($uid);

				$query_new = "SELECT count(*) tab_new FROM ".TABLE_CAT_ITEMS_HELPREQ." r1	
								JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
								WHERE i1.archive=0 AND i1.active=1 AND i1.profile_id = 0 AND i1.author_id = ".addslashes($uid)." AND tab_check = 0";

				break;
			case 'myhelp' :
				$query_tot = "SELECT count(DISTINCT r1.item_id) tab_tot FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
								JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
								WHERE i1.archive=0 AND i1.active=1 AND r1.`sender_id` = ".addslashes($uid)." AND i1.profile_id = 0";

				$query_new = "SELECT count(*) tab_new FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
								JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
								WHERE i1.archive=0 AND i1.active=1 AND r1.`sender_id` = ".addslashes($uid)." AND i1.profile_id = 0 AND r1.`req_status` = ".REQ_STATUS_CONFIRM." AND r1.`tab_check` = 0";
				break;
			case 'mymsg' :
				$query_new = "SELECT count(m1.id) as tab_new     
							   FROM ".TABLE_CAT_MSG." m1
							   WHERE m1.to_id=".addslashes($uid)." AND m1.msg_status = 1";
				break;
			case 'thinghelp' :
				$query_tot = "SELECT count(i1.id) as tab_tot FROM ".TABLE_CAT_ITEMS." i1 
							  JOIN ".TABLE_CAT_ITEMS_LANGS." i2 ON i1.id=i2.item_id AND i2.lang_id=1 
							  JOIN ".TABLE_CAT_CATITEMS." cc1 ON i1.id=cc1.item_id 
							  JOIN ".TABLE_CAT_CATALOG." c1 ON cc1.sect_id=c1.id 
							  JOIN ".TABLE_CAT_CATALOG_LANGS." c2 ON c1.id=c2.sect_id AND c2.lang_id=1 
							  WHERE i1.archive=0 AND i1.active=1 AND i1.author_id=".addslashes($uid)." AND i1.profile_id='3'";
				break;
			case 'mybonuswin' :
				$query_tot = "SELECT count(b1.id) as tab_tot
								FROM ".TABLE_CAT_BONUS_WINS." w1
								INNER JOIN ".TABLE_CAT_BONUS." b1 ON w1.item_id=b1.id
								INNER JOIN ".TABLE_CAT_BONUS_LANGS." b2 ON b1.id=b2.item_id AND b2.lang_id='".$this->LangId."'
								WHERE w1.user_id='".addslashes($uid)."'";
				break;
		}

		if($query_tot) {
			$res['tab_tot'] = $this->db->query($query_tot);

			if(!empty($res['tab_tot'])) {
				$res['tab_tot'] = $res['tab_tot'][0]['tab_tot'];
			}
		}
		if($query_new) {
			$res['tab_new'] = $this->db->query($query_new);

			if(!empty($res['tab_new'])) {
				$res['tab_new'] = $res['tab_new'][0]['tab_new'];
			}
		}
		return $res;
	}

	public function getAllMsg($myid, $usrid){

		$query = "SELECT DISTINCT b1.*, b2.is_read, s1.account_type, s1.name, s1.fname, s1.orgname, s1.pic_sm
		FROM ".TABLE_CAT_MSG." b1
		JOIN ".TABLE_CAT_MSG_P2P." b2 ON b1.id=b2.msg_id AND b1.to_id = b2.user_id
		JOIN ".TABLE_SHOP_BUYERS." s1 ON b1.from_id=s1.id
		WHERE b1.from_id IN(".addslashes($myid).", ".addslashes($usrid).")
		AND b1.to_id IN(".addslashes($usrid).",".addslashes($myid).")
		ORDER BY b1.add_date ASC";

		//echo $query; die();

		return $this->db->query($query);
	}
}