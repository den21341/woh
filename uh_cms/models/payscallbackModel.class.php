<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define('PAYSTAT_UNKNOWN',	0);	
define('PAYSTAT_SUCCESS',	1);
define('PAYSTAT_FAILED',	2);
define('PAYSTAT_PENDING',	3);

class PayscallbackModel extends Model {
	protected $LangId;	
	
	function __construct($config, $db, $LangId) {
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
	}

	public function dump_PayOperation($gate_id, $callbackid, $data, $opid = 0)
	{
		// update rate to each product
		$query1 = "INSERT INTO ".TABLE_PAYS_DUMP." (paygate_id, callback_id, add_date, opid, info) 
			VALUES ('".$gate_id."', '".$callbackid."', NOW(), '".$opid."', '".addslashes($data)."')";
		$this->db->exec($query1);		
	}
	
	public function add_PayOperationIk($reqd, $cat) {
		$op_id = $reqd['inv_id'];
		
		$status_id = PAYSTAT_UNKNOWN;
		switch($reqd['inv_st'])
		{
			case "success":	$status_id = PAYSTAT_SUCCESS;	break;
			case "fail":	$status_id = PAYSTAT_FAIL;	break;
			case "pending":	$status_id = PAYSTAT_PENDING;	break;
		}
		
		$first_time_success = false;
		
		// Find if the invoice payment is added
		$query = "SELECT * FROM ".TABLE_PAYS_OPERATIONS." WHERE op_id='".addslashes($op_id)."'";
		$res = $this->db->query($query);

		if( count($res)>0 )		
		{
			echo "Update<br>";
			
			if( ($status_id == PAYSTAT_SUCCESS) && ($res[0]['status'] != PAYSTAT_SUCCESS) )
			{
				$first_time_success = true;
			}
			
			echo "First time: $first_time_success<br>";
			// this invoice was in db, so just update the status
			$query = "UPDATE ".TABLE_PAYS_OPERATIONS." SET status='".$status_id."', modify_date=NOW() WHERE id=".$res[0]['id'];
			$this->db->exec($query);
			
			echo "Updated<br>";
		}
		else
		{
			echo "Insert<br>";
			
			// Prepare data 
			$pm_no = $reqd['pm_no'];

			$pmparts = explode("_", $pm_no);
			//print_r($pmparts);
			$reqproj_id = $pmparts[1];
			$reqpayer_id = $pmparts[2];
			$ownproj_id = (isset($pmparts[3]) ? $pmparts[3] : 0);

			$reqtype = $pmparts[1]; // PAYMENT TYPE
			$reqtypePid = $pmparts[2]; // PROJJECT ID
			$reqtypeUid = (isset($pmparts[3]) ? $pmparts[3] : 0); // USER ID

			if($ownproj_id == 0) {
				$ownproj_id = 1;
			}

			//echo $reqproj_id.'<br>';
			//echo $reqpayer_id.'<br>'; die();

			$payer_id = 0;
			$reciever_id = 0;
			$proj_id = 0;

			$proji = $cat->Item_Info($reqproj_id);

			if( isset($proji['id']) && ($proji['id'] != 0) ) {
				$proj_id = $proji['id'];
				$reciever_id = $proji['author_id'];
			}

			$payeri = $cat->Buyer_Info($reqpayer_id);

			if( $payeri['id'] != 0 ) {
				$payer_id = $payeri['id'];
			} else {
				$payer_id = ($reqpayer_id == -1 ? $reqpayer_id : 0);
			}
			
			//echo $payer_id.":".$reciever_id.":".$proj_id."<br>";
			
			if( $status_id == PAYSTAT_SUCCESS )
				$first_time_success = true;
			//echo "First time: $first_time_success<br>";

			if($reqtype == 1 && $reqtypeUid != 0 && $status_id == PAYSTAT_SUCCESS) { // THIS IS PROMISE
				echo 'pre-one';
				
					$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." SET promise_pay = 1, `show` = 1 WHERE sender_id = ".addslashes($reqtypeUid)." AND item_id = ".addslashes($reqtypePid);
					$this->db->exec($query);

					$query = "UPDATE ".TABLE_PAYS_PROMISE." SET is_pay = 1 WHERE item_id = ".addslashes($reqtypePid)." AND user_id = ".addslashes($reqtypeUid);
					$this->db->exec($query);

					$cat->updUserMoney($reqtypeUid, 8);

					$payer_id = $reqtypeUid;
					$proj_id = $reqtypePid;
					$reciever_id = 0;
					$ownproj_id = 0;
				//}
			} else if ($reqtype == 2 && $reqtypeUid != 0 && $status_id == PAYSTAT_SUCCESS) { //
				echo 'one';
				$query = "SELECT id FROM ".TABLE_CAT_ITEMS_HELPREQ." WHERE req_amount = ".addslashes($reqd['am'])." AND sender_id =  ".addslashes($reqtypeUid)." AND item_id = ".addslashes($reqtypePid)." AND req_status = 0 ";
				$isp = $this->db->query($query);

				$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ." SET req_status = 1, `show` = 1 WHERE id = ".$isp[0]['id'];
				$this->db->exec($query);

				$cat->updUserMoney($reqtypeUid, 10);

				$payer_id = $reqtypeUid;
				$proj_id = $reqtypePid;
				$reciever_id = 0;
				$ownproj_id = 0;

			} else if($reqtype == 3 && $reqtypeUid != 0 && $status_id == PAYSTAT_SUCCESS) { // ITEM PERCENT
				echo 'three';
				$query = "UPDATE ".TABLE_CAT_ITEMS." SET status = ".PROJ_STATUS_PAYED." WHERE id = ".addslashes($reqtypePid)." AND author_id = ".addslashes($reqtypeUid);
				$this->db->exec($query);

				//echo $query; die();

				$payer_id = $reqtypeUid;
				$proj_id = $reqtypePid;
				$reciever_id = 0;
				$ownproj_id = 0;

			} else if($reqtype == 4 && $reqtypeUid != 0 && $status_id == PAYSTAT_SUCCESS) { // THIS PAY WHEN USER PROMISE MORE THAN 1000... HE MUST PAY 10 percent from promise sum
				echo 'four';

				$reqd['prom_val'] = (isset($pmparts[4]) ? $pmparts[4] : 0);

				if($reqd['prom_val']) {
					$isReqSent = ( ($reqMy = $cat->Buyer_ReqIsSend($reqtypeUid, $reqtypePid)) !== false );

					$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ." (item_id, sender_id, is_second, req_type, req_status, req_amount, add_date, modify_date, comments, is_promise, promise_pay, `show`)
						VALUES ('$reqtypePid', '$reqtypeUid', '".($isReqSent ? 1 : 0)."', '".REQ_TYPE_MONEY."', '".REQ_STATUS_CONFIRM."', '".str_replace(",", ".", $reqd['prom_val'])."',
						NOW(), NOW(), '' ".addslashes("").", ".REQ_PROMISE.", ".REQ_PRIMISE_NOPAY.", 1)";
					$this->db->exec($query);
				}

				$cat->addWohPayedItem($reqtypeUid);

				$payer_id = $reqtypeUid;
				$proj_id = $reqtypePid;
				$reciever_id = 0;
				$ownproj_id = 0;

			}
			if (!$reqproj_id && $ownproj_id && $status_id == PAYSTAT_SUCCESS) {
				echo 'true';
				$sub_query = "INSERT INTO " . TABLE_PAYS_CURRENCY . "(user_id, proj_id, pays_count, pays_sum, status, date) VALUES(" . $payer_id . ',' . $ownproj_id . ',' . '1,' . $reqd['am'] . ',' . $status_id . ", NOW())";
				$this->db->exec($sub_query);

				$sub_query = "SELECT id FROM " . TABLE_LOCAL_CURRENCY . " WHERE user_id = " . $payer_id;
				$res = $this->db->query($sub_query);

				if (empty($res)) {
					$sub_query = "INSERT INTO " . TABLE_LOCAL_CURRENCY . " (user_id) VALUES(" . $payer_id . ") ";
					$this->db->exec($sub_query);
				}

				$sub_query = "UPDATE " . TABLE_CAT_ITEMS . " SET is_play = 1 WHERE id = " . $ownproj_id;
				$this->db->exec($sub_query);

				$sub_query = "UPDATE " . TABLE_AB_TESTING_LIST . " SET is_pay = 1 WHERE proj_id = " . $ownproj_id;
				$this->db->exec($sub_query);

				$sub_query = "UPDATE " . TABLE_CAT_ITEMS . " SET status = 0 WHERE id = " . $ownproj_id;
				$this->db->exec($sub_query);
			}
				// add new record to db
				$query = "INSERT INTO " . TABLE_PAYS_OPERATIONS . " (payer_id, reciever_id, status, proj_id, proj_prof, paygate_id, callback_id, 
				add_date, amount, currency, op_id, op_status, pay_via, info) 
				VALUES ('$payer_id', '$reciever_id', '$status_id', '$proj_id', 0, 1, 4, 
				NOW(), '" . $reqd['am'] . "', '" . $reqd['cur'] . "', '" . $reqd['inv_id'] . "', '" . $reqd['inv_st'] . "', '" . $reqd['pw_via'] . "', '" . addslashes(json_encode($reqd)) . "')";
				if (!$this->db->exec($query))
					return false;
			//echo "Inserted<br>";
		}

		if( $first_time_success ) {

			//echo "First<br>";
			if( $payer_id != 0 && $ownproj_id)  {
				echo 'ik_payed';
				//echo "Buyer req added<br>";
				// Add info as project request
				$isReqSent = ( ($reqMy = $cat->Buyer_ReqIsSend($payer_id, $proj_id)) !== false );

				$req_byconv = (isset($pmparts[3]) ? 1 : 0);

				//if( $isReqSent )
				//	$this->goToUrl($this->pageView->Page_BuildUrl("proj", "view/".$projinfo['id']));
				
				//if( $helpsum > 0 )
				//{
					//$this->pageModel->addHelpSum($projid, $uid, $helpsum, $req_help_type, $helptxt);					
					
					//$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ." (item_id, sender_id, req_type, req_status, req_amount, add_date, modify_date, comments)
					//VALUES ('$proj_id', '$payer_id', '".REQ_TYPE_MONEY."', '".( $payer_id != 0 ? REQ_STATUS_NEW : REQ_STATUS_CONFIRM )."', '".str_replace(",", ".", $reqd['am'])."', 
					//NOW(), NOW(), '".addslashes("")."')";
				
					$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ." (item_id, sender_id, is_second, req_type, req_status, req_amount, add_date, modify_date, comments, req_byconv)
						VALUES ('$proj_id', '$payer_id', '".($isReqSent ? 1 : 0)."', '".REQ_TYPE_MONEY."', '".( $payer_id != 0 ? REQ_STATUS_CONFIRM : REQ_STATUS_CONFIRM )."', '".str_replace(",", ".", $reqd['am'])."',
						NOW(), NOW(), '".addslashes("")."', ".$req_byconv.")";

					if( !$this->db->exec($query) )
						return false;
					else {
						$new_req_id = $this->db->insert_id();
						
						// Add rate record
						if( $isReqSent ) {
							// Don't add help rate
							//$query
						}
						else {

							if( $payer_id != 0 ) {
								$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ_RATE." (author_id, user_id, item_id, req_id, rate, add_date, rate_comments) VALUES
									('$reciever_id', '$payer_id', '$proj_id', '$new_req_id', '10', NOW(), 'Спасибо!')";
								if( !$this->db->exec($query) )
									return false;
							}

						}

						if($payer_id == -1) {
							/*$cok_hash = 0;

							if(!isset($_COOKIE["anonuser"]) || $_COOKIE["anonuser"] == '') {
								$cok_hash = md5(rand(0, PHP_INT_MAX));
								setcookie("anonuser", $cok_hash, time() + 60 * 60 * 24 * 365, '/');
							}*/

							$query = "INSERT INTO " . TABLE_ANON_REQUESTS . " (req_id, user_hash, add_date ) 
							 		  VALUES(" . $new_req_id . ", '" . (isset($reqd["anonuser"]) ? $reqd["anonuser"] : '' )  . "', NOW())";

							$this->db->exec($query);
						}

						// ADD REQ TO POST AFFILIATE

						// CHECK IF PROJ WITH COMMISION 20% OR 5%
						$deff_commision = WOH_PERCENT;

						$query = "SELECT * FROM ".TABLE_CAT_ITEMS." WHERE is_arbitr = 1 AND id = ".$proj_id;

						$check = $this->db->query($query);

						//print_r($check); die();

						if(!empty($check)) {
							$deff_commision = WOH_ARBITR_COMMISION;
						}

						//$query = "INSERT INTO `test1` (name, other_id, dt) VALUES(".($reqd['papid']." - ".$reqd['anonuser']).",'1','2016-03-24 04:00:00') )";
						//$this->db->exec($query);

						echo '<br>';
						echo 'COMM = '.$deff_commision.'<br>';
						echo 'PAPVisitorId = '.(isset($reqd['PAPVisitorId']) ? $reqd['PAPVisitorId'] : '---');
						echo '<br>';

						// CURRENCY EXCHANGE // TO RUB

						$am = '';

						if($reqd['cur'] != 'RUB') {
							$am *= UhCmsExch::conver($reqd['cur'], 'RUB');
						}

						$saleTracker = new Pap_Api_SaleTracker(WWWHOST . 'affiliate/scripts/sale.php', true);
						$saleTracker->setAccountId('default1');
						$saleTracker->setVisitorId($reqd['papid']);

						$sale = $saleTracker->createSale();
						$sale->setTotalCost(str_replace(",", ".", $am));
						//echo '%'.$deff_commision * 100; die();
						$sale->setCustomCommission($am * $deff_commision);
						$sale->setOrderID($new_req_id);
						$sale->setProductID($proj_id);
						$saleTracker->register();

						//$reqd['kadid'] = '802457373xeb1b4a284f0398eb23c87acc5786e87a';

						if($reqd['kadid'] != '') {

							//set POST variables
							$url = 'http://postback.kadam.ru/ru/postback/';
							$fields = [
								'data' => $reqd['kadid'],
								'cost' => urlencode(($reqd['am'] * $deff_commision))
							];

							$field_string = http_build_query($fields);

							//echo $field_string; die();

							$ch = curl_init();

							//echo $url.'?'.$field_string; die();

							curl_setopt($ch, CURLOPT_URL, $url.'?'.$field_string);
							//curl_setopt($ch, CURLOPT_POST, 1);
							//curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);

							//echo $ch; die();

							$result = curl_exec($ch);
							echo '<hr>'.'f1='. $result . '<hr>';

							curl_close($ch);
						}

						// ADD CONVERSION TO GOOGLE ADWORDS

						if($reqd['gclid'] != '') {

							echo '<br><br><hr><br>';
							echo $reqd['gclid'].'<br>';

							$conversionName = 'test_woh';
							$gclid = $reqd['gclid'];
							$conversionTime = date('Ymd hms') . ' PM';
							$conversionValue = $am;

							$adw = new UhCmsAdwords();

							try {
								// Get AdWordsUser from credentials in "../auth.ini"
								// relative to the AdWordsUser.php file's directory.
								$user = $adw->getUserObj();

								// Log every SOAP XML request and response.
								$user->LogAll();

								// Run the example.
								$adw->AddOflineConversion($user, $conversionName, $gclid,
									$conversionTime, $conversionValue);
							} catch (OAuth2Exception $e) {
								ExampleUtils::CheckForOAuth2Errors($e);
							} catch (ValidationException $e) {
								ExampleUtils::CheckForOAuth2Errors($e);
							} catch (Exception $e) {
								printf("An error has occurred: %s\n", $e->getMessage());
							}

							$query = "INSERT INTO `test1` (name, other_id, dt) VALUES('gclid - ".$reqd['gclid']."','1','2016-03-24 04:00:00')";
							$this->db->exec($query);
						}

						echo '<hr>ANALITYCS<hr>';

						if($reqd['ga'] != '') {

							$cid = substr($reqd['ga'], 6);

							//echo $reqd['s_ga'].'<br>'.$cid; die();

							/*$t = new Tracker('UA-64704503-1', $reqd['s_ga'], $payer_id);

							if(($reqd['ut_s']) != '') {
								$t->set('dimension1', $reqd['ut_s']);
							}

							if(($reqd['ut_c']) != '') {
								$t->set('dimension2', $reqd['ut_c']);
							}

							if(($reqd['aid']) != '') {
								$t->set('dimension3', $reqd['aid']);
							}

							$t->send(
								'event',
								array(
									'eventCategory' => 'Conversion event',
									'eventAction' => 'Conversion',
									'eventLabel' => '(conv)'
								)); */


							/*
							&pa=purchase                            // Enhanced Ecommerce event
							&ti=2424131                             // Order ID (2424131)
							&ta=External partner                    // Affiliate or Sales man
							&tr=1398000                             // Total Price TAX incl.
							&tt=275000                              // TAX
							&tcc=Premium Delivery                   // Coupon


							 &t=event                                // Event or pagevies
							 &ec=Eshop                               // Event Category
							 &ea=Payment                             // Event Action
							 &el=832224                              // Event Label - transaction ID
							*/

							//echo '<pre>';
							//print_r($reqd); die();


							$url = 'http://www.google-analytics.com/collect';

							$fields = [
								'v' => '1',
								'tid' => 'UA-64704503-1',
								'cid' => $cid,
								'pa' => 'purchase',
								'ti' => $proj_id,
								'tr' => $reqd['am'],
								'cu' => $reqd['cur'],
								'cos' => 3,
								't' => 'event',
							];
							
							if($reqd['utm_c'] != '') {
								$fields['ci'] = $reqd['utm_c'];
							}

							if($reqd['utm_s'] != '') {
								$fields['ck'] = $reqd['utm_s'];
							}

							if($reqd['s_ga'] != '') {
								//'ec' => 'payment_initialization',
								//'ea' => 'successful_payment',
								$fields['ec'] = 'payment_initialization_repost';
								$fields['ea'] = 'successful_payment3';
								$fields['ta'] = $reqd['s_ga'];

							} else if($reqd['aid'] != '') {
								$fields['ec'] = 'payment_initialization_affiliate';
								$fields['ea'] = 'successful_payment1';
								$fields['ta'] = $reqd['aid'];

							} else {
								$fields['ec'] = 'payment_initialization';
								$fields['ea'] = 'successful_payment';
								$reqd['ea'] = 'successful_payment';
							}

							$field_string = http_build_query($fields);
							//echo $field_string; die();

							$ch = curl_init();

							//echo $url.'?'.$field_string; die();

							//echo $field_string; die();

							curl_setopt($ch, CURLOPT_URL, $url.'?'.$field_string);
							//curl_setopt($ch, CURLOPT_POST, 1);
							//curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);

							//echo $ch; die();

							$result = curl_exec($ch);
							echo '<hr>f2=' . $result . '<hr>';

							curl_close($ch);

						}

						return 0;
					}
				//}
			}
		}
		return 0;
	}
	
	public function manual_proc_op($reciever_id, $proj_id, $payer_id, $amount, $issecond=false)
	{
		$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ." (item_id, sender_id, is_second, req_type, req_status, req_amount, add_date, modify_date, comments)
		VALUES ('$proj_id', '$payer_id', '".($issecond ? 1 : 0)."', '".REQ_TYPE_MONEY."', '".( $payer_id != 0 ? REQ_STATUS_CONFIRM : REQ_STATUS_CONFIRM )."', '".str_replace(",", ".", $amount)."',
						NOW(), NOW(), '".addslashes("")."')";
		echo $query."<br>";
		if( !$this->db->exec($query) )
		{
			echo $this->db->getError()."<br>";
			return false;
		}
		else
		{
			$new_req_id = $this->db->insert_id();
			
			echo "ok $new_req_id<br>";
		
			// Add rate record
			if( $issecond )
			{
				// Don't add help rate
				//$query
			}
			else
			{
				if( $payer_id != 0 )
				{
					$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ_RATE." (author_id, user_id, item_id, req_id, rate, add_date, rate_comments) VALUES
					('$reciever_id', '$payer_id', '$proj_id', '$new_req_id', '10', NOW(), 'Спасибо!')";
					if( !$this->db->exec($query) )
					{
						echo $this->db->getError()."<br>";
						return false;
					}
				}
			}
		}
	}
	
	//public function Item_CloseEnded()
	//{
	//	$query = "UPDATE ".TABLE_CAT_ITEMS." SET status='".PROJ_STATUS_ENDED."' WHERE end_date<NOW()";
	//	$this->db->exec($query);
	//}
		
}