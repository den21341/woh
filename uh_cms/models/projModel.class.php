<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define('DEFAULT_PAY_VAL', 100);

class ProjModel extends PageModelAuth
{
	private $langs;

	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db, $LangId, "proj");

		$this->langs = UhCmsApp::getLangs();
	}

	public function pageLib()
	{
		return $this->page;
	}

	public function getUserIdByHash($uhash) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE guid_uhash='".addslashes($uhash)."'";

		$res = $this->db->query($query);

		if( count($res)>0 )
			return $res[0]['id'];
		return 0;
	}

	protected function checkAuthor($author_id, $projid)
	{
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS." WHERE author_id='$author_id' AND id='$projid'";
		$res = $this->db->query($query);

		if( count($res) == 0 )
			return false;

		return true;
	}

	public function saveProject($author_id, $projid, $reqd)
	{
		$is_author = $this->checkAuthor($author_id, $projid);
		if( !$is_author )
			return false;

		$stdt_parts = explode(".", $reqd->stdt);
		$stdt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $stdt_parts[2], $stdt_parts[1], $stdt_parts[0], 1, 0, 0);

		$endt_parts = explode(".", $reqd->endt);
		$endt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $endt_parts[2], $endt_parts[1], $endt_parts[0], 1, 0, 0);


		// Get current project data to compare for changes made 
		$query = "SELECT i1.*, i2.title2, i2.descr0, i2.descr  
			FROM ".TABLE_CAT_ITEMS." i1 
			INNER JOIN ".TABLE_CAT_ITEMS_LANGS." i2 ON i1.id=i2.item_id AND i2.lang_id='".$this->LangId."' 
			WHERE i1.author_id='$author_id' AND i1.id='$projid'";
		$res = $this->db->query($query);

		// Check if the were any changed, then reset flag
		$add_sql_mod = "";
		if( count($res)>0 )
		{
			if( (stripslashes($res[0]['title2']) != $reqd->tit) || (stripslashes($res[0]['descr0']) != $reqd->descr) || (stripslashes($res[0]['descr']) != $reqd->descrfull) )
			{
				$add_sql_mod = " moderated=0, ";
			}
		}
		// Save new data
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET obl_id='".$reqd->robl."', city_id='".$reqd->rcity."', modify_date=NOW(), $add_sql_mod
			start_date='".$stdt_sql."', end_date='".$endt_sql."', amount_type='".$reqd->ptype."', amount='".$reqd->pamount."' WHERE id='$projid'";
		$this->db->exec($query);

		$query = "UPDATE ".TABLE_CAT_ITEMS_LANGS." SET title2='".addslashes($reqd->tit)."', descr0='".addslashes($reqd->descr)."', 
			descr='".addslashes($reqd->descrfull)."' WHERE item_id='$projid' AND lang_id='".$this->LangId."'";
		$this->db->exec($query);

		if( $reqd->sect != 0 )
		{
			$query = "UPDATE ".TABLE_CAT_CATITEMS." SET sect_id='".$reqd->sect."' WHERE item_id='".$projid."'";
			$this->db->exec($query);
		}

		return true;
	}

	public function checkDubProject($guid)
	{
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS." WHERE dub_guid='".addslashes($guid)."'";
		$res = $this->db->query($query);
		if( count($res)>0 )
			return true;

		return false;
	}

	public function addProject($author_id, $reqd) {
		$newpid = 0;

		$stdt_parts = explode(".", $reqd->stdt);
		$stdt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $stdt_parts[2], $stdt_parts[1], $stdt_parts[0], 1, 0, 0);

		$endt_parts = explode(".", $reqd->endt);
		$endt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $endt_parts[2], $endt_parts[1], $endt_parts[0], 1, 0, 0);

		//$aval = ($reqd->ptype == PROJ_TYPE_HUMAN ? $reqd->pamount : 0 );
		//$acost = ($reqd->ptype == PROJ_TYPE_MONEY ? $reqd->pamount : 0 );

		$currency_id = ( $reqd->ptype == PROJ_TYPE_MONEY ? $reqd->pamountizm : 0 );

		$prof_id = PROJ_NEEDHELP;
		if( $reqd->eventtype == 1 )		$prof_id = PROJ_EVENT;
		if( $reqd->thingtype == 1 )		$prof_id = PROJ_THINGS;
		if( $reqd->helptype == 1 )		$prof_id = PROJ_SENDHELP;

		$query = "INSERT INTO ".TABLE_CAT_ITEMS." (author_id, profile_id, obl_id, city_id, status, add_date, modify_date, start_date, end_date, 
				amount_type, amount, currency_id, title, url, dub_guid, is_arbitr) 
				VALUES ('$author_id', '".$prof_id."', '".$reqd->robl."', '".$reqd->rcity."', 0 , NOW(), NOW(), '".$stdt_sql."', '".$endt_sql."', 
				'".$reqd->ptype."', '".$reqd->pamount."', '".$currency_id."', '".addslashes($reqd->tit)."', '', '".addslashes($reqd->dubguid)."', ".addslashes(($reqd->arbitr == 'on' ? 1 : 0)).")";

		if( $this->db->exec($query) )
		{
			$newpid = $this->db->insert_id();
			for($i=0; $i<count($this->langs); $i++)
			{
				$query = "INSERT INTO ".TABLE_CAT_ITEMS_LANGS." (item_id, lang_id, title2, descr0, descr, page_title, page_keywords, page_descr) VALUES 
				('$newpid', '".$this->langs[$i]['id']."', '".addslashes($reqd->tit)."', '".addslashes($reqd->descr)."', '".addslashes($reqd->descrfull)."', '', '', '')";
				if( !$this->db->exec($query) )
				{
					return 0;
				}
			}

			$query = "INSERT INTO ".TABLE_CAT_CATITEMS." (item_id, sect_id) VALUES ('$newpid', '".$reqd->sect."')";
			$this->db->exec($query);
		}
		//else 
		//	return $newpid;

		return $newpid;
	}

	public function setProjectBackground($projid, $backid)
	{
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET background='".$backid."' WHERE id='".$projid."'";
		return $this->db->exec($query);
	}

	public function delProjectPhoto($picid, $projid)
	{
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS_PICS." WHERE id='$picid'";
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			$pic = $res[0];

			if( $pic['item_id'] != $projid )
				return false;

			if( file_exists(stripslashes($pic['filename'])) )
				unlink(stripslashes($pic['filename']));
			if( file_exists(stripslashes($pic['filename_big'])) )
				unlink(stripslashes($pic['filename_big']));
			if( file_exists(stripslashes($pic['filename_thumb'])) )
				unlink(stripslashes($pic['filename_thumb']));
			if( file_exists(stripslashes($pic['filename_ico'])) )
				unlink(stripslashes($pic['filename_ico']));

			$query = "DELETE FROM ".TABLE_CAT_ITEMS_PICS." WHERE id='$picid'";
			$this->db->exec($query);

			return true;
		}
		return false;
	}

	public function addProjectPhoto($author_id, $projid, $pfile, $ind=0, $dir = '')
	{
		$is_author = $this->checkAuthor($author_id, $projid);
		if( !$is_author )
			return false;

		$newpname = $projid."_".rand(10000,40000);

		$pext = ".jpg";
		$ppos = strrpos($pfile['name'], ".");
		if( $ppos !== FALSE )
			$pext = mb_substr($pfile['name'], $ppos);


		$src_path = CAT_PHOTO_DIR.$newpname.mb_strtolower($pext);

		$src_path = $dir.$src_path;

		//echo '<br>'.$src_path;

		//print_r($pfile);
		//echo $src_path; die();
			if (!move_uploaded_file($pfile['tmp_name'], $src_path)) {
				return false;
			}

		// Add record to database
		$psz = UhCmsUtils::GetImageSizeAll($src_path);
		$pw = 200;
		$ph = 150;
		if( $psz != null )
		{
			$pw = $psz['w'];
			$ph = $psz['h'];
		}

		$query = "INSERT INTO ".TABLE_CAT_ITEMS_PICS." (item_id, filename, title, sort_num, src_w, src_h)
		VALUES ('$projid', '".addslashes($src_path)."', '', '$ind', '$pw', '$ph')";
		if( !$this->db->exec($query) )
			return false;

		$file_id = $this->db->insert_id();

		// Make filename for resized pics
		$newfname = $newpname.".jpg";

		// Make big image with watermark				
		if( UhCmsUtils::ResizeImage($src_path, $dir.CAT_BIG_DIR.$newfname, ".jpg", BIGPIC_W, BIGPIC_H, true) )
		{
			$psz = UhCmsUtils::GetImageSizeAll($dir.CAT_BIG_DIR.$newfname);
			$pw = BIGPIC_W;
			$ph = BIGPIC_H;
			if( $psz != null )
			{
				$pw = $psz['w'];
				$ph = $psz['h'];
			}

			$query = "UPDATE ".TABLE_CAT_ITEMS_PICS." SET filename_big='".addslashes(CAT_BIG_DIR.$newfname)."', big_w='$pw', big_h='$ph' WHERE id='".$file_id."'";
			$this->db->exec($query);
		}

		// Make thumb image with watermark
		if( UhCmsUtils::ResizeImage($src_path, $dir.CAT_THUMB_DIR.$newfname, ".jpg", THUMB_W, THUMB_H, true) )
		{
			$psz = UhCmsUtils::GetImageSizeAll($dir.CAT_THUMB_DIR.$newfname);
			$pw = THUMB_W;
			$ph = THUMB_H;
			if( $psz != null )
			{
				$pw = $psz['w'];
				$ph = $psz['h'];
			}

			$query = "UPDATE ".TABLE_CAT_ITEMS_PICS." SET filename_thumb='".addslashes(CAT_THUMB_DIR.$newfname)."', thumb_w='$pw', thumb_h='$ph' WHERE id='".$file_id."'";
			$this->db->exec($query);
		}

		// Make ico image with watermark
		if( UhCmsUtils::ResizeImage($src_path, $dir.CAT_SMTHUMB_DIR.$newfname, ".jpg", SMTHUMB_W, SMTHUMB_H, true) )
		{
			$psz = UhCmsUtils::GetImageSizeAll($dir.CAT_SMTHUMB_DIR.$newfname);
			$pw = SMTHUMB_W;
			$ph = SMTHUMB_H;
			if( $psz != null )
			{
				$pw = $psz['w'];
				$ph = $psz['h'];
			}

			$query = "UPDATE ".TABLE_CAT_ITEMS_PICS." SET filename_ico='".addslashes(CAT_SMTHUMB_DIR.$newfname)."', ico_w='$pw', ico_h='$ph' WHERE id='".$file_id."'";
			$this->db->exec($query);
		}
		/*if(UhCmsUtils::ResizeImage($src_path, CAT_PROJ_DIR.$newfname, ".jpg", PR_W, PR_H, true)) {
			$psz = UhCmsUtils::GetImageSizeAll(CAT_PROJ_DIR.$newfname);
			$pw = PR_W;
			$ph = PR_H;
			if( $psz != null )
			{
				$pw = $psz['w'];
				$ph = $psz['h'];
			}

			$query = "UPDATE ".TABLE_CAT_ITEMS_PICS." SET filename_pr='".addslashes(CAT_PROJ_DIR.$newfname)."', pr_w='$pw', pr_h='$ph' WHERE id='".$file_id."'";
			$this->db->exec($query);
		}*/

		return true;
	}

	public function set_UserAvatar($uinf, $fname, $ftmp)
	{
		if( $uinf['pic'] != "" )
		{
			//echo "Del: ".$uinf['pic']."<br>";
			@unlink($uinf['pic']);
		}

		if( $uinf['pic_sm'] != "" )
		{
			//echo "Del: ".$uinf['pic_sm']."<br>";
			@unlink($uinf['pic_sm']);
		}

		$newfname0 = $uinf['id']."_".rand(0,1000);

		$newfname = $newfname0.".jpg";
		$newfname_sm = $newfname0."_sm.jpg";

		if( UhCmsUtils::ResizeImage($ftmp, AVATAR_DIR.$newfname, ".jpg", AVATAR_W, AVATAR_H, true) )
		{
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic='".addslashes(AVATAR_DIR.$newfname)."' WHERE id='".$uinf['id']."'";
			$this->db->exec($query);
		}

		if( UhCmsUtils::ResizeImage($ftmp, AVATAR_DIR.$newfname_sm, ".jpg", AVATAR_SM_W, AVATAR_SM_H, true) )
		{
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic_sm='".addslashes(AVATAR_DIR.$newfname_sm)."' WHERE id='".$uinf['id']."'";
			$this->db->exec($query);
		}

		unlink($ftmp);
	}

	public function addHelpSum($projid, $uid, $sum, $type, $comment)
	{
		$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ." (item_id, sender_id, req_type, req_amount, add_date, modify_date, comments) 
			VALUES ('$projid', '$uid', '$type', '".str_replace(",", ".", $sum)."', NOW(), NOW(), '".addslashes($comment)."')";
		if( !$this->db->exec($query) )
			return false;

		return true;
	}

	public function add_ProjComment($author_id, $reqd)
	{
		$query = "INSERT INTO ".TABLE_CAT_ITEMS_COMMENT." (item_id, author_id, add_date, author, author_email)
			VALUES ('".$reqd['projid']."', '$author_id', NOW(), '', '')";

		if( $this->db->exec($query) ) {
			$newpid = $this->db->insert_id();

			for($i=0; $i<count($this->langs); $i++) {

				$query = "INSERT INTO ".TABLE_CAT_ITEMS_COMMENT_LANGS." (item_id, lang_id, content) VALUES
					('$newpid', '".$this->langs[$i]['id']."', '".addslashes(strip_tags($reqd['comment'], '<br>'))."')";

				if( !$this->db->exec($query) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function getUserBox($id) {
		if(is_numeric($id)) {
			$query = "SELECT * FROM " . TABLE_SHOP_BUYERS_BOX . " WHERE buyer_id = " . $id;
			$res = $this->db->query($query);
			return $res[0];
		}
		else
			return false;
	}

	public function sendUserMailMsg($subject, $txt, $uid, $unsub = 0) {
		$tomail = $this->getUserEmail($uid);

		if (!$tomail)
			return;

		//$subject = "Новый комментарий к Вашему проекту на сайте " . WWWHOST . "";
		/*$txt = "<hr><div><h3>Кто-то оставил комментарий к вашему <a href=".WWWHOST.'proj/view/'.$item_id.">проекту</a> на сайте <a href=" . WWWHOST . ">" . $this->cfg['NAME_RU'] . "</h3></a></div>

						<br><br>Это письмо отправлено в автоматическом режиме и на него не нужно отвечать. Чтобы написать ответ на полученное сообщение вам необходимо войти в свой кабинет.

						<br><br><hr>Служба поддержки " . $this->cfg['NAME_RU'] . "";*/

				$txt .= "<br><br>Служба поддержки "."<a href=\"http://wayofhelp.com\">".$this->cfg['NAME_RU']."</a>";
		$render = '<br><br>
		<p style="color: #ababab;">Вы получили это письмо, потому что подписались<br>
		на новости от сайта <a href="https://wayofhelp.com">WayOfHelp</a>.<br><br>

		Вы всегда можете отказаться от получения наших<br>
		писем, для этого просто перейдите по ссылке</p>
		';
		$testunsub = ($unsub == 0? "" : $render);

		UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $txt.$testunsub);
		UhCmsApp::getMailerInstance()->sendHTMLmail('qwe100com@mail.ru', $subject, $txt.$testunsub);
	}

	public function getUserEmail($uid) {
		$query = "SELECT email FROM ".TABLE_SHOP_BUYERS." WHERE id = ".$uid;

		$email = $this->db->query($query);

		if(isset($email[0]['email']) && $email[0]['email'])
			return $email[0]['email'];
		return 0;
	}

	public function checkProjUser($uid, $projid) {
		if(is_numeric($uid) && is_numeric($projid)) {
			$query = "SELECT * FROM ".TABLE_CAT_ITEMS." WHERE id = ".addslashes($projid)." AND author_id = ".addslashes($uid);
			return $this->db->query($query);
		} return false;
	}

	public function checkABTesting($proj_id) {
		if(is_numeric($proj_id)) {
			$query = "SELECT * FROM ".TABLE_AB_TYPE;
			$type = $this->db->query($query);
			$type = (!$type[0]['type_next'] ? $type[0]['type'] : $type[0]['type_next']);

			$query = "SELECT * FROM " . TABLE_AB_TESTING_LIST . " 
								WHERE proj_id = " . addslashes($proj_id);
			$check = $this->db->query($query);

			if (empty($check)) {
				//print_r($type);
				$query = "SELECT * FROM " . TABLE_AB_TESTING . " WHERE test_type = ".$type;
				//print_r($query);
				$res = $this->db->query($query);

				if (empty($res))
					return [['pays_val' => DEFAULT_PAY_VAL]];

				if ($res[0]['pays_next'] == 0) {
					$query = "UPDATE " . TABLE_AB_TESTING . " SET pays_next = 1 WHERE test_type = ".$type;
				} else if ($res[0]['pays_next'] >= count($res)) {
					$query = "UPDATE " . TABLE_AB_TESTING . " SET pays_next = 1 WHERE test_type = ".$type;
				} else {
					$query = "UPDATE " . TABLE_AB_TESTING . " SET pays_next = pays_next + 1 WHERE test_type = ".$type;
				}
				$this->db->exec($query);


				$query = "INSERT " . TABLE_AB_TESTING_LIST . " (proj_id, pays_val, is_pay, test_type) VALUES(" . addslashes($proj_id) . "," . $res[0]['pays_next'] . ", 0, ".$type.")";
				$this->db->exec($query);

				/*if($type > count($type)) {
                    $query = "UPDATE " . TABLE_AB_TYPE . " SET type_next = 1";
                } else {
                    $query = "UPDATE " . TABLE_AB_TYPE . " SET type_next = type_next + 1";
                }*/
				$query = "UPDATE " . TABLE_AB_TYPE . " SET type_next = 2";
				$this->db->exec($query);
			}

			$query = "SELECT abl1.proj_id, abl1.test_type, abl1.is_pay, ab1.pays_val FROM " . TABLE_AB_TESTING_LIST . " abl1 
									JOIN " . TABLE_AB_TESTING . " ab1 ON ab1.id = abl1.pays_val" . "
									WHERE abl1.proj_id = " . $proj_id;
			return $this->db->query($query);

		} return false;
	}

	public function checkPaymentsProj($uid, $proj_id) {
		if(is_numeric($uid) && is_numeric($proj_id)) {
			$query = "SELECT * FROM ".TABLE_CAT_ITEMS." WHERE id = ".addslashes($proj_id)." AND author_id = ".addslashes($uid)." AND is_play = 1";
			return $this->db->query($query);
		} return false;
	}

	public function checkAmountTypeProj($proj_id) {
		$query = "SELECT amount_type FROM ".TABLE_CAT_ITEMS." WHERE id = ".addslashes($proj_id);
		return $this->db->query($query);
	}

	public function checkCountMoneyProj($uid) {
		$query = "SELECT * FROM ".TABLE_CAT_ITEMS." WHERE status = ".PROJ_STATUS_RUN." AND `amount_type` = ".PROJ_TYPE_MONEY." AND `author_id` = ".addslashes($uid)." AND `is_play` = 1";
		return $this->db->query($query);
	}

	public function checkUserHelp($type = PROJ_TYPE_MONEY, $data = 'all', $uid) {
		$cond = "";
		if($data == 'all') {
			$cond = " AND r1.add_date > i1.add_date ";
		}

		$query = "SELECT SUM(r1.req_amount) as sum FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
					JOIN ".TABLE_CAT_ITEMS." i1 ON i1.author_id = r1.sender_id
					WHERE r1.`req_type` = ".addslashes($type)." AND `req_status` = ".REQ_STATUS_CONFIRM." AND `sender_id` = ".addslashes($uid).$cond;

		return $this->db->query($query);
	}

	public function runProjABtest($projId) {
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET is_play = 1 WHERE id = ".addslashes($projId);
		$this->db->exec($query);
		$query = "UPDATE ".TABLE_AB_TESTING_LIST." SET is_pay = 1 WHERE proj_id = ".addslashes($projId);
		return $this->db->exec($query);
	}

	public function setUserProjUnswer($uid, $req) {
		$id = 1;
		foreach ($req as $ans) {
			if(!is_array($ans)) {
				//echo $ans.'<br>';
				$query = 'INSERT INTO jhlp_proj_test_ans (user_id, q_id, answer) VALUES ('.$uid.','.$id.',"'.addslashes($ans).'")';
				$this->db->exec($query);
			} else {
				foreach ($ans as $check) {
					if($check) {
						//echo $check.'<br>';
						$query = 'INSERT INTO jhlp_proj_test_ans (user_id, q_id, answer) VALUES (' . $uid . ',' . $id . ',"' . addslashes($check) . '")';
						$this->db->exec($query);
					}
				}
			}
			++$id;
		}
	}

	public function setUserMoney($uid, $money = 0) {
		if(is_numeric($uid) && is_numeric($money)) {
			$query = "UPDATE " . TABLE_SHOP_BUYERS_BOX . " SET money = money + ".addslashes($money).", help_money = help_money + ".addslashes($money)." WHERE buyer_id = ".addslashes($uid);
			return $this->db->exec($query);
		} else return false;
	}

	public function setHelpReq($uid, $projid, $helpReq = REQ_TYPE_OTHER, $reqStatus = 1, $amount = 1, $second = 0, $is_promise = REQ_PRIMISE_NOPAY, $show = 1, $req = 0) {
		$query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ." (item_id, sender_id, req_type, req_status, is_second, show_first, req_amount, add_date, modify_date, comments, is_promise, `show`, req_byconv)
		 			VALUES(".$projid.", ".$uid.", ".$helpReq.", ".addslashes($reqStatus).", ".$second.", 0, ".$amount.", NOW(), NOW(), '', ".$is_promise.", ".addslashes($show).", ".addslashes($req).")";
		return $this->db->exec($query);
	}

	// METHOD CONVERT ALL USER PROMISE TO ONE CURRENCY AND COUNT REQUESTS

	public function getUserSumPromiseMoney($uid, $with_wohpay = 0, $convert = 0, $cur_from = 'RUB', $cur_to = 'GRN', $promis = REQ_PROMISE, $prom_send = REQ_PRIMISE_NOPAY) {
		if($convert) {
			$query = "SELECT r1.item_id, i1.currency_id, r1.req_amount FROM ".TABLE_CAT_ITEMS_HELPREQ." r1
		 		  JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = r1.item_id
		 		  WHERE r1.sender_id =  ".addslashes($uid)." AND r1.is_promise = ".$promis." AND r1.promise_pay = ".$prom_send
					. ($with_wohpay ? " AND item_id IN (SELECT item_id FROM " . TABLE_BUYER_WOHPAY . " WHERE user_id = " . $uid . ")" : '');

			$res = $this->db->query($query);

			$req_sum = 0;

			foreach ($res as $itm) {
				$query = "SELECT * FROM ".TABLE_CAT_CURRENCY." WHERE id = ".$itm['currency_id'];
				$curr_data = $this->db->query($query);

				if(isset($curr_data[0]) && ($curr_data[0]['cur_code'] == $cur_from)) { // CONVERT RUB TO UAH
					$itm['req_amount'] *= UhCmsExch::conver($cur_from, $cur_to);
				}

				$req_sum += $itm['req_amount'];
			}

			return  array(array('psum' => round($req_sum, 2), 'pcur' => $cur_to));

		} else {
			$query = "SELECT sum(req_amount) as psum FROM " . TABLE_CAT_ITEMS_HELPREQ . " WHERE sender_id = " . addslashes($uid) . " AND is_promise = " . addslashes($promis) . "
		 	AND promise_pay = " . addslashes($prom_send) . ($with_wohpay ? " AND item_id IN (SELECT item_id FROM " . TABLE_BUYER_WOHPAY . " WHERE user_id = " . $uid . ")" : '');

			return $this->db->query($query);
		}
	}

	public function setBuyerNumber($uid, $code, $phone, $is_active = PHONE_NOACTIVE) {
		$query = "SELECT * FROM ".TABLE_CAT_BUYER_SMS." WHERE buyer_id = ".addslashes($uid);
		$res = $this->db->query($query);

		if(empty($res))
			$query = "INSERT INTO ".TABLE_CAT_BUYER_SMS." (buyer_id, code, phone, is_active)
		 			VALUES(".addslashes($uid).", ".addslashes($code).", ".addslashes($phone).", ".addslashes($is_active).")";
		else
			$query = "UPDATE ".TABLE_CAT_BUYER_SMS." SET code = ".addslashes($code).", phone = ".addslashes($phone).", is_active = ".PHONE_NOACTIVE;

		return $this->db->exec($query);
	}

	public function updBuyerNumber($uid) {
		$query = "UPDATE ".TABLE_CAT_BUYER_SMS." SET is_active = ".PHONE_ACTIVE." WHERE buyer_id = ".addslashes($uid);
		return $this->db->exec($query);
	}

	public function checkBuyerNumber($uid) {
		$query = "SELECT * FROM ".TABLE_CAT_BUYER_SMS." WHERE is_active = ".PHONE_ACTIVE." AND buyer_id = ".addslashes($uid);
		return $this->db->query($query);
	}

	public function getPaymntInfo($type = 'count', $lang = 1) {
		$type_select = "";
		if($type == 'count')
			$type_select = " COUNT(*) count ";
		if($type == 'all')
			$type_select = " * ";

		$query = "SELECT ".$type_select." FROM ".TABLE_ITEM_PAYMENT." WHERE lang = ".addslashes($lang);
		return $this->db->query($query);
	}

	public function setProjPayment($pid, $pymnt, $is_woh) {
		if(is_array($pymnt)) {
			for($i=1; $i<count($pymnt); ++$i) {
				if($pymnt[$i] != '') {
					$query = "INSERT INTO ".TABLE_ITEM_PAYMENT_LIST." (item_id, payment_id, account) VALUES (".addslashes($pid).", ".addslashes($i).", '".addslashes($pymnt[$i])."')";
					$this->db->exec($query);
				}
			}
			if($is_woh == 'on') {
				//$query = "INSERT INTO ".TABLE_ITEM_PAYMENT_LIST." (item_id, payment_id, account) VALUES (".addslashes($pid).", 10 , 1)";
				//$this->db->exec($query);
			}
			return 1;
		} return 0;
	}

	public function getItem($pid) {
		$query = "SELECT i1.*, c1.name curr_name, b1.name uname, b1.fname ufname, b1.id uid FROM ".TABLE_CAT_ITEMS." i1
		 JOIN ".TABLE_CAT_CURRENCY_LANGS." c1 ON c1.item_id = i1.currency_id
		 JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = i1.author_id
		  WHERE i1.id = ".$pid;

		return $this->db->query($query);
	}

}