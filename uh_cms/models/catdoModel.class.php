<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CatdoModel extends PageModelAuth
{
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db, $LangId, "index");		
	}
	
	public function pageLib()
	{
		return $this->page;
	}
	
	public function restorePass($reqdata)
	{
		return true;
	}

	public function searchLocationDB($name, $loc = 'reg') {
		switch ($loc) {
		 	case 'reg': $query = "SELECT * FROM ".TABLE_REGION_LANG." WHERE name LIKE '".addslashes($name.'%')."'"; break;
			case 'country': $query = "SELECT * FROM ".TABLE_COUNTRY_LANG." WHERE name LIKE '".addslashes($name.'%')."'"; break;
			default: return '';
		}
		return $this->db->query($query);
	}
}