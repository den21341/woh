<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class RatingModel extends PageModelAuth
{
	private $langs;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db, $LangId, "proj");

		$this->langs = UhCmsApp::getLangs();
	}
	
	public function pageLib()
	{
		return $this->page;
	}
	
	public function getUserIdByHash($uhash)
	{
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE guid_uhash='".addslashes($uhash)."'";
		$res = $this->db->query($query);
		if( count($res)>0 )
			return $res[0]['id'];
		
		return 0;
	}	
	
	public function getRateTableData()
	{
		$query = "SELECT i1.*, i2.title2, i2.descr0 as descr,  
				u1.isvip, u1.orgname, u1.name, u1.fname, u1.account_type, u1.pic, u1.pic_sm, u1.sc_share_rate  
			FROM ".TABLE_CAT_ITEMS." i1 
			INNER JOIN ".TABLE_CAT_ITEMS_LANGS." i2 ON i1.id=i2.item_id AND i2.lang_id=".$this->LangId." 
			INNER JOIN ".TABLE_SHOP_BUYERS." u1 ON i1.author_id=u1.id
			WHERE i1.archive=0 AND i1.profile_id='".PROJ_NEEDHELP."' AND i1.start_date<NOW() AND i1.end_date>NOW() 
			ORDER BY i1.item_priority_rate DESC
			LIMIT 0,15";
		$res = $this->db->query($query);
		
		return $res;
	}

	public function getPageData($id) {
		$query = "SELECT * FROM ".TABLE_PAGES_DES_LANGS." WHERE page_des_id = ".$id;
		return $this->db->query($query);
	}
	
	/*
	public function addProject($author_id, $reqd)
	{		
		$newpid = 0;
		
		$stdt_parts = explode(".", $reqd->stdt);
		$stdt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $stdt_parts[2], $stdt_parts[1], $stdt_parts[0], 1, 0, 0);
		
		$endt_parts = explode(".", $reqd->endt);
		$endt_sql = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $endt_parts[2], $endt_parts[1], $endt_parts[0], 1, 0, 0);
						
		//$aval = ($reqd->ptype == PROJ_TYPE_HUMAN ? $reqd->pamount : 0 );
		//$acost = ($reqd->ptype == PROJ_TYPE_MONEY ? $reqd->pamount : 0 );
		
		$currency_id = ( $reqd->ptype == PROJ_TYPE_MONEY ? $reqd->pamountizm : 0 );
		
		$prof_id = PROJ_NEEDHELP;
		if( $reqd->eventtype == 1 )		$prof_id = PROJ_EVENT;	
		if( $reqd->thingtype == 1 )		$prof_id = PROJ_THINGS;
		
		$query = "INSERT INTO ".TABLE_CAT_ITEMS." (author_id, profile_id, obl_id, city_id, add_date, modify_date, start_date, end_date, 
				amount_type, amount, currency_id, title, url) 
				VALUES ('$author_id', '".$prof_id."', '".$reqd->robl."', '".$reqd->rcity."', NOW(), NOW(), '".$stdt_sql."', '".$endt_sql."', 
				'".$reqd->ptype."', '".$reqd->pamount."', '".$currency_id."', '".addslashes($reqd->tit)."', '')";
		if( $this->db->exec($query) )
		{
			$newpid = $this->db->insert_id();
			for($i=0; $i<count($this->langs); $i++)
			{
				$query = "INSERT INTO ".TABLE_CAT_ITEMS_LANGS." (item_id, lang_id, title2, descr0, descr, page_title, page_keywords, page_descr) VALUES 
				('$newpid', '".$this->langs[$i]['id']."', '".addslashes($reqd->tit)."', '".addslashes($reqd->descr)."', '".addslashes($reqd->descrfull)."', '', '', '')";
				if( !$this->db->exec($query) )
				{
					return 0;
				}
			}
			
			$query = "INSERT INTO ".TABLE_CAT_CATITEMS." (item_id, sect_id) VALUES ('$newpid', '".$reqd->sect."')";
			$this->db->exec($query);
		}
		//else 
		//	return $newpid;
		
		return $newpid;
	}
	*/
		
}
?>