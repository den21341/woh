<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class InfoModel extends PageModelAuth {

	protected $langId;

	function __construct($config, $db, $LangId, $pagename="index") {
		parent::__construct($config, $db, $LangId, $pagename);

		$this->langId = $LangId;
	}
	
	public function pageLib() {
		return $this->page;
	}
	
	public function getInfoPage($pagename) {
		$this->page = new SitePage($this->db, $this->LangId, $pagename);
	}

	public function getPageData($id) {
		$query = "SELECT * FROM ".TABLE_PAGES_DES_LANGS." WHERE page_des_id = ".addslashes($id)." AND lang_id = ".addslashes($this->langId)." ORDER BY page_block ASC";
		return $this->db->query($query);
	}

	public function setUserMsg(array $req) {
		$query = "INSERT INTO " . TABLE_HELP_MSG . "(name, email, phone, message, date) 
							VALUES (" . "'{$req['name']}'" . ', ' . "'{$req['email']}'" . ', ' . "'{$req['phone']}'" . ', ' . "'{$req['msg']}'" .', '." NOW() ) ";
		$this->db->exec($query);
	}
}