<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class LoginModel extends PageModelAuth
{
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db, $LangId, "index");		
	}
	
	public function pageLib()
	{
		return $this->page;
	}
	
	public function restorePass($reqdata)
	{
		return true;
	}
	
		
	public function sendRestorePassEmail($tomail, $actguid)
	{
		if( $tomail == "" )
			return;
	
		$subject = "Восстановление пароля на сайте ".WWWHOST;
		$txt = "Вы отправили заявку для восстановления пароля.
				
Для того чтобы сменить пароль для Вашей учетной записи, Вам необходимо пройти по ссылке ниже:
".UhCmsUtils::Page_BuildUrl("login", "setpass", "guid=".$actguid)."
		
Служба поддержки ".$this->cfg['NAME_RU']."
";
		
		UhCmsApp::getMailerInstance()->sendMail($tomail, $subject, $txt);		
	}
	
	public function setRestorePassGuid($uid)
	{
		$guid = UhCmsUtils::makeUuid();
		
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET guid_make='".addslashes($guid)."' WHERE id='$uid'";
		$this->db->exec($query);
		
		return $guid;
	}
	
	public function setNewPass($guid, $pass)
	{
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET passwd=PASSWORD('".addslashes($pass)."'), guid_make='' WHERE guid_make='".addslashes($guid)."'";
		$this->db->exec($query);
	
		return $guid;
	}
	
	public function findUserByMakeGuid($guid)
	{
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE guid_make='".addslashes($guid)."'";
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			return $res[0];
		}
	
		return false;
	}
	
	public function findUserByLogin($login)
	{
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE login='".addslashes($login)."'";
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			return $res[0];
		}
		
		return false;
	}

	public function findUserByFbId($fb_id) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE fb_uid='".addslashes($fb_id)."'";
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			return $res[0];
		}
		
		return false;
	}
	
	public function findUserByVkId($vk_id) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE vk_uid='".addslashes($vk_id)."'";
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			return $res[0];
		}
	
		return false;
	}
	
	public function findUserByOkId($ok_id) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE od_uid='".addslashes($ok_id)."'";
		$res = $this->db->query($query);
		if( count($res)>0 )
		{
			return $res[0];
		}
	
		return false;
	}

	public function smplAddPersonFB($uInf, $redir = '') {
		$reg['rname'] = $uInf['first_name'];
		$reg['rfname'] = $uInf['last_name'];
		$reg['rlogin'] = '';
		$reg['pic'] = '';

		//print_r($uInf['pic']['data']['url']); die();

		if (isset($uInf['email']) && $uInf['email'] != "")
			$reg['rlogin'] = $uInf['email'];

		if (isset($uInf['pic']) && $uInf['pic'] != "") {
			$reg['pic'] = $uInf['pic'];
		}

		// IF FB DATA IS OK ADD PERSON TO DB, ELSE GO TO MAIN REGISTRATION

		if ($reg['rname'] && $reg['rfname'] && $reg['rlogin']) {

			$uhash = UhCmsUtils::makeUuid();
			$actuid = UhCmsUtils::makeUuid();
			$deactuid = UhCmsUtils::makeUuid();

			$passwd = UhCmsUtils::makeUpass();

			$query = "INSERT INTO " . TABLE_SHOP_BUYERS . " (account_type, isactive_web, login, passwd, name, fname, email, obl_id, city_id, guid_uhash, guid_act, guid_deact, 
				add_date, fb_uid, profession, comments)
			VALUES (" . USR_TYPE_PERS . ", 1, '" . addslashes($reg['rlogin']) . "', PASSWORD('" . addslashes($passwd) . "'), '" . addslashes($reg['rname']) . "', '" . addslashes($reg['rfname']) . "',
			'" . addslashes($reg['rlogin']) . "', '', '', 
			'" . addslashes($uhash) . "', '" . addslashes($actuid) . "', '" . addslashes($deactuid) . "', NOW(), '" . addslashes($uInf['id']) . "', 
			'', '')";

			if (!$this->db->exec($query)) {
				return false;
			}

			$uid = $this->db->insert_id();

			$query = "INSERT INTO ".TABLE_SHOP_BUYERS_BOX."(buyer_id) VALUES('".$uid."')";
			$this->db->exec($query);

			$fb_access_token = UhCmsApp::getSesInstance()->GetSessionToken();

			UhCmsApp::getSesInstance()->AuthMakeSocial($uid, $reg['rlogin'], $passwd, $fb_access_token, '', true);

			// Add avatar

			if ($reg['pic'] != "") {
				$newfname0 = $uid . "_" . rand(0, 1000);

				$newfname = $newfname0 . ".jpg";
				$newfname_sm = $newfname0 . "_sm.jpg";

				if (copy($reg['pic'], AVATAR_DIR . $newfname)) {
					$query = "UPDATE " . TABLE_SHOP_BUYERS . " SET pic='" . addslashes(AVATAR_DIR . $newfname) . "' WHERE id='" . $uid . "'";
					$this->db->exec($query);

					// Make small ico
					if (UhCmsUtils::ResizeImage(AVATAR_DIR . $newfname, AVATAR_DIR . $newfname_sm, ".jpg", AVATAR_SM_W, AVATAR_SM_H)) {
						$query = "UPDATE " . TABLE_SHOP_BUYERS . " SET pic_sm='" . addslashes(AVATAR_DIR . $newfname_sm) . "' WHERE id='" . $uid . "'";
						$this->db->exec($query);
					}
				}
			}
			return true;
		} else
			return false;
	}

	public function checkLogin($email) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE login='".addslashes($email)."'";
		$res = $this->db->query($query);
		if( count($res) > 0 )
			return true;

		return false;
	}

	public function smplAddPersonVK($uInf) {

		$reg['rname'] = $uInf['first_name'];
		$reg['rfname'] = $uInf['last_name'];
		$reg['rlogin'] = '';
		$reg['pic'] = '';

		//print_r($uInf['pic']['data']['url']); die();

		if (isset($uInf['email']) && $uInf['email'] != "")
			$reg['rlogin'] = $uInf['email'];

		if (isset($uInf['photo_200']) && $uInf['photo_200'] != "") {
			$reg['pic'] = $uInf['photo_200'];
		}

		// IF VK DATA IS OK, ADD PERSON TO DB, ELSE GO TO MAIN REGISTRATION

		if ($reg['rname'] && $reg['rfname'] && $reg['rlogin']) {

			$uhash = UhCmsUtils::makeUuid();
			$actuid = UhCmsUtils::makeUuid();
			$deactuid = UhCmsUtils::makeUuid();

			$passwd = UhCmsUtils::makeUpass();

			$query = "INSERT INTO " . TABLE_SHOP_BUYERS . " (account_type, isactive_web, login, passwd, name, fname, email, obl_id, city_id, guid_uhash, guid_act, guid_deact, 
				add_date, vk_uid, profession, comments)
			VALUES (" . USR_TYPE_PERS . ", 1, '" . addslashes($reg['rlogin']) . "', PASSWORD('" . addslashes($passwd) . "'), '" . addslashes($reg['rname']) . "', '" . addslashes($reg['rfname']) . "',
			'" . addslashes($reg['rlogin']) . "', '', '', 
			'" . addslashes($uhash) . "', '" . addslashes($actuid) . "', '" . addslashes($deactuid) . "', NOW(), '" . addslashes($uInf['uid']) . "', 
			'', '')";

			if (!$this->db->exec($query)) {
				return false;
			}

			$uid = $this->db->insert_id();

			$query = "INSERT INTO ".TABLE_SHOP_BUYERS_BOX."(buyer_id) VALUES('".$uid."')";
			$this->db->exec($query);

			$fb_access_token = UhCmsApp::getSesInstance()->GetSessionToken();

			UhCmsApp::getSesInstance()->AuthMakeSocial($uid, $reg['rlogin'], $passwd, $fb_access_token, '', true);

			// Add avatar

			if ($reg['pic'] != "") {
				$newfname0 = $uid . "_" . rand(0, 1000);

				$newfname = $newfname0 . ".jpg";
				$newfname_sm = $newfname0 . "_sm.jpg";

				if (copy($reg['pic'], AVATAR_DIR . $newfname)) {
					$query = "UPDATE " . TABLE_SHOP_BUYERS . " SET pic='" . addslashes(AVATAR_DIR . $newfname) . "' WHERE id='" . $uid . "'";
					$this->db->exec($query);

					// Make small ico
					if (UhCmsUtils::ResizeImage(AVATAR_DIR . $newfname, AVATAR_DIR . $newfname_sm, ".jpg", AVATAR_SM_W, AVATAR_SM_H)) {
						$query = "UPDATE " . TABLE_SHOP_BUYERS . " SET pic_sm='" . addslashes(AVATAR_DIR . $newfname_sm) . "' WHERE id='" . $uid . "'";
						$this->db->exec($query);
					}
				}
			}
			return true;
		} else
			return false;
	}

	public function smplAddPersonOK($uInf, $tokenInfo) {
		$reg['rname'] = $uInf['first_name'];
		$reg['rfname'] = $uInf['last_name'];
		$reg['rlogin'] = $uInf['uid'];
		$reg['pic'] = '';

		//print_r($uInf['pic']['data']['url']); die();

		if (isset($uInf['email']) && $uInf['email'] != "")
			$reg['rlogin'] = $uInf['email'];

		if (isset($uInf['photo_400']) && $uInf['photo_400'] != "") {
			$reg['pic'] = $uInf['photo_400'];
		}

		// IF VK DATA IS OK, ADD PERSON TO DB, ELSE GO TO MAIN REGISTRATION

		if ($reg['rname'] && $reg['rfname']) {

			$uhash = UhCmsUtils::makeUuid();
			$actuid = UhCmsUtils::makeUuid();
			$deactuid = UhCmsUtils::makeUuid();

			$passwd = UhCmsUtils::makeUpass();

			$query = "INSERT INTO " . TABLE_SHOP_BUYERS . " (account_type, isactive_web, login, passwd, name, fname, email, obl_id, city_id, guid_uhash, guid_act, guid_deact, 
				add_date, od_uid, profession, comments)
			VALUES (" . USR_TYPE_PERS . ", 1, '" . addslashes($reg['rlogin']) . "', PASSWORD('" . addslashes($passwd) . "'), '" . addslashes($reg['rname']) . "', '" . addslashes($reg['rfname']) . "',
			'', '', '', 
			'" . addslashes($uhash) . "', '" . addslashes($actuid) . "', '" . addslashes($deactuid) . "', NOW(), '" . addslashes($uInf['uid']) . "', 
			'', '')";

			if (!$this->db->exec($query)) {
				return false;
			}

			$uid = $this->db->insert_id();

			$query = "INSERT INTO ".TABLE_SHOP_BUYERS_BOX."(buyer_id) VALUES('".$uid."')";
			$this->db->exec($query);

			$ok_access_token = UhCmsApp::getSesInstance()->GetSessionToken();

			UhCmsApp::getSesInstance()->AuthMakeSocial($uInf['uid'], $reg['rlogin'], $passwd, $ok_access_token, '', true);

			//UhCmsApp::getSesInstance()->AddSocialSession($tokenInfo, $uInf['uid']);
			return true;
		} else
			return false;

	}


}