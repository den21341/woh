<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define('DEF_WEBACTIVE', 0);

class RegistrationModel extends PageModelAuth
{
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db, $LangId, "index");		
	}
	
	public function pageLib()
	{
		return $this->page;
	}
	
	protected function sendRegConfirmMail($tomail, $passwd, $actguid) {
		if( $tomail == "" )
			return;
		
		$subject = "Регистрация на сайте ".WWWHOST;
		$txt = "<br>Вы только что зарегистрировались на сайте ".$this->cfg['NAME_RU'].".
".($passwd != "" ? "<br>Ваши данные для входа:<br>
Логин: ".$tomail."
<br>Пароль: ".$passwd."
" : "")."
<br>Для того, чтобы активировать свою учетную запись и подтвердить корректность введенных данных, Вам необходимо пройти по ссылке ниже:<a href=".UhCmsUtils::Page_BuildUrl("registration", "activate", "guid=".$actguid)."> 
".UhCmsUtils::Page_BuildUrl("registration", "activate", "guid=".$actguid)."<a/>
<br><hr><br>
С уважением, <br>
Служба поддержки ".$this->cfg['NAME_RU']."
";
		
		UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $txt);
	}
	
	public function saveRegDoc($uid, $file, $ind)
	{
		$curfname = $file['name'];
		
		$fext = "";
		$ppos = mb_strrpos($curfname, ".");
		if( $ppos !== FALSE )
			$fext = mb_substr($curfname, $ppos);
		
		$newfname = $uid."_1".$fext;
		for($i=2;$i<50;$i++)
		{			
			if( !@file_exists(CAT_REGDOC_DIR.$newfname) )
				break;
			
			$newfname = $uid."_".$i.$fext;
		}
		
		@move_uploaded_file($file['tmp_name'], CAT_REGDOC_DIR.$newfname);
		
		// Load reg doc to db
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET regdoc".$ind."='".addslashes(CAT_REGDOC_DIR.$newfname)."' WHERE id='$uid'";
		$this->db->exec($query);
	}
	
	public function checkLoginExists($email)
	{
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE login='".addslashes($email)."'";
		$res = $this->db->query($query);
		if( count($res) > 0 )
			return true;
		
		return false;
	} 
	
	public function addPersonFb($fb_user_id, $reqdata, $token)
	{
		$uhash = UhCmsUtils::makeUuid();
		$actuid = UhCmsUtils::makeUuid();
		$deactuid = UhCmsUtils::makeUuid();
		
		$passwd = UhCmsUtils::makeUpass();
		
		$query = "INSERT INTO ".TABLE_SHOP_BUYERS." (account_type, isactive_web, login, passwd, name, fname, email, obl_id, city_id, guid_uhash, guid_act, guid_deact, 
				add_date, fb_uid, profession, comments)
			VALUES (".USR_TYPE_PERS.", 1, '".addslashes($reqdata->rlogin)."', PASSWORD('".addslashes($passwd)."'), '".addslashes($reqdata->rname)."', '".addslashes($reqdata->rfname)."',
			'".addslashes($reqdata->rlogin)."', '".$reqdata->robl."', '".$reqdata->rcity."', 
			'".addslashes($uhash)."', '".addslashes($actuid)."', '".addslashes($deactuid)."', NOW(), '".addslashes($fb_user_id)."', 
			'".addslashes($reqdata->rprof)."', '".addslashes($reqdata->rdescr)."')";			
		//echo $query."<br>";			
		if( !$this->db->exec($query) )
		{
			return false;
		}
		
		$uid = $this->db->insert_id();

		$this->insertToBox($uid);

		// Send confirm email
		//$this->sendRegConfirmMail($reqdata->rlogin, "", $actuid);
		
		UhCmsApp::getSesInstance()->AuthMakeSocial($uid, $reqdata->rlogin, $passwd, $token, '', true);
		
		// Add avatar
		if( ($reqdata->rusepic == 1) && ($reqdata->pic != "") )
		{
			$newfname0 = $uid."_".rand(0,1000);
	
			$newfname = $newfname0.".jpg";
			$newfname_sm = $newfname0."_sm.jpg";
			
			if( copy( $reqdata->pic, AVATAR_DIR.$newfname ) )
			{
				$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic='".addslashes(AVATAR_DIR.$newfname)."' WHERE id='".$uid."'";
				$this->db->exec($query);
				
				// Make small ico
				if( UhCmsUtils::ResizeImage(AVATAR_DIR.$newfname, AVATAR_DIR.$newfname_sm, ".jpg", AVATAR_SM_W, AVATAR_SM_H) )
				{
					$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic_sm='".addslashes(AVATAR_DIR.$newfname_sm)."' WHERE id='".$uid."'";
					$this->db->exec($query);
				}
			}			
		}
		
		return true;
	}
	
	public function addPersonVk($vk_user_id, $reqdata, $token)
	{
		$uhash = UhCmsUtils::makeUuid();
		$actuid = UhCmsUtils::makeUuid();
		$deactuid = UhCmsUtils::makeUuid();
	
		$passwd = UhCmsUtils::makeUpass();
	
		$query = "INSERT INTO ".TABLE_SHOP_BUYERS." (account_type, isactive_web, login, passwd, name, fname, email, obl_id, city_id, guid_uhash, guid_act, guid_deact,
				add_date, vk_uid, profession, comments)
			VALUES (".USR_TYPE_PERS.", 1, '".addslashes($reqdata->rlogin)."', PASSWORD('".addslashes($passwd)."'), '".addslashes($reqdata->rname)."', '".addslashes($reqdata->rfname)."',
			'".addslashes($reqdata->rlogin)."', '".$reqdata->robl."', '".$reqdata->rcity."', 
			'".addslashes($uhash)."', '".addslashes($actuid)."', '".addslashes($deactuid)."', NOW(), '".addslashes($vk_user_id)."',
			'".addslashes($reqdata->rprof)."', '".addslashes($reqdata->rdescr)."')";
		//echo $query."<br>";
		if( !$this->db->exec($query) )
		{
			return false;
		}
		
		// Send confirm email
		//$this->sendRegConfirmMail($reqdata->rlogin, "", $actuid);
	
		$uid = $this->db->insert_id();

		$this->insertToBox($uid);

		UhCmsApp::getSesInstance()->AuthMakeSocial($uid, $reqdata->rlogin, $passwd, $token, $vk_user_id, true);
		
		// Add avatar
		if( ($reqdata->rusepic == 1) && ($reqdata->pic != "") )
		{
			$newfname0 = $uid."_".rand(0,1000);
		
			$newfname = $newfname0.".jpg";
			$newfname_sm = $newfname0."_sm.jpg";
				
			if( copy( $reqdata->pic, AVATAR_DIR.$newfname ) )
			{
				$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic='".addslashes(AVATAR_DIR.$newfname)."' WHERE id='".$uid."'";
				$this->db->exec($query);
		
				// Make small ico
				if( UhCmsUtils::ResizeImage(AVATAR_DIR.$newfname, AVATAR_DIR.$newfname_sm, ".jpg", AVATAR_SM_W, AVATAR_SM_H) )
				{
					$query = "UPDATE ".TABLE_SHOP_BUYERS." SET pic_sm='".addslashes(AVATAR_DIR.$newfname_sm)."' WHERE id='".$uid."'";
					$this->db->exec($query);
				}
			}
		}
	
		return true;
	}
	
	public function addPersonOk($ok_user_id, $reqdata, $token)
	{
		$uhash = UhCmsUtils::makeUuid();
		$actuid = UhCmsUtils::makeUuid();
		$deactuid = UhCmsUtils::makeUuid();
	
		$passwd = UhCmsUtils::makeUpass();
	
		$query = "INSERT INTO ".TABLE_SHOP_BUYERS." (account_type, isactive_web, login, passwd, name, fname, email, obl_id, city_id, guid_uhash, guid_act, guid_deact,
				add_date, od_uid, profession, comments)
			VALUES (".USR_TYPE_PERS.", 1, '".addslashes($reqdata->rlogin)."', PASSWORD('".addslashes($passwd)."'), '".addslashes($reqdata->rname)."', '".addslashes($reqdata->rfname)."',
			'".addslashes($reqdata->rlogin)."', '".$reqdata->robl."', '".$reqdata->rcity."', 
			'".addslashes($uhash)."', '".addslashes($actuid)."', '".addslashes($deactuid)."', NOW(), '".addslashes($ok_user_id)."',
			'".addslashes($reqdata->rprof)."', '".addslashes($reqdata->rdescr)."')";
		//echo $query."<br>";
		if( !$this->db->exec($query) )
		{
			return false;
		}
		
		// Send confirm email
		//$this->sendRegConfirmMail($reqdata->rlogin, "", $actuid);
	
		$uid = $this->db->insert_id();

		$this->insertToBox($uid);
	
		UhCmsApp::getSesInstance()->AuthMakeSocial($uid, $reqdata->rlogin, $passwd, $token, $ok_user_id, true);
	
		return true;
	}
	
	public function addPerson($reqdata) {
		$uhash = UhCmsUtils::makeUuid();
		$actuid = UhCmsUtils::makeUuid();
		$deactuid = UhCmsUtils::makeUuid();

		$query = "INSERT INTO ".TABLE_SHOP_BUYERS." (account_type, isactive_web, login, passwd, name, fname, email, obl_id, city_id,  guid_uhash, guid_act, guid_deact, add_date, profession, comments) 
			VALUES (".USR_TYPE_PERS.", '".DEF_WEBACTIVE."', '".addslashes($reqdata->rlogin)."', PASSWORD('".addslashes($reqdata->rpasswd1)."'), '".addslashes($reqdata->rname)."', '".addslashes($reqdata->rfname)."', 
			'".addslashes($reqdata->rlogin)."', '".$reqdata->robl."', '".$reqdata->rcity."', 
			'".addslashes($uhash)."', '".addslashes($actuid)."', '".addslashes($deactuid)."', NOW(), '".addslashes($reqdata->rprof)."', '".addslashes($reqdata->rdescr)."')";
		//echo $query."<br>";
		if( !$this->db->exec($query) )
		{
			return false;
		}
		
		$newusrid = $this->db->insert_id();
		/* ADD TO TABLE buyer_box */
		$this->insertToBox($newusrid);
		/*************************/

		if(isset($reqdata->uhash) && $reqdata->uhash != '') {
			$ureferer_id = $this->getUserIdByHash($reqdata->uhash);

			if($ureferer_id != 0) {
				$this->addBringFriend($ureferer_id, $newusrid);
			}

		}

		// Send confirm email
		if( preg_match(EMAIL_REGEXP, trim($reqdata->rlogin)) == 0 )
		{
			// Это не email, т.е. телефон
			$query = "UPDATE ".TABLE_SHOP_BUYERS." SET isactive_web=1 WHERE id='".$newusrid."'";
			if( !$this->db->exec($query) )
			{
				//return false;
			}
		}
		else
		{
			$this->sendRegConfirmMail($reqdata->rlogin, $reqdata->rpasswd1, $actuid);
		}
		
		return true;
	}
	
	public function addCompany($reqdata)
	{
		$uhash = UhCmsUtils::makeUuid();
		$actuid = UhCmsUtils::makeUuid();
		$deactuid = UhCmsUtils::makeUuid();
		
		$tel = $this->formatPhone($reqdata->rtel);
	
		$query = "INSERT INTO ".TABLE_SHOP_BUYERS." (account_type, isactive_web, login, passwd, orgname, orgsphere, name, fname, email, phone, obl_id, city_id, 
				employee_num, guid_uhash, guid_act, guid_deact, add_date, comments)
			VALUES (".USR_TYPE_COMP.", '".DEF_WEBACTIVE."', '".addslashes($reqdata->rlogin)."', PASSWORD('".addslashes($reqdata->rpasswd1)."'),
			'".addslashes($reqdata->rorg)."', '".addslashes($reqdata->rorgsphere)."',   
			'".addslashes($reqdata->rname)."', '".addslashes($reqdata->rfname)."', '".addslashes($reqdata->rlogin)."', '".addslashes($tel)."', 
			'".$reqdata->robl."', '".$reqdata->rcity."', 
			'".addslashes($reqdata->remplnum)."', '".addslashes($uhash)."', '".addslashes($actuid)."', '".addslashes($deactuid)."', NOW(), '".addslashes($reqdata->rdescr)."')";			
		//echo $query."<br>";			
		if( !$this->db->exec($query) )
		{
			return false;
		}
		$newcomid = $this->db->insert_id();
		$this->insertToBox($newcomid );
		// Send confirm email
		$this->sendRegConfirmMail($reqdata->rlogin, $reqdata->rpasswd1, $actuid);
	
		return $newcomid;
	}
	
	public function addOrganization($reqdata)
	{
		$uhash = UhCmsUtils::makeUuid();
		$actuid = UhCmsUtils::makeUuid();
		$deactuid = UhCmsUtils::makeUuid();
		
		$tel = $this->formatPhone($reqdata->rtel);
	
		$query = "INSERT INTO ".TABLE_SHOP_BUYERS." (account_type, isactive_web, login, passwd, orgname, orgsphere, name, fname, email, phone, obl_id, city_id, 
				employee_num, guid_uhash, guid_act, guid_deact, add_date, comments)
			VALUES (".USR_TYPE_ORG.", '".DEF_WEBACTIVE."', '".addslashes($reqdata->rlogin)."', PASSWORD('".addslashes($reqdata->rpasswd1)."'),
			'".addslashes($reqdata->rorg)."', '".addslashes($reqdata->rorgsphere)."',   
			'".addslashes($reqdata->rname)."', '".addslashes($reqdata->rfname)."', '".addslashes($reqdata->rlogin)."', '".addslashes($tel)."',
			'".$reqdata->robl."', '".$reqdata->rcity."',  
			'".addslashes($reqdata->remplnum)."', '".addslashes($uhash)."', '".addslashes($actuid)."', '".addslashes($deactuid)."', NOW(), '".addslashes($reqdata->rdescr)."')";			
		//echo $query."<br>";			
		if( !$this->db->exec($query) )
		{
			return false;
		}
		
		$orguid = $this->db->insert_id();
		$this->insertToBox($orguid);

		// Send confirm email
		$this->sendRegConfirmMail($reqdata->rlogin, $reqdata->rpasswd1, $actuid);
	
		return $orguid;
	}	
	
	protected function formatPhone($tel)
	{
		$restel = implode("", explode(" ", $tel));
		$restel = implode("", explode("-", $restel));
		$restel = implode("", explode("(", $restel));
		$restel = implode("", explode(")", $restel));
		$restel = implode("", explode("+", $restel));
		
		return $restel;
	}
	
	public function activateAccount($guid)
	{		
		// V1 old mode, no auto login		
		//$query = "UPDATE ".TABLE_SHOP_BUYERS." SET isactive_web=1, guid_act='' WHERE guid_act='".addslashes($guid)."'";

		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET isactive_web=1 WHERE guid_act='".addslashes($guid)."'";
		return $this->db->exec($query);
	}

	/* ADD COLUMN TO TABLE buyers_box */

	protected function insertToBox($id) {
		$query = "INSERT INTO ".TABLE_SHOP_BUYERS_BOX."(buyer_id) VALUES('".$id."')";
		if( !$this->db->exec($query) )
		{
			return false;
		}
		return true;
	}

	public function getUserIdByHash($uhash) {
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE guid_uhash='".addslashes($uhash)."'";

		$res = $this->db->query($query);

		if( count($res)>0 )
			return $res[0]['id'];
		return 0;
	}
	
	public function addBringFriend($uid, $friend_id) {
		$query = "INSERT INTO ".TABLE_BRING_FRIEND." (from_id, friend_id, active, add_date) 
		 			VALUES(".$uid.", ".$friend_id.", 0, NOW())";
		return $this->db->exec($query);
	}

}