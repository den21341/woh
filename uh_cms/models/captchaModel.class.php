<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CaptchaModel extends Model
{
	protected $LangId;	
	
	protected $coding_table;
	protected $decode_table;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		
		$this->coding_table = Array(
				'X', 'F', 'D', 'E', 'U', 'S', 'P', 'N', 'W', 'T',
				'M', 'Y', 'O', 'A', 'B', 'I', 'V', 'L', 'K', 'R',
				'Z', 'G');
		$this->decode_table = Array(
				'X' => 0, 'F' => 1, 'D' => 2, 'E' => 3, 'U' => 4, 'S' => 5, 'P' => 6, 'N' => 7, 'W' => 8, 'T' => 9,
				'M' => 10, 'Y' =>11, 'O' =>12, 'A' =>13, 'B' =>14, 'I' =>15, 'V' =>16, 'L' =>17, 'K' =>18, 'R' => 19,
				'Z' => 20, 'G' => 21);
	}
	
	public function decode_letter($letter)
	{
		$curnumber = $this->decode_table[$letter];
		$curdigit = $curnumber % 10;
		
		return $curdigit; 
	} 
	
	public function generate_code()
	{
		$newcode = Array();
		$cryptcode = "";
		for( $i=0; $i<7; $i++ )
		{
			$newcode[$i] = rand(0, 21);
			$cryptcode .= $this->coding_table[$newcode[$i]];
		}
		
		return $cryptcode;
	}
}
?>