
var cnvTxtNone = document.createElement('canvas');;
cnvTxtNone.id = "cnvTxtNone";


cnvTxtNone.width = 2;
cnvTxtNone.height = 2;

var txtNone = new THREE.Texture(cnvTxtNone);

/*
var shaderVertexShine = `

attribute vec3 colorRandom;

uniform vec3 colorBase;
uniform	float	alphaBase;
uniform	float	alphaNone;

uniform float pos;

varying vec4 vColor;  // 'varying' vars are passed to the fragment shader


void main() {

	//vec4 t2  =  projectionMatrix * modelViewMatrix * vec4(0.0,0.0,0.0, 1.0 );

	vec4 t  =  projectionMatrix * modelViewMatrix * vec4(0.0, 0.0, 0.0, 1.0 );

	gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );

	if (t.z < gl_Position.z )
		vColor = vec4(1.0, 1.0, 1.0, 0.0);

	else
		if (color.r < 0.5 )
		{
			vColor = vec4(colorBase,  alphaNone);
			if ( gl_Position.y > 10.0)
				gl_Position.y += 5.0;
			else
			if ( gl_Position.y < -10.0)
				gl_Position.y -= 5.0;
else

				gl_Position.y += sin((gl_Position.y )/10.0)*5.0;
	//			gl_Position.y += 100.0;
//			else
//			gl_Position.y += 10.0;
//				gl_Position.y += sin((gl_Position.y+20.0)/20.0)*10.0;
	//		gl_Position.y -= sin((-(gl_Position.y-0.5))*2.0)*20.0;
		}
		else
		{
			vColor = vec4(colorBase,  alphaBase);
	//		gl_Position.y -= 20.0;

		}



}
`;


*/

var shaderVertexShine = '	attribute vec3 colorRandom;	uniform vec3 colorBase;uniform	float	alphaBase;uniform	float	alphaNone;uniform float pos;varying vec4 vColor;void main() {	//vec4 t2  =  projectionMatrix * modelViewMatrix * vec4(0.0,0.0,0.0, 1.0 );	vec4 t  =  projectionMatrix * modelViewMatrix * vec4(0.0, 0.0, 0.0, 1.0 );	gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );	if (t.z < gl_Position.z )		vColor = vec4(1.0, 1.0, 1.0, 0.0);	else		if (color.r < 0.5 )		{			vColor = vec4(colorBase,  alphaNone);			if ( gl_Position.y > 10.0)				gl_Position.y += 5.0;			else			if ( gl_Position.y < -10.0)				gl_Position.y -= 5.0;else				gl_Position.y += sin((gl_Position.y )/10.0)*5.0;		}		else		{			vColor = vec4(colorBase,  alphaBase);			}}';

var shaderFragmentShine = "varying vec4 vColor;\nvarying float alphaColor;\n\nvoid main() {\n//	gl_FragColor = vec4(vColor, alphaColor);  // adjust the alpha\n	gl_FragColor = vColor;  // adjust the alpha\n\n}\n";
var CountryObject = function (dataCntrs, idxC, txt, txtCanvas, txtCtx) {


	function highlightOn(){}
	function highlightOff(){}

	function selectOn(){}
	function selectOff(){}

	var i =0;


	dataCntrs.countries[idxC].iso2 = dataCntrs.countries[idxC].iso2.toLowerCase();


	this.center_ll = {
		lng:dataCntrs.countries[idxC].center[0],
		lat:dataCntrs.countries[idxC].center[1]
	};
	this.txtContur = txt;

	var cntr = dataCntrs.countries[idxC];
	//var vc = THREE.Vec
	for (var a = cntr.areas.length; a--;)
	{
		txtCtx.beginPath();
		txtCtx.moveTo((dataCntrs.verts_crd_ll[cntr.areas[a][0]].lng+180) / 360 * txtCanvas.width,
			(dataCntrs.verts_crd_ll[cntr.areas[a][0]].lat+90) / 180 * txtCanvas.height);
		for (var i = 1, li = cntr.areas[a].length ; i<li; i++){

			txtCtx.lineTo((dataCntrs.verts_crd_ll[cntr.areas[a][i]].lng+180) / 360 * txtCanvas.width,
				(dataCntrs.verts_crd_ll[cntr.areas[a][i]].lat+90) / 180 * txtCanvas.height);
		}
		txtCtx.stroke();

	}


	var fcCnt = dataCntrs.countries[ idxC].face_count/3,
		fcOfst = dataCntrs.countries[ idxC].face_offset;
	var faces = [];
	var uv = [];

	uv.length = faces.length = fcCnt;

	var cnvW = txtCanvas.width;
	var cnvH = txtCanvas.height;

	for(i = 0; i <fcCnt; i++)
	{
		var fc = new THREE.Face3(dataCntrs.faces[fcOfst+i*3], dataCntrs.faces[fcOfst+i*3+1], dataCntrs.faces[fcOfst+i*3+2]);

		//	fc.vertexNormals[0] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3]].clone();
		//		fc.vertexNormals[1] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3+1]].clone();
		//		fc.vertexNormals[2] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3 +2]].clone();

		faces[i] = fc;

		uv[i] = [
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.a].lng + 180) / 360// * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.a].lat +90 )/180// *	cnvH
			),
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.b].lng + 180) / 360// * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.b].lat +90 )/180 //*	cnvH
			),
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.c].lng + 180) / 360 // * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.c].lat +90 )/180 // *	cnvH
			)
		]
	}




	var geomCap = new THREE.Geometry();
	geomCap.vertices = dataCntrs.verts_crd_3d;
	geomCap.faces = faces;
//	geomCap.faceVertexUvs[0] = uv ;

	//geomCap.computeFaceNormals();
	//geomCap.computeVertexNormals();

	var matCap = new THREE.MeshLambertMaterial({
//	var matCap = new THREE.MeshBasicMaterial({
			color: 0xffffff //,
		, opacity: 0,
		transparent: true
			, side:THREE.DoubleSide
//		,	map: this.txtContur

		});

	this.meshCap = new THREE.Mesh(geomCap, matCap);

	this.meshCap.name = "cap "+ dataCntrs.countries[idxC].name.en + " - " + i;

	this.meshCap.objParent = this;


	this.grpBdrs = new THREE.Object3D();
	this.grpShines = new THREE.Object3D();

	var cntrArs = dataCntrs.countries[idxC].areas;


	var clrShineBase = new THREE.Color(0,1,0);

	var uniShine = {
		colorBase: { type: "c", value: clrShineBase },
		alphaBase: { type: "f", value: 0.6 },
		alphaNone: { type: "f", value: 0.0 }	};

	this.materialShine = new THREE.ShaderMaterial({
		//	blending:       THREE.AdditiveBlending,
		//	depthTest:      false,
		//	blending:       THREE.AdditiveBlending,
		vertexColors: THREE.VertexColors,
		//color:  new THREE.Color( 0xffffff ) ,
		transparent:true,
		uniforms: uniShine,
		vertexShader: shaderVertexShine,
		fragmentShader: shaderFragmentShine
		, side:THREE.DoubleSide
		, depthTest: false
		//, blendSrc: THREE.DstAlphaFactor
		, blendSrc: THREE.OneFactor
	});

	var clrShineTop =  new THREE.Color(1,0,0);
	var clrShineLow =  new THREE.Color(0,0,0);

	for (i = cntrArs.length; i--;){
		var geomLine = new THREE.Geometry();
		var geomShine = new THREE.Geometry();

		var ar = cntrArs[i];
		var l = ar.length;

		var  vertsShine = [],  facesShine = [];

		for (var v = 0; v < l; v++){
			if (dataCntrs.verts_crd_3d[ar[v]] === undefined)
				console.log("bad");
			geomLine.vertices.push(dataCntrs.verts_crd_3d[ar[v]].clone().multiplyScalar(1.001));

			vertsShine.push(dataCntrs.verts_crd_3d[ar[v]].clone().multiplyScalar(1.001));
//			vertsShine.push(dataCntrs.verts_crd_3d[ar[v]].clone().multiplyScalar(1.001));
		}

		for (var v = 1; v < l; v++){

			var fc1 = new THREE.Face3(v-1, v, v -1);
			var fc2 = new THREE.Face3(v-1, v, v );

			fc1.vertexColors[0] = clrShineLow;
			fc1.vertexColors[1] = clrShineLow;
			fc1.vertexColors[2] = clrShineTop;

			fc2.vertexColors[0] = clrShineTop;
			fc2.vertexColors[1] = clrShineTop;
			fc2.vertexColors[2] = clrShineLow;

			facesShine.push(fc1);
			facesShine.push(fc2);

		}
		geomShine.vertices = vertsShine;

		geomShine.faces = facesShine;


		var matLine = new THREE.LineBasicMaterial({color: 0xffffff	});

		//geom.name =  dataCntrs.countries[idxC].name.en + " - " + i;
		var ln = new THREE.Line(geomLine, matLine);
		ln.name =  "line "+ dataCntrs.countries[idxC].name.en + " - " + i;
		this.grpBdrs.add(ln);

		this.grpShines.add(new THREE.Mesh( geomShine, this.materialShine  ));

		//this.geometry.faces[i].vertexColors[0] = new THREE.Color(Math.floor(this.geometry.faces[i].a / 3)/ cntSlice, 0,0);

	}












	var img = new Image();
//	objIso2Contry[dataCntrs.countries[idxC].iso2] = {sceneObj: meshCap, img: img};

	img.onload67 = function() {

		var iso2 = img.src.substring(img.src.length - 6, img.src.length - 4);

		var  txtCanvas, txtCntxt;

		if (txtCanvas === undefined)
		{
			txtCanvas = document.createElement('canvas');
			txtCanvas.id = "txtCnv_" +iso2;
			//		document.getElementById("body").appendChild(txtCanvas );
		}

		if (txtCntxt  === undefined)
			txtCntxt = txtCanvas.getContext('2d');

		txtCanvas.width = img.width;
		txtCanvas.height = img.height;

		txtCntxt.clearRect(0,0, img.width, img.height);

		txtCntxt.drawImage(img, 0, 0);


		objIso2Contry[iso2].sceneObj.material.map = new THREE.Texture(txtCanvas);

		objIso2Contry[iso2].sceneObj.material.needsUpdate = true;

		objIso2Contry[iso2].sceneObj.material.map.needsUpdate = true;
		//meshRu.material.needsUpdate = true;

	}
	//img.src = "textures/flags/" + dataCntrs.countries[idxC].iso2 + ".svg";




}


CountryObject.prototype = {
	constructor: CountryObject,

	setTimeFly: function(tm){
		this.timeFly = tm;
	}
}














































var GEOL = GEOL || {};

GEOL.CountryObject = function (dataCntrs, idxC, txt, txtCanvas, txtCtx) {



	var shaderFragmentShine = "varying vec4 vColor;\nvarying float alphaColor;\n\nvoid main() {\n//	gl_FragColor = vec4(vColor, alphaColor);  // adjust the alpha\n	gl_FragColor = vColor;  // adjust the alpha\n\n}\n";








	var shaderVertexShine =
	/*		"\n" +
				"\n" +
				"	uniform vec3 colorBase;\n" +
				"	uniform	float	alphaBase;\n" +
				"	uniform	float	alphaNone;\n" +
				"	uniform	float	heightShine;\n" +
				"	uniform	float	visibleDistance;\n" +
				"\n" +
				"	uniform float pos;\n" +
				"\n" +
				"	varying vec4 vColor;  // 'varying' vars are passed to the fragment shader\n" +
				"\n" +
				"\n" +
				"	void main() {\n" +
				"\n" +
				"		float hgt = -30000000.0;\n" +
				"		//vec4 t2  =  projectionMatrix * modelViewMatrix * vec4(0.0,0.0,0.0, 1.0 );\n" +
				"//\n" +
				"//		vec4 t  =  projectionMatrix * modelViewMatrix * vec4(0.0, 0.0, 0.0, 1.0 );\n" +
				"\n" +
				"		gl_Position = modelViewMatrix * vec4(position, 1.0 );\n" +
				"		if (gl_Position.z < -visibleDistance)\n" +
				"//			if (gl_Position.z > 0.0)\n" +
				"		{\n" +
				"			gl_Position = projectionMatrix *gl_Position ;\n" +
				"			vColor = vec4(1.0, 1.0, 1.0, 0.0);\n" +
				"			return;\n" +
				"		}\n" +
				"		gl_Position = projectionMatrix *gl_Position ;\n" +
				"\n" +
				"//		if (t.z < gl_Position.z )\n" +
				"//			vColor = vec4(1.0, 1.0, 1.0, 0.0);\n" +
				"//		else\n" +
				"		gl_Position.z -= 10.0;\n" +
				"		if (color.r < 0.5 )\n" +
				"		{\n" +
				"			vColor = vec4(colorBase,  alphaNone);\n" +
				"			if ( gl_Position.y > hgt)\n" +
				"				gl_Position.y += heightShine;\n" +
				"			else\n" +
				"			if ( gl_Position.y < -hgt)\n" +
				"				gl_Position.y -= heightShine;\n" +
				"			else\n" +
				"\n" +
				"				gl_Position.y += sin((gl_Position.y )/hgt)*heightShine;\n" +
				"			//			gl_Position.y += 100.0;\n" +
				"//			else\n" +
				"//			gl_Position.y += 10.0;\n" +
				"//				gl_Position.y += sin((gl_Position.y+20.0)/20.0)*10.0;\n" +
				"			//		gl_Position.y -= sin((-(gl_Position.y-0.5))*2.0)*20.0;\n" +
				"		}\n" +
				"		else\n" +
				"		{\n" +
				"			vColor = vec4(colorBase,  alphaBase);\n" +
				"			//		gl_Position.y -= 20.0;\n" +
				"\n" +
				"		}\n" +
				"	}\n";
	*/`


	uniform vec3 colorBase;
	uniform	float	alphaBase;
	uniform	float	alphaNone;
	uniform	float	heightShine;
	uniform	float	visibleDistance;

	uniform float pos;

	varying vec4 vColor;  // 'varying' vars are passed to the fragment shader


	void main() {

		float hgt = 5.0;
		//vec4 t2  =  projectionMatrix * modelViewMatrix * vec4(0.0,0.0,0.0, 1.0 );
//
//		vec4 t  =  projectionMatrix * modelViewMatrix * vec4(0.0, 0.0, 0.0, 1.0 );

		gl_Position = modelViewMatrix * vec4(position, 1.0 );
		if (gl_Position.z < -visibleDistance)
//			if (gl_Position.z > 0.0)
		{
			gl_Position = projectionMatrix *gl_Position ;
			vColor = vec4(1.0, 1.0, 1.0, 0.0);
			return;
		}
		gl_Position = projectionMatrix *gl_Position ;

//		if (t.z < gl_Position.z )
//			vColor = vec4(1.0, 1.0, 1.0, 0.0);
//		else
		gl_Position.z -= 10.0;
		if (color.r < 0.5 )
		{
			vColor = vec4(colorBase,  alphaNone);
			if ( gl_Position.y > hgt)
				gl_Position.y += heightShine;
			else
			if ( gl_Position.y < -hgt)
				gl_Position.y -= heightShine;
			else
				gl_Position.y += sin((gl_Position.y )/hgt)*heightShine;
			//			gl_Position.y += 100.0;
//			else
//			gl_Position.y += 10.0;
//				gl_Position.y += sin((gl_Position.y+20.0)/20.0)*10.0;
			//		gl_Position.y -= sin((-(gl_Position.y-0.5))*2.0)*20.0;
		}
		else
		{
			vColor = vec4(colorBase,  alphaBase);
			//		gl_Position.y -= 20.0;

		}



	}
`


		;

	var me = this;

	me.info = {
		name:dataCntrs.countries[idxC].name,
		iso2: dataCntrs.countries[idxC].iso2,
		iso3: dataCntrs.countries[idxC].iso3
	};


	this.stateHighlight = false;
	this.stateSelect = false;

	this.highlightOn =function (){
		this.grpShines.visible = true;
		this.stateHighlight = true;
		this.updateColor();

	};
	this.highlightOff = function (){
		this.grpShines.visible = false;
		this.stateHighlight = false;
		this.updateColor();

	};

	this.selectOn = function (){
		GEOL.geocamControls.locate_ll(me.center_ll);

		GEOL.piramide.setCenter(me.center_ll );
		this.stateSelect = true;
		this.updateColor();

		GEOL.circleSelect.hide();
		GEOL.circleLight.hide();

		GEOL.askInfoCountry(GEOL.mainuser_id, this.info.iso2);

	};
	this.selectOff = function(){
		GEOL.hideInfo();
		this.stateSelect = false;
		this.updateColor();
	};

	this.updateColor = function(){
		var stt = GEOL.colorsDef.countryColor[this.stateProject];
		var clr;

		if (this.stateHighlight)
		{
			clr = stt.hovered;
		}
		else{
			if (this.stateSelect)
			{
				clr = stt.active;
			}
			else
				clr = stt.normal;
		}

		if (this.meshCap === undefined)
		{
			alert("mesh not defined!");
			return;
		}

		this.meshCap.material.color.set( clr.cap );
		this.meshCap.material.opacity = clr.opacity === undefined?
			0 : clr.opacity;

		for(var i = this.grpShines.children.length; i--;)
		{
			this.grpShines.children[i].material.uniforms.colorBase.value = clr.shine;
			this.grpShines.children[i].material.uniforms.colorBase.needsUpdate = true;

			this.grpShines.children[i].material.uniforms.heightShine.value = clr.shineHeight;
			this.grpShines.children[i].material.uniforms.heightShine.needsUpdate = true;

			this.grpShines.children[i].visible = clr.shineVisible;
		}

		for(var i = this.grpBdrs.children.length; i--;)
		{
			this.grpBdrs.children[i].material.color = clr.line.color;
			this.grpBdrs.children[i].material.opacity = clr.line.alpha;
			this.grpBdrs.children[i].material.needsUpdate = true;
		}

	}

	this.stateProject = "none";
	this.setProjectState  = function (prjState){
		this.stateProject = prjState;
		this.updateColor();
	};


	var i = 0;

	this.center_ll = {
		lng:dataCntrs.countries[idxC].center[0],
		lat:dataCntrs.countries[idxC].center[1]
	};
	this.txtContur = txt;

	var cntr = dataCntrs.countries[idxC];
	//var vc = THREE.Vec
	for (var a = cntr.areas.length; a--;)
	{
		txtCtx.beginPath();
		txtCtx.moveTo((dataCntrs.verts_crd_ll[cntr.areas[a][0]].lng+180) / 360 * txtCanvas.width,
			(dataCntrs.verts_crd_ll[cntr.areas[a][0]].lat+90) / 180 * txtCanvas.height);
		for (var i = 1, li = cntr.areas[a].length ; i<li; i++){

			txtCtx.lineTo((dataCntrs.verts_crd_ll[cntr.areas[a][i]].lng+180) / 360 * txtCanvas.width,
				(dataCntrs.verts_crd_ll[cntr.areas[a][i]].lat+90) / 180 * txtCanvas.height);
		}
		txtCtx.stroke();

	}


	var fcCnt = dataCntrs.countries[ idxC].face_count/3,
		fcOfst = dataCntrs.countries[ idxC].face_offset;
	var faces = [];
	var uv = [];

	uv.length = faces.length = fcCnt;

//	var cnvW = txtCanvas.width;
//	var cnvH = txtCanvas.height;

	for( i = 0; i <fcCnt; i++)
	{
		var fc = new THREE.Face3(dataCntrs.faces[fcOfst+i*3], dataCntrs.faces[fcOfst+i*3+1], dataCntrs.faces[fcOfst+i*3+2]);

		//	fc.vertexNormals[0] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3]].clone();
		//		fc.vertexNormals[1] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3+1]].clone();
		//		fc.vertexNormals[2] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3 +2]].clone();

		faces[i] = fc;

		uv[i] = [
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.a].lng + 180) / 360// * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.a].lat +90 )/180// *	cnvH
			),
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.b].lng + 180) / 360// * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.b].lat +90 )/180 //*	cnvH
			),
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.c].lng + 180) / 360 // * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.c].lat +90 )/180 // *	cnvH
			)
		]
	}




	var geomCap = new THREE.Geometry();
	geomCap.vertices = dataCntrs.verts_crd_3d;
	geomCap.faces = faces;
//	geomCap.faceVertexUvs[0] = uv ;

	//geomCap.computeFaceNormals();
	//geomCap.computeVertexNormals();

	var matCap = new THREE.MeshBasicMaterial({
//	var matCap = new THREE.MeshBasicMaterial({
		color: 0x0000ff //,
		, opacity: 0.5
		, transparent: true
		, side:THREE.DoubleSide
//		,	map: this.txtContur

	});

	this.meshCap = new THREE.Mesh(geomCap, matCap);
	this.meshCap.objParent = this;
	this.meshCap.objType  = "countryCap";
	this.meshCap.name = "countryCap - " + dataCntrs.countries[idxC].name.en;


	this.grpBdrs = new THREE.Object3D();
	this.grpShines = new THREE.Object3D();

	var cntrArs = dataCntrs.countries[idxC].areas;


	var clrShineBase = new THREE.Color(0,1,0);

	this.uniShine = {
		colorBase: { type: "c", value: clrShineBase },
		alphaBase: { type: "f", value: 0.6 },
		alphaNone: { type: "f", value: 0.0 },
		heightShine: { type: "f", value: 10.0 },
		visibleDistance: { type: "f", value: 10.0 }
	};

	this.updateVisibleDistanse = function(){
		this.uniShine.visibleDistance.value = GEOL.visibleDistance;
		this.uniShine.visibleDistance.needsUpdate = true;
	}
	this.materialShine = new THREE.ShaderMaterial({
		//	blending:       THREE.AdditiveBlending,
		//	depthTest:      false,
		//	blending:       THREE.AdditiveBlending,
		vertexColors: THREE.VertexColors,
		//color:  new THREE.Color( 0xffffff ) ,
		transparent:true,
		uniforms: this.uniShine,
		vertexShader: shaderVertexShine,
		fragmentShader: shaderFragmentShine
		, side:THREE.DoubleSide
		, depthWrite: false
		//, blendSrc: THREE.DstAlphaFactor
		, blendSrc: THREE.OneFactor

	});

	var clrShineTop =  new THREE.Color(1,0,0);
	var clrShineLow =  new THREE.Color(0,0,0);

	for ( i = cntrArs.length; i--;){
		var geomLine = new THREE.Geometry();
		var geomShine = new THREE.Geometry();

		var ar = cntrArs[i];
		var l = ar.length;

		var  vertsShine = [],  facesShine = [];

		for (var v = 0; v < l; v++){
			if (dataCntrs.verts_crd_3d[ar[v]] === undefined)
				console.log("bad");
			geomLine.vertices.push(dataCntrs.verts_crd_3d[ar[v]].clone().multiplyScalar(1.001));

			vertsShine.push(dataCntrs.verts_crd_3d[ar[v]].clone().multiplyScalar(1.001))
		}

		for (var v = 1; v < l; v++){

			var fc1 = new THREE.Face3(v-1, v, v -1);
			var fc2 = new THREE.Face3(v-1, v, v );

			fc1.vertexColors[0] = clrShineLow;
			fc1.vertexColors[1] = clrShineLow;
			fc1.vertexColors[2] = clrShineTop;

			fc2.vertexColors[0] = clrShineTop;
			fc2.vertexColors[1] = clrShineTop;
			fc2.vertexColors[2] = clrShineLow;

			facesShine.push(fc1);
			facesShine.push(fc2);

		}
		geomShine.vertices = vertsShine;
		geomShine.faces = facesShine;

		var matLine = new THREE.LineBasicMaterial({
			color: GEOL.colorsDef.countryColor.none.normal.line.color,
			transparent: true,
			opacity:GEOL.colorsDef.countryColor.none.normal.line.alpha

		});

		var ln = new THREE.Line(geomLine, matLine);
		ln.name =  "line "+ dataCntrs.countries[idxC].name.en + " - " + i;
		this.grpBdrs.add(ln);

		var sh = new THREE.Mesh( geomShine, this.materialShine);
		sh.name = "shine "+ dataCntrs.countries[idxC].name.en + " - " + i;

		sh.objType = "shine";
		sh.objParent = this;

		sh.renderOrder = GEOL.renderOrderShine;
		this.grpShines.add(sh);

	}


	this.grpShines.visible = false;




	GEOL.objBoders.add(this.grpBdrs);
	GEOL.scene.add(this.grpShines);

	GEOL.objCountries.add(this.meshCap);


	this.updateColor();

};