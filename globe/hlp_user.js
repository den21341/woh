
var GEOL = GEOL || {};

GEOL.Help_User = function (data){
	this.graphInfo = data;
	this.center_ll = {lat: data.info.lat,lng: data.info.lng};


	var me = this;

	this.canvas = document.createElement("canvas");
	this.canvas.id = "ava_" +data.gid;
	if(GEOL.sets && GEOL.sets.debug)
		document.getElementById("body").appendChild(this.canvas  );

	me.txtSz = 128;
	me.canvas.width = 	me.canvas.height = me.txtSz;

	this.ctx  = me.canvas.getContext('2d');

	this.textureAva = new THREE.Texture(this.canvas );


	this.textureAva.needsUpdate = true;



	this.geom = GEOL.createGeometryGex(6);


	if (GEOL.paramIcons.user_sprite)
	{
		this.mat   = new THREE.SpriteMaterial( {
			map: GEOL.txtIcons.user_help,
			transparent:true,
			fog:true,
			depthWrite:false,
			depthTest:false
			 } );
		this.mesh  = new THREE.Sprite( this.mat );
	}
	else
	{
		this.mat  = new THREE.MeshBasicMaterial({
//		side:THREE.DoubleSide,
			transparent:true,
			map: GEOL.txtIcons.user_help,
			depthWrite:false
		});

		this.mesh = new THREE.Mesh( this.geom , this.mat  );
	}





//	this.mesh.rotateZ(deg2rad(-data.info.lat));

	this.mesh.rotateY(-Math.PI/2 + deg2rad(data.info.lng));

	this.mesh.rotateX(deg2rad(-data.info.lat));

	//var v1 = new THREE.Vector3(0,0,1);
	////rotateAroundWorldAxis(this.mesh, v1, deg2rad(-90));
	//v1.set(0,1, 0);
	//rotateAroundWorldAxis(this.mesh, v1, -Math.PI/2 + deg2rad(data.info.lng));

//	this.mesh.rotateZ();


	this.mesh.translateZ(GEOL.BASE_RAD * GEOL.heightGeo.user_help);
	this.mesh.objType = "usr_help";
	this.mesh.name = "usr_help - " + this.graphInfo.gid;

	this.scale = GEOL.paramIcons.user_help.scale;
	this.mesh.scale.set(this.scale , this.scale , this.scale );

	this.mesh.objParent = this;

	this.mesh.renderOrder = (GEOL.renderOrderUser+=3);








	this.stateSelect = false;
	this.stateHighlight = false;

	this.highlightOn =function (){
		this.stateHighlight = true;
		GEOL.circleLight.showHighlight(this);
	};

	this.highlightOff = function (){
		this.stateHighlight = false;
		GEOL.circleLight.hide();
	};

	this.selectOn = function (){
		GEOL.geocamControls.locate_ll(this.center_ll);
		GEOL.circleSelect.showSelect(this);
		this.stateSelect = true;
		GEOL.piramide.setCenter(this.center_ll);

		GEOL.askInfoUserHelp(this.graphInfo.gid);

	};

	this.selectOff = function(){
		GEOL.hideInfo();

		this.stateSelect = false;

	};

	this.updateSelection = function(){

	};






	var ava = new Image();
	ava.onload = function(){
	//	ava.onerror = function(){


		var szMin = Math.min( this.width,  this.height	);
		var sz = me.txtSz - 28;



		me.ctx.save();
		me.ctx.beginPath();
		me.ctx.arc(me.txtSz/2, me.txtSz/2, sz/2, 0, Math.PI * 2, true);
		me.ctx.closePath();

		me.ctx.clip();
		//Clip the image and position the clipped part on the canvas:
		//	JavaScript syntax: 	context.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);

		//me.ctx.drawImage(this, (this.width - sz)/2, (this.height - sz)/2, sz, sz, 0,0,50,50);
		me.ctx.drawImage(this, (this.width - szMin)/2, (this.height - szMin)/2, szMin, szMin,
			(me.txtSz - sz)/2,(me.txtSz - sz)/2,sz,sz);

		/*me.ctx.beginPath();
		 me.ctx.arc(0, 0, 25, 0, Math.PI * 2, true);
		 me.ctx.clip();
		 me.ctx.closePath();
		 */
		me.ctx.restore();

		me.ctx.beginPath();
		me.ctx.arc(me.txtSz/2, me.txtSz/2, sz/2, 0, Math.PI * 2, true);

		me.ctx.strokeStyle="rgba(23,216,17, 0.95)";

		me.ctx.lineWidth = 6;

		me.ctx.stroke();


		me.textureAva.needsUpdate = true;

	}

	ava.onerror = function(){

		var sz = me.txtSz - 28;

		me.ctx.beginPath();
		me.ctx.arc(me.txtSz/2, me.txtSz/2, sz/2, 0, Math.PI * 2, true);

		me.ctx.strokeStyle="rgba(255,255,255, 0.95)";
		me.ctx.lineWidth = 6;

		me.ctx.fillStyle = "red";
		me.ctx.fill();

		me.ctx.stroke();

		me.textureAva.needsUpdate = true;

	}
	ava.src = data.info.url_ava;

}