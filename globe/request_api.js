/**
 * Created by Morss on 16.09.16.
 */

var mainuser_id;

var REQAJXURL = req_ajx_host+"ajx/globusapi/";
//var REQAJXURL = req_ajx_host+"globe/api.php";

function askMainUserData(_umain_id){
	mainuser_id = _umain_id;
	var postData = {user_id: _umain_id, request: "geo_main"};
	jQuery.ajax({
		method: "POST",
		url: REQAJXURL,
		data: postData,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			applyMainUserData(data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


}

var objHlpUser = {};


function applyMainUserData(dat){

	objGraph.data	= dat.data;

	objGraph.nodes = {};
	t = dat.data;

	var nds = objGraph.data.nodes;
	objGraph.data.source_node["chldrn"] = {};
	objGraph.nodes[objGraph.data.source_node.gid] = objGraph.data.source_node;
	objGraph.nodes[objGraph.data.source_node.gid]["lvl"] = 0;
	for (var k = nds.length; k--;){
		if (objGraph.data.nodes[k].lvl > 0)
		{
			objGraph.nodes[objGraph.data.nodes[k].gid] = objGraph.data.nodes[k];
			objGraph.nodes[objGraph.data.nodes[k].gid]["chldrn"] = {};

			if (objHlpUser[objGraph.data.nodes[k].gid] === undefined){
				objHlpUser[objGraph.data.nodes[k].gid] = new Help_User(objGraph.data.nodes[k]);
				scene.add(objHlpUser[objGraph.data.nodes[k].gid].mesh);
			}
		}
	}


	var edgs = objGraph.data.edges;
	for (var k = edgs.length; k--;){
		if (objGraph.edgesFromTo[edgs[k].from] == undefined)
			objGraph.edgesFromTo[edgs[k].from] = {};
		objGraph.edgesFromTo[edgs[k].from][edgs[k].to] = 1;

		if (objGraph.edgesToFrom[edgs[k].to] == undefined)
			objGraph.edgesToFrom[edgs[k].to] = {};
		objGraph.edgesToFrom[edgs[k].to][edgs[k].from] = 1;
	}


	for (var k = edgs.length; k--;) {
		if (objGraph.nodes[edgs[k].from] !== undefined &&
			objGraph.nodes[edgs[k].to] !== undefined)
		{
			objGraph.nodes[edgs[k].from]["chldrn"][edgs[k].to] =	objGraph.nodes[edgs[k].to];
			if (objGraph.nodes[edgs[k].to]["prnt"] == undefined)
				objGraph.nodes[edgs[k].to]["prnt"] =	objGraph.nodes[edgs[k].from];
		}
	}

	var ll_s = {lat:0, lng:0}, ll_e = {lat:0, lng:0};

	for (var k  in objGraph.nodes){
		for (var ch  in objGraph.nodes[k]["chldrn"]){
			ll_s.lng = objGraph.nodes[k].info.lng +180;
			ll_s.lat = objGraph.nodes[k].info.lat;

			ll_e.lng = objGraph.nodes[ch].info.lng +180;
			ll_e.lat = objGraph.nodes[ch].info.lat;

			objGraph.links[k+"_"+ch] = new Link(ll_s, ll_e,	new THREE.Color(0, 0, 0.5));

		}
	}
	for (var k  in objGraph.links){
		scene.add(objGraph.links[k].mesh);

	}
	var mnusr = new Main_User(dat.data.source_node);
	scene.add( mnusr.mesh );


	GEOL.geocamControls.updateVisibleDistanse();

}

function createTree(mnUserId, netData) {


	var frm = objGraph.edgesFromTo;
	var nd_dat = objGraph.nodes;
	var edges = netData.edges;
	var treeRef;

	if (objGraph.treeItems === undefined)
	{
		objGraph.treeItems = {rootMinus: {id: mnUserId, lvl: 0, twigs:{}},
			rootPlus: {id: mnUserId, lvl: 0, twigs:{}}};
	}

	treeRef = objGraph.treeItems;

	// set level for MY_PROJECTS nodes
	var arPrjId = [];
	var ndT, idT;
	for (var t in nd_dat)
	{

		ndT = nd_dat[t];
		ndT.lvl = -1;
		if (ndT.nd_type == 'usr')
		{
			ndT.tr_us = 0;
			ndT.trd = 1;
		}
	}



	// set levels for project helpers nodes


	var branch = treeRef.rootMinus.twigs;
	for (var twId in branch)
	{
		t = netData.edges.edgesToFrom[twId];
		for (var key in t ){
			if (nd_dat[key].nd_type != 'usr')
				continue;
			if (branch[twId].twigs[key] === undefined
				&& nd_dat[key].lvl == -2)
			{
				branch[twId].twigs[key] = {id: key, lvl: -2, twigs: {}};
				nd_dat[key].tr_us = 1;
			}


			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}


		}
	}

	createBrunchPlus(treeRef.rootPlus, 0, frm, nd_dat);

}

function createLinks(){

}

GEOL.askMainUserData = function(_umain_id){
	mainuser_id = _umain_id;
	var postData = {user_id: _umain_id, request: "geo_main"};
	jQuery.ajax({
		method: "POST",
		url: REQAJXURL,
		data: postData,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			GEOL.applyMainUserData(data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


};

GEOL.graph = {nodes:{}, links:{}, edgesFromTo:{},
	edgesToFrom:{}};

GEOL.applyMainUserData = function (dat){

	GEOL.dataGraph = [];
	GEOL.graph.nodes = [];
	GEOL.dataGraph = dat.data;


	var d = dat.data;

	var nds = d.nodes;

	GEOL.graph.main_user_id = d.source_node.gid;
	GEOL.graph.nodes[d.source_node.gid] = new GEOL.Main_User(dat.data.source_node);
	GEOL.graph.nodes[d.source_node.gid].graphChildren ={};
	GEOL.graph.nodes[d.source_node.gid].graphInfo.lvl = 0

	for (var k = nds.length; k--;){
		if (nds[k].lvl == 1)
		{
			if (GEOL.graph.nodes[nds[k].gid] === undefined){
				GEOL.graph.nodes[nds[k].gid] = new GEOL.Help_User(nds[k]);
				GEOL.graph.nodes[nds[k].gid].graphChildren ={};
			}
		}
	}


	var edgs = d.edges;
	for (var k = edgs.length; k--;){

		if (GEOL.graph.edgesFromTo[edgs[k].from] == undefined)
			GEOL.graph.edgesFromTo[edgs[k].from] = {};
		GEOL.graph.edgesFromTo[edgs[k].from][edgs[k].to] = edgs[k];

		if (GEOL.graph.edgesToFrom[edgs[k].to] == undefined)
			GEOL.graph.edgesToFrom[edgs[k].to] = {};
		GEOL.graph.edgesToFrom[edgs[k].to][edgs[k].from] = 1;
	}


	for (var k = edgs.length; k--;){
		if (GEOL.graph.nodes[edgs[k].from] !== undefined &&
			GEOL.graph.nodes[edgs[k].to] !== undefined)
		{
			GEOL.graph.nodes[edgs[k].from]["graphChildren"][edgs[k].to] =
				GEOL.graph.nodes[edgs[k].to] ;
			if (GEOL.graph.nodes[edgs[k].to]["graphParent"] == undefined)
				GEOL.graph.nodes[edgs[k].to]["graphParent"] =
					GEOL.graph.nodes[edgs[k].from] ;
		}
	}

	var ll_s = {lat:0, lng:0}, ll_e = {lat:0, lng:0};
	
	//for (var k  in GEOL.graph.nodes){
		for (var ch  in GEOL.graph.nodes[GEOL.graph.main_user_id ]["graphChildren"]){
			ll_s.lng = GEOL.graph.nodes[GEOL.graph.main_user_id].graphInfo.info.lng + 180;
			ll_s.lat = GEOL.graph.nodes[GEOL.graph.main_user_id].graphInfo.info.lat;

			ll_e.lng = GEOL.graph.nodes[ch].graphInfo.info.lng + 180;
			ll_e.lat = GEOL.graph.nodes[ch].graphInfo.info.lat;

			GEOL.graph.links[k+"_"+ch] = 8978798;
			var c =	GEOL.colorsDef.projectColor[GEOL.graph.edgesFromTo[GEOL.graph.main_user_id][ch]["p_type"] ];
			c = c || new THREE.Color(0, 0, 0.5);

			var g = new GEOL.Link(ll_s, ll_e, c,


				{from: k, to:ch});
			GEOL.graph.links[k+"_"+ch] = g;

			GEOL.iso2PrjState[GEOL.graph.nodes[ch].graphInfo.info.iso2] = "none";

		}




	//}
	for (var k  in GEOL.graph.links) {
			GEOL.objUserModeLinks.add(GEOL.graph.links[k].mesh);

	}

	for (var k  in GEOL.graph.nodes){
		if (k != GEOL.graph.main_user_id)
		{
			GEOL.objUserModeHelpers.add(GEOL.graph.nodes[k].mesh);
			GEOL.iso2UsrState[GEOL.graph.nodes[k].graphInfo.info.iso2] ="helped";
		}
		else
		{
			GEOL.mainUserMash = GEOL.graph.nodes[k].mesh;
			GEOL.objClickable.add(GEOL.graph.nodes[k].mesh);
		}

	}

	GEOL.updateCountryColor();

	GEOL.bLoadedUser = true;
	GEOL.locateMainUser();
	GEOL.geocamControls.updateStates();
}

GEOL.loadCountries = function (){

	jQuery.ajax({
		method: "POST",
		url: GEOL.REQAJXURL_data,
		//data: postDat,
		dataType: "json",
		success: function(data){
			if (!data)
			{	alert("Returned empty data-object");	return;		}
			GEOL.readCountries(data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})
}



GEOL.Base64 = {
	decode: function(src, type) {
		var raw = atob(src);
		var len = raw.length;
		var buf = new ArrayBuffer(len);
		var dst = new Uint8Array(buf);
		for (var i = 0; i < len; ++i)
			dst[i] = raw.charCodeAt(i);

		return type ? new type(buf) : buf;
	},

	encode: function(src) {
		var src = new Uint8Array(src.buffer, src.byteOffset, src.byteLength);
		var len = src.length;
		var dst = '';
		for (var i = 0; i < len; ++i)
			dst += String.fromCharCode(src[i]);

		return btoa(dst);
	}
};



GEOL.readCountries = function (data){
	this.dataCountries = {};
	this.dataCountries.verts = GEOL.Base64.decode(data.geom.verts, Int16Array);
	this.dataCountries.faces = GEOL.Base64.decode(data.geom.faces, Uint16Array);
	this.dataCountries.coast = GEOL.Base64.decode(data.geom.coast, Uint16Array);

	this.dataCountries.verts_crd_3d = [];
	this.dataCountries.verts_crd_ll =[] ;
	this.dataCountries.verts_crd_3d = [] ;
	this.dataCountries.countries = [];


	var i = 0, ln = 0;

	for ( i = 0, ln = this.dataCountries.verts.length/2; i < ln; ++i)
	{
		var ps = {lng: 180.0 * this.dataCountries.verts[2*i  + 0]/32768.0,
			lat: 90.0 * this.dataCountries.verts[2*i + 1]/32768.0};
		this.dataCountries.verts_crd_ll[i] = ps;
		this.dataCountries.verts_crd_3d[i] = project_ecef_deg(ps.lng,
			ps.lat, GEOL.BASE_RAD);
	}

	for( i = 0, ln =data.countries.length; i < ln; i++){
		var cnt = data.countries[i];

		cnt.borders = GEOL.Base64.decode(cnt.borders, Uint16Array);

		var abdr = cnt.borders;
		var l = abdr.length;

		var cntAr = 0;
		cnt.areas = [new Array()];

		for(var b = 0; b<l; b++ ){
			if (abdr[b] > 60000)
			{
				cnt.areas.push(new Array());
				cntAr++;
			}
			else
			{
				cnt.areas[cntAr].push(abdr[b]);
			}
		}

		this.dataCountries.countries.push(cnt);
	}


	this.cnvTextCountrs = document.createElement('canvas');;
	this.cnvTextCountrs.id = "cnvTextCountrs";
	this.txtCntxt = this.cnvTextCountrs.getContext('2d');

	this.txtCntr = new THREE.Texture(this.cnvTextCountrs);


	for( i = this.dataCountries.countries.length; i--;)
	{
		this.iso2ParentCountries[
			this.dataCountries.countries[i].iso2] =
			new GEOL.CountryObject(this.dataCountries,
			i, this.txtCntr, this.cnvTextCountrs, this.txtCntxt);
/*		me.objCountries.add(this.arGeoCountries[i].meshCap);
		me.objBoders.add(arCntrs[i].grpBdrs);

		scene.add(arCntrs[i].grpShines);
		*/
	}

	GEOL.updateCountryColor();

	GEOL.bLoadedCountry = true;
	GEOL.locateMainUser();
	GEOL.geocamControls.updateStates();

}


GEOL.askUserProjects = function(user_id){

	var postData = {user_id: user_id, request: "geo_projects_list"};
	jQuery.ajax({
		method: "POST",
		url: REQAJXURL,
		data: postData,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			GEOL.readUserProjects(data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


};

GEOL.readUserProjects = function(data){

	var p = data.data.projects;
	for(var i = 0; i< p.length; i++){
		if(GEOL.dataProjects[p[i].gid] == undefined )

		{
			GEOL.dataProjects[p[i].gid] = p[i];
			GEOL.arrProjects[p[i].gid] = new GEOL.Project(p[i]);
			GEOL.objProjectModeProjects.add(GEOL.arrProjects[p[i].gid].mesh);
		}
	}

	for( var k in GEOL.iso2ParentCountries){
		GEOL.iso2PrjState[k] =	"forbidden";
	}


	for( var k in GEOL.arrProjects){
		if( GEOL.iso2ParentCountries[GEOL.arrProjects[k].graphInfo.iso2] != undefined){

			switch(GEOL.arrProjects[k].graphInfo.status){
				case "completed":
					GEOL.iso2PrjState[GEOL.arrProjects[k].graphInfo.iso2] = "covered";
					break;
				case "available":
					GEOL.iso2PrjState[GEOL.arrProjects[k].graphInfo.iso2] = "available";
					break;
				default :
					GEOL.iso2PrjState[GEOL.arrProjects[k].graphInfo.iso2] = "forbidden";

			}
		}
	}

	GEOL.updateCountryColor();
	GEOL.geocamControls.updateStates();
}



GEOL.askInfoProject = function(gid){

	var postData = {gid: gid,
		request: "geo_project_info"};
	jQuery.ajax({
		method: "POST",
		url: REQAJXURL,
		data: postData,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			GEOL.readInfo(data.data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


};


GEOL.readInfoProject = function(data){
	GEOL.showInfo(data.content);
}

GEOL.askInfoUserHelp = function(gid){

	var postData = {main_id: mainuser_id, gid: gid,
		request: "geo_user_info_help"};
	jQuery.ajax({
		method: "POST",
		url: REQAJXURL,
		data: postData,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			GEOL.readInfo(data.data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


};


GEOL.readInfoUserHelp = function(data){
	GEOL.showInfo(data.content);
}

GEOL.askInfoUserMain = function(gid){

	var postData = {gid: gid,
		request: "geo_user_info_main"};
	jQuery.ajax({
		method: "POST",
		url: REQAJXURL,
		data: postData,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			GEOL.readInfo(data.data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


};


GEOL.readInfoUserMain = function(data){
	GEOL.showInfo(data.content);
}


GEOL.askInfoCountry = function(gid, iso2){

	var postData = {gid: gid, iso2: iso2,
		request: "geo_country_info"};
	jQuery.ajax({
		method: "POST",
		url: REQAJXURL,
		data: postData,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			GEOL.readInfo(data.data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


};


GEOL.readInfo  = function(data){
	GEOL.showInfo(data.content);
};

GEOL.readInfoCountry  = function(data){
	GEOL.showInfo(data.content);
};

function runAnim(nd) {
	var ll_s = {lat:0, lng:0}, ll_e = {lat:0, lng:0};
	//for (var k  in nd){
	for (var ch  in nd[GEOL.graph.main_user_id ]["graphChildren"]) {
	 ll_s.lng = nd[GEOL.graph.main_user_id].graphInfo.info.lng + 180;
	 ll_s.lat = nd[GEOL.graph.main_user_id].graphInfo.info.lat;

	 ll_e.lng = nd[ch].graphInfo.info.lng + 180;
	 ll_e.lat = nd[ch].graphInfo.info.lat;

	 GEOL.graph.links[k+"_"+ch] = 8978798;
	 var c =	GEOL.colorsDef.projectColor[GEOL.graph.edgesFromTo[GEOL.graph.main_user_id][ch]["p_type"] ];
	 c = c || new THREE.Color(0, 0, 0.5);

	 var g = new GEOL.Link(ll_s, ll_e, c,


	 {from: k, to:ch});
	 GEOL.graph.links[k+"_"+ch] = g;

	 GEOL.iso2PrjState[nd[ch].graphInfo.info.iso2] = "none";

	 }

	//}

	for (var k  in GEOL.graph.links) {
	 GEOL.objUserModeLinks.add(GEOL.graph.links[k].mesh);
	}
}