/**
 * Created by Morss on 01.10.16.
 */




//getRotationFromMatrix(object.matrix, object.scale) -> (object.matrix) and multiplySelf -> multiply. r71 � PierreDuc Jul 23 '15 at 13:39

// Rotate an object around an arbitrary axis in world space
function rotateAroundWorldAxis(object, axis, radians) {
	var rotWorldMatrix = new THREE.Matrix4();
	rotWorldMatrix.makeRotationAxis(axis.normalize(), radians);
	rotWorldMatrix.multiply(object.matrix);        // pre-multiply
	object.matrix = rotWorldMatrix;
	object.rotation.setFromRotationMatrix(object.matrix, object.scale);
}



var GEOL = GEOL || {};

GEOL.AURA_MAIN = "aura_usr_main";
GEOL.AURA_USER = "aura_usr_help";



GEOL.AuraIconObject = function (p_mode){
	//this.graphInfo = data;
	//this.center_ll = {lat: data.info.lat,lng: data.info.lng};


	var  me = this;

	this.canvas = document.createElement("canvas");
	this.canvas.id = "auraIcon";


	document.getElementById("body").appendChild(this.canvas  );

	me.txtSz = 256;
	me.canvas.width = 	me.canvas.height = me.txtSz;

	this.ctx  = me.canvas.getContext('2d');



	this.texture = new THREE.Texture(this.canvas );


	this.texture.needsUpdate = true;


	this.geom = new THREE.Geometry();


	var vrt = [], fcs = [];
	vrt.push(new THREE.Vector3(0,0,0.2));
	var uv = [];
	this.cntBdr = 6;

	for (var i = 0; i< this.cntBdr; i++){
		vrt.push(new THREE.Vector3(
			Math.cos(i* Math.PI*2/this.cntBdr) ,
			Math.sin(i* Math.PI*2/this.cntBdr) ,
			0
		));
	}

	for (var i = 2; i< (this.cntBdr+1); i++){
		var fc = new THREE.Face3(i-1, i,0);
		fcs.push(fc);

		uv.push([
			new THREE.Vector2(
				vrt[i-1].x/2 +0.5, vrt[i-1].y/2 + 0.5
			),
			new THREE.Vector2(
				vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5

			),
			new THREE.Vector2(	0.5,  0.5	)
		]);

		if (i == this.cntBdr)
		{
			var fc = new THREE.Face3(i, 1,0);
			fcs.push(fc);

			uv.push([
				new THREE.Vector2(
					vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5
				),
				new THREE.Vector2(
					vrt[1].x/2 +0.5, vrt[1].y/2 + 0.5
				),
				new THREE.Vector2(	0.5,  0.5	)
			]);
		}
	}

	this.geom.vertices = vrt;
	this.geom.faces = fcs;
	this.geom.faceVertexUvs[0] = uv;

	this.geom.verticesNeedUpdate =this.geom.uvsNeedUpdate = true;


	if (GEOL.paramIcons.user_main.sprite)
	{
		this.mat   = new THREE.SpriteMaterial( {
			map: this.texture,
			transparent:true,
			depthWrite:false
		} );
		this.mesh  = new THREE.Sprite( this.mat );
	}
	else
	{
		this.mat  = new THREE.MeshBasicMaterial({
			transparent:true,
			map: this.texture,
			depthWrite:false
		});

		this.mesh = new THREE.Mesh( this.geom , this.mat  );
	}

	this.quatOrig = new THREE.Quaternion();

	this.quatOrig.set(this.mesh.quaternion.x,
		this.mesh.quaternion.y,
		this.mesh.quaternion.z,
		this.mesh.quaternion.w
	);



	this.showUnder = function(mesh){

		//return;
		this.mesh.quaternion.set(this.quatOrig.x,
			this.quatOrig.y,
			this.quatOrig.z,
			this.quatOrig.w
		);

		this.mesh.rotateY(-Math.PI/2 + deg2rad(mesh.objParent.center_ll.lng ));
		this.mesh.rotateX(deg2rad(-mesh.objParent.center_ll.lat ));

		this.mesh.visible = true;
		this.mesh.renderOrder = mesh.renderOrder - 1;

		this.mesh.position.set(
			mesh.position.x,
			mesh.position.y,
			mesh.position.z
		);

		var sc = mesh.scale;
		this.mesh.scale.set(sc.x*2, sc.y*2, sc.z*2);


	};

	this.hide = function(){
		this.mesh.visible = false;
	};


		/*

			this.mesh.rotateZ(deg2rad(-data.info.lat));
			this.mesh.rotateY(-Math.PI/2 + deg2rad(data.info.lng));
			this.mesh.rotateX(deg2rad(-data.info.lat));

		*/
	this.mesh.translateZ(40.4);
	this.mesh.objType = "auraHelp";
	this.mesh.name = "auraHelp " ;

	var sc = GEOL.paramIcons.user_help.scale;
	this.mesh.scale.set(sc, sc, sc);

	this.mesh.objParent = this;

	this.mesh.renderOrder = (GEOL.renderOrderUser+=3);


	var ava = new Image();
	ava.onload = function(){

		var szMin = Math.min( this.width,  this.height	);
		var sz = me.txtSz - 28;

		me.ctx.drawImage(this, (this.width - szMin)/2, (this.height - szMin)/2, szMin, szMin,
			(me.txtSz - sz)/2,(me.txtSz - sz)/2,sz,sz);

		me.texture.needsUpdate = true;

	}

	ava.onerror = function(){

		me.ctx.clearRect(0, 0, me.txtSz, me.txtSz);
		me.ctx.shadowBlur=me.txtSz/3;
		var sz = me.txtSz - 60;
		switch (p_mode){
			case GEOL.AURA_MAIN:
				me.ctx.shadowColor= GEOL.colorsDef.auras.user_main;
				break;
			case GEOL.AURA_USER:
				me.ctx.shadowColor= GEOL.colorsDef.auras.user_help;
				break;
			default :
				me.ctx.shadowColor= GEOL.colorsDef.aura.others;

		}

		me.ctx.beginPath();
		me.ctx.arc(me.txtSz/2, me.txtSz/2, sz/4, 0, 2 * Math.PI, false);
		me.ctx.fillStyle = 'rgba(255, 255, 255, 1.0)';
		me.ctx.fill();
		me.ctx.fill();
		me.ctx.fill();
		me.ctx.fill();

		me.ctx.globalCompositeOperation = 'destination-out';
		me.ctx.shadowBlur=0;

		me.ctx.fill();
		me.ctx.globalCompositeOperation = 'source-over';

		me.texture.needsUpdate = true;

	}
	ava.src = GEOL.linkAuraIcon;

	GEOL.scene.add(this.mesh);
}
