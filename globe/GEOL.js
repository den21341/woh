/**
 * Created by Morss on 03.10.16.
 */

var printObj = function( o, maxLevel, level )
{
	if ( typeof level === "undefined" )
	{
		level = 0;
	}
	if ( typeof maxLevel === "undefined" )
	{
		maxLevel = 0;
	}

	var str = '';
	// Remove this if you don't want the pre tag, but make sure to remove
	// the close pre tag on the bottom as well
	if ( level == 0 )
	{
		str = '<pre>';   // can also be <pre>
	}

	var levelStr = '<br>';
	for ( var x = 0; x < level; x++ )
	{
		levelStr += '    ';   // all those spaces only work with <pre>
	}

	if ( maxLevel != 0 && level >= maxLevel )
	{
		str += levelStr + '...<br>';
		return str;
	}

	for ( var p in o )
	{
		switch(typeof o[p])
		{
			case 'string':
			case 'number':    // .tostring() gets automatically applied
			case 'boolean':   // ditto
				str += levelStr + p + ': ' + o[p] + ' <br>';
				break;

			case 'object':    // this is where we become recursive
				if (p == 'path' || p == 'view' || (o[p] && o[p].innerHTML !== undefined))
				{
					break;
				}
			default:
				str += levelStr + p + ': [ <br>' + printObj ( o[p], maxLevel, level + 1 ) + levelStr + ']</br>';
				break;
		}
	}

	// Remove this if you don't want the pre tag, but make sure to remove
	// the open pre tag on the top as well
	if ( level == 0 )
	{
		str += '</pre>';   // also can be </pre>
	}
	return str;
};

var GEOL = GEOL || {};

GEOL.BASE_RAD = 40;
GEOL.REQAJXURL_data = req_ajx_host+"globe/data/map.json";

GEOL.iso2ParentCountries = {};
GEOL.arrProjects = {};
GEOL.iso2PrjState = {};
GEOL.iso2UsrState = {};


GEOL.renderOrderOcean = 10000;

GEOL.renderOrderShine = 100000;
GEOL.renderOrderLink = 200000;
GEOL.renderOrderUser = 300000;

GEOL.renderOrderProject = 400000;
GEOL.renderOrderCircle = 500000;
GEOL.renderOrderPiramide = 600000;

GEOL.camSphereDistanse = 10000;

GEOL.dataProjects = {};

GEOL.bLoadedUser = false;
GEOL.bLoadedCountry = false;

GEOL.timeNow = function(){
	return ( performance || Date ).now();
}

GEOL.locateMainUser = function(){

	GEOL.piramide.updateGeometry();

	if (GEOL.bLoadedUser &&
	GEOL.bLoadedCountry ){

		GEOL.GLRenderer.render(GEOL.scene, GEOL.camera);

		GEOL.geocamControls.locate_ll(
			{

				lat: GEOL.graph.nodes[GEOL.graph.main_user_id ].graphInfo.info.lat,
				lng: GEOL.graph.nodes[GEOL.graph.main_user_id ].graphInfo.info.lng
			});


		//GEOL.auraIcon.showUnder(GEOL.graph.nodes[GEOL.graph.main_user_id ].mesh);



		GEOL.startAnumateLinks();
		GEOL.piramide.setCenter({

			lat: GEOL.graph.nodes[GEOL.graph.main_user_id ].graphInfo.info.lat,
			lng: GEOL.graph.nodes[GEOL.graph.main_user_id ].graphInfo.info.lng
		});
		GEOL.piramide.updateGeometry();

	}

}

GEOL.modeShow = "users";

GEOL.updateCountryColor = function(disableSelect){

	if (disableSelect)
	{
		for (var is in GEOL.iso2ParentCountries)
		{

			GEOL.iso2ParentCountries[is].stateSelect = false;
			GEOL.iso2ParentCountries[is].stateHighlight = false;
			GEOL.iso2ParentCountries[is].updateColor();
		};
	}


	if (GEOL.modeShow == "users")
		for (var is in GEOL.iso2ParentCountries)
		{
			if (GEOL.iso2UsrState[is] )
				GEOL.iso2ParentCountries[is].setProjectState(GEOL.iso2UsrState[is]);
			else
				GEOL.iso2ParentCountries[is].setProjectState("none");
		}
	else
		for (var is in GEOL.iso2PrjState)
		{
			if (GEOL.iso2ParentCountries[is] !== undefined)
			{
				GEOL.iso2ParentCountries[is].setProjectState(GEOL.iso2PrjState[is]);
			}
		}

}

function onSwitchCountry(value, obj) {

	if (window.GEOL){
		window.GEOL.switchCountryMode(value);

	}

}

GEOL.mobileAndTabletcheck = function() {
	var check = false;

	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};


GEOL.init = function(wrap_id, main_u_id, options){

	this.sets = options || {};


	if (this.sets.mobile !== undefined)
	{
		GEOL.isTouchDevice = this.sets.mobile;
	}
	else
		GEOL.isTouchDevice = GEOL.mobileAndTabletcheck();

	this.sets.center_position = this.sets.center_position || {shift_x: -459};

	var me = this;
	me.mainuser_id = main_u_id;
	me.DOM_container = document.getElementById(wrap_id);




	me.DOM_container.innerHTML = '<div id="GL-output"></div><div id="GL-stats">	</div>' +
		'<div id="GL-controls-wrap">' +
			'<div id="GL-btn-usermode">User mode</div>' +
			'<div id="GL-btn-prjmode">Project mode</div>' +
			'<div id="GL-switch-cntrymode">' +
				'<div>Country highlight</div>'+
				'<div class="GL-switch" id="GL-switch-cntry" data-callback="onSwitchCountry"></div>' +
			'</div>' +

		'</div>' +
		'<div id="GL-info-wrap"><div id="GL-info-container"> </div> </div>'
		;

	$('#GL-switch-cntry').easyswitch();
	//console.log(me.objCountries.visible;


	me.switchCountryMode = function(val){
		me.objCountries.visible = !!val;
		GEOL.updateCountryColor(true);
		GEOL.hideInfo ();

	};






	me.containerInfo = document.getElementById("GL-info-container");
	me.wrapInfo = document.getElementById("GL-info-wrap");

	me.GL_container = document.getElementById("GL-output");

	me.GL_reposition = function (){
		var wPrnWrp = me.GL_container.parentElement.clientWidth;
		var wGl = wPrnWrp + 2 * Math.abs(this.sets.center_position.shift_x);

		me.GL_container.style.width = wGl+'px';

		if (me.sets.center_position.shift_x < 0)
			me.GL_container.style.left = 2*this.sets.center_position.shift_x+'px';
		else
			me.GL_container.style.right = '-' + (2*this.sets.center_position.shift_x) +'px';


		if (me.sets.center_position.top !== undefined)
		{

			var hPrnWrp = me.GL_container.parentElement.clientHeight;
			var hGl2 = (hPrnWrp - this.sets.center_position.top)    ;
			var hGl = hGl2 * 2;
			me.GL_container.style.top = '-'+ (hGl2 - this.sets.center_position.top)+'px';
			me.GL_container.style.height = hGl+'px';

		}
		else
		{
			var hPrnWrp = me.GL_container.parentElement.clientHeight;
			me.GL_container.style.height = hPrnWrp +'px';
		}

	};

	me.GL_reposition ();


	var stats = {};
	if (this.sets.show_stats && Stats){
		stats = new Stats("tygydym-");
		stats.setMode(0); // 0: fps, 1: ms

		// Align top-left
		stats.domElement.style.position = 'absolute';
		stats.domElement.style.left = '0px';
		stats.domElement.style.top = '0px';

		document.getElementById("GL-stats").appendChild(stats.domElement);
	}
	else
		stats.update = function(){};

	me.scene = new THREE.Scene();
	me.sceneOver = new THREE.Scene();

//	me.scene.fog=new THREE.Fog( 0xffffff, 0.015, 100 );
//	me.scene.fog.name = "fogScene";
//	me.sceneOver.fog=new THREE.Fog( 0xffffff, 0.015, 100 );
//	me.sceneOver.fog.name = "fogSceneOver";

	this.GLRenderer = new THREE.WebGLRenderer();

	me.GL_container.appendChild(me.GLRenderer.domElement);

	me.camera = new THREE.PerspectiveCamera(45, 1 /*asp ratio*/ , 0.1, 2000);

	// position and point the camera to the center of the scene
	me.camera.position.x = -20;
	me.camera.position.y = 30;
	me.camera.position.z = 40;
	me.camera.lookAt(new THREE.Vector3(00, 0, 0));

	me.geocamControls = new THREE.GeocamControls(me.camera, me.GL_container,
		me.scene);
	me.geocamControls.minDistance = 30;
	me.geocamControls.maxDistance = 500;
	me.geocamControls.autoRotate = false;






	var ambiLight = new THREE.AmbientLight(0x888888);
	me.scene.add(ambiLight);
	var spotLight = new THREE.DirectionalLight(0x555555);
	spotLight.position.set(-20, 30, 40);
	spotLight.intensity = 1.0;
//	me.scene.add(spotLight);

	this.objClickable = new THREE.Object3D();
	me.objClickable.name = "objClickable";


	var textLoader = new THREE.TextureLoader();
	var planetTexture = textLoader.load(req_ajx_host+"globe/textures/earth.jpg");


	me.txtProjects = {
		normal: {},	highlight: {},	select: {}
	};
	function createTextSpot(isProj ){

		//Создаем канвас в ширину и в высоту равный диаметру круга

		var szSqr = 128;
		var txtCanvas = document.createElement('canvas');
		txtCanvas.id = "GL_white_spot";
		if (GEOL.sets.debug === true)
			document.getElementById("body").appendChild(txtCanvas );

		txtCanvas.width = szSqr;
		txtCanvas.height = szSqr;
		//Задаем хар-ки круга и рисуем
		var txtCntxt = txtCanvas.getContext('2d');

		txtCntxt.clearRect(0, 0, szSqr, szSqr);
		//txtCntxt.shadowBlur=szSqr/4;
		//txtCntxt.shadowColor=GEOL.colorsDef.auras.sphere_corona;

		txtCntxt.beginPath();
		txtCntxt.arc(szSqr/2, szSqr/2, szSqr/3, 0, 2 * Math.PI, false);
		if (isProj)
		{
			txtCntxt.fillStyle = 'rgba(23,216,17, 0.95)';
			txtCntxt.strokeStyle='white';
		}
		else
		{
			txtCntxt.fillStyle = 'white';
			txtCntxt.strokeStyle='rgba(23,216,17, 0.95)';

		}

		txtCntxt.lineWidth = 6;
		txtCntxt.fill();
		txtCntxt.stroke();

		var texture = new THREE.Texture(txtCanvas);
		texture.needsUpdate = true;

		return texture;
	}
	var txSpot = createTextSpot();

	me.txtIcons = {
//		user_help:  textLoader.load(GEOL.paramIcons.user_help.url_spot),
//		user_main:  textLoader.load(GEOL.paramIcons.user_main.url_spot)
		user_help:  txSpot,
		user_main:  txSpot

	};

	var txSpotProj = createTextSpot(true);

	for( var k in GEOL.paramIcons.project.url.normal){
		//GEOL.txtProjects.normal[k] = textLoader.load(GEOL.paramIcons.project.url.normal[k]);
		GEOL.txtProjects.normal[k] = txSpotProj ;
	};
	for( var k in GEOL.paramIcons.project.url.highlight){
		GEOL.txtProjects.highlight[k] = textLoader.load(GEOL.paramIcons.project.url.highlight[k]);;
	};
	for( var k in GEOL.paramIcons.project.url.select){
		GEOL.txtProjects.select[k] = textLoader.load(GEOL.paramIcons.project.url.select[k]);;
	};


	var sphereOcean = new THREE.Mesh(new THREE.SphereGeometry(GEOL.BASE_RAD *
		GEOL.heightGeo.sphereOcean, 60, 60),
		new THREE.MeshLambertMaterial({map: planetTexture})
	);

	sphereOcean.rotateOnAxis(new THREE.Vector3(0,1,0), Math.PI);

	sphereOcean.name = "sphereOcean";
	sphereOcean.objType = "sphereOcean";
	me.objClickable.add(sphereOcean);

	me.GeoTmrNow = ( performance || Date ).now();
	me.GeoTmrPrev = ( performance || Date ).now() - 30;


	me.objCountries = new THREE.Object3D();
	me.objCountries.visible = false;
//	me.objUsers = new THREE.Object3D();
//	me.objLinks = new THREE.Object3D();
	me.objBoders = new THREE.Object3D();

	me.objUserModeHelpers = new THREE.Object3D();
	me.objUserModeLinks = new THREE.Object3D();

	me.objProjectModeProjects = new THREE.Object3D();;
	me.objProjectModeProjects.visible = false;
	me.objProjectModeProjects.name = "containerProjects";

	me.scene.add(me.objClickable);
	me.scene.add(me.objBoders);
	me.scene.add(me.objUserModeLinks);

	me.objClickable.add(me.objCountries	);

	me.objClickable.add(me.objUserModeHelpers);
	me.objClickable.add(me.objProjectModeProjects);



	me.createEnvironmentStars();

	me.piramide = new GEOL.Piramide();

	//me.auraIcon = new GEOL.AuraIconObject(GEOL.AURA_MAIN);

	me.circleLight = new GEOL.CircleUser("light");

	me.circleSelect = new GEOL.CircleUser("select");

	function onWindowResize( event ) {

		me.GL_reposition();

		var  height = me.GL_container.clientHeight - 4, width = me.GL_container.clientWidth;

		me.camera.aspect = width / height;
		me.camera.updateProjectionMatrix();
		me.GLRenderer.setSize( width, height );
		me.piramide.updateGeometry();


	}

	onWindowResize( );






	this.loadCountries();

	//this.askMainUserData(me.mainuser_id);


	this.prevVisDist = - 10000000000;
	me.render = function () {

		if (me.prevVisDist != GEOL.visibleDistance)
		{
			me.scene.traverse(function(child){

				if(child.objType)
				{
					switch(child.objType){
						case "shine":
						//case "piramidePick":
							child.objParent.updateVisibleDistanse();
					}
				}
			});
			me.piramide.updateVisible();
		}
		me.prevVisDist = GEOL.visibleDistance;

		GEOL.updateAnumateLinks();

		me.GeoTmrNow = ( performance || Date ).now();

		stats.update();
		me.geocamControls.update(me.GeoTmrNow , me.GeoTmrPrev );

		if (me.objProjectModeProjects.visible )
		{
			var chld = me.objProjectModeProjects.children;
			var cmPos = me.geocamControls.object.position;
			for (var i = chld.length; i--;)
			{
				chld[i].renderOrder = GEOL.renderOrderProject - chld[i].position.distanceTo( cmPos  );
			}
		}



		// render using requestAnimationFrame

		// update the picking ray with the camera and mouse position

	// http://stackoverflow.com/questions/19046972/three-js-cannot-view-a-sprite-through-a-mesh-with-transparency

		me.GLRenderer.autoClear = true;

		me.GLRenderer.render(me.scene, me.camera);
		me.GLRenderer.autoClear = false;

		me.GLRenderer.render(me.sceneOver, me.camera);
		me.GeoTmrPrev = me.GeoTmrNow ;

		requestAnimationFrame(me.render);
	}


	me.render();

	{
		window.addEventListener('resize', onWindowResize, false);

		var raycaster = new THREE.Raycaster();
		var mouse = new THREE.Vector2();
		var mouseDown = new THREE.Vector2();


		function onMouseDown( event ) {
			mouseDown.x =  event.offsetX === undefined ? event.layerX : event.offsetX ;
			mouseDown.y = - event.offsetY === undefined ? event.layerY : event.offsetY ;
		}

		var mouseClickDist = 3;

		this.selectedMeshCountry = undefined;
		this.selectedMeshIcon = undefined;

		this.touchStraight = true;
		function onTouchMove(){
			me.touchStraight = false;
		}

		function onMouseUp( event ) {

			if (!me.touchStraight)
			{
				me.touchStraight = true;
				return;
			}

			//document.getElementById('txtLog').innerHTML = 'hjjjjjjjj';
			//document.getElementById('txtLog').innerHTML = 	printObj(event, 4);
			//console.log(printObj(event, 4));

			mouse.x =  event.offsetX == undefined ? event.layerX : event.offsetX ;
			mouse.y =  event.offsetY == undefined ? event.layerY : event.offsetY ;



			if ((! (mouse.x || mouse.y )) && event.changedTouches)
			{
				var tch;
				if (event.changedTouches[0])
					tch = event.changedTouches[0];
				else if (event.touches[0])
					tch = event.touches[0];
				else return;

				var rect = me.GLRenderer.domElement.getBoundingClientRect();

			 //alert('tch.clientX  ' + tch.clientX  + 'tch.clientY'   + tch.clientY );
				mouse.x = tch.clientX - rect.left;
				mouse.y = tch.clientY - rect.top;
			//	alert('x: ' + mouse.x + ', y: '+mouse.y);

			}
			else
			// calculate mouse position in normalized device coordinates
			// (-1 to +1) for both components
			{

			}

			if (!GEOL.isTouchDevice)
				if (Math.abs(mouse.x - mouseDown.x) > mouseClickDist ||
					Math.abs(mouse.y - mouseDown.y) > mouseClickDist)
					return;

			//alert('x: ' + mouse.x + ', y: '+mouse.y);

			mouse.x = ( mouse.x / me.GL_container.clientWidth) * 2 - 1;
			mouse.y = - ( mouse.y / me.GL_container.clientHeight ) * 2 + 1;

//			alert('x ' + mouse.x + ', y - '+mouse.y);

			raycaster.setFromCamera( mouse, me.camera );

			// calculate objects intersecting the picking ray
			var intersects = raycaster.intersectObjects( me.objClickable.children, true );

			for ( var i = 0; i < intersects.length; i++ ) {

				if (intersects[ i ].object.objParent !== undefined){
					console.log(intersects[ i ].object.name);
				}


		//		console.log(intersects[ i ].object.name);
			}
			if (intersects.length )
			{
				var slct = intersects[ 0 ].object;

				switch(intersects[ 0 ].object.objType)
				{
					case "countryCap":
					{
						if ((me.selectedMeshCountry !== undefined) &&
							me.selectedMeshCountry.id != slct.id){
							me.selectedMeshCountry.objParent.selectOff();
						}
						me.selectedMeshCountry = slct;
						me.selectedMeshCountry.objParent.selectOn();
					}
						break;
					case  "usr_main":
					case  "usr_help":
					case "project":
					{

						if ((me.selectedMeshIcon !== undefined) &&
							me.selectedMeshIcon.id != slct.id){
							me.selectedMeshIcon.objParent.selectOff();
						}
						me.selectedMeshIcon = slct;
						me.selectedMeshIcon.objParent.selectOn();
					}
						break;
					default :
					{
						me.hideInfo();
					}

				}

			}
			else
				me.hideInfo();


		}





		me.pointedMeshCountry = undefined;
		me.pointedMeshIcon = undefined;

		me.hoveredMesh = undefined;

		function onMouseMove( event ) {

//			GEOL.piramide.updateVisible();

			// calculate mouse position in normalized device coordinates
			// (-1 to +1) for both components

			mouse.x =  event.offsetX == undefined ? event.layerX : event.offsetX ;
			mouse.y =  event.offsetY == undefined ? event.layerY : event.offsetY ;

			mouse.x = ( mouse.x / me.GL_container.clientWidth) * 2 - 1;
			mouse.y = - ( mouse.y / me.GL_container.clientHeight ) * 2 + 1;

			raycaster.setFromCamera( mouse, me.camera );

			// calculate objects intersecting the picking ray
			var intersects = raycaster.intersectObjects( me.objClickable.children, true );

			var bFreeCapPointed = true;
			var bFreeUserPointed = true;
			var bPointed  = false;
			for ( var i = 0; i < intersects.length; i++ ) {

				if (intersects[ i ].object.objType == "sprite glow" || bPointed)
					break;

				switch(intersects[ i ].object.objType)
				{
					case  "usr_main":
					case  "usr_help":
					case "project":
					{
						if (GEOL.pointedMeshIcon &&
							(GEOL.pointedMeshIcon.id != intersects[ i ].object.id) )
						{
							GEOL.pointedMeshIcon.objParent.highlightOff();
						}

						GEOL.pointedMeshIcon = intersects[ i ].object;
						GEOL.pointedMeshIcon.objParent.highlightOn();
						bFreeUserPointed  = false;
					//	console.log(intersects[ i ].object.name);
					//	console.log(intersects[ i ].distance);
						bPointed  = true;
					}
						break;

				}
			}

			for ( var i = 0; i < intersects.length; i++ ) {

				if (intersects[ i ].object.objType == "sprite glow" || bPointed)
					break;

				switch(intersects[ i ].object.objType)
				{
					case  "countryCap":
					{
						if (GEOL.pointedMeshCountry &&
							(GEOL.pointedMeshCountry.id != intersects[ i ].object.id) )
						{
							GEOL.pointedMeshCountry.objParent.highlightOff();
						}

						GEOL.pointedMeshCountry = intersects[ i ].object;
						GEOL.pointedMeshCountry.objParent.highlightOn();
						bFreeCapPointed = false;
						bPointed  = true;

					}
						break;
				}
			}

			if (bFreeCapPointed && GEOL.pointedMeshCountry)
			{
				GEOL.pointedMeshCountry.objParent.highlightOff();
			}
			if (bFreeUserPointed && GEOL.pointedMeshIcon)
			{
				GEOL.pointedMeshIcon.objParent.highlightOff();
			}

			if (intersects.length>0){
				//me.geocamControls.locate_ll(intersects[ 0 ].object.objParent.center_ll)
			}

		}

		me.GL_container.addEventListener( 'mousedown', onMouseDown, false );
		me.GL_container.addEventListener( 'mouseup', onMouseUp, false );
		me.GL_container.addEventListener( 'touchend', onMouseUp, false );
		me.GL_container.addEventListener( 'touchmove', onTouchMove, false );
		me.GL_container.addEventListener( 'mousemove', onMouseMove, false );


	}


	this.setUserMode = function (){

		me.objUserModeLinks.visible = true;
		me.objUserModeHelpers.visible = true;
		GEOL.objProjectModeProjects.visible = false;

		GEOL.modeShow = "users";
		GEOL.updateCountryColor(true);
		GEOL.hideInfo ();
	};

	this.setProjectMode = function (){

		GEOL.askUserProjects(me.mainuser_id);

		me.objUserModeLinks.visible = false;
		me.objUserModeHelpers.visible = false;
		GEOL.objProjectModeProjects.visible = true;


		GEOL.modeShow = "projects";
		GEOL.updateCountryColor(true);
		GEOL.hideInfo ();


	};

	var elBtnUser = document.getElementById("GL-btn-usermode");
	var elBtnPrj = document.getElementById("GL-btn-prjmode");

	elBtnUser.onclick = this.setUserMode;
	elBtnPrj.onclick =  this.setProjectMode;


	this.showInfo = function(html){

		GEOL.containerInfo.innerHTML = html;
		GEOL.wrapInfo.style.visibility = 'visible';

		GEOL.piramide.updateGeometry();
		me.piramide.meshPir.visible = true;

	};

	this.hideInfo = function(){
		GEOL.wrapInfo.style.visibility = 'hidden';
		me.piramide.meshPir.visible = false;
		GEOL.circleSelect.hide();
		GEOL.circleLight.hide();

		GEOL.containerInfo.innerHTML = '';
	};

	//GEOL.navigateInit();
	this.askMainUserData(me.mainuser_id);
	onSwitchCountry(1, $('#c'));

}
