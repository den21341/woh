
var cnvTxtNone = document.createElement('canvas');;
cnvTxtNone.id = "cnvTxtNone";


cnvTxtNone.width = 2;
cnvTxtNone.height = 2;

var txtNone = new THREE.Texture(cnvTxtNone);


var shaderVertexShine = `

attribute vec3 colorRandom;

uniform vec3 colorBase;
uniform	float	alphaBase;
uniform	float	alphaNone;

uniform float pos;

varying vec4 vColor;  // 'varying' vars are passed to the fragment shader


void main() {

	//vec4 t2  =  projectionMatrix * modelViewMatrix * vec4(0.0,0.0,0.0, 1.0 );

	vec4 t  =  projectionMatrix * modelViewMatrix * vec4(0.0, 0.0, 0.0, 1.0 );

	gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );

	if (t.z < gl_Position.z )
		vColor = vec4(1.0, 1.0, 1.0, 0.0);

	else
		if (color.r < 0.5 )
		{
			vColor = vec4(colorBase,  alphaNone);
			if ( gl_Position.y > 10.0)
				gl_Position.y += 5.0;
			else
			if ( gl_Position.y < -10.0)
				gl_Position.y -= 5.0;
else

				gl_Position.y += sin((gl_Position.y )/10.0)*5.0;
	//			gl_Position.y += 100.0;
//			else
//			gl_Position.y += 10.0;
//				gl_Position.y += sin((gl_Position.y+20.0)/20.0)*10.0;
	//		gl_Position.y -= sin((-(gl_Position.y-0.5))*2.0)*20.0;
		}
		else
		{
			vColor = vec4(colorBase,  alphaBase);
	//		gl_Position.y -= 20.0;

		}



}
`;

var shaderFragmentShine = `
varying vec4 vColor;
varying float alphaColor;

void main() {
//	gl_FragColor = vec4(vColor, alphaColor);  // adjust the alpha
	gl_FragColor = vColor;  // adjust the alpha

}
`;

GEOL.CountryObject = function (dataCntrs, idxC, txt, txtCanvas, txtCtx) {


	function highlightOn(){}
	function highlightOff(){}

	function selectOn(){}
	function selectOff(){}

	dataCntrs.countries[idxC].iso2 = dataCntrs.countries[idxC].iso2.toLowerCase();


	this.center_ll = {
		lng:dataCntrs.countries[idxC].center[0],
		lat:dataCntrs.countries[idxC].center[1]
	};
	this.txtContur = txt;

	var cntr = dataCntrs.countries[idxC];
	//var vc = THREE.Vec
	for (var a = cntr.areas.length; a--;)
	{
		txtCtx.beginPath();
		txtCtx.moveTo((dataCntrs.verts_crd_ll[cntr.areas[a][0]].lng+180) / 360 * txtCanvas.width,
			(dataCntrs.verts_crd_ll[cntr.areas[a][0]].lat+90) / 180 * txtCanvas.height);
		for (var i = 1, li = cntr.areas[a].length ; i<li; i++){

			txtCtx.lineTo((dataCntrs.verts_crd_ll[cntr.areas[a][i]].lng+180) / 360 * txtCanvas.width,
				(dataCntrs.verts_crd_ll[cntr.areas[a][i]].lat+90) / 180 * txtCanvas.height);
		}
		txtCtx.stroke();

	}


	var fcCnt = dataCntrs.countries[ idxC].face_count/3,
		fcOfst = dataCntrs.countries[ idxC].face_offset;
	var faces = [];
	var uv = [];

	uv.length = faces.length = fcCnt;

	var cnvW = txtCanvas.width;
	var cnvH = txtCanvas.height;

	for(var i = 0; i <fcCnt; i++)
	{
		var fc = new THREE.Face3(dataCntrs.faces[fcOfst+i*3], dataCntrs.faces[fcOfst+i*3+1], dataCntrs.faces[fcOfst+i*3+2]);

		//	fc.vertexNormals[0] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3]].clone();
		//		fc.vertexNormals[1] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3+1]].clone();
		//		fc.vertexNormals[2] = dataCntrs.verts_crd_3d[dataCntrs.faces[fcOfst+i*3 +2]].clone();

		faces[i] = fc;

		uv[i] = [
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.a].lng + 180) / 360// * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.a].lat +90 )/180// *	cnvH
			),
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.b].lng + 180) / 360// * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.b].lat +90 )/180 //*	cnvH
			),
			new THREE.Vector2(
				(dataCntrs.verts_crd_ll[fc.c].lng + 180) / 360 // * cnvW
				,
				1 - (dataCntrs.verts_crd_ll[fc.c].lat +90 )/180 // *	cnvH
			)
		]
	}




	var geomCap = new THREE.Geometry();
	geomCap.vertices = dataCntrs.verts_crd_3d;
	geomCap.faces = faces;
	geomCap.faceVertexUvs[0] = uv ;

	//geomCap.computeFaceNormals();
	//geomCap.computeVertexNormals();

	var matCap = new THREE.MeshLambertMaterial({
//	var matCap = new THREE.MeshBasicMaterial({
			color: 0xffffff //,
			, transparent: true
			, side:THREE.DoubleSide
		,	map: this.txtContur

		});

	this.meshCap = new THREE.Mesh(geomCap, matCap);
	this.meshCap.name = "cap "+ dataCntrs.countries[idxC].name.en + " - " + i;
	this.meshCap.objParent = this;


	this.grpBdrs = new THREE.Object3D();
	this.grpShines = new THREE.Object3D();

	var cntrArs = dataCntrs.countries[idxC].areas;


	var clrShineBase = new THREE.Color(0,1,0);

	var uniShine = {
		colorBase: { type: "c", value: clrShineBase },
		alphaBase: { type: "f", value: 0.6 },
		alphaNone: { type: "f", value: 0.0 }	};

	this.materialShine = new THREE.ShaderMaterial({
		//	blending:       THREE.AdditiveBlending,
		//	depthTest:      false,
		//	blending:       THREE.AdditiveBlending,
		vertexColors: THREE.VertexColors,
		//color:  new THREE.Color( 0xffffff ) ,
		transparent:true,
		uniforms: uniShine,
		vertexShader: shaderVertexShine,
		fragmentShader: shaderFragmentShine
		, side:THREE.DoubleSide
		, depthTest: false
		//, blendSrc: THREE.DstAlphaFactor
		, blendSrc: THREE.OneFactor
	});

	var clrShineTop =  new THREE.Color(1,0,0);
	var clrShineLow =  new THREE.Color(0,0,0);

	for (var i = cntrArs.length; i--;){
		var geomLine = new THREE.Geometry();
		var geomShine = new THREE.Geometry();

		var ar = cntrArs[i];
		var l = ar.length;

		var  vertsShine = [],  facesShine = [];

		for (var v = 0; v < l; v++){
			if (dataCntrs.verts_crd_3d[ar[v]] === undefined)
				console.log("bad");
			geomLine.vertices.push(dataCntrs.verts_crd_3d[ar[v]].clone().multiplyScalar(1.001));

			vertsShine.push(dataCntrs.verts_crd_3d[ar[v]].clone().multiplyScalar(1.001))
//			vertsShine.push(dataCntrs.verts_crd_3d[ar[v]].clone().multiplyScalar(1.001))
		}

		for (var v = 1; v < l; v++){

			var fc1 = new THREE.Face3(v-1, v, v -1);
			var fc2 = new THREE.Face3(v-1, v, v );

			fc1.vertexColors[0] = clrShineLow;
			fc1.vertexColors[1] = clrShineLow;
			fc1.vertexColors[2] = clrShineTop;

			fc2.vertexColors[0] = clrShineTop;
			fc2.vertexColors[1] = clrShineTop;
			fc2.vertexColors[2] = clrShineLow;

			facesShine.push(fc1);
			facesShine.push(fc2);

		}
		geomShine.vertices = vertsShine;

		geomShine.faces = facesShine;


		var matLine = new THREE.LineBasicMaterial({color: 0xffffff	});

		//geom.name =  dataCntrs.countries[idxC].name.en + " - " + i;
		var ln = new THREE.Line(geomLine, matLine);
		ln.name =  "line "+ dataCntrs.countries[idxC].name.en + " - " + i;
		this.grpBdrs.add(ln);

		this.grpShines.add(new THREE.Mesh( geomShine, this.materialShine  ));

		//this.geometry.faces[i].vertexColors[0] = new THREE.Color(Math.floor(this.geometry.faces[i].a / 3)/ cntSlice, 0,0);

	}













}


CountryObject.prototype = {
	constructor: CountryObject,

	setTimeFly: function(tm){
		this.timeFly = tm;
	}
}

