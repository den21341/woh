<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 04.06.15
 * Time: 3:08
 */



$js = file_get_contents("names.json");
$nms = json_decode($js)->names;



$js = file_get_contents("avas.json");
$avs = json_decode($js)->avas;
$ava_main = json_decode($js)->ava_main;


$js = file_get_contents("positions.json");

$js_dec = json_decode($js);


$ll_pos = $js_dec->data;


/*
 * file_put_contents("out.json", json_encode($nms)."\n".count($nms));
*/

$js = file_get_contents("cities.json");
$country_cities = json_decode($js)->countries;

$prjTop = 15;
$ndsTop = 5;
$usrIdTop = 100;


if (isset($_REQUEST["cnt_prj"]) && $_REQUEST["cnt_prj"] > 0)
{
	$prjTop = $_REQUEST["cnt_prj"];
}
if (isset($_REQUEST["cnt_usr"]) && $_REQUEST["cnt_usr"] > 0)
{
	$ndsTop = $_REQUEST["cnt_usr"];
}
if (isset($_REQUEST["max_uid"]) && $_REQUEST["max_uid"] > 0)
{
	$usrIdTop = $_REQUEST["max_uid"];
}

$answ = array();
$arTp = array();

for ($i = 1; $i< 13; $i++)
	$arTp[] = 't'.$i;

$ansData = array();


function createProject($gid, $mrShow){

	global $arTp ;

	// data for vis.js
	$prj["gid"] = $gid;

	// data for graph showing
	$prj["p_type"] =  $arTp[ rand ( 0 , count($arTp) -1 ) ];
	$prj["nd_type"] = "prj";
	$prj["lvl"] = -1;


	// data for user interaction and any other stuff
	$prj["info"] = array();
	$prj["info"]["title"] = "some title - ".$gid;
	$prj["info"]["content"] = "Lorem Ipsum content";
	$prj["info"]["url_view"] = "http://wayofhelp.com/proj/view/". mt_rand ( 100 , 1000 ) . "/";

	return $prj;

}

function createUser($gid, $lvl, $mrShow, $mrLoad, $p_type = null){

	global $arTp ;
	global $nms;

	global $avs;
	global $ll_pos;

	$ta = rand ( 1 , count($avs))-1;



	$acp = array();
	$acp["nd_type"] = "usr";
	if ($p_type != null)
		$acp["p_type"] = $p_type;
	else
		$acp["p_type"] = $arTp[ mt_rand ( 0 , count($arTp) -1 )];
	$acp["gid"] = "u".$gid;

	$acp["lvl"] = $lvl ;

	$acp["size"] = 25;//mt_rand ( 10, 30 );

	$cntr_nmb = rand ( 0 , 3);
	$city_nmb = rand ( 0 , count($ll_pos[$cntr_nmb]->cities) - 1) ;

	$acp["info"] = array("uname" => $nms[mt_rand(0, 59)]->name,
		"child_skip" => (rand ( 1 , 14) > 6) ? rand ( 6 , 24) : -1,

		"title" => "accceptor",
		"ava_pos" => $ta,
		"url_ava" => $avs[$ta],
		"url_view" => "http://wayofhelp.com/users/viewinfo/". mt_rand ( 10 , 100 ) . "/",

		"iso2" =>$ll_pos[$cntr_nmb]->name->iso2,
		"lat" => $ll_pos[$cntr_nmb]->cities[$city_nmb]->lat,
		"lng" => $ll_pos[$cntr_nmb]->cities[$city_nmb]->lng

	);

	$acp["info"]["url_ava"] = (rand ( 0 , 10 ) <1) ? "" : $acp["info"]["url_ava"];

	return $acp;

}

function createEdge($from, $to, $p_type){

	global $arTp ;
	global $nms;
	global $avs;

	$ed = array();
	$ed['from'] = $from ;
	$ed['to'] = $to;

	$utc = mt_rand(1060070279, time());
	$ed["info"] = array(
		"time_date" => date("Y-m-d\TH:i:s", $utc),
		//	"title" => "some info about doing help"
	);
	$ed["p_type"] = $p_type;
	return $ed;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "settings_save")

{

	$sets = array();
	$sets["graph_layout"] = $_REQUEST["graph_layout"];
	$sets["show_avas"] = $_REQUEST["show_avas"];
	$sets["show_labels"] = $_REQUEST["show_labels"];


	$json = json_encode($sets);

	file_put_contents("sets.json",$json);

	$answ = array("status"=>"ok");
	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "node_mainuser_info")

{
	$answ = array("status"=>"ok");

	$ansData ["request"] = "node_mainuser_info";

	$ansData["source_node"] = array();
	$ansData["source_node"]["gid"] = $_REQUEST["gid"];
	$ansData["content"] = "some content about  main user - ".$_REQUEST['gid'];

	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "node_project_info")

{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "node_project_info";

	$ansData["source_node"] = array();
	$ansData["source_node"]["gid"] = $_REQUEST["gid"];
	$ansData["content"] = "some content about project - ".
		$_REQUEST['gid'];

	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "node_user_info")

{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "node_user_info";

	$ansData["source_node"] = array();
	$ansData["source_node"]["gid"] = $_REQUEST["gid"];
	$ansData["content"] = "some content about  user - ".$_REQUEST['gid'];

	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}



if (isset($_REQUEST["request"]) && 	$_REQUEST["request"]  == "tree_main_users"){

		$answ = array("status"=>"ok");

		$ansData ["request"] = $_REQUEST["request"];

		// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
		$ansData ["source_node"] = array();

		// data for vis.js
		$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;

		$ansNodes = array();
		$ansEdges = array();


		// build tree for user's help acceptors

		$l1 = mt_rand ( 1 ,$ndsTop );

		for($i = 0; $i<$l1; $i++){

			$uid_1 = mt_rand ( 1 , $usrIdTop );

			$acp = createUser($uid_1, 1, true, false);
			$ansNodes[] = $acp;

			// создаем связь
			// data for vis.js

			$ansEdges[] = createEdge($_REQUEST["gid"], "u".$uid_1, $acp["p_type"]);
		}

		$ansData["nodes"] = $ansNodes ;
		$ansData["edges"] = $ansEdges;
		$answ["data"] = $ansData;


		$json = json_encode($answ);
		echo $json;
		exit;


}

if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "tree_main_projects" )
{
	$answ["status"] = "ok";



	$ansData ["request"] = $_REQUEST["request"] ;

	// main start node
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;

	$lprj = mt_rand ( 1 , $prjTop );

	$ansNodes = array();
	$ansEdges = array();



	// create projects

	$ecnt = 1;

	for($i = 0; $i<$lprj; $i++){

		$prj = array();

		$up_1 = mt_rand ( 1 , $usrIdTop );

		// data for vis.js
		$prj["gid"] = "p".$up_1;

		// data for graph showing
		$prj["p_type"] =  $arTp[ mt_rand ( 0 , 2 )];
		$prj["nd_type"] = "prj";
		$prj["lvl"] = -1;


		// data for user interaction and any other stuff
		$prj["info"] = array();
		$prj["info"]["title"] = "some title - ".$up_1;
		$prj["info"]["content"] = "Lorem Ipsum content";
		$prj["info"]["url_view"] = "http://wayofhelp.com/proj/view/". mt_rand ( 100 , 1000 ) . "/";

		$ansNodes[] = $prj;

		$ed = array();

		// add connection between MAIN_USER and MY_PROJECT

		// data for vis.js





		$ansEdges[] = createEdge($prj["gid"], $ansData ["source_node"]["gid"], $prj["p_type"]);


		$lhlp = mt_rand ( 1 , $ndsTop );

		for($h = 0; $h<$lhlp; $h++){

			$hlp = createUser(mt_rand ( 1 , $usrIdTop ), -2, false, false  );

			$ansNodes[] = $hlp;

			// data for vis.js

			$ansEdges[] = createEdge($hlp["gid"], $prj["gid"], $prj["p_type"]);
		}


	}


	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}




if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "unfold_usr")

{

	$f = 0;
	if ($f)
	{
		$json = file_get_contents("unfdemo.json");
		echo $json;
		exit;
	}

	$answ = array("status"=>"ok");




	$ansData ["request"] = $_REQUEST["request"];

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;

	$l1 = mt_rand ( 1 ,$ndsTop );
	$ansData ["source_node"]["child_skip"] = $_REQUEST["skip"] + $l1;

	if (mt_rand ( 1 , 10 )<5)
		$ansData ["source_node"]["child_skip"] = -1;


	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors



	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		$acp = createUser($uid_1, $_REQUEST["lvl"] + 1, true, false);
		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge($_REQUEST["gid"], "u".$uid_1, $acp["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "unfold_prj")

{
	$answ = array("status"=>"ok");


	$ansData ["request"] = $_REQUEST["request"];

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;

	$l1 = mt_rand ( 1 ,$ndsTop );
	$ansData ["source_node"]["child_skip"] = $_REQUEST["skip"] + $l1;

	if (mt_rand ( 1 , 10 )<6){
		$ansData ["source_node"]["child_skip"] = -1;
	}


	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors


	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		$acp = createUser($uid_1, -2, true, false);

		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge( "u".$uid_1, $_REQUEST["gid"], $acp["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}

// load full data about user connections



if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "unfold_mn_prj")
{
	$answ = array("status"=>"ok");

	$ansData ["request"] = $_REQUEST["request"];

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;

	$l1 = mt_rand ( 1 ,$ndsTop );
	$ansData ["source_node"]["child_skip_prj"] = $_REQUEST["skip"] + $l1;

	if (mt_rand ( 1 , 10 )<6){
		$ansData ["source_node"]["child_skip_prj"] = -1;
	}




	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors











	$prj = array();

	$up_1 = mt_rand ( 1 , $usrIdTop );

	// data for vis.js
	$prj["gid"] = "p".$up_1;

	// data for graph showing
	$prj["p_type"] =  $arTp[ mt_rand ( 0 , 2 )];
	$prj["nd_type"] = "prj";

	$prj["lvl"] = -1;


	// data for user interaction and any other stuff
	$prj["info"] = array();
	$prj["info"]["child_skip"] = (rand(1,10) <6)? rand(10,30) : -1;
	$prj["info"]["title"] = "some title - ".$up_1;
	$prj["info"]["content"] = "Lorem Ipsum content";
	$prj["info"]["url_view"] = "http://wayofhelp.com/proj/view/". mt_rand ( 100 , 1000 ) . "/";

	$ansNodes[] = $prj;

	$ed = array();

	// add connection between MAIN_USER and MY_PROJECT

	// data for vis.js

	$ed = createEdge($prj["gid"], $ansData ["source_node"]["gid"], $prj["p_type"]);
	$ed['label'] = "получил помощь";

	$ansEdges[] = $ed;





	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		$acp = createUser($uid_1, -2, true, false);
		$acp["info"]["child_skip"] = -1;
		$acp["lvl"] = -2;

		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge( "u".$uid_1, $prj["gid"], $prj["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && 	$_REQUEST["request"]  == "unfold_mn_usr"){

	$answ = array("status"=>"ok");

	$ansData ["request"] = $_REQUEST["request"];

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;


	$l1 = mt_rand ( 1 ,$ndsTop );
	$ansData ["source_node"]["child_skip_usr"] = $_REQUEST["skip"] + $l1;

	if (mt_rand ( 1 , 10 )<3){
		$ansData ["source_node"]["child_skip_usr"] = -1;
	}


	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors

	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		$acp = createUser($uid_1, 1, true, false);
		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge($_REQUEST["gid"], "u".$uid_1, $acp["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;


}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "tree_project_show")

{
	$answ = array("status"=>"ok");




	$ansData ["request"] = "tree_project_show";

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;
	$ansData ["source_node"]["more_show"] = (rand(1,10) <4);





	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors

	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		$acp = createUser($uid_1, 1, true, false);
		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge( "u".$uid_1, $_REQUEST["gid"], $acp["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}

if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "tree_user_show")

{
	$answ = array("status"=>"ok");




	$ansData ["request"] = "tree_user_show";

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;
	$ansData ["source_node"]["more_show"] = (rand(1,10) <4);
	$ansData ["source_node"]["more_load"] = (rand(1,10) <4);





	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors

	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		$acp = createUser($uid_1, 1, true, false);
		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge($_REQUEST["gid"], "u".$uid_1, $acp["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}

// load next level from this user (not full)
if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "tree_user_load")
{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "tree_user_load";

	// инфо про узел графа, т.е. юзера, для которого загружаются подузлы
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] =  $_REQUEST["gid"] ;
	$ansData ["source_node"]["more_show"] = (rand(1,10) <4);
	$ansData ["source_node"]["more_load"] = (rand(1,10) <4);



	$ansNodes = array();
	$ansEdges = array();


	// build tree for user's help acceptors

	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );
		$acp = createUser($uid_1, 99999999, true, false );
		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js

		$ansEdges[] = createEdge($_REQUEST["gid"], "u".$uid_1, $acp["p_type"]);
	}

	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}





if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "geo_projects_list")
{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "geo_projects_list";
	global $country_cities;





	$prjs = array();

	for($i = 0; $i < 20; $i++){
		$prj = array();
		$prj["gid"] = rand(1,100002);
		$prj["p_type"] = "t".rand(1,3);

		$prj["status"] = rand(1,12) > 7 ? "completed":"available";

		$tcntr = rand ( 1 , count($country_cities))-1;

		$tcity  = rand ( 1 , count($country_cities[$tcntr]->cities))-1;

		$prj["iso2"] = $country_cities[$tcntr]->iso2;
		$prj["lng"] = $country_cities[$tcntr]->cities[$tcity][0];
		$prj["lat"] = $country_cities[$tcntr]->cities[$tcity][1];

		$prjs[] = $prj;

	}

	$ansData["projects"] =$prjs;

	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}
















if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "geo_project_info")
{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "geo_project_info";
	global $country_cities;

	$ansData["content"] = "geo_project_info <br>" . "project - ". $_REQUEST["gid"];

	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "geo_user_info_help")
{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "geo_user_info_help";
	global $country_cities;

	$ansData["content"] = "geo_user_info_help <br>" . "help user  - ". $_REQUEST["gid"];

	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}

if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "geo_user_info_main")
{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "geo_user_info_main";
	global $country_cities;

	$ansData["content"] = "geo_user_info_main <br>" . "main user  - ". $_REQUEST["gid"];

	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}


if (isset($_REQUEST["request"]) && $_REQUEST["request"] == "geo_country_info")
{
	$answ = array("status"=>"ok");


	$ansData ["request"] = "geo_country_info";
	global $country_cities;

	$ansData["content"] = "geo_country_info <br>" . "main user  - ". $_REQUEST["gid"].
	"<br>country code - " . $_REQUEST["iso2"];

	$answ["data"] = $ansData;

	$json = json_encode($answ);
	echo $json;
	exit;

}

























if (isset($_REQUEST["request"]) && ($_REQUEST["request"] == "tree_main" ||
		$_REQUEST["request"] == "geo_main"))
{

	$f = 0;
	if ($f)
	{
		$json = file_get_contents("usrdemo.json");
		echo $json;
		exit;
	}

	if (isset($_REQUEST["user_id"]))
		$musrId = $_REQUEST["user_id"];
	else
		$musrId = 1234;
	$answ["status"] = "ok";



	$ansData["settings"] = array();


	if (file_exists ("sets.json"))
	{
		$st = file_get_contents("sets.json");
		$star = json_decode($st);
	}
	else
	{
		$star = array();
		$star["graph_layout"] = "binary_tree";
		$star["show_avas"] = true;
	}


	$star =  (array) $star;


	if (isset($star["graph_layout"]))
	{
		$ansData["settings"]["graph_layout"] = $star["graph_layout"];
	}
	else
		$ansData["settings"]["graph_layout"] = "binary_tree";

	if (isset($star["show_avas"]))
	{
		$ansData["settings"]["show_avas"] = $star["show_avas"];
	}
	else
		$ansData["settings"]["show_avas"] = false;

	if (isset($star["show_labels"]))
	{
		$ansData["settings"]["show_labels"] = $star["show_labels"];
	}
	else
		$ansData["settings"]["show_labels"] = false;

	$ansData["settings"]["debug_config"] = array(
		"cnt_prj" => $prjTop,
		"cnt_usr" =>$ndsTop,
		"max_uid" => $usrIdTop);



	$ansData ["request"] = "tree_main";

	// main start node
	$ansData ["source_node"] = array();

	// data for vis.js
	$ansData ["source_node"]["gid"] = "mnu" . $musrId;
	// data for graph showing


	// data for user interaction and any other stuff

	$ansData ["source_node"]["info"] = array();

	$lprj = mt_rand ( 1 , $prjTop );

	$ansData ["source_node"]["info"]["child_skip_prj"] = rand(10,30);
		//(rand(1,10) <8)? rand(10,30) : -1;
	$ansData ["source_node"]["info"]["child_skip_usr"] = rand(10,30);
		//(rand(1,10) <8)? rand(10,30) : -1;
	$ansData ["source_node"]["info"]["uname"]	= "Админ WayOfHelp";
	$ansData ["source_node"]["info"]["url_ava"] = $ava_main;
















	global $ll_pos;


	$cntr_nmb = rand ( 0 , 3);
	$city_nmb = rand ( 0 , count($ll_pos[$cntr_nmb]->cities) - 1) ;






	$ansData ["source_node"]["info"]["iso2"] = $ll_pos[$cntr_nmb]->name->iso2;

	$ansData ["source_node"]["info"]["lng"] = $ll_pos[$cntr_nmb]->cities[$city_nmb]->lng;
	$ansData ["source_node"]["info"]["lat"] = $ll_pos[$cntr_nmb]->cities[$city_nmb]->lat;


	$ansNodes = array();
	$ansEdges = array();



	// create projects

	$ecnt = 1;


	for($i = 0; $i<$lprj; $i++){

		$prj = array();

		$up_1 = mt_rand ( 1 , $usrIdTop );

		// data for vis.js
		$prj["gid"] = "p".$up_1;

		// data for graph showing
		$prj["p_type"] =  $arTp[ mt_rand ( 0 , 2 )];
		$prj["nd_type"] = "prj";

		$prj["lvl"] = -1;


		// data for user interaction and any other stuff
		$prj["info"] = array();
		$prj["info"]["child_skip"] = (rand(1,10) <6)? rand(10,30) : -1;
//		$prj["info"]["more_show"] = (mt_rand ( 1 , 1000 ) < 500);
		$prj["info"]["title"] = "some title - ".$up_1;
		$prj["info"]["content"] = "Lorem Ipsum content";
		$prj["info"]["url_view"] = "http://wayofhelp.com/proj/view/". mt_rand ( 100 , 1000 ) . "/";

		$ansNodes[] = $prj;

		$ed = array();

		// add connection between MAIN_USER and MY_PROJECT

		// data for vis.js

		$ed = createEdge($prj["gid"], $ansData ["source_node"]["gid"], $prj["p_type"]);


		$ed['label'] = "получил помощь";

		$ansEdges[] = $ed;


		$lhlp = mt_rand ( 1 , $ndsTop );

		for($h = 0; $h<$lhlp; $h++){

			$hlp = createUser(mt_rand ( 1 , $usrIdTop ), -2, false, false  );

			$hlp["info"]["child_skip"] = -1;
			$hlp["lvl"] = -2;

			$ansNodes[] = $hlp;

			// data for vis.js

			$ansEdges[] = createEdge($hlp["gid"], $prj["gid"], $prj["p_type"]);
		}


	}



	// build tree for user's help acceptors

	$l1 = mt_rand ( 1 ,$ndsTop );

	for($i = 0; $i<$l1; $i++){

		$uid_1 = mt_rand ( 1 , $usrIdTop );

		if ($uid_1 == $musrId)
			continue;

		$acp = createUser($uid_1, 1, true, false);

		$ansNodes[] = $acp;

		// создаем связь
		// data for vis.js
		$ed = 		createEdge($ansData ["source_node"]["gid"], "u".$uid_1, $acp["p_type"]);
		$ed['label'] = "оказал помощь";

		$ansEdges[] = $ed;



		$l2 = mt_rand ( 1 ,$ndsTop );

		for($y = 0; $y<$l2; $y++){

			$uid_2 = mt_rand ( 1 , $usrIdTop );
			if ($uid_2 == $musrId)
				continue;
			if ($uid_2 == $uid_1)
				continue;

			$acp = createUser($uid_2, 2, true, false);

			$ansNodes[] = $acp;

			// создаем связь
			// data for vis.js

			$ed = createEdge("u".$uid_1, "u".$uid_2, $acp["p_type"]);;
			$ed['label'] = "оказал помощь";

			$ansEdges[] = $ed;


			$l3= mt_rand ( 1 ,$ndsTop );

			for($n = 0; $n<$l3; $n++){
				$uid_3 = mt_rand ( 1 , $usrIdTop );


				if ($musrId == $uid_3)
					continue;
				if ($uid_1 == $uid_3)
					continue;

				$acp = createUser($uid_3, 3, true, false);

				$ansNodes[] = $acp;

				// создаем связь
				// data for vis.js

				$ansEdges[] = createEdge("u".$uid_2, "u".$uid_3, $acp["p_type"]);;


				$l4= mt_rand ( 1 , $ndsTop );

				for($n = 0; $n<$l4; $n++){
					$uid_4 = mt_rand ( 1 , $usrIdTop );


					if ($musrId == $uid_4)
						continue;
					if ($uid_1 == $uid_4)
						continue;

					$acp = createUser($uid_4, 4, true, false);

					$ansNodes[] = $acp;

					// создаем связь
					// data for vis.js

					$ansEdges[] = createEdge("u".$uid_3, "u".$uid_4, $acp["p_type"]);;

				}

			}
		}




		/*
				for($y = 0; $y<35; $y++){
					$uid_2 = mt_rand ( 1 , 300 );
					if ($uid_2 == $uid_1)
						continue;


					$act_nd["type"] = "help";
					$act_nd["u_from"] = $uid_1;
					$act_nd["u_to"] = $uid_2;

					$act_nd["acttype"] = $arTp[ mt_rand ( 0 , 2 )];
					$act_nd["time_utc"] = mt_rand(1060070279, time());
					$act_nd["time_date"] = date("Y-m-d\TH:i:s", $act_nd["time_utc"]);
					$act_nd["is_more"] = true;
					$act_nd["info"] = array("title"=>"some title string",
						"info_text"=>"some description", "url"=>"http://some_url");

					$acts[] = $act_nd;

					for($n = 0; $n<35; $n++){
						$uid_3 = mt_rand ( 1 , 300 );
						$uid_to = mt_rand ( 1 , 300 );

						if ($uid_2 == $uid_3)
							continue;
						if ($uid_1 == $uid_3)
							continue;
						if ($uid_to == $uid_3)
							continue;

						$act_nd = array();

						$act_nd["type"] = "help";
						$act_nd["u_from"] = $uid_2;
						$act_nd["u_to"] = $uid_3;

						$act_nd["acttype"] = $arTp[ mt_rand ( 0 , 2 )];
						$act_nd["time_utc"] = mt_rand(1060070279, time());
						$act_nd["time_date"] = date("Y-m-d\TH:i:s", $act_nd["time_utc"]);
						$act_nd["is_more"] = true;
						$act_nd["info"] = array("title"=>"some title string",
							"info_text"=>"some description", "url"=>"http://some_url");

						$acts[] = $act_nd;
					}
				}

			*/

	}

	//print_r($answ);
	$ansData["nodes"] = $ansNodes ;
	$ansData["edges"] = $ansEdges;
	$answ["data"] = $ansData;


	$json = json_encode($answ);
	echo $json;
	exit;

}
else
{
	$answ["status"]="some error explanation";

	$json = json_encode($answ);
	echo $json;

	exit;
}
