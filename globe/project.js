


var GEOL = GEOL || {};

GEOL.Project = function (data){
	this.graphInfo = data;

	var  me = this;

	this.center_ll = {lat: data.lat,lng: data.lng};

	this.textureNorm = GEOL.txtProjects.normal[data.p_type];
	this.textureHifhlight = GEOL.txtProjects.highlight[data.p_type];
	this.textureSelect = GEOL.txtProjects.select[data.p_type];


	this.stateSelect = false;
	this.stateHighlight = false;

	this.highlightOn =function (){
		this.stateHighlight = true;
		this.updateTextureState();
	};

	this.highlightOff = function (){
		this.stateHighlight = false;
		this.updateTextureState();
	};

	this.selectOn = function (){
		GEOL.geocamControls.locate_ll(this.center_ll);
		GEOL.piramide.setCenter(this.center_ll);
		this.stateSelect = true;
		this.updateTextureState();

		GEOL.askInfoProject(this.graphInfo.gid);
	};

	this.selectOff = function(){
		GEOL.containerInfo.innerHTML = '';
	//	GEOL.auraIcon.hide();
		this.stateSelect = false;
		this.updateTextureState();
	};

	this.updateTextureState = function(){
		if (this.stateHighlight)
		{
			this.mesh.material.map = this.textureHifhlight;
		}
		else
		if (this.stateSelect)
			this.mesh.material.map = this.textureSelect;
		else
			this.mesh.material.map = this.textureNorm;

		this.mesh.material.needsUpdate = true;

		var sc = 12;

		if (this.stateHighlight ||
			this.stateSelect )
				sc = GEOL.visibleDistance /300* GEOL.paramIcons.project.scale_act;
			else
				sc = GEOL.visibleDistance /300
					* GEOL.paramIcons.project.scale;
		this.mesh.scale.set(sc, sc, sc);
	};

	this.geom = GEOL.createGeometryGex(6);

	this.geom.verticesNeedUpdate =this.geom.uvsNeedUpdate = true;


	if (GEOL.paramIcons.project.sprite)
	{
		this.mat  = new THREE.SpriteMaterial({
			// transparent:true,
			color: 0xffffff,
			map: this.textureNorm
			, depthWrite: false
			, depthTest: false
			//,blending: THREE.AdditiveBlending,

			//,
			//color: new THREE.Color(0xff)
		});

		this.mesh = new THREE.Sprite(  this.mat  );

	}
	else
	{
		this.mat  = new THREE.MeshBasicMaterial({
//		side:THREE.DoubleSide,
			transparent:true,
			map: this.textureNorm,
			depthWrite:false
		});

		this.mesh = new THREE.Mesh( this.geom , this.mat  );
	}


	this.mesh.material.map.needsUpdate = true;


	this.scale = GEOL.paramIcons.project.scale;
	this.mesh.scale.set(this.scale ,this.scale ,this.scale );

	this.mesh.rotateY(-Math.PI/2 + deg2rad(data.lng));

	this.mesh.rotateX(deg2rad(-data.lat));


	this.mesh.translateZ(GEOL.BASE_RAD * GEOL.heightGeo.project);
	this.mesh.objType = "project";
	this.mesh.name = "project " + this.graphInfo.gid + " - " + data.status;
	this.mesh.objParent = this;
	this.mesh.renderOrder = (GEOL.renderOrderUser += 3);

}