
GEOL.createEnvironmentStars =function(){

	var textLoader = new THREE.TextureLoader();
	textLoader.load(req_ajx_host+"globe/textures/star_sprite.png", createStars);
	function createStars(texture){

		var geom = new THREE.Geometry();
		var material = new THREE.PointsMaterial({
			transparent: true,
			// color: color, // — Points color in hexadecimal. Default is 0xffffff.
			map:texture, // — a texture.If defined, then a point has the data from texture as colors. Default is null.
			size:5,// — Define size of points. Default is 1.0.
			sizeAttenuation: true, //sizeAttenuation, // — Enable/disable size attenuation with distance.
			//vertexColors:vertexColors ? THREE.VertexColors : THREE.NoColors	, // — Define whether the material uses vertex colors, or not. Default is false.
			fog:false //— Define whether the material color is affected by global fog settings. Default is true.
		});

		var color = new THREE.Color(0xff0000);
		var range = 500;
		var m = new THREE.Matrix4();
		var angZ, angY;
		var vaxY = new THREE. Vector3(0,1,0);
		var vaxZ = new THREE. Vector3(0,0,1);

		var nPrtcl = 3000;
		geom.vertices.length =nPrtcl;
		for (var i = 0; i < nPrtcl; i++) {

			m.identity();
			var vcPos = new THREE.Vector3(400+Math.random()*600,0,0);

			angZ = Math.PI * (-0.5+Math.random()) * Math.cos(Math.PI/2 * Math.random());
			angY = Math.PI*2*Math.random();

			m.makeRotationZ(angZ);

			m.makeRotationY(Math.PI*2*Math.random());

			vcPos.applyAxisAngle( vaxZ, angZ );
			vcPos.applyAxisAngle( vaxY, angY );

			geom.vertices[i] = vcPos;

			var clr = new THREE.Color(0x00ff00);
			var h = color.getHSL().h,	s = color.getHSL().s,	l = color.getHSL().l;
			clr.setHSL(	h,	s,	Math.random() * l);

			geom.colors.push(clr);
		}
		var meshStars = new THREE.Points(geom, material);
		meshStars.name = "cloudStars";
		GEOL.scene.add(meshStars);
	}


	function createTextGlow(){

		//Создаем канвас в ширину и в высоту равный диаметру круга

		var szSqr = 1024;
		var txtCanvas = document.createElement('canvas');
		txtCanvas.id = "GL_cnv_glow";
		if (GEOL.sets.debug === true)
			document.getElementById("body").appendChild(txtCanvas );

		txtCanvas.width = szSqr;
		txtCanvas.height = szSqr;
		//Задаем хар-ки круга и рисуем
		var txtCntxt = txtCanvas.getContext('2d');

		txtCntxt.clearRect(0, 0, szSqr, szSqr);
		txtCntxt.shadowBlur=szSqr/4;
		txtCntxt.shadowColor=GEOL.colorsDef.auras.sphere_corona;

		txtCntxt.beginPath();
		txtCntxt.arc(szSqr/2, szSqr/2, szSqr/4, 0, 2 * Math.PI, false);
		txtCntxt.fillStyle = 'white';
		txtCntxt.fill();

		var texture = new THREE.Texture(txtCanvas);
		texture.needsUpdate = true;

		return texture;
	}











	


	var mapGlow = createTextGlow();
	var matGlow = new THREE.SpriteMaterial( { map: mapGlow, color: 0xffffff, fog: true } );

	var spriteGlow = new THREE.Sprite( matGlow );

	//spriteGlow.translateZ(8);
	spriteGlow.name = "sprite glow";
	spriteGlow.scale.multiplyScalar(3.9*GEOL.BASE_RAD);
	spriteGlow. renderOrder = GEOL.renderOrderOcean;
	GEOL.scene.add( spriteGlow );
}

