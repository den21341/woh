/**
 * @author qiao / https://github.com/qiao
 * @author mrdoob / http://mrdoob.com
 * @author alteredq / http://alteredqualia.com/
 * @author WestLangley / http://github.com/WestLangley
 */

THREE.GeocamControls = function ( object, domElement, scene ) {

    this.object = object;
    this.domElement = ( domElement !== undefined ) ? domElement : document;

    // API

    this.enabled = true;

    this.center = new THREE.Vector3();

    this.userZoom = true;
    this.userZoomSpeed = 1.0;

    this.userRotate = true;
    this.userRotateSpeed = 1.0;

    this.userPan = true;
    this.userPanSpeed = 2.0;

    this.autoRotate = false;
    this.autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60

    this.minPolarAngle = 0.05  ; // radians
    this.maxPolarAngle = Math.PI - 0.05; // radians

    this.minDistance = 0;
    this.maxDistance = Infinity;

    this.keys = { LEFT: 37, UP: 38, RIGHT: 39, BOTTOM: 40 };

    // internals

    var scope = this;

    var EPS = 0.000001;
    var PIXELS_PER_ROUND = 1800;

    var rotateStart = new THREE.Vector2();
    var rotateEnd = new THREE.Vector2();
    var rotateDelta = new THREE.Vector2();

    var zoomStart = new THREE.Vector2();
    var zoomEnd = new THREE.Vector2();
    var zoomDelta = new THREE.Vector2();

    var phiDelta = 0;
    var thetaDelta = 0;
	var scale = 2.5;


    var lastPosition = new THREE.Vector3();

    var STATE = { NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2 };
    var state = STATE.NONE;

    // events

    var changeEvent = { type: 'change' };


    this.rotateLeft = function ( angle ) {

        if ( angle === undefined ) {

            angle = getAutoRotationAngle();

        }

        thetaDelta -= angle;
		this.updateRotateState();


	};


	GEOL.visibleDistance = 10000;

	this.updateVisibleDistanse = function () {
		this.update();
		var dst = this.object.position.length();
		var sinBAO = GEOL.BASE_RAD / dst;
		var CO = GEOL.BASE_RAD * sinBAO;
		var AC = dst - CO;
		var cosBAO = Math.sqrt(1 - Math.pow(sinBAO, 2));



		GEOL.visibleDistance = 	AC;
		GEOL.globeDistance = 	dst - GEOL.BASE_RAD;

		if (false)
		{
		if (GEOL.scene)
		{
			GEOL.scene.fog.far = GEOL.visibleDistance;
			GEOL.scene.fog.near = GEOL.visibleDistance*0.98;
		}

		if (GEOL.sceneOver)
		{
			GEOL.sceneOver.fog.far = GEOL.visibleDistance;
			GEOL.sceneOver.fog.near = GEOL.visibleDistance*0.98;
		}

		}

	};

	this.rotateRight = function ( angle ) {

        if ( angle === undefined ) {

            angle = getAutoRotationAngle();

        }

        thetaDelta += angle;
		this.updateRotateState();


	};

    this.rotateUp = function ( angle ) {

        if ( angle === undefined ) {

            angle = getAutoRotationAngle();

        }

        phiDelta -= angle;
		this.updateRotateState();


	};

    this.rotateDown = function ( angle ) {

        if ( angle === undefined ) {

            angle = getAutoRotationAngle();

        }

        phiDelta += angle;
		this.updateRotateState();
	};

    this.zoomIn = function ( zoomScale ) {

        if ( zoomScale === undefined ) {

            zoomScale = getZoomScale();

        }

        scale /= zoomScale;

		this.updateVisibleDistanse();
		this.updateScaleState();
	};

    this.zoomOut = function ( zoomScale ) {

        if ( zoomScale === undefined ) {

            zoomScale = getZoomScale();

        }

        scale *= zoomScale;

		this.updateVisibleDistanse();
		this.updateScaleState();
	};

    this.pan = function ( distance ) {

        distance.transformDirection( this.object.matrix );
        distance.multiplyScalar( scope.userPanSpeed );

        this.object.position.add( distance );
        this.center.add( distance );

    };


	this.updateScaleState = function(){

		if (GEOL.mainUserMash )
		{
			var sc = GEOL.visibleDistance /300
				* GEOL.paramIcons.user_main.scale;

			GEOL.mainUserMash.scale
				.set(sc , sc, sc);
		}

		var c = GEOL.objUserModeHelpers.children;

		var sc = GEOL.visibleDistance /300
			* GEOL.paramIcons.user_help.scale;

		for (var i = c.length; i--;)
		{
			c[i].scale.set(sc , sc, sc);
		}

		var bs = GEOL.paramIcons.user_circle.scaleRatio;
		if (GEOL.circleLight  )
		{
			if (GEOL.circleLight.selectType == "usr_main")
				bs *= GEOL.paramIcons.user_main.scale;
			else
				bs *= GEOL.paramIcons.user_help.scale;

			sc = GEOL.visibleDistance /300 * bs;

			GEOL.circleLight.mesh.scale.set(sc,
				sc ,
				sc );

		}

		bs = GEOL.paramIcons.user_circle.scaleRatio;
		if (GEOL.circleSelect  )
		{
			if (GEOL.circleSelect.selectType == "usr_main")
				bs *= GEOL.paramIcons.user_main.scale;
			else
				bs *= GEOL.paramIcons.user_help.scale;

			sc = GEOL.visibleDistance /300 * bs;

			GEOL.circleSelect.mesh.scale.set(sc,
				sc ,
				sc );

		}

		if (GEOL.objProjectModeProjects.visible)
			for( var k in GEOL.arrProjects){
				if (GEOL.arrProjects[k].stateHighlight ||
					GEOL.arrProjects[k].stateSelect )
					sc = GEOL.visibleDistance /300* GEOL.paramIcons.project.scale_act;
				else
					sc = GEOL.visibleDistance /300
						* GEOL.paramIcons.project.scale;
				GEOL.arrProjects[k].mesh.scale.set(sc, sc, sc);
			}

		if (GEOL.objUserModeLinks.visible){
			for (var k  in GEOL.graph.links){
				GEOL.graph.links[k].updateScaleGeometry();

			}

		}
	};

	this.updateRotateState = function(){

		var v4 = new THREE.Vector4(0,0,0,1);
		var v3= new THREE.Vector3(0,0,0);;


		if (GEOL.mainUserMash )
		{

			v3 = GEOL.mainUserMash.position;
			v4.set(v3.x, v3.y, v3.z, 1);
			v4.applyMatrix4(GEOL.camera.matrixWorldInverse);

			//v4.z *= -1;
			v4.z += GEOL.visibleDistance;
			if (v4.z < 0)
				GEOL.mainUserMash.material.opacity = 0;
			else
			if (v4.z < 5)
				GEOL.mainUserMash.material.opacity =  v4.z / 5;
			else
				GEOL.mainUserMash.material.opacity = 1;

//			console.log("z " +v4.z);
		}

		var c = GEOL.objUserModeHelpers.children;

		if (GEOL.objUserModeHelpers.visible)
		for (var i = c.length; i--;)
		{
			v3 = c[i].position;
			v4.set(v3.x, v3.y, v3.z, 1);
			v4.applyMatrix4(GEOL.camera.matrixWorldInverse);

			v4.z += GEOL.visibleDistance;
			if (v4.z < 0)
				c[i].material.opacity = 0;
			else
			if (v4.z < 5)
				c[i].material.opacity =  v4.z / 5;
			else
				c[i].material.opacity = 1;
		}


		c = GEOL.objProjectModeProjects.children;
		if (GEOL.objProjectModeProjects.visible)
		for (var i = c.length; i--;)
		{
			v3 = c[i].position;
			v4.set(v3.x, v3.y, v3.z, 1);
			v4.applyMatrix4(GEOL.camera.matrixWorldInverse);

			v4.z += GEOL.visibleDistance;
			if (v4.z < 0)
				c[i].material.opacity = 0;
			else
			if (v4.z < 5)
				c[i].material.opacity =  v4.z / 5;
			else
				c[i].material.opacity = 1;
		}

	};

	this.updateStates = function(){
		this.updateVisibleDistanse()
		this.updateRotateState();
		this.updateScaleState();
	}

    function getAutoRotationAngle() {

        return 2 * Math.PI / 60 / 60 * scope.autoRotateSpeed;

    }

    function getZoomScale() {

        return Math.pow( 0.95, scope.userZoomSpeed );

    }

	//( performance || Date )

	//var tmPrev  = this.getTime.now() - 30;
	//var tmNow = this.getTime.now() ;



	var vLocateStart = new THREE.Vector4();

	this.qLocateStart = new THREE.Quaternion();
	this.qLocateEnd = new THREE.Quaternion();
	this.qTmp = new THREE.Quaternion();

	this.dLocateDist = 100;
	this.bLocating = false;

	this.tmLocatingLength = 500;
	this.tmLocate_ll_start = 0;


	this.locate_ll = function (ll){

		this.bLocating = true;
		var vPos = this.object.position;
		this.dLocateDist = vPos.length();

		vLocateStart.set(vPos.x, vPos.y, vPos.z, vPos.w );

		this.tmLocate_ll_start = ( performance || Date ).now();



		//var a = new THREE.Euler( deg2rad(llStart.lat), deg2rad(llStart.lng), Math.PI/2, 'XYZ' );//ZYX
		var a = new THREE.Matrix4();//ZYX
		var b = new THREE.Matrix4();//ZYX

		a.makeRotationX(deg2rad(-ll.lat+90));
		b.makeRotationY(deg2rad(ll.lng-90));

		b.multiply(a);

		var v2 = new THREE.Vector4(0,1,0, 1);
		v2.applyMatrix4(b);

		this.qLocateEnd.setFromRotationMatrix(b);


		var offset = this.object.position;

		var theta = Math.atan2( offset.x, offset.z );

		// angle from y-axis

		// phi - lat, 0 - North Pole/90
		// theta - lng, 0 - Indian Ocean/90

		var phi = Math.atan2( Math.sqrt( offset.x * offset.x + offset.z * offset.z ), offset.y );


		//.slerp ( qStart, qEnd, qTarget, t )

		a.identity();
		b.identity();

		a.makeRotationX(phi);
		b.makeRotationY(theta);

		b.multiply(a);

		 v2 = new THREE.Vector4(0,1,0, 1);
		v2.applyMatrix4(b);


		this.qLocateStart.setFromRotationMatrix(b);

	}

	this.light = new THREE.SpotLight(0xffffff);
	this.light.lookAt(new THREE.Vector3(0,0,0));
	this.light.angle = Math.PI/45;
	this.light.intensity = 2;
	this.light.distance=1000;
	this.light.penumbra = 0.8;
	scene.add(this.light);


	var t, tm;
	this.update = function (tmN, tmP) {

		if (this.bLocating)
		{

			var v = new THREE.Vector3(0,1,0);
			tm = ( performance || Date ).now();

			if ( tm >	this.tmLocate_ll_start  + this.tmLocatingLength ){
				t = 1;
				this.bLocating = false;
			}
			else
				t = (tm - this.tmLocate_ll_start)  / this.tmLocatingLength;

			THREE.Quaternion.slerp( this.qLocateStart, this.qLocateEnd, this.qTmp, t );
			v.applyQuaternion(this.qTmp);

			v.multiplyScalar(this.dLocateDist);

			this.object.position.set(v.x, v.y, v.z);

			this.object.lookAt( this.center );
			this.updateRotateState();
			return;
		}

		//	tmNow = this.getTime.now() ;
		var position = this.object.position;
		var offset = position.clone().sub( this.center );

		// angle from z-axis around y-axis

		var theta = Math.atan2( offset.x, offset.z );

		// angle from y-axis

		// phi - lat, 0 - North Pole/90
		// theta - lng, 0 - Indian Ocean/90

		var phi = Math.atan2( Math.sqrt( offset.x * offset.x + offset.z * offset.z ), offset.y );

		if ( this.autoRotate ) {

			this.rotateLeft( getAutoRotationAngle() );

		}



		this.rotateLeft( 2 * Math.PI * this.vDelta.x * 0.1/ PIXELS_PER_ROUND * scope.userRotateSpeed );
		this.rotateUp( 2 * Math.PI * this.vDelta.y * 0.1/ PIXELS_PER_ROUND * scope.userRotateSpeed );

		this.vDelta.multiplyScalar(Math.pow(0.9, ( (tmN || window.GeoTmrNow) - (tmP ||window.GeoTmrPrev )) / 20));

		theta += thetaDelta;
		phi += phiDelta;


		// restrict phi to be between desired limits
		phi = Math.max( this.minPolarAngle, Math.min( this.maxPolarAngle, phi ) );

		// restrict phi to be betwee EPS and PI-EPS
		phi = Math.max( EPS, Math.min( Math.PI - EPS, phi ) );

		var radius = offset.length() * scale;

		// restrict radius to be between desired limits
		radius = Math.max( this.minDistance, Math.min( this.maxDistance, radius ) );

		offset.x = radius * Math.sin( phi ) * Math.sin( theta );
		offset.y = radius * Math.cos( phi );
		offset.z = radius * Math.sin( phi ) * Math.cos( theta );

		position.copy( this.center ).add( offset );

		this.object.lookAt( this.center );

		thetaDelta = 0;
		phiDelta = 0;
		scale = 1;

		if ( lastPosition.distanceTo( this.object.position ) > 0 ) {

			this.dispatchEvent( changeEvent );

			lastPosition.copy( this.object.position );

		}

		if (GEOL && GEOL.piramide){
			GEOL.piramide.updateVisible();
		}

		var pos = this.object.position.clone();

		pos.setLength ( 500 );

		this.light.position.set(pos.x, pos.y, pos.z);

		//	this.updateVisibleDistanse();

		//	console.log("prev " + tmPrev + "\nnow " + tmNow +"\nsub " + (tmNow - tmPrev) +"\n\n");
//		tmPrev = tmNow;

	};

	this.vDelta = new THREE.Vector2(0,0);
    function onMouseDown( event ) {

        if ( scope.enabled === false ) return;
        if ( scope.userRotate === false ) return;

        event.preventDefault();

        if ( event.button === 0 ) {

            state = STATE.ROTATE;

            rotateStart.set( event.clientX, event.clientY );

        } else if ( event.button === 1 ) {

            state = STATE.ZOOM;

            zoomStart.set( event.clientX, event.clientY );

        } else if ( event.button === 2 ) {

          //  state = STATE.PAN;

        }

        document.addEventListener( 'mousemove', onMouseMove, false );
        document.addEventListener( 'mouseup', onMouseUp, false );

    }

    function onMouseMove( event ) {

        if ( scope.enabled === false ) return;

        event.preventDefault();

        if ( state === STATE.ROTATE ) {

            rotateEnd.set( event.clientX, event.clientY );
			rotateDelta.subVectors( rotateEnd, rotateStart );

			scope.vDelta.add(rotateDelta);

//            scope.rotateLeft( 2 * Math.PI * rotateDelta.x / PIXELS_PER_ROUND * scope.userRotateSpeed );
  //          scope.rotateUp( 2 * Math.PI * rotateDelta.y / PIXELS_PER_ROUND * scope.userRotateSpeed );

            rotateStart.copy( rotateEnd );

        } else if ( state === STATE.ZOOM ) {

            zoomEnd.set( event.clientX, event.clientY );
            zoomDelta.subVectors( zoomEnd, zoomStart );

            if ( zoomDelta.y > 0 ) {

                scope.zoomIn();

            } else {

                scope.zoomOut();

            }

            zoomStart.copy( zoomEnd );

        } else if ( state === STATE.PAN ) {

            var movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
            var movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

            scope.pan( new THREE.Vector3( - movementX, movementY, 0 ) );

        }

    }

    function onMouseUp( event ) {

        if ( scope.enabled === false ) return;
        if ( scope.userRotate === false ) return;

        document.removeEventListener( 'mousemove', onMouseMove, false );
        document.removeEventListener( 'mouseup', onMouseUp, false );

        state = STATE.NONE;

    }

    function onMouseWheel( event ) {

        if ( scope.enabled === false ) return;
        if ( scope.userZoom === false ) return;

        var delta = 0;

        if ( event.wheelDelta ) { // WebKit / Opera / Explorer 9

            delta = event.wheelDelta;

        } else if ( event.detail ) { // Firefox

            delta = - event.detail;

        }

        if ( delta > 0 ) {

            scope.zoomOut();

        } else {

            scope.zoomIn();

        }

		var e = event || window.event;


		e.returnValue = false;
		e.preventDefault ? e.preventDefault() : (e.returnValue = false);

		e.cancelBubble = true;
		return false;
	}

    function onKeyDown( event ) {

        if ( scope.enabled === false ) return;
        if ( scope.userPan === false ) return;

        switch ( event.keyCode ) {

            case scope.keys.UP:
                scope.pan( new THREE.Vector3( 0, 1, 0 ) );
                break;
            case scope.keys.BOTTOM:
                scope.pan( new THREE.Vector3( 0, - 1, 0 ) );
                break;
            case scope.keys.LEFT:
                scope.pan( new THREE.Vector3( - 1, 0, 0 ) );
                break;
            case scope.keys.RIGHT:
                scope.pan( new THREE.Vector3( 1, 0, 0 ) );
                break;
        }

		this.updateVisibleDistanse();
	}

    this.domElement.addEventListener( 'contextmenu', function ( event ) { event.preventDefault(); }, false );
    this.domElement.addEventListener( 'mousedown', onMouseDown, false );
    this.domElement.addEventListener( 'mousewheel', onMouseWheel, false );
    this.domElement.addEventListener( 'DOMMouseScroll', onMouseWheel, false ); // firefox
    this.domElement.addEventListener( 'keydown', onKeyDown, false );


};

THREE.GeocamControls.prototype = Object.create( THREE.EventDispatcher.prototype );