

var COUNT_LINK_SLICE  = 50;
var COUNT_LINK_SEGMENTS = 4;
var LINK_RADIUS = 0.1;

var GEOL = GEOL || {};



function  QuatSlerp(res,
q,  p,  t)
{
	var p1 = Array(4);
	var  omega, cosom, sinom, scale0, scale1;

	// косинус угла
	cosom = q.x* p.x + q.y* p.y + q.z*p.z + q.w* p.w;

	if ( cosom <0.0 )
	{
		cosom = -cosom;
		p1[0] = - p.x;  p1[1] = - p.y;
		p1[2] = - p.z;  p1[3] = - p.w;
	}
	else
	{
		p1[0] = p.x;    p1[1] = p.y;
		p1[2] = p.z;    p1[3] = p.w;
	}

	if ( (1.0 - cosom) > 0.001 )
	{
		// стандартный случай (slerp)
		omega = Math.acos(cosom);
		sinom = Math.sin(omega);
		scale0 = Math.sin((1.0 - t) * omega) / sinom;
		scale1 = Math.sin(t * omega) / sinom;
	}
	else
	{
		// если маленький угол - линейная интерполяция
		scale0 = 1.0 - t;
		scale1 = t;
	}

	res.x = scale0 * q.x + scale1 * p1[0];
	res.y = scale0 * q.y + scale1 * p1[1];
	res.z = scale0 * q.z + scale1 * p1[2];
	res.w = scale0 * q.w + scale1 * p1[3];
}

GEOL.linkHgtCoeff = 0;

THREE.EarthLink = THREE.Curve.create(


function ( llStart, llEnd  ) {




		//var a = new THREE.Euler( deg2rad(llStart.lat), deg2rad(llStart.lng), Math.PI/2, 'XYZ' );//ZYX
		var a = new THREE.Matrix4();//ZYX
		var b = new THREE.Matrix4();//ZYX

		a.makeRotationZ(deg2rad(llStart.lat));
		b.makeRotationY(deg2rad(llStart.lng));

		b.multiply(a);
		var v2 = new THREE.Vector4(1,0,0, 1);
		v2.applyMatrix4(b);

		this.qStart = new THREE.Quaternion();
		this.qStart.setFromRotationMatrix(b);


		a.identity();
		b.identity();
		a.makeRotationZ(deg2rad(llEnd.lat));
		var v3 = new THREE.Vector4(1,0,0, 1);
		v3.applyMatrix4(a);
		b.makeRotationY(deg2rad(llEnd.lng));

		b.multiply(a);

		v3 = new THREE.Vector4(1,0,0, 1);
		v3.applyMatrix4(b);


		this.qEnd = new THREE.Quaternion();
		this.qEnd.setFromRotationMatrix(b);

		this.qTmp = new THREE.Quaternion();


		var v1 = new THREE.Vector3(1,0,0);
		v1.applyQuaternion(this.qStart);
		var v2 = new THREE.Vector3(1,0,0);
		v2.applyQuaternion(this.qEnd);

		this.dist1 = v1.angleTo(v2)/Math.PI;




	},

	function ( t ) {

		//THREE.Quaternion.slerp( this.qStart, this.qEnd, this.qTmp, t);
		QuatSlerp(this.qTmp, this.qStart, this.qEnd, t);

		var v = new THREE.Vector3(1,0,0);

		//var h = Math.sin(this.dist1);

		v.applyQuaternion(this.qTmp);




		v.multiplyScalar(GEOL.linkHgtCoeff  * ( 1+ GEOL.linkage.height_coef  * this.dist1 * Math.sin(Math.PI*t)));



		return v;

	}

);


var Link = function ( llStart, llEnd, ColorBase ) {




	this.colorBase = ColorBase;
	this.llStart = llStart;
	this.llEnd = llEnd;

	this.shaderVertex = "		attribute vec3 colorRandom;\n\n		uniform vec3 colorBase;\n		uniform	float	alphaBase;\n		uniform	float	alphaNone;\n		uniform float	minPos;\n		uniform float	maxPos;\n\n		uniform float pos;\n\n		varying vec3 vColor;  // 'varying' vars are passed to the fragment shader\n		varying float alphaColor;\n\n		void main() {\n\n			if (color.r < minPos )\n			{\n			alphaColor = alphaNone;\n			}\n		else\n		if (color.r < maxPos )\n		{\n			alphaColor = alphaBase;\n			}\n		else\n		alphaColor = alphaNone;\n\n\n		vColor = colorBase;   // pass the color to the fragment shader\n		//vColor = colorRandom;\n		//gl_PointSize = size;\n		gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );\n\n	}"	;
	this.shaderFragment = "\n		varying vec3 vColor;\n		varying float alphaColor;\n\n		void main() {\n			gl_FragColor = vec4(vColor, alphaColor);  // adjust the alpha\n\n		}\n";


	this.startTime = function(tmLngth){
		this.tmStart = GEOL.timeNow();
		this.tmLength = tmLngth;
	}


	this.updateTime = function(){

		this.tmLength = tmLngth;
	}



	var e = new THREE.EarthLink(llStart, llEnd  );

	//e = new THREE.LineCurve(new THREE.Vector3(-100,100,0), new THREE.Vector3(100,100,0));

	this.geometry  = new THREE.TubeGeometry(
		e,  //path
		COUNT_LINK_SLICE  ,    //segments
		LINK_RADIUS,     //radius
		COUNT_LINK_SEGMENTS,     //radiusSegments
		false  //closed
	);


	var cntSlice = COUNT_LINK_SLICE+1;


	for (var i = 0, sz = this.geometry.faces.length; i < sz; i++)
	{
		this.geometry.faces[i].vertexColors[0] = new THREE.Color(Math.floor(this.geometry.faces[i].a / 3)/ cntSlice, 0,0);
		this.geometry.faces[i].vertexColors[1] = new THREE.Color(Math.floor(this.geometry.faces[i].b / 3)/ cntSlice, 0,0);
		this.geometry.faces[i].vertexColors[2] = new THREE.Color(Math.floor(this.geometry.faces[i].c / 3)/ cntSlice, 0,0);
	}


	this.uniforms = {
		colorBase: { type: "c", value: this.colorBase },
		alphaBase: { type: "f", value: 1.0 },
		alphaNone: { type: "f", value: 0.1 },
		minPos: { type: "f", value: 0.1 },
		maxPos: { type: "f", value: 0.9 },
		ixTime: { type: "f", value: 0 },
		cntSlice: { type: "f", value: cntSlice }
	};


	this.material = new THREE.ShaderMaterial({
		//	blending:       THREE.AdditiveBlending,
		//	depthTest:      false,
		//	blending:       THREE.AdditiveBlending,
		vertexColors: THREE.VertexColors,
		//color:  new THREE.Color( 0xffffff ) ,
		transparent:true,
		uniforms: this.uniforms,
		vertexShader: this.shaderVertex,
		fragmentShader: this.shaderFragment
	});





	//var obTube = new
	this.mesh = new THREE.Mesh( this.geometry, this.material );

	this.mesh.objType = "link";


	return this;

};

Link.prototype = {
	constructor: Link,

	setTimeFly: function(tm){
		this.timeFly = tm;
	}
}

GEOL.EarthLink = THREE.Curve.create(

	function ( llStart, llEnd  ) {




		//var a = new THREE.Euler( deg2rad(llStart.lat), deg2rad(llStart.lng), Math.PI/2, 'XYZ' );//ZYX
		var a = new THREE.Matrix4();//ZYX
		var b = new THREE.Matrix4();//ZYX

		a.makeRotationZ(deg2rad(llStart.lat));
		b.makeRotationY(deg2rad(llStart.lng));

		b.multiply(a);
		THREE.Clock()
		var v2 = new THREE.Vector4(1,0,0, 1);
		v2.applyMatrix4(b);

		this.qStart = new THREE.Quaternion();
		this.qStart.setFromRotationMatrix(b);


		a.identity();
		b.identity();
		a.makeRotationZ(deg2rad(llEnd.lat));
		var v3 = new THREE.Vector4(1,0,0, 1);
		v3.applyMatrix4(a);
		b.makeRotationY(deg2rad(llEnd.lng));

		b.multiply(a);

		v3 = new THREE.Vector4(1,0,0, 1);
		v3.applyMatrix4(b);


		this.qEnd = new THREE.Quaternion();
		this.qEnd.setFromRotationMatrix(b);

		this.qTmp = new THREE.Quaternion();


		var v1 = new THREE.Vector3(1,0,0);
		v1.applyQuaternion(this.qStart);
		var v2 = new THREE.Vector3(1,0,0);
		v2.applyQuaternion(this.qEnd);

		this.dist1 = v1.angleTo(v2)/Math.PI;




	},

	function ( t ) {

		//THREE.Quaternion.slerp( this.qStart, this.qEnd, this.qTmp, t);
		QuatSlerp(this.qTmp, this.qStart, this.qEnd, t);

		var v = new THREE.Vector3(1,0,0);

		//var h = Math.sin(this.dist1);

		v.applyQuaternion(this.qTmp);




		v.multiplyScalar(GEOL.linkHgtCoeff * ( 1+ GEOL.linkage.height_coef * this.dist1 * Math.sin(Math.PI*t)));



		return v;

	}

);


GEOL.Link = function ( llStart, llEnd, ColorBase, graphInf  ) {

	GEOL.linkHgtCoeff =  GEOL.BASE_RAD * GEOL.heightGeo.user_help;

	this.graphInfo = graphInf || {from:"unknown", to: "zero", p_type:"t1"};
	this.colorBase = ColorBase;
	this.llStart = llStart;
	this.llEnd = llEnd;

	/*
	this.shaderVertex = `

		attribute vec3 colorRandom;

	uniform vec3 colorBase;
	uniform	float	alphaBase;
	uniform	float	alphaNone;
	uniform float	minPos;
	uniform float	maxPos;

	uniform float pos;

	varying vec3 vColor;  // 'varying' vars are passed to the fragment shader
	varying float alphaColor;

	void main() {

		if (color.r < minPos )
		{
			alphaColor = alphaNone;
		}
		else
		if (color.r < maxPos )
		{
			alphaColor = alphaBase;
		}
		else
			alphaColor = alphaNone;


		vColor = colorBase;   // pass the color to the fragment shader
		//vColor = colorRandom;
		//gl_PointSize = size;
		gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );

	}
	`;

	this.shaderFragment = `
		varying vec3 vColor;
	varying float alphaColor;

	void main() {
		gl_FragColor = vec4(vColor, alphaColor);  // adjust the alpha

	}
`;
*/


	this.shaderVertex = "\n		attribute vec3 colorRandom;\n\n	uniform vec3 colorBase;\n	uniform	float	alphaBase;\n	uniform	float	alphaNone;\n	uniform float	minPos;\n	uniform float	maxPos;\n\n	uniform float pos;\n\n	varying vec3 vColor;  // 'varying' vars are passed to the fragment shader\n	varying float alphaColor;\n\n	void main() {\n\n		if (color.r < minPos )\n		{\n			alphaColor = alphaNone;\n		}\n		else\n		if (color.r < maxPos )\n		{\n			alphaColor = alphaBase;\n		}\n		else\n			alphaColor = alphaNone;\n\n\n		vColor = colorBase;   // pass the color to the fragment shader\n		//vColor = colorRandom;\n		//gl_PointSize = size;\n		gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );\n\n	}\n";

	this.shaderFragment = "\n		varying vec3 vColor;\n	varying float alphaColor;\n\n	void main() {\n		gl_FragColor = vec4(vColor, alphaColor);  // adjust the alpha\n\n	}\n";

	this.startAnimation = function(tmLngth, loop){
		this.tmStart = GEOL.timeNow();

		this.bLoopAnimation = loop;
		this.bEnableAnimation = true;

		this.tmLength = tmLngth;
		this.tmLength2 = tmLngth*2;
	}

	this.bLoopAnimation = false;
	this.bEnableAnimation = false;

	this.updateAnimation = function(){

		if(!this.bEnableAnimation)
			return;

		if (this.bLoopAnimation){

			var t = (GEOL.GeoTmrNow - this.tmStart)/this.tmLength;
			this.uniforms.minPos.value = 1- (t - Math.floor(t))*2 ;
			this.uniforms.maxPos.value = 1+ this.uniforms.minPos.value ;

			this.uniforms.minPos.needsUpdate = true;
			this.uniforms.maxPos.needsUpdate = true;


		}
		else
		{
			this.uniforms.minPos.value = (GEOL.GeoTmrNow - this.tmStart - this.tmLength) / this.tmLength;
			this.uniforms.maxPos.value = (GEOL.GeoTmrNow - this.tmStart) / this.tmLength;
			this.uniforms.minPos.needsUpdate = true;
			this.uniforms.maxPos.needsUpdate = true;
		}
	}


	this.lnkEarth = new GEOL.EarthLink(llEnd, llStart  );

	//e = new THREE.LineCurve(new THREE.Vector3(-100,100,0), new THREE.Vector3(100,100,0));

	this.geometry  = new THREE.TubeGeometry(
		this.lnkEarth,  //path
		GEOL.linkage.segments  ,    //segments
		GEOL.linkage.size,     //radius
		GEOL.linkage.faces,     //radiusSegments
		false  //closed
	);

	this.updateScaleGeometry = function(){


		var sc = GEOL.visibleDistance /300
			* GEOL.linkage.size;

		var gm  = new THREE.TubeGeometry(
			this.lnkEarth,  //path
			GEOL.linkage.segments  ,    //segments
			sc,     //radius
			GEOL.linkage.faces,     //radiusSegments
			false  //closed
		);

		var vrThs = this.mesh.geometry.vertices;
		var vrGm = gm.vertices;

		if (vrThs.length != vrGm.length)
		{
			console.log("\""+this.mesh.name + "\" - unequal vertices count when update geometry");
			return;
		}

		var v;
		for (var i = vrThs.length; i--;){
			v = vrGm[i];
			vrThs[i].set(v.x, v.y, v.z);
		}

		//this.mesh.geometry.vertices = gm.vertices;
		this.mesh.geometry.verticesNeedUpdate = true;
 	};

	var cntSlice = GEOL.linkage.segments+1;


	for (var i = 0, sz = this.geometry.faces.length; i < sz; i++)
	{
		//var c =new THREE.Color(Math.floor(this.geometry.faces[i].a / GEOL.COUNT_LINK_SEGMENTS)/ cntSlice, 0,0)
		this.geometry.faces[i].vertexColors[0] =
			new THREE.Color(Math.floor(this.geometry.faces[i].a / GEOL.linkage.faces)/ cntSlice, 0,0);
		this.geometry.faces[i].vertexColors[1] = new THREE.Color(Math.floor(this.geometry.faces[i].b / GEOL.linkage.faces	)/ cntSlice, 0,0);
		this.geometry.faces[i].vertexColors[2] = new THREE.Color(Math.floor(this.geometry.faces[i].c / GEOL.linkage.faces)/ cntSlice, 0,0);
	}


	this.uniforms = {
		colorBase: { type: "c",
			value: this.colorBase
			},
		alphaBase: { type: "f", value: 1.0 },
		alphaNone: { type: "f", value: 0.1 },
		minPos: { type: "f", value: 0.0 },
		maxPos: { type: "f", value: 0.999 },
		ixTime: { type: "f", value: 0 },
		cntSlice: { type: "f", value: cntSlice }
	};


	this.material = new THREE.ShaderMaterial({
		//	blending:       THREE.AdditiveBlending,
		//	depthTest:      false,
		//	blending:       THREE.AdditiveBlending,
		vertexColors: THREE.VertexColors,
		//color:  new THREE.Color( 0xffffff ) ,
		transparent:true,
		uniforms: this.uniforms,
		vertexShader: this.shaderVertex,
		fragmentShader: this.shaderFragment,
		depthWrite: false
	});





	//var obTube = new
	this.mesh = new THREE.Mesh( this.geometry, this.material );
	this.mesh.renderOrder = (GEOL.renderOrderLink += 3);

	this.mesh.objType = "link";
	this.mesh.objParent = this;

	this.mesh.name = "link: "+this.graphInfo.from + " -> "+this.graphInfo.to ;


	return this;

};

GEOL.Link.prototype = {
	constructor: Link,

	setTimeFly: function(tm){
		this.timeFly = tm;
	}
}

GEOL.startAnumateLinks = function() {
	for (var k in GEOL.graph.links)
		GEOL.graph.links[k].startAnimation(1400, true);
}
GEOL.updateAnumateLinks = function(){
	for (var k in GEOL.graph.links)
		GEOL.graph.links[k].updateAnimation();
}
GEOL.stopAnumateLinks = function(){
}