var GEOL = GEOL || {};




GEOL.colorsDef = {
	// настройки цветов страны

	countryColor:{
		// дефолтное, при режиме юзеров
		none: {
			// наведенное состоние
			hovered:{
				opacity:0, //прозрачность для поверхности, в интервале 0..1
				shineVisible:true, // показывать ли заборчик
				shineHeight: 5, // высота заборчика
				shine: new THREE.Color(0x00ff00), // цвет забора
				cap: new THREE.Color(0xffffff) // цвет поверхности
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			// выбранное
			active:{
				opacity:0.4,
				shineVisible:true,
				shineHeight: 5,
				shine: new THREE.Color(0x00ff00),
				cap: new THREE.Color(0xffffff),
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			// нормальное
			normal:{
				opacity:0,
				shineVisible:true,
				shineHeight: 5,
				shine: new THREE.Color(0x00ff00),
				cap: new THREE.Color(0xffffff),
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.1
				}
			}
		},
		helped: {
			hovered:{
				opacity:0.6,
				shineVisible:true,
				shineHeight: 5,
				shine: new THREE.Color(0x09c531 ),
				cap: new THREE.Color(0x0af23c)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			active:{
				opacity:0.6,
				shineVisible:true,
				shineHeight: 5,
				shine: new THREE.Color(0x09c531),
				cap: new THREE.Color(0x0af23c)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			normal:{
				opacity:0.4,
				shineVisible:false,
				shineHeight: 5,
				shine: new THREE.Color(0x09c531),
				cap: new THREE.Color(0x0af23c)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.1
				}
			}

		},
		// а это цвета для режима проектов
		// состояние страны, в которой есть реализованнеу роекты
		covered: {
			hovered:{
				opacity:0.8,
				shineVisible:true,
				shineHeight: 10,
				shine: new THREE.Color(0xa2de01),
				cap: new THREE.Color(0xa2de01)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			active:{
				opacity:0.8,
				shineVisible:true,
				shineHeight: 10,
				shine: new THREE.Color(0xa2de01),
				cap: new THREE.Color(0xa2de01)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			normal:{
				opacity:0.35,
				shineVisible:false,
				shineHeight: 10,
				shine: new THREE.Color(0xa2de01),
				cap: new THREE.Color(0xa2de01)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.1
				}
			}

		},
		// доступные проекты
		available: {
			hovered:{
				opacity:0.4,
				shineVisible:true,
				shineHeight: 10,
				shine: new THREE.Color(0xffff00),
				cap: new THREE.Color(0xffff00)

				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			active:{
				opacity:0.4,
				shineVisible:true,
				shineHeight: 10,
				shine: new THREE.Color(0xffff00),
				cap: new THREE.Color(0xffff00)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			normal:{
				opacity:0.4,
				shineVisible:false,
				shineHeight: 10,
				shine: new THREE.Color(0xffff00),
				cap: new THREE.Color(0xffff00)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.1
				}
			}

		},
		// если для этой страны вообще в системе нету проектов
		forbidden: {
			hovered:{
				opacity:0.0,
				shineVisible:true,
				shineHeight: 0.00001,
				shine: new THREE.Color(0xffffff),
				cap: new THREE.Color(0xff0000)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			active:{
				opacity:0.0,
				shineVisible:true,
				shineHeight: 0.00001,
				shine: new THREE.Color(0xffffff),
				cap: new THREE.Color(0xff0000)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.9
				}
			},
			normal:{
				opacity:0.0,
				shineVisible:false,
				shineHeight: 0.00001,
				shine: new THREE.Color(0xffffff),
				cap: new THREE.Color(0xff0000)
				,
				line: {
					color:new THREE.Color(0xffffff),
					alpha: 0.1
				}
			}

		}
	},

	// это цвета для линий связей между юзерами
	/*
	 projectColor : {

	 t1:new THREE.Color( 0xf84840	),
	 t2:new THREE.Color( 0xffc12d	),
	 t3:new THREE.Color( 0xf36624	),
	 t4:new THREE.Color( 0x2b78b8	),
	 t5:new THREE.Color( 0x38b348	),
	 t6:new THREE.Color( 0x40c2f8	),
	 t7:new THREE.Color( 0xbe8756	),
	 t8:new THREE.Color( 0xb79afa	),
	 t9:new THREE.Color( 0x6b2b43	),
	 t10:new THREE.Color( 0x698422	),
	 t11:new THREE.Color( 0x006f00	),
	 t12:new THREE.Color( 0x888888	)
	 }*/
	projectColor : {

		t1:new THREE.Color( 0xff0000	),
		t2:new THREE.Color( 0xff9c00	),
		t3:new THREE.Color( 0xffea00	),

		t4:new THREE.Color( 0x4ac302	),
		t5:new THREE.Color( 0x017edf	),
		t6:new THREE.Color( 0x0021b1	),

		t7:new THREE.Color( 0x7d05ca	),
		t8:new THREE.Color( 0x00b6c6	),
		t9:new THREE.Color( 0xb9c500	),

		t10:new THREE.Color( 0xff9465	),
		t11:new THREE.Color( 0xff539c	),
		t12:new THREE.Color( 0xcd76ff	)
	}
 	,
	auras:{		// если везде выше цвет задается в формате threejs, то в аурах этим цветом
		// заполняется канвас, т.е. цвет должен быть в HTML-формате

		sphere_corona:'rgba(94, 174, 251, 0.5)'

		// user_main:"red", // не актуально
		// user_help:"red", // не актуально
		//	others:'#00ddff'

	}
	//GEOL.urlAuraIcon = "textures/aura_icon.png";

};

// нормальные иконки для юзеров
GEOL.linkIcons = {
	user_help: req_ajx_host+"globe/textures/icons/icon_help_norm.png",
	user_main: req_ajx_host+"globe/textures/icons/icon_main_norm.png"
};

// параметры геометрии связи

GEOL.linkage = {
	size: 0.5, // радиус цилиндра связи
	faces: 4, // количество граней
	segments: 50, // количество сегментов, чем меньше, тем угловастее
	height_coef: 0.15 // коэффициент возвышения дуги над поверхностью
};

// параметры иконок
GEOL.paramIcons = {

	user_sprite: true, // использовать ли спрайт, т.е. всегда прямосмотрящий
	// иконка просто пользователя
	user_help:{
		url_spot: req_ajx_host+"globe/textures/icons/icon_help_norm.png",
		scale: 3, // коэффициент масштабирования,
		scale_touch: 15



	},
	user_main:{
		url_spot: req_ajx_host+"globe/textures/icons/icon_main_norm.png",
		scale: 10,
		scale_touch: 18


	},


	user_circle:{
		scaleRatio: 2 // добавочный коэффициент масштаба для появляющегося кружка с авкой

	},
	// параметры иконки проектов
	// текстуры для нормального, наведенного и подсвеченного состояния задаются картинками
	project:{
		scale: 3,
		scale_act: 10,
		scale_touch: 15,
		scale_act_touch: 20,
		url:{
			normal:{
				t1: req_ajx_host+"globe/textures/icons/norm/1.png",
				t2: req_ajx_host+"globe/textures/icons/norm/2.png",
				t3: req_ajx_host+"globe/textures/icons/norm/3.png",
				t4: req_ajx_host+"globe/textures/icons/norm/s-intellect.png",
				t5: req_ajx_host+"globe/textures/icons/norm/s-medicine.png",
				t6: req_ajx_host+"globe/textures/icons/norm/s-const.png",
				t7: req_ajx_host+"globe/textures/icons/norm/s_icons_art.png",
				t8: req_ajx_host+"globe/textures/icons/norm/s_icons_travel.png",
				t9: req_ajx_host+"globe/textures/icons/norm/s_icons_startup.png",
				t10: req_ajx_host+"globe/textures/icons/norm/s-sport.png",
				t11: req_ajx_host+"globe/textures/icons/norm/s_icon_dream1-act.png",
				t12: req_ajx_host+"globe/textures/icons/norm/s-clothes.png"},
			highlight:{
				t1: req_ajx_host+"globe/textures/icons/light/1.png",
				t2: req_ajx_host+"globe/textures/icons/light/2.png",
				t3: req_ajx_host+"globe/textures/icons/light/3.png",
				t4: req_ajx_host+"globe/textures/icons/light/s-intellect.png",
				t5: req_ajx_host+"globe/textures/icons/light/s-medicine.png",
				t6: req_ajx_host+"globe/textures/icons/light/s-const.png",
				t7: req_ajx_host+"globe/textures/icons/light/s_icons_art.png",
				t8: req_ajx_host+"globe/textures/icons/light/s_icons_travel.png",
				t9: req_ajx_host+"globe/textures/icons/light/s_icons_startup.png",
				t10: req_ajx_host+"globe/textures/icons/light/s-sport.png",
				t11: req_ajx_host+"globe/textures/icons/light/s_icon_dream1-act.png",
				t12: req_ajx_host+"globe/textures/icons/light/s-clothes.png"},
			select:{
				t1: req_ajx_host+"globe/textures/icons/select/1.png",
				t2: req_ajx_host+"globe/textures/icons/select/2.png",
				t3: req_ajx_host+"globe/textures/icons/select/3.png",
				t4: req_ajx_host+"globe/textures/icons/select/s-intellect.png",
				t5: req_ajx_host+"globe/textures/icons/select/s-medicine.png",
				t6: req_ajx_host+"globe/textures/icons/select/s-const.png",
				t7: req_ajx_host+"globe/textures/icons/select/s_icons_art.png",
				t8: req_ajx_host+"globe/textures/icons/select/s_icons_travel.png",
				t9: req_ajx_host+"globe/textures/icons/select/s_icons_startup.png",
				t10: req_ajx_host+"globe/textures/icons/select/s-sport.png",
				t11: req_ajx_host+"globe/textures/icons/select/s_icon_dream1-act.png",
				t12: req_ajx_host+"globe/textures/icons/select/s-clothes.png"}
		},

		// проекты наверно лучше спрайтом показывать. На метео-сайте тоже спрайтами)
		sprite: true
	}


};

// коэффициенты, насколько высото над поверхностью шарика
GEOL.heightGeo = {
	user_help:1.001,
	user_main:1.002,
	user_circle:1.003,
	project:1.001,
	sphereOcean:0.994
};

// параметры луча
GEOL.paramPiramide = {

	color: 0x00ff00,
	alpha: 0.0
};

GEOL.nav = {rotate: 0.01, zoom:0.01};

GEOL.touchScale = 5;