
var GEOL = GEOL || {};

GEOL.navTimer = 0;

GEOL.navState = "";
GEOL.navTouchShort = true;



GEOL.navCallback = {

	_moveUp: function(){
		GEOL.navState = '_moveUp';
	},
	_moveDown: function(){
		GEOL.navState = '_moveDown';
	},
	_moveLeft: function(){
		GEOL.navState = '_moveLeft';
	},
	_moveRight: function(){
		GEOL.navState = '_moveRight';
	},
	_zoomIn: function(){
		GEOL.navState = '_zoomIn';
	},
	_zoomOut: function(){GEOL.navTouchLong = true;
		GEOL.navState = '_zoomOut';
	}
};



GEOL.callbackMouseUpNav = function(e){
	if 	(GEOL.navTimer)
	{
		clearInterval(GEOL.navTimer);
		GEOL.navTimer = 0;
	}

	GEOL.navTouchShort = true;

	//if (e.preventDefault)		e.preventDefault();
	//if (e.stopPropagation)		e.stopPropagation();
};

GEOL.navTimerCallback =function(){


	switch (GEOL.navState){
		case '_moveUp':
			GEOL.geocamControls.rotateUp  ( GEOL.visibleDistance /300 * GEOL.nav.rotate ) ;
			break;
		case '_moveDown':
			GEOL.geocamControls.rotateDown  ( GEOL.visibleDistance /300 * GEOL.nav.rotate) ;
			break;
		case '_moveLeft':

			GEOL.geocamControls.rotateLeft( GEOL.visibleDistance /300 * GEOL.nav.rotate );
			break;
		case '_moveRight':
			GEOL.geocamControls.rotateRight( GEOL.visibleDistance /300 * GEOL.nav.rotate );

				break;
		case '_zoomIn':
				GEOL.geocamControls.zoomIn( 1.01 ) ;
			break;
		case '_zoomOut':
			GEOL.geocamControls.zoomOut( 1.01 ) ;
			break;
		case '_fit':
			break;
	}

};

GEOL.navStartTimer = function(){
	GEOL.navTouchShort = false;

	if 	(GEOL.navTimer)
		clearInterval(GEOL.navTimer);
	
	GEOL.navTimer = setInterval(GEOL.navTimerCallback , 30);
};
GEOL.navigateInit = function(){



	var wrp = document.getElementById("wrapGEOL");

	if (!wrp)
	{
		console.log('#wrapGEOL not exists.');
		return;
	}


	this.navigationDOM = {};

	var navigationDivs = ['up', 'down', 'left', 'right', 'zoomIn', 'zoomOut'
		//, 'zoomExtends'
		 ];
	var navigationDivActions = {
		up:function(){
			GEOL.navState = '_moveUp';
			GEOL.navStartTimer();
		},
		down: function(){
			GEOL.navState = '_moveDown';
			GEOL.navStartTimer();
		},
		left: function(){
			GEOL.navState = '_moveLeft';
			GEOL.navStartTimer();
		},
		right: function(){
			GEOL.navState = '_moveRight';
			GEOL.navStartTimer();
		},
		zoomIn: function(){
			GEOL.navState = '_zoomIn';
			GEOL.navStartTimer();
		},
		zoomOut: function(){
			GEOL.navState = '_zoomOut';
			GEOL.navStartTimer();
		}

		//	'_moveUp':, '_moveDown', '_moveLeft',
		//'_moveRight', '_zoomIn', '_zoomOut'
		//, '_fit'
	};

	var navTouchEnd = {

		up: function(e){
//			alert('div');


			if (GEOL.navTouchShort )
			{
//				alert('up');
				GEOL.geocamControls.
					rotateUp  ( GEOL.visibleDistance /300 * GEOL.nav.rotate *25) ;

			}
			GEOL.navTouchShort = true;

			//if (e.preventDefault)				e.preventDefault();
			//if (e.stopPropagation)				e.stopPropagation();
		},
		down: function(){
			if (GEOL.navTouchShort )
				GEOL.geocamControls.rotateDown  ( GEOL.visibleDistance /300 * GEOL.nav.rotate *25) ;
			GEOL.navTouchShort = true;
		},
		left: function(){
			if (GEOL.navTouchShort )
				GEOL.geocamControls.rotateLeft( GEOL.visibleDistance /300 * GEOL.nav.rotate *25);
			GEOL.navTouchShort = true;
		},
		right: function(){
			if (GEOL.navTouchShort )
				GEOL.geocamControls.rotateRight( GEOL.visibleDistance /300 * GEOL.nav.rotate *25);
			GEOL.navTouchShort = true;
		},
		zoomIn: function(){
			if (GEOL.navTouchShort )
				GEOL.geocamControls.zoomIn( 1.1 ) ;
			GEOL.navTouchShort = true;
		},
		zoomOut: function(){GEOL.navTouchLong = true;
			if (GEOL.navTouchShort )
				GEOL.geocamControls.zoomOut( 1.1 ) ;
			GEOL.navTouchShort = true;
		}
	};

	this.navigationDOM['wrapper'] = document.createElement('div');
	this.navigationDOM['wrapper'].className = 'GL-navigation';

	this.DOM_container.appendChild(this.navigationDOM['wrapper']);

	//if (! GEOL.isTouchDevice)
	//GEOL.isTouchDevice = 'ontouchstart' in window && (navigator.maxTouchPoints || navigator.msMaxTouchPoints);

	var cls = 'GL-button ';

	for (var i = 0; i < navigationDivs.length; i++) {
		this.navigationDOM[navigationDivs[i]] = document.createElement('div');
		this.navigationDOM[navigationDivs[i]].className =  cls + 'GL-' + navigationDivs[i];
		this.navigationDOM['wrapper'].appendChild(this.navigationDOM[navigationDivs[i]]);

		this.navigationDOM[navigationDivs[i]].addEventListener( 'mousedown',
			navigationDivActions[navigationDivs[i]], false );

		//if (GEOL.isTouchDevice)
		{
			this.navigationDOM[navigationDivs[i]].addEventListener('touchend', navTouchEnd[navigationDivs[i]], false);
		}
	}


	document.addEventListener( 'mouseup', GEOL.callbackMouseUpNav, false );
	document.addEventListener('touchcancel', GEOL.callbackMouseUpNav, false);
	document.addEventListener('touchend', GEOL.callbackMouseUpNav, false);

//	document.addEventListener( 'mouseup', alert('mouseup'), false );
//	document.addEventListener('touchcancel', alert('touchcancel') , false);
//	document.addEventListener('touchend', alert('touchend'), false);

	document.addEventListener( 'touchend', GEOL.applyTouch, false );

	if(GEOL.isTouchDevice)
		GEOL.applyTouch();
};

GEOL.touchIsScaled = false;

GEOL.applyTouch = function(){
	if (GEOL.touchIsScaled )
	return;
	GEOL.touchIsScaled  = true;
	// параметры иконок

	GEOL.paramIcons.user_help.scale =
		GEOL.paramIcons.user_help.scale_touch ;
	GEOL.paramIcons.user_main.scale =
		GEOL.paramIcons.user_main.scale_touch;
	GEOL.paramIcons.project.scale =
		GEOL.paramIcons.project.scale_touch ;
	GEOL.paramIcons.project.scale_act =
		GEOL.paramIcons.project.scale_act_touch;


	GEOL.isTouchDevice = true;

	var elements = GEOL.DOM_container.getElementsByClassName('GL-button' ); // или:

	for (var i = 0; i < elements.length; i++) {
		elements[i].className += " touch";
	}

	GEOL.geocamControls.updateScaleState();

};
/*
var NavigationHandler = function () {
	function NavigationHandler(body, canvas) {
		var _this = this;

		_classCallCheck(this, NavigationHandler);

		this.body = body;
		this.canvas = canvas;

		this.iconsCreated = false;
		this.navigationHammers = [];
		this.boundFunctions = {};
		this.touchTime = 0;
		this.activated = false;

		this.body.emitter.on("activate", function () {
			_this.activated = true;_this.configureKeyboardBindings();
		});
		this.body.emitter.on("deactivate", function () {
			_this.activated = false;_this.configureKeyboardBindings();
		});
		this.body.emitter.on("destroy", function () {
			if (_this.keycharm !== undefined) {
				_this.keycharm.destroy();
			}
		});

		this.options = {};
	}

	_createClass(NavigationHandler, [{
		key: 'setOptions',
		value: function setOptions(options) {
			if (options !== undefined) {
				this.options = options;
				this.create();
			}
		}
	}, {
		key: 'create',
		value: function create() {
			if (this.options.navigationButtons === true) {
				if (this.iconsCreated === false) {
					this.loadNavigationElements();
				}
			} else if (this.iconsCreated === true) {
				this.cleanNavigation();
			}

			this.configureKeyboardBindings();
		}
	}, {
		key: 'cleanNavigation',
		value: function cleanNavigation() {
			// clean hammer bindings
			if (this.navigationHammers.length != 0) {
				for (var i = 0; i < this.navigationHammers.length; i++) {
					this.navigationHammers[i].destroy();
				}
				this.navigationHammers = [];
			}

			// clean up previous navigation items
			if (this.navigationDOM && this.navigationDOM['wrapper'] && this.navigationDOM['wrapper'].parentNode) {
				this.navigationDOM['wrapper'].parentNode.removeChild(this.navigationDOM['wrapper']);
			}

			this.iconsCreated = false;
		}

		/**
		 * Creation of the navigation controls nodes. They are drawn over the rest of the nodes and are not affected by scale and translation
		 * they have a triggerFunction which is called on click. If the position of the navigation controls is dependent
		 * on this.frame.canvas.clientWidth or this.frame.canvas.clientHeight, we flag horizontalAlignLeft and verticalAlignTop false.
		 * This means that the location will be corrected by the _relocateNavigation function on a size change of the canvas.
		 *
		 * @private


	}, {
		key: 'loadNavigationElements',
		value: function loadNavigationElements() {
			var _this2 = this;

			this.cleanNavigation();

			this.navigationDOM = {};
			var navigationDivs = ['up', 'down', 'left', 'right', 'zoomIn', 'zoomOut', 'zoomExtends'];
			var navigationDivActions = ['_moveUp', '_moveDown', '_moveLeft', '_moveRight', '_zoomIn', '_zoomOut', '_fit'];

			this.navigationDOM['wrapper'] = document.createElement('div');
			this.navigationDOM['wrapper'].className = 'vis-navigation';
			this.canvas.frame.appendChild(this.navigationDOM['wrapper']);

			for (var i = 0; i < navigationDivs.length; i++) {
				this.navigationDOM[navigationDivs[i]] = document.createElement('div');
				this.navigationDOM[navigationDivs[i]].className = 'vis-button vis-' + navigationDivs[i];
				this.navigationDOM['wrapper'].appendChild(this.navigationDOM[navigationDivs[i]]);

				var hammer = new Hammer(this.navigationDOM[navigationDivs[i]]);
				if (navigationDivActions[i] === "_fit") {
					hammerUtil.onTouch(hammer, this._fit.bind(this));
				} else {
					hammerUtil.onTouch(hammer, this.bindToRedraw.bind(this, navigationDivActions[i]));
				}

				this.navigationHammers.push(hammer);
			}

			// use a hammer for the release so we do not require the one used in the rest of the network
			// the one the rest uses can be overloaded by the manipulation system.
			var hammerFrame = new Hammer(this.canvas.frame);
			hammerUtil.onRelease(hammerFrame, function () {
				_this2._stopMovement();
			});
			this.navigationHammers.push(hammerFrame);

			this.iconsCreated = true;
		}
	}, {
		key: 'bindToRedraw',
		value: function bindToRedraw(action) {
			if (this.boundFunctions[action] === undefined) {
				this.boundFunctions[action] = this[action].bind(this);
				this.body.emitter.on("initRedraw", this.boundFunctions[action]);
				this.body.emitter.emit("_startRendering");
			}
		}
	}, {
		key: 'unbindFromRedraw',
		value: function unbindFromRedraw(action) {
			if (this.boundFunctions[action] !== undefined) {
				this.body.emitter.off("initRedraw", this.boundFunctions[action]);
				this.body.emitter.emit("_stopRendering");
				delete this.boundFunctions[action];
			}
		}

		/**
		 * this stops all movement induced by the navigation buttons
		 *
		 * @private


	}, {
		key: '_fit',
		value: function _fit() {
			if (new Date().valueOf() - this.touchTime > 700) {
				// TODO: fix ugly hack to avoid hammer's double fireing of event (because we use release?)
				this.body.emitter.emit("fit", { duration: 700 });
				this.touchTime = new Date().valueOf();
			}
		}

		/**
		 * this stops all movement induced by the navigation buttons
		 *
		 * @private


	}, {
		key: '_stopMovement',
		value: function _stopMovement() {
			for (var boundAction in this.boundFunctions) {
				if (this.boundFunctions.hasOwnProperty(boundAction)) {
					this.body.emitter.off("initRedraw", this.boundFunctions[boundAction]);
					this.body.emitter.emit("_stopRendering");
				}
			}
			this.boundFunctions = {};
		}
	}, {
		key: '_moveUp',
		value: function _moveUp() {
			this.body.view.translation.y += this.options.keyboard.speed.y;
		}
	}, {
		key: '_moveDown',
		value: function _moveDown() {
			this.body.view.translation.y -= this.options.keyboard.speed.y;
		}
	}, {
		key: '_moveLeft',
		value: function _moveLeft() {
			this.body.view.translation.x += this.options.keyboard.speed.x;
		}
	}, {
		key: '_moveRight',
		value: function _moveRight() {
			this.body.view.translation.x -= this.options.keyboard.speed.x;
		}
	}, {
		key: '_zoomIn',
		value: function _zoomIn() {
			this.body.view.scale *= 1 + this.options.keyboard.speed.zoom;
			this.body.emitter.emit('zoom', { direction: '+', scale: this.body.view.scale });
		}
	}, {
		key: '_zoomOut',
		value: function _zoomOut() {
			this.body.view.scale /= 1 + this.options.keyboard.speed.zoom;
			this.body.emitter.emit('zoom', { direction: '-', scale: this.body.view.scale });
		}

		/**
		 * bind all keys using keycharm.


	}, {
		key: 'configureKeyboardBindings',
		value: function configureKeyboardBindings() {
			var _this3 = this;

			if (this.keycharm !== undefined) {
				this.keycharm.destroy();
			}

			if (this.options.keyboard.enabled === true) {
				if (this.options.keyboard.bindToWindow === true) {
					this.keycharm = keycharm({ container: window, preventDefault: true });
				} else {
					this.keycharm = keycharm({ container: this.canvas.frame, preventDefault: true });
				}

				this.keycharm.reset();

				if (this.activated === true) {
					this.keycharm.bind("up", function () {
						_this3.bindToRedraw("_moveUp");
					}, "keydown");
					this.keycharm.bind("down", function () {
						_this3.bindToRedraw("_moveDown");
					}, "keydown");
					this.keycharm.bind("left", function () {
						_this3.bindToRedraw("_moveLeft");
					}, "keydown");
					this.keycharm.bind("right", function () {
						_this3.bindToRedraw("_moveRight");
					}, "keydown");
					this.keycharm.bind("=", function () {
						_this3.bindToRedraw("_zoomIn");
					}, "keydown");
					this.keycharm.bind("num+", function () {
						_this3.bindToRedraw("_zoomIn");
					}, "keydown");
					this.keycharm.bind("num-", function () {
						_this3.bindToRedraw("_zoomOut");
					}, "keydown");
					this.keycharm.bind("-", function () {
						_this3.bindToRedraw("_zoomOut");
					}, "keydown");
					this.keycharm.bind("[", function () {
						_this3.bindToRedraw("_zoomOut");
					}, "keydown");
					this.keycharm.bind("]", function () {
						_this3.bindToRedraw("_zoomIn");
					}, "keydown");
					this.keycharm.bind("pageup", function () {
						_this3.bindToRedraw("_zoomIn");
					}, "keydown");
					this.keycharm.bind("pagedown", function () {
						_this3.bindToRedraw("_zoomOut");
					}, "keydown");

					this.keycharm.bind("up", function () {
						_this3.unbindFromRedraw("_moveUp");
					}, "keyup");
					this.keycharm.bind("down", function () {
						_this3.unbindFromRedraw("_moveDown");
					}, "keyup");
					this.keycharm.bind("left", function () {
						_this3.unbindFromRedraw("_moveLeft");
					}, "keyup");
					this.keycharm.bind("right", function () {
						_this3.unbindFromRedraw("_moveRight");
					}, "keyup");
					this.keycharm.bind("=", function () {
						_this3.unbindFromRedraw("_zoomIn");
					}, "keyup");
					this.keycharm.bind("num+", function () {
						_this3.unbindFromRedraw("_zoomIn");
					}, "keyup");
					this.keycharm.bind("num-", function () {
						_this3.unbindFromRedraw("_zoomOut");
					}, "keyup");
					this.keycharm.bind("-", function () {
						_this3.unbindFromRedraw("_zoomOut");
					}, "keyup");
					this.keycharm.bind("[", function () {
						_this3.unbindFromRedraw("_zoomOut");
					}, "keyup");
					this.keycharm.bind("]", function () {
						_this3.unbindFromRedraw("_zoomIn");
					}, "keyup");
					this.keycharm.bind("pageup", function () {
						_this3.unbindFromRedraw("_zoomIn");
					}, "keyup");
					this.keycharm.bind("pagedown", function () {
						_this3.unbindFromRedraw("_zoomOut");
					}, "keyup");
				}
			}
		}
	}]);

	return NavigationHandler;
}();


*/