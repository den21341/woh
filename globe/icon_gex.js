/**
 * Created by Morss on 01.10.16.
 */


var GEOL = GEOL || {};

GEOL.createGeometryGex = function (sliceCount){



	var gm = new THREE.Geometry();


	var vrt = [], fcs = [];
	vrt.push(new THREE.Vector3(0,0,0.02));
	var uv = [];


	for (var i = 0; i< sliceCount; i++){
		vrt.push(new THREE.Vector3(
			Math.cos(i* Math.PI*2/sliceCount),
			Math.sin(i* Math.PI*2/sliceCount),
			0
		));

	}





	for (var i = 2; i< (sliceCount+1); i++){
		var fc = new THREE.Face3(i-1, i,0);
		fcs.push(fc);

		uv.push([
			new THREE.Vector2(
				vrt[i-1].x/2 +0.5, vrt[i-1].y/2 + 0.5
			),
			new THREE.Vector2(
				vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5

			),
			new THREE.Vector2(	0.5,  0.5	)
		]);

		if (i == sliceCount)
		{
			var fc = new THREE.Face3(i, 1,0);
			fcs.push(fc);

			uv.push([
				new THREE.Vector2(
					vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5
				),
				new THREE.Vector2(
					vrt[1].x/2 +0.5, vrt[1].y/2 + 0.5
				),
				new THREE.Vector2(	0.5,  0.5	)
			]);
		}
	}

	gm.vertices = vrt;
	gm.faces = fcs;
	gm.faceVertexUvs[0] = uv;

	return gm;
}