/**
 * Created by Morss on 01.10.16.
 */




//getRotationFromMatrix(object.matrix, object.scale) -> (object.matrix) and multiplySelf -> multiply. r71 � PierreDuc Jul 23 '15 at 13:39

// Rotate an object around an arbitrary axis in world space
function rotateAroundWorldAxis(object, axis, radians) {
	var rotWorldMatrix = new THREE.Matrix4();
	rotWorldMatrix.makeRotationAxis(axis.normalize(), radians);
	rotWorldMatrix.multiply(object.matrix);        // pre-multiply
	object.matrix = rotWorldMatrix;
	object.rotation.setFromRotationMatrix(object.matrix, object.scale);
}

var Help_User = function (data){
	this.data = data;

	var  me = this;

	this.canvas = document.createElement("canvas");
	this.canvas.id = "ava_" +data.gid;
	document.getElementById("body").appendChild(this.canvas  );

	me.canvas.width = 	me.canvas.height = 128;

	this.ctx  = me.canvas.getContext('2d');


	var sz = 120;


	//this.ctx.strokeStyle="rgb(123,255,0)";
//	this.ctx.lineWidth = 6;
//	this.ctx.beginPath();
//	this.ctx.arc(sz/2+4, sz/2+4, sz/2+1, 0, Math.PI * 2);
//	this.ctx.stroke();


	this.texture = new THREE.Texture(this.canvas );


	this.texture.needsUpdate = true;

	//this.geom = new THREE.PlaneGeometry(5, 5, 2,2);




	this.geom = new THREE.Geometry();


	var vrt = [], fcs = [];
	vrt.push(new THREE.Vector3(0,0,0.2));
	var uv = [];
	this.cntBdr = 6;

	for (var i = 0; i< this.cntBdr; i++){
		vrt.push(new THREE.Vector3(
			Math.cos(i* Math.PI*2/this.cntBdr),
			Math.sin(i* Math.PI*2/this.cntBdr),
			0
		));
	}

	for (var i = 2; i< (this.cntBdr+1); i++){
		var fc = new THREE.Face3(i-1, i,0);
		fcs.push(fc);

		//uv.push(new THREE.Vector2(vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5));


		uv.push([
			new THREE.Vector2(
				vrt[i-1].x/2 +0.5, vrt[i-1].y/2 + 0.5
			),
			new THREE.Vector2(
				vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5

			),
			new THREE.Vector2(	0.5,  0.5	)
		]);

		if (i == this.cntBdr)
		{
			var fc = new THREE.Face3(i, 1,0);
			fcs.push(fc);

			//uv.push(new THREE.Vector2(vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5));

			uv.push([
				new THREE.Vector2(
					vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5
				),
				new THREE.Vector2(
					vrt[1].x/2 +0.5, vrt[1].y/2 + 0.5
				),
				new THREE.Vector2(	0.5,  0.5	)
			]);
		}
	}

	this.geom.vertices = vrt;
	this.geom.faces = fcs;
	this.geom.faceVertexUvs[0] = uv;

	this.geom.verticesNeedUpdate =this.geom.uvsNeedUpdate = true;


	this.mat  = new THREE.MeshBasicMaterial({
//		side:THREE.DoubleSide,
		transparent:true,
		map: this.texture });

	this.mesh = new THREE.Mesh( this.geom , this.mat  );

//	this.mesh.rotateZ(deg2rad(-data.info.lat));

	this.mesh.rotateY(-Math.PI/2 + deg2rad(data.info.lng));

	this.mesh.rotateX(deg2rad(-data.info.lat));

	//var v1 = new THREE.Vector3(0,0,1);
	////rotateAroundWorldAxis(this.mesh, v1, deg2rad(-90));
	//v1.set(0,1, 0);
	//rotateAroundWorldAxis(this.mesh, v1, -Math.PI/2 + deg2rad(data.info.lng));

//	this.mesh.rotateZ();


	this.mesh.translateZ(40.3);
	this.mesh.objType = "usr_help";
	this.mesh.scale.set(0.5,0.5,0.5);
	this.mesh.objParent = this;


	var ava = new Image();
	ava.onload = function(){


		var szMin = Math.min( this.width,  this.height	);
		var sz = 120;



		me.ctx.save();
		me.ctx.beginPath();
		me.ctx.arc(sz/2+4, sz/2+4, sz/2, 0, Math.PI * 2, true);
		me.ctx.closePath();
		me.ctx.clip();
		//Clip the image and position the clipped part on the canvas:
		//	JavaScript syntax: 	context.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);

		//me.ctx.drawImage(this, (this.width - sz)/2, (this.height - sz)/2, sz, sz, 0,0,50,50);
		me.ctx.drawImage(this, (this.width - szMin)/2, (this.height - szMin)/2, szMin, szMin, 5,5,sz,sz);

		/*me.ctx.beginPath();
		 me.ctx.arc(0, 0, 25, 0, Math.PI * 2, true);
		 me.ctx.clip();
		 me.ctx.closePath();
		 */
		me.ctx.restore();

		var rad = 62;
		var cntr = me.canvas.width / 2;
		me.ctx.lineJoin="round";

		me.ctx.strokeStyle="rgba(255,255,255, 0.95)";
		me.ctx.lineWidth = 6;
		me.ctx.beginPath();
		me.ctx.moveTo(cntr+rad, cntr);

		for (var i = 0; i< me.cntBdr; i++){
			me.ctx.lineTo(cntr+rad * Math.cos(i* Math.PI*2/me.cntBdr),
				cntr+rad * Math.sin(i* Math.PI*2/me.cntBdr))		;
		}
		me.ctx.lineTo(cntr+rad, cntr);
		me.ctx.stroke();


		me.texture.needsUpdate = true;
		//	me.mesh.material.needsUpdate = true;

	}

	ava.src = data.info.url_ava;

}

var GEOL = GEOL || {};

GEOL.Help_User = function (data){
	this.graphInfo = data;
	this.center_ll = {lat: data.info.lat,lng: data.info.lng};


	var  me = this;

	this.canvas = document.createElement("canvas");
	this.canvas.id = "ava_" +data.gid;
	document.getElementById("body").appendChild(this.canvas  );

	me.txtSz = 128;
	me.canvas.width = 	me.canvas.height = me.txtSz;

	this.ctx  = me.canvas.getContext('2d');





	//this.ctx.strokeStyle="rgb(123,255,0)";
//	this.ctx.lineWidth = 6;
//	this.ctx.beginPath();
//	this.ctx.arc(sz/2+4, sz/2+4, sz/2+1, 0, Math.PI * 2);
//	this.ctx.stroke();


	this.texture = new THREE.Texture(this.canvas );


	this.texture.needsUpdate = true;

	//this.geom = new THREE.PlaneGeometry(5, 5, 2,2);




	this.geom = new THREE.Geometry();


	var vrt = [], fcs = [];
	vrt.push(new THREE.Vector3(0,0,0.2));
	var uv = [];
	this.cntBdr = 6;

	for (var i = 0; i< this.cntBdr; i++){
		vrt.push(new THREE.Vector3(
			Math.cos(i* Math.PI*2/this.cntBdr),
			Math.sin(i* Math.PI*2/this.cntBdr),
			0
		));
	}

	for (var i = 2; i< (this.cntBdr+1); i++){
		var fc = new THREE.Face3(i-1, i,0);
		fcs.push(fc);

		//uv.push(new THREE.Vector2(vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5));


		uv.push([
			new THREE.Vector2(
				vrt[i-1].x/2 +0.5, vrt[i-1].y/2 + 0.5
			),
			new THREE.Vector2(
				vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5

			),
			new THREE.Vector2(	0.5,  0.5	)
		]);

		if (i == this.cntBdr)
		{
			var fc = new THREE.Face3(i, 1,0);
			fcs.push(fc);

			//uv.push(new THREE.Vector2(vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5));

			uv.push([
				new THREE.Vector2(
					vrt[i].x/2 +0.5, vrt[i].y/2 + 0.5
				),
				new THREE.Vector2(
					vrt[1].x/2 +0.5, vrt[1].y/2 + 0.5
				),
				new THREE.Vector2(	0.5,  0.5	)
			]);
		}
	}

	this.geom.vertices = vrt;
	this.geom.faces = fcs;
	this.geom.faceVertexUvs[0] = uv;

	this.geom.verticesNeedUpdate =this.geom.uvsNeedUpdate = true;


	if (GEOL.paramIcons.user_main.sprite)
	{
		this.mat   = new THREE.SpriteMaterial( {
			map: this.texture,
			transparent:true,
			depthWrite:false
			 } );
		this.mesh  = new THREE.Sprite( this.mat );
	}
	else
	{
		this.mat  = new THREE.MeshBasicMaterial({
//		side:THREE.DoubleSide,
			transparent:true,
			map: this.texture,
			depthWrite:false
		});

		this.mesh = new THREE.Mesh( this.geom , this.mat  );
	}





//	this.mesh.rotateZ(deg2rad(-data.info.lat));

	this.mesh.rotateY(-Math.PI/2 + deg2rad(data.info.lng));

	this.mesh.rotateX(deg2rad(-data.info.lat));

	//var v1 = new THREE.Vector3(0,0,1);
	////rotateAroundWorldAxis(this.mesh, v1, deg2rad(-90));
	//v1.set(0,1, 0);
	//rotateAroundWorldAxis(this.mesh, v1, -Math.PI/2 + deg2rad(data.info.lng));

//	this.mesh.rotateZ();


	this.mesh.translateZ(40.4);
	this.mesh.objType = "usr_help";
	this.mesh.name = "usr_help - " + this.graphInfo.gid;

	var sc = GEOL.paramIcons.user_help.scale;
	this.mesh.scale.set(sc, sc, sc);

	this.mesh.objParent = this;

	this.mesh.renderOrder = (GEOL.renderOrderUser+=3);








	this.stateSelect = false;
	this.stateHighlight = false;

	this.highlightOn =function (){
		this.stateHighlight = true;

	};
	this.highlightOff = function (){
		this.stateHighlight = false;

	};

	this.selectOn = function (){
		GEOL.geocamControls.locate_ll(this.center_ll);
		GEOL.piramide.setCenter(this.center_ll);
		GEOL.auraIcon.showUnder(this.mesh);
		this.stateSelect = true;

	};
	this.selectOff = function(){
		GEOL.containerInfo.innerHTML = '';
		GEOL.auraIcon.hide();
		this.stateSelect = false;

	};

	this.updateSelection = function(){

	};






	var ava = new Image();
	ava.onload = function(){
	//	ava.onerror = function(){


		var szMin = Math.min( this.width,  this.height	);
		var sz = me.txtSz - 28;



		me.ctx.save();
		me.ctx.beginPath();
		me.ctx.arc(me.txtSz/2, me.txtSz/2, sz/2, 0, Math.PI * 2, true);
		me.ctx.closePath();

		me.ctx.clip();
		//Clip the image and position the clipped part on the canvas:
		//	JavaScript syntax: 	context.drawImage(img,sx,sy,swidth,sheight,x,y,width,height);

		//me.ctx.drawImage(this, (this.width - sz)/2, (this.height - sz)/2, sz, sz, 0,0,50,50);
		me.ctx.drawImage(this, (this.width - szMin)/2, (this.height - szMin)/2, szMin, szMin,
			(me.txtSz - sz)/2,(me.txtSz - sz)/2,sz,sz);

		/*me.ctx.beginPath();
		 me.ctx.arc(0, 0, 25, 0, Math.PI * 2, true);
		 me.ctx.clip();
		 me.ctx.closePath();
		 */
		me.ctx.restore();

		me.ctx.beginPath();
		me.ctx.arc(me.txtSz/2, me.txtSz/2, sz/2, 0, Math.PI * 2, true);

		me.ctx.strokeStyle="rgba(255,255,255, 0.95)";
		me.ctx.lineWidth = 6;

		/*
		 var rad = 62;
		 var cntr = me.canvas.width / 2;
		 me.ctx.lineJoin="round";

		 me.ctx.strokeStyle="rgba(255,255,255, 0.95)";
		 me.ctx.lineWidth = 6;
		 me.ctx.beginPath();
		 me.ctx.moveTo(cntr+rad, cntr);

		 for (var i = 0; i< me.cntBdr; i++){
		 me.ctx.lineTo(cntr+rad * Math.cos(i* Math.PI*2/me.cntBdr),
		 cntr+rad * Math.sin(i* Math.PI*2/me.cntBdr))		;
		 }
		 me.ctx.lineTo(cntr+rad, cntr);
		 */
		me.ctx.stroke();


		me.texture.needsUpdate = true;

	}

	ava.onerror = function(){
	//	ava.onload = function(){



			/*	var szMin = Math.min( this.width,  this.height	);
				var sz = 120;



				var rad = 62;
				var cntr = me.canvas.width / 2;
				me.ctx.lineJoin="round";

				me.ctx.strokeStyle="rgba(255,255,255, 0.95)";
				me.ctx.lineWidth = 6;
				me.ctx.beginPath();
				me.ctx.moveTo(cntr+rad, cntr);

				for (var i = 0; i< me.cntBdr; i++){
					me.ctx.lineTo(cntr+rad * Math.cos(i* Math.PI*2/me.cntBdr),
						cntr+rad * Math.sin(i* Math.PI*2/me.cntBdr))		;
				}
				me.ctx.lineTo(cntr+rad, cntr);
			 me.ctx.fillStyle = "red";
			 me.ctx.fill();
				me.ctx.stroke();


				me.texture.needsUpdate = true;
		*/




		var sz = me.txtSz - 28;




		me.ctx.beginPath();
		me.ctx.arc(me.txtSz/2, me.txtSz/2, sz/2, 0, Math.PI * 2, true);

		me.ctx.strokeStyle="rgba(255,255,255, 0.95)";
		me.ctx.lineWidth = 6;

		me.ctx.fillStyle = "red";
		me.ctx.fill();
		/*
		 var rad = 62;
		 var cntr = me.canvas.width / 2;
		 me.ctx.lineJoin="round";

		 me.ctx.strokeStyle="rgba(255,255,255, 0.95)";
		 me.ctx.lineWidth = 6;
		 me.ctx.beginPath();
		 me.ctx.moveTo(cntr+rad, cntr);

		 for (var i = 0; i< me.cntBdr; i++){
		 me.ctx.lineTo(cntr+rad * Math.cos(i* Math.PI*2/me.cntBdr),
		 cntr+rad * Math.sin(i* Math.PI*2/me.cntBdr))		;
		 }
		 me.ctx.lineTo(cntr+rad, cntr);
		 */
		me.ctx.stroke();


		me.texture.needsUpdate = true;

	}
	ava.src = data.info.url_ava;

}