



var GEOL = GEOL || {};

GEOL.Piramide = function () {


	var shaderFragmentPiramide =
		"varying vec4 vColor;\n" +
			"void main() {\n" +
			"gl_FragColor = vColor;\n" +
			"}\n" 	;


	var shaderVertexPiramide =
		"\n" +
			"	uniform	vec2 posLeftTop;\n" +
			"	uniform	vec2 posLeftBottom;\n" +
			"	uniform	vec2 posRightTop;\n" +
			"	uniform	vec2 posRightBottom;\n" +
			"\n" +
			"	uniform vec3 colorBase;\n" +
			"	uniform	float	alphaCenter;\n" +
			"	uniform	float	alphaCap;\n" +
			"\n" +
			"\n" +
			"	uniform	float	visibleDistance;\n" +
			"	uniform	float	opaqueDistance;\n" +
			"\n" +
			"	varying vec4 vColor;  // 'varying' vars are passed to the fragment shader\n" +
			"\n" +
			"\n" +
			"	void main() {\n" +
			"\n" +
			"		 float alph = alphaCenter;\n" +
			"		 vec3 c;\n" +
			"		 if (color.z > 0.5)\n" +
			"		 {\n" +
			"			gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );\n" +
			"		 }\n" +
			"		 else\n" +
			"		 {\n" +
			"			 if (color.x > 0.5)\n" +
			"			 {\n" +
			"				 if (color.y > 0.5)\n" +
			"				 	gl_Position  =  vec4(posRightTop, 0.0, 1.0 );\n" +
			"				 else\n" +
			"				 	gl_Position  =  vec4(posRightBottom, 0.0, 1.0 );\n" +
			"			 }\n" +
			"			 else\n" +
			"			 {\n" +
			"				 if (color.y > 0.5)\n" +
			"					 gl_Position  =  vec4(posLeftTop, 0.0, 1.0 );\n" +
			"				 else\n" +
			"					 gl_Position  =  vec4(posLeftBottom, 0.0, 1.0 );\n" +
			"			 }\n" +
			"		 }\n" +
			"\n" +
			"		 vColor = vec4(colorBase, alph);\n" +
			"	}\n"

/*	`

	uniform	vec2 posLeftTop;
	uniform	vec2 posLeftBottom;
	uniform	vec2 posRightTop;
	uniform	vec2 posRightBottom;

	uniform vec3 colorBase;
	uniform	float	alphaCenter;
	uniform	float	alphaCap;


	uniform	float	visibleDistance;
	uniform	float	opaqueDistance;

	varying vec4 vColor;  // 'varying' vars are passed to the fragment shader


	void main() {

		 float alph = alphaCenter;
		 vec3 c;
		 if (color.z > 0.5)
		 {
			gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );
		 }
		 else
		 {
			 if (color.x > 0.5)
			 {
				 if (color.y > 0.5)
				 	gl_Position  =  vec4(posRightTop, 0.0, 1.0 );
				 else
				 	gl_Position  =  vec4(posRightBottom, 0.0, 1.0 );
			 }
			 else
			 {
				 if (color.y > 0.5)
					 gl_Position  =  vec4(posLeftTop, 0.0, 1.0 );
				 else
					 gl_Position  =  vec4(posLeftBottom, 0.0, 1.0 );
			 }
		 }

		 vColor = vec4(colorBase, alph);
	}
`
*/;


var arClr = [
	new THREE.Color(0,0,1),
	new THREE.Color(0,1,0),
	new THREE.Color(1,1,0),
	new THREE.Color(1,0,0),
	new THREE.Color(0,0,0)
	];



	var geomPir = new THREE.Geometry();
	geomPir.vertices = [
		new THREE.Vector3(0,0,0),
		new THREE.Vector3(20,40,50),
		new THREE.Vector3(10,60,20),
		new THREE.Vector3(5,14,50),
		new THREE.Vector3(220,50,70),
		new THREE.Vector3(2320,50,10)
	],
	geomPir.faces = [
		new THREE.Face3(0, 2, 1),
		new THREE.Face3(0, 2, 3),
		new THREE.Face3(0, 3, 4),
		new THREE.Face3(0, 4, 1)
	];

	var abc = ["a", "b", "c"];
	for (var z = 0; z < 4; z++){
		for (var y = 0; y < 3; y++){
//			geomPir.faces[z].vertexColors[y] = arClr[
//				geomPir.faces[z][abc[y]]];
			geomPir.faces[z].vertexColors[y] = arClr[
				geomPir.faces[z][abc[y]]];
		}
	}
	//geomPir.faces.length = 2;

//	geomCap.faceVertexUvs[0] = uv ;

	//geomCap.computeFaceNormals();
	//geomCap.computeVertexNormals();

	var matPir = new THREE.MeshBasicMaterial({
//	var matCap = new THREE.MeshBasicMaterial({
		color: 0x0000ff //,
		//, opacity: 0.5
		, transparent: true
		, side:THREE.DoubleSide
//		,	map: this.txtContur

	});

	geomPir.dynamic = true

	this.setCenter = function(ll) {
		this.ll = ll;
		var v = new THREE.Vector3(-1, 0, 0);

		var axeZ = new THREE.Vector3( 0, 0, 1 );
		var angLat = -deg2rad(ll.lat);

		v.applyAxisAngle( axeZ, angLat );

		var axeY = new THREE.Vector3( 0, 1, 0 );
		var angLng = deg2rad(ll.lng);

		v.applyAxisAngle( axeY, angLng );

		v.multiplyScalar(GEOL.BASE_RAD);

		for (var i = this.meshPir.geometry.vertices.length; i--;)
		//	this.meshPir.geometry.vertices[i].set(v.x,v.y,v.z);

		this.meshPir.geometry.vertices[0].set(v.x,v.y,v.z);

		this.meshPir.needsUpdate = true;
		this.meshPir.geometry.needsUpdate = true;
		this.meshPir.geometry.verticesNeedUpdate = true;
		this.meshPir.verticesNeedUpdate = true;

		//this.meshPir.visible = true;

		this.updateGeometry();
	} ;




	function getOffset(el) {
		el = el.getBoundingClientRect();
		return {
			left: el.left + window.scrollX,
			top: el.top + window.scrollY
		}
	}

	this.updateGeometry = function(){

		//return;

		var divInf = document.getElementById("GL-info-wrap");
		var divGL = document.getElementById("GL-output");
		var rcInf = divInf.getBoundingClientRect();
		var rcGL = divGL.getBoundingClientRect();


		var width = divInf.clientWidth / rcGL.width *2;
		var height = divInf.clientHeight / rcGL.height *2;


		var left = ((divInf.offsetLeft ) / GEOL.GL_container.clientWidth) * 2 - 1;
		var top = - ( divInf.offsetTop / GEOL.GL_container.clientHeight ) * 2 + 1;


		this.uniPir.posLeftTop.value.set(left, top);
		this.uniPir.posLeftBottom.value.set(left, top - height);
		this.uniPir.posRightTop.value.set(left + width, top);
		this.uniPir.posRightBottom.value.set(left + width, top - height);

		this.uniPir.posLeftTop.needsUpdate = true;
		this.uniPir.posLeftBottom.needsUpdate = true;
		this.uniPir.posRightTop.needsUpdate = true;
		this.uniPir.posRightBottom.needsUpdate = true;

		/*
		 bottom		 :		 308
		 height		 :		 200
		 left		 :		 642
		 right		 :		 842
		 top		 :		 108
		 width		 :		 200
		 */
	};
	this.uniPir = {
		posLeftTop: { type: "2f", value: new THREE.Vector2(-1, 0.60) },
		posLeftBottom: { type: "2f", value: new THREE.Vector2(0.10, 0.200) },
		posRightTop: { type: "2f", value: new THREE.Vector2(1.0, 1.0) },
		posRightBottom: { type: "2f", value: new THREE.Vector2(0.60, 0.200) },

		colorBase: { type: "c", value: new THREE.Color(GEOL.paramPiramide.color) },
		alphaCenter: { type: "f", value: GEOL.paramPiramide.alpha },
		alphaCap: { type: "f", value: 0.3 },
		opaqueDistance: { type: "f", value: 10.0 },
		visibleDistance: { type: "f", value: 10.0 }

	};

	this.updateVisible = function(){

		var dst = GEOL.camera.position.length();
		var sinBAO = GEOL.BASE_RAD / dst;
		var CO = GEOL.BASE_RAD * sinBAO;
		var AC = dst - CO;
		var cosBAO = Math.sqrt(1 - Math.pow(sinBAO, 2));


		var BC = cosBAO * GEOL.BASE_RAD;

		var LC = BC * 0.9;

		var cosMCL = LC / GEOL.BASE_RAD ;
		var sinMCL = Math.sqrt(1 - Math.pow(cosMCL, 2));

		var ML = sinMCL * GEOL.BASE_RAD;

		this.uniPir.visibleDistance.value = AC;
		this.uniPir.visibleDistance.needsUpdate = true;


		this.uniPir.opaqueDistance.value = AC - ML;
		this.uniPir.opaqueDistance.needsUpdate = true;


		var v = GEOL.camera.position;
		for (var i = this.meshPir.geometry.vertices.length; --i;)
			this.meshPir.geometry.vertices[i].set(v.x,v.y,v.z);



		this.ll = this.ll || {};

		v = new THREE.Vector3(-1, 0, 0);

		var axeZ = new THREE.Vector3( 0, 0, 1 );
		var angLat = -deg2rad(this.ll.lat);

		v.applyAxisAngle( axeZ, angLat );

		var axeY = new THREE.Vector3( 0, 1, 0 );
		var angLng = deg2rad(this.ll.lng);

		v.applyAxisAngle( axeY, angLng );

		v.multiplyScalar(GEOL.BASE_RAD);

	//	for (var i = this.meshPir.geometry.vertices.length; i--;)
			//	this.meshPir.geometry.vertices[i].set(v.x,v.y,v.z);

			this.meshPir.geometry.vertices[0].set(v.x,v.y,v.z);








		var m = new THREE.Matrix4();


	//	m.multiplyMatrices(GEOL.camera.projectionMatrix,
	//		GEOL.camera.matrixWorld);
		m.multiplyMatrices(m, this.meshPir.matrixWorld);



		v.applyMatrix4(GEOL.camera.matrixWorldInverse);

	//	console.log(" v x " + (v.x));
//		console.log(" v y " + (v.y));
//		console.log(" v z " + (v.z));

		var dist = -v.z;

		var visDist = AC;
		var opqDist = AC - ML;

		var alph = GEOL.paramPiramide.alpha;


		if (dist > visDist)
		alph = 0;
		else
		if (dist > opqDist)
		{
			var hgtTrnsp = visDist - opqDist;
			alph = alph*(
				(hgtTrnsp - (dist - opqDist)) / (hgtTrnsp)
					);
		}

		this.uniPir.alphaCenter.value = alph;
		this.uniPir.alphaCenter.needsUpdate = true;
	}

	this.materialPiramide = new THREE.ShaderMaterial({

		vertexColors: THREE.VertexColors,
		transparent:true,
		uniforms: this.uniPir,
		vertexShader: shaderVertexPiramide,
		fragmentShader: shaderFragmentPiramide
		, side:THREE.DoubleSide
		, depthWrite: false
		, depthTest: false
		//, blendSrc: THREE.DstAlphaFactor
		, blendSrc: THREE.OneFactor

	});

	this.meshPir  = new THREE.Mesh(geomPir, this.materialPiramide);
	this.meshPir.visible = false;
	this.meshPir.objParent = this;
	this.meshPir.objType  = "piramidePick";
	this.meshPir.name = "piramide Pick";


	this.meshPir.renderOrder = GEOL.renderOrderPiramide;

	GEOL.piramide = this;

	GEOL.sceneOver.add(this.meshPir);
	//GEOL.scene.add(this.meshPir);


};