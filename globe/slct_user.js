
var GEOL = GEOL || {};

GEOL.CircleUser = function (nm){

	//this.center_ll = {lat: ll.lat,lng: ll.lng};


	var  me = this;

	this.texture = new THREE.Texture();

	this.geom = GEOL.createGeometryGex(6);

	if (GEOL.paramIcons.user_sprite)
	{
		this.mat  = new THREE.SpriteMaterial({
			color: new THREE.Color(0xffffff),
			//side:THREE.DoubleSide,
			transparent:true,
			map: me.texture,
			depthWrite:false,
			depthTest:false
		});
		this.mesh = new THREE.Sprite( this.mat  );

	}
	else
	{
		this.mat  = new THREE.MeshBasicMaterial({
			color: new THREE.Color(0xffffff),
			//side:THREE.DoubleSide,
			transparent:true,
			map: me.texture,
			depthWrite:false
		});
		this.mesh = new THREE.Mesh( this.geom , this.mat  );
	}


	this.mesh.objType = "usr_circle";
	this.mesh.name = "usr_circle_"+nm;

	this. sc = GEOL.paramIcons.user_help.scale;
	this.mesh.scale.set(this.sc, this.sc, this.sc);

	this.mesh.objParent = this;

	this.mesh.renderOrder = (GEOL.renderOrderCircle);

	this.stateHighlight = false;
	this.stateSelect = false;

	this.showHighlight =function (obj){
		this.stateHighlight = true;
		this.mesh.visible = true;

		this.mesh.material.map = obj.textureAva;
		this.mesh.material.needsUpdate = true;


		this.reposition(obj);
	};

	this.showSelect = function (obj){

		this.stateSelect = true;
		this.mesh.visible = true;

		this.mesh.material.map = obj.textureAva;
		this.mesh.material.needsUpdate = true;

		this.reposition(obj);

	};
	this.hide = function(){
		this.stateHighlight = false;
		this.stateSelect = false;
		this.mesh.visible = false;
	};

	this.scaleRatio = GEOL.paramIcons.user_circle.scaleRatio;


	this.reposition = function(obj){

		this.mesh.position.set(0,0,0);
		this.mesh.quaternion.set(0,0,0,1);

		this.mesh.rotateY(-Math.PI/2 + deg2rad(obj.center_ll.lng));
		this.mesh.rotateX(deg2rad(-obj.center_ll.lat));

		this.mesh.translateZ(GEOL.BASE_RAD * GEOL.heightGeo.user_circle);

		this.selectType = obj.mesh.objType;

		this.sc = obj.mesh.scale.x *  GEOL.paramIcons.user_circle.scaleRatio;

		this.mesh.scale.set(this.sc, this.sc, this.sc);

	};


	GEOL.sceneOver.add(this.mesh);
//	GEOL.scene.add(this.mesh);

}