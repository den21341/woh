/**
 * Created by Morss on 10.04.16.
 */


function closeGraphAction(){
	alert('close action! need instead "alert()" implement hiding pop-up graph window.')
}

var offsetInfProjY = -20,
	offsetInfProjX = -10;
var offsetInfUserY = -20,
	offsetInfUserX = -10;

var nAniCrnt, nAniMax = 100;
var dAniDur = 3;



var dAniFrmDur = 1000* dAniDur / nAniMax;
var dgrRot = 0;
var tmRotPrev = Date.now();

var ndT, idT;

var network = null;
var ecnt = 0;
var mainuser_gid;
var ndsDtT, ndsObT;


var j, n, x, y, u, t, t1, t2, k, l, l1, dn, de, ed;
var nodes, edges;
var dtInitGraph;



var curtainIntervalId;
function showCurtain(){
	if (curtainIntervalId !== undefined)
	{
		clearInterval(curtainIntervalId);
		curtainIntervalId = undefined;
	}
	$('#graph-curtain').show();
	curtainIntervalId  = setInterval(rotateLoader, 10);
}

function hideCurtain(){
	if (curtainIntervalId !== undefined)
	{
		clearInterval(curtainIntervalId);
		curtainIntervalId = undefined;
	}
	$('#graph-curtain').hide();

}

//--------------------
function rotateLoader() {
	var div = document.getElementById('graph-loader-wrap'),
		tmCur  = Date.now();
	if ((tmCur - tmRotPrev) < 10)
		return;

	dgrRot += 5;
	div.style.webkitTransform = 'rotate('+dgrRot+'deg)';
	div.style.mozTransform    = 'rotate('+dgrRot+'deg)';
	div.style.msTransform     = 'rotate('+dgrRot+'deg)';
	div.style.oTransform      = 'rotate('+dgrRot+'deg)';
	div.style.transform       = 'rotate('+dgrRot+'deg)';

	tmRotPrev = Date.now();
}



function showNodeImages(bShow){

	var arUsr=[];
	ndsDtT =  network.body.data.nodes._data;
	if (bShow)
	{
		for (var k in ndsDtT)
		{
			if (ndsDtT[k].nd_type == 'usr' )
			{
				arUsr.push({id: ndsDtT[k].gid, image: ndsDtT[k].info.url_ava ? ndsDtT[k].info.url_ava : obAvasDef[ndsDtT[k].group], shape: 'circularImage'});
			}
		}
	}
	else
	{
		for (var k in ndsDtT)
		{
			if (ndsDtT[k].nd_type == 'usr')
			{
				var upd = {id: ndsDtT[k].gid,
					shape: 'icon',
					icon: {
						size: ndsDtT[k].size *2
					}
				};
				if (ndsDtT[k].info.more_show || ndsDtT[k].info.more_load)
				{
					upd.icon.code = '\uf002';
					if (upd.shapeProperties === undefined)
						upd.shapeProperties = {};
					upd.shapeProperties.borderDashes = [5,10];
				}
				else
				{
					upd.icon.code = '\uf001';
					if (upd.shapeProperties === undefined)
						upd.shapeProperties = {};
					upd.shapeProperties.borderDashes = false;
				}

				arUsr.push(upd);
			}
		}
	}

	try{
		network.body.data.nodes.update(arUsr);
	}
	catch (e){alert(e);}
}

function updateNodeState(gid){

	ndsDtT =  network.body.data.nodes._data;

	if (document.getElementById('graph-users-ava').checked)
	{
		if (ndsDtT[gid].nd_type == 'usr' )
		{
			var upd = {
				id: gid, image: ndsDtT[gid].info.url_ava ? ndsDtT[gid].info.url_ava : obAvasDef[ndsDtT[k].group],
				shape: 'circularImage'};
			if (ndsDtT[gid].info.more_show || ndsDtT[gid].info.more_load)
			{
				if (upd.shapeProperties === undefined)
					upd.shapeProperties = {};
				upd.shapeProperties.borderDashes = [5,10];
			}
			else
			{
				if (upd.shapeProperties === undefined)
					upd.shapeProperties = {};
				upd.shapeProperties.borderDashes = false;
			}

			try{
				network.body.data.nodes.update(upd);
			}
			catch (e){alert(e);}

		}
	}
	else
	{
		if (ndsDtT[gid].nd_type == 'usr')
		{
			var upd = {
				id: gid,
				image: obAvasDef[ndsDtT[gid].group],
				shape: 'icon',
				icon: {
					size: ndsDtT[gid].size *2
				}
			};
			if (ndsDtT[gid].info.more_show || ndsDtT[gid].info.more_load)
			{
				upd.icon.code = '\uf002';
				//if (upd.shapeProperties === undefined)						upd.shapeProperties = {};					upd.shapeProperties.borderDashes = [5,10];
			}
			else
			{
				upd.icon.code = '\uf001';
				//if (upd.shapeProperties === undefined)						upd.shapeProperties = {};					upd.shapeProperties.borderDashes = false;
			}
			try{
				network.body.data.nodes.update(upd);
			}
			catch (e){alert(e);}
		}

	}
}


// not clearly used
function destroyClearNetwork() {
	if (network !== null) {
		network.destroy();
		network = null;
	}

	nodes = null, edges = null;

}

var nd;

function buttonMoreClick(elem){
	if(elem.hasAttribute("data-node-id")){
		var gid = elem.getAttribute("data-node-id");
		if(network.body.data.nodes._data[gid])
		{
			if(network.body.data.nodes._data[gid].info.more_load ||
				network.body.data.nodes._data[gid].info.more_show)
			{	askNodeShowData(gid)
				//$(this).parent().parent().hide();
			}
		}
	}
}

function initGraph(id_vis_wrap, _userId) {

	if (network)
	{
		hideCurtain();
		return;
	}

	$('#'+id_vis_wrap).html(
		'<div id="vis-container">' +
			'<div id="graph-control-wrap">' +
			'<select id="graph-select-layout">'+
			'<option value="phisics" selected="selected">Эмуляция физики</option>' +
			'<option value="binary_tree" >Двоичное дерево</option>' +
			'<!--option value="circle">По кругу</option--> ' +
			'<option value="cluster">Кластеризация</option>' +
			'</select>' +
			'<label><input type="checkbox" checked	id="graph-users-ava" />показать авы юзеров</label>' +
			'&nbsp;&nbsp;&nbsp;&nbsp;<span id="graph-items-count"></span>' +
			'<div id="graph-control-close" onclick="_closeAction();"></div>'+
			'</div>'+
			'<div id="graph-wrapper">' +
			'<div id="graph-container"></div>' +
			'<div id="graph-info-project">' +
			'<div class="content"></div>' +
			'<div><button class="graph-info-more" onclick="buttonMoreClick(this);">Развернуть узел</button></div>' +
			'</div>' +
			'<div id="graph-info-user">' +
			'<div class="content"></div>' +
			'<div><button class="graph-info-more" onclick="buttonMoreClick(this);">Развернуть узел</button></div>' +
			'</div>' +
			'<div id="graph-curtain">' +
			'<div id="graph-loader-wrap">' +
			'<img id="graph-loader" src="/img/graph/loader-128.png"></div>' +
			'<div id="graph-curtain-text"></div>' +
			'</div>' +
			'</div>' +
			'</div>'+
			'</div>');

	$('#graph-curtain-text').html("Данные загружаются...");




	var optionsDef = {

		nodes:	{
			brokenImage:'/img/graph/brokenimage.png'
		},
		interaction: {
			navigationButtons: true,
			keyboard: true
		},

		edges:{
			color: {
				inherit:'to'
			}
		},


		groups: {
			useDefaultGroups: true,
			ed_more:{
				color: 'rgba(255,0,0, 0.5)',
				width: 5,
				smooth:{
					type:'continuous'
				}
			},
			u_help:{
				borderWidth:2,
				size:10,
				color: {background:'rgb(00,55,0)',
					border:'rgb(55,50,0)'},
				shape: 'dot'
			},
			u_main: {

				mass:30,
				fixed:
				{
					x:true,
					y:true
				},
				borderWidth:8,
				borderWidthSelected:10,
				size:60,
				shape: 'circularImage',
				image:'/img/graph/brokenimage.png',
				shapeProperties: {
					useBorderWithImage:true
				},
				color: {
					background:'#35b045',
					border:'#35b045',
					highlight:'#35b045'
				}
			}
		},
		physics: {
			enabled: false,

			maxVelocity: 14,
			minVelocity: 0.75,
			stabilization: {
				enabled: false,
				iterations: 100,
				updateInterval: 100,
				onlyDynamicEdges: false,
				fit: true
			}
		},
		configure: {
			filter:function (option, path) {
				if (path.indexOf('physics') !== -1) {
					return true;
				}
				if (path.indexOf('smooth') !== -1 || option === 'smooth') {
					return true;
				}
				return false;
			}
		}
	};

	var uGrpDt = [
		{
			clrIcon:'#f84840'
		},
		{
			clrIcon:'#ffc12d'
		},
		{
			clrIcon:'#f36624'
		},

		{
			clrIcon:'#2b78b8'
		},
		{
			clrIcon:'#38b348'
		},
		{
			clrIcon:'#40c2f8'
		},

		{
			clrIcon:'#be8756'
		},
		{
			clrIcon:'#b79afa'
		},
		{
			clrIcon:'#6b2b43'
		},

		{
			clrIcon:'#698422'
		},
		{
			clrIcon:'#006f00'
		},
		{
			clrIcon:'#888888'
		}
	];

	for(i = 1; i <=12; i++){
		optionsDef.groups["u_t"+i] ={

			size:20,
			borderWidth:7,
			borderWidthSelected:9,
			shape:'icon',
			icon : {
				back: {
					shape: 'circle'
				},
				face: 'FontVisGraph',
				code: '\uf001'
			},
			shapeProperties:{
				borderDashes: [5,10]
			}
		};
		optionsDef.groups["u_t"+i].color = {
			background:'#fff',
			border:uGrpDt[i-1].clrIcon};
		optionsDef.groups["u_t"+i].icon.color = uGrpDt[i-1].clrIcon;


		optionsDef.groups["p_t"+i] ={

			mass:15,
			shape:'icon',
			icon : {
				face: 'FontVisGraph',
				code: '\uf000',
				color:uGrpDt[i-1].clrIcon
			},
			color: {
				background:'#fff',
				border:uGrpDt[i-1].clrIcon
			}
		};

	}

	showCurtain();

	var elemGraph = document.getElementById('graph-container');
	if (elemGraph.addEventListener) {
		if ('onwheel' in document) {
			// IE9+, FF17+, Ch31+
			elemGraph.addEventListener("wheel", onWheel);
		} else if ('onmousewheel' in document) {
			// устаревший вариант события
			elemGraph.addEventListener("mousewheel", onWheel);
		} else {
			// Firefox < 17
			elemGraph.addEventListener("MozMousePixelScroll", onWheel);
		}
	} else { // IE8-
		elemGraph.attachEvent("onmousewheel", onWheel);
	}

	var _me = this;
	var isFF;

	function onWheel(e) {
		e = e || window.event;

		if (_me.isFF === undefined)
		{
			isFF = (navigator.userAgent.toLowerCase().indexOf('firefox') > -1);
		}
		if (isFF)
			return;
		e.returnValue = false;
		e.preventDefault ? e.preventDefault() : (e.returnValue = false);

		e.cancelBubble = true;
		return false;


	}

	$( "#graph-select-layout" ).change(repositionGraph);


	// create a network with def options

	var gData = {nodes:{}, edges: {}};
	gData.nodes = new vis.DataSet();
	gData.edges = new vis.DataSet();

	gData.edges.edgesFromArrTo = {};
	gData.edges.edgesToArrFrom = {};

	var container = document.getElementById('graph-container');
	network = new vis.Network(container, gData, optionsDef);

	network.body.data.custom = {};


	network.on("stabilizationProgress", function(params) {
		rotateLoader();
	});
	network.once("stabilizationIterationsDone", function() {
		hideCurtain();
	});

	network.on("showPopup", function (params) {	});
	network.on("hidePopup", function () {	});


	network.on("click", function (params) {

		if (params.nodes.length	){

			$('#graph-info-project').hide();
			$('#graph-info-user').hide();

			var nd = this.body.nodes[params.nodes[0]];
			if( nd != undefined && nd.options) {
				if(nd)
				{
					switch(nd.options.nd_type){
						case "usr":
							askNodeUserInfo(nd.id);


							break;
						case "prj":
							askNodeProjectInfo(nd.id);

							break;
						case "mnusr":
							break;
					}
				}
			}
		}
		else{
			$('#graph-info-project').hide();
			$('#graph-info-user').hide();
			$('#graph-button-show').hide();
			$('#graph-button-load').hide();
		}
	});

	network.on("hold", function (params) {


		if (params.nodes.length	){


			var nd = this.body.data.nodes._data[params.nodes[0]];
			if( nd != undefined && nd.info) {

				if (nd.info.more_show)
				{
					askNodeShowData(nd.id);
				}
				else
				if (nd.info.more_load)
				{
					askNodeShowData(nd.id);
				}
			}
		}
	});


	network.on("zoom", function (params) {


		repositionInfo();
		if (0){
			$('#graph-info-user').hide();
			$('#graph-info-project').hide();
			$('#graph-button-show').hide();
			$('#graph-button-load').hide();
		}

	});

	network.on("resize", function (params) {

		repositionInfo();
		if (0){
			$('#graph-info-user').hide();
			$('#graph-info-project').hide();
			$('#graph-button-show').hide();
			$('#graph-button-load').hide();
		}
	});

	network.cust = {};
	network.cust.pPrevDrag= {x:0, y:0};

	network.on("dragStart", function (params) {
		network.cust.pPrevDrag.x = params.pointer.DOM.x;
		network.cust.pPrevDrag.y = params.pointer.DOM.y;
	});

	network.on("dragging", function (params) {

		if (params.nodes.length == 0)
		{var inf = $('#graph-info-user');

			if( inf.is(':visible') )
			{
				var pos = inf.position();

				inf.css({top: pos.top + params.pointer.DOM.y - network.cust.pPrevDrag.y  + 'px',
					left: pos.left + params.pointer.DOM.x - network.cust.pPrevDrag.x  + 'px'});
			}
			else
			{
				inf = $('#graph-info-project');

				if( inf.is(':visible') )
				{
					var pos = inf.position();
					inf.css({top: pos.top + params.pointer.DOM.y - network.cust.pPrevDrag.y  + 'px',
						left: pos.left + params.pointer.DOM.x - network.cust.pPrevDrag.x  + 'px'});
				}
			}

			inf = $('#graph-button-show');

			if( inf.is(':visible') )
			{
				var pos = inf.position();
				inf.css({top: pos.top + params.pointer.DOM.y - network.cust.pPrevDrag.y  + 'px',
					left: pos.left + params.pointer.DOM.x - network.cust.pPrevDrag.x  + 'px'});
			}

			inf = $('#graph-button-load');

			if( inf.is(':visible') )
			{
				var pos = inf.position();
				inf.css({top: pos.top + params.pointer.DOM.y - network.cust.pPrevDrag.y  + 'px',
					left: pos.left + params.pointer.DOM.x - network.cust.pPrevDrag.x  + 'px'});
			}

			network.cust.pPrevDrag.x = params.pointer.DOM.x;
			network.cust.pPrevDrag.y = params.pointer.DOM.y;
		}
		else{
			$('#graph-info-user').hide();
			$('#graph-info-project').hide();
			$('#graph-button-show').hide();
			$('#graph-button-load').hide();
		}
	});

	askMainUserData(_userId);

	$('#graph-users-ava').change(function() {
		showNodeImages(this.checked);
	});
}

function _closeAction(){
	sendGraphSettings($( "#graph-select-layout" ).find('option:selected').val(),
		document.getElementById('graph-users-ava').checked);

	closeGraphAction();
}

function AddNodesEdges(dat_nodes, dat_edges,
					   _root_gid, _x, _y){

	nodes = network.body.data.nodes;
	edges = network.body.data.edges;

	l = dat_nodes.length;
	var xp = -400, xu = 400, ypu = 0, tnd, ted;

	if (_x !== undefined)
		xp = xu = _x;
	if (_y !== undefined)
		ypu = _y;


	$('#graph-curtain-text').html("Данные загружаются...");

	var arNodes = [];
	var obNdIds  = {};

	var isImage = document.getElementById('graph-users-ava').checked;
	var grps = network.groups.groups;


	//	arUsr.push({id: ndsDtT[k].gid, image: ndsDtT[k].image_ava ? ndsDtT[k].image_ava : obAvasDef[ndsDtT[k].group], shape: 'circularImage'});


	for ( i = 0; i < l; i++){

		tnd = dat_nodes[i];
		var nd_id = false;

		// create usual nodes

		switch (tnd.nd_type) {
			case "prj":
			{
				if(nodes._data[tnd.gid] == undefined)
				{
					tnd.label  = tnd.gid;


					nd_id = tnd.gid;

					tnd.id = tnd.gid;
					tnd.group = 'p_'+tnd.p_type;
					tnd.title =  tnd.info.title ;
					tnd.x = xp;
					tnd.y = ypu;

				}
			}
				break;
			case "usr":
			{
				if(nodes._data[tnd.gid] == undefined)
				{

					nd_id = tnd.gid;

					tnd.label  = tnd.gid;
					tnd.id = tnd.gid;
					tnd.group = 'u_'+ tnd.p_type;
					tnd.title = tnd.info.uname ;
					tnd.x = xu;
					tnd.y = ypu;
					if (isImage)
					{
						tnd.shape = 'circularImage';
						tnd.image = (tnd.info.url_ava) ? tnd.info.url_ava : obAvasDef[tnd.group];
					}
					else{
						tnd.shape = 'icon';
						if (tnd.icon === undefined)
							tnd.icon = {};
						if (!tnd.size)
							tnd.size = grps[tnd.group].size;
						tnd.icon.size =  tnd.size  *2;

						if (tnd.info.more_show || tnd.info.more_load)
						{
							tnd.icon.code = '\uf002';
						}
						else
						{
							tnd.icon.code = '\uf001';
						}
					}
				}
			}
				break;
			default:
				break;
		}

		if(obNdIds[tnd.gid] == undefined &&
			nodes._data[tnd.gid] == undefined)
		{
			arNodes.push(tnd);
			obNdIds[tnd.gid] = 1;
		}
	}



	try{
		nodes.add(arNodes);
	}
	catch (e){ alert(e);}

	// create usual edges
	l = dat_edges.length;

	var arEdges = [];
	var obEdIds = {};

	for ( i = 0; i < l; i++){

		ted = dat_edges[i];

		ted.id =  'e'+(++ecnt);
		if(obEdIds[ted.from + '_' +ted.to] == undefined &&
			(edges.edgesFromArrTo[ted.from] == undefined ||
				edges.edgesFromArrTo[ted.from][ted.to] == undefined))
		{
			ted.arrows = {};
			ted.arrows.middle = {};

			ted.arrows.middle.enabled = true;

			arEdges.push(ted);
			obEdIds[ted.from + '_' +ted.to] = ted;
		}
	}

	var nd_dat = nodes._data;

	// set style for edges of user-project tree brunch
	// and prepare for breadthing older edges
	for( var n in obEdIds){
		nd_dat[obEdIds[n].to].info.time_date = obEdIds[n].info.time_date;

		if (nd_dat[obEdIds[n].from].lvl == undefined)
			continue;

		if (nd_dat[obEdIds[n].from].lvl < 0 )
		{
			obEdIds[n].dashes = true;
			if (obEdIds[n].color === undefined)
				obEdIds[n].color = {};
			obEdIds[n].color.inherit = 'from';
		}

	}
	// set width accordant to date
	var obFromToUsrs ={};
	for( var n in obEdIds){

		if (nd_dat[obEdIds[n].to].lvl > -1
			|| nd_dat[obEdIds[n].to].lvl === undefined)
		{
			if (obFromToUsrs[obEdIds[n].from] === undefined)
			{
				obFromToUsrs[obEdIds[n].from] = {};
			}
			obFromToUsrs[obEdIds[n].from][obEdIds[n].to] = obEdIds[n];
		}
	}

	testDatesRecurs(_root_gid, obFromToUsrs, nd_dat);

	try{
		edges.add(arEdges);
	}
	catch (e){ alert(e);}


	if (_x !== undefined)
	{
		repositionGraph();
	}
	// now calculate levels...

}

function testDatesRecurs(ndFrom, obFrmTo, nd_dat, tm){

	if (obFrmTo[ndFrom] === undefined
		||	nd_dat[ndFrom].dt_hlp === undefined
		)
	{
		nd_dat[ndFrom].dt_hlp = tm;
		return;
	}

	var ed;
	for (var k in obFrmTo[ndFrom]){
		var ed = obFrmTo[ndFrom][k];
		if (nd_dat[ndFrom].dt_hlp > ed.info.time_date)
			return;

		ed.width = 5;

		ed.shadow = {
			enabled: true,
			color: 'rgb(0,0,0)',
			x: 	2,
			y: 2};

		nd_dat[ed.to].dt_hlp = ed.info.time_date;

		testDatesRecurs(k, obFrmTo, nd_dat, ed.info.time_date);
	}
}

function applyMainUserData(dat){
	dtInitGraph	= dat;
	$('#graph-curtain-text').html("Подготовка данных..." );
	$('#graph-items-count').html("Всего узлов: "+ dtInitGraph.data.nodes.length +". Всего связей: "+ dtInitGraph.data.edges.length );


	setTimeout(_applyMainUserData, 1);
}

function _applyMainUserData(){





	network.stopSimulation();

//	clear data

//	destroyClearNetwork();



	t = dtInitGraph.data;

	// read settings
	var sts;
	if ( t.settings)
		sts = t.settings;
	else
		sts = {};


	if (sts.graph_layout)
	{
		switch (sts.graph_layout){
			case "binary_tree":
			case "circle":
			case "cluster":
				break;
			case "phisics":
			default :
				sts.graph_layout = "phisics";
				break;
		}
	}
	else
		sts.graph_layout = "phisics";

	var nodes = network.body.data.nodes;
	var edges = network.body.data.edges;

	if (nodes.length > 0)
	{	for (var k in nodes._data)
	{
		try {
			nodes.remove(nodes._data[k].id);
		}
		catch (err) {
			alert(err);
		}
	}
	}
	if (edges.length > 0)
	{
		for (var k in edges._data)
		{
			try {
				edges.remove(edges._data[k].id);
			}
			catch (err) {
				alert(err);
			}
		}
		edges.edgesFromArrTo = {};
		edges.edgesToArrFrom = {};
	}




	// create main_user node
	u = t.source_node;
	mainuser_gid = u.gid;

	u.id = mainuser_gid;
	u.nd_type = "mnusr";
	u.label= u.uname;
	u.group= 'u_main'
	u.image = u.url_ava;
	u.x= 0;
	u.y=0;
	u.title ='<a href="#">User link</a>';
	u.level = 0;
	u.dt_hlp = "0000-00-00T00:00:00";

	nodes.add(u);

	// create all other nodes

	dn = t.nodes;
	de = t.edges;



	AddNodesEdges(dn, de, mainuser_gid);

	/*
	 edges.add({id: 'e'+ (++ecnt), from: 'p_'+ t[i].uid, to: uid});

	 t1 = t[i].helpers;
	 l1 = t1.length;

	 for ( j = 0; j < l1; j++){

	 nd = {id: 'u_'+ t1[j].uid,
	 group: 'u_help',
	 title: 	'<a href="#">' + t1[j].name +'</a>',
	 x: xf, y:yf,
	 prnt_id: 'p_'+ t[i].uid,
	 level:2,
	 children:{}
	 };

	 ed = {id: 'e'+ (++ecnt), from: 'u_'+ t1[j].uid, to: 'p_'+ t[i].uid};

	 if (j >= maxNd_1)
	 {
	 nd.hidden = true;
	 nd.physics = false;
	 ed.hidden = true;
	 ed.physics = false;
	 nd.label = 'hidden';


	 }

	 try{
	 nodes.add(nd);
	 }catch(e){};

	 edges.add(ed);
	 }
	 if (j > maxNd_1){

	 nd = {
	 prnt_id: 'p_'+ t[i].uid,
	 id: 'nshw_'+ 'p_'+ t[i].uid,
	 edge_id: 'eshw_'+ 'p_'+ t[i].uid,
	 group: 'nd_more',
	 title: 	'+ ' + (l1 - maxNd_1).toString(),
	 physics:  false,
	 prnt_id: 'p_'+ t[i].uid


	 };

	 ed = {
	 id: 'eshw_'+ 'p_'+ t[i].uid,
	 group: 'ed_more',
	 from: 'p_'+ t[i].uid,
	 to: 'nshw_'+ 'p_'+ t[i].uid,
	 physics:  false,
	 color: 'rgba(255,0,0, 0.5)',
	 width: 5,
	 smooth: false
	 };

	 try{
	 nodes.add(nd);
	 edges.add(ed);
	 arNdSub['nshw_'+ 'p_'+ t[i].uid] = {prnt_id:'p_'+ t[i].uid, nd_id: 'nshw_'+ 'p_'+ t[i].uid,
	 dX: 0, dY: 100};

	 }
	 catch (e){

	 };
	 }

	 };

	 t = dat.data.actors;

	 for ( i = t.length; --i>=0;)
	 {
	 t[i].u_from = 'u_' + t[i].u_from;
	 t[i].u_to = 'u_' + t[i].u_to;
	 }

	 appendUsers(uid, t, nodes, edges);

	 */






	if (nodes.length + edges.length > 500)
	{
		network.setOptions({edges:{smooth : false}});
	}


	if (sts.show_avas === true || sts.show_avas == "true"){
		showNodeImages(true);
		$("#graph-users-ava").attr('checked', 'checked');
	}

	$("#graph-select-layout").find("option").each( function(){
		if ($(this).val() == sts.graph_layout){
			$(this).attr('selected', 'selected');
			return false;
		}
	});

	repositionGraph(1);
}

function ObjectLength_Modern( object ) {
	return Object.keys(object).length;
}
function ObjectLength_Legacy( obj ) {
	var length = 0;
	for( var key in obj ) {
		if( obj.hasOwnProperty(key) ) {
			++length;
		}
	}
	return length;
}
var ObjectLength = Object.keys ? ObjectLength_Modern : ObjectLength_Legacy;
//---------------
var itrCount = 0;
var itrMax = 5000;
function animateReposition(){

	nAniCrnt = 1;

	var nds = network.body.nodes;
	var nd;
	for(var k in nds){
		nd = nds[k];
		if (nd.x_new !== undefined)
		{
			nd.x_d = nd.x_new - nd.x_old;
			nd.y_d = nd.y_new - nd.y_old;
		}
	}
	updateAnimateNodesPosition();
}

function updateAnimateNodesPosition(){
	if (nAniCrnt > nAniMax)
		return;
	var nds = network.body.nodes;
	var sb = network.body.subPlus;
	var nd;
	var tmRat = nAniCrnt / nAniMax;
	for(var k in nds){
		nd = nds[k];
		if (nd.x_old !== undefined){
			nd.x = nd.x_old + nd.x_d * swing(tmRat);
			nd.y = nd.y_old + nd.y_d * swing(tmRat);
		}

	}

	if (isInitFit)
		network.fit();
	else
		network.redraw();
	++nAniCrnt;
	setTimeout(updateAnimateNodesPosition, dAniFrmDur);
}

var objSwingRet = {};
function swing(t) {
	if (objSwingRet[t] === undefined )
		objSwingRet[t] = (-Math.cos(t*Math.PI)/2) + 0.5
	return objSwingRet[t];
}
//--------------


var isInitFit = false;

function repositionGraph(init){

	isInitFit = (init === 1);

	/*	if ($("#graph-select-layout").find("option:selected").val() == 'phisics')
	 {
	 network
	 }

	 if ($("#graph-select-layout").find("option:selected").val() == 'phisics')
	 {
	 network
	 }
	 */
	switch ($("#graph-select-layout").find("option:selected").val()){
		case "binary_tree":
			repositionAsBinaryTree(init);
			break;
		case "circle":
			repositionAsCircle(init);
			break;
		case "cluster":
			repositionAsCluster(init);
			break;

		case "phisics":
		default :

			repositionAsPhisics(init);
			break;
	}

	hideCurtain();

}

function repositionAsBinaryTree(init){
	var options = {
		edges:{smooth: {}},
		physics: {
			enabled: false
		}
	};
	if (network.body.data.nodes.length + network.body.data.edges.length < 500)
	{
		options.edges.smooth.enabled = true;
		options.edges.smooth.type = "continuous";
	}
	else
		options.edges.smooth.enabled = false;


	network.setOptions(options);

	createTree(mainuser_gid, network.body.data);

	placeTreeBinary(network.body.data.treeItems, 50, 120);

	ndsObT = network.body.nodes;
	if (init === 1)
	{
		for (var k in ndsObT){
			if (ndsObT[k].x_new !== undefined)
			{
				network.body.nodes[k].x = ndsObT[k].x_new;
				network.body.nodes[k].y = ndsObT[k].y_new;
			}
		}
		network.redraw();
		network.fit();
	}
	else
		animateReposition();
}

function createTree(mnUserId, netData){


	var frm = netData.edges.edgesFromArrTo;
	var nd_dat = netData.nodes._data
	var edges = netData.edges;
	var treeRef;

	if (netData.treeItems === undefined)
	{
		netData.treeItems = {rootMinus: {id: mnUserId, lvl: 0, twigs:{}},
			rootPlus: {id: mnUserId, lvl: 0, twigs:{}}};
	}

	treeRef = netData.treeItems;

	// set level for MY_PROJECTS nodes
	var arPrjId = [];
	for (var t in nd_dat)
	{

		ndT = nd_dat[t];
		if (ndT.nd_type == 'usr')
		{
			ndT.tr_us = 0;
			ndT.tr = 1;
		}
		else
		{
			ndT.tr_us = 1;
			idT = ndT.id;
			if 	(ndT.nd_type == 'prj')
			{

				arPrjId.push(idT);
				if (treeRef.rootMinus.twigs[idT] === undefined)
				{
					treeRef.rootMinus.twigs[idT] = {id: idT, lvl: -1, twigs: {} };
				}
			}
		}

		itrCount++;
		if(itrCount > itrMax){
			rotateLoader();
			itrCount = 0;
		}
	}



	// set levels for project helpers nodes


	var branch = treeRef.rootMinus.twigs;
	for (var twId in branch)
	{
		t = netData.edges.edgesToArrFrom[twId];
		for (var key in t ){
			if (nd_dat[key].nd_type != 'usr')
				continue;
			if (branch[twId].twigs[key] === undefined)
			// ||	nd_dat[twId].lvl === undefined)
			{
				branch[twId].twigs[key] = {id: key, lvl: -2, twigs: {}};
				nd_dat[key].tr_us = 1;
			}


			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}


		}
	}

	createBrunchPlus(treeRef.rootPlus, 0, frm, nd_dat);

}


function createBrunchPlus(trPrnt, lvl, frmTo, nd_dt){

	++lvl;

	for (var m in frmTo[trPrnt.id])
	{

		if ((nd_dt[m].tr != 1) ||
			(nd_dt[m].tr_us))
			continue;

		if (trPrnt.twigs[m] === undefined)
			trPrnt.twigs[m] = {id: m, lvl: lvl, twigs: {}}

		if (nd_dt[m] && nd_dt[m].lvl)
			trPrnt.twigs[m].lvl = nd_dt[m].lvl;
		else
			trPrnt.twigs[m].lvl = lvl;

		itrCount++;
		if(itrCount > itrMax){
			rotateLoader();
			itrCount = 0;
		}

	}

	for (var m in trPrnt.twigs)
	{
		if ((nd_dt[m].tr != 1) ||
			(nd_dt[m].tr_us))
			continue;
		nd_dt[m].tr_us = 1;

		createBrunchPlus(trPrnt.twigs[m], lvl, frmTo, nd_dt);

		itrCount++;
		if(itrCount > itrMax){
			rotateLoader();
			itrCount = 0;
		}

	}
}


function placeTreeBinary(tree, wdhLeaf, dstLvl){

	var pnt = {x: 0, y: 0};

	tree.rootPlus.posX = tree.rootPlus.posY =
		tree.rootMinus.posX = tree.rootMinus.posY = 0;

	countLeaves(tree.rootPlus );
	countLeaves(tree.rootMinus );

	normalizeTreeLevelHeight(tree.rootPlus,	dstLvl, 3, 0.7);
	normalizeTreeLevelHeight(tree.rootMinus, dstLvl, 3, 0.7);


	tree.rootMinus.prefLvlHeight = dstLvl;
	tree.rootPlus.prefLvlHeight = dstLvl;

	setX(tree.rootMinus, 0);
	setX(tree.rootPlus, 0);

	setY(tree.rootPlus, 0 , wdhLeaf);
	setY(tree.rootMinus, 0 , wdhLeaf);

	tree.rootMinus.posX =	tree.rootMinus.posY = 0;
	tree.rootPlus.posX =	tree.rootPlus.posY = 0;



	ndsObT = network.body.nodes;
	calculatePlaceBinaryTreeBranch(tree.rootPlus, network);
	calculatePlaceBinaryTreeBranch(tree.rootMinus, network);



}


function normalizeTreeLevelHeight(trItem, minHeight, minCountRize, ratioRize){
	/*	Object
	 id		:		"mnu375"
	 leaves		:		46
	 lvl		:		0
	 posX		:		0
	 posY		:		0
	 twigs		:		Object
	 */

	var prefHgt = minHeight, hgt = 0, l = 0;
	if (trItem.twigs)
		l = ObjectLength(trItem.twigs);

	if (l > 0)
	{
		for (var k in trItem.twigs)
		{
			normalizeTreeLevelHeight(trItem.twigs[k], minHeight, minCountRize, ratioRize);

			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}

		}

		for (var k in trItem.twigs)
		{
			if (hgt < trItem.twigs[k].prefLvlHeight)
				hgt = trItem.twigs[k].prefLvlHeight;

			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}
		}

		for (var k in trItem.twigs)
		{
			trItem.twigs[k].prefLvlHeight = hgt ;

			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}

		}

		if (ObjectLength(trItem.twigs) >= minCountRize )
		{
			prefHgt = l * ratioRize * minHeight;
		}
		trItem.prefLvlHeight  = prefHgt;
	}
	else trItem.prefLvlHeight  = minHeight;

}

function setY(trItem, pY, wdh){

	var itmT;
	var cnt = 0;
	if (ObjectLength(trItem.twigs) > 0)
	{
		for (var k in trItem.twigs)
		{
			cnt += trItem.twigs[k].leaves;
		}
		var stY = pY - (cnt*wdh / 2) ;
		cnt = 0;
		for (var k in trItem.twigs)
		{
			itmT =trItem.twigs[k];
			itmT.bndY = stY + cnt*wdh;
			itmT.posY = itmT.bndY + trItem.twigs[k].leaves*wdh/2;
			setY(itmT, itmT.posY, wdh);
			cnt += trItem.twigs[k].leaves;


			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}

		}
	}
	else trItem.posY = pY ;
}


function setX(trItem, shiftX){

	if (trItem.lvl > 0)
		trItem.posX = shiftX;
	else
		trItem.posX = -shiftX;

	var shiftNext = shiftX + trItem.prefLvlHeight;

	if (ObjectLength(trItem.twigs) > 0)
	{
		for (var k in trItem.twigs)
		{
			setX(trItem.twigs[k], shiftNext);


			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}


		}

	}
}


function countLeaves(trItem ){

	trItem.leaves = 0;
	if (ObjectLength(trItem.twigs) > 0)
		for (var k in trItem.twigs)
		{
			countLeaves(trItem.twigs[k]) ;
			trItem.leaves += trItem.twigs[k].leaves;


			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}




		}
	else trItem.leaves  = 1;

}

function calculatePlaceBinaryTreeBranch(trItem, network){
	if (ObjectLength(trItem.twigs) > 0)
	{
		for (var k in  trItem.twigs)
		{
			calculatePlaceBinaryTreeBranch(trItem.twigs[k], network);


			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}




		}
	}

	ndsObT[trItem.id].x_old = ndsObT[trItem.id].x;
	ndsObT[trItem.id].y_old = ndsObT[trItem.id].y;

	ndsObT[trItem.id].x_new = trItem.posX;
	ndsObT[trItem.id].y_new = trItem.posY;
}
//--------------
function repositionAsCircle(init){
	/* fixme: circling not realized
	 and	 createTree(mainuser_gid, network.data);	 */
}

function repositionAsPhisics(init){

	var options = {
		edges:{smooth: {}},
		physics: {
			enabled: true,
			maxVelocity: 14,
			minVelocity: 0.75
		}
	};
	if (network.body.data.nodes.length + network.body.data.edges.length < 500)
	{
		options.edges.smooth.enabled = true;
		options.edges.smooth.type = "dynamic";
	}
	else
		options.edges.smooth.enabled = false;



	network.setOptions(options);

	if (init === 1)
	{
		network.stabilize();
		network.fit();
	}
	else
		network.startSimulation();

}
//--------------
function repositionAsCluster(init){

	var options = {
		edges:{smooth: {}},
		physics: {
			enabled: false
		}
	};
	if (network.body.data.nodes.length + network.body.data.edges.length < 500)
	{
		options.edges.smooth.enabled = true;
		options.edges.smooth.type = "continuous";
	}
	else
		options.edges.smooth.enabled = false;



	network.setOptions(options);

	// count project groups

	// FIXME: maximal gropus count == 18 !!! NotMore

	var ratNdsCount = Math.sqrt(network.body.data.nodes.length) * 0.1;
	if (ratNdsCount < 1)
		ratNdsCount = 1;
	var radGex = 500 * ratNdsCount;
	var szGex = 200 * ratNdsCount;
	var frcGex = 0.05;

	var grps = {};
	ndsDtT = network.body.data.nodes._data;
	ndsObT = network.body.nodes;

	var ndsShow = {};
	for (k in ndsDtT)

	{
		if (ndsDtT[k].nd_type != 'prj' &&
			ndsDtT[k].nd_type != 'usr' &&
			ndsDtT[k].nd_type != 'mnusr')
			continue;

		ndsShow[k] = ndsObT[ndsDtT[k].id];

		if (ndsDtT[k].nd_type != 'mnusr')
		{
			if (grps[ndsDtT[k].p_type] === undefined)
			{
				grps[ndsDtT[k].p_type] = {};
			}
			grps[ndsDtT[k].p_type][k] = ndsDtT[k];
		}
		else
			ndsObT[ndsDtT[k].id].x_new =
				ndsObT[ndsDtT[k].id].y_new =
					ndsObT[ndsDtT[k].id].x_old =
						ndsObT[ndsDtT[k].id].y_old = 0;


		itrCount++;
		if(itrCount > itrMax){
			rotateLoader();
			itrCount = 0;
		}




	}


	var arPosGex = [];

	var p;
	for (i = 0; i < 6; i++){
		p = rotateAroundZero(1, Math.PI * 2 * i/6);
		arPosGex.push({x: p.x, y: p.y});
	}

	for (i = 0; i < 6; i++){
		p = rotateAroundZero(2,  Math.PI * 2 * i/6);
		arPosGex.push({x: p.x, y: p.y});
	}

	for (i = 0; i < 6; i++){
		arPosGex.push(
			{
				x: arPosGex[6+i].x + (arPosGex[6+i].x - arPosGex[6+i +1].x)/2,
				y: arPosGex[6+i].y + (arPosGex[6+i].y - arPosGex[6+i +1].y)/2}
		);
	}

	if (ObjectLength(grps) > 18)
		alert("Maximal groups count <= 18 !!\nBecause of gexagon two level position!\n Current count - " + ObjectLength(grps) );

	for (i = 0; i < arPosGex.length; i++){
		arPosGex[i].x = arPosGex[i].x * radGex;
		arPosGex[i].y = arPosGex[i].y * radGex;

	}

	l = ObjectLength(grps);
	if (l <7)
		arPosGex.length = 6;
	shuffle(arPosGex);



	var i = 0;

	for( k in grps){
		for(t in grps[k])
		{
			ndsObT[t].x_old = ndsObT[t].x;
			ndsObT[t].y_old = ndsObT[t].y;

			pTm = getRandomGex(szGex);
			ndsObT[t].x_new = arPosGex[i].x + pTm.x;
			ndsObT[t].y_new = arPosGex[i].y + pTm.y;



			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}



		}
		i++;
	}


	var z = network.body.data.edges.edgesFromArrTo;

	for( var f in z){
		if (ndsDtT[f].nd_type == 'mnusr')
			continue;

		if (ndsShow[f] === undefined)
			continue;

		for(var t in z[f])
		{
			if (ndsShow[t] === undefined)
				continue;

			if (ndsObT[f].x_new < ndsObT[t].x_new)
				ndsObT[t].x_new -= Math.abs(ndsObT[f].x_new - ndsObT[t].x_new) * frcGex;
			else
				ndsObT[t].x_new += Math.abs(ndsObT[f].x_new - ndsObT[t].x_new) * frcGex;
			if (ndsObT[f].y_new < ndsObT[t].y_new)
				ndsObT[t].y_new -= Math.abs(ndsObT[f].y_new - ndsObT[t].y_new) * frcGex;
			else
				ndsObT[t].y_new += Math.abs(ndsObT[f].y_new - ndsObT[t].y_new) * frcGex;



			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}



		}
	}



	if (init === 1)
	{
		for (var k in ndsObT){
			if (ndsObT[k].x_new !== undefined)
			{
				network.body.nodes[k].x = ndsObT[k].x_new;
				network.body.nodes[k].y = ndsObT[k].y_new;
			}



			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}



		}
		network.redraw();
		network.fit();
	}
	else
		animateReposition();
}



var  rt_p = {'x':0, 'y':0} ;

function rotateAroundZero(lng, ang){

	rt_p.x = lng * Math.sin(ang);
	rt_p.y = lng * Math.cos(ang);

	return rt_p;
}

var pTm = {x:0, y:0};
function getRandomGex(szGex){

	do {
		pTm.x = Math.random();
		pTm.y = Math.random();
	}while(!ptInsideGex(pTm))

	pTm.x = pTm.x * 2 * szGex - szGex;
	pTm.y = pTm.y * 2 * szGex - szGex;
	return pTm;
}

var gexPlg = [{	x: 0.5,	y: 0},
	{x: 0.935,	y: 0.25},
	{x: 0.935,	y: 0.75},
	{x: 0.5,    y: 1},
	{x: 0.07,	y: 0.75},
	{x: 0.07,	y: 0.25},
	{x: 0.5,	y: 0}];


function ptInsideGex ( pt)
{
	var pI , pY;
	var bInside = false;
	for ( i = 1; i < 7; i++)
	{
		pI = gexPlg[i];
		pY = gexPlg[i-1];

		if ((((pI.y <= pt.y) && (pt.y < pY.y)) || ((pY.y <= pt.y) && (pt.y < pI.y)))
			&& (pt.x < (pY.x - pI.x) * (pt.y - pI.y) / (pY.y - pI.y) + pI.x))
			bInside = !bInside;
	}
	return bInside;
}

function shuffle(a) {
	var j, x, i;
	for (i = a.length; i; i -= 1) {
		j = Math.floor(Math.random() * i);
		x = a[i - 1];
		a[i - 1] = a[j];
		a[j] = x;
	}
}
//----------------
function askMainUserData(_umain_id){
	var postData = {user_id: _umain_id, request: "tree_main"};
	jQuery.ajax({
		method: "POST",
		url: "api.php",
		data: postData,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			applyMainUserData(data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


}

function askNodeShowData(gid){

	var nd = network.body.data.nodes._data[gid];

	if (nd === undefined)
		return;

	var postDat = {gid: gid};
	if (nd.info.more_show)
		postDat.request ="tree_user_show";
	else
	if (nd.info.more_load)
		postDat.request ="tree_user_load";


	if (nd.info.time_utc !== undefined)
		postDat.time_utc = nd.info.time_utc;
	if (nd.info.time_date !== undefined)
		postDat.time_date = nd.info.time_date;

	jQuery.ajax({
		method: "POST",
		url: "api.php",
		data: postDat,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			applyAppendNodeData(data);

			$('#graph-button-show').hide();
			$('#graph-button-load').hide();

		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})


}

function applyAppendNodeData(dat){


	t = dat.data;
	var ndSrc = t.source_node.gid;

	var actNodes = network.body.nodes;
	var datNodes = network.body.data.nodes._data;


	var xPls = 0;
	var yPls = 0;

	if (datNodes[ndSrc] )
	{
		network.body.data.nodes._data[ndSrc].info.more_load = t.source_node.more_load;
		network.body.data.nodes._data[ndSrc].info.more_show = t.source_node.more_show;

		xPls = actNodes[ndSrc].x;
		yPls = actNodes[ndSrc].y;
	}

	AddNodesEdges(t.nodes, t.edges, ndSrc, xPls, yPls);

	updateNodeState(ndSrc);

}



function askNodeUserInfo(u_gid){

	if(u_gid.charAt(0) != 'u')
		return;

	var postDat = {gid: u_gid, request: "node_user_info"};

	jQuery.ajax({
		method: "POST",
		url: "api.php",
		data: postDat,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			showUserInfo(data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})

}

function showUserInfo(data){
	var d  = data.data;

	var pgid = d.gid;
	if (network.body.nodes[pgid]){

		var elInfoUser = $('#graph-info-user');
		elInfoUser.attr("data-node-id", pgid);
		elInfoUser.find('.content').html(d.content);


		l= {top: 9000,
			left: 9000};
		elInfoUser.css(l);
		elInfoUser.show();

		repositionInfo();

		$("#graph-info-user").find(".graph-info-more").attr('data-node-id', pgid);

	}
}


function askNodeProjectInfo(p_gid){

	if(p_gid.charAt(0) != 'p')
		return;

	var postDat = {gid: p_gid, request: "node_project_info"};

	jQuery.ajax({
		method: "POST",
		url: "api.php",
		data: postDat,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
			showProjectInfo(data);
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	})

}

function showProjectInfo(data){

	var d  = data.data;
	var pgid = d.gid;

	if (network.body.nodes[pgid]){

		var elInfoPrj = $('#graph-info-project');
		elInfoPrj.attr("data-node-id", pgid);

		elInfoPrj.find('.content').html(d.content);

		l= {top: 9000,
			left: 9000};
		elInfoPrj.css(l);
		elInfoPrj.show();

		repositionInfo();
		$("#graph-info-project").find(".graph-info-more").attr('data-node-id', pgid);

	}
}

function repositionInfo(){

	var inf = $('#graph-info-project');

	if( inf.is(':visible') )
	{

		nd = inf.attr("data-node-id");
		var ndPos = network.getPositions(nd);

		ndPos = network.canvasToDOM(ndPos[nd]);

		inf.css({top: ndPos.y - inf.height() + offsetInfProjY + 'px',
			left: ndPos.x + offsetInfProjX + 'px'});

	}
	else{

		var inf = $('#graph-info-user');

		if( inf.is(':visible') )
		{
			nd = inf.attr("data-node-id");
			var ndPos = network.getPositions(nd);

			ndPos = network.canvasToDOM(ndPos[nd]);

			inf.css({top: ndPos.y - inf.height() + offsetInfUserY + 'px',
				left: ndPos.x + offsetInfUserX + 'px'});
		}
	}

}



function sendGraphSettings(graph_lay, show_avas){

	var postDat = {request: "settings_save",
		show_avas: show_avas,
		graph_layout: graph_lay};

	jQuery.ajax({
		method: "POST",
		url: "api.php",
		data: postDat,
		dataType: "json",
		success: function(data){
			if (!data)
			{
				alert("Returned empty data-object");
				return;
			};
			if (data.status == undefined || data.status != "ok")
			{
				alert("Error status - " + data.status);
				return;
			};
		},
		error: function( jqXHR, textStatus, errorThrown )
		{
			alert("Error: " + textStatus + "\n" + errorThrown);
		}
	});

}