/**
 * Created by Morss on 10.04.16.
 */


var REQAJXURL = "";
onMouse = false;

if (typeof isRealDemo != 'undefined')
	REQAJXURL = "/ajx/graphapi/api.php";
else
if (typeof isVisDemo == 'undefined')
	REQAJXURL  = "http://wayofhelp.com/ajx/graphapi/";
else
	REQAJXURL = "/ajx/graphapi/";


var DEFGRAPHLAYOUT = "physics"; // расположение по умолчанию, если не указано явно -  "physics" OR "cluster" OR "binary_tree"

var ASK_COUNT = 5; // сколько запрашивать, при развороте узла

var AVA_U_DEF = "http://wayofhelp.com/img/graph/ava_u_def.png"; // НЕОБХОДИМАЯ дефолтная авка, если какая ошибка загрузки

var	OB_AVAS_DEF = {}; // а сюда по желанию можно засунуть дефолтные картинки-аватарки, но уже в зависимости от типа проекта, и только если не указан url авки юзера

for (var i = 1; i <=12; i++){ // 12 - потому что сейчас такое количество проектов{

	// OB_AVAS_DEF["u_t"+i] = "http://numberimage.ru/graph/img/graph/ava_u_"+i+".png";
}


function ObjGraph(_container, _user_id, _dbg){

	var isDebug;
	if ( typeof _dbg != 'undefined')
		isDebug = _dbg;


	this.onClose = function (){
		alert('close action! need instead "alert()" implement hiding pop-up graph window.')
	};

	var elChkAvas, elChkLbls;

	var style_def =  ".definf-ava{float:left; max-width:100px;}" +
		".definf-ava a { text-decoration: none;}" +
		".graph-info-baloon {" +
		"background-color: #EFEFEF;" +
		"border: 1px solid #CCC;}" +
		".definf-ava img {width:80px;height:80px;border-radius:40px; margin: 0 15px 0 0;}" +
		".graph-definf-content {min-width: 250px; padding: 5px;}" +
		".graph-definf-content p{margin-bottom:0;}"+
		".w300{min-width:300px}" +
		".graph-info-wrap{padding:10px 0}" +
		"#graph-info-pntr" +
		"{position: absolute;" +
		"width: 15px;" +
		"height: 15px;" +
		"transform: rotate(45deg);" +
		"background-color: #EFEFEF;" +
		"border-right: 1px solid #CCC ;" +
		"border-bottom: 1px solid #CCC;	}" +
		"#graph-info-pntr.top" +
		"{transform: rotate(225deg);}";

	var offsetInfProjY = -20, offsetInfProjX = -10;
	var offsetInfUserY = -20, offsetInfUserX = -10;

	var treeWidthLeaf = 50, treeDistanceLvl = 150;


	var nAniCrnt, nAniMax = 100;
	var dAniDur = 3; // in seconds

	var dAniFrmDur = 1000* dAniDur / nAniMax, dgrRot = 0, tmRotPrev = Date.now();

	var ndT, idT, nd, edg;

	var network = null;
	var ecnt = 0;
	var mainuser_gid;
	var edgsDtT, ndsDtT, ndsObT;


	var i, n, u, t, l, dn, de;
	var nodes, edges;
	var dtInitGraph;

	var elStyleInfoBaloon;
	var _me = this;
	var isFF;


	var curtainIntervalId;

	function ObjectLength_Modern( object ) {
		return Object.keys(object).length;
	}
	function ObjectLength_Legacy( obj ) {
		var length = 0;
		for( var key in obj ) {
			if( obj.hasOwnProperty(key) ) {
				++length;
			}
		}
		return length;
	}

	var ObjectLength = Object.keys ? ObjectLength_Modern : ObjectLength_Legacy;

	function showCurtain(){
		if (curtainIntervalId !== undefined)
		{
			clearInterval(curtainIntervalId);
			curtainIntervalId = undefined;
		}
		$('#graph-curtain').show();
		curtainIntervalId  = setInterval(rotateLoader, 10);
	}

	function hideCurtain(){
		if (curtainIntervalId !== undefined)
		{
			clearInterval(curtainIntervalId);
			curtainIntervalId = undefined;
		}
		$('#graph-curtain').hide();

	}

//--------------------
	function rotateLoader() {
		var div = document.getElementById('graph-loader-wrap'),
			tmCur  = Date.now();
		if (div === undefined)
			return;

		if ((tmCur - tmRotPrev) < 10)
			return;

		dgrRot += 5;
		div.style.webkitTransform = 'rotate('+dgrRot+'deg)';
		div.style.mozTransform    = 'rotate('+dgrRot+'deg)';
		div.style.msTransform     = 'rotate('+dgrRot+'deg)';
		div.style.oTransform      = 'rotate('+dgrRot+'deg)';
		div.style.transform       = 'rotate('+dgrRot+'deg)';

		tmRotPrev = Date.now();
	}

	function showNodeLabels(bShow){
		var arNds=[];
		var arEds=[];
		ndsDtT =  network.body.data.nodes._data;
		edgsDtT =  network.body.data.edges._data;

		if (bShow)
		{
			for (var k in ndsDtT)
			{
				if (ndsDtT[k].nd_type == 'usr' ||
					ndsDtT[k].nd_type == 'mnusr')
				{
					arNds.push({
						id: ndsDtT[k].gid,
						label: ndsDtT[k].info.label ? ndsDtT[k].info.label : ndsDtT[k].info.uname
					});
				}
				else
				if (ndsDtT[k].nd_type == 'prj' )
				{
					arNds.push({
						id: ndsDtT[k].gid,
						label: ndsDtT[k].info.label ? ndsDtT[k].info.label : ndsDtT[k].info.title
					});
				}
			}

			for (var k in edgsDtT){
				if (edgsDtT[k].id !== undefined)
					arEds.push({ id: edgsDtT[k].id,
						label:edgsDtT[k].info.label});
			}
		}
		else
		{
			for (var k in ndsDtT)
			{
				arNds.push({
					id: ndsDtT[k].gid,
					label: undefined
				});
			}
			for (var k in edgsDtT){
				if (edgsDtT[k].id !== undefined)
					arEds.push({ id: edgsDtT[k].id,
						label: undefined});
			}
		}

		try{
			network.body.data.nodes.update(arNds);
			network.body.data.edges.update(arEds);

		}
		catch (e){alert(e);}
	}

	function showNodeImages(bShow){

		var arUsr=[];
		ndsDtT =  network.body.data.nodes._data;
		if (bShow)
		{
			for (var k in ndsDtT)
			{
				if (ndsDtT[k].nd_type == 'usr' )
				{

					var upd = {id: ndsDtT[k].gid,
						image: ndsDtT[k].info.url_ava ? ndsDtT[k].info.url_ava : (OB_AVAS_DEF[ndsDtT[k].group] ? OB_AVAS_DEF[ndsDtT[k].group] : AVA_U_DEF ),
						shape: 'circularImage',
						shapeProperties: {}
					};

					if (ndsDtT[k].info.child_skip !== undefined &&  ndsDtT[k].info.child_skip > -1)
					{
						upd.shapeProperties.borderDashes = [5,10];
					}
					else
					{
						upd.shapeProperties.borderDashes = false;
					}
					arUsr.push(upd);
				}
			}
		}
		else
		{
			for (var k in ndsDtT)
			{
				if (ndsDtT[k].nd_type == 'usr')
				{
					var upd = {id: ndsDtT[k].gid,
						shape: 'icon',
						icon: {
							size: ndsDtT[k].size *2
						}
					};
					if (ndsDtT[k].info.child_skip !== undefined &&  ndsDtT[k].info.child_skip > -1)
					{
						upd.icon.code = '\uf103';
						if (upd.shapeProperties === undefined)
							upd.shapeProperties = {};
						upd.shapeProperties.borderDashes = [5,10];
					}
					else
					{
						upd.icon.code = '\uf102';
						if (upd.shapeProperties === undefined)
							upd.shapeProperties = {};
						upd.shapeProperties.borderDashes = false;
					}

					arUsr.push(upd);
				}
			}
		}

		try{
			network.body.data.nodes.update(arUsr);
		}
		catch (e){alert(e);}
	}

	function updateNodeState(gid){
		ndsDtT =  network.body.data.nodes._data;

		switch (ndsDtT[gid].nd_type)
		{
			case 'usr':
			{
				if (document.getElementById('graph-users-avas').checked)
				{

					var upd = {
						id: gid,
						image: ndsDtT[gid].info.url_ava ? ndsDtT[gid].info.url_ava : (OB_AVAS_DEF[ndsDtT[gid].group] ? OB_AVAS_DEF[ndsDtT[gid].group] : AVA_U_DEF ),
						shape: 'circularImage'};
					if (ndsDtT[gid].info.child_skip !== undefined &&  ndsDtT[gid].info.child_skip > -1)
					{
						if (upd.shapeProperties === undefined)
							upd.shapeProperties = {};
						upd.shapeProperties.borderDashes = [5,10];
					}
					else
					{
						if (upd.shapeProperties === undefined)
							upd.shapeProperties = {};
						upd.shapeProperties.borderDashes = false;
					}

					try{
						network.body.data.nodes.update(upd);
					}
					catch (e){alert(e);}
				}
				else
				{

					var upd = {
						id: gid,
						shape: 'icon',
						icon: {
							size: ndsDtT[gid].size *2
						}
					};
					if (isUnfoldedNode(gid))
					{
						upd.icon.code = '\uf102';
					}
					else
					{
						upd.icon.code = '\uf103';
					}
					try{
						network.body.data.nodes.update(upd);
					}
					catch (e){alert(e);}
				}
			}
				break;
			case 'prj':
			{
				var upd = {
					id: gid,
					shape: 'icon',
					icon: {
						size: ndsDtT[gid].size *2
					}
				};

				if (isUnfoldedNode(gid))
				{
					upd.icon.code = '\uf100';
				}
				else
				{
					upd.icon.code = '\uf101';
				}

				try{
					network.body.data.nodes.update(upd);
				}
				catch (e){alert(e);}
			}
				break;
			case 'mnusr':
			{

				if (isUnfoldedMnPrj())
					$('#graph-maininfo-btn-proj').hide();
				if ( isUnfoldedMnUsr())
					$('#graph-maininfo-btn-user').hide();

				if (isUnfoldedMnPrj() &&  isUnfoldedMnUsr())
				{
					network.body.data.nodes.update({id: mainuser_gid,
						shapeProperties : {borderDashes: false}
					});
				}
				else
				{
					network.body.data.nodes.update({id: mainuser_gid,
						shapeProperties : {borderDashes: [10,25]}
					});
				}
			}
				break;
		}
	}

	function isUnfoldedMnPrj(){

		var t = network.body.data.nodes._data[mainuser_gid];
		if (t=== undefined)
		{
			alert ("Узел " + mainuser_gid + " не существует!")
			return true;
		}

		if (t.info.child_skip_prj !== undefined &&  t.info.child_skip_prj > -1)
			return false;
		return true;

	}

	function isUnfoldedMnUsr(){

		var t = network.body.data.nodes._data[mainuser_gid];
		if (t=== undefined)
		{
			alert ("Узел " + mainuser_gid + " не существует!")
			return true;
		}

		if (t.info.child_skip_usr !== undefined &&  t.info.child_skip_usr > -1)
			return false;
		return true;
	}

	function isUnfoldedNode(gid){

		var t = network.body.data.nodes._data[gid];
		if (t=== undefined)
		{
			alert ("Узел " + gid + " не существует!")
			return true;
		}

		if (t.info.child_skip === undefined)
		{
			alert ("child_skip для узла " + gid + " не определен!")
			return true;
		}

		if (t.info.child_skip < 0)
		{
			return true;
		}
		return false;

	}


// not clearly used
	function destroyClearNetwork() {
		if (network !== null) {
			network.destroy();
			network = null;
		}

		nodes = null, edges = null;

	}


	function saveSettings(){
		sendGraphSettings($( "#graph-select-layout" ).find('option:selected').val(),
			document.getElementById('graph-users-avas').checked,
			document.getElementById('graph-users-labels').checked);
	}

	var _closeAction = function (){
		sendGraphSettings($( "#graph-select-layout" ).find('option:selected').val(),
			document.getElementById('graph-users-avas').checked,
			document.getElementById('graph-users-labels').checked);
		_me.onClose();

	};

	function parseNumber(n) {
		if (!isNaN(parseFloat(n)) && isFinite(n))
			return parseFloat(n);

		return NaN;
	}
	function isNumber(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}




	function AddNodesEdges(dat_nodes, dat_edges,
						   _root_gid, _x, _y){

		if(_root_gid === undefined )
		{
			alert("'_root_gid' is undefined" );
			return;
		}

		nodes = network.body.data.nodes;
		var nd_dat = nodes._data;
		edges = network.body.data.edges;

		l = dat_nodes.length;
		var xp = -400, xu = 400, ypu = 0, tnd, ted;

		if (_x !== undefined)
			xp = xu = _x;
		if (_y !== undefined)
			ypu = _y;

		$('#graph-curtain-text').html("Данные загружаются...");

		var arNodes = [];
		var obNdIds  = {};

		var isImage = document.getElementById('graph-users-avas').checked;
		var isLabel = document.getElementById('graph-users-labels').checked;
		var grps = network.groups.groups;

		for ( i = 0; i < l; i++){

			tnd = dat_nodes[i];
			var nd_id = false;

			// create usual nodes

			switch (tnd.nd_type) {
				case "prj":
				{
					if(nodes._data[tnd.gid] == undefined)
					{
						nd_id = tnd.gid;

						tnd.id = tnd.gid;
						tnd.group = 'p_'+tnd.p_type;
						tnd.title =  tnd.info.title ;
						tnd.x = xp;
						tnd.y = ypu;

						if (isLabel)
						{
							tnd.label = tnd.info.label ? tnd.info.label : tnd.info.title;
						}
						else
							tnd.label = undefined;

						tnd.shape = 'icon';
						if (tnd.icon === undefined)
							tnd.icon = {};
						if (!tnd.size)
							tnd.size = grps[tnd.group].size;
						tnd.icon.size =  tnd.size  *2;

						if (tnd.info.child_skip !== undefined && tnd.info.child_skip > -1)
						{
							tnd.icon.code = '\uf101';
						}
						else
						{
							tnd.info.child_skip = -1;
							tnd.icon.code = '\uf100';
						}

					}
				}
					break;
				case "usr":
				{
					if(nodes._data[tnd.gid] == undefined)
					{


						if (tnd.size !== undefined)
							if (isNumber(tnd.size))
								tnd.size = parseFloat(tnd.size);
						//	else alert("узел с id " + tnd.gid +" имеет некорректное число в \"size\": "+tnd.size);
						if (tnd.x !== undefined)
							if (isNumber(tnd.x))
								tnd.x = parseFloat(tnd.x);
						//else alert("узел с id " + tnd.gid +" имеет некорректное число в \"x\": - "+tnd.x);
						if (tnd.y !== undefined)
							if (isNumber(tnd.y))
								tnd.y = parseFloat(tnd.y);
						//else alert("узел с id " + tnd.gid +" имеет некорректное число в \"y\": - "+tnd.y);
						if (tnd.mass !== undefined)
							if (isNumber(tnd.mass))
								tnd.mass = parseFloat(tnd.mass);
						//else alert("узел с id " + tnd.gid +" имеет некорректное число в \"mass\": - "+tnd.mass);

						if (tnd.borderWidth !== undefined)
							if (isNumber(tnd.borderWidth))
								tnd.borderWidth = parseFloat(tnd.borderWidth);
						//else alert("узел с id " + tnd.gid +" имеет некорректное число в \"borderWidth\": - "+tnd.borderWidth);
						if (tnd.borderWidthSelected !== undefined)
							if (isNumber(tnd.borderWidthSelected))
								tnd.borderWidthSelected = parseFloat(tnd.borderWidthSelected);
						//else alert("узел с id " + tnd.gid +" имеет некорректное число в \"borderWidthSelected\": - "+tnd.borderWidthSelected);



						nd_id = tnd.gid;

						tnd.id = tnd.gid;
						tnd.group = 'u_'+ tnd.p_type;
						tnd.title = tnd.info.uname ;
						tnd.x = xu;
						tnd.y = ypu;

						if ( tnd.lvl == -2)
							tnd.info.child_skip = -1;

						if (isImage){
							tnd.shape = 'circularImage';
							tnd.image = (tnd.info.url_ava) ? tnd.info.url_ava : (OB_AVAS_DEF[tnd.group] ? OB_AVAS_DEF[tnd.group] : AVA_U_DEF );
						}
						else{
							tnd.shape = 'icon';
							if (tnd.icon === undefined)
								tnd.icon = {};
							if (!tnd.size)
								tnd.size = grps[tnd.group].size;
							tnd.icon.size =  tnd.size  *2;

							if (tnd.info.child_skip !== undefined && tnd.info.child_skip > -1)
							{
								tnd.icon.code = '\uf103';
							}
							else
							{
								tnd.info.child_skip = -1;
								tnd.icon.code = '\uf102';
							}
						}

						if (isLabel)
						{
							tnd.label = tnd.info.label ? tnd.info.label : tnd.info.uname;
						}
						else
							tnd.label = undefined;

					}
				}
					break;
				default:
					break;
			}


			if(obNdIds[tnd.gid] == undefined &&
				nodes._data[tnd.gid] == undefined)
			{
				if (isDebug)
					tnd.label = tnd.id;
				arNodes.push(tnd);
				obNdIds[tnd.gid] = arNodes.length - 1;
			}
			else
			{
				if (nodes._data[tnd.gid] == undefined)
				{

					if (arNodes[obNdIds[tnd.gid]].lvl < 0 )//&& tnd.lvl > 0)
					{
						arNodes[obNdIds[tnd.gid]].lvl = tnd.lvl ;
					}
					else
						arNodes[obNdIds[tnd.gid]].lvl = Math.min(arNodes[obNdIds[tnd.gid]].lvl , tnd.lvl );
				}
			}
		}



		try{
			nodes.add(arNodes);
		}
		catch (e){ alert(e);}

		// create usual edges
		l = dat_edges.length;

		var arEdges = [];
		var obEdIds = {};

		for ( i = 0; i < l; i++){

			ted = dat_edges[i];

			ted.id =  'e'+(++ecnt);
			if(obEdIds[ted.from + '_' +ted.to] == undefined &&
				(edges.edgesFromArrTo[ted.from] == undefined ||
				edges.edgesFromArrTo[ted.from][ted.to] == undefined))
			{
				ted.arrows = {};
				ted.arrows.middle = {};

				ted.arrows.middle.enabled = true;
				if (ted.font === undefined)
					ted.font = {};
				ted.font.align = 'top';

				arEdges.push(ted);
				obEdIds[ted.from + '_' +ted.to] = ted;

				if (ted.info === undefined)
					ted.info = {};

				if (ted.info.label === undefined)
					if (ted.label !== undefined){
						ted.info.label = ted.label;
					}

				if (isLabel)
					ted.label = ted.info.label;
				else
					ted.label = undefined;
			}
		}


		// set style for edges of user-project tree brunch
		// and prepare for breadthing older edges
		for( var n in obEdIds){
			nd_dat[obEdIds[n].to].info.time_date = obEdIds[n].info.time_date;

			if (nd_dat[obEdIds[n].from].lvl == undefined)
				continue;

			if (nd_dat[obEdIds[n].from].lvl < 0 )
			{
				obEdIds[n].dashes = true;
				if (obEdIds[n].color === undefined)
					obEdIds[n].color = {};
				obEdIds[n].color.inherit = 'from';
			}

		}
		// set width accordant to date
		var obFromToUsrs ={};
		for( var n in obEdIds){

			if (nd_dat[obEdIds[n].to].lvl > -1
				|| nd_dat[obEdIds[n].to].lvl === undefined)
			{
				if (obFromToUsrs[obEdIds[n].from] === undefined)
				{
					obFromToUsrs[obEdIds[n].from] = {};
				}
				obFromToUsrs[obEdIds[n].from][obEdIds[n].to] = obEdIds[n];
			}
		}

		try{
			testDatesRecurs(_root_gid, obFromToUsrs, nd_dat);
			edges.add(arEdges);
		}
		catch (e){ alert(e);}

		ndsObT = network.body.nodes;
		for (var k in ndsObT){
			if (ndsObT[k].x_new === undefined)
			{
				ndsObT[k].x_new = ndsObT[k].x_old = ndsObT[k].x;
				ndsObT[k].y_new = ndsObT[k].y_old = ndsObT[k].y;
			}
		}


		// now calculate levels...

		$('#graph-items-count').html("Всего узлов: "+ network.body.data.nodes.length +". Всего связей: "+ network.body.data.edges.length );

		var eToFrm = edges.edgesToArrFrom;

		l = dat_nodes.length;

		var sErr = "";
		for (var i = 0; i<l; i++){
			if(dat_nodes[i].lvl == undefined){
				if (sErr.length != 0)
				{
					sErr += ", ";
				}
				if (i % 5 == 0)
					sErr += "\n";
				sErr += dat_nodes[i].gid;
			}
		}

		if (sErr.length != 0)
		{
			//alert("Узлы не имеют свойства 'lvl': " + sErr );
			return;
		}

		for (var i = 0; i<l; i++){
			if(dat_nodes[i].lvl < 0){

				nd_dat[dat_nodes[i].gid].minused = "catched";

				for (var z in edges.edgesFromArrTo[dat_nodes[i].gid])
				{
					nd_dat[dat_nodes[i].gid].tr_prnt = z;
					break;
				}
				if 	(nd_dat[dat_nodes[i].gid].tr_prnt === undefined)
				{
					//alert("Для узла '"+ dat_nodes[i].gid + "' не найден узел-предок.");
					return;
				}
				else
				{
					//console.log("nd_dat['" + dat_nodes[i].gid +"'].tr_prnt === " + nd_dat[dat_nodes[i].gid].tr_prnt );
				}



			}
			else
			if(dat_nodes[i].lvl > 0){

				if (nd_dat[dat_nodes[i].gid].tr_prnt !== undefined)
					continue;

				if(_root_gid != mainuser_gid)
				{
					if(edges.edgesFromArrTo[_root_gid][dat_nodes[i].gid] == undefined)
					{
						//alert("Узeл from ("+ _root_gid + ") и узел to (" + dat_nodes[i].gid +
						//	") не имеют связи.");
						return;
					}
					if(nd_dat[_root_gid].lvl + 1 != nd_dat[dat_nodes[i].gid].lvl )
					{
						//alert("Узeл from ("+ _root_gid + ") и узел to (" + dat_nodes[i].gid +
						//	") имеют не последовательные значения 'lvl': "+
						//	nd_dat[_root_gid].lvl +	" и " + nd_dat[dat_nodes[i].gid].lvl);
						return;
					}

					nd_dat[dat_nodes[i].gid].tr_prnt = _root_gid;
				}
				else
				{
					var idPrn;

					for (var z in edges.edgesToArrFrom[dat_nodes[i].gid])
					{
						if(nd_dat[z].lvl +1 == nd_dat[dat_nodes[i].gid].lvl)
						{
							idPrn = z;
							break;
						}
					}

					if(idPrn == "")
					{
						//alert("Для узла '"+ dat_nodes[i].gid + "' не найден узел-предок.");
						return;
					}

					nd_dat[dat_nodes[i].gid].tr_prnt = idPrn;
				}

			}
			else
			{
				//alert("Узел '"+ dat_nodes[i].gid + "' имеет 'lvl' равный '"+dat_nodes[i].lvl +"'.");
				return;
			}
		}


		l = 0;
		for (var z in nd_dat){
			if(nd_dat[z].tr_prnt == undefined){
				if (sErr.length != 0)
				{
					sErr += ", ";
				}
				if (l % 5 == 0)
					sErr += "\n";
				sErr += z ;
				l++;
			}
		}

		if (sErr.length != 0)
		{
			//	alert("Узлы не имеют родительского узла 'tr_prnt': " + sErr);
		}

		l = dat_nodes.length;
		for (var i = 0; i<l; i++){
		}
		for (var z in nd_dat){
			if (nd_dat[z].twigs == undefined)
				nd_dat[z].twigs = {gid:z, data:{}};
		}

		if (nd_dat[mainuser_gid].twigs_minus == undefined)
			nd_dat[mainuser_gid].twigs_minus = {gid:mainuser_gid, data:{}};
		if (nd_dat[mainuser_gid].twigs_plus == undefined)
			nd_dat[mainuser_gid].twigs_plus = {gid:mainuser_gid, data:{}};

		for (var z in nd_dat){
			if (z == mainuser_gid)
				continue;
			if (nd_dat[z].tr_prnt == mainuser_gid){
				if(nd_dat[z].lvl > 0)
					if (nd_dat[mainuser_gid].twigs_plus.data[z] == undefined)
						nd_dat[mainuser_gid].twigs_plus.data[z] = nd_dat[z];
				if(nd_dat[z].lvl < 0)
					if (nd_dat[mainuser_gid].twigs_minus.data[z] == undefined)
						nd_dat[mainuser_gid].twigs_minus.data[z] = nd_dat[z];
			}
			else
			if (nd_dat[nd_dat[z].tr_prnt].twigs.data[z] == undefined)
				nd_dat[nd_dat[z].tr_prnt].twigs.data[z] = nd_dat[z];
		}


		if (_x !== undefined)
		{
			repositionGraph();
		}

	}

	function testDatesRecurs(ndFrom, obFrmTo, nd_dat, tm){

		if (obFrmTo[ndFrom] === undefined
			||	nd_dat[ndFrom].dt_hlp === undefined
		)
		{
			nd_dat[ndFrom].dt_hlp = tm;
			return;
		}

		var ed;
		for (var k in obFrmTo[ndFrom]){
			var ed = obFrmTo[ndFrom][k];
			//if ((ndFrom.nd_type == "mnusr") &&
			if (nd_dat[ndFrom].dt_hlp > ed.info.time_date)
				return;

			ed.width = 5;

			ed.shadow = {
				enabled: true,
				color: 'rgb(0,0,0)',
				x: 	2,
				y: 2};

			nd_dat[ed.to].dt_hlp = ed.info.time_date;

			testDatesRecurs(k, obFrmTo, nd_dat, ed.info.time_date);
		}
	}



//---------------
	var itrCount = 0;
	var itrMax = 5000;
	function animateReposition(newCntr, newScl){

		nAniCrnt = 1;

		var nds = network.body.nodes;
		var nd;
		for(var k in nds){
			nd = nds[k];
			if (nd.x_new !== undefined)
			{
				nd.x_d = nd.x_new - nd.x_old;
				nd.y_d = nd.y_new - nd.y_old;
			}
		}

		if (newCntr !== undefined && newScl !== undefined)
		{
			network.moveTo({
				position: newCntr,
				scale: newScl,
				animation: {
					duration: nAniMax * dAniFrmDur,
					easingFunction: "easeInOutCubic"
				}
			});
		}

		updateAnimateNodesPosition();
	}

	function updateAnimateNodesPosition(l){
		if (nAniCrnt > nAniMax)
		{
			if ($("#graph-select-layout").find("option:selected").val() ==	 "binary_tree"){
				for (var k in network.body.nodes)
					network.moveNode(k, network.body.nodes[k].x, network.body.nodes[k].y);
			}

			return;
		}

		if (nAniCrnt == nAniMax)
			l=90;

		var nds = network.body.nodes;
		var sb = network.body.subPlus;
		var nd;
		var tmRat = nAniCrnt / nAniMax;
		for(var k in nds){
			nd = nds[k];
			if (nd.x_old !== undefined){
				nd.x = nd.x_old + nd.x_d * swing(tmRat);
				nd.y = nd.y_old + nd.y_d * swing(tmRat);
			}

		}

		if (isInitFit || isReposFit)
			network.fit();
		else
			network.redraw();
		++nAniCrnt;
		setTimeout(updateAnimateNodesPosition, dAniFrmDur);
	}

	var objSwingRet = {};
	function swing(t) {
		if (objSwingRet[t] === undefined )
			objSwingRet[t] = (-Math.cos(t*Math.PI)/2) + 0.5
		return objSwingRet[t];
	}
//--------------


	var isInitFit = false;
	var isReposFit;

	function extendRec(rcAdd, rcBs)
	{
		if (rcBs.top > rcAdd.top )
			rcBs.top = rcAdd.top;
		if (rcBs.left > rcAdd.left )
			rcBs.left = rcAdd.left;
		if (rcBs.right < rcAdd.right )
			rcBs.right = rcAdd.right;
		if (rcBs.bottom < rcAdd.bottom )
			rcBs.bottom = rcAdd.bottom;
	}

	function isInCanvas(rcTst){
		var u = $('#graph-container canvas');

		if (u.length == 0)
		{
			alert("Element '#graph-container canvas' not exists!");
			return false;
		}
		if (u.length > 1)
		{
			alert("Found some number of '#graph-container canvas' elements. It must to be a single instance!");
			return false;
		}


		var cnvW = u.width();
		var cnvH = u.height();

		var pntTL = {
			y: rcTst.top,
			x: rcTst.left
		};
		var pntBR = {
			y: rcTst.bottom,
			x: rcTst.right
		};

		pntTL = network.canvasToDOM(pntTL);
		pntBR = network.canvasToDOM(pntBR);

		if (pntTL.x < 0 || pntTL.y < 0)
		{
			return false;
		}

		if (pntBR.x > cnvW || pntBR.y > cnvH)
		{
			return false;
		}

		return true;
	}


	function repositionGraph(init){

		nAniCrnt = 9999999999;
		isInitFit = (init === 1);

		$("#graph-wrapper").find(".graph-info-baloon").hide();

		switch ($("#graph-select-layout").find("option:selected").val()){
			case "binary_tree":
				repositionAsBinaryTree(init);
				break;
			case "circle":
				repositionAsCircle(init);
				break;
			case "cluster":
				repositionAsCluster(init);
				break;

			case "physics":
			default :

				repositionAsPhysics(init);
				break;
		}

		hideCurtain();

		showNodeImages(document.getElementById('graph-users-avas').checked);
	}

	function repositionAsBinaryTree(init){
		var options = {
			edges:{smooth: {}},
			physics: {
				enabled: false
			}
		};
		if (network.body.data.nodes.length + network.body.data.edges.length < 500)
		{
			options.edges.smooth.enabled = true;
			options.edges.smooth.type = "continuous";
		}
		else
			options.edges.smooth.enabled = false;


		network.setOptions(options);

		createTree(mainuser_gid, network.body.data);

		placeTreeBinary(network.body.data.treeItems, treeWidthLeaf, treeDistanceLvl);

		ndsObT = network.body.nodes;
		if (init === 1)
		{
			for (var k in ndsObT){
				if (ndsObT[k].x_new !== undefined)
				{
					network.body.nodes[k].x = ndsObT[k].x_new;
					network.body.nodes[k].y = ndsObT[k].y_new;
				}
			}
			network.fit();
		}
		else
		{
			var ndIndxs = network.body.nodeIndices;

			var nds = network.body.nodes;

			var rcBndStrt, rcBndEnd;
			var newC, newS;
			if (ndIndxs.length > 0)
			{
				var sId = ndIndxs[0];
				rcBndStrt = network.getBoundingBox(ndIndxs[0]);

				rcBndStrt.w2 = (rcBndStrt.right - rcBndStrt.left)/2;
				rcBndStrt.h2 = (rcBndStrt.bottom - rcBndStrt.top)/2;

				rcBndEnd = {
					top: nds[sId].y_new - rcBndStrt.h2,
					left: nds[sId].x_new - rcBndStrt.w2,
					right: nds[sId].x_new + rcBndStrt.h2,
					bottom: nds[sId].y_new + rcBndStrt.h2
				};

				var bx, bx1;


				for ( i = ndIndxs.length; --i > 0; ){
					sId = ndIndxs[i];
					bx =network.getBoundingBox(sId);
					bx.w2 = (bx.right - bx.left)/2;
					bx.h2 = (bx.bottom - bx.top)/2;
					extendRec(bx, rcBndStrt);

					bx1 = {
						top: nds[sId].y_new - bx.h2,
						left: nds[sId].x_new - bx.w2,
						right: nds[sId].x_new + bx.h2,
						bottom: nds[sId].y_new + bx.h2
					}
					extendRec(bx1, rcBndEnd);
				}

				if (isInCanvas(rcBndStrt) && (!isInCanvas(rcBndEnd))){

					var xDistance = Math.abs(rcBndEnd.right - rcBndEnd.left) * 1.1;
					var yDistance = Math.abs(rcBndEnd.bottom - rcBndEnd.top) * 1.1;

					var xZoomLevel = network.canvas.frame.canvas.clientWidth / xDistance;
					var yZoomLevel = network.canvas.frame.canvas.clientHeight / yDistance;

					newS = xZoomLevel <= yZoomLevel ? xZoomLevel : yZoomLevel;
					newC = {
						x: (rcBndEnd.right + rcBndEnd.left)/2,
						y: (rcBndEnd.bottom + rcBndEnd.top)/2
					};
				}
				animateReposition(newC, newS);
			}
		}


	}

	function createTree(mnUserId, netData){


		var frm = netData.edges.edgesFromArrTo;
		var nd_dat = netData.nodes._data
		var edges = netData.edges;
		var treeRef;

		if (netData.treeItems === undefined)
		{
			netData.treeItems = {rootMinus: {id: mnUserId, lvl: 0, twigs:{}},
				rootPlus: {id: mnUserId, lvl: 0, twigs:{}}};
		}

		treeRef = netData.treeItems;

		// set level for MY_PROJECTS nodes
		var arPrjId = [];
		for (var t in nd_dat)
		{

			ndT = nd_dat[t];
			if (ndT.nd_type == 'usr')
			{
				ndT.tr_us = 0;
				ndT.tr = 1;
			}
			else
			{
				ndT.tr_us = 1;
				idT = ndT.id;
				if 	(ndT.nd_type == 'prj')
				{

					arPrjId.push(idT);
					if (treeRef.rootMinus.twigs[idT] === undefined)
					{
						treeRef.rootMinus.twigs[idT] = {id: idT, lvl: -1, twigs: {} };
					}
				}
			}

			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}
		}



		// set levels for project helpers nodes


		var branch = treeRef.rootMinus.twigs;
		for (var twId in branch)
		{
			t = netData.edges.edgesToArrFrom[twId];
			for (var key in t ){
				if (nd_dat[key].nd_type != 'usr')
					continue;
				if (branch[twId].twigs[key] === undefined
					&& nd_dat[key].lvl == -2)
				{
					branch[twId].twigs[key] = {id: key, lvl: -2, twigs: {}};
					nd_dat[key].tr_us = 1;
				}


				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}


			}
		}

		createBrunchPlus(treeRef.rootPlus, 0, frm, nd_dat);

	}

	function createBrunchPlus(trPrnt, lvl, frmTo, nd_dt){

		++lvl;
		if (trPrnt.id == "u39")
		{
			k = true;
		}

		for (var m in frmTo[trPrnt.id])
		{

			if ((nd_dt[m].tr != 1) ||
				(nd_dt[m].tr_us))
				continue;

			if (nd_dt[m].tr_prnt == trPrnt.id){
				if (trPrnt.twigs[m] === undefined)
					trPrnt.twigs[m] = {id: m, lvl: lvl, twigs: {}}

				if (nd_dt[m] && nd_dt[m].lvl)
					trPrnt.twigs[m].lvl = nd_dt[m].lvl;
				else
					trPrnt.twigs[m].lvl = lvl;

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}
			}

		}

		for (var m in trPrnt.twigs)
		{
			if ((nd_dt[m].tr != 1) ||
				(nd_dt[m].tr_us))
				continue;
			nd_dt[m].tr_us = 1;

			createBrunchPlus(trPrnt.twigs[m], lvl, frmTo, nd_dt);

			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}

		}
	}

	var nds_data, nds_act;

	function placeTreeBinary(tree, wdhLeaf, dstLvl){

		nds_data = network.body.data.nodes._data;
		nds_act = network.body.nodes;
		var pnt = {x: 0, y: 0};



		countLeavesNodes(nds_data[mainuser_gid].twigs_plus );
		countLeavesNodes(nds_data[mainuser_gid].twigs_minus);


		normalizeTreeLevelHeightNodes(nds_data[mainuser_gid].twigs_plus,	dstLvl, 3, 0.7);
		normalizeTreeLevelHeightNodes(nds_data[mainuser_gid].twigs_minus,	dstLvl, 3, 0.7);



		nds_data[mainuser_gid].twigs_plus.prefLvlHeight = dstLvl;
		nds_data[mainuser_gid].twigs_minus.prefLvlHeight = dstLvl;

		setXnodes(nds_data[mainuser_gid].twigs_plus, 0);
		setXnodes(nds_data[mainuser_gid].twigs_minus, 0);

		setYnodes(nds_data[mainuser_gid].twigs_plus, 0 , wdhLeaf);
		setYnodes(nds_data[mainuser_gid].twigs_minus, 0 , wdhLeaf);

		nds_data[mainuser_gid].twigs_plus.posX =	tree.rootMinus.posY = 0;
		nds_data[mainuser_gid].twigs_plus.posX =	tree.rootPlus.posY = 0;

		nds_data[mainuser_gid].twigs_plus.posX = nds_data[mainuser_gid].twigs_plus.posY =
			nds_data[mainuser_gid].twigs_minus.posX = nds_data[mainuser_gid].twigs_minus.posY = 0;


		calculatePlaceBinaryTreeBranchNodes(nds_data[mainuser_gid].twigs_plus, network);
		calculatePlaceBinaryTreeBranchNodes(nds_data[mainuser_gid].twigs_minus, network);



	}

	function normalizeTreeLevelHeightNodes(twg, minHeight, minCountRize, ratioRize){

		var prefHgt = minHeight, hgt = 0, l = 0;
		if (twg.data)
			l = ObjectLength(twg.data);

		if (l > 0)
		{
			for (var k in twg.data)
			{
				normalizeTreeLevelHeightNodes(twg.data[k].twigs, minHeight, minCountRize, ratioRize);

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}

			}

			for (var k in twg.data)
			{
				if (hgt < twg.data[k].twigs.prefLvlHeight)
					hgt = twg.data[k].twigs.prefLvlHeight;

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}
			}

			for (var k in twg.data)
			{
				twg.data[k].twigs.prefLvlHeight = hgt ;

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}

			}

			if (ObjectLength(twg.data) >= minCountRize )
			{
				prefHgt = l * ratioRize * minHeight;
			}
			twg.prefLvlHeight  = prefHgt;
		}
		else twg.prefLvlHeight  = minHeight;

	}

	function setYnodes(twig, pY, wdh){

		var itmT;
		var cnt = 0;
		if (ObjectLength(twig.data) > 0)
		{
			for (var k in twig.data)
			{
				cnt += twig.data[k].twigs.leaves;
			}
			var stY = pY - (cnt*wdh / 2) ;
			cnt = 0;
			for (var k in twig.data)
			{
				itmT = twig.data[k].twigs;
				itmT.bndY = stY + cnt*wdh;
				itmT.posY = itmT.bndY + twig.data[k].twigs.leaves*wdh/2;
				setYnodes(itmT, itmT.posY, wdh);
				cnt += twig.data[k].twigs.leaves;


				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}

			}
		}
		else twig.posY = pY ;
	}


	function setXnodes(twig, shiftX){

		if (nds_data[twig.gid].lvl > 0)
			twig.posX = shiftX;
		else
			twig.posX = -shiftX;

		var shiftNext = shiftX + twig.prefLvlHeight;

		if (ObjectLength(twig.data) > 0)
		{
			for (var k in twig.data)
			{
				setXnodes(twig.data[k].twigs, shiftNext);


				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}


			}

		}
	}

	function countLeavesNodes(twgs){

		twgs.leaves = 0;
		if (ObjectLength(twgs.data) > 0)
			for (var k in twgs.data)
			{
				countLeavesNodes(twgs.data[k].twigs) ;
				twgs.leaves += twgs.data[k].twigs.leaves;

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}
			}
		else twgs.leaves  = 1;
	}



	function calculatePlaceBinaryTreeBranchNodes(twig, network){
		if (ObjectLength(twig.data) > 0)
		{
			for (var k in  twig.data)
			{
				calculatePlaceBinaryTreeBranchNodes(twig.data[k].twigs, network);

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}
			}
		}

		nds_act[twig.gid].x_old = nds_act[twig.gid].x;
		nds_act[twig.gid].y_old = nds_act[twig.gid].y;

		nds_act[twig.gid].x_new = twig.posX;
		nds_act[twig.gid].y_new = twig.posY;
	}


//--------------
	function repositionAsCircle(init){
		/* fixme: circling not realized
		 and	 createTree(mainuser_gid, network.data);	 */
	}

	function repositionAsPhysics(init){

		var options = {
			edges:{smooth: {}},
			physics: {
				enabled: true,
				maxVelocity: 14,
				minVelocity: 0.75
			}
		};
		if (network.body.data.nodes.length + network.body.data.edges.length < 500)
		{
			options.edges.smooth.enabled = true;
			options.edges.smooth.type = "dynamic";
		}
		else
			options.edges.smooth.enabled = false;



		network.setOptions(options);

		if (init === 1)
		{
			network.stabilize();
			network.fit();
		}
		else
			network.startSimulation();

	}
//--------------
	function repositionAsCluster(init){

		var options = {
			edges:{smooth: {}},
			physics: {
				enabled: false
			}
		};
		if (network.body.data.nodes.length + network.body.data.edges.length < 500)
		{
			options.edges.smooth.enabled = true;
			options.edges.smooth.type = "continuous";
		}
		else
			options.edges.smooth.enabled = false;


		network.setOptions(options);

		// count project groups

		// FIXME: maximal gropus count == 18 !!! NotMore

		var ratNdsCount = Math.sqrt(network.body.data.nodes.length) * 0.1;
		if (ratNdsCount < 1)
			ratNdsCount = 1;
		var radGex = 500 * ratNdsCount;
		var szGex = 200 * ratNdsCount;
		var frcGex = 0.05;

		var grps = {};
		ndsDtT = network.body.data.nodes._data;
		ndsObT = network.body.nodes;

		var ndsShow = {};
		for (k in ndsDtT)

		{
			if (ndsDtT[k].nd_type != 'prj' &&
				ndsDtT[k].nd_type != 'usr' &&
				ndsDtT[k].nd_type != 'mnusr')
				continue;

			ndsShow[k] = ndsObT[ndsDtT[k].id];

			if (ndsDtT[k].nd_type != 'mnusr')
			{
				if (grps[ndsDtT[k].p_type] === undefined)
				{
					grps[ndsDtT[k].p_type] = {};
				}
				grps[ndsDtT[k].p_type][k] = ndsDtT[k];
			}
			else
				ndsObT[ndsDtT[k].id].x_new =
					ndsObT[ndsDtT[k].id].y_new =
						ndsObT[ndsDtT[k].id].x_old =
							ndsObT[ndsDtT[k].id].y_old = 0;


			itrCount++;
			if(itrCount > itrMax){
				rotateLoader();
				itrCount = 0;
			}




		}


		var arPosGex = [];

		var p;
		for (i = 0; i < 6; i++){
			p = rotateAroundZero(1, Math.PI * 2 * i/6);
			arPosGex.push({x: p.x, y: p.y});
		}

		for (i = 0; i < 6; i++){
			p = rotateAroundZero(2,  Math.PI * 2 * i/6);
			arPosGex.push({x: p.x, y: p.y});
		}

		for (i = 0; i < 6; i++){
			arPosGex.push(
				{
					x: arPosGex[6+i].x + (arPosGex[6+i].x - arPosGex[6+i +1].x)/2,
					y: arPosGex[6+i].y + (arPosGex[6+i].y - arPosGex[6+i +1].y)/2}
			);
		}

		if (ObjectLength(grps) > 18)
			alert("Maximal groups count <= 18 !!\nBecause of gexagon two level position!\n Current count - " + ObjectLength(grps) );

		for (i = 0; i < arPosGex.length; i++){
			arPosGex[i].x = arPosGex[i].x * radGex;
			arPosGex[i].y = arPosGex[i].y * radGex;

		}

		l = ObjectLength(grps);
		if (l <7)
			arPosGex.length = 6;
		shuffle(arPosGex);

		var i = 0;

		for( k in grps){
			for(t in grps[k])
			{
				ndsObT[t].x_old = ndsObT[t].x;
				ndsObT[t].y_old = ndsObT[t].y;

				pTm = getRandomGex(szGex);
				ndsObT[t].x_new = arPosGex[i].x + pTm.x;
				ndsObT[t].y_new = arPosGex[i].y + pTm.y;

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}
			}
			i++;
		}

		var z = network.body.data.edges.edgesFromArrTo;

		for( var f in z){
			if (ndsDtT[f].nd_type == 'mnusr')
				continue;

			if (ndsShow[f] === undefined)
				continue;

			for(var t in z[f])
			{
				if (ndsShow[t] === undefined)
					continue;

				if (ndsObT[f].x_new < ndsObT[t].x_new)
					ndsObT[t].x_new -= Math.abs(ndsObT[f].x_new - ndsObT[t].x_new) * frcGex;
				else
					ndsObT[t].x_new += Math.abs(ndsObT[f].x_new - ndsObT[t].x_new) * frcGex;
				if (ndsObT[f].y_new < ndsObT[t].y_new)
					ndsObT[t].y_new -= Math.abs(ndsObT[f].y_new - ndsObT[t].y_new) * frcGex;
				else
					ndsObT[t].y_new += Math.abs(ndsObT[f].y_new - ndsObT[t].y_new) * frcGex;

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}
			}
		}

		if (init === 1)
		{
			for (var k in ndsObT){
				if (ndsObT[k].x_new !== undefined)
				{
					network.body.nodes[k].x = ndsObT[k].x_new;
					network.body.nodes[k].y = ndsObT[k].y_new;
				}

				itrCount++;
				if(itrCount > itrMax){
					rotateLoader();
					itrCount = 0;
				}
			}
			network.redraw();
			network.fit();
		}
		else
		{
			var ndIndxs = network.body.nodeIndices;

			var nds = network.body.nodes;

			var rcBndStrt, rcBndEnd;
			var newC, newS;
			if (ndIndxs.length > 0)
			{
				var sId = ndIndxs[0];
				rcBndStrt = network.getBoundingBox(ndIndxs[0]);

				rcBndStrt.w2 = (rcBndStrt.right - rcBndStrt.left)/2;
				rcBndStrt.h2 = (rcBndStrt.bottom - rcBndStrt.top)/2;

				rcBndEnd = {
					top: nds[sId].y_new - rcBndStrt.h2,
					left: nds[sId].x_new - rcBndStrt.w2,
					right: nds[sId].x_new + rcBndStrt.h2,
					bottom: nds[sId].y_new + rcBndStrt.h2
				};

				var bx, bx1;


				for ( i = ndIndxs.length; --i > 0; ){
					sId = ndIndxs[i];
					bx =network.getBoundingBox(sId);
					bx.w2 = (bx.right - bx.left)/2;
					bx.h2 = (bx.bottom - bx.top)/2;
					extendRec(bx, rcBndStrt);

					bx1 = {
						top: nds[sId].y_new - bx.h2,
						left: nds[sId].x_new - bx.w2,
						right: nds[sId].x_new + bx.h2,
						bottom: nds[sId].y_new + bx.h2
					}
					extendRec(bx1, rcBndEnd);
				}

				if (isInCanvas(rcBndStrt) && (!isInCanvas(rcBndEnd))){

					var xDistance = Math.abs(rcBndEnd.right - rcBndEnd.left) * 1.1;
					var yDistance = Math.abs(rcBndEnd.bottom - rcBndEnd.top) * 1.1;

					var xZoomLevel = network.canvas.frame.canvas.clientWidth / xDistance;
					var yZoomLevel = network.canvas.frame.canvas.clientHeight / yDistance;

					newS = xZoomLevel <= yZoomLevel ? xZoomLevel : yZoomLevel;
					newC = {
						x: (rcBndEnd.right + rcBndEnd.left)/2,
						y: (rcBndEnd.bottom + rcBndEnd.top)/2
					};
				}
				animateReposition(newC, newS);
			}
		}


	}



	var  rt_p = {'x':0, 'y':0} ;

	function rotateAroundZero(lng, ang){

		rt_p.x = lng * Math.sin(ang);
		rt_p.y = lng * Math.cos(ang);

		return rt_p;
	}

	var pTm = {x:0, y:0};
	function getRandomGex(szGex){

		do {
			pTm.x = Math.random();
			pTm.y = Math.random();
		}while(!ptInsideGex(pTm))

		pTm.x = pTm.x * 2 * szGex - szGex;
		pTm.y = pTm.y * 2 * szGex - szGex;
		return pTm;
	}

	var gexPlg = [{	x: 0.5,	y: 0},
		{x: 0.935,	y: 0.25},
		{x: 0.935,	y: 0.75},
		{x: 0.5,    y: 1},
		{x: 0.07,	y: 0.75},
		{x: 0.07,	y: 0.25},
		{x: 0.5,	y: 0}];


	function ptInsideGex ( pt)
	{
		var pI , pY;
		var bInside = false;
		for ( i = 1; i < 7; i++)
		{
			pI = gexPlg[i];
			pY = gexPlg[i-1];

			if ((((pI.y <= pt.y) && (pt.y < pY.y)) || ((pY.y <= pt.y) && (pt.y < pI.y)))
				&& (pt.x < (pY.x - pI.x) * (pt.y - pI.y) / (pY.y - pI.y) + pI.x))
				bInside = !bInside;
		}
		return bInside;
	}

	function shuffle(a) {
		var j, x, i;
		for (i = a.length; i; i -= 1) {
			j = Math.floor(Math.random() * i);
			x = a[i - 1];
			a[i - 1] = a[j];
			a[j] = x;
		}
	}
//----------------


	function askNodeUserInfo(u_gid){

		if(u_gid.charAt(0) != 'u')
			return;

		var postDat = {gid: u_gid, request: "node_user_info"};

		jQuery.ajax({
			method: "POST",
			url: REQAJXURL,
			data: postDat,
			dataType: "json",
			success: function(data){
				if (!data)
				{
					alert("Returned empty data-object");
					return;
				};
				if (data.status == undefined || data.status != "ok")
				{
					alert("Error status - " + data.status);
					return;
				};
				showUserInfo(data);
			},
			error: function( jqXHR, textStatus, errorThrown )
			{
				alert("Error: " + textStatus + "\n" + errorThrown);
			}
		})

	}

	function showUserInfo(data){
		var d  = data.data;

		var pgid = d.source_node.gid;
		var ndDt = network.body.data.nodes._data[pgid];


		if (ndDt){

			var elInfoAbout = $('#graph-info-div');
			elInfoAbout.attr("data-node-id", pgid);

			if (d.content == undefined || d.content.indexOf('<') == -1)
			{

				if (elStyleInfoBaloon === undefined){
					elStyleInfoBaloon  = document.createElement('style');
					elStyleInfoBaloon.innerHTML = style_def;

					document.body.appendChild(elStyleInfoBaloon);
				}

				var unm = ndDt.info.uname;
				if (unm == undefined)
					unm = ndDt.label;

				var sCnt = '<div class="graph-definf-content" >' +
					'<div class="definf-ava"><a href="'+
					ndDt.info.url_view + '"><img src="' +
					ndDt.info.url_ava + '"/></a></div>'+

					'<div class="graph-definf-txtwrap"><p><b>' + unm + '</b><br>' +
					'<a href="'+ndDt.info.url_view +'">Профиль пользователя</a></p></div>' +
					'<div style="clear:both;"></div>' +
					'</div>';

				elInfoAbout.find('.content').html(sCnt);
			}
			else
				elInfoAbout.find('.content').html(d.content);


			$('#graph-btns-prj').hide();
			elInfoAbout.removeClass("w300");
			$('#graph-btns-mnusr').hide();


			if (ndDt.info.child_skip < 0)
			{
				$('#graph-btns-usr').hide();
			}
			else
				$('#graph-btns-usr').show();


			l= {left: 9000};
			elInfoAbout.css(l);
			elInfoAbout.show();

			repositionInfo();
		}
		else
		{
			alert("showUserInfo() - error.\n network.body.data.nodes._data[pgid] - " + network.body.data.nodes._data[pgid].toString());
		}
	}



	function askNodeProjectInfo(p_gid){

		if(p_gid.charAt(0) != 'p')
			return;

		var postDat = {gid: p_gid, request: "node_project_info"};

		jQuery.ajax({
			method: "POST",
			url: REQAJXURL,
			data: postDat,
			dataType: "json",
			success: function(data){
				if (!data)
				{
					alert("Returned empty data-object");
					return;
				};
				if (data.status == undefined || data.status != "ok")
				{
					alert("Error status - " + data.status);
					return;
				};
				showProjectInfo(data);
			},
			error: function( jqXHR, textStatus, errorThrown )
			{
				alert("Error: " + textStatus + "\n" + errorThrown);
			}
		})
	}

	function showProjectInfo(data){

		var d  = data.data;
		var pgid;
		if (d.source_node)
			pgid = d.source_node.gid;
		else
			pgid = d.gid;

		if (pgid === undefined)
		{
			alert("Не найден gid!\n"+JSON.stringify(data));
			return;
		}


		var ndDt =  network.body.data.nodes._data[pgid]
		if (ndDt){

			var elInfoAbout = $('#graph-info-div');
			elInfoAbout.attr("data-node-id", pgid);


			if (d.content == undefined || d.content.indexOf('<') == -1)
			{
				if (elStyleInfoBaloon === undefined){
					elStyleInfoBaloon  = document.createElement('style');
					elStyleInfoBaloon.innerHTML = style_def;

					document.body.appendChild(elStyleInfoBaloon);
				}

				var ttl = ndDt.info.title;
				if (ttl == undefined)
					ttl = ndDt.label;
				var sCnt = '<div class="graph-definf-content" >' +
					'<div class="graph-definf-txtwrap"><p><b>' + ttl  + '</b><br>' +
					'<a href="'+ndDt.info.url_view +'">Страница проекта</a></p></div>' +
					'<div style="clear:both;"></div>' +
					'</div>';

				elInfoAbout.find('.content').html(sCnt);
			}
			else
				elInfoAbout.find('.content').html(d.content);

			$('#graph-btns-usr').hide();
			elInfoAbout.removeClass("w300");
			$('#graph-btns-mnusr').hide();


			if (ndDt.info.child_skip < 0)
			{
				$('#graph-btns-prj').hide();
			}
			else
				$('#graph-btns-prj').show();

			l= {left: 9000};
			elInfoAbout.css(l);
			elInfoAbout.show();

			repositionInfo();
		}
		else
		{
			alert("showProjectInfo() - error.\n network.body.data.nodes._data[pgid] - " + network.body.data.nodes._data[pgid].toString());
		}
	}

	var pnw, rt2 = 1.41421356237/ 2, rtt;
	var inf_mrg = 5;
	function repositionInfo(){

		var elInfoAbout = $('#graph-info-div');

		if( elInfoAbout.is(':visible') )
		{
			nd = elInfoAbout.attr("data-node-id");
			if (nd)
			{

				var ndPos = network.getPositions(nd);
				ndPos = network.canvasToDOM(ndPos[nd]);

				var scl = network.body.view.scale  ;

				var cw=$('#graph-container').width() ,
					ch=$('#graph-container').height();

				var bw=elInfoAbout.width(),
					bh=elInfoAbout.height(),
					bnd=network.body.nodes[nd];

				var //ew = bnd.shape.width*scl, eh = bnd.shape.height*scl,
					er = bnd.shape.radius*scl;

				var elInfoPnt = $('#graph-info-pntr');

				var t, l = bw + er + 5, pl, pt;
				pnw =elInfoPnt.width();
				t = bh + er + pnw/2;

				if (bw < er*2){
					l = ndPos.x - bw/2 ;
					pl = bw/2 ;
				}
				else
				if (ndPos.x + er + inf_mrg > cw){
					l = ndPos.x - bw + er;
					pl = bw - er;
				}
				else
				{
					if (ndPos.x > cw - inf_mrg - bw/2)
					{
						l = cw - bw - inf_mrg;
						pl = ndPos.x - l;
					}
					else{
						if (ndPos.x > inf_mrg + bw/2){
							l = ndPos.x - bw/2;
							pl = bw/2;
						}
						else{
							if(ndPos.x > inf_mrg + er){
								l = inf_mrg;
								pl = ndPos.x - l;
							}
							else
							{l = ndPos.x - er;
								pl = er;
							}
						}
					}
				}

				rtt = pnw*rt2;
				if(pl < rtt)
				{
					pl = rtt;
					l = l - (rtt - pl);
				}
				if((bw - pl) < rtt)
				{
					pl = bw - rtt;

					l = l + (rtt - (bw -pl));
				}

				pl = pl - pnw/2;



				if(ndPos.y > t)
				{
					t = ndPos.y - t;
					pt = (bh - pnw/2 );
					if(elInfoPnt.hasClass("top")){
						elInfoPnt.removeClass("top");
					}
				}
				else
				{
					t = ndPos.y + er + pnw/2;

					pt = -pnw /2;

					if(!elInfoPnt.hasClass("top"))
						elInfoPnt.addClass("top");
				}

				elInfoPnt.css({top: pt  +'px', left: pl+'px'});


				var css = {top: t + 'px',
					left: l + 'px'};
				elInfoAbout.css(css);
			}
		}

	}



	function askUnfoldNode(gid_nd,  _rqst_type){

		var nd =  network.body.data.nodes._data[gid_nd];

		var postDat = {
			gid: gid_nd,
			request: _rqst_type,
			count:  ASK_COUNT,
			lvl: nd.lvl
		};

		if (nd.info.time_date)
			postDat.time_date = nd.info.time_date;

		if (nd.p_type)
			postDat.p_type = nd.p_type;

		switch (_rqst_type){
			case "unfold_prj": // раскрытие проекта
			case "unfold_usr":  // раскрытие приемника помощи

				if (nd.info.child_skip == undefined || nd.info.child_skip < 0)
				{
					alert("nd.info.child_skip = " + nd.info.child_skip + "\nЧто означает, показывать больше нечего.");
					return;
				}
				postDat.skip = nd.info.child_skip;

				break;
			case "unfold_mn_usr": // раскрытие правой стороны от Главного Юзера
				if (nd.info.child_skip_usr == undefined || nd.info.child_skip_usr < 0)
				{
					alert("nd.info.child_skip_usr = " + nd.info.child_skip_usr + "\nOзначает, что показывать больше нечего.");
					return;
				}
				postDat.skip = nd.info.child_skip_usr;

				break;
			case "unfold_mn_prj": // раскрытие левой стороны от Главного Юзера
				if (nd.info.child_skip_prj == undefined || nd.info.child_skip_prj < 0)
				{
					alert("nd.info.child_skip_prj = " + nd.info.child_skip_prj + "\nOзначает, что показывать больше нечего.");
					return;
				}
				postDat.skip = nd.info.child_skip_prj;
				break;

		}


		jQuery.ajax({
			method: "POST",
			url: REQAJXURL,
			data: postDat,
			dataType: "json",
			success: function(data){
				if (!data)
				{
					alert("Returned empty data-object");
					return;
				};
				if (data.status == undefined || data.status != "ok")
				{
					alert("Error status - " + data.status);
					return;
				};
				showUnfoldNode(data);
			},
			error: function( jqXHR, textStatus, errorThrown )
			{
				alert("Error: " + textStatus + "\n" + errorThrown);
			}
		})

	};

	function showUnfoldNode(_data){

		t = _data.data;
		var ndSrcId = t.source_node.gid;
		var ndSrcObj = t.source_node;

		var actNodes = network.body.nodes;
		var datNodes = network.body.data.nodes._data;


		var xPls = 0;
		var yPls = 0;

		if (datNodes[ndSrcId] )
		{
			xPls = actNodes[ndSrcId].x;
			yPls = actNodes[ndSrcId].y;

			nd = undefined;
			switch (t.request)
			{
				case "unfold_usr":
				{
					nd = $('#graph-info-user').find('.graph-info-more');

					if (ndSrcObj.child_skip !== undefined)
						datNodes[ndSrcId].info.child_skip = ndSrcObj.child_skip;

				}
					break;

				case  "unfold_prj":
				{
					if (ndSrcObj.child_skip !== undefined)
						datNodes[ndSrcId].info.child_skip = ndSrcObj.child_skip;
					else
						datNodes[ndSrcId].info.child_skip = -1;
				}
					break;
				case "unfold_mn_usr": // раскрытие правой стороны от Главного Юзера
				{
					if (ndSrcObj.child_skip_usr !== undefined)
						datNodes[ndSrcId].info.child_skip_usr = ndSrcObj.child_skip_usr;
					else
						datNodes[ndSrcId].info.child_skip_usr = -1;
				}
					break;
				case "unfold_mn_prj": // раскрытие левой стороны от Главного Юзера
				{
					if (ndSrcObj.child_skip_prj !== undefined)
						datNodes[ndSrcId].info.child_skip_prj = ndSrcObj.child_skip_prj;
					else
						datNodes[ndSrcId].info.child_skip_prj = -1;
				}
					break;
			}

			if (nd != undefined && nd.length>0)
			{
				if (isUnfoldedNode(ndSrcId))
					nd.hide();
				else
					nd.show();
			}

			if ( isUnfoldedMnPrj())
				$('#graph-maininfo-btn-proj').hide();
			else
				$('#graph-maininfo-btn-proj').show();

			if ( isUnfoldedMnUsr())
				$('#graph-maininfo-btn-user').hide();
			else
				$('#graph-maininfo-btn-user').show();

			AddNodesEdges(t.nodes, t.edges, ndSrcId, xPls, yPls);
			updateNodeState(ndSrcId);
		}
		else
			alert("Узел-предок " + ndSrcId + " не существует.");
	}


	function sendGraphSettings(graph_lay, show_avas, show_labels){

		var postDat = {
			request: "settings_save",
			show_labels: show_labels,
			show_avas: show_avas,
			graph_layout: graph_lay};

		jQuery.ajax({
			method: "POST",
			url: REQAJXURL,
			data: postDat,
			dataType: "json",
			success: function(data){
				if (!data)
				{
					alert("Returned empty data-object");
					return;
				};
				if (data.status == undefined || data.status != "ok")
				{
					alert("Error status - " + data.status);
					return;
				};
			},
			error: function( jqXHR, textStatus, errorThrown )
			{
				alert("Error: " + textStatus + "\n" + errorThrown);
			}
		});

	}



	var buttonUserMoreClick = function (ev){
		var elInfoAbout = $('#graph-info-div');
		if(	elInfoAbout.is(':visible') ){
			var gid = elInfoAbout.attr("data-node-id");

			t = network.body.data.nodes._data[gid];
			l = t.info.child_skip;

			if (l=== undefined)
			{
				alert('"child_skip" for ' + gid +' is undefined! ');
				return;
			}

			if (l > -1)
				askUnfoldNode(gid, "unfold_usr", l, ASK_COUNT);
		}
	};

	var buttonProjectMoreClick = function (ev){
		var elInfoAbout = $('#graph-info-div');
		if(	elInfoAbout.is(':visible') ){
			var gid = elInfoAbout.attr("data-node-id");
			t = network.body.data.nodes._data[gid];
			l = t.info.child_skip;

			if (l=== undefined)
			{
				alert('"child_skip" for ' + gid +' is undefined! ');
				return;
			}

			if (l > -1)
				askUnfoldNode(gid, "unfold_prj", l, ASK_COUNT);
		}
	};


	var buttonMainMorePrjClick = function (ev){
		askUnfoldNode(mainuser_gid, "unfold_mn_prj", network.body.data.nodes._data[mainuser_gid].info.child_skip_prj, ASK_COUNT);
	};
	var buttonMainMoreUsrClick = function (ev){
		askUnfoldNode(mainuser_gid, "unfold_mn_usr", network.body.data.nodes._data[mainuser_gid].info.child_skip_usr, ASK_COUNT);
	};


	function askNodeMainUserInfo(){

		var postDat = {gid: mainuser_gid, request: 'node_mainuser_info'};

		jQuery.ajax({
			method: "POST",
			url: REQAJXURL,
			data: postDat,
			dataType: "json",
			success: function(data){
				if (!data)
				{
					alert("Returned empty data-object");
					return;
				};
				if (data.status == undefined || data.status != "ok")
				{
					alert("Error status - " + data.status);
					return;
				};
				showMainUserInfo(data);
			},
			error: function( jqXHR, textStatus, errorThrown )
			{
				alert("Error: " + textStatus + "\n" + errorThrown);
			}
		})

	}
	function showMainUserInfo(data){
		var d  = data.data;

		var pgid = d.source_node.gid;
		var ndDt = network.body.data.nodes._data[pgid]
		if (ndDt){

			var elInfoAbout = $('#graph-info-div');
			elInfoAbout.attr("data-node-id", pgid);
			if (d.content == undefined || d.content.indexOf('<') == -1)
			{

				if (elStyleInfoBaloon === undefined){
					elStyleInfoBaloon  = document.createElement('style');
					elStyleInfoBaloon.innerHTML = style_def;

					document.body.appendChild(elStyleInfoBaloon);
				}

				var unm = ndDt.info.uname;
				if (unm == undefined)
					unm = ndDt.label;
				var sCnt = '<div class="graph-definf-content" >' +
					'<div class="definf-ava"><a href="'+
					ndDt.info.url_view + '"><img src="' +
					ndDt.info.url_ava + '"/></a></div>'+

					'<div class="graph-definf-txtwrap"><p><b>' + unm + '</b></p></div>' +
					'<div style="clear:both;"></div>' +
					'</div>';
				elInfoAbout.find('.content').html(sCnt);
			}
			else
				elInfoAbout.find('.content').html(d.content);


			$('#graph-btns-prj').hide();
			$('#graph-btns-usr').hide();
			elInfoAbout.addClass("w300");
			$('#graph-btns-mnusr').show();

			if (ndDt.info.child_skip_prj !== undefined && ndDt.info.child_skip_prj> -1)
			{
				$('#graph-bpls-mnprj').show();
			}
			else
				$('#graph-bpls-mnprj').hide();

			if (ndDt.info.child_skip_usr !== undefined && ndDt.info.child_skip_usr> -1)
			{
				$('#graph-bpls-mnusr').show();
			}
			else
				$('#graph-bpls-mnusr').hide();

			l= {left: 9000};
			elInfoAbout.css(l);
			elInfoAbout.show();



			repositionInfo();
		}
		else
		{
			alert("showMainUserInfo() - error.\n network.body.data.nodes._data[pgid] - " + network.body.data.nodes._data[pgid].toString());
		}
	}

	function askMainUserData(_umain_id){
		var postData = {user_id: _umain_id, request: "tree_main"};
		jQuery.ajax({
			method: "POST",
			url: REQAJXURL,
			data: postData,
			dataType: "json",
			success: function(data){
				if (!data)
				{
					alert("Returned empty data-object");
					return;
				};
				if (data.status == undefined || data.status != "ok")
				{
					alert("Error status - " + data.status);
					return;
				};
				applyMainUserData(data);
			},
			error: function( jqXHR, textStatus, errorThrown )
			{
				alert("Error: " + textStatus + "\n" + errorThrown);
			}
		})


	}
	function applyMainUserData(dat){
		dtInitGraph	= dat;
		$('#graph-curtain-text').html("Подготовка данных..." );
		$('#graph-items-count').html("Всего узлов: "+ dtInitGraph.data.nodes.length +". Всего связей: "+ dtInitGraph.data.edges.length );


		setTimeout(_applyMainUserData, 1);


	}

	function wait1secFont(){
		network.redraw();
	}

	function _applyMainUserData(){

		network.stopSimulation();

		//	clear data
		//	destroyClearNetwork();

		t = dtInitGraph.data;

		// read settings
		var sts;
		if ( t.settings)
			sts = t.settings;
		else
			sts = {};


		if (sts.graph_layout)
		{
			switch (sts.graph_layout){
				case "physics":
				case "circle":
				case "cluster":
				case "binary_tree":
					break;

				default :
					sts.graph_layout = DEFGRAPHLAYOUT;
					break;
			}
		}
		else
			sts.graph_layout = DEFGRAPHLAYOUT;


		$("#graph-select-layout").find("option").each( function(){
			if ($(this).val() == sts.graph_layout){
				$(this).attr('selected', 'selected');
				return false;
			}
		});

		var nodes = network.body.data.nodes;
		var edges = network.body.data.edges;

		if (nodes.length > 0)
		{
			for (var k in nodes._data)
			{
				try {
					nodes.remove(nodes._data[k].id);
				}
				catch (err) {
					alert(err);
				}
			}
		}
		if (edges.length > 0)
		{
			for (var k in edges._data)
			{
				try {
					edges.remove(edges._data[k].id);
				}
				catch (err) {
					alert(err);
				}
			}
			edges.edgesFromArrTo = {};
			edges.edgesToArrFrom = {};
		}




		// create main_user node
		u = t.source_node;
		mainuser_gid = u.gid;

		u.id = mainuser_gid;
		u.nd_type = "mnusr";
		u.label= u.info.uname;
		u.group= 'u_main'
		u.image = u.info.url_ava;
		u.x= 0;
		u.y=0;
		u.title = u.info.uname;
		u.lvl = 0;
		u.dt_hlp = "0000-00-00T00:00:00";

		if (u.info.child_skip_prj === undefined)
			u.info.child_skip_prj = -1;
		if (u.info.child_skip_usr === undefined)
			u.info.child_skip_usr = -1;

		u.shapeProperties = {};
		if (u.info.child_skip_prj > -1 || u.info.child_skip_usr > -1)
			u.shapeProperties.borderDashes = [10,25];
		else
			u.shapeProperties.borderDashes = false;

		u.tr_prnt = -1;
		nodes.add(u);

		// create all other nodes

		dn = t.nodes;
		de = t.edges;

		$('#graph-maininfo-btn-proj').attr('data-node-id', mainuser_gid);
		$('#graph-maininfo-btn-user').attr('data-node-id', mainuser_gid);

		AddNodesEdges(dn, de, mainuser_gid);

		if (nodes.length + edges.length > 500)
		{
			network.setOptions({edges:{smooth : false}});
		}


		if (sts.show_avas === true || sts.show_avas == "true"){
			showNodeImages(true);
			$("#graph-users-avas").attr('checked', 'checked');
		}

		if (sts.show_labels === true || sts.show_labels == "true"){
			showNodeLabels(true);
			$("#graph-users-labels").attr('checked', 'checked');
		}


		repositionGraph(1);
		setTimeout(wait1secFont, 500);
		setTimeout(wait1secFont, 1500);
		setTimeout(wait1secFont, 10000);
	}



	// fixme: start execution
	if (network)
	{hideCurtain();return;}

	$('#'+_container).html(
		'<div id="vis-container">' +
		'<div id="graph-control-wrap">' +
		'<div class="graph-ctrl-item"><select id="graph-select-layout">'+
		'<option value="physics" selected="selected">Эмуляция физики</option>' +
		'<option value="binary_tree" >Двоичное дерево</option>' +
		//'<!--option value="circle">По кругу</option--> ' +
		'<option value="cluster">Кластеризация</option>' +
		'</select></div>' +
		'<div class="graph-ctrl-item"><input type="checkbox" checked id="graph-users-avas"><label for="graph-users-avas">показать аватарки</label></div>' +
		'<div class="graph-ctrl-item"><input type="checkbox" id="graph-users-labels"><label for="graph-users-labels">показать подписи</label></div>' +
		'<div class="graph-ctrl-info"><span id="graph-items-count"></span></div>' +
		//'<div id="graph-control-close" ></div>'+
		'</div>'+

		'<div id="graph-wrapper">' +
		'<div id="graph-container">' +'</div>' +

		'<div class="graph-info-baloon" id="graph-info-div">' +
		'<div id="graph-info-pntr"></div>' +
		'<div class="graph-info-wrap">' +
		'<div class="content"></div>' +
		'<div class="graph-info-btn-wrap" id="graph-btns-prj">' +
		'<button id="graph-bpls-prj" class="graph-info-more" >Еще (+)</button>' +
		'</div>' +
		'<div class="graph-info-btn-wrap" id="graph-btns-usr">' +
		'<button id="graph-bpls-usr" class="graph-info-more" >Еще (+)</button>' +
		'</div>' +
		'<div class="graph-info-btn-wrap" id="graph-btns-mnusr">' +
		'<button id="graph-bpls-mnprj" class="graph-info-more">Еще проекты (+)</button>' +
		'<button id="graph-bpls-mnnusr" class="graph-info-more">Еще пользователи (+)</button>' +
		'</div>' +
		'</div>' +
		'</div>' +

		'<div id="graph-curtain">' +
		'<div id="graph-loader-wrap">' +
		'<img id="graph-loader" src="/img/graph/loader-128.png">' +
		'</div>' +
		'<div id="graph-curtain-text"></div>' +
		'</div>' +
		'</div>' +
		'</div>');


	$('#graph-maininfo-btn-proj').click(buttonMainMorePrjClick);
	$('#graph-maininfo-btn-user').click(buttonMainMoreUsrClick);

	$('#graph-bpls-mnprj').click(buttonMainMorePrjClick);
	$('#graph-bpls-mnnusr').click(buttonMainMoreUsrClick);

	$('#graph-curtain-text').html("Данные загружаются...");

	$('#graph-info-btn-prj').click(buttonProjectMoreClick);
	$('#graph-info-btn-usr').click(buttonUserMoreClick);

	$('#graph-bpls-prj').click(buttonProjectMoreClick);
	$('#graph-bpls-usr').click(buttonUserMoreClick);

	var optionsDef = {
		nodes:	{
			brokenImage: AVA_U_DEF
		},
		interaction: {
			navigationButtons: true,
			keyboard: true
		},
		edges:{
			color: {
				inherit:'to'
			}
		},


		groups: {
			useDefaultGroups: true,
			ed_more:{
				color: 'rgba(255,0,0, 0.5)',
				width: 5,
				smooth:{
					type:'continuous'
				}
			},
			u_main: {

				mass:30,
				fixed:
				{
					x:true,
					y:true
				},
				borderWidth:8,
				borderWidthSelected:10,
				size:60,
				shape: 'circularImage',
				image: AVA_U_DEF,
				shapeProperties: {
					useBorderWithImage:true
				},
				color: {
					background:'#35b045',
					border:'#35b045',
					highlight:'#35b045'
				}
			}
		},
		physics: {
			enabled: false,

			maxVelocity: 14,
			minVelocity: 0.75,
			stabilization: {
				enabled: false,
				iterations: 100,
				updateInterval: 100,
				onlyDynamicEdges: false,
				fit: true
			}
		}
	};

	var uGrpDt = [
		{	clrIcon:'#f84840'	},
		{	clrIcon:'#ffc12d'	},
		{	clrIcon:'#f36624'	},
		{	clrIcon:'#2b78b8'	},
		{	clrIcon:'#38b348'	},
		{	clrIcon:'#40c2f8'	},
		{	clrIcon:'#be8756'	},
		{	clrIcon:'#b79afa'	},
		{	clrIcon:'#6b2b43'	},
		{	clrIcon:'#698422'	},
		{	clrIcon:'#006f00'	},
		{	clrIcon:'#888888'	}
	];

	for(i = 1; i <=12; i++){
		optionsDef.groups["u_t"+i] ={

			size:20,
			borderWidth:7,
			borderWidthSelected:9,
			shape:'icon',
			icon : {
				back: {
					shape: 'circle'
				},
				face: 'FontVisShift',
				code: '\uf102'
			},
			shapeProperties:{
				borderDashes: [5,10]
			}
		};
		optionsDef.groups["u_t"+i].color = {
			background:'#fff',
			border:uGrpDt[i-1].clrIcon};
		optionsDef.groups["u_t"+i].icon.color = uGrpDt[i-1].clrIcon;


		optionsDef.groups["p_t"+i] ={

			mass:15,
			size:20,
			shape:'icon',
			icon : {
				face: 'FontVisShift',
				code: '\uf100',
				color:uGrpDt[i-1].clrIcon
			},
			color: {
				background:'#fff',
				border:uGrpDt[i-1].clrIcon
			}
		};

	}

	showCurtain();


	var elemGraph = document.getElementById('graph-container');
			if (elemGraph.addEventListener) {
				if ('onwheel' in document) {
					// IE9+, FF17+, Ch31+
					elemGraph.addEventListener("wheel", onWheel);
				} else if ('onmousewheel' in document) {
					// устаревший вариант события
					elemGraph.addEventListener("mousewheel", onWheel);
				} else {
					// Firefox < 17
					elemGraph.addEventListener("MozMousePixelScroll", onWheel);
				}
			} else { // IE8-
				elemGraph.attachEvent("onmousewheel", onWheel);

			}

	function onWheel(e) {
		e = e || window.event;

		if (_me.isFF === undefined)
		{
			isFF = (navigator.userAgent.toLowerCase().indexOf('firefox') > -1);
		}
		if (isFF)
			return;
		e.returnValue = false;
		e.preventDefault ? e.preventDefault() : (e.returnValue = false);

		e.cancelBubble = true;
		return false;
	}

	$( "#graph-select-layout" ).change(repositionGraph);
	// create a network with def options

	var gData = {nodes:{}, edges: {}};
	gData.nodes = new vis.DataSet();
	gData.edges = new vis.DataSet();

	gData.edges.edgesFromArrTo = {};
	gData.edges.edgesToArrFrom = {};

	var container = document.getElementById('graph-container');
	network = new vis.Network(container, gData, optionsDef);

	network.body.data.custom = {};


	network.on("stabilizationProgress", function(params) {
		rotateLoader();
	});
	network.once("stabilizationIterationsDone", function() {
		hideCurtain();
	});

	network.on("showPopup", function (params) {	});
	network.on("hidePopup", function () {	});


	network.on("click", function (params) {

		$('#graph-wrapper').find('.graph-info-baloon').hide();

		if (params.nodes.length	){

			var nd = this.body.nodes[params.nodes[0]];
			if( nd != undefined && nd.options) {
				if(nd)
				{
					switch(nd.options.nd_type){
						case "usr":
							askNodeUserInfo(nd.id);
							break;
						case "prj":
							askNodeProjectInfo(nd.id);

							break;
						case "mnusr":
							askNodeMainUserInfo(nd.id);
							break;
						default:
							break;
					}
				}
			}
		}
		else{
			$("#graph-wrapper").find(".graph-info-baloon").hide();
		}
	});

	network.on("hold", function (params) {

		if (params.nodes.length	){

			var nd = this.body.data.nodes._data[params.nodes[0]];
			if( nd != undefined && nd.info) {

				$("#graph-wrapper").find(".graph-info-baloon").hide();

				switch  (nd.nd_type)
				{
					case 'usr':
					{
						l = nd.info.child_skip;

						if (l=== undefined)
						{
							alert('"child_skip" for ' + params.nodes[0] +' is undefined! ');
							return;
						}
						if (l > -1)
							askUnfoldNode(params.nodes[0], "unfold_usr" );
					}
						break;
					case 'prj':
					{
						l = nd.info.child_skip;
						if (l=== undefined)
						{
							alert('"child_skip" for ' + params.nodes[0] +' is undefined! ');
							return;
						}
						if (l > -1)
							askUnfoldNode(params.nodes[0], "unfold_prj");
					}
						break;
					case 'mnusr':
					{
						if (network.body.data.nodes._data[mainuser_gid].info.child_skip_prj > -1)
							askUnfoldNode(mainuser_gid, "unfold_mn_prj");
						if (network.body.data.nodes._data[mainuser_gid].info.child_skip_usr > -1)
							askUnfoldNode(mainuser_gid, "unfold_mn_usr");
					}
						break;
				}
			}
		}
	});

	network.on("zoom", function (params) {
		repositionInfo();		});

	network.on("resize", function (params) {
		repositionInfo();		});

	network.cust = {};
	network.cust.pPrevDrag= {x:0, y:0};

	network.on("dragStart", function (params) {
		network.cust.pPrevDrag.x = params.pointer.DOM.x;
		network.cust.pPrevDrag.y = params.pointer.DOM.y;
	});

	network.on("dragging", function (params) {
		if (params.nodes.length == 0)
		{

			repositionInfo();
		}
		else{
			$('#graph-info-div').hide();
		}
	});

	askMainUserData(_user_id);

	$('#graph-users-avas').change(function() {
		showNodeImages(this.checked);
	});
	elChkAvas = $('#graph-users-avas');

	$('#graph-users-labels').change(function() {
		showNodeLabels(this.checked);
	});

	elChkLbls = $('#graph-users-labels');

}