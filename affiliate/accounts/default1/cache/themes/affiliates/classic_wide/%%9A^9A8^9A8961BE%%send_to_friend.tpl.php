<?php /* Smarty version 2.6.18, created on 2016-10-04 06:10:26
         compiled from send_to_friend.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'send_to_friend.tpl', 3, false),)), $this); ?>
<!-- send_to_friend -->
<fieldset class="SendToFriend">
    <legend><?php echo smarty_function_localize(array('str' => 'Send to a friend'), $this);?>
</legend>
    <?php echo "<div id=\"recipients\" class=\"ContactUsText\"></div>"; ?>
    <?php echo "<div id=\"from\" class=\"ContactUsText\"></div>"; ?>
    <?php echo "<div id=\"subject\" class=\"ContactUsText\"></div>"; ?>
    <?php echo "<div id=\"message\" class=\"ContactUsText\"></div>"; ?>
    <?php echo "<div id=\"bannerId\"></div>"; ?>
</fieldset>

<?php echo "<div id=\"FormMessage\"></div>"; ?>
<?php echo "<div id=\"SaveButton\"></div>"; ?>
<?php echo "<div id=\"CloseButton\"></div>"; ?>
<div class="clear"></div>