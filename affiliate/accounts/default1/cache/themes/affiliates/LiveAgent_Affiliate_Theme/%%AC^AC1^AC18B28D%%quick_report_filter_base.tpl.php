<?php /* Smarty version 2.6.18, created on 2016-10-04 06:00:43
         compiled from quick_report_filter_base.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'quick_report_filter_base.tpl', 5, false),)), $this); ?>
<!-- quick_report_filter_base -->
<div class="ReportsFilter">

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Channel'), $this);?>
</div>
        <?php echo "<div id=\"channel\"></div>"; ?>
        <div class="clear"></div>
    </div>

	<div class="FilterRow">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Campaign'), $this);?>
</div> 
		<?php echo "<div id=\"campaignid\"></div>"; ?>
		<div class="clear"></div>
	</div>

	<div class="FilterRow">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Destination URL'), $this);?>
</div>
		<?php echo "<div id=\"destinationurl\"></div>"; ?>
		<div class="clear"></div>
	</div>
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Date'), $this);?>
</div>
    		<?php echo "<div id=\"datetime\"></div>"; ?>
		<div class="clear"></div>
	</div>

</div>

<div style="clear: both;"></div>
