<?php /* Smarty version 2.6.18, created on 2016-10-04 06:00:43
         compiled from signup_subaffiliates.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'signup_subaffiliates.tpl', 4, false),)), $this); ?>
<!-- signup_subaffiliates -->
<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'SubSignupOverview'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'SubSignupOverviewDescription'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"signupLink\"></div>"; ?>
</div>

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'SubSignupDownloadForms'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'SubSignupDownloadFormsDescription'), $this);?>
</div>
	</div>
  <?php echo "<div id=\"downloadJoinForm\"></div>"; ?>
  <?php echo "<div id=\"downloadLoginForm\"></div>"; ?>
</div>

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'SubSignupStats'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'Number of your direct subaffiliates:'), $this);?>
 <?php echo "<div id=\"numberOfSubaffiliates\"></div>"; ?></div>
	</div>
  <table>
  <tr>
    <td align="center"><?php echo "<div id=\"SubaffiliateSaleStats\"></div>"; ?></td>
    <td align="center"><?php echo "<div id=\"SubaffiliatesTree\"></div>"; ?></td>
  </tr>
  </table> 	
</div>