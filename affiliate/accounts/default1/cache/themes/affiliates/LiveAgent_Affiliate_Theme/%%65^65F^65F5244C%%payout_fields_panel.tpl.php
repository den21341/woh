<?php /* Smarty version 2.6.18, created on 2016-10-04 06:00:43
         compiled from payout_fields_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'payout_fields_panel.tpl', 5, false),)), $this); ?>
<!--    payout_fields_panel     -->

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Payout method and data'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <?php echo "<div id=\"payoutoptionid\" class=\"PayoutOptionId\"></div>"; ?>
  <?php echo "<div id=\"payoutOptions\"></div>"; ?>
</div>

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Payout balances'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
	<?php echo "<div id=\"minimumPayoutOptions\"></div>"; ?>
	<?php echo "<div id=\"minimumpayout\"></div>"; ?>
	<div class="clear"></div>


</div>

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Invoicing options'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
	<?php echo "<div id=\"invoicingNotSupported\"></div>"; ?>
	<?php echo "<div id=\"applyVatInvoicing\"></div>"; ?>
	<?php echo "<div id=\"vatPercentage\"></div>"; ?>
	<?php echo "<div id=\"vatNumber\"></div>"; ?>
	<?php echo "<div id=\"amountOfRegCapital\"></div>"; ?>
	<?php echo "<div id=\"regNumber\"></div>"; ?>
</div>