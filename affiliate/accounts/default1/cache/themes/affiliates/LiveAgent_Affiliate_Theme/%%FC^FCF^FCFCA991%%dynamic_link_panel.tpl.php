<?php /* Smarty version 2.6.18, created on 2016-10-04 06:00:43
         compiled from dynamic_link_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'dynamic_link_panel.tpl', 4, false),)), $this); ?>
<!--    dynamic_link_panel  -->
<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Dynamic link'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
    <?php echo "<div id=\"desturl\"></div>"; ?>
    <?php echo "<div id=\"preview\" class=\"DynamicLinkPreview\"></div>"; ?>
    <?php echo "<div id=\"code\" class=\"DynamicLinkTextArea\"></div>"; ?>
</div>

<?php echo "<div id=\"message\"></div>"; ?>
<?php echo "<div id=\"getCodeButton\"></div>"; ?>
<?php echo "<div id=\"closeButton\"></div>"; ?>