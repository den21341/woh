<?php /* Smarty version 2.6.18, created on 2016-10-04 06:00:27
         compiled from home.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'home.tpl', 15, false),)), $this); ?>
<!-- home -->
<table border="0" cellspacing="0" cellpadding="0">
<tr>
  <td><div class="FormFieldset"><?php echo "<div id=\"WelcomeMessage\" class=\"WelcomeMessage\"></div>"; ?></div></td>
  <td rowspan="2" valign="top"><?php echo "<div id=\"AffiliateManager\"></div>"; ?></td>
</tr>
<tr>
  <td valign="top"><div class="FormFieldset" style="position:relative;"><?php echo "<div id=\"RefreshButton\" class=\"HomeRefresh\"></div>"; ?><?php echo "<div id=\"PeriodStats\"></div>"; ?>
</div></td>
</tr>
</table>

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Quick Navigation Icons'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <div class="IconsPanel">
  	<?php echo "<div id=\"Tutorial\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"MyProfile\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Reports\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"SignupSubaffiliates\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Campaigns\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Banners\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"DirectLinks\" class=\"SimpleIcon\"></div>"; ?>
  </div>		
</div>