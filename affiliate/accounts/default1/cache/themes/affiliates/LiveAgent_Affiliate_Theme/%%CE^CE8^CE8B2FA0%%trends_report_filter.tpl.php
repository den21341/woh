<?php /* Smarty version 2.6.18, created on 2016-10-04 06:00:54
         compiled from trends_report_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'trends_report_filter.tpl', 5, false),)), $this); ?>
<!-- trends_report_filter -->
    
<div class="TrendReportsFilter">
    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Channel'), $this);?>
</div>
        <?php echo "<div id=\"channel\"></div>"; ?>
        <div class="clear"></div>
    </div>

	<div class="FilterRow">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Campaign'), $this);?>
</div>
		<?php echo "<div id=\"campaignid\"></div>"; ?>
		<div class="clear"></div>
	</div> 
     
    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Banner'), $this);?>
</div>
        <?php echo "<div id=\"bannerid\"></div>"; ?>
        <div class="clear"></div>
    </div>

	<div class="FilterRow">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'DestinationURL'), $this);?>
</div>
		<?php echo "<div id=\"destinationurl\"></div>"; ?>
		<div class="clear"></div>
	</div>

	<div class="FilterRow">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Status'), $this);?>
</div>
                <?php echo "<div id=\"rstatus\"></div>"; ?>
		<div class="clear"></div>
	</div>
       	<?php echo "<div id=\"additionalFilters\"></div>"; ?>

</div>
<div style="clear: both;"></div>
   