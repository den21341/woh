<?php /* Smarty version 2.6.18, created on 2016-10-04 06:00:43
         compiled from email_notifications.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'email_notifications.tpl', 4, false),)), $this); ?>
<!-- email_notifications -->
<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Email notifications'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'Set up which email notifications you want to receive'), $this);?>
</div>
	</div>
  <?php echo "<div id=\"aff_notification_on_new_sale\" class=\"AffiliateNotification\"></div>"; ?>
  <?php echo "<div id=\"aff_notif_disable_zero_comm_on_new_sale\" class=\"AffiliateNotification\"></div>"; ?>
  <?php echo "<div id=\"aff_notofication_on_new_extra_bonus\" class=\"AffiliateNotification\"></div>"; ?>
  <?php echo "<div id=\"aff_notification_on_change_comm_status\" class=\"AffiliateNotification\"></div>"; ?>
  <?php echo "<div id=\"aff_notification_on_subaff_signup\" class=\"AffiliateNotification\"></div>"; ?>
  <?php echo "<div id=\"aff_notification_on_subaff_sale\" class=\"AffiliateNotification\"></div>"; ?>
  <?php echo "<div id=\"aff_notification_on_direct_link_approved_declined\" class=\"AffiliateNotification\"></div>"; ?>
  <?php echo "<div id=\"aff_notification_daily_report\"></div>"; ?>
  <?php echo "<div id=\"aff_notification_weekly_report\"></div>"; ?>
  <?php echo "<div id=\"aff_notification_monthly_report\"></div>"; ?>
</div>

<?php echo "<div id=\"FormMessage\"></div>"; ?>
<?php echo "<div id=\"SaveButton\"></div>"; ?>
<div class="clear"></div>