<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:08
         compiled from home.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'home.tpl', 8, false),)), $this); ?>
<!-- home -->
<div class="Dash_FormFieldset"><?php echo "<div id=\"WelcomeMessage\" class=\"WelcomeMessage\"></div>"; ?></div>

<?php echo "<div id=\"PeriodStats\"></div>"; ?>

<div class="Dash_FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Quick Navigation Icons'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <div class="IconsPanel">
  	<?php echo "<div id=\"Tutorial\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"MyProfile\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Reports\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"SignupSubaffiliates\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Campaigns\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Banners\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"DirectLinks\" class=\"SimpleIcon\"></div>"; ?>
  </div>		
</div>