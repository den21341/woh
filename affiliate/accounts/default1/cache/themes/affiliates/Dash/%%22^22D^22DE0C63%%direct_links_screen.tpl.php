<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from direct_links_screen.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'direct_links_screen.tpl', 4, false),)), $this); ?>
<!-- direct_links_screen -->
<div class="Dash_FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Links'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'Links may not be changed. If you want to change link, you can delete the old link and create new, which must merchant again approve'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"UrlsGrid\"></div>"; ?>
</div>
<div class="Dash_FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Usage'), $this);?>
</div>
	</div>
	<p>
	<?php echo smarty_function_localize(array('str' => 'Read more about DirectLinks'), $this);?>
 <a href='#189911-directlinks_explained' style="text-decoration: underline; font-weight: bold; color:#135fab"><?php echo smarty_function_localize(array('str' => 'here'), $this);?>
</a>.
	<?php echo smarty_function_localize(array('str' => 'You don\'t need to enter each and every URL address of your pages, you can use star convention.'), $this);?>
<br/>
	<?php echo smarty_function_localize(array('str' => 'So for example pattern'), $this);?>
 <strong>*yoursite.com*</strong> <?php echo smarty_function_localize(array('str' => 'will match:'), $this);?>
<br />
	www.yoursite.com<br />
	subdomain.yoursite.com<br />
	www.yoursite.com/something.html<br />
	www.yoursite.com/dir/something.php?parameters<br />
	</p>
	  <fieldset>
			<legend><?php echo smarty_function_localize(array('str' => 'Test URL matching'), $this);?>
</legend>
			<div class="HintText"><?php echo smarty_function_localize(array('str' => 'You can test if your pattern matches the given URL.'), $this);?>
</div>
			<table class="Dash_URLTable">
				<tbody>
					<tr>
						<td><div class="Inliner"><?php echo smarty_function_localize(array('str' => 'Pattern'), $this);?>
</div></td><td><?php echo "<div id=\"pattern\" class=\"FormFieldBigInline FormFieldOnlyInput\"></div>"; ?></td>
					</tr>
					<tr>
						<td><div class="Inliner"><?php echo smarty_function_localize(array('str' => 'Real url'), $this);?>
</div></td><td><?php echo "<div id=\"realUrl\" class=\"FormFieldBigInline FormFieldOnlyInput\"></div>"; ?></td>
					</tr>
					<tr>
						<td><?php echo "<div id=\"checkButton\"></div>"; ?></td>
						<td><?php echo "<div id=\"message\"></div>"; ?></td>
					</tr>
				</tbody>
			</table>				  
	  </fieldset>
</div>

