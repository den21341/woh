<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from campaign_tracking_codes_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'campaign_tracking_codes_filter.tpl', 5, false),)), $this); ?>
<!-- campaign_tracking_codes_filter -->

<div class="CampaignTrackingCodesFilter">
    <div class="FilterRow">       
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Campaign'), $this);?>
</div>
        <?php echo "<div id=\"campaignid\"></div>"; ?>
        <div class="clear"></div>
    </div>
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Status'), $this);?>
</div>
    	<?php echo "<div id=\"status\"></div>"; ?>
		<div class="clear"></div>
	</div>
</div>       
<div style="clear: both;"></div>
   