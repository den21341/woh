<?php /* Smarty version 2.6.18, created on 2016-10-04 03:56:59
         compiled from custom/getting_started.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'custom/getting_started.tpl', 4, false),)), $this); ?>
<div class="Dash_FormFieldset">
	<div class="Dash_StartItem Dash_PromoTips">
		<a href='#188461-promotion_tips'>
		<span><?php echo smarty_function_localize(array('str' => 'Promotion tips'), $this);?>
</span>
		<?php echo smarty_function_localize(array('str' => 'Tips and common practice you can do to increase the traffic and earn more commissions from our affiliate program.'), $this);?>

		</a>
	</div>
	<div class="Dash_StartItem Dash_FAQ">
		<a href='#150623-frequently_asked_questions'>
		<span><?php echo smarty_function_localize(array('str' => 'Frequently asked questions'), $this);?>
</span>
		<?php echo smarty_function_localize(array('str' => 'Questions and answers about our affiliate program, the conditions, payouts, tracking, and much more.'), $this);?>

		</a>
	</div>
	<div class="Dash_StartItem Dash_AdvTracking">
		<a href='#159219-advanced_tracking' class="Dash_AdvTracking">
		<span><?php echo smarty_function_localize(array('str' => 'Advanced tracking'), $this);?>
</span>
		<?php echo smarty_function_localize(array('str' => 'Possibilities of advanced tracking in our affiliate program - includes tracking od Advertisement channels and Sub Id tracking.'), $this);?>

		</a>
	</div>
	<div class="Dash_StartItem Dash_LinkTracking">
		<a href='#189911-directlinks_explained' class="">
		<span><?php echo smarty_function_localize(array('str' => 'DirectLink tracking explained'), $this);?>
</span>
		<?php echo smarty_function_localize(array('str' => 'Explains how DirectLink tracking works and how you can use it.'), $this);?>

		</a>
	</div>
</div>