<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from contact_us.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'contact_us.tpl', 4, false),)), $this); ?>
<!-- contact_us -->
<div class="Dash_FormFieldset Dash_ContactUs">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Contact us'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'You can contact us directly using this contact form'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"subject\" class=\"ContactUsText\"></div>"; ?>
	<?php echo "<div id=\"text\" class=\"ContactUsText\"></div>"; ?>
	<?php echo "<div id=\"FormMessage\"></div>"; ?>
	<?php echo "<div id=\"SaveButton\"></div>"; ?>
</div>
<div class="clear"></div>