<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from referringurl_advanced_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'referringurl_advanced_filter.tpl', 6, false),)), $this); ?>
<!-- referringurl_advanced_filter -->

<div class="ReferringURLFilter">
			
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Date of sale'), $this);?>
</div>
		<?php echo "<div id=\"datetime\"></div>"; ?>
		<div class="clear"></div>
	</div>	     
        
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Sale status'), $this);?>
</div>
		<?php echo "<div id=\"rstatus\"></div>"; ?>
		<div class="clear"></div>
	</div>	
        
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Group by'), $this);?>
</div>
		<?php echo "<div id=\"groupby\"></div>"; ?>
		<div class="clear"></div>
	</div>		
        
</div>
<div style="clear: both;"></div>