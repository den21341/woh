<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from affiliate_theme_settings.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'affiliate_theme_settings.tpl', 4, false),)), $this); ?>
<!-- affiliate_theme_settings -->
<div class="Dash_FormFieldset Themes">
	<div class="ThemeActive">
		<div class="FormFieldsetSectionTitle"><?php echo smarty_function_localize(array('str' => 'Selected theme'), $this);?>
</div>
		<?php echo "<div id=\"selectedTheme\"></div>"; ?>
	</div>

	<div class="FormFieldsetSectionTitle"><?php echo smarty_function_localize(array('str' => 'Other themes'), $this);?>
</div>
	<?php echo "<div id=\"otherThemes\"></div>"; ?>
	<div class="clear"></div>
</div>