<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from banners.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'banners.tpl', 4, false),)), $this); ?>
<!-- banners -->
<div class="Dash_FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Quick Navigation Icons'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <div class="IconsPanel">	
  	<?php echo "<div id=\"Campaigns\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"DirectLinks\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Channels\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"AffLinkProtector\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Reports\" class=\"SimpleIcon\"></div>"; ?>
  </div>
</div>

<div class="Dash_FormFieldset BannerFilterAndGrid">
  <?php echo "<div id=\"BannersFilter\"></div>"; ?>
  <?php echo "<div id=\"BannersGrid\"></div>"; ?>
</div>