<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from payouts_history_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'payouts_history_filter.tpl', 6, false),)), $this); ?>
<!-- payouts_history_filter -->

<div class="PayoutsHistoryFilter">		

	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Date paid'), $this);?>
</div>
		<?php echo "<div id=\"dateinserted\"></div>"; ?>
		<div class="clear"></div>
	</div>	
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Merchant note'), $this);?>
</div>
		<?php echo "<div id=\"merchantnote\"></div>"; ?>
		<div class="clear"></div>
	</div>	
</div>

<div style="clear: both;"></div>