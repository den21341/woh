<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from direct_linking_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'direct_linking_panel.tpl', 4, false),)), $this); ?>
<!-- direct_linking_panel -->
<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Direct Linking'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'DirectLinkingDescription'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"ManageDirectLinks\"></div>"; ?>
</div>