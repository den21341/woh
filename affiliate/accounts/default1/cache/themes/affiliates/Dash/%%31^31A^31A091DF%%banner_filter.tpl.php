<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from banner_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'banner_filter.tpl', 4, false),)), $this); ?>
<!-- banner_filter -->
<div class="BannersFilter">
	<div class="FilterRow">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Campaign'), $this);?>
</div>
		    <?php echo "<div id=\"campaignid\" class=\"FilterCampaign\"></div>"; ?>
		<div class="clear"></div>
	</div>

	<div class="FilterRow FilterRowOdd">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Channel'), $this);?>
</div>
       		<?php echo "<div id=\"channel\" class=\"FilterCampaign\"></div>"; ?>
		<div class="clear"></div>
	</div>
	
	<div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Target url'), $this);?>
</div>
        <?php echo "<div id=\"destinationurl\" class=\"FilterCampaign\"></div>"; ?>
        <div class="clear"></div>
    </div>
    
    <div class="FilterRow FilterRowOdd">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Banner type'), $this);?>
</div>
        <?php echo "<div id=\"type\"></div>"; ?>
        <div class="clear"></div>
    </div>

	<div class="FilterRow FilterAdditionalData">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Additional data'), $this);?>
</div>
		<?php echo "<div id=\"displaystats\"></div>"; ?>
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'For date range'), $this);?>
</div>
		<?php echo "<div id=\"statsdate\"></div>"; ?>
		<div class="FilterLegend"></div>
        <?php echo "<div id=\"show_with_stats_only\"></div>"; ?>
		<div class="clear"></div>
	</div>

	<div class="FilterRow FilterRowOdd">           
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Banner size'), $this);?>
</div>
		<?php echo "<div id=\"bannerSize\"></div>"; ?>
		<div class="clear"></div>
	</div>
            	
	<div class="FilterRow">
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Custom'), $this);?>
</div>
		<?php echo "<div id=\"custom\"></div>"; ?>
		<div class="clear"></div>
	</div>
                 
	<div style="clear: both;"></div>

</div>    
    