<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from channel_stats_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'channel_stats_filter.tpl', 5, false),)), $this); ?>
<!-- channel_stats_filter -->

<div class="ReportsFilter">
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Campaign'), $this);?>
</div> 
				<?php echo "<div id=\"campaignid\"></div>"; ?>
		<div class="clear"></div>
	</div>
    
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Date'), $this);?>
</div>
            <?php echo "<div id=\"datetime\"></div>"; ?>
		<div class="clear"></div>
	</div>
	
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Status'), $this);?>
</div>
            <?php echo "<div id=\"rstatus\"></div>"; ?>
		<div class="clear"></div>
	</div>  
</div>
<div style="clear: both;"></div>