<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:13
         compiled from period_stats.stpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'period_stats.stpl', 3, false),array('modifier', 'currency_span', 'period_stats.stpl', 4, false),array('modifier', 'number_span', 'period_stats.stpl', 28, false),)), $this); ?>
<!-- period_stats -->
<div class="DashStatsBlock DashLargeBlock">
	<h3 title="<?php echo smarty_function_localize(array('str' => 'Commissions'), $this);?>
 <?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
"><?php echo smarty_function_localize(array('str' => 'Commissions'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
	<div class="DashLargeBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysCommissions']->commission->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
	<div class="DashLargeBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysCommissions']->commission->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysCommissions']->commission->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
		</div>
	<?php echo "<div id=\"commissionsChart\"></div>"; ?>
	</div>
</div>

<?php echo "<div id=\"newsContent\"></div>"; ?>

<div class="DashStatsBlock DashSmallBlock">
    <h3 title="<?php echo smarty_function_localize(array('str' => 'Conversion Rate'), $this);?>
 <?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
"><?php echo smarty_function_localize(array('str' => 'Conversion Rate'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
    <div class="DashSmallBlockTop DashSmallChart">
    <?php echo "<div id=\"conversionRateChart\"></div>"; ?>
    </div>
</div>
<div class="DashStatsBlock DashSmallBlock">
	<h3 title="<?php echo smarty_function_localize(array('str' => 'Clicks'), $this);?>
 <?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
"><?php echo smarty_function_localize(array('str' => 'Clicks'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
	<div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysClicks']->count->all)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Total'), $this);?>
</span></div>
	<div class="DashSmallBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysClicks']->count->unique)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Unique'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysClicks']->count->raw)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Repeated'), $this);?>
</span>
		</div>
	</div>
</div>
<div class="DashStatsBlock DashSmallBlock">
	<h3 title="<?php echo smarty_function_localize(array('str' => 'Impressions'), $this);?>
 <?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
"><?php echo smarty_function_localize(array('str' => 'Impressions'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
	<div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysImpressions']->count->all)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Total'), $this);?>
</span></div>
	<div class="DashSmallBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysImpressions']->count->unique)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Unique'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysImpressions']->count->raw)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Repeated'), $this);?>
</span>
		</div>
	</div>
</div>
<div class="DashStatsBlock DashSmallBlock">
	<h3 title="<?php echo smarty_function_localize(array('str' => 'All Refunds'), $this);?>
 <?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
"><?php echo smarty_function_localize(array('str' => 'All Refunds'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
	<div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysRefunds']->commission->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
	<div class="DashSmallBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysRefunds']->commission->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysRefunds']->commission->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
		</div>
	</div>
</div>
<div class="DashStatsBlock DashSmallBlock">
    <h3 title="<?php echo smarty_function_localize(array('str' => 'All Chargebacks'), $this);?>
 <?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
"><?php echo smarty_function_localize(array('str' => 'All Chargebacks'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
    <div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysChargebacks']->commission->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
    <div class="DashSmallBlockBottom">
        <div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysChargebacks']->commission->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

            <span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
        </div>
        <div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysChargebacks']->commission->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

            <span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
        </div>
    </div>
</div>
<div class="clear"></div>