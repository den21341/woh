<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from campaigns_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'campaigns_list.tpl', 4, false),)), $this); ?>
<!-- campaigns_list -->
<div class="Dash_FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Quick Navigation Icons'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <div class="IconsPanel">
  	<?php echo "<div id=\"Banners\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"DirectLinks\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Channels\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"AffLinkProtector\" class=\"SimpleIcon\"></div>"; ?>
  	<?php echo "<div id=\"Reports\" class=\"SimpleIcon\"></div>"; ?>
  </div>	
</div>
<div class="Dash_FormFieldset">
  <?php echo "<div id=\"CampaignsFilter\"></div>"; ?>
  <?php echo "<div id=\"CampaignsGrid\"></div>"; ?>
</div>