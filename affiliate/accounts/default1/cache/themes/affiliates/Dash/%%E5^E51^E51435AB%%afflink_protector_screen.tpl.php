<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:24
         compiled from afflink_protector_screen.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'afflink_protector_screen.tpl', 4, false),)), $this); ?>
<!-- afflink_protector_screen -->
<div class="Dash_FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Affiliate Link Protector/Cloaker'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
	<?php echo smarty_function_localize(array('str' => 'AffLinkProtectorInnerDescription'), $this);?>

</div>

<div class="Dash_FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Generate Cloaked File'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <table class="Dash_LinkProtectorTable">
      <tr>
          <td class="AffiliateLinkProtectorURLLabel"><?php echo smarty_function_localize(array('str' => 'URL to protect / cloak'), $this);?>
</td><td class="AffiliateLinkProtectorURL"><?php echo "<div id=\"urlToProtect\"></div>"; ?></td>
      </tr>
      <tr>
          <td><?php echo smarty_function_localize(array('str' => 'Redirection type'), $this);?>
</td><td><?php echo "<div id=\"redirectionType\"></div>"; ?></td>
      </tr>
      <tr>
          <td></td><td><?php echo "<div id=\"generateButton\"></div>"; ?></td>
      </tr>
  </table>
</div>

<div class="Dash_FormFieldset">
  <?php echo smarty_function_localize(array('str' => 'AffLinkProtectorHowToUse'), $this);?>

</div>