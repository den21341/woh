<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:08
         compiled from header.stpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'header.stpl', 3, false),)), $this); ?>
<!-- header -->
<div class="Dash_LAHeader">
	<a class="Dash_LALogo" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['programName'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" href="<?php echo $this->_tpl_vars['baseUrl']; ?>
/"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['programLogo'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" class="Dash_LALogoImage"/></a>
	<div style="position: absolute; right: 0px;"><?php echo $this->_tpl_vars['quChatButton']; ?>
</div>
	<div class="Dash_LACopyright"><?php echo $this->_tpl_vars['papCopyrightText']; ?>
<br /><?php echo ((is_array($_tmp=$this->_tpl_vars['papVersionText'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</div>
	<?php echo "<div id=\"Breadcrumbs\"></div>"; ?>
</div>


