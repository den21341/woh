<?php /* Smarty version 2.6.18, created on 2016-10-04 03:55:36
         compiled from banner_widget_header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'banner_widget_header.tpl', 7, false),)), $this); ?>
<!--    banner_widget_header   -->
<table class="BannerHeader" width="100%" padding="2" style="margin-left: 8px;">
<tr>
  <td style="color: #000000" class="TextAlignLeft" colspan="2">
  	<div class="BannerTypeLabel"><?php echo "<div id=\"bannerTypeLabel\"></div>"; ?></div>
  </td>
  <td><?php echo smarty_function_localize(array('str' => 'Banner name: '), $this);?>
</td>
  <td class="TextAlignLeft" style="color: #000000"><?php echo "<div id=\"bannerNameLabel\" class=\"Inline\"></div>"; ?></td>
</tr><tr>
  <td><?php echo smarty_function_localize(array('str' => 'Campaign: '), $this);?>
</td>
  <td class="TextAlignLeft" style="color: #000000"><?php echo "<div id=\"campaignNameLabel\" class=\"Inline\"></div>"; ?></td>

  <td class="TextAlignLeft"><?php echo smarty_function_localize(array('str' => 'Target: '), $this);?>
</td>
  <td style="color: #000000" class="TextAlignLeft"><?php echo "<div id=\"bannerTargetUrlLabel\" class=\"Inline\"></div>"; ?></td>
</tr>
<tr>
  <td><?php echo smarty_function_localize(array('str' => 'Commissions: '), $this);?>
</td>
  <td style="color: #000000" class="TextAlignLeft" colspan="3"><?php echo "<div id=\"campaignDetailsLabel\" class=\"Inline\"></div>"; ?></td>
</tr>
</table>