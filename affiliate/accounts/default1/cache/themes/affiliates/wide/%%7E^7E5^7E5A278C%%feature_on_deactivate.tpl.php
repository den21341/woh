<?php /* Smarty version 2.6.18, created on 2016-10-04 06:09:01
         compiled from feature_on_deactivate.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'feature_on_deactivate.tpl', 2, false),)), $this); ?>
<!-- feature_on_deactivate-->
<?php echo smarty_function_localize(array('str' => 'Are you sure you want to deactivate this feature?'), $this);?>
