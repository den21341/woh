<?php /* Smarty version 2.6.18, created on 2016-10-05 01:14:33
         compiled from sub_aff_tree.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'sub_aff_tree.tpl', 6, false),)), $this); ?>
<!--  sub_aff_tree    -->

<div class="Dash_FormFieldset">

    <div class="SubAffiliates">
    <?php echo smarty_function_localize(array('str' => 'Number of all your subaffiliates:'), $this);?>
 <?php echo "<div id=\"numberOfAllSubaffiliates\"></div>"; ?>
    </div>

    <div class="SubAffiliates">
        <?php echo "<div id=\"treeSwitch\"></div>"; ?>
        <div class="clear"></div>

        <div class="ClassicTree">
            <?php echo "<div id=\"subAffTree\"></div>"; ?>
        </div>
        
        <div class="GraphicTree">
            <?php echo "<div id=\"graphicSubAffTree\"></div>"; ?>
        </div>
    </div>

</div>