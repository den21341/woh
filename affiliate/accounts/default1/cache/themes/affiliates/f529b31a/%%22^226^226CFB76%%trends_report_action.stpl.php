<?php /* Smarty version 2.6.18, created on 2016-10-06 06:14:55
         compiled from trends_report_action.stpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', 'trends_report_action.stpl', 7, false),array('modifier', 'number_span', 'trends_report_action.stpl', 13, false),array('modifier', 'currency_span', 'trends_report_action.stpl', 18, false),array('function', 'localize', 'trends_report_action.stpl', 12, false),array('function', 'math', 'trends_report_action.stpl', 28, false),array('function', 'ratio', 'trends_report_action.stpl', 35, false),array('function', 'ratioPercentage', 'trends_report_action.stpl', 55, false),)), $this); ?>
<!-- trends_report_action -->
<?php $_from = $this->_tpl_vars['selected']['transactionTypes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['trans'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['trans']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['transactions']):
        $this->_foreach['trans']['iteration']++;
?>
	<?php if ($this->_tpl_vars['transactions']->type == $this->_tpl_vars['actionFilter'] && $this->_tpl_vars['transactions']->commissiontypeid == $this->_tpl_vars['commtypeidFilter']): ?>
		<table class="TrendStats">
    		<tbody>   
                <tr class="gray">
            		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->name)) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</td>
            		<td></td>
            		<td></td>
        		</tr>
                <tr class="light">
                    <td><?php echo smarty_function_localize(array('str' => 'number'), $this);?>
</td>
                    <td><?php echo ((is_array($_tmp=$this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->count->all)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>
</td>
                    <td></td>
                </tr>
                <tr class="dark">
                    <td><?php echo smarty_function_localize(array('str' => 'Fixed cost'), $this);?>
</td>
                    <td><?php echo ((is_array($_tmp=$this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->fixedCost->all)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
</td>
                    <td></td>
                </tr>
                <tr class="light">
                    <?php if ($this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->refunds->commission->all + $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->chargebacks->commission->all == 0): ?>
                        <td><?php echo smarty_function_localize(array('str' => 'commission'), $this);?>
</td>                        
                        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->commission->all)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
</td>
                    <?php else: ?>                     
                        <td><?php echo smarty_function_localize(array('str' => 'commission'), $this);?>
<br><span style="font-size:10px">total / <span style="color:#B33024">refunds and charge backs</span></span></td></td>                        
                        <td><?php echo smarty_function_math(array('equation' => "x+y+z",'x' => $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->commission->all,'y' => $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->refunds->commission->all,'z' => $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->chargebacks->commission->all,'assign' => 'selectedCommissions'), $this);?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['selectedCommissions'])) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<br>
                            <span style="font-size:10px">
                                <?php echo ((is_array($_tmp=$this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->commission->all)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
 / 
                                <?php echo smarty_function_math(array('equation' => "x+y",'x' => $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->refunds->commission->all,'y' => $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->chargebacks->commission->all,'assign' => 'selectedCommissionsLoss'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['selectedCommissionsLoss'])) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

                            </span>
                        </td>  
                    <?php endif; ?>
                    <td>(avg <?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->commission->all,'p2' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
)</td>
                </tr>
                <tr class="dark">
                    <?php if ($this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->refunds->totalCost->all + $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->chargebacks->totalCost->all == 0): ?>
                        <td><?php echo smarty_function_localize(array('str' => 'revenue'), $this);?>
</td>
                        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->totalCost->all)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
</td>
                    <?php else: ?>
                        <td><?php echo smarty_function_localize(array('str' => 'revenue'), $this);?>
<br><span style="font-size:10px">total / <span style="color:#B33024">refunds and charge backs</span></span></td>
                        <td><?php echo smarty_function_math(array('equation' => 'x-y-z','x' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->totalCost->all,'y' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->refunds->totalCost->all,'z' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->chargebacks->totalCost->all,'assign' => 'selectedTotalCostMinusLoss'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['selectedTotalCostMinusLoss'])) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
 <br>
                            <span style="font-size:10px">
                                <?php echo smarty_function_math(array('equation' => "x+y+z",'x' => $this->_tpl_vars['selectedTotalCostMinusLoss'],'y' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->refunds->totalCost->all,'z' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->chargebacks->totalCost->all,'assign' => 'selectedTotalCost'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['selectedTotalCost'])) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
 / 
                                <?php echo smarty_function_math(array('equation' => '-x-y','x' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->refunds->totalCost->all,'y' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->chargebacks->totalCost->all,'assign' => 'selectedTotalLoss'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['selectedTotalLoss'])) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

                            </span>
                        </td>
                    <?php endif; ?>
                    <td>(avg <?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->totalCost->all,'p2' => $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
)</td>
                </tr>
                <tr class="light">
                    <td><?php echo smarty_function_localize(array('str' => 'conversion'), $this);?>
</td>
                    <td><span title="<?php echo smarty_function_localize(array('str' => 'count'), $this);?>
(<?php echo $this->_tpl_vars['selected']['transactionTypesFirstTier']->types[($this->_foreach['trans']['iteration']-1)]->count->all; ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks'), $this);?>
(<?php echo $this->_tpl_vars['selected']['clicks']->count->all; ?>
)" class="NumberData"><?php echo smarty_function_ratioPercentage(array('p1' => $this->_tpl_vars['selected']['transactionTypes']->types[($this->_foreach['trans']['iteration']-1)]->count->all,'p2' => $this->_tpl_vars['selected']['clicks']->count->all), $this);?>
</span></td>
                	<td></td>
                </tr>
			</tbody>
		</table>                                
	<?php endif; ?>	
<?php endforeach; endif; unset($_from); ?>