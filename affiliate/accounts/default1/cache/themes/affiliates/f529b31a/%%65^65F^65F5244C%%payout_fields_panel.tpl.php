<?php /* Smarty version 2.6.18, created on 2016-10-05 01:14:20
         compiled from payout_fields_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'payout_fields_panel.tpl', 3, false),)), $this); ?>
<!--    payout_fields_panel     -->
<fieldset>
    <legend><?php echo smarty_function_localize(array('str' => 'Payout method and data'), $this);?>
</legend>
    <?php echo "<div id=\"payoutoptionid\" class=\"PayoutOptionId\"></div>"; ?>
    <?php echo "<div id=\"payoutOptions\"></div>"; ?>
</fieldset>

<fieldset>
	<legend><?php echo smarty_function_localize(array('str' => 'Payout balances'), $this);?>
</legend>
	<?php echo "<div id=\"minimumPayoutOptions\"></div>"; ?>
	<?php echo "<div id=\"minimumpayout\"></div>"; ?>
</fieldset>

<fieldset>
	<legend><?php echo smarty_function_localize(array('str' => 'Invoicing options'), $this);?>
</legend>
	<?php echo "<div id=\"invoicingNotSupported\"></div>"; ?>
	<?php echo "<div id=\"applyVatInvoicing\"></div>"; ?>
	<?php echo "<div id=\"vatPercentage\"></div>"; ?>
	<?php echo "<div id=\"vatNumber\"></div>"; ?>
	<?php echo "<div id=\"amountOfRegCapital\"></div>"; ?>
	<?php echo "<div id=\"regNumber\"></div>"; ?>
</fieldset>