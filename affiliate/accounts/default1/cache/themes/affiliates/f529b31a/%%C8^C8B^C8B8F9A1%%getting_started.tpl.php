<?php /* Smarty version 2.6.18, created on 2016-10-05 01:56:02
         compiled from custom/getting_started.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'custom/getting_started.tpl', 4, false),)), $this); ?>
<table>
<tr>
  <td valign="top" width="400px" style="padding: 20px;">
<a href='#188461-promotion_tips' style="text-decoration: underline; font-weight: bold; color:#135fab"><?php echo smarty_function_localize(array('str' => 'Promotion tips'), $this);?>
</a><br/>
<?php echo smarty_function_localize(array('str' => 'Tips and common practice you can do to increase the traffic and earn more commissions from our affiliate program.'), $this);?>
 
  </td>

</tr><tr>

  <td valign="top" width="400px" style="padding: 20px;">
<a href='#150623-frequently_asked_questions' style="text-decoration: underline; font-weight: bold; color:#135fab"><?php echo smarty_function_localize(array('str' => 'Frequently asked questions'), $this);?>
</a><br/>
<?php echo smarty_function_localize(array('str' => 'Questions and answers about our affiliate program, the conditions, payouts, tracking, and much more.'), $this);?>

  </td>

</tr><tr>

  <td valign="top" width="400px" style="padding: 20px;">
<a href='#159219-advanced_tracking' style="text-decoration: underline; font-weight: bold; color:#135fab"><?php echo smarty_function_localize(array('str' => 'Advanced tracking'), $this);?>
</a><br/>
<?php echo smarty_function_localize(array('str' => 'Possibilities of advanced tracking in our affiliate program - includes tracking od Advertisement channels and Sub Id tracking.'), $this);?>

  </td>

</tr><tr>

  <td valign="top" width="400px" style="padding: 20px;">
<a href='#189911-directlinks_explained' style="text-decoration: underline; font-weight: bold; color:#135fab"><?php echo smarty_function_localize(array('str' => 'DirectLink tracking explained'), $this);?>
</a><br/>
<?php echo smarty_function_localize(array('str' => 'Explains how DirectLink tracking works and how you can use it.'), $this);?>

  </td>

</tr>
</table>