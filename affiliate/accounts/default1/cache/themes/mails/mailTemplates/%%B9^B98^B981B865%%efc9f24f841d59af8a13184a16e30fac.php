<?php /* Smarty version 2.6.18, created on 2016-10-04 07:52:12
         compiled from text://efc9f24f841d59af8a13184a16e30fac */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'text://efc9f24f841d59af8a13184a16e30fac', 10, false),)), $this); ?>
<h1 style="font-family: Arial;"><font size="4">Поздравляем</font></h1>
<p style="font-family: Arial;"><font size="2">Ваша почтовые учетные записи настроены верно и программа может отправлять электронную почту.</font></p>
<div style="font-family: Arial;">
    <fieldset>
        <legend><font size="2">Настройка почтовой учетной записи</font></legend>
        <table>
            <tbody><tr>
                <td style="font-weight: bold;"><font size="2">Название почтовой учетной записи</font></td>

                <td><font size="2"><?php echo ((is_array($_tmp=$this->_tpl_vars['account_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</font></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><font size="2">Имя отправителя</font></td>
                <td><font size="2"><?php echo ((is_array($_tmp=$this->_tpl_vars['from_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</font></td>
            </tr>
            <tr>

                <td style="font-weight: bold;"><font size="2">Адрес отправителя</font></td>
                <td><font size="2"><?php echo ((is_array($_tmp=$this->_tpl_vars['account_email'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</font></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><font size="2">Использовать протокол SMTP</font></td>
                <td><font size="2"><?php echo $this->_tpl_vars['use_smtp']; ?>
</font></td>
            </tr>

            <tr>
                <td style="font-weight: bold;"><font size="2">SMTP сервер</font></td>
                <td><font size="2"><?php echo ((is_array($_tmp=$this->_tpl_vars['smtp_server'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</font></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><font size="2">SMTP порт</font></td>
                <td><font size="2"><?php echo ((is_array($_tmp=$this->_tpl_vars['smtp_port'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</font></td>

            </tr>
            <tr>
                <td style="font-weight: bold;"><font size="2">SMTP аутентификация</font></td>
                <td><font size="2"><?php echo $this->_tpl_vars['smtp_auth']; ?>
</font></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><font size="2">Использовать безопасное соединение</font></td>

                <td><font size="2"><?php echo $this->_tpl_vars['smtp_ssl']; ?>
</font></td>
            </tr>
            <tr>
                <td style="font-weight: bold;"><font size="2">Имя пользователя SMTP</font></td>
                <td><font size="2"><?php echo ((is_array($_tmp=$this->_tpl_vars['smtp_username'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</font></td>
            </tr>
            <tr>

                <td style="font-weight: bold;"><font size="2">Почтовая учетная запись по умолчанию</font></td>
                <td><font size="2"><?php echo $this->_tpl_vars['is_default']; ?>
</font></td>
            </tr>
        </tbody></table>
    </fieldset>
</div>