<?php /* Smarty version 2.6.18, created on 2016-09-30 01:45:42
         compiled from footer.stpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'footer.stpl', 6, false),)), $this); ?>
		<!-- footer -->
		<div class="cleaner"></div>
<div class="c1_Footer">
	<div class="c1_FooterContainer">
		<div class="c1_FooterList c1_FirstList">
			<h3><?php echo smarty_function_localize(array('str' => 'About us'), $this);?>
</h3>
			<ul>
				<li><a href="https://www.qualityunit.com"><?php echo smarty_function_localize(array('str' => 'Company'), $this);?>
</a></li>
				<li><a href="https://www.qualityunit.com/company/contact-us/"><?php echo smarty_function_localize(array('str' => 'Contact us'), $this);?>
</a></li>
                <li><a href="https://www.qualityunit.com/privacy-policy-quality-unit/"><?php echo smarty_function_localize(array('str' => 'Privacy Policy'), $this);?>
</a></li>
                <li><a href="https://www.postaffiliatepro.com"><?php echo smarty_function_localize(array('str' => 'Product homepage'), $this);?>
</a></li>
				<li><a href="https://www.facebook.com/PostAffiliatePro" class="c1_Facebook"><?php echo smarty_function_localize(array('str' => 'Facebook'), $this);?>
</a></li>
				<li><a href="http://twitter.com/#!/qualityunit" class="c1_Twitter"><?php echo smarty_function_localize(array('str' => 'Twitter'), $this);?>
</a></li>
				<li><a href="https://plus.google.com/106499593065725716164?prsrc=3" class="c1_Google"><?php echo smarty_function_localize(array('str' => 'Google+'), $this);?>
</a></li>
			</ul>
		</div>
		<div class="c1_FooterList">
			<h3><?php echo smarty_function_localize(array('str' => 'Affiliates'), $this);?>
</h3>
			<ul>
				<li><a href="<?php echo $this->_tpl_vars['baseUrl']; ?>
/affiliates/"><?php echo smarty_function_localize(array('str' => 'Affiliates information'), $this);?>
</a></li>
				<li><a href="https://www.postaffiliatepro.com/features/affiliate-link-styles/"><?php echo smarty_function_localize(array('str' => 'Affiliate link formats'), $this);?>
</a></li>
			</ul>
		</div>
		<div class="c1_FooterList">
			<h3>Merchants</h3>
			<ul>
				<li><a href="<?php echo $this->_tpl_vars['baseUrl']; ?>
/merchants/home.php"><?php echo smarty_function_localize(array('str' => 'Merchants information'), $this);?>
</a></li>
				<li><a href="http://addons.qualityunit.com/integration-methods"><?php echo smarty_function_localize(array('str' => 'Integration methods'), $this);?>
</a></li>
				<li><a href="https://support.qualityunit.com/712031-API"><?php echo smarty_function_localize(array('str' => 'API Reference'), $this);?>
</a></li>
				<li><a href="<?php echo $this->_tpl_vars['baseUrl']; ?>
/samples_tests"><?php echo smarty_function_localize(array('str' => 'Samples and Tests'), $this);?>
</a></li>
			</ul>
		</div>
		<div class="c1_FooterList c1_Mobile">
			<h3><?php echo smarty_function_localize(array('str' => 'Mobile apps'), $this);?>
</h3>
			<ul>
				<li><a href="https://itunes.apple.com/us/app/post-affiliate-pro-mobile/id1100644457" class="c1_AppStore"></a></li>
				<li><a href="https://play.google.com/store/apps/details?id=com.qualityunit.android.postaffiliatepro" class="c1_Gplay"></a></li>
			</ul>
		</div>
		<div class="c1_FooterList">
			<h3><?php echo smarty_function_localize(array('str' => 'Useful tools'), $this);?>
</h3>
			<ul>
				<li><a href="https://www.ladesk.com/"><?php echo smarty_function_localize(array('str' => 'Help Desk Software'), $this);?>
</a></li>
				<li><a href="https://www.ladesk.com/tour/live-chat-software/"><?php echo smarty_function_localize(array('str' => 'Live Chat Software'), $this);?>
</a></li>
				<li><a href="https://www.ladesk.com/features/knowledge-base/"><?php echo smarty_function_localize(array('str' => 'Knowledgebase builder'), $this);?>
</a></li>
				<li><a href="https://www.ladesk.com/features/call-center/"><?php echo smarty_function_localize(array('str' => 'Call Center'), $this);?>
</a></li>
			</ul>
		</div>
		<div class="c1_Copyright"><?php echo $this->_tpl_vars['papCopyrightText']; ?>
</div>
	</div>
</div>
<script id="pap_x2s6df8d" src="../scripts/trackjs.php" type="text/javascript">
</script>
<script type="text/javascript">
papTrack();
PostAffTracker.writeCookieToCustomField("visitorId");
</script>