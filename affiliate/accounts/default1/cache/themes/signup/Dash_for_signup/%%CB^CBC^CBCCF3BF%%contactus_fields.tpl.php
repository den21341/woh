<?php /* Smarty version 2.6.18, created on 2016-09-30 02:32:49
         compiled from contactus_fields.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'contactus_fields.tpl', 4, false),)), $this); ?>
<!-- contactus_fields -->

<div class="ContactusForm">
	<div class="ContactusFormLegend"><?php echo smarty_function_localize(array('str' => 'Contact us'), $this);?>
</div>
	<?php echo "<div id=\"name\" class=\"ContactUsText\"></div>"; ?>
	<?php echo "<div id=\"email\" class=\"ContactUsText\"></div>"; ?>
	<?php echo "<div id=\"text\" class=\"ContactUsText\"></div>"; ?>
	<?php echo "<div id=\"recaptcha\"></div>"; ?>
	<?php echo "<div id=\"FormMessage\"></div>"; ?>  
	<?php echo "<div id=\"SendButton\"></div>"; ?>
    <div class="clear"></div>
</div>