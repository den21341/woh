<?php /* Smarty version 2.6.18, created on 2016-09-30 02:32:49
         compiled from post_signup_page.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'post_signup_page.tpl', 4, false),)), $this); ?>
<!-- post_signup_page -->

<div class="c1_Wrapper c1_WrapperContact">
	<h1 class="c1_single"><?php echo smarty_function_localize(array('str' => 'Post signup page.'), $this);?>
</h1>
	<div class="c1_WrapperContainer">
		<div class="c1_WindowIn c1_PostSignup">
			<div class="WindowContent">
				<h2><?php echo smarty_function_localize(array('str' => 'You have been successfully signed up.'), $this);?>
</h2>
			</div>
		</div>
	</div>
</div>