<?php /* Smarty version 2.6.18, created on 2016-10-04 03:52:56
         compiled from set_new_password_form.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'set_new_password_form.tpl', 3, false),)), $this); ?>
<!-- set_new_password_form -->
<div class="c1_Wrapper c1_WrapperContact">
	<h1 class="c1_single"><?php echo smarty_function_localize(array('str' => 'Set Password'), $this);?>
</h1>
	<div class="c1_WrapperContainer">
		<div class="c1_WindowIn c1_TopPadding">
			<div class="WindowContent">
			<div class="c1_Uname">
			<?php echo "<div id=\"username\"></div>"; ?>
			</div>
            <?php echo "<div id=\"password\"></div>"; ?>
			<div class="CaptContainer">
            <?php echo "<div id=\"lost_pw_captcha\"></div>"; ?>
			</div>
			<?php echo "<div id=\"FormMessage\"></div>"; ?>
			<?php echo "<div id=\"SendButton\"></div>"; ?>
			<?php echo "<div id=\"requestNewPassword\"></div>"; ?>
			<?php echo "<div id=\"backToLogin\"></div>"; ?>
			<div class="clear"></div>
			</div>
		</div>
	</div>
</div>