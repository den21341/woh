<?php /* Smarty version 2.6.18, created on 2016-10-04 06:28:29
         compiled from signup_form_disabled.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'signup_form_disabled.tpl', 7, false),)), $this); ?>
<!-- signup_form_disabled -->
<div class="MainPanel">
<div class="ContentLeft">
<div class="ContentRight">
<div class="MainInfoContent">

<p><?php echo smarty_function_localize(array('str' => 'Signup form is disabled.'), $this);?>
</p>

<div class="clear"></div>
</div>
</div>
</div>
</div>