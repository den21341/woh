<?php /* Smarty version 2.6.18, created on 2016-10-04 06:28:16
         compiled from account_post_signup_page.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'account_post_signup_page.tpl', 11, false),)), $this); ?>
<!-- account_post_signup_page -->

<div class="PostSignUpPage">
<div class="FormPanel">
<div class="MainPanel">
<div class="ContentLeft">
<div class="ContentRight">
<div class="MainInfoContent">
<div class="PostSignUp">

<h2 class="noborder"><?php echo smarty_function_localize(array('str' => 'Post signup page.'), $this);?>
</h2><?php echo smarty_function_localize(array('str' => 'You have been successfully signed up.'), $this);?>
  


</div>
</div>
</div>
</div>
</div>
</div>
</div>