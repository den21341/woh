<?php /* Smarty version 2.6.18, created on 2016-10-04 07:20:03
         compiled from header.stpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'header.stpl', 4, false),)), $this); ?>
<!-- header -->
<div class="Header">
    <div class="HeaderInfo">
        <strong><a class="Logo" title="<?php echo ((is_array($_tmp=$this->_tpl_vars['programName'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" href="<?php echo $this->_tpl_vars['baseUrl']; ?>
/">
                   <img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['programLogo'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" class="LogoImage"></a></strong>
        <strong class="Title"><?php echo ((is_array($_tmp=$this->_tpl_vars['programName'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</strong>
        <p class="Text"></p>
    </div>
    <?php if ($this->_tpl_vars['isLogged'] == 1): ?>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'topmenu_logged.stpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php else: ?>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'topmenu_notlogged.stpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php endif; ?>
</div>


