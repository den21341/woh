<?php /* Smarty version 2.6.18, created on 2016-10-04 07:20:43
         compiled from contactus_fields.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'contactus_fields.tpl', 5, false),)), $this); ?>
<!-- contactus_fields -->

<div class="ContactusForm">
    <fieldset>
        <legend><?php echo smarty_function_localize(array('str' => 'Contact us'), $this);?>
</legend>
        <?php echo "<div id=\"name\" class=\"ContactUsText\"></div>"; ?>
        <?php echo "<div id=\"email\" class=\"ContactUsText\"></div>"; ?>
        <?php echo "<div id=\"text\" class=\"ContactUsText\"></div>"; ?>
        <?php echo "<div id=\"recaptcha\"></div>"; ?>
    </fieldset>

    <?php echo "<div id=\"FormMessage\"></div>"; ?>
    <?php echo "<div id=\"SendButton\"></div>"; ?>
    <div class="clear"></div>
</div>