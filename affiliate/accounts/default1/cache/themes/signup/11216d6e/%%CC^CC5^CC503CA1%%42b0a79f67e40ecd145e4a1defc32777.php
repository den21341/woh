<?php /* Smarty version 2.6.18, created on 2016-10-05 05:37:16
         compiled from text://42b0a79f67e40ecd145e4a1defc32777 */ ?>
<!-- index -->
<div class="MainPanel">
<div class="ContentLeft">
<div class="ContentRight">
<div class="MainInfoContent">

<div class="login-box-white"><div class="login-box"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'join_now.stpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div></div>
<h2 class="first">Пожизненные % от приведенных пользователей</h2>
<p>Зарабатывайте 16 % от каждого взноса приведенного пользователя и 10 % пожизненно от всех последующих его взносов.</p>
<h2>Мы знаем, как возвращать Вашего пользователя еще и еще!!!</h2>
<p>
Визуализация добрых дел каждого пользователя:
<ul>
<li>Международная карта добрых дел;</li>
<li>Региональная карта добрых дел;</li>
<li>Дерево добрых дел (добрые дела пользователя и добрые дела других людей, на которых повлиял пользователь). Одно доброе дело может привести к 3,5,10.. другим.</li>
</ul>
</p>
<h2>How does it work?</h2>
<p>It couldn't be simpler! We've streamlined our entire affiliate process to ensure ease of use, 
while still maintaining extremely accurate tracking methods. The process is as follows:</p>
<ol>
    <li>Visitor clicks on an affiliate link on your site or in an email.</li>
    <li>The visitors IP is logged and a cookie is placed in their browser for tracking purposes.</li>
    <li>The visitor browses our site, and may decide to order.</li>
    <li>If the visitor orders (the order need not be placed during the same browser session--cookies and IPs are stored indefinitely), the order will be registered as a sale for you.</li>
    <li>We will review and approve the sale.</li>
    <li>You will receive commission payouts on the 15th of every month (if your balance is at least $100) via PayPal.</li>
</ol>
<p><strong>That's it! You send us business, we send you money!</strong></p>
<h2>Rules and Requirements</h2>
<p>We do have some basic ground rules for the program, so please read before signing     up:</p>
<ul>
    <li>All participants must have a PayPal account (in order to receive payouts).</li>
    <li>All participants in the US must enter a US Social Security or Taxpayer ID number. International customers simply enter &quot;International&quot; in this field.</li>
    <li>You may NOT receive credit for referring yourself.</li>
</ul>
<p>Participants caught violating or attempting to circumvent these rules will have all affiliate credit voided and will not receive payouts. Additionally, they will be banned from further participation in our affiliate program.</p>
<h2>Ready to join?</h2>
<p>Signing up and getting your account configured couldn't be easier. You can be referring business our way in as little as five minutes.</p>
<p><a href="./signup.php">&raquo; click here to join</a></p>


<div class="clear"></div>
</div>
</div>
</div>
</div>