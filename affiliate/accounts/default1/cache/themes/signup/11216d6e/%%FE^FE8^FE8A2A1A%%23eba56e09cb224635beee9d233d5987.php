<?php /* Smarty version 2.6.18, created on 2016-10-06 04:15:38
         compiled from text://23eba56e09cb224635beee9d233d5987 */ ?>
<!-- faq_public -->
<div class="MainPanel">
<div class="ContentLeft">
<div class="ContentRight">
<div class="MainInfoContent">

<div class="login-box-white"><div class="login-box"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'join_now.stpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div></div>

<h2 class="first">Часто задаваемые вопросы</h2>

<table border=0 cellspacing=0 cellpadding=2>
    <tr><td class="TextAlignLeft">&nbsp;&nbsp;<a class=ap_faq href="#whatineed">1. Что мне нужно сделать, чтобы стать партнером?</a></td></tr>
    <tr><td class="TextAlignLeft">&nbsp;&nbsp;<a class=ap_faq href="#howpaid">2. Как я знаю, что была оплата от моего реферала?</a></td></tr>
    <tr><td class="TextAlignLeft">&nbsp;&nbsp;<a class=ap_faq href="#payment">3. Как происходят выплаты?</a></td></tr>
    <tr><td class="TextAlignLeft">&nbsp;&nbsp;<a class=ap_faq href="#setup">4. Как настроить учетную запись?</a></td></tr>
    <tr><td class="TextAlignLeft">&nbsp;&nbsp;<a class=ap_faq href="#afflink">5. Что такое партнерская ссылка?</a></td></tr>
    <tr><td class="TextAlignLeft">&nbsp;&nbsp;<a class=ap_faq href="#ppc">6. Могу ли я рекламировать вас через контекстную рекламу в поисковых системах?</a></td></tr>
    <tr><td class="TextAlignLeft">&nbsp;&nbsp;<a class=ap_faq href="#training">7. Есть ли у вас какие-либо учебные программы для партнеров?</a></td></tr>
    <tr><td class="TextAlignLeft">&nbsp;&nbsp;<a class=ap_faq href="#whatnow">8. Что мне нужно сделать?</a></td></tr>
</table>
<br/>

<p>
<a name="whatineed"></a>
<span class="top_feat">1. Что мне нужно сделать , чтобы стать партнером?</span><br/>
Ничего!
Кроме вашего желания стать партнером.
</p>
<p>
Вам даже не обязательно иметь свой сайт. Вы можете рекламировать нашу продукцию через поисковые системы  (<a href="http://adwords.google.com/support/"> Узнайте, как</a>), посты в интернет-форумах и Twitter, размещение ссылок на Facebook, LinkedIn, запись видео, или просто по электронной почте для всех ваших друзей и людей, которых вы знаете, по вашей партнерской ссылке на наш продукт. Если у Вас есть собственный веб-сайт, просто попробуйте разместить на нем наши готовые промо-материалы с вашими партнерскими ссылками. Так же вы можете создать свои собственные промо, будь то баннеры, тизеры, прелендинги, статьи с ссылками, использовать брендирование и т.д.
</p>
<p>
Все, что вам нужно сделать, это отправить посетителя на наш сайт с помощью специальной ссылки (так называемый «партнерской ссылке»), и если он или она сделают оплату у нас, вы получите комиссионные.
</p>

<p>
<a name="howpaid"></a>
<span class="top_feat">2. Как я знаю, что была оплата от моего реферала?</span><br/>
Партнерская программа работает на базе Post Affiliate Pro, - это ведущее программное обеспечение партнерского отслеживания.  Для отслеживания рефералов и  наилучшей надежности использует сочетание кукисов и IP - адресов . Когда посетитель следует по вашей партнерской ссылке на наш сайт, наша партнерская система регистрирует его и помещает куки на его компьютере. Когда посетитель производит оплату, партнерская система проверяет куки (если они не найдены, проверяет наличие IP-адреса) и переводит на ваш счет % от стоимости оплаты. 
Этот процесс является абсолютно автоматическим. Все ваши рефералы будут надлежащим образом отслеживаться.
<br/>
</p>
<p>
На базе Post Affiliate Pro работают тысячами интернет-торговцев и партнерских програм по всему миру.
</p>
<p>
<a name="payment"></a>
<span class="top_feat">3. Как происходят выплаты?</span><br/>
Выплаты происходят в течении 3 дней, после зачисления денег на счет, на выбранную вами платежную систему. Минимальная сумма выплат 700 руб.
</p>
<p>
<a name="setup"></a>
<span class="top_feat">4. Как настроить учетную запись?</span><br/>
Настроить учетную запись очень просто, и это абсолютно бесплатно.
</br>
Все, что вам нужно, это перейти к <a target="_blank" href="http://affiliate.wayofhelp.com/affiliates/signup.php" >Форме регистрации</a> и заполнить ее. После чего вы получите электронное письмо с дальнейшими инструкциями.
</p>
<p>
Как наш партнер, вы будете иметь свою собственную панель управления, где вы можете увидеть подробную статистику трафика и оплат от ваших рефералов, посмотреть новости и учебные материалы, а также выбрать баннеры и текстовые ссылки.
</p>
<p>
Затем  вам нужно  разместить партнерскую ссылку, баннер, текстовую ссылку или любую другую ссылку на ваш сайт. Так же подойдут контекстные объявления из поисковых систем и оплатой за действие из других источников трафика (тизерные сети, баннерные сети).
</p>

<p>
<a name="afflink"></a>
<span class="top_feat">5. Что такое партнерская ссылка?</span><br/>
Партнерская ссылка это специальный URL , куда вы должны посылать посетителей. Вы получите URL для различных баннеров в вашей партнерской панели после входа в систему .
</p>

<p>
<a name="ppc"></a>
<span class="top_feat">6. Могу ли я рекламировать вас через контекстную рекламу в поисковых системах?</span><br/>
ДА! Вы можете рекламировать нас через контекстную рекламу в поисковых системах.  Мы видим, что несколько наших партнеров очень удачно используют этот вид рекламы, и делают на этом хорошие деньги. ДА! Вы можете рекламировать нас через контекстную рекламу в поисковых системах.  Мы видим, что несколько наших партнеров очень удачно используют этот вид рекламы, и делают на этом хорошие деньги. 
</p>

<p>
<a name="whatnow"></a>
<span class="top_feat">7. Есть ли у вас какие-либо учебные программы для партнеров?</span><br/>
Да, основы партнерского маркетинга и наиболее полезные советы описаны в вашей партнерской панели. Так же вы можете найти новые советы и методы в нашей партнерской рассылке.
</p>

<p>
<a name="whatnow"></a>
<span class="top_feat">8. Что мне нужно сделать?</span><br/>
Несколько простых шагов: <br/>
1. Перейдите к <a href=signup.php>Форме регистрации</a><br/>
2. Заполните форму  <br/>
3. Перейдите по ссылке на электронной почте и установите пароли.<br/> 
4. Войдите в свою собственную партнерскую панель и выберите кампанию, которую вы хотите рекламировать  и которая, по вашему мнению, принесет Вам большую прибыль. Выберите  промо материалы из имеющихся (баннеры, ссылки, тизеры и др.), либо создайте свои собственные и подставьте  в них партнерскую ссылку.<br/>
5. Поместите  промо на главной странице вашего сайта или на любых других страницах. Пробуйте разные варианты,  чтобы увеличить вашу прибыль. Так же пробуйте варианты с контекстной рекламой и CPA  сетями.<br/>
6. Получите пожизненную комиссию от каждого вашего реферала!
</p>

<div class="clear"></div>
</div>
</div>
</div>
</div>