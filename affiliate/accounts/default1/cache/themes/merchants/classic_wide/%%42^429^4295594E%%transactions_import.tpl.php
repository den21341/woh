<?php /* Smarty version 2.6.18, created on 2016-10-04 06:32:33
         compiled from transactions_import.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'transactions_import.tpl', 8, false),)), $this); ?>
<!-- transactions_import -->
<div class="TransactionImport">
    <table>
        <tr>
            <td valign='top'>
    			<div class="TransactionImportFields FloatLeft">
        			<fieldset>
            			<legend><?php echo smarty_function_localize(array('str' => 'Import file format'), $this);?>
</legend>
            			<?php echo "<div id=\"fields\"></div>"; ?>
                        <?php echo "<div id=\"addButton\"></div>"; ?>
        			</fieldset>
    			</div>
            </td>
            <td valign='top'>
                <div class="TransactionImportFile FloatLeft">
                    <fieldset>
                        <legend><?php echo smarty_function_localize(array('str' => 'Import file'), $this);?>
</legend>
                        <?php echo "<div id=\"delimiter\"></div>"; ?>
                        <?php echo "<div id=\"source\" class=\"ImportRadioGroup\"></div>"; ?>
                        <?php echo "<div id=\"url\"></div>"; ?>
                        <?php echo "<div id=\"uploadFile\"></div>"; ?>
                        <?php echo "<div id=\"exportFilesGrid\"></div>"; ?> 
                        <?php echo "<div id=\"serverFile\"></div>"; ?>
                        <?php echo "<div id=\"skipFirstRow\"></div>"; ?>
                        <?php echo "<div id=\"transactionType\"></div>"; ?>
                    </fieldset>
                </div>
                <div class="clear"></div>
                <div class="TransactionImportSettings FloatLeft">
                    <fieldset>
                        <legend><?php echo smarty_function_localize(array('str' => 'Transaction import settings'), $this);?>
</legend>
                        <?php echo "<div id=\"computeAtomaticaly\"></div>"; ?>
                        <?php echo "<div id=\"matchTransaction\"></div>"; ?>
                        <?php echo "<div id=\"matchTransactionStatus\"></div>"; ?> 
                        <?php echo "<div id=\"transactionStatus\"></div>"; ?>
                    </fieldset>
                </div>
                <div class="clear"></div>
                <div class="TransactionImportSettings FloatLeft">
                    <?php echo "<div id=\"importButton\"></div>"; ?>
                </div>
            </td>
        </tr>
    </table>
</div>