<?php /* Smarty version 2.6.18, created on 2016-10-06 05:58:14
         compiled from quick_report_content_multitier.stpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'quick_report_content_multitier.stpl', 7, false),array('function', 'ratio', 'quick_report_content_multitier.stpl', 40, false),array('modifier', 'escape', 'quick_report_content_multitier.stpl', 22, false),array('modifier', 'currency_span', 'quick_report_content_multitier.stpl', 23, false),array('modifier', 'currency', 'quick_report_content_multitier.stpl', 24, false),)), $this); ?>
<!-- quick_report_content_multitier -->

<div class="OverviewInnerBox">
    <table class="StatsSummaries">
        <tr class="gray">
            <td rowspan="2"></td>
            <td align="center" rowspan="2"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</td>
            <td align="center" rowspan="2"><?php echo smarty_function_localize(array('str' => 'Declined'), $this);?>
</td>
            <td align="center" colspan="2"><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</td>
        </tr>
        <tr class="gray">
            <td align="center"><?php echo smarty_function_localize(array('str' => 'Unpaid'), $this);?>
</td>
            <td align="center"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</td>
        </tr>
        <?php echo $this->_tpl_vars['sumTransaction']->clear(); ?>

        <?php $_from = $this->_tpl_vars['transactionTypesTier']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['transactions']):
?>
            <?php if ($this->_tpl_vars['transactions']->type != 'R' && $this->_tpl_vars['transactions']->type != 'H'): ?>
                <?php if ($this->_tpl_vars['transactions']->type != 'A' || $this->_tpl_vars['transactions']->campaignName != ''): ?>
                    <?php echo $this->_tpl_vars['sumTransaction']->add($this->_tpl_vars['transactions']); ?>

                <?php endif; ?>
                <tr>
                    <td class="TextAlignLeft" valign="top"><?php if ($this->_tpl_vars['transactions']->type == 'A' && $this->_tpl_vars['transactions']->campaignName != ''): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->campaignName)) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 - <?php endif; ?><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->name)) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</td>
                    <td class="TextAlignRight" valign="top"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->commission->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

                      <?php if ($this->_tpl_vars['transactions']->refunds->commission->pending != 0): ?> <br/><span class="NumberDataRed"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->refunds->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
</span><?php endif; ?>
                      <?php if ($this->_tpl_vars['transactions']->chargebacks->commission->pending != 0): ?> <br/><span class="NumberDataOrange"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->chargebacks->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
</span><?php endif; ?></td>
                    <td class="TextAlignRight" valign="top"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->commission->declined)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

                      <?php if ($this->_tpl_vars['transactions']->refunds->commission->declined != 0): ?> <br/><span class="NumberDataRed"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->refunds->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
</span><?php endif; ?>
                      <?php if ($this->_tpl_vars['transactions']->chargebacks->commission->declined != 0): ?> <br/><span class="NumberDataOrange"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->chargebacks->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
</span><?php endif; ?></td>
                    <td class="TextAlignRight" valign="top"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->commission->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

                      <?php if ($this->_tpl_vars['transactions']->refunds->commission->approved != 0): ?> <br/><span class="NumberDataRed"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->refunds->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
</span><?php endif; ?>
                      <?php if ($this->_tpl_vars['transactions']->chargebacks->commission->approved != 0): ?> <br/><span class="NumberDataOrange"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->chargebacks->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
</span><?php endif; ?></td>
                    <td class="TextAlignRight" valign="top"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->commission->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

                      <?php if ($this->_tpl_vars['transactions']->refunds->commission->paid != 0): ?> <br/><span class="NumberDataRed"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->refunds->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
</span><?php endif; ?>
                      <?php if ($this->_tpl_vars['transactions']->chargebacks->commission->paid != 0): ?> <br/><span class="NumberDataOrange"><?php echo ((is_array($_tmp=$this->_tpl_vars['transactions']->chargebacks->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
</span><?php endif; ?></td>
                </tr>
             <?php endif; ?>
        <?php endforeach; endif; unset($_from); ?>
        <tr>
            <td class="TextAlignLeft"><?php echo smarty_function_localize(array('str' => 'Avg. commission per click (all/repeated/unique)'), $this);?>
</td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions pending'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->all; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->pending,'p2' => $this->_tpl_vars['clicks']->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions pending'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks repeated'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->raw; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->pending,'p2' => $this->_tpl_vars['clicks']->count->raw), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions pending'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks unique'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->unique; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->pending,'p2' => $this->_tpl_vars['clicks']->count->unique), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions declined'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->all; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->declined,'p2' => $this->_tpl_vars['clicks']->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions declined'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks repeated'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->raw; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->declined,'p2' => $this->_tpl_vars['clicks']->count->raw), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions declined'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks unique'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->unique; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->declined,'p2' => $this->_tpl_vars['clicks']->count->unique), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions approved'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->all; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->approved,'p2' => $this->_tpl_vars['clicks']->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions approved'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks repeated'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->raw; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->approved,'p2' => $this->_tpl_vars['clicks']->count->raw), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions approved'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks unique'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->unique; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->approved,'p2' => $this->_tpl_vars['clicks']->count->unique), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions paid'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->all; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->paid,'p2' => $this->_tpl_vars['clicks']->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions paid'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks repeated'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->raw; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->paid,'p2' => $this->_tpl_vars['clicks']->count->raw), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions paid'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'clicks unique'), $this);?>
 (<?php echo $this->_tpl_vars['clicks']->count->unique; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->paid,'p2' => $this->_tpl_vars['clicks']->count->unique), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
        </tr>
        <tr>
            <td class="TextAlignLeft"><?php echo smarty_function_localize(array('str' => 'Avg. commission per impression (all/repeated/unique)'), $this);?>
</td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions pending'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->all; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->pending,'p2' => $this->_tpl_vars['impressions']->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions pending'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions repeated'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->raw; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->pending,'p2' => $this->_tpl_vars['impressions']->count->raw), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions pending'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions unique'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->unique; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->pending,'p2' => $this->_tpl_vars['impressions']->count->unique), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions declined'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->all; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->declined,'p2' => $this->_tpl_vars['impressions']->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions declined'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions repeated'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->raw; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->declined,'p2' => $this->_tpl_vars['impressions']->count->raw), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions declined'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions unique'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->unique; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->declined,'p2' => $this->_tpl_vars['impressions']->count->unique), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions approved'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->all; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->approved,'p2' => $this->_tpl_vars['impressions']->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions approved'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions repeated'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->raw; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->approved,'p2' => $this->_tpl_vars['impressions']->count->raw), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions approved'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions unique'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->unique; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->approved,'p2' => $this->_tpl_vars['impressions']->count->unique), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions paid'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->all; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->paid,'p2' => $this->_tpl_vars['impressions']->count->all), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions paid'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions repeated'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->raw; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->paid,'p2' => $this->_tpl_vars['impressions']->count->raw), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span>
                                /<span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions paid'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'impressions unique'), $this);?>
 (<?php echo $this->_tpl_vars['impressions']->count->unique; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->paid,'p2' => $this->_tpl_vars['impressions']->count->unique), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
        </tr>
        <tr>
            <td class="TextAlignLeft"><?php echo smarty_function_localize(array('str' => 'Avg. commission per sale'), $this);?>
</td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions pending'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->pending)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'sales pending'), $this);?>
 (<?php echo $this->_tpl_vars['salesTier']->count->pending; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->pending,'p2' => $this->_tpl_vars['salesTier']->count->pending), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions declined'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->declined)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'sales declined'), $this);?>
 (<?php echo $this->_tpl_vars['salesTier']->count->declined; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->declined,'p2' => $this->_tpl_vars['salesTier']->count->declined), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions approved'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->approved)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'sales approved'), $this);?>
 (<?php echo $this->_tpl_vars['salesTier']->count->approved; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->approved,'p2' => $this->_tpl_vars['salesTier']->count->approved), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
            <td class="TextAlignRight"><span class="NumberData" title="<?php echo smarty_function_localize(array('str' => 'commissions paid'), $this);?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['sumTransaction']->commission->paid)) ? $this->_run_mod_handler('currency', true, $_tmp) : smarty_modifier_currency($_tmp)); ?>
) : <?php echo smarty_function_localize(array('str' => 'sales paid'), $this);?>
 (<?php echo $this->_tpl_vars['salesTier']->count->paid; ?>
)"><?php echo ((is_array($_tmp=smarty_function_ratio(array('p1' => $this->_tpl_vars['sumTransaction']->commission->paid,'p2' => $this->_tpl_vars['salesTier']->count->paid), $this))) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp));?>
</span></td>
        </tr>
    </table>
    <br>
    <br>
    * <span class="NumberDataRed"><?php echo smarty_function_localize(array('str' => 'Refunds are marked with red color'), $this);?>
</span><br>
    ** <span class="NumberDataOrange"><?php echo smarty_function_localize(array('str' => 'Chargebacks are marked with orange color'), $this);?>
</span>  
</div>