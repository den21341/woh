<?php /* Smarty version 2.6.18, created on 2016-09-30 01:48:42
         compiled from home_panel_content.stpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'home_panel_content.stpl', 4, false),array('modifier', 'currency_span', 'home_panel_content.stpl', 5, false),array('modifier', 'number_span', 'home_panel_content.stpl', 47, false),)), $this); ?>
<!-- home_panel_content -->

<div class="DashStatsBlock DashLargeBlock Sales">
	<h3><?php echo smarty_function_localize(array('str' => 'Total cost of Sales'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(This month)'), $this);?>
</span></h3>
	<div class="DashLargeBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthSales']->totalCost->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<span class="DashStatsLabel"><?php echo $this->_tpl_vars['thisMonthSales']->count->approved; ?>
 <?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
	<div class="DashLargeBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthSales']->totalCost->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo $this->_tpl_vars['thisMonthSales']->count->paid; ?>
 <?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthSales']->totalCost->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo $this->_tpl_vars['thisMonthSales']->count->pending; ?>
 <?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
		</div>
	<?php echo "<div id=\"commissionsChart\"></div>"; ?>
	</div>
</div>

<?php echo "<div id=\"newsContent\"></div>"; ?>

<?php if ($this->_tpl_vars['pendingTasks'] != false): ?>
<div class="DashStatsBlock DashSmallBlock PendingTasks">
	<h3><?php echo smarty_function_localize(array('str' => 'Pending Tasks'), $this);?>
</h3>
	<div class="DashSmallBlockBottom">
		<div class="DashSubstatsBlock">
			<span class="NumberData"><?php echo $this->_tpl_vars['pendingTasks']['affiliates']; ?>
</span>
			<a href="#Affiliate-Manager" class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Affiliates'), $this);?>
</a>			
		</div>
		<div class="DashSubstatsBlock">
			<span class="NumberData"><?php echo $this->_tpl_vars['pendingTasks']['commissions']; ?>
</span>
			<a href="#Transaction-Manager" class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Commissions'), $this);?>
</a>			
		</div>
		<div class="DashSubstatsBlock">
			<span class="NumberData"><?php echo $this->_tpl_vars['pendingTasks']['links']; ?>
</span>
			<a href="#Direct-Links-Manager" class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'DirecLink Urls'), $this);?>
</a>			
		</div>
		<div class="DashSubstatsBlock">
			<span class="NumberData"><?php echo $this->_tpl_vars['pendingTasks']['emails']; ?>
</span>
			<a href="#Mail-Outbox" class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Unsent emails'), $this);?>
</a>			
		</div>
	</div>
</div>
<?php endif; ?>

<div class="DashStatsBlock DashSmallBlock">
	<h3><?php echo smarty_function_localize(array('str' => 'Clicks'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
	<div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysClicks']->count->all)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Total'), $this);?>
</span></div>
	<div class="DashSmallBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysClicks']->count->unique)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Unique'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysClicks']->count->raw)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Repeated'), $this);?>
</span>
		</div>
	</div>
</div>
<div class="DashStatsBlock DashSmallBlock">
	<h3><?php echo smarty_function_localize(array('str' => 'Impressions'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
	<div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysImpressions']->count->all)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Total'), $this);?>
</span></div>
	<div class="DashSmallBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysImpressions']->count->unique)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Unique'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysImpressions']->count->raw)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Repeated'), $this);?>
</span>
		</div>
	</div>
</div>
<div class="DashStatsBlock DashSmallBlock">
	<h3><?php echo smarty_function_localize(array('str' => 'Commissions'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
	<div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysCommissions']->commission->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
	<div class="DashSmallBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysCommissions']->commission->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysCommissions']->commission->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
		</div>
	</div>
</div>
<div class="DashStatsBlock DashSmallBlock">
	<h3><?php echo smarty_function_localize(array('str' => 'All Refunds'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
	<div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysRefunds']->commission->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
	<div class="DashSmallBlockBottom">
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysRefunds']->commission->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
		</div>
		<div class="DashSubstatsBlock">
			<?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysRefunds']->commission->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

			<span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
		</div>
	</div>
</div>
<div class="DashStatsBlock DashSmallBlock">
    <h3><?php echo smarty_function_localize(array('str' => 'All Chargebacks'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(30 days)'), $this);?>
</span></h3>
    <div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysChargebacks']->commission->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
    <div class="DashSmallBlockBottom">
        <div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysChargebacks']->commission->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

            <span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
        </div>
        <div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['last30DaysChargebacks']->commission->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

            <span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
        </div>
    </div>
</div>
<?php if ($this->_tpl_vars['actionCommissionsEnabled'] == 'Y'): ?>
<div class="DashStatsBlock DashSmallBlock">
    <h3><?php echo smarty_function_localize(array('str' => 'Number of Actions'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(this month)'), $this);?>
</span></h3>
    <div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthActionCommissions']->count->approved)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
    <div class="DashSmallBlockBottom">
        <div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthActionCommissions']->count->paid)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

            <span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
        </div>
        <div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthActionCommissions']->count->pending)) ? $this->_run_mod_handler('number_span', true, $_tmp) : smarty_modifier_number_span($_tmp)); ?>

            <span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
        </div>
    </div>
</div>
<div class="DashStatsBlock DashSmallBlock">
    <h3><?php echo smarty_function_localize(array('str' => 'Total cost of Actions'), $this);?>
 <span><?php echo smarty_function_localize(array('str' => '(this month)'), $this);?>
</span></h3>
    <div class="DashSmallBlockTop"><?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthActionCommissions']->totalCost->approved)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<span><?php echo smarty_function_localize(array('str' => 'Approved'), $this);?>
</span></div>
    <div class="DashSmallBlockBottom">
        <div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthActionCommissions']->totalCost->paid)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

            <span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Paid'), $this);?>
</span>
        </div>
        <div class="DashSubstatsBlock">
            <?php echo ((is_array($_tmp=$this->_tpl_vars['thisMonthActionCommissions']->totalCost->pending)) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>

            <span class="DashStatsLabel"><?php echo smarty_function_localize(array('str' => 'Pending'), $this);?>
</span>
        </div>
    </div>
</div>
<?php endif; ?>