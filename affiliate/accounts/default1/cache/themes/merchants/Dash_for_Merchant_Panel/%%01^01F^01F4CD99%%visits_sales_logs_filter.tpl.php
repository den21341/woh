<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from visits_sales_logs_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'visits_sales_logs_filter.tpl', 5, false),)), $this); ?>
<!-- visits_sales_logs_filter -->

<div class="SaleVisitLogFilter">
    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Date of visit'), $this);?>
</div>
            <?php echo "<div id=\"datevisit\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Track method'), $this);?>
</div>
            <?php echo "<div id=\"trackmethod\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Status'), $this);?>
</div>
            <?php echo "<div id=\"rstatus\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Custom'), $this);?>
</div>
            <?php echo "<div id=\"custom\"></div>"; ?>
        <div class="clear"></div>
    </div>
</div>