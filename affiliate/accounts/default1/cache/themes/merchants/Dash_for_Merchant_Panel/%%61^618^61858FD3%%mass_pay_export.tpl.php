<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:49
         compiled from mass_pay_export.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'mass_pay_export.tpl', 5, false),)), $this); ?>
<!-- mass_pay_export -->

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'MassPay export files'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'Here you can download export files for all payouts grouped by payout option.'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"filesList\" class=\"ExportFilesList\"></div>"; ?>
</div>

<?php echo "<div id=\"sendButton\"></div>"; ?>
<div class="clear"></div>