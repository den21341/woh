<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from recurring_commissions.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'recurring_commissions.tpl', 4, false),)), $this); ?>
<!-- recurring_commissions -->
<div class="FormFieldset">
    <div class="FormFieldsetHeader">
        <div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Recurring Commissions'), $this);?>
</div>
        <div class="FormFieldsetHeaderDescription"></div>
    </div>
<?php echo "<div id=\"RecurrenceType\" class=\"RecurrenceType\"></div>"; ?>
<?php echo "<div id=\"recurrenceAfterDays\"></div>"; ?>
<?php echo "<div id=\"numberOfRecurrence\"></div>"; ?>
<?php echo "<div id=\"RecurringCommissionsPanel\"></div>"; ?>
</div>