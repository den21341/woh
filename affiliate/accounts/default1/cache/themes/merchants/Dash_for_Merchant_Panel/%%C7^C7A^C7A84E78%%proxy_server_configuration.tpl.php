<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from proxy_server_configuration.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'proxy_server_configuration.tpl', 4, false),)), $this); ?>
<!-- proxy_server_configuration -->
<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'ProxyServer'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <?php echo "<div id=\"server\"></div>"; ?>
  <?php echo "<div id=\"port\"></div>"; ?>
  <?php echo "<div id=\"user\"></div>"; ?>
  <?php echo "<div id=\"password\"></div>"; ?>
  <?php echo "<div id=\"FormMessage\"></div>"; ?>
</div>
<div class="pad_left">
<?php echo "<div id=\"saveButton\"></div>"; ?>
</div>
<div class="clear"></div>