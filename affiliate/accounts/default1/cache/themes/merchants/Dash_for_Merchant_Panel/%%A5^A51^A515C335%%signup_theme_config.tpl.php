<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from signup_theme_config.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'signup_theme_config.tpl', 4, false),)), $this); ?>
<!-- signup_theme_config -->

<div class="FormFieldset TabDescription">
  <?php echo smarty_function_localize(array('str' => '<h3>Signup Form design</h3>
  The design can be divided into several parts. The form itself is displayed using template. Look of the whole page is controlled by theme and it\'s templates.'), $this);?>

</div>

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Signup form template'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'The template of your Signup Form defines the form, sections and fields.'), $this);?>
 </div>
	</div>
	<?php echo "<div id=\"EditTemplateButton\"></div>"; ?>
</div>

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Other available themes'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'You can choose from the themes below. Click on <strong>Select this theme</strong> to set it as a new default theme.'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"otherThemes\"></div>"; ?>
</div>