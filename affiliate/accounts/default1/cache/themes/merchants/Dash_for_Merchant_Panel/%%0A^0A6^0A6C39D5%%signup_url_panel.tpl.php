<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from signup_url_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'signup_url_panel.tpl', 3, false),)), $this); ?>
<!--	signup_url_panel	-->
<div class="Dash_GeneralAffiliateLink">
	<div class="FloatLeft First"><?php echo smarty_function_localize(array('str' => 'URL to Signup Form:'), $this);?>
</div>
	<div class="FloatLeft"><?php echo "<div id=\"SignupFormUrl\"></div>"; ?></div>
	<div class="clear"></div>
</div>