<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from upload_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'upload_panel.tpl', 4, false),)), $this); ?>
<!-- upload_panel -->
<div class="FormFieldset Recipients">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Attachments'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <?php echo "<div id=\"uploadedFiles\"></div>"; ?>
</div>