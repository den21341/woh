<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from transactions_import.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'transactions_import.tpl', 5, false),)), $this); ?>
<!-- transactions_import -->
<div class="TransactionImport">
  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Import file format'), $this);?>
</div>
  	</div>
  	<?php echo "<div id=\"fields\"></div>"; ?>
    <?php echo "<div id=\"addButton\"></div>"; ?>
  </div>

  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Import file'), $this);?>
</div>
  	</div>
    <?php echo "<div id=\"delimiter\"></div>"; ?>
    <?php echo "<div id=\"source\" class=\"ImportRadioGroup\"></div>"; ?>
    <?php echo "<div id=\"url\"></div>"; ?>
    <?php echo "<div id=\"uploadFile\"></div>"; ?>
    <?php echo "<div id=\"exportFilesGrid\"></div>"; ?> 
    <?php echo "<div id=\"serverFile\"></div>"; ?>
    <?php echo "<div id=\"skipFirstRow\"></div>"; ?>
    <?php echo "<div id=\"transactionType\"></div>"; ?>
  </div>

  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Transaction import settings'), $this);?>
</div>
  	</div>
    <?php echo "<div id=\"computeAtomaticaly\"></div>"; ?>
    <?php echo "<div id=\"matchTransaction\"></div>"; ?>
    <?php echo "<div id=\"matchTransactionStatus\"></div>"; ?>
    <?php echo "<div id=\"transactionStatus\"></div>"; ?>
  </div>
  
    <?php echo "<div id=\"importButton\"></div>"; ?>
</div>