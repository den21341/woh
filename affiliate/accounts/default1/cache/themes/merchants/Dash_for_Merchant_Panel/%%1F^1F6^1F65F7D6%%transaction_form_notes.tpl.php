<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from transaction_form_notes.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'transaction_form_notes.tpl', 5, false),)), $this); ?>
<!-- transaction_form_notes -->

<div class="FormFieldset Note">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'System note'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'Visible only to merchant'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"systemnote\" class=\"Note\"></div>"; ?>
</div>

<div class="FormFieldset Note">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Merchant note'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'Visible also to affiliate'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"merchantnote\" class=\"Note\"></div>"; ?>
</div>