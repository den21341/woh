<?php /* Smarty version 2.6.18, created on 2016-10-11 05:05:25
         compiled from text://0d5a829bb3564e347916a0fabf0cdd81 */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'text://0d5a829bb3564e347916a0fabf0cdd81', 2, false),array('modifier', 'currency_span', 'text://0d5a829bb3564e347916a0fabf0cdd81', 13, false),)), $this); ?>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<b>Invoice Number:</b> <?php echo ((is_array($_tmp=$this->_tpl_vars['invoicenumber'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<br/>
<b>Invoice date:</b> <?php echo $this->_tpl_vars['date']; ?>
<br/>
<br/>
<b>Affiliate Details:</b><br/>
<?php echo ((is_array($_tmp=$this->_tpl_vars['firstname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['lastname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 (<?php echo ((is_array($_tmp=$this->_tpl_vars['username'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
)<br/>
<?php echo ((is_array($_tmp=$this->_tpl_vars['data2'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<br/>
<?php echo ((is_array($_tmp=$this->_tpl_vars['data3'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<br/>
<?php echo ((is_array($_tmp=$this->_tpl_vars['data7'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['data4'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<br/>
<?php echo ((is_array($_tmp=$this->_tpl_vars['data5'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['data6'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<br/>
<br/>
<b>Payment Details:</b> Affiliate commissions<br/>
Amount: <?php echo ((is_array($_tmp=$this->_tpl_vars['payment'])) ? $this->_run_mod_handler('currency_span', true, $_tmp) : smarty_modifier_currency_span($_tmp)); ?>
<br/>
<br/>
<b>Note:</b><br/>
<?php echo ((is_array($_tmp=$this->_tpl_vars['affiliate_note'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
<br/>