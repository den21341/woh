<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from pay_affiliates_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'pay_affiliates_filter.tpl', 4, false),)), $this); ?>
<!-- pay_affiliates_filter -->
<div class="PayAffiliatesFilter">
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Show affiliates'), $this);?>
</div> 
				<?php echo "<div id=\"reachedMinPayout\"></div>"; ?>
		<div class="clear"></div>
	</div>

	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Date created'), $this);?>
</div> 
				<?php echo "<div id=\"dateinserted\"></div>"; ?>
		<div class="clear"></div>
	</div>
				
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Affiliate'), $this);?>
</div> 
                <?php echo "<div id=\"rstatus\"></div>"; ?>
		<div class="clear"></div>
	</div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Campaign'), $this);?>
</div> 
                <?php echo "<div id=\"campaignid\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Statistics payout status'), $this);?>
</div>
        <?php echo "<div id=\"payoutstatus\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Transaction data'), $this);?>
</div>
                <?php echo "<div id=\"transactionData\"></div>"; ?>
        <div class="clear"></div>
    </div>
</div>

<div style="clear: both;"></div>
   