<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from view_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'view_panel.tpl', 4, false),)), $this); ?>
<!-- view_panel -->
<div class="FormFieldset MandatoryFields">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Choose view'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
    <?php echo "<div id=\"viewType\"></div>"; ?>
    <br />
    <?php echo "<div id=\"EditView\"></div>"; ?>
</div>

<div class="ButtonSet"><?php echo "<div id=\"CancelButton\"></div>"; ?></div>