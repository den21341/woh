<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from multiple_currencies_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'multiple_currencies_panel.tpl', 5, false),)), $this); ?>
<!--    multiple_currencies_panel   -->

<div class="FormFieldset DefaultCurrency">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Multiple currencies'), $this);?>
</div>
	</div>

    <?php echo "<div id=\"grid\"></div>"; ?>
    <?php echo "<div id=\"computeCurrencies\"></div>"; ?>
</div>