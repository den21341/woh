<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from payouts_to_affiliates_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'payouts_to_affiliates_filter.tpl', 5, false),)), $this); ?>
<!-- payouts_to_affiliates_filter -->

<div class="PayoutsByAffiliatesFilter">    
	<div class="FilterRow">       
		<div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Payment date'), $this);?>
</div>
		<?php echo "<div id=\"dateinserted\"></div>"; ?>
		<div class="clear"></div>
	</div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Custom'), $this);?>
</div>
        <?php echo "<div id=\"customData\"></div>"; ?>
        <div class="clear"></div>
    </div>
</div>

<div style="clear: both;"></div>