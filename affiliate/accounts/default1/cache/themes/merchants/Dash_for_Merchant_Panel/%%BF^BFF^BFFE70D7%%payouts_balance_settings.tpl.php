<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from payouts_balance_settings.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'payouts_balance_settings.tpl', 4, false),)), $this); ?>
<!-- payouts_balance_settings -->
<div class="FormFieldset PayoutsBalanceForm">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Minimum payout balances'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'Affiliates will be able to choose only from these payout balance options.'), $this);?>
</div>
	</div>
	<?php echo "<div id=\"payoutOptions\"></div>"; ?>
  <?php echo "<div id=\"minimumPayout\"></div>"; ?>
</div>
<div class="pad_left">
<?php echo "<div id=\"FormMessage\"></div>"; ?>
<?php echo "<div id=\"SaveButton\"></div>"; ?>
</div>
<div class="clear"></div>