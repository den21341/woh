<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from performance_rewards.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'performance_rewards.tpl', 3, false),)), $this); ?>
<!--    performance_rewards     -->
<div class="ScreenHeader">
<?php echo smarty_function_localize(array('str' => 'Performance rewards is a powerful feature that allows you to reward your affiliates for their performance.'), $this);?>

</div>
<div class="FormFieldset">
    <?php echo "<div id=\"filter\"></div>"; ?>
    <?php echo "<div id=\"grid\"></div>"; ?>
</div>