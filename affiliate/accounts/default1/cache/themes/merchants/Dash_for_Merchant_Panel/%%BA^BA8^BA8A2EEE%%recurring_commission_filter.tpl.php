<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from recurring_commission_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'recurring_commission_filter.tpl', 4, false),)), $this); ?>
<!-- recurring_commission_filter -->

<div class="FilterRow">
    <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Date created'), $this);?>
</div>
   <?php echo "<div id=\"datecreated\"></div>"; ?>
   <div class="clear"></div>
</div>
<div class="FilterRow">
    <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Last commission date'), $this);?>
</div>
   <?php echo "<div id=\"datelastcommission\"></div>"; ?>
   <div class="clear"></div>
</div>
<div class="FilterRow">
    <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Status'), $this);?>
</div>
   <?php echo "<div id=\"rstatus\"></div>"; ?>
   <div class="clear"></div>
</div>

<div class="FilterRow">
    <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Recurrence'), $this);?>
</div>
   <?php echo "<div id=\"recurrencepresetid\"></div>"; ?>
   <div class="clear"></div>
</div>

<div class="FilterRow">
    <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Custom'), $this);?>
</div>
   <?php echo "<div id=\"custom\"></div>"; ?>
   <div class="clear"></div>
</div>
                