<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from top_affiliates_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'top_affiliates_filter.tpl', 4, false),)), $this); ?>
<!--    top_affiliates_filter    -->
<div>
    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Affiliate status'), $this);?>
</div>
        <?php echo "<div id=\"rstatus\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Statistics date range'), $this);?>
</div>
        <?php echo "<div id=\"statsdaterange\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Campaign'), $this);?>
</div>
        <?php echo "<div id=\"campaignid\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Transaction status'), $this);?>
</div>
        <?php echo "<div id=\"transactionstatus\"></div>"; ?>
        <div class="clear"></div>
    </div>

    <div class="FilterRow">
        <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Custom filter'), $this);?>
</div>
        <?php echo "<div id=\"custom\"></div>"; ?>
        <div class="clear"></div>
    </div>
</div>
<div style="clear: both;"></div>