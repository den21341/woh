<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from recaptcha_settings.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'recaptcha_settings.tpl', 4, false),)), $this); ?>
<!--    recaptcha_settings     -->
<div class="FormFieldset AffiliateSignupRecaptchaSettings">
    <div class="FormFieldsetHeader">
        <div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Signup settings'), $this);?>
</div>
    </div>
    <?php echo "<div id=\"affiliate_signup_recaptcha\"></div>"; ?>
    <?php echo "<div id=\"account_signup_recaptcha\"></div>"; ?>
</div>

<div class="FormFieldset RequestNewPasswordRecaptchaSettings">
    <div class="FormFieldsetHeader">
        <div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Request new password settings'), $this);?>
</div>
    </div>
    <?php echo "<div id=\"request_new_password_recaptcha\"></div>"; ?>
</div>
<?php echo "<div id=\"google_recaptcha_settings\"></div>"; ?>

<div class="margin_left">
    <?php echo "<div id=\"FormMessage\"></div>"; ?>
    <?php echo "<div id=\"save_button\"></div>"; ?>
</div>