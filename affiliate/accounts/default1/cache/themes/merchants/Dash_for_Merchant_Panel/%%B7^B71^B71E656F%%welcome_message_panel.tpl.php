<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from welcome_message_panel.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'welcome_message_panel.tpl', 4, false),)), $this); ?>
<!--    welcome_message_panel   -->
<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Welcome message'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
    <?php echo "<div id=\"variableValues\"></div>"; ?>
  <?php echo "<div id=\"WelcomeMessage\" class=\"WelcomeMessage\"></div>"; ?>
</div>