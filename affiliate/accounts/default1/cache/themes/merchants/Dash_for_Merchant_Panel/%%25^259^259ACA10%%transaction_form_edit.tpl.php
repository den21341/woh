<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from transaction_form_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'transaction_form_edit.tpl', 4, false),)), $this); ?>
<!-- transaction_form_edit -->

<div class="MandatoryFields pad_bottom">
  <div class="FormFieldsetSectionTitle"><?php echo smarty_function_localize(array('str' => 'Mandatory fields'), $this);?>
</div>
  <?php echo "<div id=\"transid\"></div>"; ?>
  <?php echo "<div id=\"userid\"></div>"; ?>
  <?php echo "<div id=\"campaignid\"></div>"; ?>
  <?php echo "<div id=\"dateinserted\"></div>"; ?>
  <?php echo "<div id=\"rtype\"></div>"; ?>
  <?php echo "<div id=\"rstatus\" class=\"StatusListBox\"></div>"; ?>
  <?php echo "<div id=\"commtypeid\"></div>"; ?>
  <?php echo "<div id=\"bannerid\"></div>"; ?>
  <?php echo "<div id=\"channel\"></div>"; ?>
  <?php echo "<div id=\"totalcost\" class=\"MFields\"></div>"; ?>
  <?php echo "<div id=\"fixedcost\" class=\"MFields\"></div>"; ?>
  <?php echo "<div id=\"commission\" class=\"MFields2\"></div>"; ?>
  <?php echo "<div id=\"commissionTag\"></div>"; ?> 
  <?php echo "<div id=\"multiTier\"></div>"; ?>
  <?php echo "<div id=\"ComputeCommissionsButton\"></div>"; ?>
</div>

<div class="OptionalFields pad_bottom">
  <div class="FormFieldsetSectionTitle"><?php echo smarty_function_localize(array('str' => 'Optional fields'), $this);?>
</div>
  <?php echo "<div id=\"orderid\" class=\"OFields\"></div>"; ?>
  <?php echo "<div id=\"productid\" class=\"OFields\"></div>"; ?>
  <?php echo "<div id=\"payoutstatus\"></div>"; ?>
  <?php echo "<div id=\"countrycode\"></div>"; ?>
</div>

<div class="OptionalFieldsRight pad_bottom">
  <div class="FormFieldsetSectionTitle"><?php echo smarty_function_localize(array('str' => 'Optional fields'), $this);?>
</div>
  <?php echo "<div id=\"data1\"></div>"; ?>
  <?php echo "<div id=\"data2\"></div>"; ?>
  <?php echo "<div id=\"data3\"></div>"; ?>
  <?php echo "<div id=\"data4\"></div>"; ?>
  <?php echo "<div id=\"data5\"></div>"; ?>
</div>