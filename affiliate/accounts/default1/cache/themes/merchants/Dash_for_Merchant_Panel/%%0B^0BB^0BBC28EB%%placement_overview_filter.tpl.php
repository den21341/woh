<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from placement_overview_filter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'placement_overview_filter.tpl', 4, false),)), $this); ?>
<!--	placement_overview_filter		-->

<div class="FilterRow">
    <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Reached/didn\'t reach conditions'), $this);?>
</div>
   <?php echo "<div id=\"reachedCondition\"></div>"; ?>
   <div class="clear"></div>
</div>

<div class="FilterRow">
    <div class="FilterLegend"><?php echo smarty_function_localize(array('str' => 'Order ID'), $this);?>
</div>
    <?php echo "<div id=\"orderid\"></div>"; ?>
    <?php echo smarty_function_localize(array('str' => 'You can input multiple order IDs separated either by new line or comma, use \'%\' for like search'), $this);?>

    <div class="clear"></div>
</div>