<?php /* Smarty version 2.6.18, created on 2016-09-30 01:47:01
         compiled from vat_settings.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'vat_settings.tpl', 5, false),)), $this); ?>
<!-- vat_settings -->

<div class="FormFieldset VatForm">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'VAT settings'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
  <?php echo "<div id=\"support_vat\" class=\"VatSettingsForm\"></div>"; ?>
  <?php echo "<div id=\"vat_percentage\"></div>"; ?>
  <?php echo "<div id=\"vat_computation\"></div>"; ?>
</div>

<div class="FormFieldset">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Payout invoice - VAT version'), $this);?>
</div>
		<div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'HTML format of the invoice for users with VAT applicable.'), $this);?>

<?php echo smarty_function_localize(array('str' => 'You can use Smarty syntax in this template and the constants from the list below.'), $this);?>
</div>
	</div>
  <div class="InvoiceEditor">
  <?php echo "<div id=\"payout_invoice_with_vat\"></div>"; ?>
  </div>
  <div class="FormFieldLabel"><div class="Inliner"><?php echo smarty_function_localize(array('str' => 'Payout preview'), $this);?>
</div></div>
  <div class="FormFieldInputContainer">
      <div class="FormFieldInput"><?php echo "<div id=\"userid\"></div>"; ?></div>
      <div class="FormFieldHelp"><?php echo "<div id=\"previewInvoiceHelp\"></div>"; ?></div>
      <div><?php echo "<div id=\"previewInvoiceWithVat\"></div>"; ?></div>
      <?php echo "<div id=\"formPanel\"></div>"; ?>
      <div class="FormFieldDescription">
          <?php echo smarty_function_localize(array('str' => 'By clicking Preview invoice you can see how the invoice will look like for the specified affiliate.'), $this);?>

      </div>
  </div>
  <div class="clear"/></div>  
</div>
<div class="pad_left pad_top">
<?php echo "<div id=\"FormMessage\"></div>"; ?>
<?php echo "<div id=\"SaveButton\"></div>"; ?>
</div>
<div class="clear"></div>