<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:50
         compiled from payouts_balance_form.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'payouts_balance_form.tpl', 4, false),)), $this); ?>
<!-- payouts_balance_form -->
<div class="FormFieldset PayoutsBalanceForm">
    <div class="FormFieldsetHeader">
        <div class="FormFieldsetHeaderDescription"><?php echo smarty_function_localize(array('str' => 'Apply Default payout balance for existing affiliates:'), $this);?>
</div>
    </div>
    <?php echo "<div id=\"applyToAffiliates\"></div>"; ?>
</div>
<div class="pad_left">
    <?php echo "<div id=\"submitButton\"></div>"; ?>
    <?php echo "<div id=\"cancelButton\"></div>"; ?>
</div>

<div class="clear"></div>