<?php /* Smarty version 2.6.18, created on 2016-09-30 01:46:17
         compiled from affiliates_import.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'affiliates_import.tpl', 3, false),)), $this); ?>
<!-- affiliates_import -->
<div class="FormFieldset">
  <h3><?php echo smarty_function_localize(array('str' => 'Available fields for affiliate data'), $this);?>
</h3>
  <?php echo smarty_function_localize(array('str' => 'Choose which fields you want to use to store data for your affiliates. Some fields are mandatory, and you have up to 25 optional fields for which you can decide what information they will keep, if they will be optional, mandatory, or not displayed at all. These fields will be displayed in affiliate signup form and affiliate profile editation.'), $this);?>

</div>

<div class="AffiliateImport">
  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Import file format'), $this);?>
</div>
  	</div>
  	<?php echo "<div id=\"fields\"></div>"; ?>
    <?php echo "<div id=\"addButton\"></div>"; ?>
  </div>

  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Import file'), $this);?>
</div>
  	</div>
    <?php echo "<div id=\"delimiter\"></div>"; ?>
    <?php echo "<div id=\"source\" class=\"ImportRadioGroup\"></div>"; ?>
    <?php echo "<div id=\"url\"></div>"; ?>
    <?php echo "<div id=\"uploadFile\"></div>"; ?>
    <?php echo "<div id=\"exportFilesGrid\"></div>"; ?> 
    <?php echo "<div id=\"serverFile\"></div>"; ?>
    <?php echo "<div id=\"skipFirstRow\"></div>"; ?>
  </div>

  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle"><?php echo smarty_function_localize(array('str' => 'Affiliate import settings'), $this);?>
</div>
  	</div>
    <?php echo "<div id=\"matchAffiliate\"></div>"; ?>
    <?php echo "<div id=\"affiliateStatus\"></div>"; ?>
  </div>
  
    <?php echo "<div id=\"importButton\"></div>"; ?>
</div>