<?php /* Smarty version 2.6.18, created on 2016-09-30 01:45:35
         compiled from text://d87364de980d906ab699c18944581b58 */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'text://d87364de980d906ab699c18944581b58', 2, false),array('modifier', 'escape', 'text://d87364de980d906ab699c18944581b58', 10, false),)), $this); ?>
<font size="2">
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Dear merchant'), $this);?>
,</span><br><br>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Your merchant login username was changed'), $this);?>
</span><br>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'from:'), $this);?>
 </span>
<span style="font-weight: bold; color: rgb(51, 0, 255); font-family: Arial;"><?php echo $this->_tpl_vars['old_username']; ?>
</span><br>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'to:'), $this);?>
 </span>
<span style="font-weight: bold; color: rgb(51, 0, 255); font-family: Arial;"><?php echo $this->_tpl_vars['username']; ?>
</span>
<span style="font-family: Arial;"><br><br></span>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Regards,'), $this);?>
</span><br><br>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Your'), $this);?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['postAffiliatePro'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
.</span>

</font>