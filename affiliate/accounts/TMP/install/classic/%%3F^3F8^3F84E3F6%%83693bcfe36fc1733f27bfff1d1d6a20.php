<?php /* Smarty version 2.6.18, created on 2016-09-30 01:45:35
         compiled from text://83693bcfe36fc1733f27bfff1d1d6a20 */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'localize', 'text://83693bcfe36fc1733f27bfff1d1d6a20', 2, false),array('modifier', 'escape', 'text://83693bcfe36fc1733f27bfff1d1d6a20', 7, false),)), $this); ?>
<font size="2">
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Dear merchant,'), $this);?>
</span><br><br>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'there is new unconfirmed banner:'), $this);?>
 <?php echo $this->_tpl_vars['bannerName']; ?>
 (<?php echo $this->_tpl_vars['bannerId']; ?>
)</span><br><br>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Please check and confirm content of this banner.'), $this);?>
 </span><br/><br/>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Banner was created or modified by:'), $this);?>
 <?php echo $this->_tpl_vars['merchantUsername']; ?>
</span><br/><br/>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Regards'), $this);?>
,</span><br/><br/>
<span style="font-family: Arial;"><?php echo smarty_function_localize(array('str' => 'Your'), $this);?>
 <?php echo ((is_array($_tmp=$this->_tpl_vars['postAffiliatePro'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
.</span>
</font>