<?php
/**
 * Net_Socket2
 *
 * PHP Version 5
 *
 * Copyright (c) 1997-2013 The PHP Group
 *
 * This source file is subject to version 2.0 of the PHP license,
 * that is bundled with this package in the file LICENSE, and is
 * available at through the world-wide-web at
 * http://www.php.net/license/2_02.txt.
 * If you did not receive a copy of the PHP license and are unable to
 * obtain it through the world-wide-web, please send a note to
 * license@php.net so we can mail you a copy immediately.
 *
 * Authors: Stig Bakken <ssb@php.net>
 *          Chuck Hagenbuch <chuck@horde.org>
 *
 * @category  Net
 * @package   Net_Socket2
 * @author    Stig Bakken <ssb@php.net>
 * @author    Chuck Hagenbuch <chuck@horde.org>
 * @copyright 1997-2003 The PHP Group
 * @license   http://www.php.net/license/2_02.txt PHP 2.02
 * @link      http://pear.php.net/packages/Net_Socket2
 */

/**
 * Generalized Socket class.
 *
 * @category  Net
 * @package   Net_Socket2
 * @author    Stig Bakken <ssb@php.net>
 * @author    Chuck Hagenbuch <chuck@horde.org>
 * @copyright 1997-2003 The PHP Group
 * @license   http://www.php.net/license/2_02.txt PHP 2.02
 * @link      http://pear.php.net/packages/Net_Socket2
 */
class Net_Socket {
    
    const READ = 1;
    const WRITE = 2;
    const ERROR = 4;
    
    /**
     * Socket file pointer.
     * @var resource $fp
     */
    public $fp = null;

    /**
     * Whether the socket is blocking. Defaults to true.
     * @var boolean $blocking
     */
    private $blocking = true;

    /**
     * Whether the socket is persistent. Defaults to false.
     * @var boolean $persistent
     */
    private $persistent = false;

    /**
     * The IP address to connect to.
     * @var string $addr
     */
    private $addr = '';

    /**
     * The port number to connect to.
     * @var integer $port
     */
    private $port = 0;

    /**
     * Number of seconds to wait on socket operations before assuming there's no more data. Defaults to no timeout.
     * @var integer|float $timeout
     */
    private $timeout = null;

    /**
     * Number of bytes to read at a time in readLine() and readAll(). Defaults to 2048.
     * @var integer $lineLength
     */
    private $lineLength = 2048;

    /**
     * The string to use as a newline terminator. Usually "\r\n" or "\n".
     * @var string $newline
     */
    private $newline = "\r\n";

    /**
     * Connect to the specified port. If called when the socket is already connected, it disconnects and connects again.
     * @param string $addr IP address or host name (may be with protocol prefix).
     * @param integer $port TCP port number.
     * @param boolean $persistent (optional) Whether the connection is persistent (kept open between requests by the web server).
     * @param integer $timeout (optional) Connection socket timeout.
     * @param array $options (optional) See options for stream_context_create.
     * @throws InvalidArgumentException On empty $addr
     * @throws Gpf_Net_SocketException On connecting error.
     */
    public function connect($addr, $port = 0, $persistent = null, $timeout = null, $options = null) {
        if (is_resource($this->fp)) {
            @fclose($this->fp);
            $this->fp = null;
        }
        if (!$addr) {
            throw new InvalidArgumentException('Server address cannot be empty');
        } else if (strspn($addr, ':.0123456789') == strlen($addr)) {
            $this->addr = strpos($addr, ':') !== false ? '['.$addr.']' : $addr;
        } else {
            $this->addr = $addr;
        }
        $this->port = $port % 65536;
        if ($persistent !== null) {
            $this->persistent = $persistent;
        }
        $openfunc = $this->persistent ? 'pfsockopen' : 'fsockopen';
        $errno = 0;
        $errstr = '';
        $old_track_errors = @ini_set('track_errors', 1);
        if ($timeout <= 0) {
            $timeout = @ini_get('default_socket_timeout');
        }
        if (function_exists('stream_context_create')) {
            // PHP 5.6 introduced breaking change, so this sets thing as they were prior 5.6 release
            $socket_options = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false));
            if(is_array($options)) {
                $socket_options = array_merge($options, $socket_options);
            }
            $context = stream_context_create($socket_options);
            // Since PHP 5 fsockopen doesn't allow context specification
            if (function_exists('stream_socket_client')) {
                $flags = STREAM_CLIENT_CONNECT;
                if ($this->persistent) {
                    $flags = STREAM_CLIENT_PERSISTENT;
                }
                $addr = $this->addr . ':' . $this->port;
                $fp = stream_socket_client($addr, $errno, $errstr, $timeout, $flags, $context);
            } else {
                $fp = @$openfunc($this->addr, $this->port, $errno, $errstr, $timeout, $context);
            }
        } else {
            $fp = @$openfunc($this->addr, $this->port, $errno, $errstr, $timeout);
        }
        if (!$fp) {
            if ($errno == 0 && !strlen($errstr) && isset($php_errormsg)) {
                $errstr = $php_errormsg;
            }
            @ini_set('track_errors', $old_track_errors);
            throw new Gpf_Net_SocketException($errstr, $errno);
        }
        @ini_set('track_errors', $old_track_errors);
        $this->fp = $fp;
        $this->setTimeout();
        $this->setBlocking($this->blocking);
    }

    /**
     * Disconnects from the peer, closes the socket.
     */
    public function disconnect() {
        if (is_resource($this->fp)) {
            @fclose($this->fp);
            $this->fp = null;
        }
    }

    /**
     * Set the newline character/sequence to use.
     * @param string $newline Newline character(s)
     * @return boolean True
     */
    public function setNewline($newline) {
        $this->newline = $newline;
        return true;
    }

    /**
     * Find out if the socket is in blocking mode.
     * @return boolean The current blocking mode.
     */
    public function isBlocking() {
        return $this->blocking;
    }

    /**
     * Sets whether the socket connection should be blocking or
     * not. A read call to a non-blocking socket will return immediately
     * if there is no data available, whereas it will block until there
     * is data for blocking sockets.
     * @param boolean $mode TRUE for blocking sockets, FALSE for nonblocking.
     * @throws Gpf_Net_SocketException If not connected.
     */
    public function setBlocking($mode) {
        $this->checkConnection();
        $this->blocking = $mode;
        stream_set_blocking($this->fp, (int)$this->blocking);
    }

    /**
     * Sets the timeout value on socket descriptor, expressed in the sum of seconds and microseconds.
     * @param integer $seconds Seconds.
     * @param integer $microseconds Microseconds, optional.
     * @return boolean TRUE on success or FALSE on failure.
     * @throws Gpf_Net_SocketException If not connected.
     */
    public function setTimeout($seconds = null, $microseconds = null) {
        $this->checkConnection();
        if ($seconds === null && $microseconds === null) {
            $seconds = (int) $this->timeout;
            $microseconds = (int) (($this->timeout - $seconds) * 1000000);
        } else {
            $this->timeout = $seconds + $microseconds/1000000;
        }
        if ($this->timeout > 0) {
            return stream_set_timeout($this->fp, (int) $seconds, (int) $microseconds);
        } else {
            return false;
        }
    }

    /**
     * Sets the file buffering size on the stream.
     * See php's stream_set_write_buffer for more information.
     * @param integer $size Write buffer size.
     * @return mixed on success or an PEAR_Error object otherwise
     */
    public function setWriteBuffer($size) {
        $this->checkConnection();
        $returned = stream_set_write_buffer($this->fp, $size);
        if ($returned == 0) {
            return true;
        }
        throw new Gpf_Net_SocketException('Cannot set write buffer.');
    }

    /**
     * Returns information about an existing socket resource. Currently returns four entries in the result array:
     * <p>
     * timed_out (bool) - The socket timed out waiting for data<br>
     * blocked (bool) - The socket was blocked<br>
     * eof (bool) - Indicates EOF event<br>
     * unread_bytes (int) - Number of bytes left in the socket buffer<br>
     * </p>
     * @return array Array containing information about existing socket resource.
     * @throws Gpf_Net_SocketException if not connected.
     */
    public function getStatus() {
        $this->checkConnection();
        return stream_get_meta_data($this->fp);
    }

    /**
     * Get a specified line of data
     * @param int $size Reading ends when size - 1 bytes have been read,
     *                  or a newline or an EOF (whichever comes first).
     *                  If no size is specified, it will keep reading from
     *                  the stream until it reaches the end of the line.
     * @return mixed $size bytes of data from the socket. FALSE if error occurs.
     */
    public function gets($size = null) {
        $this->checkConnection();
        if (is_null($size)) {
            return @fgets($this->fp);
        } else {
            return @fgets($this->fp, $size);
        }
    }

    /**
     * Read a specified amount of data. This is guaranteed to return,
     * and has the added benefit of getting everything in one fread()
     * chunk; if you know the size of the data you're getting
     * beforehand, this is definitely the way to go.
     * @param integer $size The number of bytes to read from the socket.
     * @return $size bytes of data from the socket.
     */
    public function read($size) {
        $this->checkConnection();
        return @fread($this->fp, $size);
    }

    /**
     * Write a specified amount of data.
     *
     * @param string  $data Data to write.
     * @param integer $blocksize Amount of data to write at once (NULL means all at once).
     * @return int|bool If the write succeeds, returns the number of bytes written. If the write fails, returns FALSE.
     * @throws Gpf_Net_SocketException If not connected or timed out.
     */
    public function write($data, $blocksize = null) {
        $this->checkConnection();
        if (is_null($blocksize) && !(substr(PHP_OS, 0, 3) == 'WIN')) {
            $written = @fwrite($this->fp, $data);
            // Check for timeout or lost connection
            if ($written === false) {
                $meta_data = $this->getStatus();
                if (!empty($meta_data['timed_out'])) {
                    throw new Gpf_Net_SocketException('Timed out');
                }
            }
            return $written;
        } else {
            if (is_null($blocksize)) {
                $blocksize = 1024;
            }
            $pos = 0;
            $size = strlen($data);
            while ($pos < $size) {
                $written = @fwrite($this->fp, substr($data, $pos, $blocksize));
                // Check for timeout or lost connection
                if ($written === false) {
                    $meta_data = $this->getStatus();
                    if (!empty($meta_data['timed_out'])) {
                        throw new Gpf_Net_SocketException('Timed out');
                    }
                    return $written;
                }
                $pos += $written;
            }
            return $pos;
        }
    }

    /**
     * Write a line of data to the socket, followed by a trailing newline.
     * @param string $data Data to write
     * @return int fwrite() result
     */
    public function writeLine($data) {
        $this->checkConnection();
        return fwrite($this->fp, $data . $this->newline);
    }

    /**
     * Tests for end-of-file on a socket descriptor.
     * Also returns true if the socket is disconnected.
     * @return bool
     */
    public function eof() {
        return (!is_resource($this->fp) || feof($this->fp));
    }

    /**
     * Reads a byte of data
     * @return 1 byte of data from the socket.
     */
    public function readByte() {
        $this->checkConnection();
        return ord(@fread($this->fp, 1));
    }

    /**
     * Reads a word of data
     * @return 1 word of data from the socket.
     */
    public function readWord() {
        $this->checkConnection();
        $buf = @fread($this->fp, 2);
        return (ord($buf[0]) + (ord($buf[1]) << 8));
    }

    /**
     * Reads an int of data
     * @return integer 1 int of data from the socket.
     */
    public function readInt() {
        $this->checkConnection();
        $buf = @fread($this->fp, 4);
        return (ord($buf[0]) + (ord($buf[1]) << 8) + (ord($buf[2]) << 16) + (ord($buf[3]) << 24));
    }

    /**
     * Reads a zero-terminated string of data
     * @return string
     */
    public function readString() {
        $this->checkConnection();
        $string = '';
        while (($char = @fread($this->fp, 1)) != "\x00") {
            $string .= $char;
        }
        return $string;
    }

    /**
     * Reads an IP Address and returns it in a dot formatted string
     * @return Dot formatted string.
     */
    public function readIPAddress() {
        $this->checkConnection();
        $buf = @fread($this->fp, 4);
        return sprintf('%d.%d.%d.%d', ord($buf[0]), ord($buf[1]), ord($buf[2]), ord($buf[3]));
    }

    /**
     * Read until either the end of the socket or a newline, whichever
     * comes first. Strips the trailing newline from the returned data.
     * @return All available data up to a newline, without that
     *         newline, or until the end of the socket.
     */
    public function readLine() {
        $this->checkConnection();
        $line = '';
        $lenNewLine = strlen($this->newline);
        $timeout = time() + $this->timeout;
        while (!feof($this->fp) && (!$this->timeout || time() < $timeout)) {
            $line .= @fgets($this->fp, $this->lineLength);
            if (strlen($line) >= $lenNewLine && substr($line, -$lenNewLine) == $this->newline) {
                return substr($line, 0, -$lenNewLine);
            }
        }
        return $line;
    }

    /**
     * Read until the socket closes, or until there is no more data in
     * the inner PHP buffer. If the inner buffer is empty, in blocking
     * mode we wait for at least 1 byte of data. Therefore, in
     * blocking mode, if there is no data at all to be read, this
     * function will never exit (unless the socket is closed on the
     * remote end).
     * @return string  All data until the socket closes.
     */
    public function readAll() {
        $this->checkConnection();
        $data = '';
        while (!feof($this->fp)) {
            $data .= @fread($this->fp, $this->lineLength);
        }
        return $data;
    }

    /**
     * Runs the equivalent of the select() system call on the socket
     * with a timeout specified by tv_sec and tv_usec.
     * @param integer $state   Which of read/write/error to check for.
     * @param integer $tv_sec  Number of seconds for timeout.
     * @param integer $tv_usec Number of microseconds for timeout.
     * @return False if select fails, integer describing which of read/write/error
     *         are ready.
     */
    public function select($state, $tv_sec, $tv_usec = 0) {
        $this->checkConnection();
        $read = null;
        $write = null;
        $except = null;
        if ($state & self::READ) {
            $read[] = $this->fp;
        }
        if ($state & self::WRITE) {
            $write[] = $this->fp;
        }
        if ($state & self::ERROR) {
            $except[] = $this->fp;
        }
        if (false === ($sr = stream_select($read, $write, $except, $tv_sec, $tv_usec))) {
            return false;
        }
        $result = 0;
        if (count($read)) {
            $result |= self::READ;
        }
        if (count($write)) {
            $result |= self::WRITE;
        }
        if (count($except)) {
            $result |= self::ERROR;
        }
        return $result;
    }

    /**
     * Turns encryption on/off on a connected socket.
     * @param bool $enabled Set this parameter to true to enable encryption and false to disable encryption.
     * @param integer $type Type of encryption. See stream_socket_enable_crypto() for values.
     * @see http://se.php.net/manual/en/function.stream-socket-enable-crypto.php
     * @return FALSE on error, TRUE on success and 0 if there isn't enough data and the user should try again (non-blocking sockets only).
     * @throws Gpf_Net_SocketException If not connected or PHP version requirements not met.
     */
    public function enableCrypto($enabled, $type) {
        if (version_compare(phpversion(), "5.1.0", ">=")) {
            $this->checkConnection();
            return @stream_socket_enable_crypto($this->fp, $enabled, $type);
        } else {
            throw new Gpf_Net_SocketException('Enabling socket encryption requires php version >= 5.1.0');
        }
    }
    
    private function checkConnection() {
        if (!is_resource($this->fp)) {
            throw new Gpf_Net_SocketException('Not connected');
        }
    }
}
