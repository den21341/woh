<?php
/**
 *   @copyright Copyright (c) 2016 Quality Unit s.r.o.
 *   @author Martin Pullmann
 *   @package PostAffiliatePro
 *   @since Version 1.0.0
 *
 *   Licensed under the Quality Unit, s.r.o. Standard End User License Agreement,
 *   Version 1.0 (the "License"); you may not use this file except in compliance
 *   with the License. You may obtain a copy of the License at
 *   http://www.postaffiliatepro.com/licenses/license
 *
 */

/**
 * @package PostAffiliatePro plugins
 */
class PagosOnline_Config extends Gpf_Plugins_Config {
    const CUSTOM_NUMBER = 'PagosOnlineCustomNumber';
    const CUSTOM_SEPARATOR = 'PagosOnlineCustomSeparator';
    const DISCOUNT_TAX = 'PagosOnlineDiscountTax';
    const DISCOUNT_FEE = 'PagosOnlineDiscountFee';

    protected function initFields() {
        $this->addListBox($this->_('Custom field number'), self::CUSTOM_NUMBER, array('1'=>'1','2'=>'2','3'=>'3'), $this->_('The number of custom field that you are using for integration (1, 2 or 3). In case you are already using all custom fields for another purposes, you have to use method with adding custom details to an existing field - you have to define the separator value as well. See PayU (PagosOnline) integration method.'));
        $this->addTextBox($this->_('Custom value separator'), self::CUSTOM_SEPARATOR, $this->_('Custom value separator should be only set in case custom field is already used by another script. See PagosOnline integration method.'));
        $this->addCheckBox($this->_('Discount tax'), self::DISCOUNT_TAX, $this->_('Discounts tax from total cost value.'));
        $this->addCheckBox($this->_('Discount fee'), self::DISCOUNT_FEE, $this->_('Discounts fee from total cost value.'));
    }

    /**
     * @service plugin_config write
     * @param Gpf_Rpc_Params $params
     * @return Gpf_Rpc_Form
     */
    public function save(Gpf_Rpc_Params $params) {
        $form = new Gpf_Rpc_Form($params);
        Gpf_Settings::set(self::CUSTOM_SEPARATOR, $form->getFieldValue(self::CUSTOM_SEPARATOR));
        Gpf_Settings::set(self::DISCOUNT_TAX, $form->getFieldValue(self::DISCOUNT_TAX));
        Gpf_Settings::set(self::DISCOUNT_FEE, $form->getFieldValue(self::DISCOUNT_FEE));

        if (!is_numeric($form->getFieldValue(self::CUSTOM_NUMBER)) || (($form->getFieldValue(self::CUSTOM_NUMBER) > 3) && ($form->getFieldValue(self::CUSTOM_NUMBER) < 0)) ) {
            $customFieldNumberError = $this->_('You have to specify the Custom field number - values 1, 2 or 3 are allowed!');
            $form->setFieldError(self::CUSTOM_NUMBER, $customFieldNumberError);
            $form->setErrorMessage($customFieldNumberError);
            return $form;
        }
        Gpf_Settings::set(self::CUSTOM_NUMBER, $form->getFieldValue(self::CUSTOM_NUMBER));

        $form->setInfoMessage($this->_('PagosOnline settings saved'));
        return $form;
    }

    /**
     * @service plugin_config read
     * @param Gpf_Rpc_Params $params
     * @return Gpf_Rpc_Form
     */
    public function load(Gpf_Rpc_Params $params) {
        $form = new Gpf_Rpc_Form($params);
        $form->addField(self::CUSTOM_NUMBER, Gpf_Settings::get(self::CUSTOM_NUMBER));
        $form->addField(self::CUSTOM_SEPARATOR, Gpf_Settings::get(self::CUSTOM_SEPARATOR));
        $form->addField(self::DISCOUNT_TAX, Gpf_Settings::get(self::DISCOUNT_TAX));
        $form->addField(self::DISCOUNT_FEE, Gpf_Settings::get(self::DISCOUNT_FEE));
        return $form;
    }
}
