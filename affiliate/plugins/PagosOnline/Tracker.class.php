<?php
/**
 *   @copyright Copyright (c) 2016 Quality Unit s.r.o.
 *   @author Martin Pullmann
 *   @package PostAffiliatePro
 *   @since Version 1.0.0
 *
 *   Licensed under the Quality Unit, s.r.o. Standard End User License Agreement,
 *   Version 1.0 (the "License"); you may not use this file except in compliance
 *   with the License. You may obtain a copy of the License at
 *   http://www.postaffiliatepro.com/licenses/license
 *
 */

/**
 * @package PostAffiliatePro plugins
 */
class PagosOnline_Tracker extends Pap_Tracking_CallbackTracker {

    const APPROVED = '4';
    const EXPIRED = '5';
    const DECLINED = '6';

    /**
     * @return PagosOnline_Tracker
     */
    public function getInstance() {
        $tracker = new PagosOnline_Tracker();
        $tracker->setTrackerName('PagosOnline');
        return $tracker;
    }

    public function checkStatus() {
        // check payment status
        if ($this->getPaymentStatus() != self::APPROVED) {
            $this->debug('Payment status is NOT APPROVED. Transaction: ' . $this->getTransactionID() . ', status: ' . $this->getPaymentStatus());
            $this->setStatus('D');
            return false;
        }
        
        $this->setStatus('A');
        return true;
    }

    protected function discountFromTotalcost($totalcost, $value) {
        if (($value != '') && (is_numeric($value))) {
            return $totalcost - $value;
        }
        return $totalcost;
    }

    /**
     *  @return Pap_Tracking_Request
     */
    protected function getRequestObject() {
        $this->debug(' PagosOnline callback variables: ' . print_r($_REQUEST, true));
        return Pap_Contexts_Action::getContextInstance()->getRequestObject();
    }

    public function readRequestVariables() {
        $request = $this->getRequestObject();
        $cookieValue = stripslashes($request->getPostParam('extra' . Gpf_Settings::get(PagosOnline_Config::CUSTOM_NUMBER)));
        if ($request->getRequestParameter('pap_custom') != '') {
            $cookieValue = stripslashes($request->getRequestParameter('pap_custom'));
        }
        try {
            $customSeparator = Gpf_Settings::get(PagosOnline_Config::CUSTOM_SEPARATOR);
            if ($customSeparator != '') {
                $explodedCookieValue = explode($customSeparator, $cookieValue, 2);
                if (count($explodedCookieValue) == 2) {
                    $cookieValue = $explodedCookieValue[1];
                }
            }
        } catch (Gpf_Exception $e) {
            $this->debug(' PagosOnline: error with separator job:' . $e->getMessage());
        }
        $this->setCookie($cookieValue);
        
        // IP
        $this->setIpAddress($request->getRequestParameter('ip'));
        
        $this->setTotalCost($this->adjustTotalCost($request));
        $this->setTransactionID($request->getPostParam('transaction_id'));
        $this->setProductID($request->getPostParam('description'));
        $this->setPaymentStatus($request->getPostParam('state_pol'));
        $this->setCurrency($request->getPostParam('currency'));
        $this->setData1($request->getPostParam('email_buyer'));
    }

    public function getOrderID() {
        return $this->getTransactionID();
    }

    private function adjustTotalCost(Pap_Tracking_Request $request) {
        $totalCost = $request->getPostParam('value');
        $this->debug('Original totalcost: ' . $totalCost);
        if (Gpf_Settings::get(PagosOnline_Config::DISCOUNT_FEE) == Gpf::YES) {
            $fee = $request->getPostParam('administrative_fee') + $request->getPostParam('administrative_fee_base') + $request->getPostParam('administrative_fee_tax');
            $totalCost = $totalCost - $fee;
            $this->debug("Discounting fee ($fee) from totalcost.");
        }
        if (Gpf_Settings::get(PagosOnline_Config::DISCOUNT_TAX) == Gpf::YES) {
            $totalCost = $totalCost - $request->getPostParam('tax');
            $this->debug('Discounting tax (' . $request->getPostParam('tax') . ') from totalcost.');
        }
        
        $this->debug('Totalcost after discounts: ' . $totalCost);
        return $totalCost;
    }
}
