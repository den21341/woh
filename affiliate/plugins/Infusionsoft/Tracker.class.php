<?php
/**
 *   @copyright Copyright (c) 2014 Quality Unit s.r.o.
 *   @author Martin Pullmann
 *   @package PostAffiliatePro
 *   @since Version 1.0.0
 *
 *   Licensed under the Quality Unit, s.r.o. Standard End User License Agreement,
 *   Version 1.0 (the "License"); you may not use this file except in compliance
 *   with the License. You may obtain a copy of the License at
 *   http://www.postaffiliatepro.com/licenses/license
 *
 */

/**
 * @package PostAffiliatePro plugins
 */
class Infusionsoft_Tracker extends Pap_Tracking_CallbackTracker {

    private $xml = '';
    private $xmlerror = '';

    /**
     * @return Infusionsoft_Tracker
     */
    public function getInstance() {
        $tracker = new Infusionsoft_Tracker();
        $tracker->setTrackerName("Infusionsoft");
        return $tracker;
    }

    public function checkStatus() {
        return true;
    }

    private function constructDataXmlQuery($encryptedKey, $tableName, $filterName, $filterValue, $returnFields) {
    	$fields = '';
    	foreach ($returnFields as $returnField) {
    		$fields .= '<value><string>'.$returnField.'</string></value>';
    	}
    	$result = "<?xml version='1.0' encoding='UTF-8'?" . '>
<methodCall>
<methodName>DataService.query</methodName>
<params>
<param>
<value><string>' . $encryptedKey . '</string></value>
</param>
<param>
<value><string>' . $tableName . '</string></value>
</param>
<param>
<value><int>1</int></value>
</param>
<param>
<value><int>0</int></value>
</param>
<param>
<value><struct>
<member><name>' . $filterName . '</name>
<value><string>' . $filterValue . '</string></value>
</member>
</struct></value>
</param>
<param>
<value><array>
<data>' . $fields . '</data>
</array></value>
</param>';
        if ($tableName == 'Invoice') {
            $result .= '<param>
<value><string>Id</string></value>
</param>
<param>
<value><boolean>0</boolean></value>
</param>';
}
        $result .= '</params>
</methodCall>';
    }

    private function parseObjToNiceXml($obj) {
    	$content = $obj->params->param->value->array->data->value->struct;
    	$responseXml = '';
    	if ($content == null || $content->member == null) {
    		$this->debug(' No items available');
    		return '<members></members>';
    	}

    	foreach ($content->member as $item) {
    		$responseXml .= $item->asXML();
    	}
    	$string = strip_tags($responseXml, '<member><name><value>');
    	return '<members>'.$string.'</members>';
    }

    private function sendXML($xml_data, $url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: text/xml'
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);

        if (!$response) {
            $this->xmlerror = $error;
            return false;
        }

        $this->xml = $response;
        return true;
    }

    private function sendXmlRequest($xmlObject) {
    	$requestURL = 'https://' . Gpf_Settings::get(Infusionsoft_Config::SUBDOMAIN) . '.infusionsoft.com/api/xmlrpc';
    	$this->debug(' Sending XML request');
    	if ($this->sendXML($xmlObject, $requestURL)) {
    		$response = new SimpleXMLElement($this->xml);
    	} else {
    		$this->debug(' An error occurred when communicating with the remote server: ' . $this->xmlerror);
    		return false;
    	}
    	if ($response->fault->value != null) {
    		$this->debug(' An error occurred when communicating with the remote server: ' . $response->fault->value->struct->member[1]->value->asXML());
    		return false;
    	}

    	$responseObj = new SimpleXMLElement($this->parseObjToNiceXml($response));
    	$this->debug(' XML response received: '.print_r($responseObj, true));
    	return $responseObj;
    }

    /**
     *  @return Pap_Tracking_Request
     */
    protected function getRequestObject() {
        return Pap_Contexts_Action::getContextInstance()->getRequestObject();
    }

    public function readRequestVariables() {
        $request = $this->getRequestObject();
        $this->debug(' Data received: ' . print_r($request, true));

        $customerId = stripslashes($request->getRequestParameter('custId'));
        $orderId = stripslashes($request->getRequestParameter('orderId'));
        $this->setCookie($request->getRequestParameter('visitorId'));

        $encryptedKey = Gpf_Settings::get(Infusionsoft_Config::API_KEY);
        $subdomain = Gpf_Settings::get(Infusionsoft_Config::SUBDOMAIN);

        if (empty($encryptedKey) || empty($subdomain)) {
            $this->debug(' Some of mandatory API values are missing, please check your plugin settings!');
            return false;
        }

        // recurring
        if ($request->getRequestParameter('Id') != '') { // Id sent from 'Billing automation'
            $customerId = $request->getRequestParameter('Id');
        }

        if (empty($customerId) || $customerId === 'undefined') {
            $this->debug(' Customer ID not in the request, trying to find order ID.');
            if (empty($orderId) || $orderId === 'undefined') {
            	$this->debug(' Order ID not in the request, ending.');
            	return false;
            }
            $customerFromOrderXml = $this->constructDataXmlQuery($encryptedKey, 'Job', 'Id', $orderId, array ('ContactId'));
            $this->debug(' Sending XML request to get ContactId from order ' . $orderId . '.');
            $responseObj = $this->sendXmlRequest($customerFromOrderXml);
            if (!$responseObj) {
            	return false;
            }

            foreach ($responseObj->member as $item) {
            	if ($item->name == 'ContactId') {
            		$customerId = $item->value;
            		break;
            	}
            }
        }

        $invoiceXml = $this->constructDataXmlQuery($encryptedKey, 'Invoice', 'ContactId', $customerId, array ('Id', 'InvoiceTotal', 'ProductSold'));
        $this->debug(' Sending XML request to get order details.');
        $responseObj = $this->sendXmlRequest($invoiceXml);
        if (!$responseObj) {
        	return false;
        }

        foreach ($responseObj->member as $item) {
        	if ($item->name == 'ProductSold') {
        		$this->setProductID((string) $item->value);
        	}
        	if ($item->name == 'InvoiceTotal') {
        		$this->setTotalCost((string) $item->value);
        	}
        	if ($item->name == 'Id') {
        		$this->setTransactionID((string) $item->value);
        	}
        }

        $this->setEmail($request->getRequestParameter('email'));
        $this->setData1($request->getRequestParameter('email'));

        if ($this->isAffiliateRegisterAllowed()) {
	        $contactXml = $this->constructDataXmlQuery($encryptedKey, 'Contact', 'Id', $customerId, array ('FirstName', 'LastName', 'City', 'Email', 'Address2Street1'));

	        $this->debug(' Sending XML request to get contact details for affiliate registration.');
	        $responseObj = $this->sendXMLRequest($contactXml);
	        if (!$responseObj) {
	        	return false;
	        }

	        $this->setUserFirstName('Empty');
	        $this->setUserLastName('Empty');

	        foreach ($responseObj->member as $item) {
	        	if ($item->name == 'FirstName') {
	        		$this->setUserFirstName((string) $item->value);
	        	}
	        	if ($item->name == 'LastName') {
	        		$this->setUserLastName((string) $item->value);
	        	}
	        	if ($item->name == 'Email') {
	        		$this->setUserEmail((string) $item->value);
	        	}
	        	if ($item->name == 'City') {
	        		$this->setUserCity((string) $item->value);
	        	}
	        	if ($item->name == 'Address2Street1') {
	        		$this->setUserAddress((string) $item->value);
	        	}
	        }
        }
    }

    public function getOrderID() {
        return $this->getTransactionID();
    }

    protected function isAffiliateRegisterAllowed() {
    	return (Gpf_Settings::get(Infusionsoft_Config::REGISTER_AFFILIATE) == Gpf::YES);
    }
}
