<!-- account_settings -->

<fieldset>
    <legend>##Pricing options##</legend>
    {widget id="applyVatInvoicing"}
    {widget id="vatPercentage"}
</fieldset>
{widget id="feePanel"}
<fieldset>
    <legend>##Tracking Settings##</legend>
    {widget id="salesCallbackUrl" class="MainSiteUrl"}
    {widget id="isCallbackIgnoredForDeclinedCommission"}
    <div class="Line"></div>
    {widget id="forceCampaignByProductId" class="SaveUnrefered"}
    {widget id="forceChoosingProductidUseDefaultCampaign" class="SaveUnrefered UnreferedPadding"}
    <div class="Line"></div>
</fieldset>

{widget id="FormMessage"}
{widget id="sendButton"}
