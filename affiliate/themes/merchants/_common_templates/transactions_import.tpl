<!-- transactions_import -->
<div class="TransactionImport">
    <table>
        <tr>
            <td valign='top'>
    			<div class="TransactionImportFields FloatLeft">
        			<fieldset>
            			<legend>##Import file format##</legend>
            			{widget id="fields"}
                        {widget id="addButton"}
        			</fieldset>
    			</div>
            </td>
            <td valign='top'>
                <div class="TransactionImportFile FloatLeft">
                    <fieldset>
                        <legend>##Import file##</legend>
                        {widget id="delimiter"}
                        {widget id="source" class="ImportRadioGroup"}
                        {widget id="url"}
                        {widget id="uploadFile"}
                        {widget id="exportFilesGrid"} 
                        {widget id="serverFile"}
                        {widget id="skipFirstRow"}
                        {widget id="transactionType"}
                    </fieldset>
                </div>
                <div class="clear"></div>
                <div class="TransactionImportSettings FloatLeft">
                    <fieldset>
                        <legend>##Transaction import settings##</legend>
                        {widget id="computeAtomaticaly"}
                        {widget id="matchTransaction"}
                        {widget id="matchTransactionStatus"} 
                        {widget id="transactionStatus"}
                    </fieldset>
                </div>
                <div class="clear"></div>
                <div class="TransactionImportSettings FloatLeft">
                    {widget id="importButton"}
                </div>
            </td>
        </tr>
    </table>
</div>
