<!-- transactions_import -->
<div class="TransactionImport">
  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle">##Import file format##</div>
  		<div class="FormFieldsetHeaderDescription"></div>
  	</div>
  	{widget id="fields"}
    {widget id="addButton"}
  </div>

  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle">##Import file##</div>
  		<div class="FormFieldsetHeaderDescription"></div>
  	</div>
    {widget id="delimiter"}
    {widget id="source" class="ImportRadioGroup"}
    {widget id="url"}
    {widget id="uploadFile"}
    {widget id="exportFilesGrid"} 
    {widget id="serverFile"}
    {widget id="skipFirstRow"}
    {widget id="transactionType"}
  </div>

  <div class="FormFieldset">
  	<div class="FormFieldsetHeader">
  		<div class="FormFieldsetHeaderTitle">##Transaction import settings##</div>
  		<div class="FormFieldsetHeaderDescription"></div>
  	</div>
    {widget id="computeAtomaticaly"}
    {widget id="matchTransaction"}
    {widget id="matchTransactionStatus"}
    {widget id="transactionStatus"}
  </div>

  {widget id="importButton"}
</div>
