<!-- account_settings -->

<div class="FormFieldsetSectionTitle">##VAT options##</div>
  {widget id="applyVatInvoicing"}
  {widget id="vatPercentage"}

{widget id="feePanel"}

<div class="FormFieldsetSectionTitle">##Tracking Settings##</div>
  {widget id="salesCallbackUrl" class="MainSiteUrl"}
  {widget id="isCallbackIgnoredForDeclinedCommission"}
  <div class="Line"></div>
  {widget id="forceCampaignByProductId" class="SaveUnrefered"}
  {widget id="forceChoosingProductidUseDefaultCampaign" class="SaveUnrefered UnreferedPadding"}
  <div class="Line"></div>

<div class="clear"></div>
{widget id="FormMessage"}
{widget id="sendButton"}
