<!-- send_to_friend -->
<div class="FormFieldset SendToFriend">
	<div class="FormFieldsetHeader">
		<div class="FormFieldsetHeaderTitle">##Send to a friend##</div>
		<div class="FormFieldsetHeaderDescription"></div>
	</div>
    {widget id="recipients" class="ContactUsText"}
    {widget id="from" class="ContactUsText"}
    {widget id="subject" class="ContactUsText"}
    {widget id="message" class="message" class="ContactUsText"}
    {widget id="bannerId"}
</div>

{widget id="FormMessage"}
{widget id="SaveButton"}
{widget id="CloseButton"}
<div class="clear"></div>
