<!-- campaign_tracking_codes_filter -->

<div class="CampaignTrackingCodesFilter">
    <div class="FilterRow">       
        <div class="FilterLegend">##Campaign##</div>
        {widget id="campaignid"}
        <div class="clear"></div>
    </div>
    <div class="FilterRow">       
        <div class="FilterLegend">##Status##</div>
        {widget id="status"}
        <div class="clear"></div>
    </div>
</div>       
<div style="clear: both;"></div>
   
