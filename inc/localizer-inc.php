<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

$txt_ru = Array(
	"all_logo-alt" => "JustHelp - найдите себе помощь",
	"all_top-btn-dohelp" => "Оказать помощь",
	"all_top-btn-needhelp" => "Получить помощь",
	"all_top-logas" => "Вы вошли как",
	"all_top-login" => "Войти",
	"all_top-logout" => "Выйти",
	"all_top-reg" => "Регистрация",
	"all_top-cabinet" => "Мой кабинет",
	"all_bc-main" => "Главная",

	// Index - main view
	"main_rev-projget" => "Получена помощь",
	"main_rev-projtree" => "Показать мое дерево",

	// Other text resources, named as 'contoller_resource'
	// ... 	

	///////////////////////////////////////////////////////////////////////////
	//
	// Registration process
	//
	///////////////////////////////////////////////////////////////////////////

	//
	"reg_tab-person" => "Регистрация физ. лица",
	"reg_tab-comp" => "Регистрация компании",
	"reg_tab-org" => "Регистрация обществ. организаций",

	// Placeholders for input fields
	"reg_ph-org" => "Название компании",
	"reg_ph-orgo" => "Название организации",
	"reg_ph-sphere" => "Например: торговля, IT-сфера",
	"reg_ph-sphere-org" => "Например: помощь больным, юридические услуги и т.д.",
	"reg_ph-name" => "Ваше имя",
	"reg_ph-fname" => "Ваше фамилия",
	"reg_ph-tel" => "+38(КОД) НОМЕР",
	"reg_ph-prof" => "Например: адвокат",

	// Labels for input fields
	"reg_lbl-org" => "Компания",
	"reg_lbl-orgo" => "Организация",
	"reg_lbl-sphere" => "Сфера деятельности",
	"reg_lbl-login" => "Логин/E-mail",
	"reg_lbl-email" => "Ваш E-mail",
	"reg_lbl-name" => "Ваше имя",
	"reg_lbl-pass" => "Пароль",
	"reg_lbl-pass1" => "Повторить пароль",
	"reg_lbl-fname" => "Фамилия",
	"reg_lbl-tel" => "Телефон",
	"reg_lbl-country" => "Страна",
	"reg_lbl-obl" => "Регион",
	"reg_lbl-oblsel" => "Выберите свой регион",
	"reg_lbl-city" => "Город",
	"reg_lbl-citysel" => "Выберите свой город",
	"reg_lbl-empl" => "Сотрудников",
	"reg_lbl-descr" => "Описание",
	"reg_lbl-captcha" => "Введите код",
	"reg_lbl-avatar" => "Аватарка",
	"reg_lbl-avatar-use" => "Использовать фото из профиля для аватарки",
	"reg_lbl-logo" => "Логотип",
	"reg_lbl-prof" => "Профессия",
	"reg_lbl-loadphoto" => "Загрузить фото",
	"reg_lbl-background" => "Фон проектов",

	// Registration: Error texts for input fields
	"reg_err-org" => "Вы не указали название компании",
	"reg_err-orgo" => "Вы не указали название организации",
	"reg_err-email" => "Вы не указали свой E-mail",
	"reg_err-pass" => "Пароли должны быть одинаковые и не меньше 6-ти символов",
	"reg_err-sphere" => "Необходимо указать хотя бы одну сферу деятельности",
	"reg_err-name" => "Вы не указали свое имя",
	"reg_err-fname" => "Вы не указали свою фамилию",
	"reg_err-country" => "Укажите город, в котором вы находитесь",
	"reg_err-obl" => "Укажите регион, в котором вы находитесь",
	"reg_err-city" => "Укажите город, в котором вы находитесь",
	"reg_err-city-0" => "Укажите свой город",
	"reg_err-tel" => "Телефон указан с ошибками, формат +38(КОД) НОМЕР",
	"reg_err-prof" => "Профессия не задана",
	"reg_err-captcha" => "Контрольный код был введен с ошибкой",


	// Multinum strs
	"days_1" => "день",
	"days_2" => "дня",
	"days_5" => "дней",
	"min_5" => "минут",
	"hour_5" => "часов",
	"human" => "чел.",
	"human_1" => "человек",
	"human_2" => "человека",
	"human_5" => "человек",
	"its" => "шт.",
	"avail" => "Осталось",

	"requests" => "заявок",

	"time_ended" => "Срок действия истек",

	///////////////////////////////////////////////////////////////////////////
	//
	// Popup windows 
	//
	///////////////////////////////////////////////////////////////////////////

	// Login box
	"poplog_hdr" => "Вход на сайт",
	"poplog_lbl-login" => "Email",
	"poplog_lbl-pass" => "Пароль",
	"poplog_restpass" => "Восстановить пароль",
	"poplog_btn-enter" => "Войти",
	"poplog_btn-reg" => "Зарегистрироваться",
	"poplog_btn-fb" => "Войти через Facebook",
	"poplog_btn-vk" => "Войти через ВКонтакте",
	"poplog_btn-ok" => "Войти через Одноклассники",
	"poplog_gotoreg" => "Если вы у нас в первый раз, то зарегистрируйтесь как новый пользователь",

	// Add help box
	"pophelp_hdr" => "Оказать помощь",
	"pophelp_lbl-sum" => "Сумма",
	"pophelp_lbl-amount" => "Кол-во чел.",
	"pophelp_lbl-msg" => "Место для Вашего ответа",
	"pophelp_ph_msg" => "Ваш комментарий к помощи",
	"pophelp_btn-send" => "Подтвердить",
	"pophelp_btn-aft1" => "Оплатить сразу",
	"pophelp_btn-aft2" => "Либо",

	"popproj_hdr" => "Как сделать свой проект успешным",
	"popproj_text" => "<b class='text-align: center;'>Обязательно ознакомьтесь!!!</b><br>
<ul>
<li><a target=\"_blank\" href='http://wayofhelp.com/info/projexample/'>Практический пример.</a></li>
<li><a target=\"_blank\" href='http://wayofhelp.com/info/projectrise/'>Теоретическое руководство.</a></li>
</ul>",

	"popbon_hdr" => "",
	"popbon_text" => "Бонус будет размещен на портале после прохождения модерации.",


	// Get Bonus
	"popbonus_hdr" => "Забрать бонус",
	"popbonus_win" => "Вы только что выиграли подвешенный бонус.",
	"popbonus_loose" => "Вы не смогли угадать, где спрятан подвешенный бонус. В следущий раз вы сможете попробовать забрать этот бонус только через 24 часа.",
	"popbonus_timelimit" => "С момента, как вы уже попробовали забрать этот подвешенный бонус прошло меньше 24х часов. Следующую попытку Вы пока еще не можете использовать.",

	// Register box
	"popreg_hdr" => "Регистрация на сайте",
	"popreg_h-pers" => "Я - физическое лицо",
	"popreg_h-comp" => "Я - юридическое лицо",
	"popreg_h-org" => "Я - общественная организация",
	"popreg_btn-reg" => "Зарегистрироваться",
	"popreg_btn-fb" => "Войти через Facebook",
	"popreg_btn-vk" => "Войти через ВКонтакте",
	"popreg_btn-ok" => "Войти через Одноклассники",

	// Restore password box
	"poprest_hdr" => "Восстановление пароля",
	"poprest_lbl-login" => "Email",
	"poprest_dolog" => "Вернуться к диалогу входа",
	"poprest_btn-send" => "Восстановить",

	// Send message box
	"popmsg_hdr" => "Отправить сообщение",
	"popmsg_lbl-recv" => "Получатель",
	"popmsg_lbl-msg" => "Сообщение",
	"popmsg_ph_msg" => "Текст вашего сообщения",
	"popmsg_btn-send" => "Отправить",

	// Stars review box
	"poprev_hdr" => "Оценить полученную помощь",
	"poprev_lbl-rate" => "Оценка",
	"poprev_lbl-msg" => "Комментарий",
	"poprev_ph_msg" => "Ваш комментарий к оценке",
	"poprev_btn-send" => "Опубликовать",

	// Help graph box
	"popgraph_hdr" => "Дерево взаимопомощи",

	// Pop share box
	"popshare_hdr" => "Поделиться в социальных сетях",
	"popshare_text" => "<b>Ваш проект опубликован!</b><br>Вы получите помощь гораздо быстрее, если поделитесь проектом в социальных сетях.",
	"popshare_soc" => "Для того, чтобы испытать свою удачу и выиграть мгновенный приз, необходимо поделиться бонусом в социальных сетях",
	"pophelp_err-payfmt" => "Сумма должна быть больше ",
	"pophelp_err-payfmt-def" => "Сумма помощи введена неверно ",

	// Pop promo help box
	"pophelp_hdr" => "Спасибо за Вашу помощь!",
	"pophelp_hdrmy" => "Мы готовы Вам помочь",
	"pophelp_btn-dream" => "Расскажи о мечте",
	"pophelp_btn-dohelp" => "Помоги другим",
	"pophelp_btn-gethelp" => "Получи помощь",
	// Pop promo help box
	"pophelp2_hdr" => "Помогай другим!",
	"pophelp2_btn-dream" => "Расскажи о мечте",
	"pophelp2_btn-dohelp" => "Помоги другим",
	"pophelp2_btn-gethelp" => "Получи помощь",

	///////////////////////////////////////////////////////////////////////////
	//
	// Catalog pages
	//
	///////////////////////////////////////////////////////////////////////////

	"cat_lcol-hdr" => "Тематика проектов",
	"cat_free-bonus" => "Подвешенный бонус",
	"cat_flt-hdr" => "Фильтр по проектам",
	"cat_flt-all" => "Все",
	"cat_flt-popular" => "Популярные",
	"cat_flt-new" => "Новые",
	"cat_flt-ending" => "Завершающиеся",
	"cat_flt-thdr" => "Проекты по типу помощи",
	"cat_flt-tmoney" => "Деньгами",
	"cat_flt-twork" => "Делом",
	"cat_flt-tloc" => "Ближайшие к вам",
	"cat_flt-teasy" => "Легкая помощь",
	"cat_btn-gethelp" => "Получить помощь",
	"cat_btn-dream" => "Осуществить свою мечту",
	"cat_btn-thing" => "Поделиться вещами",
	"cat_btn-myhelp" => "Окажу помощь",
	"cat_it-started" => "Запущен",
	"cat_it-btn-help" => "Оказать помощь",
	"cat_it-help" => "уже оказали помощь",
	"cat_it-getthing" => "уже попросили вещь",
	"cat_it-rating" => "текущий рейтинг",
	"cat_it-btn-event" => "Принять участие",
	"cat_it-btn-thing" => "Забрать вещь",
	"cat_it-btn-myhelp" => "Попросить о помощи",
	"cat_it-event" => "уже хотят принять участие в событии",
	"cat_noproj" => "В данном разделе еще не опубликовано ни одного проекта на получение помощи.<br>Опубликуйте свой проект первым!",
	"cat_nothing" => "В данном разделе еще не опубликовано ни одного объявления с вещами на раздачу.<br>Опубликуйте свой проект первым!",


	///////////////////////////////////////////////////////////////////////////
	//
	// Free Bonus pages
	//
	///////////////////////////////////////////////////////////////////////////

	"bonus_lcol-hdr" => "Тематика проектов",
	//"bonus_flt-hdr" => "Фильтр по проектам",
	//"bonus_flt-all" => "Все",
	//"bonus_flt-popular" => "Популярные",
	//"bonus_flt-new" => "Новые",
	//"bonus_flt-ending" => "Завершающиеся",
	"bonus_btn-bonus" => "Подвесить бонус",
	"bonus_it-started" => "Запущен",
	"bonus_it-btn-getbonus" => "Получить бонус",
	"bonus_it-help" => "уже оказали помощь",
	"bonus_noproj" => "В данном разделе еще не опубликовано ни одного подвешенного бонуса.<br>Подвесьте свой бонус первым!",

	// Add photo
	"addbonus_photo-hdr" => "Добавление фото к проекту",
	"addbonus_photo-lbl" => "Прикрепить фото к проекту",
	"addbonus_photo-del" => "Удалить",
	"addbonus_photo-btn" => "Загрузить фото",
	"addproj_photo-btn-next" => "Загрузить",
	"addbonus_photo-user-lbl" => "Загрузить мою аватарку",
	"addbonus_photo-user-btn" => "Загрузить аватарку",
	"addbonus_photo-btn-finish" => "Опубликовать проект",
	// Add proj
	"addbonus_h1" => "Подвешенный бонус",
	"addbonus_h1-edit" => "Редактировать бонус",
	"addbonus_h1-created" => "Бонус подвешен",
	"addbonus_hdr" => "Заполнение данных о бонусе",
	"addbonus_ph-tit" => "Что вы хотите раздавать",
	"addbonus_lbl-tit" => "Название бонуса",
	"addbonus_lbl-difficulty" => "Сложность",
	"addbonus_lbl-amount" => "Кол-во бонусов",
	"addbonus_lbl-amount-money" => "Объем помощи",
	"addbonus_lbl-amount-human" => "Кол-во чел.",
	"addbonus_lbl-dtst" => "Дата старта",
	"addbonus_lbl-dten" => "К какой дате",
	"addbonus_lbl-country" => "Страна",
	"addbonus_lbl-country-0" => "Укажите свою страну",
	"addbonus_lbl-obl" => "Регион",
	"addbonus_lbl-obl-0" => "Укажите свой регион",
	"addbonus_lbl-city" => "Город",
	"addbonus_lbl-city-0" => "Укажите свой город",
	"addbonus_lbl-descr" => "Описание",
	"addbonus_lbl-descr-count0" => "Введите еще",
	"addbonus_lbl-descr-count1" => "символов, чтобы Вы выдавались в поиске выше",
	"addbonus_lbl-descrfull" => "Подробное описание",
	"addbonus_lbl-photo" => "Фото",
	"addbonus_lbl-background" => "Фон карточки проекта",
	"addbonus_btn-addphoto" => "Загрузить фото",
	"addbonus_btn-addbonus" => "Разместить бонус",
	"addbonus_btn-save" => "Сохранить изменения",
	// Add proj errors
	"addbonus_err-tit" => "Необходимо задать название бонуса",
	"addbonus_err-sect" => "Необходимо выбрать конечный каталог для проекта",
	"addbonus_err-amount" => "Необходимо указать количество доступных бонусов",
	"addbonus_err-amount1" => "Объем помощи должен быть допустимым числовым значением и больше нуля.",
	"addbonus_err-stdt" => "Дата должна быть указана в формате ДД.ММ.ГГГГ",
	"addbonus_err-endt" => "Дата должна быть указана в формате ДД.ММ.ГГГГ и быть больше даты начала",
	"addbonus_err-obl" => "Вы не указали страну",
	"addbonus_err-obl" => "Вы не указали регион/область",
	"addbonus_err-city" => "Вы не указали город",
	"addbonus_err-descr" => "Необходимо описать, что из себя представляет бонус и условия его получения",

	"addbonus_photo-promo" => "Наличие фотографии позволит пользователю оценить, нужен ли ему этот бонус или нет. И тогда Ваш бонус достанется именно тому, кто в нем больше нуждается!",

	// View project
	"bonusview_dt-add" => "Размещено",
	"bonusview_views" => "Всего просмотров",
	"bonusview_notify-owner" => "Вы являетесь создателем данного проекта.",
	"bonusview_nomore" => "Бонусы уже закончились",
	"bonusview_btn-getbonus" => "Получить бонус",

	"bonusview_stat-sum" => "Уже собрано",
	"bonusview_stat-up" => "до",
	"bonusview_stat-numwin" => "Уже выиграли",
	"bonusview_stat-tm" => "Осталось времени",
	"bonusview_stat-till" => "Бонус активен до",

	"bonusview_hdr-descr" => "Подробные сведения",
	"bonusview_fld-cont" => "Контактное лицо",
	"bonusview_fld-city" => "Город",
	"bonusview_fld-msg" => "Связаться",
	"bonusview_btn-msg" => "Написать сообщение",
	"bonusview_usr-rate" => "Рейтинг оказания помощи",
	"bonusview_usr-helpnum" => "Всего помог раз",
	"bonusview_hdr-need" => "Приму помощь в разделах",
	"bonusview_btn-tree" => "Показать моё дерево",

	"bonusview_hdr-comment" => "Комментарии",
	"bonusview_dt-comment" => "Добавлено",
	"bonusview_lbl-comment" => "Ваш комментарий",
	"bonusview_err-comment" => "Комментарий должен содержать какой-то текст.",
	"bonusview_btn-comment" => "Отправить",

	///////////////////////////////////////////////////////////////////////////
	//
	// Project view pages
	//
	///////////////////////////////////////////////////////////////////////////

	"proj_notallowed" => "Вернуться к просмотру моих проектов",
	"proj_backtoproj" => "Вернуться к просмотру проекта",
	"proj_backtocab" => "Вернуться в кабинет к перечню проектов",

	// Project create
	// Add photo 
	"addproj_photo-hdr" => "1. Добавление фото к проекту",
	"addproj_photo-lbl" => "Прикрепить фото к проекту",
	"addproj_photo-lbl-add" => "Прикрепить <span class='lbl-add-green'>еще фото</span> к проекту",
	"addproj_photo-del" => "Удалить",
	"addproj_photo-btn" => "Загрузить фото",
	"addproj_photo-user-lbl" => "Загрузить мою аватарку",
	"addproj_photo-user-btn" => "Загрузить аватарку",
	"addproj_photo-btn-finish" => "Опубликовать проект",
	"addproj_background-hdr" => "2. Выберите пожалуйста платежные системы, на которые Вы хотели бы получать деньги",
	"addproj_background-hdr-nxt" => "Советуем указывать как можно больше систем, это увеличит вероятность получения помощи. Так же, для сбора средств, вы можете воспользоваться нашими расчетными счетами с последующим выводом денег на себя.",
	"addproj_avatar-hdr" => "3. Загрузить аватарку автора",
	// Add proj	
	"addproj_hdr" => "Заполнение данных о проекте",
	"addproj_hdrevent" => "Заполнение данных о событии",
	"addproj_ph-tit" => "Заголовок вашего проекта",
	"addproj_lbl-tit" => "Название проекта",
	"addproj_lbl-titevent" => "Название события",
	"addproj_lbl-sect" => "В какую группу опубликовать",
	"addproj_lbl-sect0" => "Раздел не выбран",
	"addproj_lbl-type" => "Тип проекта",
	"addproj_lbl-typeevent" => "Тип события",
	"addproj_lbl-type-0" => "Помочь деньгами",	// "Сбор материальной помощи",
	"addproj_lbl-type-1" => "Набор группы людей",
	"addproj_lbl-type-2" => "Помочь делом",	//"Поиск помощи у специалиста",
	"addproj_lbl-type-3" => "Другой тип помощи",
	"addproj_lbl-amount" => "Объем помощи",
	"addproj_lbl-amount-money" => "Объем помощи",
	"addproj_lbl-amount-human" => "Кол-во чел.",
	"addproj_lbl-dtst" => "Дата старта",
	"addproj_lbl-dten" => "К какой дате",
	"addproj_lbl-country" => "Страна",
	"addproj_lbl-country-0" => "Укажите свою страну",
	"addproj_lbl-obl" => "Регион",
	"addproj_lbl-obl-0" => "Укажите свой регион",
	"addproj_lbl-city" => "Город",
	"addproj_lbl-city-0" => "Укажите свой город",
	"addproj_lbl-descr" => "Описание",
	"addproj_lbl-descr-placeholder" => "Рекомендуем использовать не менее 300 символов на описание проекта, т.к. такие проекты заслуживают большего доверия от других пользователей.",
	"addproj_lbl-descr-count0" => "Введите еще",
	"addproj_lbl-descr-count1" => "символов, чтобы Вы выдавались в поиске выше",
	"addproj_lbl-descrfull" => "Подробное описание",
	"addproj_lbl-descrdop" => "(будет видно во время поиска)",
	"addproj_lbl-descrfulldop" => "(посетитель увидит, когда зайдет в проект)",
	"addproj_lbl-photo" => "Фото",
	"addproj_lbl-background" => "Фон карточки проекта",
	"addproj_btn-addphoto" => "Загрузить фото",
	"addproj_btn-adddream" => "Осуществить мечту",
	"addproj_btn-addevent" => "Создать событие",
	"addproj_btn-addthing" => "Поделиться вещами",
	"addproj_btn-addproj" => "Разместить проект",
	"addproj_btn-save" => "Сохранить изменения",
	// Add proj errors
	"addproj_err-tit" => "Необходимо задать название проекта",
	"addproj_err-titevent" => "Необходимо задать название события",
	"addproj_err-sect" => "Необходимо выбрать конечный каталог для проекта",
	"addproj_err-sectevent" => "Необходимо выбрать конечный каталог для события",
	"addproj_err-amount" => "Необходимо указать требуемый объем помощи",
	"addproj_err-amount1" => "Объем помощи должен быть допустимым числовым значением и больше нуля.",
	"addproj_err-stdt" => "Дата должна быть указана в формате ДД.ММ.ГГГГ",
	"addproj_err-endt" => "Дата должна быть указана в формате ДД.ММ.ГГГГ и быть больше даты начала",
	"addproj_err-obl" => "Вы не указали страну",
	"addproj_err-obl" => "Вы не указали регион/область",
	"addproj_err-city" => "Вы не указали город",
	"addproj_err-descr" => "Необходимо описать, с какой целью Вы хотите получить помощь",
	"addproj_err-descrevent" => "Необходимо описать суть проводимого события",

	"addproj_photo-promo" => "По статистике, вероятность получения помощи увеличивается на 60% при наличии фотографий!",

	// View project
	"projview_dt-add" => "Размещено",
	"projview_views" => "Всего просмотров",
	"projview_rating" => "Текущий рейтинг",
	"projview_notify-owner" => "Вы являетесь создателем данного проекта.",
	"projview_notify-proj-stoped" => "В данный момент проект остановлен. Получение помощи приостановлено.",
	"projview_notify-proj-ended" => "В данный момент проект уже окончен и дальнейшее оказание помощи невозможно.",
	"projview_notify-helpwait" => "В данный момент вы уже отправили заявку на помощь. Она ожидает подтверждения.",
	"projview_notify-helpdone" => "В данный момент вы уже отправили заявку на помощь и она была подтверждена.",
	"projview_notify-helpdecl" => "Ваша заявка была отклонена.",
	"projview_btn-money" => "Помочь деньгами",
	"projview_btn-help" => "Помочь делом",
	"projview_btn-thing" => "Попросить вещь",
	"projview_btn-gethelp" => "Попросить помощь",
	"projview_nophoto-txt" => "На данный момент нет дополнительных фотографий",

	"projview_stat-sum" => "Уже собрано",
	"projview_stat-avg" => "Средняя помощь",
	"projview_woh-hlp" => "Из них по акции",
	"projview_stat-target" => "Цель",
	"projview_stat-up" => "до",
	"projview_stat-num" => "Уже помогли",
	"projview_stat-numhuman" => "Уже присоединилось",
	"projview_stat-tm" => "Осталось времени",
	"projview_stat-till" => "Помощь актуальна до",
	"projview_stat-numreq" => "Уже собрано заявок",

	"projview_hdr-descr" => "Подробные сведения",
	"projview_fld-cont" => "Контактное лицо",
	"projview_fld-city" => "Город",
	"projview_fld-msg" => "Связаться",
	"projview_btn-msg" => "Написать сообщение",
	"projview_usr-rate" => "Рейтинг оказания помощи",
	"projview_usr-helpnum" => "Всего помог раз",
	"projview_hdr-need" => "Приму помощь в разделах",
	"projview_btn-tree" => "Показать моё дерево",

	"projview_hdr-comment" => "Комментарии",
	"projview_dt-comment" => "Добавлено",
	"projview_lbl-comment" => "Ваш комментарий",
	"projview_err-comment" => "Комментарий должен содержать какой-то текст.",
	"projview_btn-comment" => "Отправить",

	"proj_tree" => "Дерево добрых дел",

	"projhelp_hdr" => "Последняя полученная помощь",
	"projhelp_today" => "сегодня",
	"projhelp_ago" => "назад",
	"projhelp_showall" => "Все предложения помощи",
	"projhelp_showallcom" => "Показать еще комментарии →",
	"projmail_help-tit" => "Ваш проект получил очередной взнос!!!",
	//"projmail_help-descr-pr" => "Ваш проект ",
	"projmail_help-descr-pr2" => "Ваш проект получил очередную помощь в размере ",
	"projmail_help-descr-pr3" => "<br>Обязательно поблагодарите пользователя ",
	"projmail_help-descr-pr4" => "<br> за оказанную помощь. <br><br> Перейдите по _hr_ и нажмите кнопку «Поблагодарить». <br><br> Благодарность не только является хорошим тоном <br> и привлекает к Вам положительную энергию успеха, <br> но и побуждает людей помогать Вам снова. ",


	"projdone_breadcr-myproj" => "Мои проекты",
	"projdone_h1-edit" => "Редактирование проекта",
	"projdone_h1-saved" => "Информация о проекте сохранена",
	"projdone_h1-new" => "Создание нового проекта",
	"projdone_h1-new-dream" => "Реализуй свою мечту",
	"projdone_h1-new-event" => "Создание нового события",
	"projdone_h1-new-thing" => "Поделись своими вещами",
	"projdone_h1-new-myhelp" => "Окажу помощь нуждающимся",
	"projdone_h1-created" => "Вы создали проект на получение помощи",
	"projdone_h1-helpdone" => "Вы отправили заявку на оказание помощи",
	"projdone_h1-thingdone" => "Вы отправили заявку на получение вещей",
	"projdone_h1-myhelpdone" => "Вы отправили заявку на получение помощи",

	"projview_rate-dream" => "Оцени мечту",
	"projview_share" => "Поделиться",
	"projview_sharing" => "Хочешь помочь, сделай репост",
	"projview_repost" => "Сделали репост",
	"projview_usr-rating" => "Рейтинг",

	// Promise page
	"projprom_prom-txt" => "<p>Спасибо за готовность помочь.<br> 
							<br>По правилам платформы, во избежании накрутки рейтинга, необходимо подтверждать свое намерение, <br> если сумма всех обещаний превышает <b>_defsum_</b> _cur_ либо единоразовый платеж - <b>_defsum1_</b> _cur_ . <br> Для этого необходимо перевести <b>_perc_%</b> от суммы обещаний<br> в ваш личный стабилизационный фонд на платформе Wayofhelp.<br> На данный момент общая сумма обещаний составляет <b>_allsum_</b>. <a href='_hr_' target='_blank' > Подробнее </a>",
	"projprom_btn" => "Обещаю помочь, если…",
	"projprom_phone-done" => "Номер телефона был подтвержден.",
	"projprom_phone-msgsend" => "На Ваш номер был отправлен код подтверждения.",
	"projprom_phone-notdone" => "Пожалуйста укажите Ваш номер телефона правильно.",
	"projprom_phone-errcode" => "Код указан неверно.",
	"projprom_frm-phone" => "Телефон",
	"projprom_frm-code" => "Код подтверждения",
	"projprom_def-msg" => " <p>Спасибо за Вашу готовность помочь.<br>
                            <br>Если пользователь насобирает нужную сумму, Мы попросим Вас выполнить свое обещание.
                            <br>Готовность помочь так же важна, как и сама помощь. Именно по этому Ваша готовность отобразится на Дереве добра, как настоящая помощь.</p>",
	"projprom_def-form-msg1" => "                 <p>Благодарим вас за готовность помочь.</p>
                            <p>Общая сумма денег, которую вы обещаете внести

                            на помощь другим проектам = ",
	"projprom_def-form-msg2" => "<p>Это солидная сумма.
                            Для исключения недоразумений, связанных с Вашими благими намерениями, введите

                            пожалуйста номер Вашего мобильного телефона в строку <span class=\"bg\">«Телефон»</span>.</p><p>На него будет

                            отправлен код, который нужно будет ввести в строку <span class=\"bg\">«Код»</span> данной формы.</p>",

	//////////////////////////////////////////////////////////////////////////////////
	// 
	// User info page
	//
	//////////////////////////////////////////////////////////////////////////////////

	"user_profession" => "Профессия",
	"user_sphere" => "Сфера деятельности",
	"user_contact" => "Контактное лицо",
	"user_mysite" => "Мои сайты",
	"user_nosite" => "вы не указали свой сайт",
	"user_about" => "Подробные сведения",
	"user_tab-needhelp" => "Просьбы этого пользователя о помощи",
	"user_tab-feedbacks" => "Отзывы об этом пользователе",

	"user_tot-req" => "заявок",
	"user_say-thanks" => "Поблагодарить",
	
	"user_tbl-btntree" => "Посмотреть наглядно",
	"user_tbl-proj" => "Проект",
	"user_tbl-projhelp" => "Уже помог",
	"user_tbl-sten" => "Начало / Конец",
	"user_tbl-left" => "Осталось",
	"user_tbl-feed" => "Отзыв",
	"user_tbl-rate" => "Оценка",
	"user_tbl-noproj" => "У этого пользователя нет ни одного созданного проекта на получение помощи.",
	"user_tbl-norate" => "Еще никто не оставил оценку о помощи этого пользователя.",

	"user_stat-rate" => "Рейтинг при оказании помощи",
	"user_stat-tothelp" => "Всего помог",
	"user_stat-helpmoney" => "Из них деньгами",
	"user_stat-tothelps" => "Всего помог",
	"user_stat-helpsum" => "На сумму",

	"user_need-hdr" => "Приму помощь в разделах",
	"user_willhelp-hdr" => "Окажу помощь в разделах",
	"user_btn-tree" => "Моё дерево",

	"user_dohelp-hdr" => "Последняя оказанная помощь пользователем",
	"user_dohelp-rate" => "",
	"user_dohelp-ago" => "назад",

	"user_share-profile1" => "Поделиться",
	"user_share-profile2" => "профилем",
	"user_rat" => "Рейтинг",
	"user_moreinfo" => "Подробные сведения",
	"user_wohpays" => "Заработано за активность",

	// SHARE USER

	//////////////////////////////////////////////////////////////////////////////////
	//
	// Reviews
	//
	//////////////////////////////////////////////////////////////////////////////////

	"rev_gethelp" => "Получена помощь",
	"rev_btn-tree" => "Моё дерево",

	//////////////////////////////////////////////////////////////////////////////////
	// 
	// Cabinet
	//
	//////////////////////////////////////////////////////////////////////////////////

	"cab_tab-profile" => "Профиль",
	"cab_tab-myhelp" => "Моя помощь",
	"cab_tab-myhelpget" => "Получаю помощь",
	"cab_tab-needhelp" => "Нуждаюсь в помощи",
	"cab_tab-dohelp" => "Оказываю помощь",
	"cab_tab-givehelp" => "Поделюсь",
	"cab_tab-msg" => "Сообщения",
	"cab_tab-bonus" => "Мои бонусы",
	"cab_tab-onlooking" => "Как меня видят другие",

	// Users profile
	"cabprof_social" => "Привязать аккаунт к соц. сети",
	"cabprof_u-profession" => "Профессия",
	"cabprof_u-sphere" => "Сфера деятельности",
	"cabprof_u-sphere-notset" => "не указана",
	"cabprof_u-contact" => "Контактное лицо",
	"cabprof_u-mysite" => "Мои сайты",
	"cabprof_u-nosite" => "вы не указали свой сайт",
	"cabprof_u-stat-rate" => "Рейтинг при оказании помощи",
	"cabprof_u-btn-edit" => "Править профиль",
	"cabprof_u-btn-topics" => "Править интересы",

	"cabprof_tree-hdr" => "Мое дерево",
	//"cabprof_tree-in" => "входящих",
	//"cabprof_tree-out" => "исходящих",
	"cabprof_tree-in" => "полученная мною помощь",
	"cabprof_tree-out" => "оказанная мною помощь",

	"cabprof_rnd-dohelp" => "Оказать<br>помощь",
	"cabprof_rnd-gethelp" => "Получить<br>помощь",
	"cabprof_rnd-msg" => "Сообщения",
	"cabprof_rnd-stat" => "Статистика",

	// Need help projects
	"cabneed_flt-hdr" => "Фильтр по проектам",
	"cabneed_flt-all" => "Все",
	"cabneed_flt-cur" => "Текущие",
	"cabneed_flt-ended" => "Оконченные",

	"cabneed_btn-new" => "Создать новый проект",
	"cabneed_btn-newevent" => "Создать событие",

	"cabneed_tbl-id" => "№",
	"cabneed_tbl-proj" => "Проект",
	"cabneed_tbl-status" => "Статус",
	"cabneed_tbl-msg" => "Сообщения",
	"cabneed_tbl-sten" => "Начало/конец",
	"cabneed_tbl-left" => "Осталось",
	"cabneed_tbl-func" => "Действия",
	"cabneed_tbl-noproj" => "У вас нет ни одного созданного проекта на получение помощи.",
	"cabneed_tbl-noproj-running" => "У вас нет ни одного текущего проекта на получение помощи.",
	"cabneed_tbl-noproj-finished" => "У вас нет ни одного оконченного проекта на получение помощи.",

	"cabneed_func-edit" => "Править",
	"cabneed_func-start" => "Запустить",
	"cabneed_func-stop" => "Остановить",
	"cabneed_func-success" => "Закрыть как удачный",
	"cabneed_prepay" => "Оплатить проект",

	"cabneed_stat-stoped" => "Проект приостановлен",
	"cabneed_stat-run" => "Проект запущен",
	"cabneed_stat-ended" => "Проект окончен",

	"cabneed_req-new" => "Новые предложения помощи",
	"cabneed_req-all" => "Все предложения помощи",

	"cabmyhelp_req-new" => "Новые просьбы о помощи",
	"cabmyhelp_req-all" => "Все просьбы помощи",

	// my do help requests 
	"cabdohelp_flt-hdr" => "Фильтр по проектам",
	"cabdohelp_flt-all" => "Все",
	"cabdohelp_flt-cur" => "Текущие",
	"cabdohelp_flt-ended" => "Оконченные",

	"cabdohelp_btn-dohelp" => "Оказать помощь",

	"cabdohelp_tbl-proj" => "Проект",
	"cabdohelp_tbl-status" => "Статус",
	"cabdohelp_tbl-msg" => "Сообщения",
	"cabdohelp_tbl-left" => "Осталось",
	"cabdohelp_tbl-func" => "Действия",
	"cabdohelp_tbl-noreq" => "В данный момент вы еще не оказали ни одной помощи.",
	"cabdohelp_tbl-noreq-running" => "Вы еще не оказали ни одной помощи по текущим проектам.",
	"cabdohelp_tbl-noreq-finished" => "Вы не оказали ни одной помощи по уже оконченным проектам.",

	"cabdohelp_stat-wait" => "Ждет подтверждения",
	"cabdohelp_stat-waitbyuser" => "Ждет проверки",
	"cabdohelp_stat-confirm" => "Подтвержден",
	"cabdohelp_stat-promise" => "Обещано",
	"cabdohelp_stat-promisemess" => "Ждет обещаных денег",
	"cabdohelp_stat-promisepay" => "Оплачено/Ждет подтверждения",
	"cabdohelp_stat-cancel" => "Отменил свою заявку",
	"cabdohelp_stat-decline" => "Помощь отклонена",

	"cabdohelp_stat-promaddr" => "Нужно будет оплатить, когда проект насобирает необходимую сумму",
	"cabdohelp_stat-promaddrmsg" => " Ждет обещанных",

	"cabdohelp_func-cancel" => "Отменить",

	// help requests to me
	"cabreq_flt-hdr" => "Фильтр по предложениям помощи",
	"cabreq_flt-all" => "Все",
	"cabreq_flt-new" => "Новые",
	"cabreq_flt-confirm" => "Подтвержденные",
	"cabreq_flt-decline" => "Отклоненные",

	"cabreq_tbl-pers" => "Кто помог",
	"cabreq_tbl-persget" => "Кто просит",
	"cabreq_tbl-persmy" => "Кто просит",
	"cabreq_tbl-help" => "Помощь",
	"cabreq_tbl-getthing" => "Запрос",
	"cabreq_tbl-status" => "Статус",
	"cabreq_tbl-func" => "Действия",

	"cabreq_help-money" => "Внес",
	"cabreq_help-moneyprom" => "Обещано",
	"cabreq_help-moneypromcheck" => "Подтвердите",
	"cabreq_rate-val" => "Ваша оценка",

	"cabreq_stat-confirm" => "Подтвержден",
	"cabreq_stat-wait" => "Ждет подтверждения",
	"cabreq_stat-decline" => "Помощь отклонена",

	"cabreq_btn-accept" => "Принять",
	"cabreq_btn-decline" => "Отклонить",
	"cabreq_btn-rate" => "Оценить помощь",

	// Messages
	"cabmsg_msg-tot" => "Всего сообщений",
	"cabmsg_msg-in" => "Входящих",
	"cabmsg_msg-out" => "Исходящих",
	"cabmsg_msg-new" => "Новых сообщений",

	"cabmsg_noitems" => "У вас нет ни одного входящего и исходящего сообщения.",

	"cabmsg_proj-started" => "Запущен",
	"cabmsg_reply" => "Ответить",
	"cabmsg_newmsg" => "Написать сообщение",

	// Bonus list
	"cabbonus_tbl-bonus" => "Бонус",
	"cabbonus_tbl-luckind" => "Счастливое число",
	"cabbonus_tbl-get" => "Получен",
	"cabbonus_tbl-func" => "Действия",
	"cabbonus_tbl-noits" => "В данный момент Вы еще не выиграли ни одного подвешенного бонуса",

	//Main index page
	"main_sec-dream" => "человек осуществили мечту",
	"main_sec-tree" => "Пользователей запустили свое ",
	"main_sec-tree1" => "Дерево добра",
	"main_sec-loc" => "Максимальное растояние между получением помощи(Киев-Якутск)",
	"main_sec-smily" => "Самый добрый пользователь",
	"main_sec-bonus1" => "ежедневно выигрывают мгновенные",
	"main_sec-bonus2" => "за добрые дела",
	"main_sec-loc" => "Максимальное растояние между получением помощи(Киев-Якутск)",
	"main_sec-help" => "Самых активных пользователей платформы ежедневно гарантированно получают помощь",
	"main_sec-h1" => "Получи помощь",
	"main_sec-h2" => "или запусти свое Дерево добрых дел",

	"projprom_def-msg-s" => "Спасибо за Вашу<br>готовность помочь!",
	"projprom_def-msg-o" => "Вы можете произвести оплату <a href='http://wayofhelp.com/cabinet/dohelp/' style='color:#9deaec;cursor: pointer;text-decoration: underline;'>прямо сейчас</a> или сделать это в любое удобное время, например, когда проект насобирает 60% заявленной суммы. 
							 Для этого необходимо зайти на портал под своим аккаунтом и перейти на вкладку \"Оказываю помощь\". 
							 Найти этот проект и нажать кнопку \"Выполнить обещание\"",


	//POPUP-POSTREGISTR
	"popup_postreg-title" => "Давай изменим мир к лучшему!",
	"popup_postreg-text" => "С таким лозунгом <b>Way of Help</b> -  благотворительный проект, который осуществляет мечты всех простых людей и делает невероятные чудеса - выходит активно на просторы интернета!</p>
        <p>Проект собрал в себе мечтателей, изобретателей, добродетелей, отзывчивых и нуждающихся в помощи людей с чистым сердцем!</p>
        <p><b>Way of Help</b> – место, где вы сможете получить и оказать посильную помощь: добрыми делами, услугами,  деньгами, советами и даже лайками!) Получить кровь от доноров, бесплатный эвакуатор, отдать котенка  или прокачанный аккаунт в какой то игре!</p>
        <p>Ведь действительно, все это реально зависит от каждого из нас и любая мелочь для кого-то совсем не мелочь)</p>
        <p>Каждый из нас имеет возможность изменить мир к лучшему! Вместе у нас больше шансов и каждый день делая добрые дела мы сможем изменить и улучшить жизни многих людей так же как и свою!</p>
        <p>Не стоит забывать, что все добрые дела возвращаются приумножившись бумерангом.</p>
        <p>О всех благих поступках узнает множество людей, среди которых наверняка найдутся те, кто смогут помочь и тебе и внести свой вклад. Твори добро другим во благо!</p>
        <p>Размести у себя на странице эту запись и это будет твоим началом и очень важным добрым делом ;)",
	"popup_postreg-share" => "Меняем мир вместе.",
	"popup_postreg-sharetit" => " запустил свою цепочку добрых дел на портале wayofhelp.",

	//NEW USER STATUS

	"mess_usr-new-stattit" => "Поздравляем! Вам присвоен новый статус…!!!",

	"mess_usr-new-statbody" => "Благодаря своей активности и стремлению помогать людям, Вы достигли нового уровня - Вам присвоен <br>
статус <b>_stat_</b>!!!
<br><br>С Вами добрых дел в мире становится больше!!!<br><br>

Перейдите по <a href='http://wayofhelp.com/cabinet/'>ссылке</a> и поделитесь этой замечательной<br>
новостью с друзьями.",
	//CHECK
	"proj_check-project" => "Вы не полностью заполнили свой профиль.<br>Убедитесь что вы указали свой email, страну и город.",
	"proj_check-editme" => "Редактировать свой профиль",
	//CRONS MESS
	"crons_dailymess-all" => "",
	"crons_dailymess-titall" => "Ваш проект получил новые комментарии и оценки",
	"crons_dailymess-titstar" => "Ваш проект получил новые оценки",
	"crons_dailymess-titcomm" => "У вашего проекта # новых комментариев",
	"crons_dailymess-bodycomm1" => "Ваш проект _title_ получил новые комментарии.<br>
Чтобы узнать их содержание, пожалуйста, перейдите по ссылке.<br><br>
Не забывайте отвечать на комментарии пользователей.<br>
Это повысит доверие и лояльность к Вашему проекту.",
	"crons_dailymess-bodycomm2" => "Перейдите по ссылке, для того, чтобы прочитать их.",
	"crons_dailymess-bodystar1" => "Ваша мечта получила новые оценки от пользователей.",
	"crons_dailymess-bodystar2" => "Перейдите по ссылке и узнайте, как была оценена Ваша мечта. ",
	"crons_dailymess-bodyall1" => "У Вашего проекта появились новые комментарии и оценки. Перейдите по ссылке для того, чтобы узнать подробнее. ",
	"crons_dailymess-bodyall2" => "Перейдите по ссылке для того, чтобы узнать подробнее. ",
	"crons_succesmess-paytit" => "Благодаря Вам успешно завершился еще один проект!",
	"crons_succesmess-finish" => "Ваш проект удачно закрылся!",

	"crons_succesmess-paybody" => "<b>У нас отличная новость!</b><br>Еще один <a href='_hr_'>проект</a> на нашей платформе достиг успешного завершения.<br>И в этом есть <b> Ваша заслуга! </b><br><br>Мы просим Вас выполнить свои обязательства по Вашим \"обещаниям\"<br>этому проекту в течение недели. <br><br>Пожалуйста, сделайте это в срок, чтобы владелец проекта<br>поскорее осуществил свою мечту.<br><br>Для выполнения обещания, <a href='_hr2_'>авторизируйтесь</a> на сайте и в личном кабинете<br>
откройте вкладку «Оказываю помощь», напротив нужного Вам проекта нажмите зеленую кнопку \"Выполнить<br>обещание\" и совершите оплату.</a>
",
	"crons_message-newshead" => "Позитив на весь день!!!",
	"crons_message-newsshare" => "Подписывайся на наши группы <a href='_VK_'>VK</a>, <a href='_FB_'>FB</a> и <a href='_OK_'>OK</a> и получай
еще больше ярких и позитивных эмоций 
",
	"crons_message-newstext" => "Начните день с добрых и позитивных эмоций.<br>
Истории о людях и их поступках, которые поднимают душу ввысь!<br>
<br>",
	"crons_message-money" => "<br>До получения статуса _hr1_ не хватает _hr2_ баллов",
	"crons_message-newbontit" => "Новый бонус ждет Вас",
	"crons_message-newbon" => "На портале WayOfHelp появились новые бонусы:<br>",
	"crons_message-newbon1" => "<ul>
<li><a href='_hr2_'>_hr1_</a></li>
</ul> ",
	"crons_message-newbon2" => "Делайте добрые дела и растите свое <a href='http://wayofhelp.com/info/tree/'>Дерево Добра</a>.<br>Заходите и выигрывайте приятные ценные призы за Вашу помощь.<br>
Больше добрых дел,- больше бонусов.<br>",

	"crons_newitem-tit" => "Новые проекты, которые могут быть Вам интересны",
	"crons_newitem-desc" => "<br>За время вашего отсутствия на портале <a href='http://wayofhelp.com/'>WayOfHelp</a> появились<br>новые проекты, которые могут быть Вам интересны<br>
<br>Посмотрите, возможно именно Ваша помощь поможет <br>
пользователю осуществить свою мечту!!!<br>
",

	"crons_message-newsdesc" => "Новые статьи на WayOfHelp:<br>",
	"crons_message-newsdown" => "Если ты тоже не равнодушен к подобным историям и стремишься сделать мир лучше, то можешь начать делать это прямо сейчас, выбрав помощь, которая тебе по плечу.<br>Запускай свою цепочку добрых дел и смотри как она растет и провоцирует новые добрые дела.",
	"crons_message-topnews" => "<ul>
<li><a href='_hr2_'>_hr1_</a></li>
</ul> ",
	"crons_message-topjob" => "_alljob_ добрых дел, из них деньгами...",
	"crons_message-alljob" => "За прошедшую неделю на нашем портале было оказано _alljob_ добрых дел. Из них деньгами: _moneyjob_ <br>",
	//"crons_message-weekuser" => "Пользователем недели стали:<br><ul>
//<li><a href='_href_'>_week_</a></li>
//</ul> <br>",
	"crons_message-active1" => "Самую большую активность проявили: <br>",
	"crons_message-active" => "<ul>
<li><a href='_hrefact_'>_active_</a></li>
</ul><br>",
	"crons_message-topusrtree" => "Рейтинг пользователей смотрите <a href='http://wayofhelp.com/rating/'>тут</a>.<br>Самое большое Дерево Добра у пользователя: _toptree_ ,- <a href='_hreftree_'>смотрите его дерево</a><br>",
	"crons_message-topprovoc" => "Больше всего добрых дел спровоцировал: _topprov_ ,- <a href='_hrefprov_'>смотрите его дерево</a><br>",
	"crons_message-newbondot" => "<ul><li><a href='_dot_'>...</a></li></ul>",
	"crons_message-accrualstop" => "Начисления за активность – _sum_ грн!",
	"crons_message-accruals" => "<b>_name_</b>, благодарим Вас за работу, которую Вы проводите<br>
для увеличения количества добрых дел в обществе.<br>
Благодаря <b>Вам</b> добрые дела множатся, и мир становится лучше.<br><br>
За Ваши активные действия Вам было начислено <b>_sum_ баллов</b> внутренней валюты.<br>
Полный список начислений можно посмотреть <a href='http://wayofhelp.com/'>тут</a>.<br><br>
Для того, чтоб <b>перевести</b> внутреннюю валюту в <b>реальные деньги</b>,<br>необходимо синхронизировать свой аккаунт с одной из социальных сетей.<br><br>Просим отнестись с пониманием к этой процедуре. Нам нужно быть уверенными,<br>что Ваш аккаунт настоящий. Мы переводим Вам реальные деньги.<br><br>
Зайдите в <a href='http://wayofhelp.com/cabinet' >Мой кабинет</a> (нужно быть авторизованым) и нажмите на одну из иконок<br>социальных сетей под словами <b>«Привязать аккаунт к социальной сети»</b>.<br><br>
После успешной модерации деньги зачисляться на Ваш проект.<br><br>Вы так же сможете заходить на портал wayofhelp с помощью тех социальных сетей,<br>которые привязали. Система будет воспринимать Вас как одного пользователя.<br><br>
Если в течении <b>3 дней</b> Вы не синхронизируете аккаунт, то деньги на Ваш проект<br>переведены <b>не будут</b>, а вернуться в фонд портала wayofhelp.<br>",
	"crons_message-presuccess" => "Ваш бонус прошел модерацию!",
	"crons_message-premoder" => "Ваш <a href='_bon_'>бонус</a> прошел модерацию и размещен на портале wayofhelp.<br>
<b>Посмотрите, как он выглядит на нашей витрине бонусов</b>.<br>
<br><br>
Вы получите уведомление, когда кто-то выиграет Ваш бонус!!!
<br><br>
Спасибо за Вашу активность и участие в увеличении количества добрых дел!
Благодаря Вам мир становится лучше!!!
<br><br>",
	//PROJECT TO CLOSE
	"crons_message-closeprojtitle" => "Внимание! Истекает срок Вашего проекта!",
	"crons_message-closeproject" => "Срок Вашего проекта <a href='_proj_'>_title_</a> истекает.<br>
Проект будет автоматически остановлен _timestop_ <br><br>
После остановки, проект безвозвратно отправится в архив.<br><br>
Если проект направлен на сбор денег, и на момент истечения срока соберет меньше 60%, то средства с<br>
проекта будут распределены на другие проекты портала <a href='http://wayofhelp.com/'>wayofhelp</a>.<br><br>
Вы можете продлить срок Вашего проекта. Для этого <a href='http://wayofhelp.com/'>авторизуйтесь</a> на сайте, в личном кабинете откройте<br>
вкладку  «Нуждаюсь в помощи», напротив нужного проекта нажмите \"Править\" и установите желаемый срок. 
",

	//SUCCESS PROJECT MESS TO OWNER
	"own_proj-succtit" => "УРА!!! Ваш проект успешно завершился!",
	"own_proj-succdesc" => "Согласно <a href='_hr_'>правилам портала</a> wayofhelp, при удачном закрытии проекта,<br>его владельцу необходимо перевести _perc_ % от собранной суммы<br>и сделать <a href='_cab_'>репост</a> об удачном завершении проекта.<br><br>Мы помогаем Вам, Вы – нам.  Все Ваши средства пойдут<br>
на развитие проекта и увеличение количества добрых дел.<br>
",
	// TREE SHARE MESS
	"tree_share-tit" => "Дерево добрых дел пользователя _name_ _fname_",
	"tree_share-descr" => "Кол-во добрых дел: _d1_. Спровоцировано добрых дел: _d2_",
	"tree_shareogtit" => "_name_ сделал _co_ добрых дел.",
	"tree_shareogdescr" => "Его добрые дела спровоцировали еще _co_ добрых дел. Заходи и смотри.",

	// SEND REGISTRATION MESS
	"reg_messtit" => "Регистрация на сайте _hr_ ",
	"reg_messdescr" => "<Вы только что зарегистрировались на сайте _hr_<br> Ваши данные для входа:
<br>Логин: _log_
<br>Пароль: _pass_
<br>Для того, чтобы активировать свою учетную запись и подтвердить корректность введенных данных, Вам необходимо пройти по ссылке ниже: 
<br>_hract_ ",

	"bonus_winmsg" => "Здравствуйте. Пользователь <a target='_blank' href='_hr_' > _name_ _fname_ </a> выиграл Ваш подвешенный бонус. <br> Пожалуйста, выполните свои обязательства по предоставлению выигрыша.",

	"bonview_boncat" => "Каталог бонусов",
	"bonview_bonhide" => "скрыть",
	"bonview_bonshow" => "показать",
	"bonview_bonnocomm" => "Никто еще не оставил комментарии к этому проекту",
	"bonview_nolog" => "Вы не авторизованы",
	"bonview_nologcomm" => "Если вы хотите оставлять комментарии, пожалуйста,",
	"bonview_nologon" => "авторизуйтесь",
	"bonview_nouser" => "Если вы не имеете учётной записи, вы должны",
	"bonview_register" => "зарегистрироваться",

	"view_ang-day" => "Ангел дня",
	"view_ang-week" => "Ангел недели",

	"cabinet_noselected" => "Не выбрано",
	"cabinet_needhelp" => "Попросил услуги",
	"cabinet_givehelp" => "Предлагаю услуги",
	"cabinet_redacted" => "Править профиль",
	"cabinet_mytree" => "Мое дерево",
	"cabinet_takemyhelp" => "полученная<br>мной помощь",
	"cabinet_givemyhelp" => "оказанная<br>мной помощь",
	"cabinet_assist" => "Окажу помощи",
	"cabinet_makeassist" => "Оказать<br>помощь",
	"cabinet_needassist" => "Нужна помощь",
	"cabinet_takeassist" => "Получить<br>помощь",
	"cabinet_adminmsg" => "Сообщение<br>администратору",
	"cabinet_next" => "Далее",
	"cabinet_open-your-tree" => "Раскрывай свое Дерево",
	"cabinet_good-deeds" => "Добрых Дел и делись",
	"cabinet_they-friend" => "им с друзьями:",
	"cabinet_hobbies" => "Править интересы",

	"catalog_bonus" => "Бонус",
	"catalog_makeproject" => "Создать свой проект",
	"catalog_all-country" => "Все страны",
	"catalog_all-region" => "Все регионы",
	"catalog_all-city" => "Все города",
	"catalog_collected-money" => "Собрано денег",

	"err404_no-page" => "К сожалению, запрашиваемая Вами страница не найдена.",
	"err404_search-info" => "Чтобы найти интересующую Вас информацию, предлагаем следующие пути:",
	"err404_main-page" => "Главную страницу",
	"err404_making-help" => "Оказание помощи",
	"err404_taking-help" => "Получение помощи",
	"err404_walk-to" => "перейти на",
	"err404_makeproj-in" => "создать проект на",
	"err404_helper" => "Подсказчик",

	"footer_rights" => "Правила участия",
	"footer_howitwork" => "Как это работает",
	"footer_good-proj" => "Удачные проекты",
	"footer_rights-sec" => "Правила безопасности",
	"footer_register" => "Регистрация",
	"footer_mycab" => "Мой кабинет",
	"footer_watch-video" => "Смотреть видео",
	"footer_good-deal-tree" => "Дерево добрых дел",
	"footer_rating-status" => "Рейтинг и Статусы",
	"footer_help-cat" => "Категории помощи",
	"footer_know-more" => "Узнать больше",
	"footer_contact" => "Связь с нами",
	"footer_ofus" => "О нас",
	"footer_contacts" => "Контакты",
	"footer_support" => "Центр поддержки",
	"footer_questions" => "Задать вопрос",
	"footer_crew" => "Команда",
	"footer_other" => "Другое",
	"footer_good-wall" => "Добрая стена",
	"footer_complaints" => "Жалобы",
	"footer_sanctions" => "Штрафные санкции",
	"footer_up-projrating" => "Как повысить рейтинг проекта",
	"footer_up-userrating" => "Как повысить рейтинг пользователя",
	"footer_start" => "Начать",
	"footer_add-fb" => "Присоединяйся к нам на Facebook",
	"footer_copyright" => "wayofhelp.com - Все права защищены",

	"header_project-finish" => "Ваш проект скоро заканчивается. <br> Он будет автоматически <br> остановлен <br>",
	"header_up-power" => "Ваша <b>Сила</b> за время последней сессии пополнилась на",
	"header_eweryday-back" => "за ежедневные посещения",
	"header_visit" => "за посещение портала",
	"header_dream-like" => "за оценку чужой мечты",
	"header_for-givehelp" => "за оказанную помощь",
	"header_for-comment" => "за комментарий",
	"header_for-repost" => "за репост",
	"header_for-angday" => "за статус <b>Ангел дня</b>.",
	"header_for-angweek" => "за статус <b>Ангел недели</b>.",
	"header_for-bonusadd" => "за <b>подвешивание бонуса</b>.",
	"header_new-dream" => "У нас на сайте появилась новая мечта",
	"header_new-dream-like" => "оцените </a> ее значимость и<br>заработайте дополнительную<br>силу.",
	"header_new-status" => "Вам присвоен новый статус",
	"header_for-activity" => "За Вашу активность и неуклонную тягу к добрым делам,<br>",
	"header_your-newstatus" => "Вам присвоен новый статус",
	"header_more-gooddeal" => "<br><br>Благодаря Вам добрых дел становится больше!!!<br><br>",
	"header_go-to" => "Перейдите по",
	"header_link" => "ссылке",
	"header_share-it" => "и поделитесь этой замечательной<br>новостью с друзьями. ",
	"header_to-newstatus" => "До статуса",
	"header_needed" => "не хватает",
	"header_earn-angelits" => "Как заработать ангелиты",
	"header_project-finished" => "<b>Поздравляем!</b> Ваш проект собрал <br> необходимую сумму. <br> Для того, чтоб получить деньги, <br>нужно",
	"header_share" => "поделиться",
	"header_share-in-soc" => "проектом в <br> своих соц. сетях.<br>",
	"header_your-position" => "Ваша позиция в рейтинге:",
	"header_up-rating" => "Увеличить рейтинг",
	"header_show-count" => "Количество показов вашего проекта:",
	"header_show-up" => "Поднять количество показов",
	"header_new-msg" => "Новых сообщений",
	"header_new-offer" => "Новых предложений",
	"header_new-offer-think" => "Новых запросов на вещи",

	"info-about_watch-how" => "Посмотри как",
	"info-about_your-name" => "Ваше имя",
	"info-about_your-phone" => "Ваш номер телефона",
	"info-about_what-can-help" => "Чем вы можете помочь?",
	
	"info-angels_angel-day-week" => "Ангел дня и недели",
	"info-angels_ang-day" => "Ангел дня, характеризует пользователя как одного из 5 лучших добродетелей ДНЯ. Рассчитывается как набранная СИЛА за сутки, либо за отдельные особые заслуги (на усмотрение администратора платформы)",
	"info-angels_ang-week" => "Ангел недели, характеризует пользователя как одного из 5 лучших добродетелей НЕДЕЛИ. Рассчитывается как набранная СИЛА за неделю, либо за отдельные особые заслуги (на усмотрение администратора платформы).",

	"info-project_how-makeproj" => "Как правильно создать проект",
	"info-project_instrustion-1" => "1. Придумайте своему проекту яркое, понятное и привлекательное название, которое отражало бы суть и выделялось среди других.",
	"info-project_instrustion-2" => "<br><br>2. Сделайте понятное описание, примерно в нескольких предложениях. Пишите грамотно и убедительно, ваша цель - вызвать в людях желание помогать Вашему проекту.
            <br>Не допускается копирование текстов из интернета - ваше описание должно быть уникальным.",
	"info-project_instrustion-3" => "<br><br>3. Выберите соответствующую категорию помощи в зависимости от того направлен Ваш проект на сбор денег или на помощь делами.",
	"info-project_instrustion-4" => "<br><br>4. Если проект направлен на сбор денег, то укажите сумму, которую Вы хотите собрать. Учтите при этом, что снять деньги можно только, когда собрано как минимум 60% от указанной суммы.
            <br>Наш совет: не ставьте слишком большую сумму (люди охотнее помогают проектам, которые в близком будущем могут успешно закончиться).
            <br>Лучше после успешного окончания одного проекта, запустить другой и продолжить сбор.",
	"info-project_instrustion-5" => "<br><br>5. Не ограничивайте срок проекта. Если помощь нужна срочно, напишите об этом в описании. После закрытия проекта по сроку его уже нельзя будет продлить.
            <br>И если Вы не соберете 60% до этого времени, то деньги не будут вам выплачены, а будут переданы на другие проекты с похожей тематикой.",
	"info-project_instrustion-6" => " <br><br>6. Выберите для проекта красивое и привлекательное фото (можно несколько), подходящее по теме.
            <br>Даже если деньги собираются на лечение человека, фото должно быть позитивным (например, фото этого человека, на котором он здоров и благополучен).
            <br>Не допускается использование фото больных людей, больных частей тела, шоковых и некачественных фото.
            <br>Лучше всего, если фото будет уникальным, т.е. не использовавшимся ранее нигде в интернете. Это повысит посещаемость вашего проекта.",
	"info-project_instrustion-7" => "<br><br>7. Обязательно сразу привяжите свой аккаунт к соцсетям (в личном кабинете) - без этого Вам не будут начисляться баллы и деньги за активность.",
	"info-project_instrustion-8" => "<br><br>8. Чтобы отредактировать проект, нужно зайти в свой профиль, нажать \"Нуждаюсь в помощи\" и напротив названия проекта выбрать \"править\".",
	"info-project_instrustion-9" => "<br><br>9. Проекты, противоречащие правилам, не проходят модерацию и удаляются.",
	
	"info-projectpract_example" => "Практический пример привлечения средств на свою кампанию.",
	"info-projectpract_content" => "Содержание",
	"info-projectpract_hide" => "убрать",
	"info-projectpract_show" => "показать",
	"info-projectpract_decor" => "Оформление.",
	"info-projectpract_fundraising" => "Сбор средств.",
	"info-projectpract_guaranteed-shows" => "Гарантированные показы.",
	"info-projectpract_social" => "Социальные сети.",
	"info-projectpract_results" => "Итоги сбора",
	"info-projectpract_conclusions" => "Выводы",
	"info-projectpract_for-example" => "В качестве примера возьмем один успешный <a target=\"_blank\" href=\"http://wayofhelp.com/proj/view/2740/\">проект</a>. Выбрали его не случайно, поскольку изначально казалось, у него вообще нет шансов на удачный исход. Дело в том, что это был уже третий по счету сбор средств для этого человека. Предыдущие два были направлены на оплату операции и первую часть реабилитации, и в общей сложности <b>собрали 70 000 грн</b>. Т.е. круг желающих помочь  был практически выжат по-полной. Тем не менее, проект решили создавать и, как оказалось, не зря.",
	"info-projectpract_recomendation" => "Рекомендации от авторов проекта:",
	"info-projectpract_making" => "1. Оформление.",
	"info-projectpract_moder" => "Сложностей с оформлением и модерацией проекта у нас не возникло. Читайте",
	"info-projectpract_tips" => "подсказки",
	"info-projectpract_make-proj" => "при создании проекта и помните, что у правильно оформленного проекта в 2 раза больше шансов стать успешным. Если коротко, то ставьте реальные фотографии, красиво и грамотно описывайте свою проблему (оптимально, если удастся вызвать эмоции).",
	"info-projectpract_after-then" => "После того, как самый простой этап был пройден, начался непосредственный сбор средств. Успешность проекта зависит от многих параметров, но я бы выделил всего два основных: получение гарантированных показов и работа с  социальными сетями. Потрудиться в любом случае придется, но игра стоит свеч.",
	"info-projectpract_im-tell" => "Я расскажу, как это делал я, - пошагово, а общие рекомендации можно посмотреть",
	"info-projectpract_here" => "тут",
	"info-projectpract_project-added" => "Итак, проект был создан 7 сентября и получил от системы <b>300 гарантированных показов</b>. Всего у меня  981 балл за активность. 1 балл = 1 показу, но большую часть я потратил на предыдущие проекты. Тут все просто: делаешь активности – зарабатываешь баллы – получаешь гарантированные показы. Тут главное не останавливаться, а делать это периодически. За первые два дня <b>удалось собрать 600 грн</b> и, что самое главное, зацепиться за регулярные бесплатные показы, но тем не менее сбор застопорился…",
	"info-projectpract_social-work" => "Пришла пора работы со своими социальными сетями. Хотя конечно эти процессы нужно запускать параллельно и сразу, поскольку количество собранных денег тоже положительно влияет на дальнейший сбор. Тут один источник дополняет другой. В общем 15-го сентября я сделал репост своего проекта в социальных сетях.",
	"info-projectpract_earlier" => "Ранее говорилось, что это был уже по счету 3-ий сбор. Именно этим можно объяснить отсутствие результата. Я был к этому готов и начал в ручном режиме через сообщения в социальных сетях связываться с друзьями и просить их сделать репост и добавить несколько слов от себя. Таким образом мое послание должно было долететь до друзей моих друзей, а их отзыв должен был добавить доверие к самому проекту.",
	"info-projectpract_more-visits" => "Собственно так и произошло. После личных просьб, заходы на проект увеличились в разы. Из 22 личных просьб сделать репост, все 22 человека + дополнительно это сделали еще 34. Самое интересное произошло чуть позже, когда один из лидеров сборной Украины по пляжному футболу Виталий Сидоренко увидел проект и сделал <a href=\"https://www.facebook.com/photo.php?fbid=1060194300712891&set=a.429075207158140.93215.100001669929144&type=3&theater\">аукцион</a>, на котором <b>удалось дополнительно собрать 10 000 грн</b>.",
	"info-projectpract_money-auction" => "Деньги от аукциона не учитываются в он-лайн проекте, так как были переведены не через платформу, а напрямую. Так же нет данных, сколько денег было собрано офф-лайн.",
	"info-projectpract_money-ofauction" => "Итог: <b>10 000 грн</b> от аукциона + <b>6000 грн</b> на проекте + неизвестно сколько от прямых перечислений. Проект мог собрать больше, но в этом уже не было необходимости,  и его закрыли.",
	"info-projectpract_experience" => "Думаю мой опыт будет полезен людям, которые только собираются сделать проект и позволю сделать несколько своих субъективных выводов:",
	"info-projectpract_big-yours" => "Чем больше у Вас",
	"info-projectpract_points" => "баллов",
	"info-projectpract_chanse" => "при создании проекта, тем больше шансов на старте разогнать проект и получить больше показов.",
	"info-projectpract_row-social" => "Без продвижения проекта в социальных сетях, вряд ли удастся быстро собрать необходимую сумму. Хотя на больших промежутках времени это безусловно реально.",
	"info-projectpract_ask-friend" => "Нужно обращаться с просьбой к тем друзьям, у которых больше всего контактов в социальных сетях.",
	"info-projectpract_make-repost" => "Делать репосты во всех социальных сетях, в которых вы зарегистрированы.",

	"info-riseproject_instruction" => "Инструкция по продвижению своего проекта на WayofHelp.",
	"info-riseproject_many-questions" => "Мы получаем много вопросов о том, как правильно продвигать проект на нашей платформе и существуют ли эффективные методы продвижения проекта среди незнакомых людей.<br>
        Перед тем, как перейти к подробному описанию самого верного пути к успешному продвижению своего проекта, ознакомьтесь пожалуйста с некоторыми фактами: ",
	"info-riseproject_repost" => "Репост (sharing) своего проекта в социальных сетях (Фейсбук, Вконтакте, Одноклассники), увеличивает пожертвования на 400%.",
	"info-riseproject_first-stage" => "На начальной стадии большинство пожертвований идут непосредственно от людей, которые Вас знают или имеют отношение к вашему кругу друзей.",
	"info-riseproject_self" => "Своими",
	"info-riseproject_active-actions" => "активными действиями",
	"info-riseproject_can-increase" => "можно увеличить пожертвования на свой проект на 300%.",
	"info-riseproject_successful-projects" => "Большинство успешных проектов на WayofHelp, прежде чем стать успешными и получить внимание прессы, сначала получили поддержку у своего близкого круга.",
	"info-riseproject_strangers" => "Незнакомые люди жертвуют  проекту деньги гораздо охотнее, если он получил предварительную поддержку от своего круга друзей.",
	"info-riseproject_properly-designed" => "Правильно оформленный проект собирает в 2 раза больше пожертвований.",
	"info-riseproject_thus" => "Таким образом, для того чтобы проект стал популярным и распространился в сети и медиа, необходимо получить первоначальные пожертвования. Добиться этого можно двумя способами:<br>",
	"info-riseproject_share-soc" => "Поделиться проектом в своих социальных сетях и продвинуть его в своем кругу знакомых;",
	"info-riseproject_free-shows" => "Получить много бесплатных показов в ТОПе платформы за свои",
	"info-riseproject_active-action" => "активные действия",
	"info-riseproject_some-try" => "Есть несколько эффективных способов увеличить сбор пожертвований в вашем сообществе.",
	"info-riseproject_your-social" => "1. Подготовьте вашу социальную сеть.",
	"info-riseproject_remember" => "Помните, Ваша главная цель - собрать деньги на свой проект. Посторонние люди начнут помогать после того, как помогут Ваши друзья.<br>
        Не все в вашем кругу друзей будут знать о платформе  WayofHelp и как через нее пожертвовать на Ваш проект. Поэтому очень важно обратиться к ним лично, перед тем как распространять свой проект на Facebook, Вконтакте или Одноклассниках. Нужно рассказать им о предстоящем сборе средств.<br>
        Придумайте увлекательную историю и причину по которой Вам необходима их поддержка. Можно каждому написать сообщение по электронной почте или позвонить лично. Это займет какое-то время, но поверьте, это того стоит.",
	"info-riseproject_say-thanks" => "2. Благодарите людей, которые Вам помогли.",
	"info-riseproject_public-thanks" => "3. Публично поблагодарите в социальных сетях каждого человека, который внес свой вклад в ваш проект. Это не только  позволит людям  почувствовать радость от своей хорошей инициативы, но и даст дополнительное продвижение Вашему проекту!<br>
        Вы всегда будете в курсе, кто Вам помог. После каждой полученной помощи вы будете получать уведомление по электронной почте. Переходите по ссылке в вашем письме, и нажимайте кнопку «Поблагодарить». Это очень важно, дать понять человеку, что его помощь очень важна для Вас. К тому же, есть большая вероятность, что он поможет еще раз.",
	"info-riseproject_pray" => "Вы должны попросить, чтобы получить.",
	"info-riseproject_not-expect" => "Не ожидайте, что люди будут репостить Ваш проект самостоятельно. Попросите их поделиться им, а еще лучше – попросить лично поручиться за сбор средств. Так у Вас появится шанс привлечь пожертвования от второй ступени друзей Вашего круга.<br>
        Эта же тактика применима и к просьбе о пожертвовании. Не бойтесь попросить пожертвование напрямую у людей из вашего круга, ведь в самом худшем случае Вам просто не помогут. Хотя статистика говорит об обратном: 80% людей помогут, если их попросить напрямую.",
	"info-riseproject_mind-local" => "4. На старте думайте локально.",
	"info-riseproject_first-then" => "Перед тем, как думать о широкой аудитории, подумайте о локальной. Это поможет получить первые пожертвования и распространить Ваш проект.<br>
        Например, если вы состоите в какой-нибудь организации, то можете попросить их распространить информацию о Вас.<br>
        Кроме того, можно обратиться к местной газете вашего города и поделиться своей историей.",
	"info-riseproject_top-portal" => "5. Параллельно продвигайте проект в ТОП портала.",
	"info-riseproject_many-visitors" => "Портал WayOfHelp  ежедневно напрямую посещает огромное число пользователей, готовых помочь. Ваш проект должен привлечь их внимание. Для этого нужно:<br>",
	"info-riseproject_right" => "Правильно",
	"info-riseproject_issue-project" => "оформить</a> проект;",
	"info-riseproject_enough-impressions" => "Получить достаточное количество показов в ТОПе (достигается активными действиями на портале).<br>
        Больше показов – больше шансов быть увиденными и получить помощь.",
	"info-riseproject_step-by-step" => "6. Делайте все по порядку.",
	"info-riseproject_above" => "Исходя из вышеперечисленного, можно сделать вывод, что любое время, потраченное на продвижение своего проекта альтернативными способами, будь то размещение на досках объявлений или работа с журналистами, вероятно, не будет иметь никакого влияния на ваш проект, если вы не сделали первоначальную ставку на:<br>
        вашу собственную социальную сеть;<br>
        продвижение своего проекта в ТОП портала.",
	"info-riseproject_im-think" => "Я думаю, что после того, как вы проделаете эту работу и увидите, что Ваша история интересна, будет целесообразно двигаться дальше и делиться ею с журналистами. Только так можно достичь успеха. Во всяком случае наша статистика говорит именно об этом.",

	"info-rules_rules" => "Правила пользования платформой WayofHelp",
	"info-rules_sanction" => "Санкции",
	"info-rules_punitive-sanction" => "Штрафные<br>санкции",
	"info-rules_protection" => "Защита",
	"info-rules_safity-rule" => "Правила<br>безопасности",
	"info-rules_project-rating" => "Рейтинг проекта",
	"info-rules_rating-up" => "Как повысить<br>рейтинг проекта",
	"info-rules_user-rating" => "Рейтинг пользователя",
	"info-rules_user-ratup" => "Как повысить<br>рейтинг пользователя",

	"info-tree_see-clearly" => "Посмотреть наглядно",
	"info-tree_tree" => "Дерево",

	"info-work_how-it-work" => "Как это работает",
	"info-work_help-anyone" => "Что бы оказать кому-либо помощь на нашем сайте, Вы можете воспользоваться тематическим каталогом, в котором размещены все запросы наших посетителей.<br>Если вы еще не создали себе учетную запись, то зарегистрируйтесь у нас на сайте или войдите с помощью своего аккаунта в социальной сети.",
	"info-work_fizical" => "Физические лица",
	"info-work_db-requests" => "База запросов помощи",
	"info-work_im" => "Я",
	"info-work_my-tree" => "Мое дерево",
	"info-work_couch" => "Помогите<br>перенести<br>диван",
	"info-work_consult" => "Окажите<br>юридическую<br>консультацию",
	"info-work_surgery" => "Помогите<br>собрать деньги<br>на операцию",
	"info-work_make-help" => "Окажу помощи",
	"info-work_making-help" => "Оказать<br>помощь",
	"info-work_good-deeds" => "Твои добрые дела увидят ВСЕ",
	"info-work_status-up" => "Твой статус вырастет",
	"info-work_angel" => "Ангел",
	"info-work_shows-tree" => "Ты увидишь как растет твое<br>собственное Дерево Добра",
	"info-work_need-help" => "Нужна помощь",
	"info-work_take-help" => "Получить<br>помощь",
	"info-work_take-help-deeds" => "Получай помощь деньгами<br>и делом",
	"info-work_make-dreams" => "Осуществляй мечты",
	"info-work_influence" => "Влияй сам на продвижение<br>своего проекта",
	"info-work_mytree-help" => "Мое дерево<br>помощи",
	"info-work_potential-good" => "Потенциальные<br>добрые дела тех,<br>кому я помог",
	"info-work_want" => "Желаем вам приятной навигации по сайту и надеемся, что вы найдете того, кому захотите помочь.",

	"infoother_social" => "Социальная платформа",
	"infoother_users" => "Пользователи",
	"infoother_charity" => "Благотварительность",
	"infoother_project" => "Проект",
	"infoother_help" => "Помощь",
	"infoother_user" => "Пользователь",

	"login_site-login" => "Вход на сайт",
	"login_pass-reset" => "Восстановление пароля",
	"login_session-lost" => "Сессия окончена",
	"login_mail-msg" => "Вам отправлено письмо",
	"login_toyour-mail" => "На ваш электронный ящик отправлено письмо с ссылкой для смены пароля.",
	"login_pass-changed" => "Пароль изменен",
	"login_youare-changepass" => "Вы только что изменили пароль на свою учетную запись.",
	"login_complete-data" => "Заполните данные для восстановления пароля",
	"login_new-pass" => "Новый пароль:",
	"login_repeat-pass" => "Повторить пароль:",
	"login_restore-pass" => "Восстановить пароль",

	"main_fontitle" => "Фон WayOfHelp",
	"main_helping-others" => "Помоги другому",
	"main_help-myself" => "или получи помощь сам!",
	"main_what-do" => "Что делать мне?",
	"main_registered" => "Зарегистрируйся",
	"main_start-your-tree" => "Запусти свое Дерево добрых дел<br>и смотри, как оно растет",
	"main_how-been" => "КАК ЭТО БУДЕТ?",
	"main_successful-proj" => "Удачные проекты",
	"main_tell-your-friends" => "Расскажи своим<br>друзьям",
	"main_and-then" => "А потом...",
	"main_bonus" => "Подвешенные бонусы",
	"main_video-btn" => "Кнопка видео",
	"main_more-ofus" => "Подробнее о нас",
	"main_reviews" => "Отзывы о полученной помощи",
	"main_user-photo" => "Фото пользователя",
	"main_total-activity" => "Самые активные",
	"main_active-user-photo" => "Фото активного пользователя",
	"main_popular-project" => "Популярные проекты",

	"myprojects_charitable" => "Благотворительный взнос",
	"myprojects_payed" => "Оплатить",
	"myprojects_waiting" => "Ждет подтверждения",
	"myprojects_rating" => "Рейтинг",
	"myprojects_accept" => "Принять",

	"project_small-photo" => "Это фото слишком маленькое, пожалуйста загрузите другое.",
	"project_size" => "Разрешение фото должно быть больше чем 400x300",
	"project_no-more-project" => "Нельзя создать больше одного денежного проекта. <br> Для этого приостановите ваш текущий денежный проект<br>",
	"project_pay-method" => "Выберите одну из перечисленых систем оплаты",
	"project_checked" => "Проверено",
	"project_complate-proj" => "Как правильно заполнить проект",
	"project_servise" => "Сервиc добрых дел",
	"project_hide" => "скрыть",
	"project_no" => "Нет",
	"project_yes" => "Да(можно выбрать сколько угодно категорий):",
	"project_send" => "Отправить",
	"project_login" => "Войти",
	"project_answer" => "Ответить",
	"project_no-login" => "Вы не авторизованы",
	"project_if-you-want" => "Если вы хотите оставлять комментарии, пожалуйста,",
	"project_make-login" => "авторизуйтесь",
	"project_if-nouser" => "Если вы не имеете учётной записи, вы должны",
	"project_registered" => "зарегистрироваться",
	"project_watch-with-this" => "С этим проектом чаще всего смотрят",
	"project_login-first" => "Сначала войдите или зарегистрируйтесь!",
	"project_point-comm" => "балл за оставленный <br><b>комментарий</b> <br>",

	"project-prepay_portal-terms" => "По условиям портала WayOfHelp",
	"project-prepay_participate" => "<br>Для участия Вашего проекта в распределении внутреннего фонда портала, равного",
	"project-prepay_uah" => "грн",
	"project-prepay_every-day" => "каждый день) и ежедневного получения средств на свой проект (согласно вашего дневного рейтинга в совершении добрых дел), необходимо <b>разово</b> внести на фонд портала",
	"project-prepay_money" => "Эти деньги будут потрачены на проекты пользователей.<br>
        <br>Зарабатывайте внутреннюю валюту своими активными действиями и конвертируйте ее в реальные деньги на свой проект. <br>",
	"project-prepay_read-more" => "Подробнее читайте",
	"project-prepay_here" => "тут",
	"project-prepay_money-table" => "Таблицу распределения денег на проекты смотри",
	"project-prepay_all-your-money" => "Все Ваши деньги идут на помощь другим проектам.",
	"project-prepay_good-tree" => "Ваше дерево добрых дел будет расти.",
	"project-prepay_good-job" => "WayOfHelp – увеличиваем количество добрых дел!!!",
	"project-prepay_updating-account" => "Пополнение счета проекта",
	"project-prepay_payed" => "Оплатить",
	"project-prepay_suc-launched" => "Ваш проект успешно запущен!",
	"project-prepay_under-terms" => "По условиям портала WayOfHelp",
	"project-prepay_to-participate" => "Для участия Вашего проекта в распределении внутреннего фонда портала, равного",
	"project-prepay_uah-to-project" => "грн. на любой проект (один или несколько). <br>
                <br>Зарабатывайте внутреннюю валюту своими активными действиями и конвертируйте ее в реальные деньги на свой проект. <br>",

	"registration_reg-on-site" => "Регистрация на сайте",
	"registration_edit-profile" => "Редактирование профиля",
	"registration_my-cabinet" => "Мой кабинет",
	"registration_step-3" => "Шаг 3. - Учетная запись активирована",
	"registration_step-2" => "Шаг 2. - Регистрация завершена",
	"registration_way-of-help" => "В каких направлениях вы готовы оказывать помощь?",
	"registration_reg-document" => "Загрузка регистрационных документов",
	"registration_certificate" => "Копия свидетельства:",
	"registration_step-1" => "Шаг 1. - Контакты и регистрационные данные",
	"registration_reg-data" => "Контакты и регистрационные данные",
	"registration_employees" => "Сотрудников:",
	"registration_agree" => "Я соглашаюсь с",
	"registration_rules" => "правилами пользования",
	"registration_undertake" => "и обязуюсь не нарушать эти правила при работе на портале WayOfHelp.com",
	
	"siterules_rule-concept" => "Правила и основные понятия",
	"siterules_independent" => "независимой социальной онлайн платформы <b>WayofHelp</b>",
	"siterules_rules-govern" => "Данные правила регулируют основные принципы взаимоотношений между пользователями",
	"siterules_platform" => "платформы WayofHelp.",
	"siterules_in-this-page" => "На этой странице вы можете ознакомиться с основными понятиями, структурой портала,",
	"siterules_learn-the-rules" => "а так же узнать правила, которые регулируют основные принципы взаимоотношений",
	"siterules_between-platform" => "между пользователями платформы.",
	"siterules_about-project" => "О проекте",
	"siterules_in-outline" => "в общих чертах",
	"siterules_concept" => "Основные понятия",
	"siterules_category" => "Категории",
	"siterules_users" => "пользователей и их",
	"siterules_less" => "возможности",
	"siterules_principles" => "Принципы",
	"siterules_visual" => "визуализации добрых",
	"siterules_good-job" => "дел пользователя",
	"siterules_work-rule" => "Правила пользования",
	"siterules_platforms" => "платформой WayofHelp",
	"siterules_donor" => "Донорам",
	"siterules_stat" => "Отчетность по",
	"siterules_charity" => "благотворительному",
	"siterules_proj" => "проекту",
	"siterules_pol" => "Политика",
	"siterules_confident" => "конфиденциальности",
	"siterules_fees-pay" => "Сборы и плата",

	"successfull_success-project" => "Здесь представлены проекты, которые завершились успешно. Благодаря Вам, их может стать еще больше.",
	"successfull_uah" => " грн. ",
	"successfull_rub" => " руб.",
	"successfull_take-think" => "Забрали вещи",
	"successfull_people-help" => " Людей помогло",
	"successfull_people" => " чел.",
	"successfull_user" => "Пользователь",
	
	"user_no-check" => "Не выбрано",
	"user_time" => "раз",
	"user_share-tree" => "Поделиться деревом<br>пользователя",
	"user_open-tree" => "Раскрывай Дерево",
	"user_good-job-friend" => "Добрых Дел и делись<br>
						им с друзьями:<br>",
	
	"getpaymnt_payed" => "Оплатить напрямую пользователю",
	"getpaymnt_steps" => "1.Выберете один из расчетных счетов пользователя и окажите помощь через удобную для вас платежную систему. <br>
                2.После чего зайдите в Ваш личный кабинет и нажмите вкладку «<b>Оказываю помощь</b>». <br>
                3.Далее найдите этот проект в списке и нажмите \"<b>Я помог</b>\". <br>
                4.Сразу после этого Ваша помощь отразится на Вашем \"<b>Дереве добра</b>\"",
	"getpaymnt_payment" => "Платежная система",
	"getpaymnt_req" => "Реквизиты",
	"getpaymnt_pay-sum" => "Оплатите нужную сумму на данные реквизиты<br>удобным для Вас способом.",
	"getpaymnt_agree" => "Подтвердить",
	"getpaymnt_push-btn" => "После оплаты нажмите кнопку <b>«Подтвердить»</b>.",
	"getpaymnt_help-user" => "Помогите пользователю через наши платежные системы",
	"getpaymnt_help-tree" => "Помощь на Вашем дереве добра отразится автоматически.",

	"cabpopup_road-to-1000" => "Даже дорога в 1000 лье начинается с первого шага!",
	"cabpopup_we-offer" => "Предлагаем прямо сейчас сделать очень простой и очень <b>нужный шаг</b> в нашем проекте! <br> Поделитесь информацией о <b>WayOfHelp</b>.
    <br>Пусть о нем узнает как можно больше людей.
    <br>Это и будет Ваше <b>первое доброе дело.</b> <br>
    В результате:",
	"cabpopup_your-tree" => "Ваше Дерево Добрых Дел начнет расти (Вы увидите это в своем личном кабинете);",
	"cabpopup_power-up" => "Вам дополнительно будут начислены <b>50</b> баллов внутренней валюты.",
	"cabpopup_good-start" => "Это отличный старт!",
	"cabpopup_share" => "Поделиться:",
	"cabpopup_add-photo" => "Добавьте фото",
	"cabpopup_add-avatar" => "Добавьте аватарку(фото)",
	"cabpopup_in-profile" => "в свой профиль",

	"getbonus_take-bonus" => "Чтобы получить бонус, вы должны угадать<br>в какой из фишек дерева скрыт этот бонус.
					Бонус будет Ваш, если вы сумеете открыть нужную фишку за три попытки.<br>Удачи!",
	"getbonus_trys" => "Попыток осталось:",
	"getbonus_no-more-try" => "У вас не осталось попыток. <br> Что бы получить новую возможность выиграть бонус,<br> необходимо оказать любую помощь на проекте. <br> Больше добрых дел, - больше попыток",
	"getbonus_winner" => "Вы только что выиграли подвешенный бонус!",
	"getbonus_lose" => "Вы не смогли угадать, где спрятан подвешенный бонус.<br>",
	"getbonus_unused-chance" => "Неиспользованные возможности выиграть бонус: ",
	"getbonus_more-goodjob" => "Больше добрых дел - больше возможностей",

	"infopopup_action" => "Акция от портала добрых дел Wayofhelp!",
	"infopopup_make-project" => "Создавай проект на нашем портале и ежеднено получай деньги из фонда",
	"infopopup_exercise" => "на его осуществление",
	"infopopup_watch" => "Смотри сколько зарабатывают другие проекты",
	"infopopup_choise" => "Решай сам, сколько денег получит твой проект",

	"newreg_to-mail" => "На Ваш почтовый ящик было отправлено письмо с подтверждением регистрации. Пройдите по ссылке в письме, чтобы активировать учетную запись.",
	"newreg_help-this-proj" => "Помогая этому проекту, Вы запускаете цепочку добрых дел. Поэтому очень важно войти под своим аккауном или зарегистрироваться.",
	"newreg_helping-this-proj" => "Помогая этому проекту, Вы запускаете цепочку добрых дел.",
	"newreg_just" => "Займет всего",
	"newreg_sec" => "2 секунды",
	"newreg_err-name" => "Вы ввели неправильно имя",
	"newreg_name" => "Имя",
	"newreg_fname" => "Фамилия",
	"newreg_err-fname" => "Вы ввели неправильно фамилию",
	"newreg_err-mail" => "Вы ввели неправильно почту или такая уже существует",
	"newreg_err-pass" => "Вы ввели неправильно пароль",
	"newreg_pass" => "Пароль",

	"bonus-cont_call-help" => "Запросы о помощи",

	"catcont_success" => "Успешные проекты",
	"catcont_call-help" => "Запросы о помощи",

	"reg_err-telephone" => "Телефон указан с ошибками",

	"cabcont_err-mail" => "Вы не указали свой E-mail/Логин",

	"cabcont_mistake-mail" => "E-mail/Логин указан с ошибками",
	"cabcont_already-registered" => "Такой E-mail/Логин уже зарегистрирован. Укажите другой адрес электронной почты.",
	"cabcont_your-city" => "Укажите ваш основной город",
	"cabcont_required-fields" => "Не все обязательные поля заполнены.",
	"cabcont_scope-published" => "Ваша оценка с отзывом была опубликована.",
	"cabcont_error" => "Произошла ошибка при публикации вашей оценки с отзывом.",
	"cabcont_my-profile" => "Мой профиль",
	"cabcont_error-msg" => "Ошибка отправки сообщения. Не найден адресат.",
	"cabcont_error-sending" => "Ошибка отправки сообщения. Вы не можете отправлять сообщения сами себе.",

	"inst-cont_code-lang" => "Не передан код языка",

	"login-cont_pass-no-same" => "Пароли отличаются или пустые. Введите корректные пароли.",

	"payscallback_mistake-txt" => "Ошибочная подпись: ",
	"payscallback_ok-txt" => "Подпись ок: ",

	"projcont_no-title" => "Вы не указали заголовок вашего проекта",
	"projcont_no-catalog" => "Вы не указали в какой раздел размещать проект",
	"projcont_no-city" => "Вы не указали город для проекта",
	"projcont_data-err" => "Дата старта проекта указана с ошибками",
	"projcont_many-help" => "Объем помощи должен быть значением больше нуля.",
	"projcont_biggest-zero" => "Объем помощи должен быть допустимым числовым значением и больше нуля.",
	"projcont_born-code" => "Ваш код подтверждения: ",
	"projcont_callback" => "Код подтверждения отправлен на номер ",

	"regcon_pass-no-same" => "Введенные пароли не совпадают",
	"regcon_no-pass" => "Вы не ввели пароль для вашей учетной записи",
	"regcon_pass-size" => "Пароль должен быть минимум 6 символов",
	"regcon_region" => "Укажите ваш регион",
	"regcon_site-rule-agree" => "Подтвердите что вы принимаете правила пользования сайтом",
	"regcon_code-controll" => "Контрольный код указан неправильно",
	"regcon_reg-error" => "Ошибка регистрации",
	"regcon_set-you-city" => "Укажите город, в котором вы живете",
	"regcon_email-mistake" => "E-mail указан с ошибками",

	"agreement_tytle" => "Повышение вероятности сбора средств",
	"agreement_you-agree" => "Вы соглашаетесь с тем, что Ваш проект может рекламировать любой партнер нашего портала, будь то медиа агенство, электронный журнал, портал, сайт или частное лицо через свои каналы и источники трафика.",
	"agreement_any-sum" => "Любая сумма, полученная на проект от рекламы партнеров, будет выплачиваться за вычетом 20% (закупка рекламных мест).",
	"agreement_help" => "Помощь, полученная органическим путем, или по обычной ссылке, будет выплачиваться согласно правилам и тарифам платформы  wayofhelp.",
	"agreement_example" => "<b>Приведем пример:</b><br><br>
					Ваш проект получил 50 руб. по партнерской ссылке и 200 руб. по обычной. Сумму, которую вы получите будет считаться по формуле: 250 – (50*20% + 200*5%) = 230 руб.",
	"agreement_instruction" => "Вы можете увеличить необходимую сумму сбора проекта на 15%,
или убрать «галочку» с пункта «Реклама»  и самим попробовать продвинуть свой проект. Для этого воспользуйтесь инструкцией: <ul> <li><a href='http://wayofhelp.com/info/projectrise/'>Инструкция по продвижению своего проекта на WayofHelp.</a></li> <li><a href='http://wayofhelp.com/info/projexample/'>Практический пример привлечения средств на свою кампанию.</a></li></ul>",
	"agreement_header" => "Данный пункт повышает вероятность сбора средств на 80%.",

	//---------------------------------------ПІСЛЯ ПЕРЕВОДУ-----------------------------------
	
	"footer_you-see" => "Вы смотрели",
	"footer_order" => "заказ",

	"project_advertising" => "Реклама",
	"project_man" => "Чел.",
	"project_good-serv" => "Сервис добрых дел",
	"project_make-take" => "Оказывай и получай помощь",
	"project_of" => "из",
	"project_scope-dream" => "балл за оценку мечты.",
	"project_scope-comm" => "балл за оставленный <br><b>комментарий</b> <br>",
	
	"reqgretir_tit" => "Поблагодарите пользователя за оказаную помощь",
	"rateinfo_tab-pos" => "Позиция",
	"rateinfo_tab-info" => "Информация о пользователе",
	"rateinfo_tab-rate" => "Рейтинг",
	"rateinfo_сurr-pos" => "Вы находитесь на последнем месте в рейтинге",
	"rateinfo_city-pos" => "Позиция в моем регионе",
	"rateinfo_reg-pos" => "Позиция в общем рейтинге",
	"rateinfo_txt-gt" => "Добрых дел",
	"rateinfo_u-pos" => "Ваша текущая позиция в рейтинге",
	"rateinfo_u-uprate" => "Вы можете поднять рейтинг",
	"rateinfo_u-uprategr" => "Вступить в группу",
	"rateinfo_u-upratesoc" => "(+ 20 баллов за каждую соц. сеть)",
	"rateinfo_u-uprate" => "Вы можете поднять рейтинг",
	"rateinfo_gr-done" => "Вы уже подписаны на группу WayOfHelp",
	"rateinfo_invite-friend" => "Привести друга",
	"rateinfo_invite-friendi" => "(+ 30 баллов за каждого друга)",
	"rateinfo_u-refid" => "Ваша реф. ссылка",
	"rateinfo_u-refid-expl" => "Скопируйте ссылку и дайте ее свои друзьям. (Ее нужно вставить в URL браузера)",
	"rateinfo_share-proj" => "Поделиться<br>другими проектами",
	"rateinfo_help-proj" => "Помочь другим<br>проектам",
	"rateinfo_all-proj" => "Еще...",
	"rateinfo_tip-subs" => "баллов за <b>подписку</b>",

	"projview_u-tree" => "Дерево пользователя",
	"projview_u-ratecomm" => "Спасибо",

	"projpopup_fpop-txt" => "Человек, который много помогает другим,<br>
        теперь сам нуждается в вашей помощи.<br><br>
        Любая помощь, включая репост его проекта в социальных сетях,<br>
        очень поможет этому пользователю.<br><br>
        Делайте добрые дела вместе с нами!",
	"projpopup_fpop-help" => "Помочь",
	"projpopup_fpop-share" => "Поделиться",
	"projpopup_tpop-text" => "Ваш репост может сильно помочь этому проекту!",
	"projpopup_tpop-text-next" => "Еще не готовы оказать помощь?<br><br>Вы можете дать большой толчок этому проекту,<br>просто поделившись в социальных сетях ",

	"projcreate_date" => "Дата",
	"projcreate_today" => "Сегодня",

	"proj_anon-username" => "Аноним",

	"basicjs_tip-adder" => "Вам начислены",
	"basicjs_load-region" => "Укажите свой регион/область",
	"basicjs_load-city" => "Укажите свой город",
	"basicjs_login-err" => "Вы неправильно ввели логин или пароль. Вход невозможен.",
	"basicjs_checklog-err" => "Вы не указали E-mail адрес, который является логином",
	"basicjs_checkhlp-err-1" => "Введенное значение не является корректным числом.",
	"basicjs_checkhlp-err-2" => "Помощь должна быть больше нуля.",
	"basicjs_checkmsg-err" => "Сообщение должно содержать текст.",
	"basicjs_checkstar-err" => "Необходимо написать комментарий к поставленной оценке",
	"basicjs_count" => "знаков осталось",
	"pophelp_payerr" => "Вы не указали систему оплаты",

	//--------------------------------БЕЗ ПЕРЕВОДА----------------------------------------------
	"cabinet_you" => "Вы",
	"addmoney_confidential" => "Конфиденциальная и безопасная обработка платежа",
	"addmoney_payment" => "Выберите способ оплаты:",
	"addmoney_bank-card" => "Банковские карты",
	"addmoney_electrons" => "Электронные деньги",
	"addmoney_telephones" => "Со счета телефона",
	"catalog_press-bonus-out" => "Выберите бонус из каталога и попытайтесь его выиграть или
<span style='color:#e6ff0f'>предложите свой приз</span> для других пользователей.<br>
Больше бонусов – больше добрих дел",
	"catalog_press-bonus-in" => "Нажмите на кнопку «Получить бонус» и испытайте свою удачу.<br> Получайте дополнительные попутки за активность на проекте",
	"catalog_press-cat" => "Вы можете помочь проектам из каталога внизу или создайте свой<br> проект и опишите, чем Вы можете помогать",
	"catalog_press-catdo" => "Вы можете попросить помощь у проектов внизу или создать свой<br> проект на получение помощи"

);

// ENGLAND SITE VERSION

$txt_eng = [

	"pophelp_payerr" => "Please, add payment system",

	"all_logo-alt" => "JustHelp –find help",
	"all_top-btn-dohelp" => "To help",
	"all_top-btn-needhelp" => "To get help",
	"all_top-logas" => "You’ve signed in as",
	"all_top-login" => "Log in",
	"all_top-logout" => "Log out",
	"all_top-reg" => "Registration",
	"all_top-cabinet" => "My profile",
	"all_bc-main" => "Main page",

	// Index - main view
	"main_rev-projget" => "Received help",
	"main_rev-projtree" => "Show my tree",

	// Other text resources, named as 'contoller_resource'
	// ...

	///////////////////////////////////////////////////////////////////////////
	//
	// Registration process
	//
	///////////////////////////////////////////////////////////////////////////

	//
	"reg_tab-person" => "Registration of individual",
	"reg_tab-comp" => "Registration of company",
	"reg_tab-org" => "Registration of public organization",

	// Placeholders for input fields
	"reg_ph-org" => "Name of company",
	"reg_ph-orgo" => "Name of organization",
	"reg_ph-sphere" => "For example: trade, IT ",
	"reg_ph-sphere-org" => "For example: help sick people, legal services etc.",
	"reg_ph-name" => "Your name",
	"reg_ph-fname" => "Your surname",
	"reg_ph-tel" => "+38(CODE) NUMBER",
	"reg_ph-prof" => "For example: lawyer",

	// Labels for input fields
	"reg_lbl-org" => "Company",
	"reg_lbl-orgo" => "Organization",
	"reg_lbl-sphere" => " Sphere  of activity",
	"reg_lbl-login" => "Login/E-mail",
	"reg_lbl-email" => "Your E-mail",
	"reg_lbl-name" => "Your name",
	"reg_lbl-pass" => "Password",
	"reg_lbl-pass1" => "Repeat password",
	"reg_lbl-fname" => "Surname",
	"reg_lbl-tel" => "Telephone",
	"reg_lbl-country" => "Country",
	"reg_lbl-obl" => "Region",
	"reg_lbl-oblsel" => "Select your region",
	"reg_lbl-city" => "City",
	"reg_lbl-citysel" => "Select your city",
	"reg_lbl-empl" => "Employees",
	"reg_lbl-descr" => "Description",
	"reg_lbl-captcha" => "Enter code",
	"reg_lbl-avatar" => "User image",
	"reg_lbl-avatar-use" => "Use profile photo for user image",
	"reg_lbl-logo" => "Logotype",
	"reg_lbl-prof" => "Profession",
	"reg_lbl-loadphoto" => "Upload photo",
	"reg_lbl-background" => "Background of projects",

	// Registration: Error texts for input fields
	"reg_err-org" => "You did not specify name of company",
	"reg_err-orgo" => "You did not specify name of organization",
	"reg_err-email" => "You did not specify your E-mail",
	"reg_err-pass" => "Passwords shall be identical and not less than 6 symbols",
	"reg_err-sphere" => "You have to specify at least the sphere of activity",
	"reg_err-name" => "You did not specify your name",
	"reg_err-fname" => "You did not specify your surname",
	"reg_err-country" => "Specify city of your location",
	"reg_err-obl" => "Specify region of your location",
	"reg_err-city" => "Specify city of your location ",
	"reg_err-city-0" => "Specify your city",
	"reg_err-tel" => "Telephone contains errors, format +38(CODE) NUMBER",
	"reg_err-prof" => "Profession is not specified",
	"reg_err-captcha" => "Wrong control code",


	// Multinum strs
	"days_1" => "day",
	"days_2" => "days",
	"days_5" => " days ",
	"min_5" => "minutes",
	"hour_5" => "hours",
	"human" => "pers.",
	"human_1" => "person",
	"human_2" => "persons",
	"human_5" => "persons ",
	"its" => "pcs.",
	"avail" => "Available",

	"requests" => "requests",

	"time_ended" => "Term expired",

	///////////////////////////////////////////////////////////////////////////
	//
	// Popup windows
	//
	///////////////////////////////////////////////////////////////////////////

	// Login box
	"poplog_hdr" => "Enter website",
	"poplog_lbl-login" => "Email",
	"poplog_lbl-pass" => "Password",
	"poplog_restpass" => "Reset password",
	"poplog_btn-enter" => "Enter",
	"poplog_btn-reg" => "To register",
	"poplog_btn-fb" => "Login with Facebook",
	"poplog_btn-vk" => "Login with Vkontakte",
	"poplog_btn-ok" => "Login with Odnoklassniki",
	"poplog_gotoreg" => "If it is your first visit, please register as a new user",

	// Add help box
	"pophelp_hdr" => "To help",
	"pophelp_lbl-sum" => "Amount",
	"pophelp_lbl-amount" => "Number of persons",
	"pophelp_lbl-msg" => "Enter your reply here",
	"pophelp_ph_msg" => "Your comment to help",
	"pophelp_btn-send" => "Send",
	"pophelp_btn-aft1" => "Pay immediately",
	"pophelp_btn-aft2" => "Or",

	"popproj_hdr" => " How to make successful project",
	"popproj_text" => "<b class='text-align: center;'>Please read!!!</b><br>
<ul>
<li><a target=\"_blank\" href='http://wayofhelp.com/info/projexample/'>Real-world example.</a></li>
<li><a target=\"_blank\" href='http://wayofhelp.com/info/projectrise/'>Manual.</a></li>
</ul>",

	"popbon_hdr" => "",
	"popbon_text" => " The bonus will be available on the portal after moderation.",


	// Get Bonus
	"popbonus_hdr" => "Get bonus",
	"popbonus_win" => "You’ve just won the suspended bonus.",
	"popbonus_loose" => " You failed to guess where the suspended bonus is. Next try to find the bonus is available in 24 hours only.",
	"popbonus_timelimit" => " Less than 24 hours passed since you tried to get the suspended bonus. You still cannot make your next attempt.",

	// Register box
	"popreg_hdr" => "Registration on website",
	"popreg_h-pers" => "I am private individual",
	"popreg_h-comp" => "I am legal entity",
	"popreg_h-org" => "I am public organization",
	"popreg_btn-reg" => "To get registered",
	"popreg_btn-fb" => "Login with Facebook",
	"popreg_btn-vk" => "Login with Vkontakte",
	"popreg_btn-ok" => "Login with Odnoklassniki",

	// Restore password box
	"poprest_hdr" => "Password reset",
	"poprest_lbl-login" => "Email",
	"poprest_dolog" => "Return to log in dialogue",
	"poprest_btn-send" => "Restore",

	// Send message box
	"popmsg_hdr" => "Send message",
	"popmsg_lbl-recv" => "Recipient",
	"popmsg_lbl-msg" => "Message",
	"popmsg_ph_msg" => "Text of your message",
	"popmsg_btn-send" => "Send",

	// Stars review box
	"poprev_hdr" => "Rate received help",
	"poprev_lbl-rate" => "Rate",
	"poprev_lbl-msg" => "Comment",
	"poprev_ph_msg" => "Your comment to rate",
	"poprev_btn-send" => "Publish",

	// Help graph box
	"popgraph_hdr" => "Tree of mutual help",

	// Pop share box
	"popshare_hdr" => "Share in social networks",
	"popshare_text" => "<b>Your project is published!</b><br> You'll get help much faster if you share the project in social networks.",
	"popshare_soc" => " To try luck and win an instant prize, share bonus in social networks",
	"pophelp_err-payfmt" => "Amount shall be larger",
	"pophelp_err-payfmt-def" => "Amount is incorrect",

	// Pop promo help box
	"pophelp_hdr" => "Thank you for your help",
	"pophelp_hdrmy" => "We are ready to help",
	"pophelp_btn-dream" => "Tell about dream",
	"pophelp_btn-dohelp" => "Help other people",
	"pophelp_btn-gethelp" => "Get help ",
	// Pop promo help box
	"pophelp2_hdr" => "Help other people!",
	"pophelp2_btn-dream" => "Tell about dream ",
	"pophelp2_btn-dohelp" => " Help other people ",
	"pophelp2_btn-gethelp" => " Get help ",

	///////////////////////////////////////////////////////////////////////////
	//
	// Catalog pages
	//
	///////////////////////////////////////////////////////////////////////////

	"cat_lcol-hdr" => "Themes of projects",
	"cat_free-bonus" => "Suspended bonus",
	"cat_flt-hdr" => "Filter by projects",
	"cat_flt-all" => "All",
	"cat_flt-popular" => "Popular",
	"cat_flt-new" => "New",
	"cat_flt-ending" => "At the stage of completion",
	"cat_flt-thdr" => "Projects by type of help",
	"cat_flt-tmoney" => "Money",
	"cat_flt-twork" => "Action",
	"cat_flt-tloc" => "Neighbouring",
	"cat_flt-teasy" => "Easy help",
	"cat_btn-gethelp" => "Get help",
	"cat_btn-dream" => "Make dream come true",
	"cat_btn-thing" => "Share things",
	"cat_btn-myhelp" => "I will help",
	"cat_it-started" => "Started",
	"cat_it-btn-help" => "To help",
	"cat_it-help" => "have already helped",
	"cat_it-getthing" => "have already requested a thing",
	"cat_it-rating" => "current rating",
	"cat_it-btn-event" => "To take part",
	"cat_it-btn-thing" => "To get the thing",
	"cat_it-btn-myhelp" => "To ask for help",
	"cat_it-event" => "already want to take part in event",
	"cat_noproj" => "No project has been published in this section to get help. <br> Publish your project first!",
	"cat_nothing" => "No project has been published in this section to share things.<br> Publish your project first!",


	///////////////////////////////////////////////////////////////////////////
	//
	// Free Bonus pages
	//
	///////////////////////////////////////////////////////////////////////////

	"bonus_lcol-hdr" => "Topics of projects",
	//"bonus_flt-hdr" => "Filter by projects",
	//"bonus_flt-all" => "All",
	//"bonus_flt-popular" => "Popular",
	//"bonus_flt-new" => "New",
	//"bonus_flt-ending" => "At the stage of completion",
	"bonus_btn-bonus" => "To suspend bonus",
	"bonus_it-started" => "Started",
	"bonus_it-btn-getbonus" => "To get bonus",
	"bonus_it-help" => "have already helped",
	"bonus_noproj" => " No suspended bonus has been published in this section.<br>Suspend your bonus first!",

	// Add photo
	"addbonus_photo-hdr" => "Adding photo to the project",
	"addbonus_photo-lbl" => "Add photo to the project",
	"addbonus_photo-del" => "Delete",
	"addbonus_photo-btn" => "Upload photo",
	"addproj_photo-btn-next" => "Upload",
	"addbonus_photo-user-lbl" => "Upload my user image",
	"addbonus_photo-user-btn" => "Upload user image",
	"addbonus_photo-btn-finish" => "To publish project",
	// Add proj
	"addbonus_h1" => "Suspended bonus",
	"addbonus_h1-edit" => "To edit bonus",
	"addbonus_h1-created" => "Bonus is suspended",
	"addbonus_hdr" => "Fill data about bonus",
	"addbonus_ph-tit" => "What do you want to share?",
	"addbonus_lbl-tit" => "Name of bonus",
	"addbonus_lbl-difficulty" => "Difficulty",
	"addbonus_lbl-amount" => "Q-ty of bonuses",
	"addbonus_lbl-amount-money" => "Amount of help",
	"addbonus_lbl-amount-human" => "Number of persons",
	"addbonus_lbl-dtst" => "Date of start",
	"addbonus_lbl-dten" => "Until",
	"addbonus_lbl-country" => "Country",
	"addbonus_lbl-country-0" => "Specify your country",
	"addbonus_lbl-obl" => "Region",
	"addbonus_lbl-obl-0" => "Specify your region",
	"addbonus_lbl-city" => "City",
	"addbonus_lbl-city-0" => "Specify your city",
	"addbonus_lbl-descr" => "Description",
	"addbonus_lbl-descr-count0" => "Enter more",
	"addbonus_lbl-descr-count1" => "symbols to have higher positions in search",
	"addbonus_lbl-descrfull" => "Detail description",
	"addbonus_lbl-photo" => "Photo",
	"addbonus_lbl-background" => "Background of project card",
	"addbonus_btn-addphoto" => "Upload photo",
	"addbonus_btn-addbonus" => "Place bonus",
	"addbonus_btn-save" => "Save changes",
	// Add proj errors
	"addbonus_err-tit" => "You should specify name of bonus",
	"addbonus_err-sect" => "You should select the destination catalogue for the project",
	"addbonus_err-amount" => "You have to specify the number of available bonuses",
	"addbonus_err-amount1" => "Amount of help shall be a valid numeric value exceeding zero.",
	"addbonus_err-stdt" => "Dates shall be in DD.MM.YYYY format",
	"addbonus_err-endt" => "Dates shall be in DD.MM.YYYY format and exceed start date",
	"addbonus_err-obl" => "You did not specify country",
	"addbonus_err-obl" => "You did not specify region/oblast",
	"addbonus_err-city" => "You did not specify city",
	"addbonus_err-descr" => "You should describe the bonus and terms for its receipt",

	"addbonus_photo-promo" => " Availability of photo allows the user evaluating its necessity. Thus your bonus will get the person who needs it most!",

	// View project
	"bonusview_dt-add" => "Placed",
	"bonusview_views" => "Total views",
	"bonusview_notify-owner" => "You are the owner of the project.",
	"bonusview_nomore" => "There are no more bonuses",
	"bonusview_btn-getbonus" => "To get bonus",

	"bonusview_stat-sum" => "Raised already",
	"bonusview_stat-up" => "until",
	"bonusview_stat-numwin" => "Won already",
	"bonusview_stat-tm" => "Time left",
	"bonusview_stat-till" => "Bonus active until",

	"bonusview_hdr-descr" => "Detail data",
	"bonusview_fld-cont" => "Contact person",
	"bonusview_fld-city" => "City",
	"bonusview_fld-msg" => "Contact",
	"bonusview_btn-msg" => "Write message",
	"bonusview_usr-rate" => "Rating of help",
	"bonusview_usr-helpnum" => "Total number of help",
	"bonusview_hdr-need" => "I will accept help in sections",
	"bonusview_btn-tree" => "Show my tree",

	"bonusview_hdr-comment" => "Comments",
	"bonusview_dt-comment" => "Added",
	"bonusview_lbl-comment" => "Your comment",
	"bonusview_err-comment" => "Comment shall contain text",
	"bonusview_btn-comment" => "Send",

	///////////////////////////////////////////////////////////////////////////
	//
	// Project view pages
	//
	///////////////////////////////////////////////////////////////////////////

	"proj_notallowed" => "Back to my projects review",
	"proj_backtoproj" => "Back to project review",
	"proj_backtocab" => " Back to the list of projects in profile",

	// Project create
	// Add photo
	"addproj_photo-hdr" => "1. Adding photo to the project",
	"addproj_photo-lbl" => "Attach photo to the project",
	"addproj_photo-lbl-add" => "Attach <span class='lbl-add-green'>more photos</span> to the project",
	"addproj_photo-del" => "Delete",
	"addproj_photo-btn" => "Upload photo",
	"addproj_photo-user-lbl" => "Upload my user image",
	"addproj_photo-user-btn" => "Upload user image",
	"addproj_photo-btn-finish" => "To publish project",
	"addproj_background-hdr" => "2. Please select the payment systems to receive money ",
	"addproj_background-hdr-nxt" => "We recommend specifying as many systems as possible; it will increase likelihood of getting help. You may take advantage of our current accounts with the subsequent withdrawal of money to own account.",
	"addproj_avatar-hdr" => "3. Upload user image of the author",
	// Add proj
	"addproj_hdr" => "Filling in data about the project",
	"addproj_hdrevent" => "Filling in data about event",
	"addproj_ph-tit" => "Title of your project",
	"addproj_lbl-tit" => "Name of project",
	"addproj_lbl-titevent" => "Name of event",
	"addproj_lbl-sect" => "Select group for publication",
	"addproj_lbl-sect0" => "Section is not selected",
	"addproj_lbl-type" => "Type of project",
	"addproj_lbl-typeevent" => "Type of event",
	"addproj_lbl-type-0" => "Help with money",
// "Financial help raising",
	"addproj_lbl-type-1" => "Admission of group of people",
	"addproj_lbl-type-2" => "Help with action",    //"Search for expert help",
	"addproj_lbl-type-3" => "Other type of help",
	"addproj_lbl-amount" => "Amount of help",
	"addproj_lbl-amount-money" => "Amount of help",
	"addproj_lbl-amount-human" => "Number of persons",
	"addproj_lbl-dtst" => "Date of start",
	"addproj_lbl-dten" => "To what date",
	"addproj_lbl-country" => "Country",
	"addproj_lbl-country-0" => "Specify your country",
	"addproj_lbl-obl" => "Region",
	"addproj_lbl-obl-0" => "Specify your region",
	"addproj_lbl-city" => "City",
	"addproj_lbl-city-0" => "Specify your city",
	"addproj_lbl-descr" => "Description",
	"addproj_lbl-descr-placeholder" => " We recommend using 300 characters minimum in the Project description, since such projects get more trust and confidence from other users.",
	"addproj_lbl-descr-count0" => "Enter more",
	"addproj_lbl-descr-count1" => "symbols to have higher positions in search",
	"addproj_lbl-descrfull" => "Detail description",
	"addproj_lbl-descrdop" => "(will be shown during search)",
	"addproj_lbl-descrfulldop" => "(will be shown to the user at signing in the website)",
	"addproj_lbl-photo" => "Photo",
	"addproj_lbl-background" => "Background of project card",
	"addproj_btn-addphoto" => "Upload photo",
	"addproj_btn-adddream" => "Make dream come true ",
	"addproj_btn-addevent" => "Create event",
	"addproj_btn-addthing" => "Share things",
	"addproj_btn-addproj" => "Place project",
	"addproj_btn-save" => "Save changes",
	// Add proj errors
	"addproj_err-tit" => "You should specify name of project",
	"addproj_err-titevent" => "You should specify name of event",
	"addproj_err-sect" => "You should select the destination catalogue for the project",
	"addproj_err-sectevent" => "You should select destination catalogue for event",
	"addproj_err-amount" => "You have to specify the required amount of help",
	"addproj_err-amount1" => "Amount of help shall be a valid numeric value exceeding zero.",
	"addproj_err-stdt" => "Dates shall be in DD.MM.YYYY format",
	"addproj_err-endt" => "Dates shall be in DD.MM.YYYY format and exceed start date",
	"addproj_err-obl" => "You did not specify country",
	"addproj_err-obl" => "You did not specify region/oblast",
	"addproj_err-city" => "You did not specify city",
	"addproj_err-descr" => "You should describe your aim to get help",
	"addproj_err-descrevent" => "You should describe the carried out event",

	"addproj_photo-promo" => " According to statistics, the probability of receiving help increases by 60% if you provide photos!",

	// View project
	"projview_dt-add" => "Placed",
	"projview_views" => "Total views",
	"projview_rating" => "Current rating",
	"projview_notify-owner" => "You are the owner of the project.",
	"projview_notify-proj-stoped" => " At the moment the project is stopped. Getting help is stopped.",
	"projview_notify-proj-ended" => " At the moment the project is already ended and further help cannot be provided.",
	"projview_notify-helpwait" => " At the moment you have already submitted a request for help. It is at the stage of confirmation.",
	"projview_notify-helpdone" => " At the moment you have already submitted a request for help. It was confirmed.",
	"projview_notify-helpdecl" => "Your request has been declined.",
	"projview_btn-money" => "Help with money",
	"projview_btn-help" => "Help with action",
	"projview_btn-thing" => "Ask for thing",
	"projview_btn-gethelp" => " Ask for help",
	"projview_nophoto-txt" => " At the moment there are no additional photographs",

	"projview_stat-sum" => "Raised already",
	"projview_stat-avg" => "Average help",
	"projview_woh-hlp" => "Where by promotional action",
	"projview_stat-target" => "Target",
	"projview_stat-up" => "Until",
	"projview_stat-num" => "Helped already ",
	"projview_stat-numhuman" => "Joined already ",
	"projview_stat-tm" => "Time left",
	"projview_stat-till" => "Help effective until",
	"projview_stat-numreq" => "Requests received already ",

	"projview_hdr-descr" => "Detail data",
	"projview_fld-cont" => "Contact person",
	"projview_fld-city" => "City",
	"projview_fld-msg" => "Contact",
	"projview_btn-msg" => "Write message",
	"projview_usr-rate" => "Rating of help",
	"projview_usr-helpnum" => "Total number of help",
	"projview_hdr-need" => "I will accept help in sections",
	"projview_btn-tree" => "Show my tree",

	"projview_hdr-comment" => "Comments",
	"projview_dt-comment" => "Added",
	"projview_lbl-comment" => "Your comment",
	"projview_err-comment" => "Comment shall contain text.",
	"projview_btn-comment" => "Send",

	"proj_tree" => "Tree of good deeds",

	"projhelp_hdr" => "Recent received help",
	"projhelp_today" => "today",
	"projhelp_ago" => "ago",
	"projhelp_showall" => "All help offers",
	"projhelp_showallcom" => "Show more comments →",
	"projmail_help-tit" => " Your project has received next payment!!!",
	//"projmail_help-descr-pr" => "Your project ",
	"projmail_help-descr-pr2" => "Your project has received next payment in the amount of ",
	"projmail_help-descr-pr3" => "<br>Make sure to thank the user",
	"projmail_help-descr-pr4" => "<br> for provided help. <br><br> Please visit _hr_ and click 'To thank'. <br> <br> Gratitude is not only a good habit <br> and attracts positive energy of success to you, <br> but it also encourages people to help you again.",


	"projdone_breadcr-myproj" => "My projects",
	"projdone_h1-edit" => "Editing of project",
	"projdone_h1-saved" => "Information about project is saved ",
	"projdone_h1-new" => "Creation of new project",
	"projdone_h1-new-dream" => "Achieve your dream",
	"projdone_h1-new-event" => "Creation of new event",
	"projdone_h1-new-thing" => "Share your things",
	"projdone_h1-new-myhelp" => "I will help people in need",
	"projdone_h1-created" => "You have created the project to get help",
	"projdone_h1-helpdone" => "You have sent request to provide help",
	"projdone_h1-thingdone" => "You have sent request to get things",
	"projdone_h1-myhelpdone" => " You have sent request to get help ",

	"projview_rate-dream" => "Rate dream",
	"projview_share" => "Share",
	"projview_sharing" => "If you want to help, repost it",
	"projview_repost" => "Reposted",
	"projview_usr-rating" => "Rating",

	// Promise page
	"projprom_prom-txt" => "<p>Thank you for your wilingness to help.<br> 
                     <br> According to the rules of the platform, in order to avoid cheating with rating, you should confirm intention, <br>if amount of all promises exceeds<b>_defsum_</b> _cur_ or one-time payment- <b>_defsum1_</b> _cur_ . <br> To do it, transfer<b>_perc_%</b> of promised amount<br>to your own stabilization fund at Wayofhelp platform.<br> At the moment the total amount of promises is <b>_allsum_</b>. <a href='_hr_' target='_blank' > See details </a>",
	"projprom_btn" => "I promise help if…",
	"projprom_phone-done" => "Telephone number was confirmed.",
	"projprom_phone-msgsend" => "Confirmation code was sent to your number.",
	"projprom_phone-notdone" => "Please specify correctly your telephone number.",
	"projprom_phone-errcode" => "Code is wrong.",
	"projprom_frm-phone" => "Telephone",
	"projprom_frm-code" => "Confirmation code",
	"projprom_def-msg" => " <p>Thank you for your willingness to help.<br>
                            <br> If the user raises the required amount, we will ask you to fulfill your promise.
                            <br> Willingness to help is as important as help. That is why your willingness to help will be displayed on the tree of good deeds as actual help.</p>",
	"projprom_def-form-msg1" => "                 <p> Thank you for your willingness to help.</p> 
                            <p>Total amount of money promised by you to help other projects 

                            = ",
	"projprom_def-form-msg2" => "<p>It is quite a large amount.
                            To avoid misunderstanding as for your good intentions, enter 

                            please your mobile phone number in line<span class=\"bg\"> “Telephone”</span>.</p><p>We will send you

                            the code to enter in line <span class=\"bg\"> “Code” </span> of the form.</p>",
	//////////////////////////////////////////////////////////////////////////////////
	//
	// User info page
	//
	//////////////////////////////////////////////////////////////////////////////////

	"user_profession" => "Profession",
	"user_sphere" => "Sphere of activity",
	"user_contact" => "Contact person",
	"user_mysite" => "My websites",
	"user_nosite" => "You did not specify your website",
	"user_about" => "Detail data",
	"user_tab-needhelp" => "Requests of this user for help",
	"user_tab-feedbacks" => "Feedback about this user",

	"user_tot-req" => "requests",
	"user_say-thanks" => "To thank",

	"user_tbl-btntree" => "Review visually ",
	"user_tbl-proj" => "Project",
	"user_tbl-projhelp" => "Helped already ",
	"user_tbl-sten" => "Start / End",
	"user_tbl-left" => "Remaining",
	"user_tbl-feed" => "Feedback",
	"user_tbl-rate" => "Rate",
	"user_tbl-noproj" => "This user has no projects for help receipt.",
	"user_tbl-norate" => "Nobody has rated help of this user.",

	"user_stat-rate" => "Rating of help provision",
	"user_stat-tothelp" => "Total help",
	"user_stat-helpmoney" => "Where the amount of money is ",
	"user_stat-tothelps" => "Total help",
	"user_stat-helpsum" => "To amount",

	"user_need-hdr" => "I will accept help in sections",
	"user_willhelp-hdr" => "I will help in sections ",
	"user_btn-tree" => "My tree",

	"user_dohelp-hdr" => "Recent help provided by the user",
	"user_dohelp-rate" => "",
	"user_dohelp-ago" => "ago",

	"user_share-profile1" => "Share",
	"user_share-profile2" => "by profile",
	"user_rat" => "Rating",
	"user_moreinfo" => "Detail data",
	"user_wohpays" => "Earned by activity",

	// SHARE USER
	//////////////////////////////////////////////////////////////////////////////////
	//
	// Reviews
	//
	//////////////////////////////////////////////////////////////////////////////////

	"rev_gethelp" => "Received help",
	"rev_btn-tree" => "My tree",

	//////////////////////////////////////////////////////////////////////////////////
	//
	// Cabinet
	//
	//////////////////////////////////////////////////////////////////////////////////

	"cab_tab-profile" => "Profile",
	"cab_tab-myhelp" => "My help",
	"cab_tab-myhelpget" => "I receive help",
	"cab_tab-needhelp" => "I need help",
	"cab_tab-dohelp" => "I help",
	"cab_tab-givehelp" => "I will share",
	"cab_tab-msg" => "Messages",
	"cab_tab-bonus" => "My bonuses",
	"cab_tab-onlooking" => "How other people see my profile",

	// Users profile
	"cabprof_social" => "Link account to social network",
	"cabprof_u-profession" => "Profession",
	"cabprof_u-sphere" => "Sphere of activity",
	"cabprof_u-sphere-notset" => "not specified",
	"cabprof_u-contact" => "Contact person",
	"cabprof_u-mysite" => "My websites",
	"cabprof_u-nosite" => "You did not specify your website",
	"cabprof_u-stat-rate" => "Rating of help provision",
	"cabprof_u-btn-edit" => "Edit profile",
	"cabprof_u-btn-topics" => "Edit interests",

	"cabprof_tree-hdr" => "My tree",
	//"cabprof_tree-in" => "incoming",
	//"cabprof_tree-out" => "outgoing",
	"cabprof_tree-in" => "help received by me",
	"cabprof_tree-out" => " help provided by me",

	"cabprof_rnd-dohelp" => "Provide <br>help",
	"cabprof_rnd-gethelp" => "Receive <br>help",
	"cabprof_rnd-msg" => "Messages",
	"cabprof_rnd-stat" => "Statistics",

	// Need help projects
	"cabneed_flt-hdr" => "Filter by projects",
	"cabneed_flt-all" => "All",
	"cabneed_flt-cur" => "Current",
	"cabneed_flt-ended" => "Ended ",

	"cabneed_btn-new" => "Create new project",
	"cabneed_btn-newevent" => "Create event",

	"cabneed_tbl-id" => "No.",
	"cabneed_tbl-proj" => "Project",
	"cabneed_tbl-status" => "Status",
	"cabneed_tbl-msg" => "Messages",
	"cabneed_tbl-sten" => "Start/end",
	"cabneed_tbl-left" => "Remaining",
	"cabneed_tbl-func" => "Actions",
	"cabneed_tbl-noproj" => " You have no created project for help receipt.",
	"cabneed_tbl-noproj-running" => " You have no current project for help receipt.",
	"cabneed_tbl-noproj-finished" => "You have no completed project for help receipt.",

	"cabneed_func-edit" => "Edit",
	"cabneed_func-start" => "Start",
	"cabneed_func-stop" => "Stop",
	"cabneed_func-success" => "Close as successful",
	"cabneed_prepay" => "Pay for project",

	"cabneed_stat-stoped" => "Project is suspended",
	"cabneed_stat-run" => "Project started",
	"cabneed_stat-ended" => "Project ended",

	"cabneed_req-new" => "New help offers",
	"cabneed_req-all" => "All help offers",

	"cabmyhelp_req-new" => "New help requests",
	"cabmyhelp_req-all" => "All help requests",

	// my do help requests
	"cabdohelp_flt-hdr" => "Filter by projects",
	"cabdohelp_flt-all" => "All",
	"cabdohelp_flt-cur" => "Current",
	"cabdohelp_flt-ended" => "Ended",

	"cabdohelp_btn-dohelp" => "To help",

	"cabdohelp_tbl-proj" => "Project",
	"cabdohelp_tbl-status" => "Status",
	"cabdohelp_tbl-msg" => "Messages",
	"cabdohelp_tbl-left" => "Remaining",
	"cabdohelp_tbl-func" => "Actions",
	"cabdohelp_tbl-noreq" => "At the moment you have not provided any help.",
	"cabdohelp_tbl-noreq-running" => " You have not provided any help by current projects.",
	"cabdohelp_tbl-noreq-finished" => " You have not provided any help by ended projects.",

	"cabdohelp_stat-wait" => "Waiting for confirmation",
	"cabdohelp_stat-waitbyuser" => "Waiting for check",
	"cabdohelp_stat-confirm" => "Confirmed",
	"cabdohelp_stat-promise" => "Promised",
	"cabdohelp_stat-promisemess" => "Waiting for promised money",
	"cabdohelp_stat-promisepay" => "Paid/Waiting for confirmation",
	"cabdohelp_stat-cancel" => "Cancelled request",
	"cabdohelp_stat-decline" => "Help declined",

	"cabdohelp_stat-promaddr" => "To be paid when project raises the required amount",
	"cabdohelp_stat-promaddrmsg" => " Waiting for promised",

	"cabdohelp_func-cancel" => "To cancel",

	// help requests to me
	"cabreq_flt-hdr" => "Filter by help offers",
	"cabreq_flt-all" => "All",
	"cabreq_flt-new" => "New",
	"cabreq_flt-confirm" => "Confirmed",
	"cabreq_flt-decline" => "Declined",

	"cabreq_tbl-pers" => "Who helped",
	"cabreq_tbl-persget" => "Who requests",
	"cabreq_tbl-persmy" => "Who requests",
	"cabreq_tbl-help" => "Help",
	"cabreq_tbl-getthing" => "Request",
	"cabreq_tbl-status" => "Status",
	"cabreq_tbl-func" => "Actions",

	"cabreq_help-money" => "Deposited",
	"cabreq_help-moneyprom" => "Promised",
	"cabreq_help-moneypromcheck" => "Confirm",
	"cabreq_rate-val" => "Your rate",

	"cabreq_stat-confirm" => "Confirmed",
	"cabreq_stat-wait" => "Waiting for confirmation",
	"cabreq_stat-decline" => "Help declined",

	"cabreq_btn-accept" => "Accept",
	"cabreq_btn-decline" => "Decline",
	"cabreq_btn-rate" => "To rate help",

	// Messages
	"cabmsg_msg-tot" => "Total messages",
	"cabmsg_msg-in" => "Incoming",
	"cabmsg_msg-out" => "Outgoing",
	"cabmsg_msg-new" => "New messages ",

	"cabmsg_noitems" => "You have no incoming and outgoing  messages.",

	"cabmsg_proj-started" => "Started",
	"cabmsg_reply" => "To reply",
	"cabmsg_newmsg" => "Write message",

	// Bonus list
	"cabbonus_tbl-bonus" => "Bonus",
	"cabbonus_tbl-luckind" => "Lucky number",
	"cabbonus_tbl-get" => "Received",
	"cabbonus_tbl-func" => "Actions",
	"cabbonus_tbl-noits" => "At the moment you have not won any suspended  bonus",

	//Main index page
	"main_sec-dream" => "persons made the dream come true",
	"main_sec-tree" => "Users started own ",
	"main_sec-tree1" => "Tree of good deeds",
	"main_sec-loc" => "Maximum distance for help receipt (Kiev-Yakutsk)",
	"main_sec-smily" => " The kindest user",
	"main_sec-bonus1" => " daily win instant",
	"main_sec-bonus2" => "for good deeds",
	"main_sec-loc" => " Maximum distance for help receipt (Kiev-Yakutsk)",
	"main_sec-help" => "The most active users of platform get guaranteed help daily",
	"main_sec-h1" => "Get help",
	"main_sec-h2" => "or start own Tree of good deeds",

	"projprom_def-msg-s" => "Thank you for your <br> willingness to help!",
	"projprom_def-msg-o" => "You can make payment<a href='http://wayofhelp.com/cabinet/dohelp/' style='color:#9deaec;cursor: pointer;text-decoration: underline;'>right now</a> or do it at any convenient time, for example, when project raises 60% of requested amount. 
                      To do it, sign in the portal using your account and open tab \"I provide help\". 
                      Search for this project and click \"Keep a promise\"",

	//POPUP-POSTREGISTR
	"popup_postreg-title" => " Let's change the world for better!",
	"popup_postreg-text" => "With such a slogan <b>Way of Help</b> -  charity project, which fulfills all the dreams of ordinary people and makes incredible miracles – actively grips the Internet!</p>
        <p> Project gathered the dreamers, inventors, virtuous, sympathetic people and people in need with a pure heart!</p>
        <p><b>Way of Help</b> is a place where you will be able to obtain and provide all possible help: good deeds, services, money, advice and even likes) Get blood from the donor, free tow, give away a kitten or levelled up account of a game!!</p>
        <p> In fact, it all actually depends on each of us and even some little thing for you is not a trifle for someone)</p> 
        <p> Each of us has the opportunity to change the world for better! Together we have more chances and doing good deeds every day we can change and improve the lives of many people as well as our own!</p>
        <p> Do not forget that all good things come back like a boomerang.</p>
        <p> A lot of people will find out about all your good deeds and among them there are certainly those who can help you with your dream. Do good deeds for the benefit of other people!</p>
        <p> Share this post on your page and it will be your start of good and very important deed ;)",
	"popup_postreg-share" => "Let’s change the world together.",
	"popup_postreg-sharetit" => " started own chain of good deeds at wayofhelp portal.",

	//NEW USER STATUS

	"mess_usr-new-stattit" => " Congratulations! You got new status…!!!",

	"mess_usr-new-statbody" => " Thanks to your activity and desire to help people you reached a new level - you got <br>
status <b>_stat_</b>!!!
<br><br>The number of good deeds in the world increases thanks to you!!!<br><br>

Follow <a href='http://wayofhelp.com/cabinet/'>link</a>and share this great news with your friends.",
	//CHECK
	"proj_check-project" => "You have not completed your profile.<br>Please specify your email, country and city.",
	"proj_check-editme" => "Edit my profile",
	//CRONS MESS
	"crons_dailymess-all" => "",
	"crons_dailymess-titall" => "Your project got new comments and rates",
	"crons_dailymess-titstar" => "Your project got new rates",
	"crons_dailymess-titcomm" => "Your project # has got new comments",
	"crons_dailymess-bodycomm1" => "Your project _title_ got new comments.<br>
To read comments, please follow the link.<br><br>
Make sure to respond to users’ comments.<br>
It will increase confidence and loyalty to your project.",
	"crons_dailymess-bodycomm2" => "Follow the link to read them.",
	"crons_dailymess-bodystar1" => "Your dream got new rates from the users.",
	"crons_dailymess-bodystar2" => "Follow the link to find out the rate of your dream. ",
	"crons_dailymess-bodyall1" => "Your project got new comments and rates. Follow the link for more details. ",
	"crons_dailymess-bodyall2" => "Follow the link for more details. ",
	"crons_succesmess-paytit" => "One more project has been successfully completed thanks to you!",
	"crons_succesmess-finish" => "Your project has been successfully completed!",

	"crons_succesmess-paybody" => "<b>We have great news!</b><br>One more<a href='_hr_'>project</a> at our platform achieved successful completion.<br>It is also<b>due to your help! </b><br><br> We ask you to keep your \"promises\" <br> by this project within a week. <br><br>Please do it in time, for the author of the project<br>to achieve own dream as soon as possible.<br><br>To keep a promise, <a href='_hr2_'>log in</a>  website and profile<br>
open tab 'I help' at the required project, click green button \"To keep<br>promise\" and make payment.</a>
",
	"crons_message-newshead" => "Good mood for all day!!!",
	"crons_message-newsshare" => "Follow our groups <a href='_VK_'>VK</a>, <a href='_FB_'>FB</a> and <a href='_OK_'>OK</a> and get even more 
cheerful and positive emotions",
	"crons_message-newstext" => "Start day with good and positive emotions.<br>
Stories about people and their actions, raising your soul heavenward!<br>
<br>",
	"crons_message-money" => "<br>To get status_hr1_ you need _hr2_ scores",
	"crons_message-newbontit" => "New bonus is waiting for you",
	"crons_message-newbon" => "New bonuses are at the WayOfHelp portal:<br>",
	"crons_message-newbon1" => "<ul>
<li><a href='_hr2_'>_hr1_</a></li>
</ul> ",
	"crons_message-newbon2" => "Make good deeds and plant<a href='http://wayofhelp.com/info/tree/'>your Tree of good deeds</a>.<br>Log in and win nice valuable prizes for your help.<br>
More goods deeds mean more bonuses.<br>",

	"crons_newitem-tit" => "New projects which may be interesting for you",
	"crons_newitem-desc" => "<br>At your absence at <a href='http://wayofhelp.com/'>WayOfHelp</a> portal<br>new projects appeared which may be interesting for you<br>
<br>Find out, may be your help will<br>
make the dream of the user come true!!!<br>
",

	"crons_message-newsdesc" => "New articles at WayOfHelp:<br>",
	"crons_message-newsdown" => "If you're not indifferent to such stories and strive to make the world a better place, then start doing it right now, choosing help within your scope of abilities.<br>Start own chain of good deeds, and see how it grows and invokes new good deeds.",
	"crons_message-topnews" => "<ul>
<li><a href='_hr2_'>_hr1_</a></li>
</ul> ",
	"crons_message-topjob" => "_alljob_ good deeds, where the amount of money is ...",
	"crons_message-alljob" => " _alljob_ good deeds were made at our portal during last week.  Where the amount of money is: _moneyjob_ <br>",
	//"crons_message-weekuser" => "Users of the week are:<br><ul>
//<li><a href='_href_'>_week_</a></li>
//</ul> <br>",
	"crons_message-active1" => "The most active are: <br>",
	"crons_message-active" => "<ul>
<li><a href='_hrefact_'>_active_</a></li>
</ul><br>",
	"crons_message-topusrtree" => "Rating of users is <a href='http://wayofhelp.com/rating/'>here</a>.<br>The biggest Tree of good deeds is: _toptree_ ,- <a href='_hreftree_'>see the Tree</a><br>",
	"crons_message-topprovoc" => "Most good deeds invoked by: _topprov_ ,- <a href='_hrefprov_'>see the Tree</a><br>",
	"crons_message-newbondot" => "<ul><li><a href='_dot_'>...</a></li></ul>",
	"crons_message-accrualstop" => "Accrual for activity – _sum_ UAH!",
	"crons_message-accruals" => "<b>_name_</b>, thank you for your activity<br>
to increase the number of good deeds in community.<br>
Due <b>to your activity</b> the number of good deeds is increasing and the world becomes better.<br><br>
For your actions you got<b>_sum_ scores</b> in local currency.<br>
See the full list of accruals<a href='http://wayofhelp.com/'>here</a>.<br><br>
To <b> convert</b> local units<b>to money</b>,<br> synchronize your account with one of social networks.<br><br>Please respect this procedure. We have to be sure <br>that your account is valid. We transfer you real money.<br><br>
Log in <a href='http://wayofhelp.com/cabinet' >My profile</a> (you have to sign in) and click one of icons<br>of social networks under<b> “Link account to social network”</b>.<br><br>
After successful moderation money will be charged to your project.<br><br>You will also be able to log in wayofhelp portal using linked social networks<br>. Your profile will be processed by the system as of one and the same user.<br><br>
Shall you fail to synchronize the account within <b>3 days</b>,  money will <br> <b> not be </b> transferred to your project and will be returned to fund of wayofhelp portal.<br>",
	"crons_message-presuccess" => "Your bonus passed moderation!",
	"crons_message-premoder" => "Your <a href='_bon_'>bonus</a> passed moderation and placed at wayofhelp portal.<br>
<b>Use our bonus window to find out how your bonus looks like</b>.<br>
<br><br>
You will get notification when someone wins your bonus!!!
<br><br>
Thank you for your active participation in order to increase the number of good deeds! Thanks to you the world is getting better!!!
<br><br>",
	//PROJECT TO CLOSE
	"crons_message-closeprojtitle" => "Attention! Your project will soon expire!",
	"crons_message-closeproject" => "Term of your project <a href='_proj_'>_title_</a> will soon expire.<br>
Project will be automatically stopped _timestop_ <br><br>
Then the project will be permanently archived.<br><br>
Shall the project relate to fund raising and reach less than 60% at expiration, funds <br>
of project will be allocated to other projects at the portal<a href='http://wayofhelp.com/'>wayofhelp</a>.<br><br>
You may prolong your project. To do it<a href='http://wayofhelp.com/'>log in</a> website, open in profile <br>
the tab “I need help”, click \"Edit\" at the required project and set required deadline. 
",

	//SUCCESS PROJECT MESS TO OWNER
	"own_proj-succtit" => "HOORAY!!! Your project has completed successfully!",
	"own_proj-succdesc" => "According <a href='_hr_'>to the rules of the portal</a> wayofhelp, at successful completion of the project,<br> its owner has to transfer _perc_ % of raised amount<br> and make <a href='_cab_'>repost</a> about successful completion of the project.<br><br>We help you, you help us. All your funds will be used to <br>
develop the project and increase the number of good deeds.<br>
",
	// TREE SHARE MESS
	"tree_share-tit" => "Tree of good deeds of user _name_ _fname_",
	"tree_share-descr" => "Q-ty of good deeds: _d1_. Invoked good deeds: _d2_",
	"tree_shareogtit" => "_name_ made _co_ good deeds.",
	"tree_shareogdescr" => "User’s good deeds invoked _co_ good deeds more. Log in and review.",

	// SEND REGISTRATION MESS
	"reg_messtit" => "Registration on website_hr_ ",
	"reg_messdescr" => "<You have just registered on website _hr_<br> Your data for log in:
<br>Login: _log_
<br>Password: _pass_
<br>To activate your account and confirm your data, follow the link below: 
<br>_hract_ ",

	"bonus_winmsg" => "Hello. User <a target='_blank' href='_hr_' > _name_ _fname_ </a> has won your suspended bonus. <br> Please fulfill your obligations and provide the award.",


	"bonview_boncat" => "Catalogue of bonuses",
	"bonview_bonhide" => "hide",
	"bonview_bonshow" => "show",
	"bonview_bonnocomm" => "Nobody has left comments to this project yet",
	"bonview_nolog" => "You are not logged in",
	"bonview_nologcomm" => "To write comments, please,",
	"bonview_nologon" => "log in",
	"bonview_nouser" => "If you do not have an account, please",
	"bonview_register" => "register",

	"view_ang-day" => "Angel of the day",
	"view_ang-week" => "Angel of the week",

	"cabinet_noselected" => "Not selected",
	"cabinet_needhelp" => "Asked for services",
	"cabinet_givehelp" => "Offer services",
	"cabinet_redacted" => "Edit profile",
	"cabinet_mytree" => "My tree",
	"cabinet_takemyhelp" => "help<br>received by me",
	"cabinet_givemyhelp" => " help <br>provided by me",
	"cabinet_assist" => "I will help",
	"cabinet_makeassist" => "To<br>help",
	"cabinet_needassist" => "I need help",
	"cabinet_takeassist" => "To get<br>help",
	"cabinet_adminmsg" => "Message <br>to administrator",
	"cabinet_next" => "Next",
	"cabinet_open-your-tree" => "Open your Tree",
	"cabinet_good-deeds" => "of Good deeds and share",
	"cabinet_they-friend" => "with friends:",
	"cabinet_hobbies" => "Edit interests",

	"catalog_bonus" => "Bonus",
	"catalog_makeproject" => "Create own project",
	"catalog_all-country" => "All countries",
	"catalog_all-region" => "All regions",
	"catalog_all-city" => "All cities",
	"catalog_collected-money" => "Collected money",

	"err404_no-page" => "We’re sorry. The page you have requested cannot be found.",
	"err404_search-info" => "To find required information, use following links:",
	"err404_main-page" => "Main page",
	"err404_making-help" => "Help provision",
	"err404_taking-help" => "Help receipt",
	"err404_walk-to" => "go to",
	"err404_makeproj-in" => "create project at",
	"err404_helper" => "Helper",

	"footer_rights" => "Rules of participation",
	"footer_howitwork" => "How it works",
	"footer_good-proj" => "Successful projects",
	"footer_rights-sec" => "Security policy",
	"footer_register" => "Registration",
	"footer_mycab" => "My profile",
	"footer_watch-video" => "Watch video",
	"footer_good-deal-tree" => "Tree of good deeds",
	"footer_rating-status" => "Rating and Statuses",
	"footer_help-cat" => "Help categories",
	"footer_know-more" => "Know more",
	"footer_contact" => "Contact us",
	"footer_ofus" => "About us",
	"footer_contacts" => "Contacts",
	"footer_support" => "Help desk",
	"footer_questions" => "Ask question",
	"footer_crew" => "Team",
	"footer_other" => "Other",
	"footer_good-wall" => "Wall of good deeds",
	"footer_complaints" => "Complaints",
	"footer_sanctions" => "Penalties",
	"footer_up-projrating" => "How to improve project rating",
	"footer_up-userrating" => " How to improve user rating",
	"footer_start" => "Start",
	"footer_add-fb" => "Join us on Facebook",
	"footer_copyright" => "wayofhelp.com -  All rights reserved",

	"header_project-finish" => "Your project will end soon. <br> It will we automatically <br> stopped<br>",
	"header_up-power" => "Your <b>Power</b> during recent session increased by ",
	"header_eweryday-back" => "for daily visits",
	"header_visit" => "for portal visiting",
	"header_dream-like" => "for rating dream of other person",
	"header_for-givehelp" => "for provided help",
	"header_for-comment" => "for comment",
	"header_for-repost" => "for repost",
	"header_for-angday" => "for status <b>Angel of the day</b>.",
	"header_for-angweek" => "for status <b>Angel of the week</b>.",
	"header_for-bonusadd" => "for <b> bonus suspension</b>.",
	"header_new-dream" => "New dream appeared on the website",
	"header_new-dream-like" => "rate </a> it and <br>get additional <br>Power.",
	"header_new-status" => "You got new status",
	"header_for-activity" => "for your activity and constant strive for good deeds,<br>",
	"header_your-newstatus" => "You got new status",
	"header_more-gooddeal" => "<br><br>The number of good deeds increases thanks to you!!!<br><br>",
	"header_go-to" => "Follow",
	"header_link" => "link",
	"header_share-it" => "and share this great <br> news with friends. ",
	"header_to-newstatus" => "Before status",
	"header_needed" => "not enough",
	"header_earn-angelits" => "How to earn angelits",
	"header_project-finished" => "<b>Congratulations!</b> Your project raised<br> required amount. <br> To get money, <br>you should",
	"header_share" => "share",
	"header_share-in-soc" => "project on<br> your social networks.<br>",
	"header_your-position" => "Your rating position:",
	"header_up-rating" => "Improve rating",
	"header_show-count" => "Number of times when your project is shown :",
	"header_show-up" => "Increase the number of times when your project is shown ",
	"header_new-msg" => "New messages",
	"header_new-offer" => "New offers",
	"header_new-offer-think" => "New requests for things",

	"info-about_watch-how" => "Watch how",
	"info-about_your-name" => "Your name ",
	"info-about_your-phone" => "Your telephone number",
	"info-about_what-can-help" => "How can you help?",

	"info-angels_angel-day-week" => "Angel of the day and week",
	"info-angels_ang-day" => "Angel of the day characterizes the user as one of 5 best virtues of the DAY. It is calculated as the earned POWER per day, or for certain special merits (at the discretion of the platform administrator)",
	"info-angels_ang-week" => "Angel of the week characterizes the user as one of 5 best virtues of the WEEK. It is calculated as the earned POWER per week, or for certain special merits (at the discretion of the platform administrator ",

	"info-project_how-makeproj" => "How to create project",
	"info-project_instrustion-1" => "1. Think of bright, clear and attractive title for your project, reflecting its essence and distinguish it among other projects.",
	"info-project_instrustion-2" => "<br><br>2. Provide clear description in a few sentences. Write correctly and clearly, your goal is to invoke a desire to help your project.
            <br> It is not allowed to copy texts from the Internet - your description should be unique.",
	"info-project_instrustion-3" => "<br><br>3. Select the appropriate category of help, depending on whether your project is aimed at raising money or help.",
	"info-project_instrustion-4" => "<br><br>4. If the project is aimed at raising money, then specify the amount you want to raise. Note that you can withdraw money only when collected at least 60% of that amount.
            <br> Our advice: do not specify too large amount (people are willing to help projects that can be successfully ended in the near future).
            <br> It is better to start another project after successful completion of this one and continue raising money.",
	"info-project_instrustion-5" => "<br><br>5. Do not limit the term of the project. If help is needed urgently, write about it in the description. After expiration of the project it cannot be extended.
            <br> And if you fail to get 60% to that time, money will not be paid to you, and will be transferred to other projects with similar themes.",
	"info-project_instrustion-6" => " <br><br>6. Select beautiful and attractive photos (several) of a relevant theme for the project.
            <br> Even if funds are raised for the medical treatment, photos of the person shall be in positive mood (for example, a photo where the person is healthy).
            <br> It is not allowed to use photos of sick people, sick parts of the body, shocking and low-quality photos.
            <br> Best of all is the unique photo, i.e., never seen before anywhere else on the Internet. It will increase traffic to your project.",
	"info-project_instrustion-7" => "<br><br>7. Be sure to immediately link your account to social networks (in your profile) as it is the only way to earn scores and money for the activity.",
	"info-project_instrustion-8" => "<br><br>8. To edit the project use your profile, click \"I need help\" and select \"edit\" next to the title of the project.",
	"info-project_instrustion-9" => "<br><br>9. Projects violating the rules do not pass moderation and are deleted.",

	"info-projectpract_example" => " A real-world example of raising funds for your project.",
	"info-projectpract_content" => "Content",
	"info-projectpract_hide" => "hide",
	"info-projectpract_show" => "show",
	"info-projectpract_decor" => "Design.",
	"info-projectpract_fundraising" => "Fund raising.",
	"info-projectpract_guaranteed-shows" => "Guaranteed impressions.",
	"info-projectpract_social" => "Social networks.",
	"info-projectpract_results" => "Results",
	"info-projectpract_conclusions" => "Findings",
	"info-projectpract_for-example" => "Let’s consider one successful <a target=\"_blank\" href=\"http://wayofhelp.com/proj/view/2740/\"> project </a> as an example. We chose it because at the beginning it seemed to have no chances of being successful. In fact, it was the third project in a row to raise funds for this person. Two previous projects were focused on raising funds to pay for the first part of the surgery and rehabilitation and in total the amount of <b> UAH 70.000 was raised</b>. I.e., circle of persons willing to help was almost involved in full. However, it was decided to create the project and, as it turned out, it was not in vain.",
	"info-projectpract_recomendation" => "Recommendations of the project authors:",
	"info-projectpract_making" => "1. Designing.",
	"info-projectpract_moder" => "We had no difficulties at designing and moderation of the project. Read",
	"info-projectpract_tips" => "tips",
	"info-projectpract_make-proj" => "when you create a project, and remember that a well-designed project is 2 times more likely to be successful. In short, use real photos, describe in nice and correct way your problem (the best is to invoke emotions).",
	"info-projectpract_after-then" => "Once the easiest stage has been passed, direct fundraising starts. The success of the project depends on many factors, but I would mention only two basic: get guaranteed impressions and work with social networks. You have to make efforts but it is worth doing.",
	"info-projectpract_im-tell" => " I would like to tell you how I did it - step by step, and the general guidelines are",
	"info-projectpract_here" => "here",
	"info-projectpract_project-added" => "Thus, the project was created on September 7 and received <b> 300 guaranteed impressions </b> in the system. In total I had 981 scores for activity. 1 score = 1 impression, but I spent most of them for previous projects. Everything is simple: your activities earn scores and you receive guaranteed impressions. The main thing is not to stop and to act regularly. For the first two days <b> I managed to collect UAH 600 </b> and, what is most important, to get regular free impressions, but fund raising slowed down ",
	"info-projectpract_social-work" => "It's time to work with social networks. Although, these processes shall be in parallel at once, because the amount of raised money has also a positive effect on further fund raising. One source complements the other source. In general, on September, 15 I made a repost of my project on social networks.",
	"info-projectpract_earlier" => "As stated previously it was already the 3rd project in a row. It may explain the lack of results. I was ready for it and started sending messages on social networks to communicate with my friends and ask them to make a repost and add a few own words. Thus my message could reach the friends of my friends, and could add to credibility of the project.",
	"info-projectpract_more-visits" => "It happened in such a way. After a personal request, visits to the project increased significantly. After 22 individual requests for repost, I got 22 + extra 34 reposts. The most interesting thing happened later, when one of the leaders of Ukrainian national beach soccer team - Vitaly Sidorenko – read about the project and made <a href=\"https://www.facebook.com/photo.php?fbid=1060194300712891&set=a.429075207158140.93215.100001669929144&type=3&theater\">auction</a>, where <b>we managed to raise additional UAH 10.000</b>.",
	"info-projectpract_money-auction" => "Money from the auction are not included in the on-line project, because they were transferred directly and not to the platform. Offline amount is also unknown.",
	"info-projectpract_money-ofauction" => "Total: <b>UAH 10.000</b> after auction + <b>UAH 6.000</b> after the project + unknown amount from direct transfers. The project could raise more, but it was no longer necessary, and it was closed.",
	"info-projectpract_experience" => "I think my experience will be useful to people who are just going to do the project and I will offer you some personal conclusions:",
	"info-projectpract_big-yours" => "More",
	"info-projectpract_points" => " scores you have",
	"info-projectpract_chanse" => "at project creation, more chances you have to promote project and get more impressions.",
	"info-projectpract_row-social" => "It is unlikely to quickly raise the necessary sum without promotion of the project on social networks. Although if you have enough time, it is quite possible.",
	"info-projectpract_ask-friend" => " Ask friends with the most contacts on social networks.",
	"info-projectpract_make-repost" => "Repost project on all social networks where you have accounts.",
	"info-riseproject_instruction" => "Instruction on project promotion on WayofHelp.",
	"info-riseproject_many-questions" => "We get a lot of questions about how to properly promote the project on our platform, and whether there are effective methods to promote the project among people whom you don’t know in person. <br>
        Before detailed description of the proper way for a successful promotion of your project, read about some facts: ",
	"info-riseproject_repost" => "Repost (sharing) of your project on social networks (Facebook, Vkontakte, Odnoklassniki), increases donation by 400%.",
	"info-riseproject_first-stage" => " At the initial stage you get the majority of donations directly from people who know you or are related to your circle of friends.",
	"info-riseproject_self" => "By taking",
	"info-riseproject_active-actions" => "active measures",
	"info-riseproject_can-increase" => "you can increase donations for the project by 300%.",
	"info-riseproject_successful-projects" => "Most successful projects of WayofHelp received first the support from close circle of friends, before they became successful and attracted attention of the press.",
	"info-riseproject_strangers" => "Unfamiliar people would donate money to the project much more willingly if it had received preliminary support from its circle of friends.",
	"info-riseproject_properly-designed" => " Properly designed project collects 2 times more donations.",
	"info-riseproject_thus" => "Thus, for the project to become popular and promote on the Internet and media, you need to get the initial donation. It can be achieved in two ways
:<br>",
	"info-riseproject_share-soc" => "Share the project on social networks and promote it in the circle of friends and acquaintances;",
	"info-riseproject_free-shows" => "Get many free impressions in TOP of the platform thanks to",
	"info-riseproject_active-action" => "activities",
	"info-riseproject_some-try" => " There are several effective ways to increase the donations in your group.",
	"info-riseproject_your-social" => "1. Prepare your social network.",
	"info-riseproject_remember" => "Remember, your main goal is to raise money for the project. Other people will help only after your friends have helped. <br>
       Not everyone in your circle of friends will know about WayofHelp platform and how to donate to your project through it. Therefore it is important to contact them personally before you share your project on Facebook, Vkontakte and Odnoklassniki. You should tell them about the upcoming fundraiser. <br>
       Come up with a compelling story and the reason why you need their support. You can send an email or call in person to every friend. It will take some time, but believe me, it is worth doing.",
	"info-riseproject_say-thanks" => "2. Thank people who have helped you.",
	"info-riseproject_public-thanks" => "3. Use social networks to thank every person who contributed to your project. This will not only allow people feeling joy of their good initiatives but will also provide additional promotion for your project! <br>
       You will always know who has helped you. After every receipt of help you will receive an e-mail notification. Follow the link in your e-mail and click the button 'Thank'. It is very important to make clear to every person that such help is of great importance for you. In addition, there is a high probability that such person will help once again.",
	"info-riseproject_pray" => "You need to ask if you want to get help.",
	"info-riseproject_not-expect" => "Do not expect that people will repost your project on their own. Ask them to share it, or better ask to personally answer for fund raising. Thus you will have a chance to attract donations from the second level of circle of your friends. <br>
       The same strategy is applied to the request for a donation. Do not be afraid to ask for a donation directly from people of your circle, because in the worst case you just will not get help. Although the statistics shows the opposite: 80% of people will help, if ask them directly.",
	"info-riseproject_mind-local" => "4. Think local at start.",
	"info-riseproject_first-then" => "Before you think about the wide audience, think local. This will help to get the first donations and promote your project. <br>
        For example, if you are the member of any organization, ask members of the organization to share information about you. <br>
        In addition, you can refer to a local newspaper in your city and share your story.",
	"info-riseproject_top-portal" => "5. At the same time promote the project to be in the top on the portal.",
	"info-riseproject_many-visitors" => "Portal WayOfHelp is daily directly attended by a huge number of people, willing to help. Your project should get their attention. To do this:<br>",
	"info-riseproject_right" => "Properly",
	"info-riseproject_issue-project" => "prepare</a> the project;",
	"info-riseproject_enough-impressions" => "To get sufficient number of impressions in TOP(by activity on the portal).<br>
       More impressions mean more chances to be noticed and get help.",
	"info-riseproject_step-by-step" => "6. Do it step by step.",
	"info-riseproject_above" => "Based on the foregoing, we can conclude that time spent for the promotion of project in alternative ways, whether bulletin boards or work with journalists, probably will not have any impact upon your project, unless you have made initial efforts in relation to: <br>
        your own social network; <br>
        promotion of your project to get top position on the portal.",
	"info-riseproject_im-think" => "I think that after you do it and see that your story is interesting, it would be appropriate to continue and share it with journalists. It is the only way to achieve success. In any case, our statistics approves it.",
	"info-rules_rules" => "Rules of WayofHelp platform",
	"info-rules_sanction" => "Penalties",
	"info-rules_punitive-sanction" => "Penalty<br>provisions",
	"info-rules_protection" => "Protection ",
	"info-rules_safity-rule" => "Safety<br>rules",
	"info-rules_project-rating" => "Project rating",
	"info-rules_rating-up" => "How to improve<br>the project rating",
	"info-rules_user-rating" => "User rating",
	"info-rules_user-ratup" => " How to improve<br>the user rating",

	"info-tree_see-clearly" => "Review visually ",
	"info-tree_tree" => "Tree",

	"info-work_how-it-work" => "How it works",
	"info-work_help-anyone" => "To help somebody on our website, use the thematic catalogue, which contains all the requests of our visitors. <br> If you have not yet created an account, please register on our website or login with your social network account.",
	"info-work_fizical" => "Private individuals",
	"info-work_db-requests" => "Base of requests",
	"info-work_im" => "I",
	"info-work_my-tree" => "My tree",
	"info-work_couch" => "Help<br>to move<br>the couch",
	"info-work_consult" => "Provide<br>legal<br>consultation",
	"info-work_surgery" => "Help<br> to raise money <br>for surgery",
	"info-work_make-help" => "I will help",
	"info-work_making-help" => "To<br>help",
	"info-work_good-deeds" => "ALL people will see your good deeds",
	"info-work_status-up" => "Your status will rise up",
	"info-work_angel" => "Angel",
	"info-work_shows-tree" => "You will watch growing your <br> own Tree of good deeds",
	"info-work_need-help" => "I need help",
	"info-work_take-help" => "Get<br>help",
	"info-work_take-help-deeds" => "Get help with money<br>and action",
	"info-work_make-dreams" => "Make your dreams come true",
	"info-work_influence" => "Influence promotion of <br>your project by yourself",
	"info-work_mytree-help" => "My tree<br>of help",
	"info-work_potential-good" => "Potential<br>good deeds of persons,<br>whom I helped to",
	"info-work_want" => " We wish you to enjoy navigation on the website and hope you find the person whom you want to help to.",

	"infoother_social" => "Social platform",
	"infoother_users" => "Users",
	"infoother_charity" => "Charity",
	"infoother_project" => "Project",
	"infoother_help" => "Help",
	"infoother_user" => "User",

	"login_site-login" => "Log in",
	"login_pass-reset" => "Reset password",
	"login_session-lost" => "Session finished",
	"login_mail-msg" => "You’ve got e-mail",
	"login_toyour-mail" => "Link for password reset was sent to your e-mail.",
	"login_pass-changed" => "Password changed",
	"login_youare-changepass" => "You have just changed password for your account.",
	"login_complete-data" => "Fill in data for password reset",
	"login_new-pass" => "New password:",
	"login_repeat-pass" => "Repeat password:",
	"login_restore-pass" => "Restore password",

	"main_fontitle" => "WayOfHelp background",
	"main_helping-others" => "Help other",
	"main_help-myself" => "or get help for yourself!",
	"main_what-do" => "What should I do?",
	"main_registered" => "Register",
	"main_start-your-tree" => "Start own Tree of good deeds <br>and watch it growing",
	"main_how-been" => "HOW WILL IT BE?",
	"main_successful-proj" => "Successful projects",
	"main_tell-your-friends" => "Tell your<br>friends",
	"main_and-then" => "And then...",
	"main_bonus" => "Suspended bonuses",
	"main_video-btn" => "Video button",
	"main_more-ofus" => "More about us",
	"main_reviews" => "Feedback about received help",
	"main_user-photo" => "User’s photo",
	"main_total-activity" => "Most active",
	"main_active-user-photo" => "Photo of active user",
	"main_popular-project" => "Popular projects",

	"myprojects_charitable" => "Charitable contribution",
	"myprojects_payed" => "Pay",
	"myprojects_waiting" => "Waiting for confirmation",
	"myprojects_rating" => "Rating",
	"myprojects_accept" => "Accept",

	"project_small-photo" => " This photo is too small, please download another one.",
	"project_size" => "Resolution of the photo shall exceed 400x300",
	"project_no-more-project" => "You cannot create more than one money raising project. <br> To do it, stop your current money raising project<br>",
	"project_pay-method" => "Select one of listed systems",
	"project_checked" => "Checked",
	"project_complate-proj" => "How to fill in the project in a proper way",
	"project_servise" => "Service of good deeds",
	"project_hide" => "hide",
	"project_no" => "No",
	"project_yes" => "Yes(select any number of categories):",
	"project_send" => "Edit",
	"project_login" => "Log in",
	"project_answer" => "Reply",
	"project_no-login" => "You are not logged in",
	"project_if-you-want" => "To write comments, please",
	"project_make-login" => "log in",
	"project_if-nouser" => "If you do not have an account, please",
	"project_registered" => "register",
	"project_watch-with-this" => "Similar projects viewed by people",
	"project_login-first" => "Log in or register first!",
	"project_point-comm" => "score for <br><b>comment</b> <br>",

	"project-prepay_portal-terms" => "According to rules of WayOfHelp portal",
	"project-prepay_participate" => "<br> for your project to participate in allocation of portal internal fund equal to ",
	"project-prepay_uah" => "UAH",
	"project-prepay_every-day" => " every day) and for the daily receipt of funds for the project (according to your daily rate of good deeds), you shall <b> one-time </b> contribute to the portal fund",
	"project-prepay_money" => "These funds will be allocated to the projects of the users.<br>
       <br> Get local currency for active operations and convert it into real money for your project. <br>",
	"project-prepay_read-more" => "Read more details",
	"project-prepay_here" => "here",


	"project-prepay_money-table" => "Table with data on money allocation by projects",
	"project-prepay_all-your-money" => "All your money will be allocated to other projects.",
	"project-prepay_good-tree" => "Your Tree of Good Deeds  will grow.",
	"project-prepay_good-job" => "WayOfHelp – we increase the number of Good Deeds!!!",
	"project-prepay_updating-account" => "Replenishment of account",
	"project-prepay_payed" => "To pay",
	"project-prepay_suc-launched" => "You project has been successfully launched!",
	"project-prepay_under-terms" => "Under terms of WayOfHelp portal",
	"project-prepay_to-participate" => " For your project to participate in the allocation of internal fund equal",
	"project-prepay_uah-to-project" => "UAH per any project (one or several). <br>
               <br> Get local currency for active operations and convert it into real money for your project. <br>",

	"registration_reg-on-site" => "Registration on website",
	"registration_edit-profile" => "Profile editing ",
	"registration_my-cabinet" => "My profile",
	"registration_step-3" => "Step 3. – Account is activated",
	"registration_step-2" => "Step 2. – Registration is completed",
	"registration_way-of-help" => "Specify categories to provide help",
	"registration_reg-document" => "Upload registration documents",
	"registration_certificate" => "Copy of certificate:",
	"registration_step-1" => "Step 1. – Contacts and registration data",
	"registration_reg-data" => " Contacts and registration data",
	"registration_employees" => "Employees:",
	"registration_agree" => "I agree with",
	"registration_rules" => "registration rules",
	"registration_undertake" => "and undertake to follow these rules while using WayOfHelp.com portal",

	"siterules_rule-concept" => "Rules and main terms",
	"siterules_independent" => "of independent social online platform<b>WayofHelp</b>",
	"siterules_rules-govern" => " These rules govern the basic principles of relations of users",
	"siterules_platform" => "of WayofHelp platform.",
	"siterules_in-this-page" => "This page provides the basic concepts, the structure of the portal",
	"siterules_learn-the-rules" => " as well as the rules that govern the basic principles of relations",
	"siterules_between-platform" => "of the users of the platform.",
	"siterules_about-project" => "About project",
	"siterules_in-outline" => "in general",
	"siterules_concept" => "Main concepts",
	"siterules_category" => "Categories",
	"siterules_users" => "of users and their",
	"siterules_less" => "opportunities",
	"siterules_principles" => "Principles",
	"siterules_visual" => "of visualization of good ",
	"siterules_good-job" => " deeds of user",
	"siterules_work-rule" => "Rules of use",
	"siterules_platforms" => "of WayofHelp platform",
	"siterules_donor" => "To donors",
	"siterules_stat" => "Report on",
	"siterules_charity" => "charity",
	"siterules_proj" => "project",
	"siterules_pol" => "Policy",
	"siterules_confident" => "of confidentiality",
	"siterules_fees-pay" => "Fees and charges",

	"successfull_success-project" => "Successful projects. Thanks to you, the number of successful projects may increase.",
	"successfull_uah" => "UAH",
	"successfull_rub" => " RUB",
	"successfull_take-think" => "Things receipt",
	"successfull_people-help" => " People helped",
	"successfull_people" => " persons",
	"successfull_user" => "Users",

	"user_no-check" => "Not selected",
	"user_time" => "times",
	"user_share-tree" => "Share the Tree of<br>the user",
	"user_open-tree" => "Open Tree",
	"user_good-job-friend" => "of Good Deeds and share<br>
               it with friends:<br>",

	"getpaymnt_payed" => "Pay directly to the user",
	"getpaymnt_steps" => "1. Choose one of the user’s settlement accounts and provide aid through payment system convenient for you. <br>
               2. Then open your profile and select tab “<b>I provide help</b>”. <br>
               3. Find this project in the list and select \"<b>I’ve helped</b>\". <br>
               4. Your help will immediately be displayed on your \"<b>Tree of Good Deeds </b>\"",
	"getpaymnt_payment" => "Payment system",
	"getpaymnt_req" => "Banking details",
	"getpaymnt_pay-sum" => "Pay required amount for these details<br> using convenient way.",
	"getpaymnt_agree" => "To confirm",
	"getpaymnt_push-btn" => "After payment click the button <b> “To confirm”</b>.",
	"getpaymnt_help-user" => " Help the user through our payment systems",
	"getpaymnt_help-tree" => " Help will be automatically displayed on your Tree of Good Deeds .",

	"cabpopup_road-to-1000" => " A journey of a thousand miles begins with a single step!",
	"cabpopup_we-offer" => "We offer you to make a very simple and very <b>important step</b> for our project! <br> Share information about <b>WayOfHelp</b>.
   <br>Let more people find out about it.
   <br>It will be <b>your first good deed.</b> <br>
   It will cause following results:",
	"cabpopup_your-tree" => "Your Tree of Good Deeds will start growing (it will be displayed in your profile);",
	"cabpopup_power-up" => "You will additionally get <b>50</b> scores of internal currency.",
	"cabpopup_good-start" => "It is a good start!",
	"cabpopup_share" => "To share:",
	"cabpopup_add-photo" => "Add photo",
	"cabpopup_add-avatar" => "Add user image (photo)",
	"cabpopup_in-profile" => "to your profile",

	"getbonus_take-bonus" => " To get the bonus, guess the <br> chip where it is hidden.
             The bonus will be yours, if you can find the chip with bonus using not more than three attempts. <br>Good luck!",
	"getbonus_trys" => "Attempts left:",
	"getbonus_no-more-try" => "You have no attempts left. <br> To get new opportunity to win bonus,<br> provide any help required at the project. <br>More Good Deeds mean more attempts",
	"getbonus_winner" => "You have just won suspended bonus!",
	"getbonus_lose" => "You failed to find suspended bonus.<br>",
	"getbonus_unused-chance" => "Unused chances to win bonus: ",
	"getbonus_more-goodjob" => "More Good Deeds mean more opportunities ",

	"infopopup_action" => "Action on Wayofhelp, portal of Good Deeds!",
	"infopopup_make-project" => " Create the project on our website and receive money from the fund on daily basis",
	"infopopup_exercise" => "for its implementation",
	"infopopup_watch" => "Review receipts of other projects",
	"infopopup_choise" => "Decide on amount to be transferred to your project",

	"newreg_to-mail" => " Confirmation of registration has been sent to your e-mail. Follow the link in email to activate your account.",
	"newreg_help-this-proj" => "Helping this project you start the chain of Good Deeds. Thus it is very important to log in using own account or register it, if necessary.",
	"newreg_helping-this-proj" => " Helping this project you start the chain of Good Deeds.",
	"newreg_just" => "It will take only",
	"newreg_sec" => "2 seconds",
	"newreg_err-name" => "Invalid name",
	"newreg_name" => "Name",
	"newreg_fname" => "Surname",
	"newreg_err-fname" => " Invalid surname",
	"newreg_err-mail" => "Invalid or already existing e-mail ",
	"newreg_err-pass" => "Invalid password ",
	"newreg_pass" => " Password",

	"bonus-cont_call-help" => "Requests for help",

	"catcont_success" => "Successful projects",
	"catcont_call-help" => "Requests for help",

	"reg_err-telephone" => "Invalid telephone number",

	"cabcont_err-mail" => "Please enter your E-mail/Login",

	"cabcont_mistake-mail" => "Invalid E-mail/Login",
	"cabcont_already-registered" => " E-mail/Login has been already registered. Enter other e-mail.",
	"cabcont_your-city" => "Specify your primary city",
	"cabcont_required-fields" => "Fill in all obligatory fields.",
	"cabcont_scope-published" => "Your review and rate have been published.",
	"cabcont_error" => "Publishing of your review and rate failed.",
	"cabcont_my-profile" => "My profile",
	"cabcont_error-msg" => "Message sending failed. Addressee not found.",
	"cabcont_error-sending" => "Message sending failed. You cannot send message to yourself.",

	"inst-cont_code-lang" => "No code language",

	"login-cont_pass-no-same" => "Passwords differ or fields are empty. Enter valid passwords.",

	"payscallback_mistake-txt" => "Invalid signature: ",
	"payscallback_ok-txt" => "Valid signature: ",

	"projcont_no-title" => "Specify title of your project",
	"projcont_no-catalog" => " Specify section of your project ",
	"projcont_no-city" => " Specify city of your project ",
	"projcont_data-err" => "Invalid date of project launch",
	"projcont_many-help" => "Value of help shall exceed zero.",
	"projcont_biggest-zero" => "Value of help shall be accepted numerical value and exceed zero.",
	"projcont_born-code" => "Your confirmation code: ",
	"projcont_callback" => "Confirmation code is sent to number ",

	"regcon_pass-no-same" => "Entered passwords do not coincide",
	"regcon_no-pass" => "Enter password to yout account",
	"regcon_pass-size" => "Password shall be 6 symbols at least",
	"regcon_region" => "Specify your region",
	"regcon_site-rule-agree" => "Agree with rules of website",
	"regcon_code-controll" => "Invalid control code",
	"regcon_reg-error" => "Registration error",
	"regcon_set-you-city" => "Specify city of residence",
	"regcon_email-mistake" => "Invalid E-mail ",

	"reqgretir_tit" => "Thank user for provided help",
	"rateinfo_tab-pos" => "Position",
	"rateinfo_tab-info" => "Information about user",
	"rateinfo_tab-rate" => "Rating",
	"rateinfo_сurr-pos" => "You take the last position in rating ",
	"rateinfo_city-pos" => "Position by my region",
	"rateinfo_reg-pos" => "Position by general rating",
	"rateinfo_txt-gt" => "of Good Deeds",
	"rateinfo_u-pos" => "Your current rating ",
	"rateinfo_u-uprate" => "You can improve your rating",
	"rateinfo_u-uprategr" => "Join the group",
	"rateinfo_u-upratesoc" => "(+ 20 scores for every social network)",
	"rateinfo_u-uprate" => "You can improve your rating",
	"rateinfo_gr-done" => "You have joined WayOfHelp group already",
	"rateinfo_invite-friend" => "Invite friend",
	"rateinfo_invite-friendi" => "(+ 30 scores per every friend)",
	"rateinfo_u-refid" => "Your referral link",
	"rateinfo_u-refid-expl" => "Copy the link and give it to their friends. (It should be inserted in the browser URL)",
	"rateinfo_share-proj" => "Share<br> other projects",
	"rateinfo_help-proj" => "Help other<br>projects",
	"rateinfo_all-proj" => "More...",
	"rateinfo_tip-subs" => "scores for<b>subscription</b>",

	"projview_u-tree" => "Tree of the user",
	"projview_u-ratecomm" => "Thank you",

	"projpopup_fpop-txt" => " The person performing a lot of good deeds for others<br>
        now needs your help.<br><br>
       Any help, including repost of his project in social networks, <br>
        is very helpful to this user.<br><br>
       Perform good deeds with us!",
	"projpopup_fpop-help" => "To help",
	"projpopup_fpop-share" => "To share",
	"projpopup_tpop-text" => "Your repost will greatly help this project!",
	"projpopup_tpop-text-next" => "Not ready to help yet?<br><br>You can significantly promote this project<br> just sharing it in social networks",

	"projcreate_date" => "Date",
	"projcreate_today" => "Today",
	
	"proj_anon-username" => "Anonymous",

	"basicjs_tip-adder" => "You've got",
	"basicjs_load-region" => "Specify your region/oblast",
	"basicjs_load-city" => "Specify your city",
	"basicjs_login-err" => "Wrong username or password. Login failed.",
	"basicjs_checklog-err" => "Please specify E-mail used as login",
	"basicjs_checkhlp-err-1" => "Entered value is not a valid number.",
	"basicjs_checkhlp-err-2" => "Help shall exceed zero.",
	"basicjs_checkmsg-err" => "The message should contain text.",
	"basicjs_checkstar-err" => "You should add a comment to your rate",
	"basicjs_count" => "X symbols left"
];


$txts = [1 => $txt_ru, 2 => $txt_eng];