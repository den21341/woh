<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

//setup php for working with Unicode data
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_language('uni');
mb_regex_encoding('UTF-8');
ob_start('mb_output_handler');

////////////////////////////////////////////////////////////////////////////////
// Defines
define("UHCATMOD_SIMPLE", "simple");
define("UHCATMOD_SUBMOD", "submodel");
define("UHCATMOD_COLORS", "colors");
define("UHCATMOD_SIZES",  "sizes");
define("UHCATMOD_COLSIZE","color_sizes");

define("UHCATADD_CAR", false);

define("UH_LINKMODE_PHP", "php");
define("UH_LINKMODE_HTML", "html");

define("UH_ENC_UTF8", "UTF-8");
define("UH_ENC_1251", "Windows-1251");

define("UH_MAILFMT_PLAIN", "plain");
define("UH_MAILFMT_HTML", "html");

define("UH_RUNMODE_INSTALL", "install");
define("UH_RUNMODE_RUN", "run");


////////////////////////////////////////////////////////////////////////////////
// Main cofigurations
$UHCMS_CONFIG = Array();


// Database
$UHCMS_CONFIG['DB_HOST'] 		= "localhost";
$UHCMS_CONFIG['DB_USER'] 		= "root";
$UHCMS_CONFIG['DB_PASSWORD'] 	= "";
$UHCMS_CONFIG['DB_NAME'] 		= "uh170_jhdb";
$UHCMS_CONFIG['DB_PREFFIX'] 	= "jhlp_";

define("DB_PREFFIX", $UHCMS_CONFIG['DB_PREFFIX']);

// Main domains for website
$UHCMS_CONFIG['WWWHOST']		= "http://work/data0512/public_html/";		// Main domain for website
$UHCMS_CONFIG['IMGHOST']		= "http://work/data0512/public_html/";		// Main domain for images in page templates
$UHCMS_CONFIG['PICHOST']		= "http://work/data0512/public_html/";		// Main domain for uploaded pics

$UHCMS_CONFIG['MVC_PATH']		= "uh_cms/";						// Path to folders with class files

define('WWWHOST', 	$UHCMS_CONFIG['WWWHOST']);
define('WWWHOST_TEST', 	$UHCMS_CONFIG['WWWHOST'].'testrev/');
define('IMGHOST', 	$UHCMS_CONFIG['IMGHOST']);
define('PICHOST', 	$UHCMS_CONFIG['PICHOST']);
define('MVC_PATH', 	$UHCMS_CONFIG['MVC_PATH']);

$UHCMS_CONFIG['NAME_RU']		= "WayOfHelp";
$UHCMS_CONFIG['NAME_EN']		= "WayOfHelp";

// Engine basic preferences
$UHCMS_CONFIG['LINK_MODE']		= UH_LINKMODE_HTML;
$UHCMS_CONFIG['DEF_ENC']		= UH_ENC_UTF8;
$UHCMS_CONFIG['DEBUG_ON']		= true;
$UHCMS_CONFIG['WITH_AUTH']		= true;

define('UHCMS_LINK_MODE', $UHCMS_CONFIG['LINK_MODE']);
define('UHCMS_DEF_ENC', $UHCMS_CONFIG['DEF_ENC']);

$UHCMS_CONFIG['RUN_MODE']		= UH_RUNMODE_INSTALL;

// Catalog engine preferences
$UHCMS_CONFIG['CAT_MODE']		= UHCATMOD_SIMPLE;		//"simple";						// submodel, colors, sizes, colors+sizes
$UHCMS_CONFIG['CAT_FILTERS']	= true;					// Use filters in catalog
$UHCMS_CONFIG['CAT_FILTERS_MAXLEV'] = 5;				// Maximum filters at once
$UHCMS_CONFIG['CAT_MAKEPOS']	= 2;					// 0 - слева в названии, 1 - справа в названии, 2 - скрывать
$UHCMS_CONFIG['CAT_PACKSELL']	= false;				// Pack selling
$UHCMS_CONFIG['CAT_COMPARE']	= true;					// Enable product comparing
$UHCMS_CONFIG['CAT_NAVBRAND']	= true;					// Navigate by brands in catalog (in titles, breadcrumbs, etc.) - true|false

// Additional modules
$UHCMS_CONFIG['MAIL_FORMAT']	= UH_MAILFMT_HTML;		//"html";			// Mail sender format, could be "html" or "plain"
$UHCMS_CONFIG['NPOSHTA']		= false;
$UHCMS_CONFIG['SMS_EPOSHTA']	= false;
$UHCMS_CONFIG['UNISENDER']		= false;
$UHCMS_CONFIG['CAPTCHA_SOFT']	= "";					//"securimage";

// Shopping bonus algorithm
define('BONUS_BASE', 5);			// 5% бонусных баллов за каждый товар на счет в бонусной системе начисления баллов, для $BONUS_TYPE_POINTS

define('BONUS_TYPE_NO',		0);
define('BONUS_TYPE_NAKOPIT',1);			// Накопительная бонусная система, скидка рассчитывается от общей суммы всех заказов
define('BONUS_TYPE_POINTS',	2);			// Бонусная система начисления бонусных гривен при оформлении заказа

$UHCMS_CONFIG['SHOP_BONUS_SYSTEM']	= BONUS_TYPE_NO;

// Admin panel configs
define('FILEMAN_POPUP_W', 750);
define('FILEMAN_POPUP_H', 480);

////////////////////////////////////////////////////////////////////////////////
// Other basic presets
define('NEWS_NEWS',		0);
define('NEWS_ARTIC',	1);
define('NEWS_REVIEW',	2);

define('BANNER_POS_FOOT',	0);
define('BANNER_POS_RIGHT',	1);

define('BANNER_TYPE_FLASH', 0);
define('BANNER_TYPE_PIC',	1);

///////////////////////////////////////////////////////////////////////////////
// Defines for WayOfHelp
define('UKRAINE', 1);
define('RUSSIA', 2);

define('USR_TYPE_PERS', 0);
define('USR_TYPE_COMP', 1);
define('USR_TYPE_ORG', 2);

$UHCMS_CONFIG['USR_TYPES']	= Array("Частное лицо", "Компания", "Организация");

//define('PROJ_PROF_HELP',	1);
//define('PROJ_PROF_EVENT',	2);

define('PROJ_ANY',	-1);
define('PROJ_NEEDHELP',	0);
define('PROJ_SENDHELP',	1);
define('PROJ_EVENT',	2);
define('PROJ_THINGS',	3);

define("RSECT_ID_DREAM", 21);
define("RSECT_ID_THING", 77);

define('PROJ_STATUS_ALL',	-10);
define('PROJ_STATUS_STOP',	-1);
define('PROJ_STATUS_RUN',	0);
define('PROJ_STATUS_ENDED',	2);
define('PROJ_STATUS_PAYED',	3);
define('PROJ_PAYED_DONE', 1);
define('PROJ_PAYED_NONE', 0);

define('PROJ_TYPE_MONEY',	0);
define('PROJ_TYPE_HUMAN',	1);
define('PROJ_TYPE_CONSULT',	2);
define('PROJ_TYPE_OTHER',	3);

define('REQ_TYPE_NONE',	-1);
define('REQ_TYPE_MONEY',	0);
define('REQ_TYPE_HUMAN',	1);
define('REQ_TYPE_OTHER',	2);
define('REQ_TYPE_THING',	3);

define('REQ_STATUS_ALL',		-10);
define('REQ_STATUS_NEW',		0);
define('REQ_STATUS_CONFIRM',	1);
define('REQ_STATUS_DECLINE',	-1);	// Declined by reciever
define('REQ_STATUS_CANCEL',	-2);		// Canceled by sender

define('MSG_STATUS_ALL',	-1);
define('MSG_STATUS_NEW',	0);
define('MSG_STATUS_READ',	1);

define('BONUS_QUIZ_STATUS_RUN',	0);
define('BONUS_QUIZ_STATUS_USED', -1);
define('BONUS_QUIZ_STATUS_WON', 1);

///////////////////////////////////////////////////////////////////////////////
// Defines for online store
define('PROD_STATUS_EMPTY', 0);
define('PROD_STATUS_NEW',	1);
define('PROD_STATUS_SPEZ',	2);
define('PROD_STATUS_SOON',	3);
define('PROD_STATUS_HIT',	4);
$UHCMS_CONFIG['PROD_STATUS']	= Array("-----", "Новинка", "Спец-цена", "Скоро", "Хит продаж");

//$UHCMS_CONFIG['CAT_FILTERS_MAXLEV']   - Вынесено определение вверх
//$CATFLT_MAX_LEV		= 5;

define('CATBRAND_SHOW_LEFT',	0);
define('CATBRAND_SHOW_RIGHT',	1);
define('CATBRAND_SHOW_NO',		2);
$UHCMS_CONFIG['CATBRAND_SHOW']		= Array("Слева", "Справа", "Не показывать");

define('PACK_DISC_PER',	0);
define('PACK_DISC_VAL', 1);
define('PACK_DISC_GIFT',2);

define('ORDER_STATUS_ERR',	0);
define('ORDER_STATUS_NEW',	1);
define('ORDER_STATUS_WAIT',	2);
define('ORDER_STATUS_REAL',	3);
define('ORDER_STATUS_COMPL',4);
define('ORDER_STATUS_READY',5);
define('ORDER_STATUS_DELIV',6);
define('ORDER_STATUS_DONE',	7);
define('ORDER_STATUS_RETURN',8);

define('ORDER_PAYSTAT_NO',	0);
define('ORDER_PAYSTAT_YES',	1);

define('ORDER_PAYTYPE_NAL',		0);
define('ORDER_PAYTYPE_BN',		1);
define('ORDER_PAYTYPE_NALOG',	2);

define('ORDER_DELIV_NO',	1);
define('ORDER_DELIV_COURIER',2);
define('ORDER_DELIV_UKRAINE',3);

$UHCMS_CONFIG['CDELIV_STR']		 = Array("", "Самовывоз", "Доставка по Киеву", "Доставка по Украине");
$UHCMS_CONFIG['CDELIV_COUNTRIES']= Array("Украина");
$UHCMS_CONFIG['CPAYTYPE_STR']	 = Array("Наличными", "Безнал", "Наложный платеж");

define('CURRENCY_NAME',	"грн");
define('CURRENCY_DB',	"grn");
define('CURRENCY_OUT',	"grn");

///////////////////////////////////////////////////////////////////////////////
// Dasic defines for admin panel
define('GROUP_NO',		0);		// No group is selected (when no logon is made)
define('GROUP_ADMIN',	1);		// Administrators Group
define('GROUP_MANAGER',	2);		// Sales manager
define('GROUP_OPERATOR',3);		// Content operator
define('GROUP_SEO',		4);		// SEO operator

define('FIELD_TYPE_EDIT',		1);
define('FIELD_TYPE_SELECT',		2);
define('FIELD_TYPE_OPTIONS',	3);
define('FIELD_TYPE_FLAG',		4);
define('FIELD_TYPE_TEXTAREA',	5);
define('FIELD_TYPE_HTML',		6);
define('FIELD_TYPE_RADIO',		7);
define('FIELD_TYPE_OPTIONS_ANY',8);
define('FIELD_TYPE_DIAP',		9);
//$FIELD_TYPE_FILE			= 9;

//$ENGLANGID					= 1;		//???????????

define('FILETYPE_DOC',	0);
define('FILETYPE_APP',	1);
define('FILETYPE_URL',	2);
define('FILETYPE_QL',	3);
$UHCMS_CONFIG['FILETYPE_STR']	= Array("Документ", "Приложение", "URL ссылка", "Опросный лист");

////////////////////////////////////////////////////////////////////////////////
// Default directories
define('SESSION_PATH',		"sesid");
define('FILE_DIR',			"files/");
define('DATA_DIR',			"pics/");

define('AWARD_DIR',			DATA_DIR."awards/");
define('AVATAR_DIR',		DATA_DIR."avatar/");

define('CAT_PHOTO_DIR',		DATA_DIR."proj/src/");
define('CAT_BIG_DIR',		DATA_DIR."proj/big/");
define('CAT_THUMB_DIR',		DATA_DIR."proj/thumb/");
define('CAT_SMTHUMB_DIR',	DATA_DIR."proj/ico/");
define('CAT_PROJ_DIR',	    DATA_DIR."proj/pr/");

define('BONUS_PHOTO_DIR',	DATA_DIR."bonus/src/");
define('BONUS_BIG_DIR',		DATA_DIR."bonus/big/");
define('BONUS_THUMB_DIR',	DATA_DIR."bonus/thumb/");
define('BONUS_SMTHUMB_DIR',	DATA_DIR."bonus/ico/");

define('NEWS_PHOTO_DIR', DATA_DIR."helpwall/src/");
define('NEWS_ICO_DIR', DATA_DIR."helpwall/ico/");
define('NEWS_PHOTO_ICO_W', 330);
define('NEWS_PHOTO_ICO_H', 240);


define('CAT_REGDOC_DIR',	DATA_DIR."regdoc/");

define('CAT_LOGO_DIR',		DATA_DIR."logo/");

define('WATERMARK_BIG',		"../img/watermark/big.png");
define('WATERMARK_THUMB',	"../img/watermark/small.png");

////////////////////////////////////////////////////////////////////////////////
// Catalog product's picture presets
define('JPEG_COMPRESS_RATIO',	90);

define('minW', 400);
define('minH', 300);

// Avatars
define('AVATAR_W',				200);
define('AVATAR_H',				200);

define('AVATAR_SM_W',			80);
define('AVATAR_SM_H',			80);

//Pics for product
define('BIGPIC_W',				800);
define('BIGPIC_H',				650);

define('THUMB_W',				308);
define('THUMB_H',				208);

define('SMTHUMB_W',				140);
define('SMTHUMB_H',				140);

define('PR_W', 740);
define('PR_H', 460);

//Pics for bonus
define('BBIGPIC_W',				800);
define('BBIGPIC_H',				650);

define('BTHUMB_W',				308);
define('BTHUMB_H',				208);

define('BSMTHUMB_W',			180);
define('BSMTHUMB_H',			180);

define('NOPHOTO_S_W',			70);
define('NOPHOTO_S_H',			70);
define('NOPHOTO_S2_W',			70);
define('NOPHOTO_S2_H',			70);
define('NOPHOTO_I_W',			154);
define('NOPHOTO_I_H',			146);
define('NOPHOTO_T_W',			320);
define('NOPHOTO_T_H',			320);

define('LOGOTHUMB_W',			227);		// ??????
define('LOGOTHUMB_H',			107);		// ??????

////////////////////////////////////////////////////////////////////////////////
// Page photos defines
define('PAGEPIC_DIR',			DATA_DIR."pages/");
define('PAGEPIC_ICO',			"ico/");
define('PAGEPIC_THUMB',			"thumb/");
define('PAGEPIC_PHOTO',			"photo/");

define('PAGEPIC_P_W',			1000);	// Photo galery big photo width
define('PAGEPIC_P_H',			1000);	// Photo galery big photo height
define('PAGEPIC_T_W',			800);	// Photo galery thumb photo width
define('PAGEPIC_T_H',			800);	// Photo galery thumb photo height
define('PAGEPIC_I_W',			150);	// Photo galery ico photo width
define('PAGEPIC_I_H',			150);	// Photo galery ico photo height

////////////////////////////////////////////////////////////////////////////////
// Photo galary defines
define('GALERY_DIR',			DATA_DIR."galery/");
define('GALERY_ICO',			"ico/");
define('GALERY_THUMB',			"thumb/");
define('GALERY_PHOTO',			"photo/");

define('GALERY_P_W',			1000);	// Photo galery big photo width
define('GALERY_P_H',			800);	// Photo galery big photo height
define('GALERY_T_W',			600);	// Photo galery thumb photo width
define('GALERY_T_H',			600);	// Photo galery thumb photo height
define('GALERY_I_W',			150);	// Photo galery ico photo width
define('GALERY_I_H',			150);	// Photo galery ico photo height

////////////////////////////////////////////////////////////////////////////////
// Database tables
// a/b Testing

define('TABLE_AB_TESTING', DB_PREFFIX."ab_testing");
define('TABLE_AB_TESTING_LIST', DB_PREFFIX."ab_testing_list");
define('TABLE_AB_TYPE', DB_PREFFIX."ab_test_type");

// Pays currency

define('TABLE_PAYS_CURRENCY', DB_PREFFIX."pays_currency");
define('TABLE_PAYS_GAME', DB_PREFFIX."pays_game");
define('TABLE_PAYS_GAME_DUMP', DB_PREFFIX."pays_game_dump");

//Buyer social network

define('TABLE_BUYER_SOCIAL', DB_PREFFIX."cat_buyer_social");

// Basic page functionality
define('TABLE_USER_GROUPS',			DB_PREFFIX."user_groups");
define('TABLE_USERS',				DB_PREFFIX."users");
define('TABLE_USER_AUTH',			DB_PREFFIX."user_auth");
define('TABLE_USER_ACCESS',			DB_PREFFIX."user_access");

define('TABLE_LANGS',				DB_PREFFIX."langs");

define('TABLE_PAGES',				DB_PREFFIX."pages");
define('TABLE_PAGES_LANGS',			DB_PREFFIX."pages_lang");
define('TABLE_PAGES_DES',			DB_PREFFIX."pages_des");
define('TABLE_PAGES_DES_LANGS',		DB_PREFFIX."pages_des_lang");
define('TABLE_PAGE_PHOTO',			DB_PREFFIX."pages_photo");
define('TABLE_PAGE_PHOTO_LANGS',	DB_PREFFIX."pages_photo_langs");
define('TABLE_PAGE_RESOURCES',		DB_PREFFIX."pages_resassign");
define('TABLE_PAGE_VIDEO',			DB_PREFFIX."page_video");
define('TABLE_PAGE_VIDEO_LANGS',	DB_PREFFIX."page_video_lang");
define('TABLE_PAGE_FILES',			DB_PREFFIX."page_file");
define('TABLE_PAGE_FILES_LANGS',	DB_PREFFIX."page_file_lang");

define('TABLE_RESOURCE',			DB_PREFFIX."resource");
define('TABLE_RESOURCE_LANGS',		DB_PREFFIX."resource_lang");

define('TABLE_SITE_OPTIONS',		DB_PREFFIX."contact_options");
define('TABLE_SLIDES',				DB_PREFFIX."slides");
define('TABLE_PREFERENCES',			DB_PREFFIX."preferences");

define('TABLE_BANNERS',				DB_PREFFIX."banners");
define('TABLE_BANNERS_LANGS',		DB_PREFFIX."banners_lang");

// Country module
define('TABLE_CONTINENT',			DB_PREFFIX."continent");
define('TABLE_COUNTRY',				DB_PREFFIX."country");
define('TABLE_COUNTRY_LANG',		DB_PREFFIX."country_lang");
define('TABLE_CITY',				DB_PREFFIX."city");
define('TABLE_CITY_LANG',			DB_PREFFIX."city_lang");
define('TABLE_REGION',				DB_PREFFIX."region");
define('TABLE_REGION_LANG',			DB_PREFFIX."region_lang");

// -------------------------------  Modules ------------------------------------
// Staff work
define('TABLE_STAFF_GROUP',			DB_PREFFIX."staff_group");
define('TABLE_STAFF_GROUP_LANGS',	DB_PREFFIX."staff_group_lang");
define('TABLE_STAFF',				DB_PREFFIX."staff");

// News
define('TABLE_NEWS',				DB_PREFFIX."news");
define('TABLE_NEWS_LANGS',			DB_PREFFIX."news_lang");

// Awards and certificates
define('TABLE_AWARDS',				DB_PREFFIX."awards");
define('TABLE_AWARDS_LANGS',		DB_PREFFIX."awards_lang");

// Partners or usefull links
define('TABLE_LINKS',				DB_PREFFIX."partner_links");
define('TABLE_LINKS_LANGS',			DB_PREFFIX."partner_links_lang");

// Vacancy
define('TABLE_VACANCY',				DB_PREFFIX."vacancy");

// Faq
define('TABLE_FAQ_GROUP',			DB_PREFFIX."faq_group");
define('TABLE_FAQ_GROUP_LANGS',		DB_PREFFIX."faq_group_lang");
define('TABLE_FAQ',					DB_PREFFIX."faq");
define('TABLE_FAQ_LANGS',			DB_PREFFIX."faq_lang");
define('TABLE_FAQ_ASSIGN',			DB_PREFFIX."faq_assign");

// Our office adresses
define('TABLE_OFFICES',				DB_PREFFIX."offices");
define('TABLE_OFFICES_LANG',		DB_PREFFIX."offices_lang");

// photo galery
define('TABLE_GALERY',				DB_PREFFIX."galery_items");
define('TABLE_GALERY_LANGS',		DB_PREFFIX."galery_items_lang");
define('TABLE_GALERY_PHOTO',		DB_PREFFIX."galery_photos");
define('TABLE_GALERY_PHOTO_LANGS',	DB_PREFFIX."galery_photos_lang");

//------------------------------ Catalog ---------------------------------------
//CATALOG
define('TABLE_CAT_PROFILE',			DB_PREFFIX."cat_profile");
define('TABLE_CAT_PROFILE_LANGS',	DB_PREFFIX."cat_profile_lang");
define('TABLE_CAT_MAKE',			DB_PREFFIX."cat_producer");
define('TABLE_CAT_MAKE_LANGS',		DB_PREFFIX."cat_producer_lang");
define('TABLE_CAT_CATALOG',			DB_PREFFIX."cat_section");
define('TABLE_CAT_CATALOG_LANGS',	DB_PREFFIX."cat_section_lang");
define('TABLE_CAT_CATALOG_PICS',	DB_PREFFIX."cat_section_pics");
define('TABLE_CAT_CATTITLES',		DB_PREFFIX."cat_section_title");
define('TABLE_CAT_ITEMS',			DB_PREFFIX."cat_item");
define('TABLE_CAT_ITEMS_STAR_RATE', DB_PREFFIX."cat_item_star_rate");
define('TABLE_CAT_ITEMS_LANGS',		DB_PREFFIX."cat_item_lang");
define('TABLE_CAT_ITEMS_MODEL',		DB_PREFFIX."cat_item_models");
define('TABLE_CAT_ITEMS_PICS',		DB_PREFFIX."cat_item_pics");
define('TABLE_CAT_ITEMS_VIDEO',		DB_PREFFIX."cat_item_video");
define('TABLE_CAT_ITEMS_VIDEO_LANGS',DB_PREFFIX."cat_item_video_lang");
define('TABLE_CAT_ITEMS_FILES',		DB_PREFFIX."cat_item_files");
define('TABLE_CAT_ITEMS_FILES_LANGS',DB_PREFFIX."cat_item_files_lang");
define('TABLE_CAT_ITEMS_COMMENT',	DB_PREFFIX."cat_item_comment");
define('TABLE_CAT_ITEMS_COMMENT_LANGS',DB_PREFFIX."cat_item_comment_lang");
define('TABLE_CAT_ITEMS_RATE',		DB_PREFFIX."cat_item_rate");
define('TABLE_CAT_ITEMS_SC_RATE',	DB_PREFFIX."cat_item_sc_rate");
define('TABLE_CAT_ITEMS_SC_RATESUM',DB_PREFFIX."cat_item_sc_ratesum");
define('TABLE_CAT_ITEMS_RELATED',	DB_PREFFIX."cat_item_related");
define('TABLE_CAT_SECT_RELATED',	DB_PREFFIX."cat_sect_releated");
define('TABLE_CAT_CATITEMS',		DB_PREFFIX."cat_sectitem");
define('TABLE_CAT_PARAM_DISP_TYPE',	DB_PREFFIX."cat_param_type");
define('TABLE_CAT_GROUP_PARAM',		DB_PREFFIX."cat_param_group");
define('TABLE_CAT_GROUP_PARAM_LANGS',DB_PREFFIX."cat_param_group_lang");
define('TABLE_CAT_PARAMS',			DB_PREFFIX."cat_param");
define('TABLE_CAT_PARAMS_LANGS',	DB_PREFFIX."cat_param_lang");
define('TABLE_CAT_PROFILE_PARAMS',	DB_PREFFIX."cat_profile_param");
define('TABLE_CAT_PARAM_OPTIONS',	DB_PREFFIX."cat_option");
define('TABLE_CAT_PARAM_OPTIONS_LANGS',DB_PREFFIX."cat_option_lang");
define('TABLE_CAT_PARAM_VALUES',	DB_PREFFIX."cat_param_value");
define('TABLE_CAT_PARAM_VALUES_OPTS',DB_PREFFIX."cat_param_valopt");
define('TABLE_CAT_MAIN_PARAMS',		DB_PREFFIX."cat_param_main");
define('TABLE_CAT_BEST_ITEMS',		DB_PREFFIX."cat_best_item");
define('TABLE_CAT_PRICES',			DB_PREFFIX."cat_price");

// Justhelp tables
define('TABLE_CAT_ITEMS_HELPREQ',	DB_PREFFIX."cat_requests");
define('TABLE_CAT_ITEMS_HELPREQ_RATE', DB_PREFFIX."cat_requests_rate");
define('TABLE_CAT_ITEMS_REQ_VIDEO', DB_PREFFIX."cat_requests_video");

define('TABLE_CAT_MSG',				DB_PREFFIX."msg_board");
define('TABLE_CAT_MSG_P2P',			DB_PREFFIX."msg_board_p2p");
define('TABLE_CAT_MSG_FILES',		DB_PREFFIX."msg_board_files");

define('TABLE_LOCAL_CURRENCY', DB_PREFFIX."local_currency");

// Catalog filters
define('TABLE_CAT_FILTERS',					DB_PREFFIX."cat_filters");
define('TABLE_CAT_FILTERS_LANGS',			DB_PREFFIX."cat_filters_lang");
define('TABLE_CAT_PROFILE_FILTERS',			DB_PREFFIX."cat_filters2profile");
define('TABLE_CAT_PROFILE_FILT_OPTIONS',	DB_PREFFIX."cat_filters_option");
define('TABLE_CAT_PROFILE_FILT_OPTIONS_LANGS',DB_PREFFIX."cat_filters_option_lang");
define('TABLE_CAT_FILTER_VALUES',			DB_PREFFIX."cat_filters_value");
define('TABLE_CAT_FILTER_VALUES_OPTS',		DB_PREFFIX."cat_filters_valueopts");

// Catalog actions
define('TABLE_CAT_ACTION',			DB_PREFFIX."cat_actions");
define('TABLE_CAT_ACTION_LANGS',	DB_PREFFIX."cat_actions_lang");
define('TABLE_CAT_ACTION_ITEMS',	DB_PREFFIX."cat_action_items");

define('TABLE_CAT_SEARCH',			DB_PREFFIX."cat_search_words");

define('TABLE_CAT_WISHLIST',		DB_PREFFIX."cat_buyer_wishlist");

define('TABLE_CAT_SECTGROUPS',		DB_PREFFIX."cat_section_groups");

define('TABLE_CAT_CURRENCY',		DB_PREFFIX."cat_currency");
define('TABLE_CAT_CURRENCY_LANGS',	DB_PREFFIX."cat_currency_langs");

// Catalog shopping
define('TABLE_SHOP_CART',			DB_PREFFIX."cat_cart");
define('TABLE_SHOP_CART_ORDERS',	DB_PREFFIX."cat_cart_orders");
define('TABLE_SHOP_ORDERS',			DB_PREFFIX."cat_orders");
define('TABLE_SHOP_ORDER_COMMENT',	DB_PREFFIX."cat_orders_comment");
define('TABLE_SHOP_BUYERS',			DB_PREFFIX."cat_buyer");
define('TABLE_SHOP_BUYER_ADDR',		DB_PREFFIX."cat_buyer_addr");
define('TABLE_SHOP_BUYER_AUTH',		DB_PREFFIX."cat_buyer_auth");
define('TABLE_SHOP_BUYER_SECTS',	DB_PREFFIX."cat_buyer_sects");

define('TABLE_CAT_MARKETS',			DB_PREFFIX."cat_market");
define('TABLE_CAT_MARKET_PRODS',	DB_PREFFIX."cat_market_prods");

define('TABLE_REPHONE_CALLBACK',	DB_PREFFIX."reqest_callback");

define('TABLE_SUBSCRIBE',			DB_PREFFIX."subscribe_list");

define('TABLE_CAT_BONUS',			DB_PREFFIX."cat_bonus");
define('TABLE_SHOP_BUYERS_BOX',		DB_PREFFIX."cat_buyer_box");
define('TABLE_CAT_BONUS_LANGS',		DB_PREFFIX."cat_bonus_lang");
define('TABLE_CAT_BONUS_PICS',		DB_PREFFIX."cat_bonus_pics");
define('TABLE_CAT_BONUS_VIDEO',		DB_PREFFIX."cat_bonus_video");
define('TABLE_CAT_BONUS_VIDEO_LANGS',DB_PREFFIX."cat_bonus_video_lang");
define('TABLE_CAT_BONUS_FILES',		DB_PREFFIX."cat_bonus_files");
define('TABLE_CAT_BONUS_FILES_LANGS',DB_PREFFIX."cat_bonus_files_lang");
define('TABLE_CAT_BONUS_COMMENT',	DB_PREFFIX."cat_bonus_comment");
define('TABLE_CAT_BONUS_COMMENT_LANGS',DB_PREFFIX."cat_bonus_comment_lang");
define('TABLE_CAT_BONUS_RATE',		DB_PREFFIX."cat_bonus_rate");
define('TABLE_CAT_BONUS_WINS',		DB_PREFFIX."cat_bonus_wins");
define('TABLE_CAT_BONUS_QUIZ',		DB_PREFFIX."cat_bonus_quiz");
define('TABLE_CAT_BONUS_QUIZ_POINTS',	DB_PREFFIX."cat_bonus_quiz_points");

define('TABLE_SEO_TITLES',			DB_PREFFIX."seo_titles");

define('TABLE_PAYS_DUMP',			DB_PREFFIX."pays_dump");
define('TABLE_PAYS_OPERATIONS',		DB_PREFFIX."pays_operations");

define('TABLE_HELP_MSG',		DB_PREFFIX."help_msg");

define('TABLE_PROJ_TEST', DB_PREFFIX."proj_test");
define('TABLE_PROJ_TEST_ANS', DB_PREFFIX."proj_test_ans");
define('TABLE_PROJ_TEST_SUB', DB_PREFFIX."proj_test_sub");
define('TABLE_CAT_BUYER_SMS', DB_PREFFIX."cat_buyer_sms");
define('TABLE_PAYS_PROMISE', DB_PREFFIX."pays_promise" );
define('TABLE_ITEM_PAYMENT', DB_PREFFIX."cat_item_payment");
define('TABLE_ITEM_PAYMENT_LIST', DB_PREFFIX."cat_item_payment_list");

// CRONS QUERY TABLE
define('TABLE_CRONS_QUERY', DB_PREFFIX."crons_query");

//POPUP TABLES
define('TABLE_PAGES_POPUP', DB_PREFFIX."pages_popup");
define('TABLE_PAGES_POPUP_INFO', DB_PREFFIX."pages_popup_info");

//BONUS VIA USER HELP NUM TABLE
define('TABLE_BONUS_HELP', DB_PREFFIX."cat_bonus_hlp");

// ITEM PAYED WOH PERCENT
define('TABLE_BUYER_WOHPAY', DB_PREFFIX."cat_buyer_wohpay");

// BOTS TABLE
define('TABLE_BOTS_PROJPAY', DB_PREFFIX."bots_projpay");

//USER IP
define('TABLE_USER_IP', DB_PREFFIX."user_ip");

//CATALOG SORT TYPE
define('TABLE_CAT_SORT', DB_PREFFIX."cat_sort");
//
define('TABLE_BRING_FRIEND', DB_PREFFIX."bring_friend");

define('TABLE_CAT_ITEM_DIAGRAM', DB_PREFFIX."cat_item_diagram");

define('CAT_HELPGET_MODE', 0);
define('CAT_HELPGIVE_MODE', 1);

// TABLE FOR ANONIMUS REQUESTS
define('TABLE_ANON_REQUESTS', DB_PREFFIX."cat_requests_anon");

////////////////////////////////////////////////////////////////////////////////
// Page load initialization
if( isset($_SERVER['PHP_SELF']) ){
    $PHP_SELF = $_SERVER['PHP_SELF'];
}

////////////////////////////////////////////////////////////////////////////////
// Global functions and arrays
function GetParameter( $parameter_name, $default_value, $replace_quotes = true )
{
    $tmp_val = $default_value;
    if( isset( $_POST[$parameter_name] ) ){
        $tmp_val = $_POST[$parameter_name];

    }else if( isset( $_GET[$parameter_name] ) )	 {
        $tmp_val = $_GET[$parameter_name];
    }else{

        $tmp_val = $default_value;
    }
    if( $replace_quotes )
    {
        if( !is_array($tmp_val) && ($tmp_val != "") )
            $tmp_val = str_replace ("\"", "&quot;", $tmp_val);
    }
    return $tmp_val;
}
$coding_table = Array(
    'X', 'F', 'D', 'E', 'U', 'S', 'P', 'N', 'W', 'T',
    'M', 'Y', 'O', 'A', 'B', 'I', 'V', 'L', 'K', 'R',
    'Z', 'G');
$decode_table = Array(
    'X' => 0, 'F' => 1, 'D' => 2, 'E' => 3, 'U' => 4, 'S' => 5, 'P' => 6, 'N' => 7, 'W' => 8, 'T' => 9,
    'M' => 10, 'Y' =>11, 'O' =>12, 'A' =>13, 'B' =>14, 'I' =>15, 'V' =>16, 'L' =>17, 'K' =>18, 'R' => 19,
    'Z' => 20, 'G' => 21);
////////////////////////////////////////////////////////////////////////////////

$REGIONS = Array("", "Киевская область", "Харьковская область", "АР Крым", "Винницкая область", "Волынская область", "Днепропетровская область",
    "Донецкая область", "Житомирская область", "Закарпатская область", "Запорожская область", "Ивано-франковская область",
    "Кировоградская область", "Луганская область", "Львовская область", "Николаевская область", "Одесская область", "Полтавская область",
    "Ровенская область", "Сумская область", "Тернопольская область", "Херсонская область", "Хмельницкая область", "Черкасская область",
    "Черниговская область", "Черновицкая область");

$STREET_TYPE = Array("ул.", "просп.", "бульвар", "переулок");

$month_names = Array("", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");

////////////////////////////////////////////////////////////////////////////////

if( $UHCMS_CONFIG['DEBUG_ON'] )
{
    error_reporting(E_ALL);

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
}
else
{
    error_reporting(0);

    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
}

////////////////////////////////////////////////////////////////////////////////
// Initialize class auto loading
function uh_cms_autoloader($class) {
    //echo getcwd();
    //echo "Autoload: ".( ADMIN_MODE ? "../" : "" ).MVC_PATH.'basic/' . $class . '.class.php<br>';
    if(TEST_MODE) {
        include '../testrev/basic/' . $class . '.class.php';
    } else
        include ( ADMIN_MODE ? "../" : "" ).MVC_PATH.'basic/' . $class . '.class.php';
}

spl_autoload_register('uh_cms_autoloader');
////////////////////////////////////////////////////////////////////////////////

date_default_timezone_set('Europe/Kiev');

////////////////////////////////////////////////////////////////////////////////

define("EMAIL_REGEXP", "/^[a-zA-Z0-9\\._-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z0-9]+$/");
define("PHONE_REGEXP", "/^\\+?[0-9]+\\s?\\(?[0-9]+\\)?\\s?([0-9]+[\\s\\-]*)+$/");

////////////////////////////////////////////////////////////////////////////////
// FB APP LOGIN: 066-253-82-70, help2015
define('FB_APP_ID', '803813303006196');
define('FB_APP_SECRET', '29a582b6fc4bb93db9cf7124deafc568');

define('INSTA_ACCESS_TOKEN', '3278074944.73c6e66.12f7491d96e44b389263e37f58615a23');

define('VK_APP_ID', '4923828');
define('VK_APP_SECRET', 'vL883PZLedJfrdFpxVCN');

define('OK_APP_ID', '1137549568');
define('OK_APP_KEY', 'CBACKOMEEBABABABA');
define('OK_APP_SECRET', '3342FE7F277E674B3B7DDAD1');

////////////////////////////////////////////////////////////////////////////////

define('REVIEWS_PER_PAGE', 4);
define('SOCIAL_CHECK_DONE', 1);
define('SOCIAL_CHECK_NONE', 0);

// USER ID
define('FROM_WOH_ID', 1992);
define('FROM_ADMIN_ID', 481);

// CHECK PROMISE MONEY
define('MONEY_PROMISE', 1000);
define('MAX_MONEY_CHECK', 500);
define('REQ_PROMISE', 1);
define('REQ_NOPROMISE', 0);
define('REQ_PRIMISE_PAY', 1);
define('REQ_PRIMISE_NOPAY', 0);
define('PHONE_ACTIVE', 1);
define('PHONE_NOACTIVE', 0);
define('GRN_MAX_PROM_VAL', 500);
define('RUB_MAX_PROM_VAL', 1000);

// SET WAYOFHELP PERCENT
define('WOH_PERCENT', 0.05);
define('WOH_PERCENT_STR', 5);
define('WOH_PERCENT_BY_PAY', 10);
define('WOH_ARBITR_COMMISION', 0.2);

// SET CONST FOR SENDPULSE
define('SENDPULSE_KEY', '71d1ded926c0944b02935aad01f4f928');
define('SENDPULSE_ID', 'f5673a5808e8f08fd07248bec7e7365c');

// CONST FOR LOCATION TYPE IN ITEM
define('ITEM_LOCATION_ALL', 0);
define('ITEM_LOCATION_COUNTRY', 1);
define('ITEM_LOCATION_REGION', 2);

// CONST FOT NEW ITEM RATING SYSTEM FOR CATALOG
define('CAT_LIMIT_NEW_PROJ_ROTATION', 2);