var head_scroll_down = false;
var socwnd_was_closed = false;
var func_call = true;
const TIME_TO_SHOW = 10;
// Return random int number including min but excluding max
function getRandomInt(min, max) 
{
	return Math.floor(Math.random() * (max - min)) + min;
}

$(document).ready(function(){
	$("body").addClass("jhscope");
	/**** SET COOKIES ****/
	var date = new Date();
	var minutes = 0.5;
	date.setTime(date.getTime() + (minutes * 60 * 1000));
	//$.cookie("poptipTimer", 1, { expires: date });
	document.cookie = 'cookie1=test; expires=Fri, 3 Aug 2001 20:47:11 UTC; path=/';
	/********************/

	var rnd_int = getRandomInt(0,3);
	$("#videoplay").bind("click", function(){
		//alert("load");
		_loadRes('popvideo', 'vplayer');
	});

	$(window).bind("scroll",function(){
		if( adapt_top )
		{
			if( !head_scroll_down && ($(window).scrollTop()>40) )
			{
				$("body").addClass("minhead");
				head_scroll_down=true
			}
			if( head_scroll_down && ($(window).scrollTop()<30) )
			{
				$("body").removeClass("minhead");
				head_scroll_down=false
			}
		}

		//console.log($(window).scrollTop() + " : " + $(document).height() + " : " + ($(window).scrollTop() + $(window).height()) );
		//document.cookie = "userName=Vasya";
		/*console.log(rnd_int);
		if(rnd_int === 0) {
			setTimeout(function () {
				if (!socwnd_was_closed) {
					if (($(window).scrollTop() + $(window).height()) >= $(document).height()) {
						$("#wnd-social").show();
						func_call = false;
					}
					else if ((($(window).scrollTop() + $(window).height()) < $(document).height()) && !socwnd_was_closed) {
						$("#wnd-social").hide();
						//socwnd_was_closed = true;
					}
				}
			}, 60000);
		}	*/
	});
	
	$(".social-wnd-close").bind("click", function(e){
		socwnd_was_closed = true;
		$("#wnd-social").hide();
		e.preventDefault();					
	});
	
	$(".social-wnd-bot span").bind("click", function(e){
		socwnd_was_closed = true;
		$("#wnd-social").hide();
		e.preventDefault();
	});
	
	//$("#signinlnk").bind("click",function(){
	//	popWnd('regdlg', '');
	//	return false
	//});
	
	$("#loginlnk").bind("click",function(){
		popWnd('logdlg', '');
		return false
	});
	
	$(".wnd-close a").bind("click", function(){
		$("#wnd-wnd").hide();
		$("#wnd-bg").hide();		
		$("#wnd-cont").html('<div class="wnd-loading"></div>');
		return false
	});

	$(".wnd-close-sm a").bind("click", function(){
		$("#wnd-wnd-sm").hide();
		$("#wnd-bg-sm").hide();
		$("#wnd-cont-sm").html('<div class="wnd-loading"></div>');
		return false
	});
	
	$(".lnkviewpromo").bind("click", function(){
		popWnd('promovideodlg', '');
		return false
	});
	
	$(".lnkgethelp, #toplnkgethelp").bind("click", function(){
		if( usr_logged == 1 )
			return true

		redirbackurl = "/proj/add/";
		
		popWnd('logdlg', '');
		return false
	});
	
	$(".replylnk, #usrmsglnk").bind("click", function(){
		popWnd('sendmsgdlg', 'uid=' + $(this).attr("data-uid") + '&projid=' + $(this).attr("data-proj") + '&replyto=' + $(this).attr("data-rmsg"));
		return false
	});
	
	$("#regformcomp").submit(function(){
		if( !checkRegFormComp() )
		{
			scrollEUp('formstart');
			return false
		}
		return true
	});
	
	$("#regformorg").submit(function(){
		if( !checkRegFormComp() )
		{
			scrollEUp('formstart');
			return false
		}
		return true
	});
	
	$("#regformpers").submit(function(){
		if( !checkRegFormPerson() )
		{
			scrollEUp('formstart');
			return false
		}
		return true
	});
	
	$("#projformadd").submit(function(){
		if( !checkAddProjForm() )
		{
			scrollEUp('formstart');
			return false
		}
		return true
	});	
	
	$("#lnkshowtree").bind("click", function(){
		popWnd('showtreedlg', 'uid=' + $(this).attr("data-uid"));
		return false
	});
	
	$("#popnotify-close").bind("click", function(){
		//alert('bind');
		$(this).parent().removeClass("tip-notify-show");
		return false
	});
	
	$("#projcomadd").submit(function(){
		$("#comment").parent().removeClass("has-error");
		
		if( $.trim($("#comment").val()) == "" )
		{
			$("#comment").parent().addClass("has-error");
			return false
		}
		//showTipAdder('popnotify-adder', 1, '???? ?? ??????????? ???????????');
		return true;
	});

	$(".faq-it a").bind("click", function(e){
		var fid = "fit" + $(this).attr("data-faq");

		$("#"+fid).toggleClass("faq-it-ivis");

		e.preventDefault();
	});
});

function checkEmail(email)
{
	var re_email = /^[a-zA-Z0-9\._-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z0-9]+$/i;
	return re_email.test(email);
}

function checkPhone(tel)
{
	var re_phone = /^\+?[0-9]+\s?\(?[0-9]+\)?\s?([0-9]+[\s-]*)+$/i;
	return re_phone.test(tel);
}

function scrollPUp()
{
	/*
	if($.browser.safari)
	{
		bodyelem = $("body");
	}
	else
	{
		if($.browser.opera)	bodyelem = $("html");
		else				bodyelem = $("html,body");
	}
	*/
	bodyelem = $("html,body");
	bodyelem.animate({scrollTop: 0});
}

function scrollEUp(elid) {
	var pos = $("#"+elid).offset();

	bodyelem = $("html,body");
	bodyelem.animate({scrollTop: pos.top});
}

function popWnd(dlg, params) {
	$("html, body").animate({ scrollTop: 0 }, "slow");
	var ytop = Math.round($(window).scrollTop());
	console.log(ytop);
	$("#wnd-wnd").css("top", (90)+"px");
	$("#wnd-bg").show();
	$("#wnd-wnd").show();
	_loadWnd(dlg, params);
}

function popWndsm(dlg, params) {
	$("html, body").animate({ scrollTop: 0 }, "slow");
	var ytop = Math.round($(window).scrollTop());
	console.log(ytop);
	$("#wnd-wnd-sm").css("top", (90)+"px");
	$("#wnd-bg-sm").show();
	$("#wnd-wnd-sm").show();
	_loadWndSm(dlg, params);
}

function _loadWndSm(dlg, params) {
	var post_req_str = "dlg=" + dlg + (params != '' ? '&'+params : '');

	$.ajax({
		type: "GET",
		url: req_ajx_host+"ajx/dialog/",
		data: post_req_str,
		dataType: "text",
		success: function(data) {
			try {
				//$("#wnd-cont-sm").html( data );
				$("#wnd-conts").html( data );
			}
			catch(e1) {
				// Some errors occure while retieving city list so do nothing
				alert(e1);
			}

		}
	});
}

function _loadWnd(dlg, params) {
	var post_req_str = "dlg=" + dlg + (params != '' ? '&'+params : '');

	$.ajax({
		type: "GET",
		url: req_ajx_host+"ajx/dialog/",
		data: post_req_str,
		dataType: "text",
		success: function(data) {
			try {
				$("#wnd-cont").html( data );
				//$("#wnd-conts").html( data );
			}
			catch(e1) {
				// Some errors occure while retieving city list so do nothing
				alert(e1);
			}

		}
	});
}

function _loadRes(resname, contid)
{
	var post_req_str = "resname=" + resname;

	$.ajax({
        type: "POST",
        url: req_ajx_host+"ajx/resource/",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	try
        	{
				//alert(data);
        		$("#"+contid).replaceWith( data );
			}
			catch(e1)
			{
				// Some errors occure while retieving city list so do nothing
				alert(e1);
			}

        }
    });
}

function showTip(tipid)
{
	var nav = $(".navbar-cust");
	var cont = $(".navbar-cust .container");

	var nav_h = nav.height();
	var pos = cont.offset();
	var cont_w = cont.width();

	//alert( nav_h +":"+ pos.left + ":" + pos.top + ":" + cont_w );

	$("#"+tipid).css("top", Math.round(nav_h + 20) + "px");
	$("#"+tipid).css("left", Math.round(pos.left + cont_w - 320) + "px");
	$("#"+tipid).addClass("tip-notify-show");
}

function showTipAdder(tipid, money, str, txt) {
	var nav = $(".navbar-cust");
	var cont = $(".navbar-cust .container");

	var nav_h = nav.height();
	var pos = cont.offset();
	var cont_w = cont.width();

	$("#"+tipid).css("top", Math.round(nav_h + 20) + "px");
	$("#"+tipid).css("left", Math.round(pos.left + cont_w - 320) + "px");
	$("#"+tipid).addClass("tip-notify-show");
	$("#"+tipid).append('<div><p>'+txt+': </p><p>'+money+' '+str+'</p></div>');
}

function showPoptipPromo()
{
	$(".poptip-close a").bind("click", function(){
		$(".poptip-promo").hide();	//removeClass("poptip-promo-show");
		$(window).off("scroll");
		return false
	});

	//var pos = $("#addpicbtn").offset();
	var winw = $(window).width();
	var winh = $(window).height();
	var topscroll = $(document).scrollTop();
	$(".poptip-promo").css("left", Math.round(winw - 600)+"px");
	$(".poptip-promo").css("top", Math.round(winh + topscroll - 320)+"px");
	$(".poptip-promo").addClass("poptip-promo-show");

	$(window).scroll(function() {
		var winw = $(window).width();
		var winh = $(window).height();
		var topscroll = $(document).scrollTop();
		$(".poptip-promo").css("left", Math.round(winw - 600)+"px");
		$(".poptip-promo").css("top", Math.round(winh + topscroll - 320)+"px");
	});
}


function loc_LoadRegion(countryobj, oblobjid, cityobjid, err) {
	//console.log('Region load');
	var countryid = countryobj.options[countryobj.selectedIndex].value;
	var oblobj = document.getElementById(oblobjid);
	var cityobj = document.getElementById(cityobjid);
	//console.log(countryid + ' ' + oblobj + ' '+cityobj);

	if( countryid == 0 )
		return;

	var post_req_str = "countryid=" + countryid;

	$.ajax({
        type: "GET",
        url: req_ajx_host+"ajx/regionlist/",
        data: post_req_str,
        dataType: "json",
        success: function(data){
        	try {
				var regions = data.regions;

				$("#"+oblobjid).empty();
				$("#"+cityobjid).empty();

				oblobj.options[0] = new Option("----- "+err[1]+" -----", 0);
				cityobj.options[0] = new Option("----- "+err[2]+" -----", 0);

				for( var i=0; i<regions.length; ++i ) {
					oblobj.options[i+1] = new Option(regions[i]['name'], regions[i]['id']);
					//console.log(regions[i]['name'] + regions[i]['id']);
				}
        		//$("#wnd-cont").html( data );
			}
			catch(e1) {
				console.log(e1);
			}

        }
    });
}

function loc_LoadCity(oblobj,cityobjid) {

	var oblid = oblobj.options[oblobj.selectedIndex].value;
	var cityobj = document.getElementById(cityobjid);

	if( oblid == 0 )
		return;

	var post_req_str = "oblid=" + oblid;

	$.ajax({
        type: "GET",
        url: req_ajx_host+"ajx/citylist/",
        data: post_req_str,
        dataType: "json",
        success: function(data) {
        	try {
				var cities = data.cities;

				//cityobj.options.length = 0;
				//cityobj.options[0] = new Option("----- ??????? ???? ????? -----", 0);

				for( var i=0; i<cities.length; ++i ) {
					//console.log(cityobj);
					cityobj.options[i+1] = new Option(cities[i]['name'], cities[i]['id']);
				}
        		//$("#wnd-cont").html( data );
			}
			catch(e1) {
				// Some errors occure while retieving city list so do nothing
				alert(e1);
			}

        }
    });
}

function removeOptions() {

}


function updtTxtCount(inp, slblid, maxl)
{
	var ost = (maxl - inp.value.length);

	if( ost <= 0 )
	{
		$("#"+slblid).hide();
		return;
		//inp.value = inp.value.substr(0, (maxl-1));
	}

	$("#"+slblid).show();
	$("#"+slblid+" span").html("<b>"+ost+"</b>");
	if( ost<=3 )
	{
		$("#"+slblid).addClass("txtcount-last");
	}
	else
	{
		$("#"+slblid).removeClass("txtcount-last");
	}
}

function checkRegFormPerson()
{
	var errors = false;
	var rname = $("#rname");
	var rfname = $("#rfname");
	var rlogin = $("#rlogin");
	var robl = document.getElementById('robl');
	var rcity = document.getElementById('rcity');

	rname.parent().parent().removeClass("has-error");
	if( $.trim( rname.val() ) == "" )
	{
		rname.parent().parent().addClass("has-error");
		errors = true;
	}

	rfname.parent().parent().removeClass("has-error");
	if( $.trim( rfname.val() ) == "" )
	{
		rfname.parent().parent().addClass("has-error");
		errors = true;
	}

	rlogin.parent().parent().removeClass("has-error");
	if( $.trim( rlogin.val() ) == "" )
	{
		rlogin.parent().parent().addClass("has-error");
		errors = true;
	}
	else if( !checkEmail(rlogin.val()) && !checkPhone(rlogin.val()) )
	{
		rlogin.parent().parent().addClass("has-error");
		errors = true;
	}

	$("#rpasswd1").parent().parent().removeClass("has-error");
	$("#rpasswd2").parent().parent().removeClass("has-error");
	if( ($("#rpasswd1").val() != $("#rpasswd2").val()) || ($("#rpasswd1").val() == "") || ($("#rpasswd1").val().length < 6) )
	{
		$("#rpasswd1").parent().parent().addClass("has-error");
		$("#rpasswd2").parent().parent().addClass("has-error");
		errors = true;
	}

	$("#robl").parent().parent().removeClass("has-error");
	if( robl.options[robl.selectedIndex].value == 0 )
	{
		$("#robl").parent().parent().addClass("has-error");
		errors = true;
	}

	$("#rcity").parent().parent().removeClass("has-error");
	if( rcity.options[rcity.selectedIndex].value == 0 )
	{
		$("#rcity").parent().parent().addClass("has-error");
		errors = true;
	}

	return !errors;
}

function checkRegFormComp()
{
	var errors = false;
	var rname = $("#rname");
	var rfname = $("#rfname");
	var rlogin = $("#rlogin");
	var rorg = $("#rorg");
	var rtel = $("#rtel");
	var robl = document.getElementById('robl');
	var rcity = document.getElementById('rcity');

	rorg.parent().parent().removeClass("has-error");
	if( $.trim( rorg.val() ) == "" )
	{
		rorg.parent().parent().addClass("has-error");
		errors = true;
	}

	rname.parent().parent().removeClass("has-error");
	if( $.trim( rname.val() ) == "" )
	{
		rname.parent().parent().addClass("has-error");
		errors = true;
	}

	rfname.parent().parent().removeClass("has-error");
	if( $.trim( rfname.val() ) == "" )
	{
		rfname.parent().parent().addClass("has-error");
		errors = true;
	}

	rlogin.parent().parent().removeClass("has-error");
	if( $.trim( rlogin.val() ) == "" )
	{
		rlogin.parent().parent().addClass("has-error");
		errors = true;
	}
	else if( !checkEmail(rlogin.val()) )
	{
		rlogin.parent().parent().addClass("has-error");
		errors = true;
	}

	rtel.parent().parent().removeClass("has-error");
	if( $.trim(rtel.val()) != "" )
	{
		if( !checkPhone( rtel.val() ) )
		{
			rtel.parent().parent().addClass("has-error");
			errors = true;
		}
	}

	$("#rpasswd1").parent().parent().removeClass("has-error");
	$("#rpasswd2").parent().parent().removeClass("has-error");
	if( ($("#rpasswd1").val() != $("#rpasswd2").val()) || ($("#rpasswd1").val() == "") || ($("#rpasswd1").val().length < 6) )
	{
		$("#rpasswd1").parent().parent().addClass("has-error");
		$("#rpasswd2").parent().parent().addClass("has-error");
		errors = true;
	}

	$("#robl").parent().parent().removeClass("has-error");
	if( robl.options[robl.selectedIndex].value == 0 )
	{
		$("#robl").parent().parent().addClass("has-error");
		errors = true;
	}

	$("#rcity").parent().parent().removeClass("has-error");
	if( rcity.options[rcity.selectedIndex].value == 0 )
	{
		$("#rcity").parent().parent().addClass("has-error");
		errors = true;
	}

	return !errors;
}

function checkAddProjForm()
{
	var errors = false;
	var rtit = $("#tit");
	var rdescr = $(".nicEdit-main");
	var pamount = $("#pamount");
	var ptype = document.getElementById('ptype');
	var rsect = document.getElementById('sect');
	var robl = document.getElementById('robl');
	var rcity = document.getElementById('rcity');

	rtit.parent().parent().removeClass("has-error");
	if( $.trim( rtit.val() ) == "" )
	{
		rtit.parent().parent().addClass("has-error");
		errors = true;
	}

	$("#sect").parent().parent().removeClass("has-error");
	if( rsect.options[rsect.selectedIndex].value == 0 )
	{
		$("#sect").parent().parent().addClass("has-error");
		errors = true;
	}
	console.log(rdescr.text());
	rdescr.parent().parent().removeClass("has-error");
	if( $.trim( rdescr.text() ) == "" )
	{
		rdescr.parent().parent().addClass("has-error");
		errors = true;
	}

	$("#robl").parent().parent().removeClass("has-error");
	if( robl.options[robl.selectedIndex].value == 0 )
	{
		$("#robl").parent().parent().addClass("has-error");
		errors = true;
	}

	$("#rcity").parent().parent().removeClass("has-error");
	if( rcity.options[rcity.selectedIndex].value == 0 )
	{
		$("#rcity").parent().parent().addClass("has-error");
		errors = true;
	}

	$("#pamount").parent().parent().removeClass("has-error");
	if( (ptype.options[ptype.selectedIndex].value == 0) || (ptype.options[ptype.selectedIndex].value == 1) )
	{
		var amountval = parseFloat(pamount.val());
		if( isNaN(amountval) || (amountval <= 0) )
		{
			$("#pamount").parent().parent().addClass("has-error");
			errors = true;
		}
	}

	return !errors;
}

function _checkLogin(ulogin, upass, err)
{
	var post_req_str = "ulogin=" + ulogin + "&upass=" + upass;

	$.ajax({
        type: "POST",
        url: req_ajx_host+"ajx/login/",
        data: post_req_str,
        dataType: "text",
        success: function(data) {

			var regx = /\/login(\/|$)/gi;

        	try {
				var res = $.parseJSON(data);

				if( res.status == "ok" ) {

					if( redirbackurl == "undefined" )
						location.href = req_ajx_host + 'cabinet/';
					else if( regx.test(redirbackurl) )
						location.href = req_ajx_host + 'cabinet/';
					else
						location.href = redirbackurl;

				}

				else if( res.status == 'error' ) {
					$("#loginerror").html(err);
					$("#loginerror").show();
				}
			}
			catch(e1) {
				alert('Error: ' + e1.message);
			}

        }
    });
}

function checkLoginForm(err) {
	$("#loginerror").hide();
	var ulog = $("#ulogin").val();
	var upass = $("#upass").val();
	
	//alert("ccc: "+ ulog + ";");

	if( ulog.trim() == "" ) {
		return false
	}
	
	//alert("check 2");

	_checkLogin(ulog, upass, err);
	
	return false;
}

function checkRestoreForm(err) {
	$("#restoreerror").hide();

	var ulog = $("#ulogin").val();

	if( ulog.trim() == "" ) {
		$("#restoreerror").html(err);
		$("#restoreerror").show();
		return false
	}

	if( !checkEmail(ulog) ) {
		$("#restoreerror").html(err);
		$("#restoreerror").show();
		return false
	}

	$("#passrestfrm").submit();

	return true;
}

function checkAddhelpForm(err)
{
	$("#doerror").hide();

	var sum = $("#helpsum").val();
	var sumval = parseFloat(sum);
	if( isNaN(sumval) )
	{
		$("#doerror").html(err[0]);
		$("#doerror").show();
		return false
	}

	if( sumval == 0 )
	{
		$("#doerror").html(err[1]);
		$("#doerror").show();
		return false
	}

	return true
}

function checkSendmsgForm(err)
{
	var txt = $("#msgtxt").val();
	if( $.trim(txt) == "" )
	{
		$("#doerror").html(err);
		$("#doerror").show();
		return false
	}

	return true
}


function checkStarForm(err)
{
	var startxt = $("#startxt").val();
	if( $.trim(startxt) == "" )
	{
		$("#doerror").html(err);
		$("#doerror").show();
		return false
	}

	return true
}

function loadProjReq(projid, contid)
{
	var post_req_str = "projid=" + projid + "&pi=3&pn=1000";

	$.ajax({
        type: "POST",
        url: req_ajx_host+"ajx/projreqs/",
        data: post_req_str,
        dataType: "text",
        success: function(data){
			console.log(data);
        	try
        	{
				//var res = $.parseJSON(data);
				$(contid).html(data);
			}
			catch(e1)
			{
				// Some errors occure while retieving city list so do nothing
				$(contid).html("Error loading data...");
			}

        }
    });
}

function drawMyTreeCabinet(canvid)
{
	var uid = $("#"+canvid).attr("data-usr");
	if( uid == null )
		return;

	var canvobj = document.getElementById(canvid);

	var post_req_str = "uid=" + uid;

	$("#mytree").prepend('<div class="loading-indication" id="treeload"><img src="'+req_ajx_host+'img/ajax-loader.gif" /> Loading...</div>');

	$.ajax({
        type: "POST",
        url: req_ajx_host+"ajx/mytreedata/",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	try
        	{
				//alert(data);

				//console.log(data);

				var res = $.parseJSON(data);

				if( res.status == "ok" )
				{
					//location.href = req_ajx_host + 'cabinet/';
					_drawMyTree(res.data, canvobj, res.mypic);
				}
				else
				{
					_drawNoData(canvobj);
				}
			}
			catch(e1)
			{
				// Some errors occure while retieving city list so do nothing
				_drawNoData(canvobj);
				alert(e1);
			}

        }
    });
}

function trimNameForGraph(name)
{
	return ( name.length > 16 ? name.substr(0,15) +"..." : name );
}

function _drawNoData(canvobj) {
	var gd = canvobj.getContext("2d");

	gd.fillStyle = "#f91010";
	gd.font = "18px Arial";
	gd.fillText("Error loading data",Math.round(canvobj.width/2)-100,Math.round(canvobj.height/2)+8);
}

function _drawMyTree(data, canvobj, upic) {

	var bl = $("#mytree").position();

	var hlen = 0;
	var crclCount = 1;

	var cx =  $("#mytree").height() / 2;
	var ch =  $("#mytree").width() / 2;

	for(var i = 0; i<data.length; ++i) {
		if(data[i].type == 1) {
			hlen++;
		}
	}

	var maxLen = (hlen>100 ? 100 : hlen);

	if(hlen > 10) {
		crclCount = Math.ceil(hlen/10);
	}

	var delta = Math.PI * 2 / (hlen > 10 ? 10 : hlen);
	var x = 0, y = 0, angle = 0;

	var fi = 0;
	var li = 0;
	var crclLen = 50;
	var crclAr = 0;
	var pointInCrirl = 0;

	$("#mytree").append('<a href="#" alt="I am" title="I am" class="cab-usr-pt" style="left: ' + (ch+130) + 'px; top: ' + (cx+16) + 'px;"><img src="' + req_ajx_host + upic + '" /></a>');

	$( "#treeload" ).remove();

	while(crclCount != 0) {
		//alert('fi = '+fi+' li = '+li+' crclCo = '+crclCount);
		li += 10+crclAr*10;
		li = (li > maxLen ? maxLen : li);

		crclLen = 50 * ++crclAr/1.1;

		//delta = Math.PI * 2 / ( fi==maxLen ? fi-maxLen : pointInCrirl);
		if(li >= maxLen){
			delta = Math.PI * 2 / (maxLen-fi);
		}

		for (var i = fi; i < li; ++i) {
			$("#mytree").append('<a target="_blank" href="'+req_ajx_host+'users/viewrev/'+data[i].uid+'/" alt="'+data[i].uname+'" title="'+data[i].uname+'" class="cab-usr-pt" style="left: ' + (((crclLen) * Math.cos(angle)) + ch+130) + 'px; top: ' + (((crclLen) * Math.sin(angle)) + cx+16) + 'px;"><img src="' + req_ajx_host + (data[i].pic != '' ? data[i].pic : 'img/no-usr.png') + '" /></a>');

			angle += delta;
		}

		crclCount--;
		fi=li;
		pointInCrirl = 10+crclAr*10 > maxLen ? (li-maxLen) : 10+crclAr*10;

		delta = Math.PI * 2 / pointInCrirl;

		if(li >= maxLen){
			return;
		}
	}

	//$("#"+divid).append('<a href="#" class="bonustree-point'+(opened ? ' bonustree-point-empty' : '')+'" id="bonuspoint'+i+'" data-ind="'+i+'" style="left: '+xpos+'%; top: '+ypos+'px;"><span>' + i + '</span></a>');

	/*var canvas = document.getElementById('mytree');
	var context = canvobj.getContext("2d");

	var cw = canvas.width;
	var ch = canvas.height;
	var cx = cw / 2;	// ????? ??????? X
	var cy = ch / 2;	// ????? ??????? Y
	var cr = 8;			// ?????? ??????
	var radius0 = 126;	// ??????? ??????? ?????? ??? ?????

	var incolor = "#f36624";	// Orange
	var outcolor = "#38b348";	// ???????

	var angstep = 2*Math.PI / data.length;	// ??? ? ???????? ????? ??????? ???????? ????
	var angarrdif = 2*Math.PI / 180.0;		// ?????????? ?????? ??????? ?? ????
	var arrlen = 18; // ????? ???????

	var x = cx-25;
	var y = cy-25;
	var width = 60;
	var height = 60;
	var imageObj = new Image();

	//context.arc(cx,cy, 20, 0, Math.PI*2,true); // you can use any shape

	imageObj.onload = function() {
		context.save();

		context.beginPath();
		context.arc(cx, cy, 25, 0, 2 * Math.PI, true);
		context.fillStyle = '#111';
		context.fill();
		context.lineWidth = 1;
		context.strokeStyle = '#444';
		context.stroke();

		context.clip();

		context.drawImage(imageObj, x, y, width, height);
		context.restore();
	};
	imageObj.src = 'http://work/data1506/public_html/pics/avatar/88_318_sm.jpg';

	var persImg = '';

	for( var i=0; i<2; ++i ) {

		var radius = radius0;

		//persImg = new Image();

		//context.arc(cx,cy, 20, 0, Math.PI*2,true); // you can use any shape


		//persImg.src = 'http://work/data1506/public_html/pics/avatar/6_887_sm.jpg';

		var loaders = [];
		loaders.push(loadSprite('http://work/data1506/public_html/pics/avatar/6_887_sm.jpg'));

		context.setTransform(1, 0, 0, 1, 0, 0);
		context.translate(cx, cy);
		context.rotate(angstep*i);

		context.lineWidth = 3;
		if( data[i].type != 0 ) {
			context.strokeStyle = outcolor;
			context.fillStyle = outcolor;
		}

		var endx = cx + Math.ceil(Math.sin(angstep*i)*radius);
		var endy = cy + Math.ceil(Math.cos(angstep*i)*radius);

		context.beginPath();
		//gd.moveTo(cx, cy);
		//gd.lineTo(endx, endy);
		context.moveTo(0, 0);
		context.lineTo(0, radius);
		context.stroke();

		context.miterLimit = 10.0;
		context.lineWidth = 1;
		context.beginPath();

		if( data[i].type == 1 ) {
			context.moveTo(0, radius+3);
			context.lineTo(-5, radius-arrlen);
			context.lineTo(5, radius-arrlen);

			context.fill();
		}

		/*var txt = data[i].uname;
		//console.log(txt);
		if( txt.length > 14 )
			txt = txt.substr(0, 14) + "...";

		context.beginPath();

		context.fillStyle = "#337ab7";
		context.font = "11px Arial";
		//gd.textAlign = "center";
		context.fillText(txt, 2, radius+16);*/

		//context.setTransform(1, 0, 0, 1, 0, 0);	// Go to standart coordinates

	//}

		/*var gd = canvobj.getContext("2d");

		/* var pic0 = new Image();
		 pic0.src = 'http://www.html5canvastutorials.com/demos/assets/darth-vader.jpg';
		 //var pic1 = new Image();
		 //pic1.src = 'http://work/data1506/public_html/img/face-help.png';

		 console.log(pic0);

		 var cw = canvobj.width;
		 var ch = canvobj.height;
		 var cx = cw / 2;	// ????? ??????? X
		 var cy = ch / 2;	// ????? ??????? Y
		 var cr = 8;			// ?????? ??????
		 var radius0 = 126;	// ??????? ??????? ?????? ??? ?????

		 var incolor = "#f36624";	// Orange
		 var outcolor = "#38b348";	// ???????

		 var angstep = 2*Math.PI / data.length;	// ??? ? ???????? ????? ??????? ???????? ????
		 var angarrdif = 2*Math.PI / 180.0;		// ?????????? ?????? ??????? ?? ????
		 var arrlen = 14; // ????? ???????

		 gd.drawImage(pic0, 10, 10);*/

		// Draw all lines - in and out
		/*for( var i=0; i<data.length; ++i ) {
		 var radius = radius0;

		 gd.setTransform(1, 0, 0, 1, 0, 0);
		 gd.translate(cx, cy);
		 gd.rotate(angstep*i);

		 gd.lineWidth = 3;
		 if( data[i].type == 0 ) {
		 gd.strokeStyle = incolor;
		 gd.fillStyle = incolor;
		 }
		 else {
		 gd.strokeStyle = outcolor;
		 gd.fillStyle = outcolor;
		 }

		 var endx = cx + Math.ceil(Math.sin(angstep*i)*radius);
		 var endy = cy + Math.ceil(Math.cos(angstep*i)*radius);

		 gd.beginPath();
		 //gd.moveTo(cx, cy);
		 //gd.lineTo(endx, endy);
		 gd.moveTo(0, 0);
		 gd.lineTo(0, radius);
		 gd.stroke();

		 gd.miterLimit = 10.0;
		 gd.lineWidth = 1;
		 gd.beginPath();
		 if( data[i].type == 1 ) {
		 gd.moveTo(0, radius+3);
		 gd.lineTo(-5, radius-arrlen);
		 gd.lineTo(5, radius-arrlen);

		 //gd.moveTo(endx, endy);
		 //gd.lineTo(cx + Math.ceil(Math.sin(angstep*i - angarrdif)*(radius-arrlen)), cy + Math.ceil(Math.cos(angstep*i - angarrdif)*(radius-arrlen)));
		 //gd.lineTo(cx + Math.ceil(Math.sin(angstep*i + angarrdif)*(radius-arrlen)), cy + Math.ceil(Math.cos(angstep*i + angarrdif)*(radius-arrlen)));
		 gd.fill();
		 }
		 else {
		 gd.moveTo(0, cr);
		 gd.lineTo(-5, cr+arrlen);
		 gd.lineTo(5, cr+arrlen);

		 //var startx = cx + Math.ceil(Math.sin(angstep*i)*cr);
		 //var starty = cy + Math.ceil(Math.cos(angstep*i)*cr);
		 //gd.moveTo(startx, starty);
		 //gd.lineTo(cx + Math.ceil(Math.sin(angstep*i - angarrdif)*(cr+arrlen)), cy + Math.ceil(Math.cos(angstep*i - angarrdif)*(cr+arrlen)));
		 //gd.lineTo(cx + Math.ceil(Math.sin(angstep*i + angarrdif)*(cr+arrlen)), cy + Math.ceil(Math.cos(angstep*i + angarrdif)*(cr+arrlen)));
		 gd.fill();
		 }

		 var txt = data[i].uname;
		 //console.log(txt);
		 if( txt.length > 14 )
		 txt = txt.substr(0, 14) + "...";


		 if( data[i].utype == 0 )
		 gd.drawImage(pic0, -16, radius+2);
		 else
		 gd.drawImage(pic1, -16, radius+2);

		 gd.beginPath();

		 //gd.fillStyle = "#337ab7";
		 //gd.font = "11px Arial";
		 //gd.textAlign = "center";
		 //gd.fillText(txt, 2, radius+16);

		 //gd.setTransform(1, 0, 0, 1, 0, 0);	// Go to standart coordinates
		 }

		 // Draw center point
		 gd.setTransform(1, 0, 0, 1, 0, 0);

		 gd.fillStyle = "#337ab7";		// ?????
		 gd.beginPath();
		 gd.arc(cx, cy, cr, 0, 2*Math.PI, true);
		 gd.fill();*/
}

function loadSprite(src) {
	var deferred = $.Deferred();
	var sprite = new Image();
	sprite.onload = function() {
		deferred.resolve();
	};
	sprite.src = src;
	return deferred.promise();
}

function drawMaingraphCabinet(canvid) {
	var uid = $("#"+canvid).attr("data-usr");
	if( uid == null )
		return;

	var canvpos = $("#"+canvid).position();

	var canvobj = document.getElementById(canvid);

	var post_req_str = "uid=" + uid;

	$.ajax({
        type: "POST",
        url: req_ajx_host+"ajx/mygraphdata/",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	try
        	{
				//alert(data);
				console.log(data);

				var res = $.parseJSON(data);

				if( res.status == "ok" )
				{
					//location.href = req_ajx_host + 'cabinet/';
					_drawMyGraph(res.data, canvobj, $("#"+canvid).parent(), canvpos.left);
				}
				else
				{
					_drawNoData(canvobj);
				}
			}
			catch(e1)
			{
				// Some errors occure while retieving city list so do nothing
				_drawNoData(canvobj);
				alert(e1);
			}

        }
    });
}

function _drawMyGraph(data, canvobj, graph_container, left_offset)
{
	var gd = canvobj.getContext("2d");

	var pic0 = new Image();
	pic0.src = '/img/ico-usr-pers.png';
	var pic1 = new Image();
	pic1.src = '/img/ico-usr-comp.png';

	var cw = canvobj.width;
	var ch = canvobj.height;
	var cx = cw / 2;	// ????? ??????? X
	var cy = ch / 2;	// ????? ??????? Y
	var cr = 9;			// ?????? ??????
	var p1 = 7, p2 = 5, p3 = 3;
	var pc = 2;			// ??????? ??????? ? ????????????? ?????
	var radius0 = 126;	// ??????? ??????? ?????? ??? ?????

	var incolor = "#f36624";	// Orange
	var outcolor = "#38b348";	// ???????
	var linecolor = "#bbbbbb";
	var bordercolor = "#ffffff";

	var hpad1 = 100, hspace = 0;
	var w0 = 180, w1 = 130, thick1 = 4;
	var w2 = 90, thick2 = 2;
	var w3 = 70, thick3 = 1;
	var arrlen = 14;

	var lblx = -30+left_offset;
	var lbly = 8;

	//////////////////////////////////////////
	// Draw in lines (get help lines) - left from center
	var indat = data.in;
	hspace = Math.ceil((ch - hpad1)/indat.length);

	// First level
	for( var i=0; i<indat.length; i++ ) {
		// Init axis to default
		gd.setTransform(1, 0, 0, 1, 0, 0);

		x = cx - w0 + 0.5;
		y0 = (hspace*Math.ceil(i/2)*( (i % 2 == 0) ? -1 : 1 ));
		y = cy + y0 + 0.5;

		tn0 = y0 / w0;

		ang = Math.atan2( y-cy, x-cx );			// find angle from X axis to line [-180:180] in rad
		ang_grad = (360.0/(2*Math.PI)) * ang;	// convert to degrees for testing

		//alert( Math.ceil(i/2) +"*" + (i%2) );

		// Draw line with arrow
		gd.strokeStyle = linecolor;
		gd.lineWidth = thick1;

		gd.beginPath();
		gd.moveTo(cx, cy);
		gd.lineTo(x, y);
		gd.closePath();
		gd.stroke();

		// Draw end point
		gd.lineWidth = pc;
		gd.fillStyle = incolor;
		gd.strokeStyle = bordercolor;

		gd.beginPath();
		gd.arc(x, y, p1, 0, 2*Math.PI, true);
		gd.closePath();
		gd.fill();
		gd.stroke();

		graph_container.append('<div style="left: '+Math.round(x+lblx)+'px; top: '+Math.round(y+lbly)+'px;"><a href="http://wayofhelp.com/users/viewinfo/'+indat[i].uid+'/" target="_blank">' + trimNameForGraph(indat[i].uname)  + '</a></div>');

		//gd.beginPath();
		//gd.fillStyle = "#337ab7";
		//gd.font = "11px Arial";
		//gd.fillText(y+" : "+ang+" : "+ang_grad, 10, hspace*i + 40);

		// Draw arrow in line end
		gd.translate(cx, cy);
		gd.rotate(ang - Math.PI/2);

		gd.fillStyle = linecolor;
		gd.beginPath();
		gd.moveTo(2, cr+20);
		gd.lineTo(-2, cr+20);
		gd.lineTo(-5, cr+20+arrlen);
		gd.lineTo(5, cr+20+arrlen);
		gd.closePath();
		gd.fill();
	}

	//////////////////////////////////////////
	// Draw in lines (get help lines) - left from center
	var outdat = data.out;
	hspace = Math.ceil((ch - hpad1)/outdat.length);

	// First level
	for( var i=0; i<outdat.length; i++ )
	{
		// Init axis to default
		gd.setTransform(1, 0, 0, 1, 0, 0);

		x = cx + w0 + 0.5;
		y0 = (hspace*Math.ceil(i/2)*( (i % 2 == 0) ? -1 : 1 ));
		y = cy + y0 + 0.5;

		tn0 = y0 / w0;

		ang = Math.atan2( y-cy, x-cx );			// find angle from X axis to line [-180:180] in rad
		ang_grad = (360.0/(2*Math.PI)) * ang;	// convert to degrees for testing

		//alert( Math.ceil(i/2) +"*" + (i%2) );

		// Draw line with arrow
		gd.strokeStyle = linecolor;
		gd.lineWidth = thick1;

		gd.beginPath();
		gd.moveTo(cx, cy);
		gd.lineTo(x, y);
		gd.closePath();
		gd.stroke();

		//gd.beginPath();
		//gd.fillStyle = "#337ab7";
		//gd.font = "11px Arial";
		//gd.fillText(y+" : "+ang+" : "+ang_grad, 10, hspace*i + 40);

		////////////////////////////////////////////////////////////////

		var outd2 = outdat[i].outdata;
		hspace2 = Math.ceil(hspace / outd2.length);

		for( var j=0; j<outd2.length; j++ )
		{
			// Init axis to default
			gd.setTransform(1, 0, 0, 1, 0, 0);

			x2 = x + w1 + 0.5;
			y2_0 = (hspace2*Math.ceil(j/2)*( (j % 2 == 0) ? -1 : 1 ));
			y2 = y + y2_0 + 0.5;

			tn2_0 = y2_0 / w1;

			ang = Math.atan2( y2-y, x2-x );			// find angle from X axis to line [-180:180] in rad
			ang_grad = (360.0/(2*Math.PI)) * ang;	// convert to degrees for testing

			//alert( Math.ceil(i/2) +"*" + (i%2) );

			// Draw line with arrow
			gd.strokeStyle = linecolor;
			gd.lineWidth = thick2;

			gd.beginPath();
			gd.moveTo(x, y);
			gd.lineTo(x2, y2);
			gd.closePath();
			gd.stroke();

			// Draw end point
			gd.lineWidth = 2;
			gd.fillStyle = outcolor;
			gd.strokeStyle = bordercolor;

			gd.beginPath();
			gd.arc(x2, y2, p2, 0, 2*Math.PI, true);
			gd.closePath();
			gd.fill();
			gd.stroke();

			graph_container.append('<div style="left: '+Math.round(x2+lblx)+'px; top: '+Math.round(y2+lbly)+'px;"><a  href="http://wayofhelp.com/users/viewinfo/'+outd2[j].uid+'/" target="_blank">' + trimNameForGraph(outd2[j].uname) + '</a></div>');

			// Draw arrow in line end
			gd.translate(x, y);
			gd.rotate(ang - Math.PI/2);

			gd.fillStyle = linecolor;
			gd.beginPath();
			gd.moveTo(1, w1-p2);
			gd.lineTo(-1, w1-p2);
			gd.lineTo(-4, w1-arrlen-p2);
			gd.lineTo(4, w1-arrlen-p2);
			gd.closePath();
			gd.fill();
		}
		////////////////////////////////////////////////////////////////

		// Init axis to default
		gd.setTransform(1, 0, 0, 1, 0, 0);

		// Draw end point
		gd.lineWidth = pc;
		gd.fillStyle = outcolor;
		gd.strokeStyle = bordercolor;

		gd.beginPath();
		gd.arc(x, y, p1, 0, 2*Math.PI, true);
		gd.closePath();
		gd.fill();
		gd.stroke();

		graph_container.append('<div style="left: '+Math.round(x + lblx)+'px; top: '+Math.round(y+lbly)+'px;"><a href="http://wayofhelp.com/users/viewinfo/'+outdat[i].uid+'/" target="_blank">' + trimNameForGraph(outdat[i].uname) + '</a></div>');

		// Draw arrow in line end
		gd.translate(cx, cy);
		gd.rotate(ang - Math.PI/2);

		gd.fillStyle = linecolor;
		gd.beginPath();
		gd.moveTo(2, w0-p1);
		gd.lineTo(-2, w0-p1);
		gd.lineTo(-5, w0-arrlen-p1);
		gd.lineTo(5, w0-arrlen-p1);
		gd.closePath();
		gd.fill();
	}

	// Init axis to default
	gd.setTransform(1, 0, 0, 1, 0, 0);

	// Draw center point
	gd.fillStyle 	= "#337ab7";		// ?????
	gd.strokeStyle	= bordercolor;
	gd.lineWidth 	= pc;
	gd.beginPath();
	gd.arc(cx, cy, cr, 0, 2*Math.PI, true);
	gd.closePath();
	gd.fill();
	gd.stroke();

}

function updtCount(inp, slblid, maxl, txt)
{
	var ost = (maxl - inp.value.length);

	$("#"+slblid).html("<b>"+ost+"</b>"+txt);
	if( ost<=3 )
	{
		$("#"+slblid).addClass("ltcount_last");
	}
	else
	{
		$("#"+slblid).removeClass("ltcount_last");
	}

	if( ost <= 0 )
	{
		inp.value = inp.value.substr(0, (maxl-1));
	}
}

function loadBonusTree(uid, bid, sesid, divid) {
	if( uid == 0 )
		return;
	if( bid == 0 )
		return;
	if( sesid == '' )
		return;
	
	var canvpos = $("#"+divid).position();
	
	var post_req_str = "uid=" + uid + "&bid=" + bid + "&sesid=" + sesid + "&rnd=" + Math.random();
	//alert(post_req_str);
	
	$.ajax({
        type: "POST",
        url: req_ajx_host+"ajx/bonustree/",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	try {
				//console.log(data);
				
				var res = $.parseJSON(data);
				
				if( res.id != 0 ) {
					//location.href = req_ajx_host + 'cabinet/';
					//_drawMyGraph(res.data, canvobj, $("#"+canvid).parent(), canvpos.left);
					
					var dw = 500;	//$("#"+divid).width();
					var dh = 300;	//$("#"+divid).height();
					
					//console.log("wid: " + dw + "; height: " + dh);
					
					var pnum = res.pointsnum;
					
					var pointarrx = Array(pnum);
					var pointarry = Array(pnum);

					if(pnum == 64) {                                          //
						var YposArr = [0, 42, 18, 47.2, 40,   51,   60,   52, 60, 58, 42,   48, 32, 24.5, 25,   17, 66, 32, 23, 20, 34, 41,   54, 41, 72, 24, 33,   25, 29,   60, 31, 11, 54, 11, 44.5, 42.5, 71, 47, 45,   66,   51, 54, 64, 42, 47,   36, 8,  39, 19,   66, 38, 29, 27, 19, 36, 47, 48, 58, 26, 37, 19, 32, 54, 35, 14, 53];
						var XposArr = [0, 69, 78, 77.7, 93.5, 89, 90.9, 66.5, 64, 78, 60, 62.3, 66, 68,   64.5, 75, 70, 69, 76, 84, 73, 80.5, 92, 85, 74, 88, 92.5, 72, 90.8, 84, 79, 80, 74, 73, 71.4, 76.5, 81, 83, 89.5, 87.5, 71, 63, 77, 64, 67.5, 62, 77, 72, 80.5, 66, 68, 76, 84, 68, 77, 93, 86, 71, 80, 90, 72, 87.8, 85, 83, 78, 72];
					}
					if(pnum == 32) {                                          //
						var YposArr = [0, 43,   40, 47, 45, 49,   59,   52, 59, 62, 42, 46, 34, 23, 39, 17,   65, 32, 21, 20, 34, 38, 45, 41, 52, 26, 35, 25, 35, 56, 31, 11, 54];
						var XposArr = [0, 70.5, 76, 77, 82, 88.5, 90.5, 66, 64, 77, 61, 65, 64, 68, 67, 74.5, 68, 68, 78, 84, 73, 80, 93, 87, 62, 86, 91, 72, 86, 84, 79, 78, 72];
					}
					if(pnum == 26) {                                          //
						var YposArr = [0, 42, 40, 47, 45, 49, 58, 50, 58, 62, 44, 38, 30, 27, 15, 23, 57, 15, 18, 20, 32, 35, 45, 39, 9, 28, 34];
						var XposArr = [0, 70.5, 76, 77, 82, 87, 91, 66, 63, 77, 61, 64, 62, 68, 70, 75, 69, 70, 80, 85, 73, 83, 91, 87, 76, 85, 92];
					}

					for( var i=1; i<=pnum; i++ ) {

						// check if the point was open in previous session
						var opened = false;
						for(var j=0; j<res.points.length; j++) {
							if( res.points[j] == i ) {
								opened = true;
								break;
							}
						}
						if (pnum == 26){
							$("#"+divid).append('<a href="#" class="bonustree-point'+(opened ? ' bonustree-point-empty' : '')+'" id="bonuspoint'+i+'" data-ind="'+i+'" style="left:'+XposArr[i]+'%; top: '+YposArr[i]+'%;"></a>');
						}

						if (pnum == 32){
							$("#"+divid).append('<a href="#" class="bonustree-point32'+(opened ? ' bonustree-point-empty' : '')+'" id="bonuspoint'+i+'" data-ind="'+i+'" style="left:'+XposArr[i]+'%; top: '+YposArr[i]+'%;"></a>');
						}

						if (pnum == 64){
							$("#"+divid).append('<a href="#" class="bonustree-point64'+(opened ? ' bonustree-point-empty' : '')+'" id="bonuspoint'+i+'" data-ind="'+i+'" style="left:'+XposArr[i]+'%; top: '+YposArr[i]+'%;"></a>');
						}

					}
					assignBonusPointHandler(uid, bid, sesid, res.id, res.reqid, res.reqco);
				}
				else
				{
					$.fancybox.open({type: 'iframe',
						maxWidth    : 1000,
						fitToView	: false,
						width: 700,
						height: 400,
						helpers: {
							title: {
								type: 'float'
							},
						},
						autoSize    : false,
						closeClick	: false,
						openEffect	: 'none',
						closeEffect	: 'none',
						padding : 0,
						href: req_ajx_host+'ajx/dialog/?dlg=getbonusresdlg&res=1'})

				}
								
			}
			catch(e1)
			{
				// Some errors occure while retieving city list so do nothing
				//_drawNoData(canvobj);
				alert(e1);
			}

        }
    });
}

function assignBonusPointHandler(uid, bid, sesid, quizid, reqid, reqco)
{
	$(".bonustree-point").bind("click", function(e){
		checkBonusTreePoint( $(this).attr("data-ind"), uid, bid, quizid, sesid, reqid, reqco );
		e.preventDefault();
	});

	$(".bonustree-point32").bind("click", function(e){
		checkBonusTreePoint( $(this).attr("data-ind"), uid, bid, quizid, sesid, reqid, reqco );
		e.preventDefault();
	});

	$(".bonustree-point64").bind("click", function(e){
		checkBonusTreePoint( $(this).attr("data-ind"), uid, bid, quizid, sesid, reqid, reqco );
		e.preventDefault();
	});
}

function checkBonusTreePoint(pointind, uid, bid, quizid, sesid, reqid, reqco) {
	if( sesid == '' )
		return;
	
	var post_req_str = "uid=" + uid + "&bid=" + bid + "&quizid=" + quizid + "&pointind=" + pointind + "&sesid=" + sesid + "&reqid=" + reqid;
	
	//alert(post_req_str);
	
	$.ajax({
        type: "POST",
        url: req_ajx_host+"ajx/bonuscheck/",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	try
        	{
				//alert(data);
				//console.log(data);
				
				var res = $.parseJSON(data);
				
				if( res.avail == 0 )
				{
					console.log("No more tries");																			
				}
				else 
				{
					if( res.found == 1 )
					{
						sendWinMsg(bid);

						$.fancybox.open({type: 'iframe',
							maxWidth    : 1000,
							fitToView	: false,
							width: 700,
							height: 400,
							helpers: {
								title: {
									type: 'float'
								},
							},
							autoSize    : false,
							closeClick	: false,
							openEffect	: 'none',
							closeEffect	: 'none',
							padding : 0,
							href: req_ajx_host+'ajx/dialog/?dlg=getbonusresdlg&res=2&bid=' + bid})
					}
					else
					{
						// Not found
						$("#bonuspoint"+pointind).addClass("bonustree-point-empty");

						if( res.nomore == 1 )
						{
							$.fancybox.open({type: 'iframe',
								maxWidth    : 1000,
								fitToView	: false,
								width: 700,
								height: 400,
								helpers: {
									title: {
										type: 'float'
									},
								},
								autoSize    : false,
								closeClick	: false,
								openEffect	: 'none',
								closeEffect	: 'none',
								padding : 0,
								href: req_ajx_host+'ajx/dialog/?dlg=getbonusresdlg&res=3&reqco=' + reqco + '&bid=' + bid})

						}
					}
				}
			}
			catch(e1)
			{
				// Some errors occure while retieving city list so do nothing
				//_drawNoData(canvobj);
				alert(e1);
			}
			
        }
    });
}

function loadMoreNews(pi, pn, contid) {
	var post_req_str = "pi="+pi+"&pn="+pn;

	$.ajax({
		type: "POST",
		url: req_ajx_host+"ajx/morenews/",
		data: post_req_str,
		dataType: "text",
		success: function(data) {
			try {
				//var res = $.parseJSON(data);
				$(contid).append(data);
			}
			catch(e1) {
				// Some errors occure while retieving city list so do nothing
				$(contid).html("Error loading data...");
			}

		}
	});
}

function setUserHelp(uid, pid) {
	var post_req_str = 'projid='+pid;

	$.ajax({
		type: "POST",
		url: req_ajx_host+"proj/sendlasthlp/",
		data: post_req_str,
		dataType: "text",
		success: function(data) {}
	});
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function createCookie(name,value,time) {
	if (time) {
		var date = new Date();
		date.setTime(date.getTime()+time);
		var expires = "; expires="+date.toUTCString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function isEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(email);
}

function checkEmailbyAjax(email) {
	var check = '';

	$.ajax({
		type: "POST",
		url: req_ajx_host + "ajx/checkmail/",
		data: 'email='+email,
		async: false,
		dataType: "json",
		success: function (data) {
			check = data
		}
	});

	return check;
}

function closePopNotify(pid) {
	if(pid)
		showTip(pid);
}

$('#btnshowproj').bind('click', function () {
	popWnd('projcreate', '');
});

$( "#toplshow" ).click(function() {
	$( "#toplshow" ).hide();
	$( "#toplhide" ).show();
	$( "#test" ).show(100);
});

$( "#toplhide" ).click(function() {
	$( "#toplshow" ).show();
	$( "#toplhide" ).hide();
	$( "#test" ).hide(100);
});

function loadProjComment(num, projid, contid)
{
	var post_comm_str = "projid=" + projid + "&pi=" + num + "&pn=10";

	$.ajax({
		type: "POST",
		url: req_ajx_host+"ajx/projcomms/",
		data: post_comm_str,
		dataType: "text",
		success: function(data) {

			try
			{
				$(contid).append(data);
			}
			catch(e1)
			{
				// Some errors occure while retieving city list so do nothing
				$(contid).append("Error loading data...");
			}

		}
	});
}

function addItemClick(id) {
	$.ajax({
		type: "POST",
		url: req_ajx_host+"ajx/addclick/",
		data: 'pid='+id,
		dataType: "text",
		success: function(data) {}
	});
}

function sendWinMsg(bid) {
	$.ajax({
		type: "POST",
		url: req_ajx_host+"ajx/sendbonmsg/",
		data: 'bid='+bid,
		dataType: "text",
		success: function(data) {}
	});
}


