var head_scroll_down = false;
var socwnd_was_closed = false;

//
//  пример вызова функции
//	drawMaingraphCabinet(123, function(res){alert(res.status)‌​;});
//

var req_ajx_host = "http://wayofhelp.com/";

// Return random int number including min but excluding max
function getRandomInt(min, max) 
{
	return Math.floor(Math.random() * (max - min)) + min;
}

function trimNameForGraph(name)
{
	return ( name.length > 16 ? name.substr(0,15) +"..." : name );
}

function _drawNoData(canvobj)
{
	var gd = canvobj.getContext("2d");
	
	gd.fillStyle = "#f91010";
	gd.font = "18px Arial";
	gd.fillText("Ошибка загрузки данных",Math.round(canvobj.width/2)-100,Math.round(canvobj.height/2)+8);
}


function drawMaingraphCabinet(uid, succesCallback)
{
	//var uid = $("#"+canvid).attr("data-usr");
	if( uid == null )
		return;
	
	//var canvpos = $("#"+canvid).position();	
	//var canvobj = document.getElementById(canvid);
	
	var post_req_str = "uid=" + uid;
	
	$.ajax({
        type: "POST",
        url: req_ajx_host+"ajx/mygraphdata/",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	try
        	{
				//alert(data);
				console.log(data);
				
				var res = $.parseJSON(data);				
				
				succesCallback.call(res);
				
				/*
				if( res.status == "ok" )
				{
					//location.href = req_ajx_host + 'cabinet/';
					_drawMyGraph(res.data, canvobj, $("#"+canvid).parent(), canvpos.left);
				}
				else
				{
					_drawNoData(canvobj);
				}
				*/
			}
			catch(e1)
			{
				// Some errors occure while retieving city list so do nothing
				_drawNoData(canvobj);
				alert(e1);
			}

        }
    });
}

function _drawMyGraph(data, canvobj, graph_container, left_offset)
{
	var gd = canvobj.getContext("2d");
	
	var pic0 = new Image();
	pic0.src = '/img/ico-usr-pers.png';
	var pic1 = new Image();
	pic1.src = '/img/ico-usr-comp.png';
	
	var cw = canvobj.width;
	var ch = canvobj.height;
	var cx = cw / 2;	// Центр рисунка X
	var cy = ch / 2;	// Центр рисунка Y
	var cr = 9;			// Радиус центра
	var p1 = 7, p2 = 5, p3 = 3;
	var pc = 2;			// Толщина контура у прорисованных точек
	var radius0 = 126;	// Внешний базовый радиус для линий
	
	var incolor = "#f36624";	// Orange
	var outcolor = "#38b348";	// Зеленый
	var linecolor = "#bbbbbb";
	var bordercolor = "#ffffff";
		
	var hpad1 = 100, hspace = 0;
	var w0 = 180, w1 = 130, thick1 = 4;
	var w2 = 90, thick2 = 2;
	var w3 = 70, thick3 = 1;
	var arrlen = 14;
	
	var lblx = -30+left_offset;
	var lbly = 8;
	
	//////////////////////////////////////////
	// Draw in lines (get help lines) - left from center
	var indat = data.in;	
	hspace = Math.ceil((ch - hpad1)/indat.length);
	
	// First level
	for( var i=0; i<indat.length; i++ )
	{
		// Init axis to default
		gd.setTransform(1, 0, 0, 1, 0, 0);
		
		x = cx - w0 + 0.5;
		y0 = (hspace*Math.ceil(i/2)*( (i % 2 == 0) ? -1 : 1 ));
		y = cy + y0 + 0.5;
		
		tn0 = y0 / w0;
		
		ang = Math.atan2( y-cy, x-cx );			// find angle from X axis to line [-180:180] in rad
		ang_grad = (360.0/(2*Math.PI)) * ang;	// convert to degrees for testing
		
		//alert( Math.ceil(i/2) +"*" + (i%2) );

		// Draw line with arrow
		gd.strokeStyle = linecolor;		
		gd.lineWidth = thick1;
		
		gd.beginPath();		
		gd.moveTo(cx, cy);
		gd.lineTo(x, y); 
		gd.closePath();
		gd.stroke();
		
		// Draw end point
		gd.lineWidth = pc;
		gd.fillStyle = incolor;
		gd.strokeStyle = bordercolor;
		
		gd.beginPath();
		gd.arc(x, y, p1, 0, 2*Math.PI, true);
		gd.closePath();
		gd.fill();
		gd.stroke();
		
		graph_container.append('<div style="left: '+Math.round(x+lblx)+'px; top: '+Math.round(y+lbly)+'px;"><a href="http://wayofhelp.com/users/viewinfo/'+indat[i].uid+'/" target="_blank">' + trimNameForGraph(indat[i].uname)  + '</a></div>');

		//gd.beginPath();
		//gd.fillStyle = "#337ab7";		
		//gd.font = "11px Arial";		
		//gd.fillText(y+" : "+ang+" : "+ang_grad, 10, hspace*i + 40);
		
		// Draw arrow in line end		
		gd.translate(cx, cy);
		gd.rotate(ang - Math.PI/2);
				
		gd.fillStyle = linecolor;	
		gd.beginPath();
		gd.moveTo(2, cr+20);
		gd.lineTo(-2, cr+20);
		gd.lineTo(-5, cr+20+arrlen); 
		gd.lineTo(5, cr+20+arrlen); 			
		gd.closePath();
		gd.fill();
	}
	
	//////////////////////////////////////////
	// Draw in lines (get help lines) - left from center
	var outdat = data.out;
	hspace = Math.ceil((ch - hpad1)/outdat.length);
	
	// First level
	for( var i=0; i<outdat.length; i++ )
	{
		// Init axis to default
		gd.setTransform(1, 0, 0, 1, 0, 0);
		
		x = cx + w0 + 0.5;
		y0 = (hspace*Math.ceil(i/2)*( (i % 2 == 0) ? -1 : 1 ));
		y = cy + y0 + 0.5;
		
		tn0 = y0 / w0;
		
		ang = Math.atan2( y-cy, x-cx );			// find angle from X axis to line [-180:180] in rad
		ang_grad = (360.0/(2*Math.PI)) * ang;	// convert to degrees for testing
		
		//alert( Math.ceil(i/2) +"*" + (i%2) );

		// Draw line with arrow
		gd.strokeStyle = linecolor;		
		gd.lineWidth = thick1;
		
		gd.beginPath();		
		gd.moveTo(cx, cy);
		gd.lineTo(x, y); 
		gd.closePath();
		gd.stroke();		

		//gd.beginPath();
		//gd.fillStyle = "#337ab7";		
		//gd.font = "11px Arial";		
		//gd.fillText(y+" : "+ang+" : "+ang_grad, 10, hspace*i + 40);
		
		////////////////////////////////////////////////////////////////
		
		var outd2 = outdat[i].outdata;
		hspace2 = Math.ceil(hspace / outd2.length);
		
		for( var j=0; j<outd2.length; j++ )
		{
			// Init axis to default
			gd.setTransform(1, 0, 0, 1, 0, 0);
		
			x2 = x + w1 + 0.5;
			y2_0 = (hspace2*Math.ceil(j/2)*( (j % 2 == 0) ? -1 : 1 ));
			y2 = y + y2_0 + 0.5;
			
			tn2_0 = y2_0 / w1;
			
			ang = Math.atan2( y2-y, x2-x );			// find angle from X axis to line [-180:180] in rad
			ang_grad = (360.0/(2*Math.PI)) * ang;	// convert to degrees for testing
			
			//alert( Math.ceil(i/2) +"*" + (i%2) );

			// Draw line with arrow
			gd.strokeStyle = linecolor;		
			gd.lineWidth = thick2;
			
			gd.beginPath();		
			gd.moveTo(x, y);
			gd.lineTo(x2, y2); 
			gd.closePath();
			gd.stroke();
			
			// Draw end point
			gd.lineWidth = 2;
			gd.fillStyle = outcolor;
			gd.strokeStyle = bordercolor;
			
			gd.beginPath();
			gd.arc(x2, y2, p2, 0, 2*Math.PI, true);
			gd.closePath();
			gd.fill();
			gd.stroke();	
			
			graph_container.append('<div style="left: '+Math.round(x2+lblx)+'px; top: '+Math.round(y2+lbly)+'px;"><a  href="http://wayofhelp.com/users/viewinfo/'+outd2[j].uid+'/" target="_blank">' + trimNameForGraph(outd2[j].uname) + '</a></div>');

			// Draw arrow in line end		
			gd.translate(x, y);
			gd.rotate(ang - Math.PI/2);
					
			gd.fillStyle = linecolor;	
			gd.beginPath();
			gd.moveTo(1, w1-p2);
			gd.lineTo(-1, w1-p2);
			gd.lineTo(-4, w1-arrlen-p2); 
			gd.lineTo(4, w1-arrlen-p2); 			
			gd.closePath();
			gd.fill();
		}
		////////////////////////////////////////////////////////////////
		
		// Init axis to default
		gd.setTransform(1, 0, 0, 1, 0, 0);
		
		// Draw end point
		gd.lineWidth = pc;
		gd.fillStyle = outcolor;
		gd.strokeStyle = bordercolor;
		
		gd.beginPath();
		gd.arc(x, y, p1, 0, 2*Math.PI, true);
		gd.closePath();
		gd.fill();
		gd.stroke();
		
		graph_container.append('<div style="left: '+Math.round(x + lblx)+'px; top: '+Math.round(y+lbly)+'px;"><a href="http://wayofhelp.com/users/viewinfo/'+outdat[i].uid+'/" target="_blank">' + trimNameForGraph(outdat[i].uname) + '</a></div>');
		
		// Draw arrow in line end		
		gd.translate(cx, cy);
		gd.rotate(ang - Math.PI/2);
				
		gd.fillStyle = linecolor;	
		gd.beginPath();
		gd.moveTo(2, w0-p1);
		gd.lineTo(-2, w0-p1);
		gd.lineTo(-5, w0-arrlen-p1); 
		gd.lineTo(5, w0-arrlen-p1); 			
		gd.closePath();
		gd.fill();
	}
	
	// Init axis to default
	gd.setTransform(1, 0, 0, 1, 0, 0);
	
	// Draw center point
	gd.fillStyle 	= "#337ab7";		// Синий
	gd.strokeStyle	= bordercolor;
	gd.lineWidth 	= pc;
	gd.beginPath();
	gd.arc(cx, cy, cr, 0, 2*Math.PI, true);
	gd.closePath();
	gd.fill();
	gd.stroke();
	
}
