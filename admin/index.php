<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("ADMIN_MODE", true);
define("TEST_MODE", false);

// Load all settings
include "../inc/db-inc.php";
//include "inc/connect-inc.php";
//include "inc/initcms-inc.php";

//phpinfo();

////////////////////////////////////////////////////////////////////////////////
// Start application
$uhcms_app = new UhCmsAdminApp($UHCMS_CONFIG);
if( $uhcms_app )
	$uhcms_app->run();


?>