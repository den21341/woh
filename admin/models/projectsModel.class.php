<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ProjectsModel extends Model {
	protected $LangId;
	public $ses;
	
	function __construct($config, $db, $LangId) {
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}

	// Return array with items

	public function get_ProjList($catModel, $pi=-1, $pn=50, $status = PROJ_STATUS_ALL, $sort = '', $flt = '') {
		$res = $catModel->Item_List(-1, 0, 0, $pi, $pn, 3, $sort, $status, $flt, 0, false, -1, 0, -1, 0, -1);

		for( $i=0; $i<count($res); $i++ ) {
			$res[$i]['account_group'] = $this->cfg['USR_TYPES'][$res[$i]['account_type']];
			$res[$i]['rnum_all'] = $catModel->Item_ReqCollected($res[$i]['id']);
			$res[$i]['rnum_conf'] = $catModel->Item_ReqCollected($res[$i]['id'], REQ_STATUS_CONFIRM);
		}

		return $res;
	}

	public function save_Proj($pid, $proj) {
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET amount='".number_format($proj['pamount'], 2, ".", "")."', active='".$proj['pactive']."', is_check='".$proj['pis_check']."', moderated='".$proj['pmoderated']."', admin_rate = '".$proj['padmrate']."', help_type = '".$proj['phlptype']."', location_type = '".$proj['ploctype']."' WHERE id='".$pid."' ";

		if( !$this->db->exec( $query ) ) {
			return false;
		}

		$query = "UPDATE ".TABLE_CAT_ITEMS_LANGS." SET title2='".addslashes($proj['ptit'])."', 
				descr0='".addslashes($proj['pdescr0'])."', descr='".addslashes($proj['pdescr'])."' 
			WHERE item_id='".$pid."' AND lang_id='".$this->LangId."'";

		if( !$this->db->exec( $query ) ) {
			return false;
		}

		$query = "UPDATE ".TABLE_CAT_CATITEMS." SET sect_id='".$proj['psect']."' WHERE item_id='".$pid."'";

		if( !$this->db->exec( $query ) ) {
			return false;
		}

		return true;
	}
	
	public function set_ProjStatus($pid, $status) {
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET status=".$status." WHERE id=$pid";
		if( !$this->db->exec( $query ) ) {
			// error
			return false;
		}
		//if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
		//{
		//	echo mysql_error();
		//}
		
		return true;
	}
	
	public function delete_Proj($pid) {
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET status='".PROJ_STATUS_ENDED."' WHERE id='$pid'";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}

	public function get_PayedItem($perc = 60) {
		$query = "SELECT i1.*, il1.title2, SUM(r1.req_amount) sum FROM ".TABLE_CAT_ITEMS." i1
					JOIN ".TABLE_CAT_ITEMS_LANGS." il1 ON i1.id = il1.item_id 
					JOIN ".TABLE_CAT_ITEMS_HELPREQ." r1 ON r1.item_id = i1.id
					 WHERE i1.amount_type = ".PROJ_TYPE_MONEY." AND r1.`show` = 1 GROUP BY i1.id HAVING(((100*sum)/i1.amount) > ".addslashes($perc).") ORDER BY i1.stop_date DESC ";
		return $this->db->query($query);
	}

	public function set_PayedItem($pid, $pyd) {
		$query = "UPDATE ".TABLE_CAT_ITEMS." SET paydone = ".addslashes($pyd)." WHERE id = ".addslashes($pid);
		return $this->db->exec($query);
	}
}