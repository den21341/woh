<?php

class DespageModel extends Model {

    protected $page;
    protected $langs;
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId, $langs)
    {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->langs = $langs;
        //$this->page = new SitePage($db, $LangId);
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function getPagesDes() {
        $query = "SELECT * FROM ".TABLE_PAGES_DES;
        return $this->db->query($query);
    }

    public function getPagesDesSubData($id) {
        $query = "SELECT * FROM ".TABLE_PAGES_DES_LANGS." WHERE page_des_id = ".$id;
        return $this->db->query($query);
    }

    public function savePagesData(array $data) {
        for($i = 1; $i<=$data['cobl']; ++$i) {
            $query = "UPDATE ".TABLE_PAGES_DES_LANGS." SET page_text = ". "'{$data[$i]}'"." WHERE page_block = ".$i." AND page_des_id = ".$data['paid'];
            $this->db->exec($query);
        }
    }

}