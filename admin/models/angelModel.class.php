<?php

class AngelModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId)
    {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function getUserMoneyList($col = 'money_day',$limit = 20) {
        $query = "SELECT b1.*, b2.name, b2.fname, b2.angel_day, b2.angel_week FROM ".TABLE_SHOP_BUYERS_BOX." as b1 
        JOIN ".TABLE_SHOP_BUYERS." as b2 ON b1.buyer_id = b2.id ORDER BY ".'b1.'.$col." DESC LIMIT 0, ".$limit;
        return $this->db->query($query);
    }

    public function getUserWithStatus() {
        $query = "SELECT b1.*, b2.name, b2.fname, b2.angel_day, b2.angel_week FROM ".TABLE_SHOP_BUYERS_BOX." as b1 
        JOIN ".TABLE_SHOP_BUYERS." as b2 ON b1.buyer_id = b2.id WHERE b2.angel_day = 1 OR b2.angel_week = 1";
        return $this->db->query($query);
    }

    public function setUserAngelStatus($uid, $st, $type = 'day', $money = 0) {
        switch ($type) {
            case 'day' :
                if($st) {
                    $query = "UPDATE " . TABLE_SHOP_BUYERS . " SET angel_day = " . $st . ",ad_count = ad_count+1, angel_day_date = '".date('Y-m-d')."' WHERE id = " . $uid;
                    $updq = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET ad_money = ".$money.", money = money + ".$money." WHERE buyer_id = ".$uid;
                    $this->db->exec($updq);
                }
                else $query = "UPDATE ".TABLE_SHOP_BUYERS." SET angel_day = ".$st." WHERE id = ".$uid;
                break;
            case 'week' :
                if($st) {
                    $query = "UPDATE ".TABLE_SHOP_BUYERS." SET angel_week = ".$st.",aw_count = aw_count+1, angel_week_date = '".date('Y-m-d')."' WHERE id =  ".$uid;
                    $updq = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET aw_money = ".$money.", money = money + ".$money." WHERE buyer_id = ".$uid;
                    $this->db->exec($updq);
                }
                else $query = "UPDATE ".TABLE_SHOP_BUYERS." SET angel_week = ".$st." WHERE id =  ".$uid;
                break;

            default: return false;
        }
        return $this->db->exec($query);
    }
}