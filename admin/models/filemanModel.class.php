<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("FM_ERR_EMPTY_NAME", 1);
define("FM_ERR_NOTEXIST", 2);
define("FM_ERR_EXIST", 3);
define("FM_ERR_MKDIR", 4);
define("FM_ERR_RMDIR", 5);
define("FM_ERR_UNLINK", 6);
define("FM_ERR_LOADFILE", 7);
define("FM_ERR_FILETYPE", 8);

define("FM_OK_MKDIR", -1001);
define("FM_OK_RMDIR", -1002);
define("FM_OK_UNLINK", -1003);
define("FM_OK_LOADFILE", -1004);

define("MAX_UPLOAD_SIZE", 5000000);

class FileManModel extends Model
{
	public $ses;
	protected $LangId;	
	private $tmp_dir;	
	
	function __construct($config, $db, $LangId, $tdir)
	{
		parent::__construct($config, $db);
		
		$this->tmp_dir = $tdir;
		
		$this->ses = UhCmsAdminApp::getSesInstance();		
		$this->LangId = $LangId;
	}
	
	public function dir_create($fname)
	{
		if( empty($fname) || $fname==".." || $fname=="." )
		{
			$this->setLastErr(FM_ERR_EMPTY_NAME);
			return false;
		}
		
		//echo $this->tmp_dir.$fname."<Br>";
		
		if( file_exists($this->tmp_dir.$fname) )
		{
			$this->setLastErr(FM_ERR_EXIST);
			return false;
		}
		
		if( !@mkdir($this->tmp_dir.$fname, 0777) )
		{
			$this->setLastErr(FM_ERR_MKDIR);
			return false;
		}
		
		$this->setLastErr(FM_OK_MKDIR);		
		return true;
	}
	
	public function dir_delete($fname)
	{
		if( empty($fname) || $fname==".." || $fname=="." )
		{
			$this->setLastErr(FM_ERR_EMPTY_NAME);
			return false;
		}
		
		//echo getcwd()."<br>".$this->tmp_dir.$fname."<Br>";
		if( !file_exists($this->tmp_dir.$fname) )
		{
			$this->setLastErr(FM_ERR_NOTEXIST);
			return false;
		}			
		
		if( !@rmdir($this->tmp_dir.$fname) )
		{
			$this->setLastErr(FM_ERR_RMDIR);
			return false;			
		}

		$this->setLastErr(FM_OK_RMDIR);		
		return true;
	}
	
	public function file_delete($fname)
	{
		if( empty($fname) || $fname==".." || $fname=="." )
		{
			$this->setLastErr(FM_ERR_EMPTY_NAME);
			return false;
		}
		
		if( !file_exists($this->tmp_dir.$fname) )
		{
			$this->setLastErr(FM_ERR_NOTEXIST);
			return false;
		}
		
		if( !@unlink($this->tmp_dir.$fname) )
		{
			$this->setLastErr(FM_ERR_UNLINK);
			return false;			
		}
		
		$this->setLastErr(FM_OK_UNLINK);	
		return true;
	}
	
	public function file_delete_arr($fnamearr)
	{
		$was_errors = false;
		
		for( $i=0; $i<count($fnamearr); $i++ )
		{
			if( !$this->file_delete($fnamearr[$i]) )
				$was_errors = true;			
		}
		
		$this->setLastErr(FM_OK_UNLINK);
		return $was_errors;
	}
	
	public function file_load($fname, $ftmpname)
	{
		$tmp_ext_pos=strrpos($fname,".");
		if($tmp_ext_pos)
		{
			$tmp_ext=substr($fname,$tmp_ext_pos+1);
			$tmp_ext=strtolower($tmp_ext);
		
			//if($tmp_ext && $tmp_ext!="" && in_array($tmp_ext,$ext_arr))
			if(isset($tmp_ext) && ($tmp_ext!=""))
			{
				$namefiles = $fname;
				//$upload_max_filesize = $MAX_UPLOAD_SIZE;
				if(file_exists($this->tmp_dir.$namefiles))
				{
					$i=1;
					while(file_exists($this->tmp_dir.$i.$namefiles))
					{
						$i++;
					}
					$namefiles = $i.$namefiles;
				}
				$uploadfile = $this->tmp_dir.$namefiles;
		
				if( move_uploaded_file($ftmpname, $uploadfile) )
				{					
					$this->setLastErr(FM_OK_LOADFILE);
					return true;
				}
				else
				{
					//$msg .= $strings['msgloaderr'][$lang];
					$this->setLastErr(FM_OK_LOADFILE);
				}
			}
			else
			{
				$this->setLastErr(FM_ERR_FILETYPE);								
			}
		}
		else
		{
			$this->setLastErr(FM_ERR_FILETYPE);
		}
		
		return false;
	}	
}
?>