<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CommentsModel extends Model
{
	protected $LangId;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function get_CommentList($catModel, $pi=-1, $pn=50)
	{
		$res = $catModel->Item_Comments(0, false, $pi, $pn, true);
		
		for( $i=0; $i<count($res); $i++ )
		{
			$res[$i]['account_group'] = $this->cfg['USR_TYPES'][$res[$i]['account_type']];
			//$res[$i]['rnum_all'] = $catModel->Item_ReqCollected($res[$i]['id']);
			//$res[$i]['rnum_conf'] = $catModel->Item_ReqCollected($res[$i]['id'], REQ_STATUS_CONFIRM);
		}
		
		return $res;
	}
	
	public function save_Comment($cid, $it)
	{
		$query = "UPDATE ".TABLE_CAT_ITEMS_COMMENT." SET 
				visible='".$it['pfirst']."', 
				rate='".$it['prate']."' 				
				WHERE id='".$cid."' ";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		$query = "UPDATE ".TABLE_CAT_ITEMS_COMMENT_LANGS." SET content='".$it['pdescr0']."' WHERE item_id='".$cid."' AND lang_id='".$this->LangId."'";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		return true;
	}
	
	public function set_ReqStatus($pid, $status)
	{	
		//$query = "UPDATE ".TABLE_CAT_ITEMS." SET status=".$status." WHERE id=$pid";
		//if( !$this->db->exec( $query ) )
		//{
		//	// error
		//	return false;
		//}
		//if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
		//{
		//	echo mysql_error();
		//}
		
		return true;
	}
	
	public function delete_Comment($cid)
	{
		$query = "DELETE FROM ".TABLE_CAT_ITEMS_COMMENT." WHERE id='$cid'";
		if( !$this->db->exec( $query ) )
			return false;
		
		$query = "DELETE FROM ".TABLE_CAT_ITEMS_COMMENT_LANGS." WHERE item_id='$cid'";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
}
?>