<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ResourcesModel extends Model
{
	public $ses;
	protected $LangId;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->ses = UhCmsAdminApp::getSesInstance();		
		$this->LangId = $LangId;
	}
	
	public function get_ResourceList()
	{
		$arr_val = Array();
		
		$query = "SELECT r1.*, r2.id as rid, r2.content FROM ".TABLE_RESOURCE." r1
			INNER JOIN ".TABLE_RESOURCE_LANGS." r2 ON r1.id=r2.item_id AND r2.lang_id='".$this->LangId."'
			ORDER BY r1.sort_num, r1.id";
		
		$res = $this->db->query( $query );
		
		return $res;		
	}	
	
	public function save_Resource($rid, $text)
	{
		$query = "UPDATE ".TABLE_RESOURCE_LANGS." SET content='".addslashes($text)."' WHERE id='".$rid."' ";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		return true;
	}	
}
?>