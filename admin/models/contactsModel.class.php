<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ContactsModel extends Model
{
	public $ses;
	protected $LangId;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->ses = UhCmsAdminApp::getSesInstance();		
		$this->LangId = $LangId;
	}
	
	public function get_ContList()
	{
		$arr = Array('ids' => Array(), 'vals' => Array());
		
		$query = "SELECT * FROM ".TABLE_SITE_OPTIONS." WHERE lang_id='".$this->LangId."' ORDER BY id";
		
		$res = $this->db->query( $query );		
		for( $i=0; $i<count($res); $i++ )
		{
			$arr['ids'][] = $res[$i]['id'];
			$arr['vals'][] = stripslashes($res[$i]['value']);
		}		
		unset($res);
		
		return $arr;		
	}	
	
	public function save_Cont($id, $val)
	{
		for( $i=0; $i<count($id); $i++ )
		{
			$query = "UPDATE ".TABLE_SITE_OPTIONS." SET value='".addslashes($val[$i])."' WHERE id='".$id[$i]."' ";
			if( !$this->db->exec( $query ) )
			{
				//
				return false;
			}
		}
		
		return true;
	}	
}
?>