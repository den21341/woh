<?php

class DevelopModel extends Model {
    public $ses;
    protected $LangId;

    function __construct($config, $db, $LangId)
    {
        parent::__construct($config, $db);

        $this->ses = UhCmsAdminApp::getSesInstance();
        $this->LangId = $LangId;
    }
}