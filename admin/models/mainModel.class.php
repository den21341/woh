<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class MainModel extends Model
{
	public $ses;
	protected $LangId;
	
	function __construct($config, $db, $LangId=1)
	{
		parent::__construct($config, $db);
		
		$this->ses = UhCmsAdminApp::getSesInstance();		
		$this->LangId = $LangId;
	}	
}
?>