<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ClientsModel extends Model
{
	protected $user;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->user = new User($db);
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function save_Buyer($uid, $uprof)
	{
		if( $this->ses->UserId == 1 )
			return false;
		
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET login='".addslashes($uprof['login'])."',
					name='".addslashes($uprof['name'])."', fname='".addslashes($uprof['fname'])."', 
					phone='".addslashes($uprof['phone'])."', email='".addslashes($uprof['email'])."', 
					web_url='".addslashes($uprof['web_url'])."',
					account_type='".$uprof['account_type']."' WHERE id='".$uid."' ";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		return true;
	}
	
	public function set_BuyerStatus($uid, $active)
	{	
		$query = "UPDATE ".TABLE_SHOP_BUYERS." SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid";
		if( !$this->db->exec( $query ) )
		{
			// error
			return false;
		}
		//if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
		//{
		//	echo mysql_error();
		//}
		
		return true;
	}
	
	public function delete_Buyer($uid)
	{
		$query = "DELETE FROM ".TABLE_SHOP_BUYERS." WHERE id='$uid'";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
}
?>