<?php

class PromiseModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId) {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function getPromiseAllItem() {
        $query = "SELECT i1.id, l1.title2, r1.req_amount, b1.id uid, b1.name, b1.fname, r1.add_date, c1.name currname FROM ".TABLE_CAT_ITEMS." i1
                JOIN ".TABLE_CAT_ITEMS_LANGS." l1 ON i1.id = l1.item_id
                JOIN ".TABLE_CAT_ITEMS_HELPREQ." r1 ON r1.item_id = i1.id
                JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = r1.sender_id
                LEFT JOIN ".TABLE_CAT_BUYER_SMS." s1 ON s1.buyer_id = b1.id
                JOIN ".TABLE_CAT_CURRENCY_LANGS." c1 ON c1.item_id = i1.currency_id
                WHERE r1.is_promise = 1 
                ORDER BY r1.add_date DESC";
        return $this->db->query($query);
    }

    public function getPromiseByAllItem($perc = 0) {
        $perc_con = "";
        $where_cond = ' i1.status = 0 AND i1.archive = 0 AND i1.active = 1 ';

        if($perc > 0) {
            $perc_con = " AND ((r1.req_amount/i1.amount)*100) > ".addslashes($perc).' ';
        } else if($perc >= 60) {
            $where_cond = "";
        }

        $query = "SELECT r1.item_id, l1.title2, i1.amount, SUM(r1.req_amount) req_amount, (SELECT SUM(req_amount) FROM jhlp_cat_requests WHERE req_type = 0 AND is_promise = 1 AND item_id = r1.item_id) promise_sum FROM jhlp_cat_requests r1 
                    JOIN jhlp_cat_item i1 ON i1.id = r1.item_id
                    JOIN jhlp_cat_item_lang l1 ON l1.item_id = i1.id
                    WHERE ".$where_cond." AND r1.req_type = 0 ".$perc_con."
                    GROUP BY r1.item_id
                    HAVING (promise_sum > 0)";
      
        return $this->db->query($query);
    }

    public function getUserPromiseByItem($pid) {
        $query = "SELECT r1.item_id, b1.id uid, b1.name, b1.fname, SUM(r1.req_amount) req_amount, s1.phone, s1.is_active FROM `jhlp_cat_requests` r1
        JOIN jhlp_cat_buyer b1 ON r1.sender_id = b1.id
        LEFT JOIN jhlp_cat_buyer_sms s1 ON s1.buyer_id = b1.id
        JOIN jhlp_cat_item i1 ON i1.id = r1.item_id
        WHERE i1.status = ".PROJ_STATUS_RUN." AND i1.archive = 0 AND i1.active = 1 AND r1.is_promise = ".REQ_PROMISE." AND r1.item_id = ".addslashes($pid)."
        GROUP BY b1.id";
        return $this->db->query($query);
    }

    public function sendBotMsg($fromid, $toid, $txt, $projid = 0, $replyto = 0) {
        $query = "INSERT INTO " . TABLE_CAT_MSG . " (item_id, from_id, to_id, reply_msg_id, add_date, modify_date, message) 
			VALUES ('$projid', '$fromid', '$toid', '$replyto', NOW(), NOW(), '" . addslashes($txt) . "')";
        if (!$this->db->exec($query))
            return false;

        $msg_id = $this->db->insert_id();
        if ($msg_id == 0)
            return false;

        $query = "INSERT INTO " . TABLE_CAT_MSG_P2P . " (msg_id, user_id, add_date, modify_date) 
			VALUES ('$msg_id', '$fromid', NOW(), NOW() )";
        if (!$this->db->exec($query))
            return false;

        $query = "INSERT INTO " . TABLE_CAT_MSG_P2P . " (msg_id, user_id, add_date, modify_date)
		VALUES ('$msg_id', '$toid', NOW(), NOW() )";
        if (!$this->db->exec($query))
            return false;

        $to_email = "";
        $from_name = "";

        $query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='" . $toid . "'";
        $res = $this->db->query($query);
        if (count($res) > 0) {
            $to_email = $res[0]['email'];
        }

        $query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='" . $fromid . "'";
        $res = $this->db->query($query);
        if (count($res) > 0) {
            $from_name = ($res[0]['account_type'] == USR_TYPE_PERS ? $res[0]['name'] . ' ' . $res[0]['fname'] : $res[0]['orgname']);
        }

        // Send notify mail
        if (($to_email != "") && ($from_name != ""))
            $this->sendMsgNotifyMail($to_email, $from_name, $txt);

        return true;
    }

    protected function sendMsgNotifyMail($tomail, $sender_name, $msgtext) {
        if( $tomail == "" )
            return;

        $subject = "Новое сообщение на сайте WayOfHelp";
        $txt = "<hr><div><h3>Вам пришло новое сообщение на сайте <a href=".WWWHOST.">".$this->cfg['NAME_RU']."</h3></a></div>

<br>Сообщение от <b>".$sender_name."</b>:
".$msgtext."

<br><br>Это письмо отправлено в автоматическом режиме и на него не нужно отвечать. Чтобы написать ответ на полученное сообщение вам необходимо войти в свой кабинет.
	
<br><br><hr>Служба поддержки ".$this->cfg['NAME_RU']."
";

        UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $txt);
    }

}

