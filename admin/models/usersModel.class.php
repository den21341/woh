<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UsersModel extends Model
{
	protected $user;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->user = new User($db);
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function get_groupList()
	{
		return $this->user->groupList();
	}
	
	public function get_userList()
	{
		$cond = "";
    	if( $this->ses->UserGroup == GROUP_ADMIN )
    	{
            if( $this->ses->UserId != 1 )
            {
            	$cond = " AND u.id<>1 ";
			}
		}
		else
		{
			$cond = " AND u.id<>1 AND u.group_id<>".GROUP_ADMIN." ";
		}
		
		return $this->user->userList($cond);
	}
	
	public function get_UserInfo($uid)
	{
		return $this->user->userInfo($uid);
	}
	
	public function add_User($uprof)
	{		
		if( $this->ses->UserGroup == GROUP_ADMIN )
		{
			$newuid = UhCmsUtils::makeUuid();
			
			$query = "INSERT INTO ".TABLE_USERS."
					(login, passwd, activation_code, isactive, name, address, city_id, zip_code, telephone,
					office_phone, cell_phone, email1, email2, email3, web_url, group_id)
				VALUES ('".addslashes($uprof['login'])."', PASSWORD('".addslashes($uprof['passwd'])."'), '$newuid', 1, '".addslashes($uprof['name'])."',
				'".addslashes($uprof['address'])."', '".addslashes($uprof['city_id'])."', '".addslashes($uprof['zip_code'])."',
				'".addslashes($uprof['telephone'])."', '".addslashes($uprof['office_phone'])."', '".addslashes($uprof['cell_phone'])."',
				'".addslashes($uprof['email1'])."', '".addslashes($uprof['email2'])."', '".addslashes($uprof['email3'])."',
				'".addslashes($uprof['web_url'])."', '".$uprof['group_id']."')";
				
			$this->db->exec( $query );			
		}		
	}
	
	public function save_User($uid, $uprof)
	{
		if( $this->ses->UserId == 1 )
			return false;
		
		$query = "UPDATE ".TABLE_USERS." SET login='".addslashes($uprof['login'])."',
					name='".addslashes($uprof['name'])."', address='".addslashes($uprof['address'])."',
					city_id='".addslashes($uprof['city_id'])."',
					zip_code='".addslashes($uprof['zip_code'])."', telephone='".addslashes($uprof['telephone'])."',
					office_phone='".addslashes($uprof['office_phone'])."', cell_phone='".addslashes($uprof['cell_phone'])."',
					email1='".addslashes($uprof['email1'])."', email2='".addslashes($uprof['email2'])."',
					email3='".addslashes($uprof['email3'])."', web_url='".addslashes($uprof['web_url'])."',
					group_id='".$uprof['group_id']."' WHERE id='".$uid."' ";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		return true;
	}
	
	public function set_Status($uid, $active)
	{	
		$query = "UPDATE ".TABLE_USERS." SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid";
		if( !$this->db->exec( $query ) )
		{
			// error
			return false;
		}
		//if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
		//{
		//	echo mysql_error();
		//}
		
		return true;
	}
	
	public function delete_User($uid)
	{
		if( $this->ses->UserId == $uid )
			return false;
	
		if( $uid == 1 )
			return false;
	
		$query = "DELETE FROM ".TABLE_USERS." WHERE id='$uid' AND id<>1";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
}
?>