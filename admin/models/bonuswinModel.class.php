<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class BonusWinModel extends Model
{
	protected $LangId;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function get_BonusWinList($sortby="", $pi=-1, $pn=50)
	{
		$sort_cond = " q1.add_date DESC ";
		switch( $sortby )
		{
			case "quiz":
				$sort_cond = " b2.title2, q1.add_date DESC ";
				break;
				
			case "user":
				$sort_cond = " u1.fname, u1.name, q1.add_date DESC ";
				break;
		}
		
		$query = "SELECT q1.*, w1.id as winid, w1.point_ind, b1.id as bonusid, b1.archive, b1.difficulty, b1.amount, b1.amount_used, b1.amount_val, b1.add_date, 
				b2.title2, u1.fname, u1.name, u1.account_type, u1.login, u1.orgname, u1.email, u1.phone    
			FROM ".TABLE_CAT_BONUS_QUIZ." q1 
			INNER JOIN ".TABLE_CAT_BONUS_WINS." w1 ON q1.id=w1.quiz_id 
			INNER JOIN ".TABLE_CAT_BONUS." b1 ON q1.item_id=b1.id 
			INNER JOIN ".TABLE_CAT_BONUS_LANGS." b2 ON b1.id=b2.item_id AND b2.lang_id='".$this->LangId."' 
			INNER JOIN ".TABLE_SHOP_BUYERS." u1 ON q1.user_id=u1.id 
			WHERE q1.status=".BONUS_QUIZ_STATUS_WON." 
			ORDER BY $sort_cond ";
		$res = $this->db->query($query);
		
		return $res;
	}
		
	/*
	public function save_Req($rid, $proj)
	{
		$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ_RATE." SET 
				show_first='".$proj['pfirst']."', 
				rate='".$proj['prate']."',				
				rate_comments='".addslashes($proj['pdescr0'])."'  
				WHERE id='".$rid."' ";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		return true;
	}
	*/
	
	public function delete_Win($winid)
	{
		$query = "SELECT q1.*, w1.id as winid, w1.point_ind 
			FROM ".TABLE_CAT_BONUS_QUIZ." q1
			INNER JOIN ".TABLE_CAT_BONUS_WINS." w1 ON q1.id=w1.quiz_id
			WHERE w1.id='".$winid."'";
		$res = $this->db->query($query);
		
		$query = "UPDATE ".TABLE_CAT_BONUS_QUIZ." SET status='".BONUS_QUIZ_STATUS_USED."' WHERE id='".$res[0]['id']."'";
		if( !$this->db->exec( $query ) )
			return false;
		
		$query = "DELETE FROM ".TABLE_CAT_BONUS_WINS." WHERE id='$winid'";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
}
?>