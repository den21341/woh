<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ExportDataModel extends Model
{
	protected $LangId;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;		
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function get_Clients()
	{
		$query = "SELECT * FROM ".TABLE_SHOP_BUYERS." ORDER BY id";
		$res = $this->db->query( $query );
		
		return $res;
	}
}
?>