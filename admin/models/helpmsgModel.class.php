<?php

class HelpmsgModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId)
    {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function getUsrHelpMsg() {
        $query = "SELECT * FROM ".TABLE_HELP_MSG." ORDER BY date DESC";
        return $this->db->query($query);
    }
}