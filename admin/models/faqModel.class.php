<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class FaqModel extends Model
{	
	public $news; 
	public $ses;
	public $langs;
	protected $LangId;
	
	function __construct($config, $db, $LangId, $langs)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->langs = $langs;
				
		$this->ses = UhCmsAdminApp::getSesInstance();		
		$this->faqs = new UhCmsFaqs($this->db, $LangId);
	}

	public function get_faqGroups()
	{
		return $this->faqs->Faqs_GroupList();
	}
	
	public function get_faqGroupInfo($group_id)
	{
		return $this->faqs->Faqs_GroupInfo($group_id);
	}
	
	public function get_faqAll()
	{
		return $this->faqs->Faqs_ItemsAll();
	}
	
	public function get_faqList($group=0)
	{
		if( $group == 0 )
			return $this->faqs->Faqs_ItemsAll();
			
		return $this->faqs->Faqs_Items($group);
	}
	
	public function get_faqInfo($id)
	{
		return $this->faqs->Faqs_ItemInfo($id);
	}
	
	public function add_faqGroup($req)
	{
		$query = "INSERT INTO ".TABLE_FAQ_GROUP." ( sort_num, filename )
			VALUES ( '".$req['sort_num']."', '".addslashes($req['filename_src'])."')";		
		if( $this->db->exec($query) )
		{
			$newid = $this->db->insert_id();
		
			for( $i=0; $i<count($this->langs); $i++ )
			{
				$query = "INSERT INTO ".TABLE_FAQ_GROUP_LANGS." ( group_id, lang_id, name, descr)
				VALUES ('$newid', '".$this->langs[$i]['id']."', '".addslashes($req['title'])."', '".addslashes($req['content'])."')";
				if( !$this->db->exec($query) )
				{
					return false;
				}
			}
		}
		else
			return false;
			
		return true;
	}
	
	public function save_faqGroup($group_id, $req)
	{
		$query = "UPDATE ".TABLE_FAQ_GROUP." SET sort_num='".$req['sort_num']."', filename='".addslashes($req['filename_src'])."' WHERE id='".$group_id."'";
		//echo $query."<br>";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		$query =  "UPDATE ".TABLE_FAQ_GROUP_LANGS." SET name='".addslashes($req['title'])."', descr='".addslashes($req['content'])."'
				WHERE group_id='".$group_id."' AND lang_id='".$this->LangId."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		return true;
	}
	
	public function delete_faqGroup($group_id)
	{
		$query = "DELETE FROM ".TABLE_FAQ_GROUP_LANGS." WHERE group_id='$group_id'";
		if( !$this->db->exec( $query ) )
			return false;
		else
		{
			$query = "DELETE FROM ".TABLE_FAQ_GROUP." WHERE id='$group_id'";
			if( !$this->db->exec( $query ) )
				return false;
		}
		
		return true;
	}
	
	public function add_faq($req)
	{
		$query = "INSERT INTO ".TABLE_FAQ." ( group_id, sort_num, add_date, filename)
			VALUES ( '".$req['group_id']."', '".$req['sort_num']."', NOW(), '".addslashes($req['filename_src'])."')";
		
		if( $this->db->exec($query) )
		{
			$newid = $this->db->insert_id();

			for( $i=0; $i<count($this->langs); $i++ )
			{
				$query = "INSERT INTO ".TABLE_FAQ_LANGS." ( item_id, lang_id, title, content )
					VALUES ('$newid', '".$this->langs[$i]['id']."', '".addslashes($req['title'])."', '".addslashes($req['content'])."')";
				if( !$this->db->exec($query) )
				{
					return false;
				}
			}
		}
		else
			return false;
			
		return true;
	}
	
	public function save_faq($id, $req)
	{		
		//$db_datest = substr($req['datest'], 6, 4)."-".substr($req['datest'], 3, 2)."-".substr($req['datest'], 0, 2)." 01:00:00";
		
		$query = "UPDATE ".TABLE_FAQ." SET group_id='".$req['group_id']."', sort_num='".$req['sort_num']."', 
				filename='".addslashes($req['filename_src'])."' WHERE id='".$id."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		$query =  "UPDATE ".TABLE_FAQ_LANGS." SET title='".addslashes($req['title'])."', content='".addslashes($req['content'])."'
				WHERE item_id='".$id."' AND lang_id='".$this->LangId."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		return true;
	}
	
	public function delete_faq($id)
	{
		$query = "DELETE FROM ".TABLE_FAQ_LANGS." WHERE item_id='$id'";
		if( !$this->db->exec( $query ) )
			return false;
		else
		{
			$query = "DELETE FROM ".TABLE_FAQ." WHERE id='$id'";
			if( !$this->db->exec( $query ) )
				return false;
		}
		
		return true;
	}
}
?>