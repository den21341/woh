<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class SeoModel extends Model
{
	protected $LangId;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;		
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function get_seoList()
	{
		$query = "SELECT * FROM ".TABLE_SEO_TITLES." WHERE lang_id='".$this->LangId."' ORDER BY url";
		$res = $this->db->query( $query );
		
		return $res;
	}
	
	public function get_Seo($id)
	{
		$query = "SELECT * FROM ".TABLE_SEO_TITLES." WHERE id='".$id."'";
		$res = $this->db->query( $query );
		
		if( count($res) > 0 )
		{
			$it = Array( 
				"id" => $res[0]['id'], 
				"url" => $res[0]['url'], 
				"h1" => $res[0]['page_h1'],
				"title" => $res[0]['page_title'],
				"keyw" => $res[0]['page_keywords'],
				"descr" => $res[0]['page_descr'],
				"text1" => $res[0]['content_text'],
				"text2" => $res[0]['content_words'] 
			);
			
			return $it;
		}
		
		return (count($res) > 0 ? $res[0] : $res);
	}
	
	private function _check_SeoExist($url, $id=0)
	{
		$query = "SELECT count(*) as totnum FROM ".TABLE_SEO_TITLES." WHERE url='".addslashes($url)."'".($id != 0 ? " AND id<>'".$id."'" : "");
		$res = $this->db->query( $query );
		
		if( count($res)>0 )
			return $res[0]['totnum'];
		
		return 0;
	}
	
	private function _add_Seo($req)
	{		
		$query = "INSERT INTO ".TABLE_SEO_TITLES." ( lang_id, add_date, modify_date, url, page_h1, page_title, page_keywords, page_descr, content_text, content_words )
    		VALUES ('".$this->LangId."', NOW(), NOW(), '".addslashes($req['url'])."', '".addslashes($req['h1'])."', '".addslashes($req['title'])."', 
    			'".addslashes($req['keyw'])."', '".addslashes($req['descr'])."', '".addslashes($req['text1'])."', '".addslashes($req['text2'])."')";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
	
	public function add_Seo($req)
	{
		$exist = $this->_check_SeoExist($req['url']);
		if( $exist > 0 )
			return false;
		
		return $this->_add_Seo($req);
	}
	
	private function _save_Seo($id, $req)
	{
		$query = "UPDATE ".TABLE_SEO_TITLES."
			SET url='".addslashes($req['url'])."', page_h1='".addslashes($req['h1'])."', page_title='".addslashes($req['title'])."', page_keywords='".addslashes($req['keyw'])."', 
			page_descr='".addslashes($req['descr'])."', content_text='".addslashes($req['text1'])."', content_words='".addslashes($req['text2'])."', 
			modify_date=NOW() 
			WHERE id='".$id."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		return true;
	}
	
	public function save_Seo($id, $req)
	{
		$exist = $this->_check_SeoExist($req['url'], $id);
		if( $exist > 0 )
			return false;
		
		return $this->_save_Seo($id, $req);
	}
	
	public function delete_Seo($id)
	{
		$query = "DELETE FROM ".TABLE_SEO_TITLES." WHERE id='$id'";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
}
?>