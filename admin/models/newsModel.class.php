<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class NewsModel extends Model
{	
	public $news; 
	public $ses;
	public $langs;
	protected $LangId;
	
	function __construct($config, $db, $LangId, $langs)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->langs = $langs;
				
		$this->ses = UhCmsAdminApp::getSesInstance();		
		$this->news = new UhCmsNews($this->db, $LangId);
	}
	
	public function get_newsList($group=-1)
	{
		//$query = "SELECT * FROM ".TABLE_SLIDES." WHERE lang_id='".$this->LangId."' ORDER BY sort_num";
		//$res = $this->db->query( $query );
		
		$res = $this->news->News_Items($group);
				
		return $res;
	}
	
	public function get_newsInfo($id)
	{
		//$query = "SELECT * FROM ".TABLE_SLIDES." WHERE id='".$id."'";
		//$res = $this->db->query( $query );		
		//return (count($res) > 0 ? $res[0] : $res);
		
		$res = $this->news->News_ItemInfo($id);
		
		return $res;
	}
	
	public function add_News($req, $img) {
		$query = "INSERT INTO ".TABLE_NEWS." ( sect_id, dtime, ngroup, first_page)
			VALUES ( '".$req['sect_id']."', NOW(), '".$req['ngroup']."', '".$req['first_page']."')";
		echo $query.'<br>';

		if( $this->db->exec($query) ) {
			$newid = $this->db->insert_id();

			for( $i=0; $i<count($this->langs); $i++ ) {
				$query = "INSERT INTO ".TABLE_NEWS_LANGS." ( news_id, lang_id, title, content )
					VALUES ('$newid', '".$this->langs[$i]['id']."', '".addslashes($req['title'])."', '".addslashes($req['content'])."')";
				echo $query.'<br>';
				if( !$this->db->exec($query) ) {
					return false;
				}
			}
			if( isset($img['pfile1']) && ($img['pfile1']['name'] != "") && ($img['pfile1']['tmp_name'] != "") ) {
				$this->addNewsPhoto($img['pfile1'], $newid);
			}
		}
		else
			return false;
			
		return true;
	}
	
	public function save_News($id, $req)
	{		
		$db_datest = substr($req['datest'], 6, 4)."-".substr($req['datest'], 3, 2)."-".substr($req['datest'], 0, 2)." 01:00:00";
		
		$query = "UPDATE ".TABLE_NEWS." SET sect_id='".$req['sect_id']."', first_page='".$req['first_page']."', 
				filename_src='".addslashes($req['filename_src'])."', dtime=NOW() WHERE id='".$id."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		$query =  "UPDATE ".TABLE_NEWS_LANGS." SET title='".addslashes($req['title'])."', content='".addslashes($req['content'])."'
				WHERE news_id='".$id."' AND lang_id='".$this->LangId."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		return true;
	}
	
	public function delete_News($id)
	{
		$query = "DELETE FROM ".TABLE_NEWS_LANGS." WHERE news_id='$id'";
		if( !$this->db->exec( $query ) )
			return false;
		else
		{
			$query = "DELETE FROM ".TABLE_NEWS." WHERE id='$id'";
			if( !$this->db->exec( $query ) )
				return false;
		}
		
		return true;
	}

	public function addNewsPhoto($pfile, $id) {
		$newpname = $id."_".rand(10000,40000);
		$pext = ".jpg";
		$ppos = strrpos($pfile['name'], ".");

		if( $ppos !== FALSE )
			$pext = mb_substr($pfile['name'], $ppos);

		$src_path = '../'.NEWS_PHOTO_DIR.$newpname.mb_strtolower($pext);

		if( !move_uploaded_file($pfile['tmp_name'], $src_path) ) {
			return false;
		}

		$query = "UPDATE ".TABLE_NEWS." SET filename_src = '".addslashes($src_path)."' WHERE id = ".$id;
		echo 'Q1 = '.$query.'<br>';
		$this->db->exec($query);

		$newfname = $newpname.".jpg";

		if( UhCmsUtils::ResizeImage($src_path, '../'.NEWS_ICO_DIR.$newfname, ".jpg", NEWS_PHOTO_ICO_W, NEWS_PHOTO_ICO_H, true) ) {
			$query = "UPDATE ".TABLE_NEWS." SET filename_ico = '".addslashes('../'.NEWS_ICO_DIR.$newfname)."' WHERE id = ".$id;
			echo 'Q2 = '.$query.'<br>';
			$this->db->exec($query);
		}
	}
}
?>