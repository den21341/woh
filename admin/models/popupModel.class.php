<?php

class PopupModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId) {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function getPagePopup($page = 'proj/view/') {
        $query = "SELECT * FROM ".TABLE_PAGES_POPUP." WHERE page = '".addslashes($page)."'";
        return $this->db->query($query);
    }

    public function getCountPopup() {
        $res = '';
        $query = "SELECT DATE(data) data FROM ".TABLE_PAGES_POPUP_INFO." GROUP BY DATE(data) ORDER BY data DESC";
        $dats = $this->db->query($query);

        if(!empty($dats)) {
            foreach ($dats as $date) {
                $query = "SELECT count(popup_id) popco, DATE(data) data, popup_id FROM " . TABLE_PAGES_POPUP_INFO . " WHERE DATE(data) = '" . $date['data'] . "' GROUP BY popup_id ";
                $res[] = $this->db->query($query);
            }
        } return $res;
    }
}

