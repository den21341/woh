<?php

class AbtestModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId) {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function getABvalues() {
        $query = "SELECT * FROM ".TABLE_AB_TESTING." WHERE 1 ";
        return $this->db->query($query);
    }

    public function setABvalues($val) {
        $query = "INSERT INTO ".TABLE_AB_TESTING." (pays_val) VALUES (".addslashes($val).") ";
        return $this->db->exec($query);
    }

    public function getABTestList($type = 1) {
        $query = "SELECT count(*) as count, t1.pays_val, tl1.test_type FROM ".TABLE_AB_TESTING_LIST." tl1
                    JOIN ".TABLE_AB_TESTING." t1 ON t1.id= tl1.pays_val 
                    WHERE tl1.test_type = ".addslashes($type)."
                    GROUP BY t1.pays_val";
        $res = $this->db->query($query);

        foreach ($res as $k => $val) {
            $query = "SELECT count(*) as count_all FROM ".TABLE_AB_TESTING_LIST." tl1
                        JOIN ".TABLE_AB_TESTING." t1 ON t1.id = tl1.pays_val 
                        WHERE t1.pays_val = ".$val['pays_val']." AND tl1.is_pay = 1 AND tl1.test_type = ".$val['test_type'];
            $res[$k]['count_all'] = $this->db->query($query)[0]['count_all'];
        }
        return $res;
    }

    public function getABallByVal() {
        $query = "SELECT count(*) as count, tl1.pays_val FROM ".TABLE_AB_TESTING_LIST." tl1
                    JOIN ".TABLE_AB_TESTING." t1 ON t1.pays_val = tl1.pays_val 
                    GROUP BY t1.pays_val";
        return $this->db->query($query);
    }

    public function delABvalues($val) {
        $query = "DELETE FROM ".TABLE_AB_TESTING." WHERE pays_val = ".addslashes($val);
        return $this->db->exec($query);
    }

    public function getInfoSum() {
        $query = "SELECT i1.*, SUM(d1.inp_req_sum) all_sum FROM ".TABLE_CAT_ITEM_DIAGRAM." d1
                  JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = d1.item_id
                  WHERE 1 
                  GROUP BY d1.item_id";
        
        return $this->db->query($query);
    }

    public function getInfoSumByProj($pid) {
        $query = "SELECT d1.* FROM ".TABLE_CAT_ITEM_DIAGRAM." d1
         JOIN ".TABLE_CAT_ITEMS." i1 ON i1.id = d1.item_id
         WHERE d1.item_id = ".addslashes($pid);

        return $this->db->query($query);
    }

}

