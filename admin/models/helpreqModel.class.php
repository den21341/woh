<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class HelpReqModel extends Model
{
	protected $LangId;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function get_HelpReqList($catModel, $pi=-1, $pn=50)
	{
		$res = $catModel->Req_List(0, "", 0, false, -1, $pi, $pn);
		
		for( $i=0; $i<count($res); $i++ )
		{
			$res[$i]['account_group'] = $this->cfg['USR_TYPES'][$res[$i]['account_type']];
			//$res[$i]['rnum_all'] = $catModel->Item_ReqCollected($res[$i]['id']);
			//$res[$i]['rnum_conf'] = $catModel->Item_ReqCollected($res[$i]['id'], REQ_STATUS_CONFIRM);
		}
		
		return $res;
	}
	
	public function save_Req($rid, $proj)
	{
		$query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ_RATE." SET 
				show_first='".$proj['pfirst']."', 
				rate='".$proj['prate']."',				
				rate_comments='".addslashes($proj['pdescr0'])."'  
				WHERE id='".$rid."' ";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		return true;
	}
	
	public function set_ReqStatus($pid, $status)
	{	
		//$query = "UPDATE ".TABLE_CAT_ITEMS." SET status=".$status." WHERE id=$pid";
		//if( !$this->db->exec( $query ) )
		//{
		//	// error
		//	return false;
		//}
		//if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
		//{
		//	echo mysql_error();
		//}
		
		return true;
	}
	
	public function delete_Req($rid)
	{
		$query = "DELETE FROM ".TABLE_CAT_ITEMS_HELPREQ_RATE." WHERE id='$rid'";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
}
?>