<?php

class TestprojModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId) {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function getUserAns() {
        $query = "SELECT user_id FROM ".TABLE_PROJ_TEST_ANS." WHERE 1 GROUP BY user_id ";
        $_uid = $this->db->query($query);

        $ans = [];
        foreach ($_uid as $uid) {
            $query = "SELECT * FROM ".TABLE_PROJ_TEST_ANS." WHERE user_id = ".$uid['user_id'];
            $ans[] = $this->db->query($query);
        }
        return $ans;
    }

    public function getUserId() {
        $query = "SELECT user_id FROM ".TABLE_PROJ_TEST_ANS." WHERE 1 GROUP BY user_id ";
        return $this->db->query($query);
    }

    public function getUserAnsBy($from, $to) {
        $query = "SELECT user_id
                    FROM ".TABLE_PROJ_TEST_ANS." a2
                    WHERE a2.q_id = 1
                    AND a2.answer 
                    BETWEEN ".addslashes($from)." 
                    AND ".addslashes($to);

        return $this->db->query($query);
    }

    public function getUserAnsByOne($uid) {
        $query = "SELECT a1.*, p1.* FROM ".TABLE_PROJ_TEST_ANS." a1
                JOIN ".TABLE_PROJ_TEST." p1 ON p1.id = a1.q_id
                WHERE user_id = ".addslashes($uid);

        return $this->db->query($query);
    }

    public function getSumAllAns() {
        $query = "SELECT * FROM ".TABLE_PROJ_TEST." t1
        JOIN ".TABLE_PROJ_TEST_SUB." s1 ON s1.q_id = t1.id WHERE s1.qtype = 0 GROUP BY t1.id";
        $allAns = $this->db->query($query);

        $res = [];

        foreach ($allAns as $ans) {
            $query = "SELECT count(a1.answer) as asum, a1.answer, t1.qname  FROM ".TABLE_PROJ_TEST_ANS." a1
                        JOIN ".TABLE_PROJ_TEST." t1 ON t1.id = a1.q_id
                        WHERE a1.`q_id` = ".$ans['q_id']." GROUP BY a1.`answer`";
            $res[] = $this->db->query($query);
        }
        /*echo '<pre>';
        print_r($res); die();*/
        return $res;
    }

}

