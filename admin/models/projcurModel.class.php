<?php

class ProjcurModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId) {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function getProjCurrencyList($pstat = PROJ_STATUS_RUN, $pamount = PROJ_TYPE_MONEY, $l1=0, $l2=20) {
        $query = "SELECT c1 . * , b1.fb_uid, b1.od_uid, b1.vk_uid, i1.id proj_id, i1.amount, i1.amount_done, i1.amount_donepercent, i1.amount_type, il1.descr, i1.item_rate
                    FROM ".TABLE_LOCAL_CURRENCY." c1
                    JOIN ".TABLE_CAT_ITEMS." i1 ON i1.author_id = c1.user_id
                    JOIN ".TABLE_CAT_ITEMS_LANGS." il1 ON i1.id = il1.item_id
                    JOIN ".TABLE_SHOP_BUYERS." b1 ON b1.id = i1.author_id
                    WHERE i1.status = ".$pstat." AND i1.amount_type = ".$pamount." GROUP BY i1.id
                    ORDER BY i1.item_rate DESC LIMIT ".$l1.", ".$l2;
        return $this->db->query($query);
    }

}