<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class SectsModel extends Model
{
	//protected $user;
	public $LangId;
	public $ses;
	public $langs;
	
	function __construct($config, $db, $LangId, $langs)
	{
		parent::__construct($config, $db);
		
		//$this->user = new User($db);
		$this->LangId = $LangId;
		$this->langs = $langs; 
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
		
	public function get_SectInfo($id)
	{
		//return $this->user->userInfo($uid);
		
		$reqd = Array();
		
		$query = "SELECT s1.*, s2.*
				FROM ".TABLE_CAT_CATALOG." s1, ".TABLE_CAT_CATALOG_LANGS." s2
				WHERE s1.id='$id' AND s1.id=s2.sect_id AND s2.lang_id='".$this->LangId."'";
		$res = $this->db->query( $query );
		if( count($res) > 0 )
		{
			$row = $res[0];
			
			$reqd['id']	= $row['id'];
			$reqd['smode']	= $row['mode'];
			$reqd['orgsect']	= $row['parent_id'];
			$reqd['orgname']	= stripslashes($row['name']);
			$reqd['orgdescr']	= stripslashes($row['descr']);
			$reqd['orgdescr0']	= stripslashes($row['descr0']);
			$reqd['orgurl']		= stripslashes($row['url']);
			//$page_title = stripslashes($row->page_title);
			//$page_key = stripslashes($row->page_keywords);
			//$page_descr = stripslashes($row->page_descr);
			$reqd['orgsort']	= $row['sort_num'];
			$reqd['myfile']		= stripslashes($row['filename']);
			$reqd['myfile2']	= stripslashes($row['filename_thumb']);
			$reqd['myfile3']	= stripslashes($row['filename_thumb_sel']);
			$reqd['orglayout']	= $row['product_layout'];
			//$reqd['orgslayout']	= $row['section_layout'];
			$reqd['orgvis']		= $row['visible'];
			$reqd['orgfirst']	= $row['show_first'];
			$reqd['urltype']	= $row['is_link'];
			$reqd['oldurl']		= stripslashes($row['url_old']);
	
			$reqd['bannerfile']	= stripslashes($row['banner_file']);
			$reqd['bannerlink']	= stripslashes($row['banner_link']);		
		}
		
		return $reqd;
	}
	
	public function add_Sect($reqd)
	{				
		$query = "INSERT INTO ".TABLE_CAT_CATALOG." ( mode, parent_id, sort_num, visible, filename, product_layout, url, show_first )
		VALUES ('".$reqd['smode']."', '".$reqd['orgsect']."', '".$reqd['orgsort']."', '".$reqd['orgvis']."', '".addslashes($reqd['myfile'])."', '0', 
				'".$reqd['orgurl']."', '".$reqd['orgfirst']."')";
		if( $this->db->exec( $query ) )
		{
			$newsectid = $this->db->insert_id();
		
			for( $i=0; $i<count($this->langs); $i++ )
			{
				if( !$this->db->exec( "INSERT INTO ".TABLE_CAT_CATALOG_LANGS." ( sect_id, lang_id, name, descr, descr0 ) VALUES
						('$newsectid', '".$this->langs[$i]['id']."', '".addslashes($reqd['orgname'])."','".addslashes($reqd['orgdescr'])."', '".addslashes($reqd['orgdescr0'])."')" ) )
				{
					//echo mysql_error();
					return false;
				}
			}
		}
		else
		{
			return false;
		}		
		
		return true;
	}
	
	public function save_Sect($id, $reqd)
	{
		$urlisused = false;
		if( $reqd['orgurl'] != "" )
		{
			$query = "SELECT * FROM ".TABLE_CAT_CATALOG." WHERE url='".addslashes($reqd['orgurl'])."' AND id<>'".$id."' AND is_link=0";
			$res = $this->db->query($query);
			if( count($res)>0 )
			{
				$urlisused = true;
			}			
		}
		
		if( $urlisused )
		{
			$msg = "URL имя страницы, которое вы указали уже используется другим разделом. Нужно указать уникальное название.";
			$mode = "edit";
			return false;
		}
		else if( $reqd['orgsect'] == $id )
		{
			$msg = "Вы не можете в качестве родительского каталога выбрать сам этот же каталог, который редактируете.";
			$mode = "edit";
			return false;
		}
		else
		{
			$query = "UPDATE ".TABLE_CAT_CATALOG." SET parent_id='".$reqd['orgsect']."', sort_num='".$reqd['orgsort']."', 
				visible='".$reqd['orgvis']."', url='".addslashes($reqd['orgurl'])."', show_first='".$reqd['orgfirst']."', is_link='".$reqd['urltype']."',
				filename='".addslashes($reqd['myfile'])."', filename_thumb='".addslashes($reqd['myfile2'])."',  filename_thumb_sel='".addslashes($reqd['myfile3'])."',  
				banner_file='".addslashes($reqd['bannerfile'])."', banner_link='".addslashes($reqd['bannerlink'])."',
				url_old='".addslashes($reqd['oldurl'])."'
				WHERE id='".$id."'";
			
			if( $this->db->exec( $query ) )
			{
				$query = "UPDATE ".TABLE_CAT_CATALOG_LANGS." SET name='".addslashes($reqd['orgname'])."', descr='".addslashes($reqd['orgdescr'])."', 
						descr0='".addslashes($reqd['orgdescr0'])."' WHERE sect_id='".$id."' AND lang_id='".$this->LangId."'";
				$this->db->exec( $query );
			}
			else
				return false;
		}					
		
		return true;
	}
	
	public function set_Status($uid, $active)
	{	
		$query = "UPDATE ".TABLE_USERS." SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid";
		if( !$this->db->exec( $query ) )
		{
			// error
			return false;
		}
		//if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
		//{
		//	echo mysql_error();
		//}
		
		return true;
	}
	
	public function delete_Sect($id)
	{
		$query = "DELETE FROM ".TABLE_CAT_CATALOG_LANGS." WHERE sect_id='$id'";
		if( !$this->db->exec( $query ) )		
			return false;		
		
		$query = "DELETE FROM ".TABLE_CAT_CATALOG." WHERE id='$id'";
		if( !$this->db->exec( $query ) )
			return false;
		
		$this->_delete_SectLinked($id);

		$this->_delete_SectSub($id);
		
		return true;
	}
	
	private function _delete_SectSub($id)
	{
		$query = "SELECT id FROM ".TABLE_CAT_CATALOG." WHERE parent_id='".$id."'";
		$its = $this->db->query( $query );
		for( $i=0; $i<count($its); $i++ )
		{
			$query = "DELETE FROM ".TABLE_CAT_CATALOG_LANGS." WHERE sect_id='".$its[$i]['id']."'";
			if( $this->db->exec( $query ) )				
			{							
				$query = "DELETE FROM ".TABLE_CAT_CATALOG." WHERE id='".$its[$i]['id']."'";
				$this->db->exec( $query );
			}
			
			$this->_delete_SectSub($its[$i]['id']);
			
			$this->_delete_SectLinked($its[$i]['id']);
		}
	}	
	
	private function _delete_SectLinked($id)
	{						
		$query = "DELETE FROM ".TABLE_CAT_CATITEMS." WHERE sect_id='$id'";
		if( !$this->db->exec( $query ) )
			return false;
	}
}
?>