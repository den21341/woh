<?php
class BonusProj extends AdmPageController{

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->bonusModel = new BonusProjModel($this->getCfg(), $db, 1);
        $this->pageView = new BonusProjView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->catLib = $this->model;
        $this->pageView->bonlist = $this->bonusModel->get_BonusList();

        $this->pageView->render_main();
    }

    public function action_status() {
        $pid = $this->getReqParam("pid", 0);
        $active = $this->getReqParam("archive", 0);

        $this->bonusModel->set_BonStatus($pid, $active);

        $this->pageView->bonlist = $this->bonusModel->get_BonusList();

        $this->pageView->render_main();
    }

    public function action_editproj() {
        $pid = $this->getReqParam("pid", 0);
        $this->pageView->boninfo = $this->bonusModel->bonusInfo($pid);
        $this->pageView->bonlist = $this->bonusModel->get_BonusList();

        $this->pageView->render_editform();
    }

    public function action_deleteproj() {
        /*$pid = $this->getReqParam("pid", 0);
        $this->projModel->delete_Proj($pid);

        $this->pageView->projlist = $this->projModel->get_ProjList($this->model);

        $this->pageView->render_main();*/
    }

    public function action_saveproj()
    {
        $bon = [];

        $bon['id'] 		= $this->getReqParam("pid", 0);
        $bon['ptitle']		= $this->getReqParam("ptitle", "");
        $bon['pdescr0']	= $this->getReqParam("pdescr0", "");
        $bon['pdescr']		= $this->getReqParam("pdescr", "");
        $bon['pamount']	= $this->getReqParam("pamount", 0);
        $bon['pam_used']	= $this->getReqParam("pam_used", 0);
        $bon['pstart']	= $this->getReqParam("pstart", 0);
        $bon['pend']	= $this->getReqParam("pend", 0);

        $this->bonusModel->bonusSave($bon['id'], $bon);

        $this->pageView->bonlist = $this->bonusModel->get_BonusList();

        $this->pageView->render_main();
    }

    /*public function action_delete()
    {
        $id = $this->getReqParam("winid", 0);
        $this->bonusModel->delete_Win($id);

        //$this->pageView->reqlist = $this->reqModel->get_HelpReqList($this->model);
        $this->pageView->catLib = $this->model;
        $this->pageView->winlist = $this->bonusModel->get_BonusWinList();

        $this->pageView->render_main();
    }*/
}