<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class PreferencesModel extends Model
{
	public $ses;
	protected $LangId;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->ses = UhCmsAdminApp::getSesInstance();		
		$this->LangId = $LangId;
	}
	
	public function get_PrefList()
	{
		$arr_val = Array();
		
		$query = "SELECT * FROM ".TABLE_PREFERENCES." ORDER BY id";
		
		$res = $this->db->query( $query );
		
		return $res;		
	}	
	
	public function save_Pref($id, $val)
	{
		$query = "UPDATE ".TABLE_PREFERENCES." SET value='".addslashes($val)."' WHERE id='".$id."' ";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		return true;
	}	
}
?>