<?php

class BonusProjModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId)
    {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function get_BonusList($sortby="")
    {
        //$sort_cond = " q1.add_date DESC ";
        /*switch( $sortby )
        {
            case "quiz":
                $sort_cond = " b2.title2, q1.add_date DESC ";
                break;

            case "user":
                $sort_cond = " u1.fname, u1.name, q1.add_date DESC ";
                break;
        }*/

        $query = "SELECT bon.id, buy.name, buy.fname, buy.email, bon.status, bon.archive, bon.amount, bon.amount_used, bon.amount_val, bon.title, bon.start_date, bon.end_date
                  FROM ".TABLE_CAT_BONUS." as bon
                  INNER JOIN ".TABLE_SHOP_BUYERS." as buy ON bon.author_id = buy.id";

        $res = $this->db->query($query);

        return $res;
    }

    public function set_BonStatus($pid, $status) {
        $query = "UPDATE ".TABLE_CAT_BONUS." SET archive = ".$status." WHERE id = $pid";
        if( !$this->db->exec( $query ) )
        {
            return false;
        }
        return true;
    }

    public function bonusInfo($id) {
        if(is_numeric($id)) {
            $query = "SELECT bon.id, buy.name, buy.fname, buy.email, bon.status, bon.archive, bon.amount, bon.amount_used, bon.amount_val, bon.title, des.descr, des.descr0, bon.start_date, bon.end_date
                  FROM ".TABLE_CAT_BONUS." as bon
                  INNER JOIN ".TABLE_SHOP_BUYERS." as buy ON bon.author_id = buy.id
                  INNER JOIN ".TABLE_CAT_BONUS_LANGS." as des ON des.item_id = bon.id
                  WHERE bon.id = {$id}";

            $res = $this->db->query($query);

            return $res;
        }
        else
            return false;
    }

    public function bonusSave($id, $bonus) {
        if(is_numeric($id)) {
            if(is_array($bonus)) {
                $query = "UPDATE ".TABLE_CAT_BONUS."
                          SET `amount` = ".$bonus['pamount'].",
                              `amount_used` = ".$bonus['pam_used'].",
                              `title` = '".$bonus['ptitle']."',
                              `start_date` = '".$bonus['pstart']."',
                              `end_date` = '".$bonus['pend']."'
	                      WHERE id = ".$id;

                $query1 = "UPDATE ".TABLE_CAT_BONUS_LANGS."
                            SET `descr` = '".$bonus['pdescr']."',
	                            `descr0` = '".$bonus['pdescr0']."'
                            WHERE `item_id` = ".$id;

                if( !$this->db->exec($query) )
                {
                    return false;
                }
                if( !$this->db->exec($query1) )
                {
                    return false;
                }
                return true;
            }
        }
    }

    /*
    public function save_Req($rid, $proj)
    {
        $query = "UPDATE ".TABLE_CAT_ITEMS_HELPREQ_RATE." SET
                show_first='".$proj['pfirst']."',
                rate='".$proj['prate']."',
                rate_comments='".addslashes($proj['pdescr0'])."'
                WHERE id='".$rid."' ";
        if( !$this->db->exec( $query ) )
        {
            //
            return false;
        }

        return true;
    }
    */

    public function delete_Win($winid)
    {
        $query = "SELECT q1.*, w1.id as winid, w1.point_ind
			FROM ".TABLE_CAT_BONUS_QUIZ." q1
			INNER JOIN ".TABLE_CAT_BONUS_WINS." w1 ON q1.id=w1.quiz_id
			WHERE w1.id='".$winid."'";
        $res = $this->db->query($query);

        $query = "UPDATE ".TABLE_CAT_BONUS_QUIZ." SET status='".BONUS_QUIZ_STATUS_USED."' WHERE id='".$res[0]['id']."'";
        if( !$this->db->exec( $query ) )
            return false;

        $query = "DELETE FROM ".TABLE_CAT_BONUS_WINS." WHERE id='$winid'";
        if( !$this->db->exec( $query ) )
            return false;

        return true;
    }
}