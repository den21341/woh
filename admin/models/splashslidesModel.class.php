<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class SplashslidesModel extends Model
{
	protected $LangId;
	public $ses;
	
	function __construct($config, $db, $LangId)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;		
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function get_slideList()
	{
		$query = "SELECT * FROM ".TABLE_SLIDES." WHERE lang_id='".$this->LangId."' ORDER BY sort_num";
		$res = $this->db->query( $query );
		
		return $res;
	}
	
	public function get_slideInfo($id)
	{
		$query = "SELECT * FROM ".TABLE_SLIDES." WHERE id='".$id."'";
		$res = $this->db->query( $query );
		
		return (count($res) > 0 ? $res[0] : $res);
	}
	
	public function add_Slide($req)
	{		
		$query = "INSERT INTO ".TABLE_SLIDES." ( lang_id, sort_num, filename, title, url, comment )
    		VALUES ('".$this->LangId."', '".$req['sort_num']."', '".addslashes($req['filename'])."', '".addslashes($req['title'])."', '".addslashes($req['url'])."', '".addslashes($req['comment'])."')";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
	
	public function save_Slide($id, $req)
	{
		$query = "UPDATE ".TABLE_SLIDES."
			SET sort_num='".$req['sort_num']."', filename='".addslashes($req['filename'])."', title='".addslashes($req['title'])."',
			comment='".addslashes($req['comment'])."', url='".addslashes($req['url'])."'
			WHERE id='".$id."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		return true;
	}
	
	public function delete_Slide($id)
	{
		$query = "DELETE FROM ".TABLE_SLIDES." WHERE id='$id'";
		if( !$this->db->exec( $query ) )
			return false;
		
		return true;
	}
}
?>