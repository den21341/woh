<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CurrencyModel extends Model
{
	protected $LangId;
	public $langs;
	public $ses;
	
	function __construct($config, $db, $LangId, $langs)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->langs = $langs;
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function get_currencyList()
	{
		$query = "SELECT c1.*, c2.name FROM ".TABLE_CAT_CURRENCY." c1 
			INNER JOIN ".TABLE_CAT_CURRENCY_LANGS." c2 ON c1.id=c2.item_id AND c2.lang_id='".$this->LangId."' 
			ORDER BY c1.sort_num, c2.name";
		$res = $this->db->query( $query );
		
		return $res;
	}
	
	public function get_currencyInfo($id)
	{
		$query = "SELECT c1.*, c2.name 
			FROM ".TABLE_CAT_CURRENCY." c1 
			INNER JOIN ".TABLE_CAT_CURRENCY_LANGS." c2 ON c1.id=c2.item_id AND c2.lang_id='".$this->LangId."'  
			WHERE c1.id='".$id."'";
		$res = $this->db->query( $query );
		
		return (count($res) > 0 ? $res[0] : $res);
	}
	
	public function add_Currency($req)
	{		
		$query = "INSERT INTO ".TABLE_CAT_CURRENCY." (sort_num, cur_symb, cur_code) VALUES ('".$req['sort_num']."', '', '".addslashes($req['cur_code'])."')";		
		if( $this->db->exec( $query ) )
		{
			$newid = $this->db->insert_id();
			
			for( $i=0; $i<count($this->langs); $i++ )
			{
				$query1 = "INSERT INTO ".TABLE_CAT_CURRENCY_LANGS." ( item_id, lang_id, name ) VALUES
					('$newid', '".$this->langs[$i]['id']."', '".addslashes($req['name'])."')"; 
				if( !$this->db->exec( $query1 ) )
				{
					//echo mysql_error();
					return false;
				}
			}
		}	
		else 
			return false;
		
		return true;
	}
	
	public function save_Currency($id, $req)
	{
		$query = "UPDATE ".TABLE_CAT_CURRENCY." SET sort_num='".$req['sort_num']."', cur_code='".addslashes($req['cur_code'])."' WHERE id='".$id."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		$query = "UPDATE ".TABLE_CAT_CURRENCY_LANGS." SET name='".addslashes($req['name'])."' WHERE item_id='".$id."' AND lang_id='".$this->LangId."'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		
		return true;
	}
	
	public function delete_Currency($id)
	{
		$query = "DELETE FROM ".TABLE_CAT_CURRENCY." WHERE id='$id'";
		if( !$this->db->exec( $query ) )
		{
			return false;
		}
		else
		{
			$query = "DELETE FROM ".TABLE_CAT_CURRENCY_LANGS." WHERE item_id='$id'";
			if( !$this->db->exec( $query ) )
				return false;
		}
		
		return true;
	}
}
?>