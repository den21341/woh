<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class PagesModel extends Model
{
	protected $page;
	protected $langs;
	protected $LangId;
	public $ses;	
	
	function __construct($config, $db, $LangId, $langs)
	{
		parent::__construct($config, $db);
		
		$this->LangId = $LangId;
		$this->langs = $langs;
		//$this->page = new SitePage($db, $LangId);
		$this->page = new PageModel($config, $db, $LangId);
		$this->ses = UhCmsAdminApp::getSesInstance();		
	}
	
	public function pageLib()
	{
		return $this->page->get_page();
	}
	
	public function pageBasic()
	{
		return $this->page;
	}
	
	public function get_pageLevel($pageid)
	{
		//return $this->page->Page_Subpages($pageid);
		return $this->page->get_page()->Page_Subpages($pageid);
	}
	
		
	public function get_PageInfo($id)
	{
		//return $this->page->Page_GetInfoById($id);
		return $this->page->get_page()->Page_GetInfoById($id);
	}
	
	////////////////////////////////////////////////////////////////////////
	// Page admin action handlers	
	public function add_Page($req)
	{		
		$query = "INSERT INTO ".TABLE_PAGES." (parent_id, page_name, create_date, modify_date, show_in_menu, page_record_type, sort_num) 
			VALUES ( '".$req['parent_id']."', '".addslashes($req['pfile'])."', NOW(), NOW(), ".$req['show_in_menu'].", ".$req['page_record_type'].", ".$req['sort_num']." )";
		if( $this->db->exec($query) )
		{
			$newpageid = $this->db->insert_id();
								
			for($i=0; $i<count($this->langs); $i++)
			{
				$query = "INSERT INTO ".TABLE_PAGES_LANGS." (item_id, lang_id, page_mean, page_title, page_keywords, page_descr, content, title, header) 
					VALUES ($newpageid, ".$this->langs[$i]['id'].", '".addslashes($req['pmean'])."',
					'".addslashes($req['seo_title'])."', '".addslashes($req['seo_keyw'])."', '".addslashes($req['seo_descr'])."',
					'".addslashes($req['content'])."', '".addslashes($req['title'])."', '".addslashes($req['header'])."' )";
				if( !$this->db->exec( $query ) )
				{
					return false;
				}
			}
		}
		else
			return false;		
	}
	
	public function save_Page($id, $req)
	{
		$query = "UPDATE ".TABLE_PAGES." SET
				parent_id='".$req['parent_id']."',
				show_in_menu='".$req['show_in_menu']."',
				with_editor='".$req['with_editor']."',
				page_record_type='".$req['page_record_type']."',
				page_name='".addslashes($req['pfile'])."',
				page_ico='".addslashes($req['page_ico'])."',
				modify_date=NOW(),
				sort_num='".$req['sort_num']."'
				WHERE id='".$id."'";
		if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}
		
		$query = "UPDATE ".TABLE_PAGES_LANGS." SET
        	page_mean='".addslashes($req['pmean'])."',
        	page_title='".addslashes($req['seo_title'])."',
			page_keywords='".addslashes($req['seo_keyw'])."',
			page_descr='".addslashes($req['seo_descr'])."',
			content='".addslashes($req['content'])."',
			title='".addslashes($req['title'])."',
			header='".addslashes($req['header'])."'
        	WHERE item_id='$id' AND lang_id='".$this->LangId."'";
       if( !$this->db->exec( $query ) )
		{
			//
			return false;
		}		
		
		return true;
	}
	
	public function delete_Page($id)
	{	
		$this->delete_PageRecurs($id);
		
		return true;
	}
	
	private function delete_PageRecurs($id)
	{
		$query = "SELECT * FROM ".TABLE_PAGES." WHERE parent_id='$id'";
		$its = $this->db->query($query);
		for($i=0; $i<count($its); $i++)
		{
			$this->delete_PageRecurs($its[$i]['id']);			
		}		
		$this->delete_PageData($id);
	}
	
	private function delete_PageData($id)
	{
		$was_errors = false;
		
		// Remove pages photos 
		$query = "SELECT * FROM ".TABLE_PAGE_PHOTO." WHERE item_id='$id'";
		$pics = $this->db->query( $query );
		for($i=0; $i<count($pics); $i++)
		{
			if( $pics[$i]['filename'] != "" )
				unlink( "../".$pics[$i]['filename'] );
			if( $pics[$i]['filename_thumb'] != "" )
				unlink( "../".$pics[$i]['filename_thumb'] );
			if( $pics[$i]['filename_ico'] != "" )
				unlink( "../".$pics[$i]['filename_ico'] );
			
			$query = "DELETE FROM ".TABLE_PAGE_PHOTO_LANGS." WHERE item_id='".$pics[$i]['id']."'";
			if( !$this->db->exec( $query ) )
				$was_errors = true;
		}
		
		$query = "DELETE FROM ".TABLE_PAGE_PHOTO." WHERE item_id='$id'";
		if( !$this->db->exec( $query ) )
			$was_errors = true;
		
		// Remove page videos
		$query = "SELECT * FROM ".TABLE_PAGE_VIDEO." WHERE item_id='$id'";
		$its = $this->db->query( $query );
		for($i=0; $i<count($its); $i++)
		{
			$query = "DELETE FROM ".TABLE_PAGE_VIDEO_LANGS." WHERE item_id='".$its[$i]['id']."'";
			if( !$this->db->exec( $query ) )
				$was_errors = true;
		}
				
		$query = "DELETE FROM ".TABLE_PAGE_VIDEO." WHERE item_id='$id'";
		if( !$this->db->exec( $query ) )
			$was_errors = true;
		
		// Remove page files
		$query = "SELECT * FROM ".TABLE_PAGE_FILES." WHERE item_id='$id'";
		$its = $this->db->query( $query );
		for($i=0; $i<count($its); $i++)
		{
			$query = "DELETE FROM ".TABLE_PAGE_FILES_LANGS." WHERE item_id='".$its[$i]['id']."'";
			if( !$this->db->exec( $query ) )
				$was_errors = true;
		}
				
		$query = "DELETE FROM ".TABLE_PAGE_FILES." WHERE item_id='$id'";
		if( !$this->db->exec( $query ) )
			$was_errors = true;
		
		// Remove text blocks assigned to the page
		$query = "DELETE FROM ".TABLE_PAGE_RESOURCES." WHERE page_id='$id'";
		if( !$this->db->exec( $query ) )
			$was_errors = true;
		
		// Remove page itself
		$query = "DELETE FROM ".TABLE_PAGES_LANGS." WHERE item_id='$id'";
		if( !$this->db->exec( $query ) )
			$was_errors = true;
		else
		{
			$query = "DELETE FROM ".TABLE_PAGES." WHERE id='$id'";
			if( !$this->db->exec( $query ) )
				$was_errors = true;
		}
		
		return $was_errors;
	}
	
	//////////////////////////////////////////////////////////////////////////////////
	//
	//		Page attached data management
	//
	//////////////////////////////////////////////////////////////////////////////////
	
	public function resource_Del($resid)
	{
		//$id = GetParameter("id", 0);
		//$resid = GetParameter("resid", 0);
		
		$query = "DELETE FROM ".TABLE_PAGE_RESOURCES." WHERE id='$resid'";
		
		return $this->db->exec($query);
	}

	public function resource_Add($id, $reqd)
	{	
		// Check if it already added
		$query = "SELECT * FROM ".TABLE_PAGE_RESOURCES." WHERE page_id='$id' AND item_id='".$reqd['resblock']."'";
		$its = $this->db->query($query);
		if( count($its) > 0 )
			return false;

		// Add resource to page
		$query = "INSERT INTO ".TABLE_PAGE_RESOURCES." (page_id, item_id, display_type, sort_num, add_date) VALUES
			('$id', '".$reqd['resblock']."', '".$reqd['resdisp']."', '".$reqd['resind']."', NOW())";
		
		return $this->db->exec($query);
	}
	
	public function photo_Minus($photoid)
	{
		//$id = GetParameter("id", 0);
		//$photoid = GetParameter("photoid", 0);
		
		$query = "UPDATE ".TABLE_PAGE_PHOTO." SET sort_num=sort_num-1 WHERE id='$photoid'";
		return $this->db->exec($query);
	}
	
	public function photo_Plus($photoid)
	{
		//$id = GetParameter("id", 0);
		//$photoid = GetParameter("photoid", 0);
		
		$query = "UPDATE ".TABLE_PAGE_PHOTO." SET sort_num=sort_num+1 WHERE id='$photoid'";
		return $this->db->exec($query);
	}
	
	public function photo_Del($photoid)
	{
		//$id = GetParameter("id", 0);
		//$photoid = GetParameter("photoid", 0);
		
		// Clean all files for this page record photo
		$query = "SELECT * FROM ".TABLE_PAGE_PHOTO." WHERE id='$photoid'";		
		$its = $this->db->query($query);
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			// Check if files exists and remove them
			if( file_exists("../".stripslashes($row['filename'])) )
				unlink("../".stripslashes($row['filename']));
			
			if( file_exists("../".stripslashes($row['filename_thumb'])) )
				unlink("../".stripslashes($row['filename_thumb']));
			
			if( file_exists("../".stripslashes($row['filename_ico'])) )
				unlink("../".stripslashes($row['filename_ico']));
			
		}
		
		// Delete record from database about file
		$query = "DELETE FROM ".TABLE_PAGE_PHOTO." WHERE id='$photoid'";
		if( $this->db->exec($query) )
		{
			$query1 = "DELETE FROM ".TABLE_PAGE_PHOTO_LANGS." WHERE item_id='$photoid'";
			$this->db->exec($query);			
		}
	}
	
	public function photo_Save($photoid, $reqd)
	{
		//$id = GetParameter("id", 0);
		//$photoid = GetParameter("photoid", 0);
		//
		//$phototitle = GetParameter("phototitle", "");
		//$photoind = GetParameter("photoind", 0);
		//$photodescr = GetParameter("photodescr", "");
		
		$query = "UPDATE ".TABLE_PAGE_PHOTO." SET sort_num='".$reqd['photoind']."' WHERE id=$photoid";
		if( !$this->db->exec($query) )
			return false;
				
		$query = "UPDATE ".TABLE_PAGE_PHOTO_LANGS." SET title='".addslashes($reqd['phototitle'])."', descr='".addslashes($reqd['photodescr'])."'
			WHERE item_id=$photoid AND lang_id='".$this->LangId."'";
		if( !$this->db->exec($query) )
			return false;
		
		return true;
	}
	
	public function photo_Add($id, $photofile, $reqd)
	{
		//$id = GetParameter("id", 0);
		
		//$phototitle = GetParameter("phototitle", "");
		//$photoind = GetParameter("photoind", 0);
		//$photodescr = GetParameter("photodescr", "");
		//$photofile = $_FILES['photofile'];
		
		//if( empty($photofile['name']) || ($photofile['name'] == "") )
		//	break;
		
		$point_pos = strrpos($photofile['name'], ".");
		if( $point_pos == -1 )
			break;
		$newfileext = substr($photofile['name'], $point_pos );
		
		$query = "INSERT INTO ".TABLE_PAGE_PHOTO." (item_id, sort_num, add_date) VALUES ('$id', '".$reqd['photoind']."', NOW())";
		if( !$this->db->exec($query) )
			return false;
			
		$picid = $this->db->insert_id();
		
		for( $i=0; $i<count($this->langs); $i++ )
		{
			$query = "INSERT INTO ".TABLE_PAGE_PHOTO_LANGS." ( item_id, lang_id, title, descr )
				VALUES ('$picid', '".$this->langs[$i]['id']."', '".addslashes($reqd['phototitle'])."', '".addslashes($reqd['photodescr'])."')";
			if( !$this->db->exec($query) )
				return false;
		}
		
		// Now we should make resized copies for all images
		$finalname = $picid.".jpg";
	
		$srcname = PAGEPIC_DIR.PAGEPIC_PHOTO.$id."_".$finalname;
		$thumbname = PAGEPIC_DIR.PAGEPIC_THUMB.$id."_".$finalname;
		$iconame = PAGEPIC_DIR.PAGEPIC_ICO.$id."_".$finalname;
	
		// Make big photo
		//$res = ResizeImage( $photofile['tmp_name'], "../".$srcname, strtolower($newfileext), ".jpg", $GALERY_P_W, $GALERY_P_H, false, $JPEG_COMPRESS_RATIO);
		$res = copy( $photofile['tmp_name'], "../".$srcname);		
		if( !$res )
		{
			echo "Произошла ошибка при создании большого пережатого изображения.<br />";
			return false;
		}
		
		$imgsz = getimagesize("../".$srcname);			
		//$imgsz = GetImageSizeAll( "../".$srcname, ".jpg" );

		$srcw = 1000;
		$srch = 1000;
		
		$thw = 0;
		$thh = 0;
		if( $imgsz !== false )
		{
			$srcw = $thw = $imgsz[0]; //$imgsz['w'];
			$srch = $thh = $imgsz[1];	//$imgsz['h'];
		}
	
		$query = "UPDATE ".TABLE_PAGE_PHOTO." SET filename='".addslashes($srcname)."', src_w=$thw, src_h=$thh WHERE id='$picid'";
		$this->db->exec($query);					
		
		// Make thumb photo
		if( ($srcw < PAGEPIC_T_W) || ($srch < PAGEPIC_T_H) )
		{
			$res = copy($photofile['tmp_name'], "../".$thumbname);
		}
		else
		{
			$res = UhCmsUtils::ResizeImage( $photofile['tmp_name'], "../".$thumbname, strtolower($newfileext), ".jpg", PAGEPIC_T_W, PAGEPIC_T_H, false, JPEG_COMPRESS_RATIO);
		}
		
		if( !$res )
		{
			echo "Произошла ошибка при создании среднего пережатого изображения.<br />";
			return false;
		}
		
		//$imgsz = GetImageSizeAll( "../".$thumbname, ".jpg" );
		$imgsz = getimagesize("../".$thumbname);
	
  		$thw = 0;
		$thh = 0;
		if( $imgsz !== false )
		{
			$thw = $imgsz[0]; //$imgsz['w'];
			$thh = $imgsz[1]; //$imgsz['h'];
		}
		
		$query = "UPDATE ".TABLE_PAGE_PHOTO." SET filename_thumb='".addslashes($thumbname)."', thumb_w=$thw, thumb_h=$thh WHERE id='$picid'";
		$this->db->exec($query);			
		
		
		// Make ico photo
		if( ($srcw < PAGEPIC_I_W) || ($srch < PAGEPIC_I_H) )
		{
			$res = copy($photofile['tmp_name'], "../".$iconame);
		}
		else
		{
			$res = UhCmsUtils::ResizeImage( $photofile['tmp_name'], "../".$iconame, strtolower($newfileext), ".jpg", PAGEPIC_I_W, PAGEPIC_I_H, true, JPEG_COMPRESS_RATIO);
		}
		if( !$res )
		{
			echo "Произошла ошибка при создании малого пережатого изображения.<br />";
			return false;
		}
		
		//$imgsz = GetImageSizeAll( "../".$iconame, ".jpg" );
		$imgsz = getimagesize("../".$iconame);
		
		$thw = 0;
		$thh = 0;
		if( $imgsz !== false )
		{
			$thw = $imgsz[0]; //$imgsz['w'];
			$thh = $imgsz[1];
		}
		
		$query = "UPDATE ".TABLE_PAGE_PHOTO." SET filename_ico='".addslashes($iconame)."', ico_w=$thw, ico_h=$thh WHERE id='$picid'";
		$this->db->exec($query);
		
		unlink($photofile['tmp_name']);
		
		return true;
	}
	
	
	public function video_Del($videoid)
	{				
		$query = "DELETE FROM ".TABLE_PAGE_VIDEO." WHERE id='$videoid'";
		if( !$this->db->exec($query) )
			return false;
		
		$query = "DELETE FROM ".TABLE_PAGE_VIDEO_LANGS." WHERE item_id='$videoid'";
		if( !$this->db->exec($query) )
			return false;
		
		return true;
	}
	
	public function video_Save($videoid, $reqd)
	{
		//$id = $item_id = GetParameter("id", 0);
		//$videoid = GetParameter("videoid", 0);
		//
		//$phototitle = GetParameter("phototitle", "");
		//$photoind = GetParameter("photoind", 0);
		//$photodescr = GetParameter("photodescr", "");
		//$photofile = GetParameter("photofile", "");
		//$photoyoutube = GetParameter("photoyoutube", "");
		
		$query = "UPDATE ".TABLE_PAGE_VIDEO." SET sort_num='".$reqd['photoind']."', filename='".addslashes($reqd['photofile'])."', 
			tube_code='".addslashes($reqd['photoyoutube'])."' WHERE id=$videoid";
		if( !$this->db->exec($query) )
			return false;
				
		$query = "UPDATE ".TABLE_PAGE_VIDEO_LANGS." SET title='".addslashes($reqd['phototitle'])."', descr='".addslashes($reqd['photodescr'])."'
			WHERE item_id=$videoid AND lang_id='".$this->LangId."'";
		if( !$this->db->exec($query) )
			return false;

		return true;
	}
	
	public function video_Add($id, $reqd)
	{
		//$id = GetParameter("id", 0);
		
		//$phototitle = GetParameter("phototitle", "");
		//$photoind = GetParameter("photoind", 0);
		//$photodescr = GetParameter("photodescr", "");
		//$photofile = GetParameter("photofile", "");
		//$photoyoutube = GetParameter("photoyoutube", "");
		
		//if( (trim($photofile) == "") && (trim($photoyoutube) == "") )
		//{
		//	return false;
		//}
		
		$query = "INSERT INTO ".TABLE_PAGE_VIDEO." (item_id, add_date, filename, tube_code, filename_ico, src_w, src_h, ico_w, ico_h, sort_num)
			VALUES ('$id', NOW(), '".addslashes($reqd['photofile'])."', '".addslashes($reqd['photoyoutube'])."', '', 0, 0, 0, 0, '".$reqd['photoind']."')";
		if( !$this->db->exec($query) )
			return false;
				
		$videoid = $this->db->insert_id();

		for( $i=0; $i<count($this->langs); $i++ )
		{
			$query = "INSERT INTO ".TABLE_PAGE_VIDEO_LANGS." (item_id, lang_id, title, descr) VALUES ('$videoid', '".$this->langs[$i]['id']."',
				'".addslashes($reqd['phototitle'])."', '".addslashes($reqd['photodescr'])."')";
			if( !$this->db->exec($query) )
				return false;
		}		
	} 
}
?>