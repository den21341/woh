<?php
define('BONUS_MONEY', 30 );
class BonModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId)
    {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }
  public function get_BonusList($sortby="")
    {
        //$sort_cond = " q1.add_date DESC ";
        /*switch( $sortby )
        {
            case "quiz":
                $sort_cond = " b2.title2, q1.add_date DESC ";
                break;

            case "user":
                $sort_cond = " u1.fname, u1.name, q1.add_date DESC ";
                break;
        }*/

        $query = "SELECT bon.id, buy.name, buy.fname, buy.email, bon.status, bon.archive, bon.amount, bon.amount_used, bon.amount_val, bon.title, bon.start_date, bon.end_date, bon.premoderation, bon.add_date
                  FROM ".TABLE_CAT_BONUS." as bon                  
                  INNER JOIN ".TABLE_SHOP_BUYERS." as buy ON bon.author_id = buy.id
				  ORDER BY bon.add_date DESC";

				  
        $res = $this->db->query($query);

        return $res;
    }

    public function set_BonStatus($pid, $status) {
        $query = "UPDATE ".TABLE_CAT_BONUS." SET archive = ".$status." WHERE id = $pid";
        if( !$this->db->exec( $query ) )
        {
            return false;
        }
        return true;
    }

    public function bonusInfo($id) {
        if(is_numeric($id)) {
            $query = "SELECT bon.id, buy.name, buy.fname, buy.email, bon.status, bon.archive, bon.amount, bon.amount_used, bon.amount_val, bon.title, des.descr, des.descr0, bon.start_date, bon.end_date, bon.premoderation
                  FROM ".TABLE_CAT_BONUS." as bon
                  INNER JOIN ".TABLE_SHOP_BUYERS." as buy ON bon.author_id = buy.id
                  INNER JOIN ".TABLE_CAT_BONUS_LANGS." as des ON des.item_id = bon.id
                  WHERE bon.id = {$id}";

            $res = $this->db->query($query);

            return $res;
        }
        else
            return false;
    }

    public function bonusSave($id, $bonus) {
        if(is_numeric($id)) {
            if(is_array($bonus)) {
                $query = "UPDATE ".TABLE_CAT_BONUS."
                          SET `amount` = ".$bonus['pamount'].",
                              `amount_used` = ".$bonus['pam_used'].",
                              `title` = '".$bonus['ptitle']."',
                              `start_date` = '".$bonus['pstart']."',
                              `end_date` = '".$bonus['pend']."',
	                          `premoderation` = '".$bonus['premoder']."'

						  WHERE id = ".$id;

                $query1 = "UPDATE ".TABLE_CAT_BONUS_LANGS."
                            SET `descr` = '".$bonus['pdescr']."',
	                            `descr0` = '".$bonus['pdescr0']."'
                            WHERE `item_id` = ".$id;

                if( !$this->db->exec($query) )
                {
                    return false;
                }
                if( !$this->db->exec($query1) )
                {
                    return false;
                }
                return true;
            }
        }
    }

	protected function sendMsgNotifyMail($tomail, $sender_name, $msgtext)
    {
        if( $tomail == "" )
            return;

        $subject = "Новое сообщение на сайте ".WWWHOST."";
        $txt = "<hr><div><h3>Вам пришло новое сообщение на сайте <a href=".WWWHOST.">".$this->cfg['NAME_RU']."</h3></a></div>

<br>Сообщение от <b>".$sender_name."</b>:
".$msgtext."

<br><br>Это письмо отправлено в автоматическом режиме и на него не нужно отвечать. Чтобы написать ответ на полученное сообщение вам необходимо войти в свой кабинет.
	
<br><br><hr>Служба поддержки ".$this->cfg['NAME_RU']."
";

        /* TEST MAIL */
        // The message
        /*$message = "<hr>
        <b>Имя</b>:check
        <a href=".WWWHOST.">SITE</a>
        <b>Электронный адрес</b>: 'qwe100com@mail.ru'
        <b>Сообщение</b>: Work mess!<hr>";*/

        // In case any of our lines are larger than 70 characters, we should use wordwrap()
        //$message = wordwrap($message, 70);
        //mail('qwe100com@mail.ru', 'WayOfHelp', $txt, "Content-type:text/html; charset = utf-8");
        /**************/

        UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $txt);
        UhCmsApp::getMailerInstance()->sendHTMLmail('qwe100com@mail.ru', $subject, $txt);
    }

    public function addBonusPower($uid){
        $query = "UPDATE ".TABLE_SHOP_BUYERS_BOX." SET bonus_money='".BONUS_MONEY."', money = money + bonus_money WHERE buyer_id= '".addslashes($uid)."'";
        $this->db->exec($query);
    }
	
    public function delete_Win($winid)
    {
        $query = "SELECT q1.*, w1.id as winid, w1.point_ind
			FROM ".TABLE_CAT_BONUS_QUIZ." q1
			INNER JOIN ".TABLE_CAT_BONUS_WINS." w1 ON q1.id=w1.quiz_id
			WHERE w1.id='".$winid."'";
        $res = $this->db->query($query);

        $query = "UPDATE ".TABLE_CAT_BONUS_QUIZ." SET status='".BONUS_QUIZ_STATUS_USED."' WHERE id='".$res[0]['id']."'";
        if( !$this->db->exec( $query ) )
            return false;

        $query = "DELETE FROM ".TABLE_CAT_BONUS_WINS." WHERE id='$winid'";
        if( !$this->db->exec( $query ) )
            return false;

        return true;
    }
	
	public function msg_Send($fromid, $toid, $txt, $projid=0, $replyto=0, $bon = 1)
    {
        $query = "INSERT INTO ".TABLE_CAT_MSG." (item_id, from_id, to_id, reply_msg_id, add_date, modify_date, message) 
			VALUES ('$projid', '$fromid', '$toid', '$replyto', NOW(), NOW(), '".addslashes($txt)."')";
        if( !$this->db->exec($query) )
            return false;

        $msg_id = $this->db->insert_id();
        if( $msg_id == 0 )
            return false;

        $query = "INSERT INTO ".TABLE_CAT_MSG_P2P." (msg_id, user_id, add_date, modify_date) 
			VALUES ('$msg_id', '$fromid', NOW(), NOW() )";
        if( !$this->db->exec($query) )
            return false;

        $query = "INSERT INTO ".TABLE_CAT_MSG_P2P." (msg_id, user_id, add_date, modify_date)
		VALUES ('$msg_id', '$toid', NOW(), NOW() )";
        if( !$this->db->exec($query) )
            return false;

        $to_email = "";
        $from_name = "";

        $query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE id='".$toid."'";
        $res = $this->db->query($query);
        if( count($res)>0 )
        {
            $to_email = $res[0]['email'];
        }

        $query = "SELECT * FROM ".TABLE_SHOP_BUYERS." WHERE id='".$fromid."'";
        $res = $this->db->query($query);
        if( count($res)>0 )
        {
            $from_name = ( $res[0]['account_type'] == USR_TYPE_PERS ? $res[0]['name'].' '.$res[0]['fname'] : $res[0]['orgname'] );
        }

        // Send notify mail
        if ($bon == 1){
        if( ($to_email != "") && ($from_name != "") )
            $this->sendMsgNotifyMail( $to_email, $from_name, $txt);
    }
        
        
        return true;
    }
}