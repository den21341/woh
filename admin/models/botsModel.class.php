<?php

class BotsModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId) {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function setBot($uid, $val = 1) {
        if (is_numeric($uid) && is_numeric($val)) {
            $query = "UPDATE " . TABLE_SHOP_BUYERS . " SET is_bot = " . addslashes($val) . " WHERE id = " . addslashes($uid);
            return $this->db->exec($query);
        }
        return false;
    }

    public function getBots($itm = 0) {
        if($itm) {
            $query = "SELECT b1.*, i1.id item_id FROM " . TABLE_SHOP_BUYERS . " b1 
            JOIN ".TABLE_CAT_ITEMS." i1 ON i1.author_id = b1.id 
            WHERE b1.is_bot = 1 AND i1.status = ".PROJ_STATUS_RUN." AND i1.active = 1 AND i1.amount_type = ".PROJ_TYPE_MONEY." GROUP BY b1.id ";
        } else
            $query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE is_bot = 1";

        return $this->db->query($query);
    }

    public function getMsgs($bid) {
        if (is_numeric($bid)) {
            $query = "SELECT m1.*, b1.name, b1.fname FROM `jhlp_msg_board` m1
                    JOIN jhlp_cat_buyer b1 ON b1.id = m1.from_id
                    WHERE m1.`to_id` = " . addslashes($bid) . "
                    ORDER BY m1.`add_date` DESC";
            return $this->db->query($query);
        }
        return false;
    }

    public function getComment($bid) {
        if (is_numeric($bid)) {
            $query = "SELECT c1.id, c1.item_id, c1.author_id, b1.name, b1.fname, c1.add_date, cl1.content 
                        FROM " . TABLE_CAT_ITEMS_COMMENT_LANGS . " cl1
                        JOIN " . TABLE_CAT_ITEMS_COMMENT . " c1 ON cl1.id = c1.id
                        JOIN " . TABLE_SHOP_BUYERS . " b1 ON b1.id = c1.author_id
                        WHERE c1.item_id IN (SELECT id FROM jhlp_cat_item WHERE author_id = " . addslashes($bid) . ")
                        ORDER BY c1.add_date DESC";
            return $this->db->query($query);
        }
        return false;
    }

    public function sendBotMsg($fromid, $toid, $txt, $projid = 0, $replyto = 0) {
        $query = "INSERT INTO " . TABLE_CAT_MSG . " (item_id, from_id, to_id, reply_msg_id, add_date, modify_date, message) 
			VALUES ('$projid', '$fromid', '$toid', '$replyto', NOW(), NOW(), '" . addslashes($txt) . "')";
        if (!$this->db->exec($query))
            return false;

        $msg_id = $this->db->insert_id();
        if ($msg_id == 0)
            return false;

        $query = "INSERT INTO " . TABLE_CAT_MSG_P2P . " (msg_id, user_id, add_date, modify_date) 
			VALUES ('$msg_id', '$fromid', NOW(), NOW() )";
        if (!$this->db->exec($query))
            return false;

        $query = "INSERT INTO " . TABLE_CAT_MSG_P2P . " (msg_id, user_id, add_date, modify_date)
		VALUES ('$msg_id', '$toid', NOW(), NOW() )";
        if (!$this->db->exec($query))
            return false;

        $to_email = "";
        $from_name = "";

        $query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='" . $toid . "'";
        $res = $this->db->query($query);
        if (count($res) > 0) {
            $to_email = $res[0]['email'];
        }

        $query = "SELECT * FROM " . TABLE_SHOP_BUYERS . " WHERE id='" . $fromid . "'";
        $res = $this->db->query($query);
        if (count($res) > 0) {
            $from_name = ($res[0]['account_type'] == USR_TYPE_PERS ? $res[0]['name'] . ' ' . $res[0]['fname'] : $res[0]['orgname']);
        }

        // Send notify mail
        if (($to_email != "") && ($from_name != ""))
            $this->sendMsgNotifyMail($to_email, $from_name, $txt);

        return true;
    }

    public function getUsrName($id) {
        if(is_numeric($id)) {
            $query = "SELECT name, fname FROM " . TABLE_SHOP_BUYERS . " WHERE id = " . addslashes($id);
            return $this->db->query($query);
        } return false;
    }

    protected function sendMsgNotifyMail($tomail, $sender_name, $msgtext) {
        if( $tomail == "" )
            return;

        $subject = "Новое сообщение на сайте WayOfHelp";
        $txt = "<hr><div><h3>Вам пришло новое сообщение на сайте <a href=".WWWHOST.">".$this->cfg['NAME_RU']."</h3></a></div>

<br>Сообщение от <b>".$sender_name."</b>:
".$msgtext."

<br><br>Это письмо отправлено в автоматическом режиме и на него не нужно отвечать. Чтобы написать ответ на полученное сообщение вам необходимо войти в свой кабинет.
	
<br><br><hr>Служба поддержки ".$this->cfg['NAME_RU']."
";

        UhCmsApp::getMailerInstance()->sendHTMLmail($tomail, $subject, $txt);
    }

    public function getBotsItems($uid) {
        $query = "SELECT i1.* FROM ".TABLE_CAT_ITEMS." i1 
         WHERE i1.author_id = ".addslashes($uid)." AND i1.status = ".PROJ_STATUS_RUN." AND i1.active = 1 AND i1.amount_type = ".PROJ_TYPE_MONEY;
        return $this->db->query($query);
    }

    public function addHelpToItem($from_id, $pid, $sum) {
        $query = "INSERT INTO ".TABLE_CAT_ITEMS_HELPREQ." (`id`, `item_id`, `sender_id`, `req_type`, `req_status`, `is_second`, `show_first`, `req_amount`, `add_date`, `modify_date`, `comments`, `is_promise`, `promise_pay`, `show`) VALUES (NULL, ".$pid.", ".$from_id.", '0', '1', '0', '0', ".$sum.", NOW(), NOW(), '', '1', '1', '1');";
        return $this->db->exec($query);
    }

}