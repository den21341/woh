<?php

class AboxModel extends Model {
    protected $LangId;
    public $ses;

    function __construct($config, $db, $LangId) {
        parent::__construct($config, $db);

        $this->LangId = $LangId;
        $this->ses = UhCmsAdminApp::getSesInstance();
    }

    public function setMoneyId($uid, $money) {
        if(is_numeric($uid) && is_numeric($money)) {
            $query = "UPDATE ".TABLE_LOCAL_CURRENCY." SET admin_money = admin_money + ".addslashes($money)." WHERE user_id = ".addslashes($uid);
            $this->db->exec($query);

            $query = "UPDATE " . TABLE_SHOP_BUYERS_BOX . " SET money = money + ".addslashes($money)." WHERE buyer_id = ".addslashes($uid);
            return $this->db->exec($query);
        } else return false;
    }

}

