<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	//include "../inc/utils-inc.php";
	//include "../inc/countryutils-inc.php";
	
	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();
	
	$UHCMS_MODNAME = "bonuswin_list";

	$fields['name']['ru'] = 'Имя';
	$fields['fname']['ru'] = 'Фамилия';
	$fields['login']['ru'] = 'Логин';
	$fields['passwd']['ru'] = 'Пароль';
	$fields['address']['ru'] = 'Адрес';
	$fields['city']['ru'] = 'Город';
	$fields['country']['ru'] = 'Страна';
	$fields['zip_code']['ru'] = 'Почтовый Индекс';
	$fields['telephone']['ru'] = 'Тел.';
	$fields['office_phone']['ru'] = 'Раб.';
	$fields['cell_phone']['ru'] = 'Моб.';
	$fields['email1']['ru'] = 'E-Mail';
	$fields['email2']['ru'] = 'E-Mail';
	$fields['email3']['ru'] = 'E-Mail';
	$fields['web_url']['ru'] = 'Веб-страница';
	$fields['groupid']['ru'] = 'Группа пользователей';
	$fields['trader']['ru'] = 'Привязка к трейдеру';

	$fields['name']['en'] = 'Name';
	$fields['fname']['en'] = 'Sirname';
	$fields['login']['en'] = 'Login';
	$fields['passwd']['en'] = 'Password';
	$fields['address']['en'] = 'Address';
	$fields['city']['en'] = 'City';
	$fields['country']['en'] = 'Country';
	$fields['zip_code']['en'] = 'Zip';
	$fields['telephone']['en'] = 'Telephone';
	$fields['office_phone']['en'] = 'Office Phone';
	$fields['cell_phone']['en'] = 'Cell Phone';
	$fields['email1']['en'] = 'E-Mail 1';
	$fields['email2']['en'] = 'E-Mail 2';
	$fields['email3']['en'] = 'E-Mail 3';
	$fields['web_url']['en'] = 'Web-url';
	$fields['groupid']['en'] = 'User Group';
	$fields['trader']['en'] = 'Trader Assign';

	$strings['editprof']['en'] = "Edit project's profile";
	$strings['newprof']['en'] = "Add user profile";
	$strings['projlist']['en'] = "Projects List";
	$strings['addbtn']['en'] = " Add New ";
	$strings['applybtn']['en'] = " Apply ";
	$strings['rowlogin']['en'] = "Login";
	$strings['rowname']['en'] = "Name";
	$strings['rowaddr']['en'] = "Address";
	$strings['rowcontact']['en'] = "Contacts";
	$strings['tipchangepwd']['en'] = "Change Password";
	$strings['tipedituser']['en'] = "Edit User Profile";
	$strings['tipdeluser']['en'] = "Delete User";
	$strings['tipstatus']['en'] = "Change Status";

	$strings['editprof']['ru'] = "Редактирование Запроса";
	$strings['newprof']['ru'] = "Добавление Нового Пользователя";
	$strings['projlist']['ru'] = "Список Предложений";
	$strings['addbtn']['ru'] = " Добавить ";
	$strings['applybtn']['ru'] = " Применить ";
	$strings['rowlogin']['ru'] = "Пользователи";
	$strings['rowname']['ru'] = "Ф.И.О.";
	$strings['rowaddr']['ru'] = "Адрес";
	$strings['rowcontact']['ru'] = "Контакты";
	$strings['tipchangepwd']['ru'] = "Изменить Пароль";
	$strings['tipedituser']['ru'] = "Редактировать Профиль";
	$strings['tipdeluser']['ru'] = "Удалить Пользователя";
	$strings['tipstatus']['ru'] = "Изменить Статус";

	////////////////////////////////////////////////////////////////////////////
	
	$citylist = Array();	//GetCityList($LangId);

	////////////////////////////////////////////////////////////////////////////
	
	if( $this->viewMode == "editreq" )
	{
		$reqi = $this->reqinfo;		
	}
	
	echo $this->viewMode;
	
	if( $this->viewMode == "editreq" )
	{
?>
	<br /><br />
    <h2><?=($this->viewMode == "editreq" ? $strings['editprof'][$lang] : $strings['newprof'][$lang]);?></h2>
	<div class="dd-sep"></div>
	<form class="w600-frm" action="<?=( $this->viewMode == "editreq" ? $this->page_BuildUrl("helpreq", "savereq") : $this->page_BuildUrl("requests", "") );?>" method="post">
	<input type="hidden" name="action" value="<?=($this->viewMode == "editreq" ? "applynew" : "add");?>" />
	<?=( $this->viewMode == "editreq" ? '<input type="hidden" name="rid" value="'.$reqi['id'].'" />' : '' );?>
	<table class="tblfrm">
	<tr>
		<th colspan="2">Общие данные</th>
	</tr>
	<tr class="even">
    	<td class="ff">Кол-во:</td>
    	<td>
    		<input name="ptit" type="text" size="10" value="<?=$reqi['req_amount'];?>" />
    	</td>
    </tr>
    <tr>
	    <td class="ff">Оценка:</td>
	    <td><select name="prate">
	<?php
		for( $i=1; $i<=10; $i++ )
		{
			echo '<option value="'.$i.'"'.($reqi['rate'] == $i ? ' selected' : '').'>'.$i.'</option>';
		}
	?>
	    </select></td>
	</tr>
	<tr class="even">
	    <td class="ff">На главную:</td>
	    <td><select name="pfirst">
	    	<option value="0">НЕТ</option>
	    	<option value="1"<?=($reqi['show_first'] == 1 ? ' selected' : '')?>>ДА</option>
	    </select></td>
	</tr>
	<tr>
		<th colspan="2">Дополнительные сведения</th>
	</tr>
	<tr class="even">
	    <td class="ff">Описание:</td>
	    <td><textarea name="pdescr0" cols="70" rows="8"><?=$reqi['rate_comments'];?></textarea></td>
	</tr>
	</table>
<?php
	if( $this->viewMode == "editreq" )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-user-save.png" alt="Сохранить изменения" /></div>';
	}
	else
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-user-add.png" alt="Добавить нового пользователя" /></div>';
	}
?>
	</form>
<?php
	}
	
	//var_dump($usergroups);
?>


    <!-- PART OF PAGE TO DISPLAY USER'S LIST -->
    <br /><br />
	<h2><?=$strings['projlist'][$lang];?></h2>
	<table class="tbldat">
	<tr>
		<th>Пользователь</th>
		<th>Группа</th>
		<th>Бонус</th>
		<th>Номер</th>
		<th>Кол-во</th>
		<th>Статус</th>
		<th>Дата</th>		
		<th class="ls">Действия</th>
	</tr>
    <?php
		$its = $this->winlist;
		
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			$user_city = "";
			$user_country = "";					
			
			$pstatus_str = '';
			
			/*
			switch( $row['req_status'] )
			{
				case REQ_STATUS_NEW:		$pstatus_str = 'Новый';	break;
				case REQ_STATUS_DECLINE:	$pstatus_str = 'Отклон.';	break;
				case REQ_STATUS_CONFIRM:	$pstatus_str = '<span style="color:#22FF22;">Принят</span>';	break;
				case REQ_STATUS_CANCEL:		$pstatus_str = '<span style="color:#22FF22;">Отменен</span>';	break;
			}
			*/
			
			//$pstatus_str = $row['rate'];

			echo '<tr'.( ($i+1) % 2 == 1 ? ' class="even"' : '' ).'>
				<td class="p-name"><a target="_blank" href="'.WWWHOST.'users/viewrev/'.$row['user_id'].'/">'.(
					$row['account_type'] == USR_TYPE_PERS ? 
						stripslashes($row['name']).' '.stripslashes($row['fname']) :
						stripslashes($row['orgname']).'<br><span>'.stripslashes($row['name']).' '.stripslashes($row['fname'].'</span>')
				).'</a></td>
				<td>'.$this->catLib->Buyer_GroupInfo($row['account_type']).'</td>
				<td class="p-name"><a target="_blank" href="'.WWWHOST.'bonus/view/'.$row['item_id'].'/" >'.stripslashes($row['title2']).'</a></td>
				<td>'.$row['point_ind'].' / '.$row['difficulty'].'</td>
				<td>'.$row['amount_used'].' / '.$row['amount'].'</td>
				<td>'.( ($row['amount'] - $row['amount_used']) > 0 ? ($row['amount'] - $row['amount_used']) : '<span style="color:red;">0</span>' ).'</td>
				<td>
					'.$row['add_date'].'
				</td>				
				<td class="ls">
					'.( false ? '<a href="'.$this->page_BuildUrl( "bonuswin", "status", 'rid='.$row['id'].'&active='.($row['status'] == NULL ? 0 : 1) ).'" title="Остановить проект"><img src="'.WWWHOST.'admin/img/a-usr-dis.png" width="22" height="22" alt="Остановить пользователя" /></a>&nbsp;' : '' ).'
					'.( false ? '<a href="'.$this->page_BuildUrl( "bonuswin", "editreq", 'rid='.$row['id'] ).'" title="Редактировать"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="Редактировать" /></a>&nbsp;' : '' ).'
					<a onclick="return confirm(\'Вы хотите удалить этот запрос. Действительно удалить выигрышь?\')" href="'.$this->page_BuildUrl( "bonuswin", "delete", 'winid='.$row['winid'] ).'" title="Удалить"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="Удалить" /></a>
				</td>
			</tr>';
		}		
    ?>
	</table>

<?php
	//include "inc/footer-inc.php";
	//include "../inc/close-inc.php";
?>
