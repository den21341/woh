<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////
	
	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();

	////////////////////////////////////////////////////////////////////////////
	// Module defines
	$strings['tipedit']['en'] = "Edit This Section Name";
   	$strings['tipdel']['en'] = "Delete This Section";
   	$strings['bclist']['en'] = "Sections list";

	$strings['tipedit']['ru'] = "Редактировать разделы каталога";
   	$strings['tipdel']['ru'] = "Удалить раздел";
   	$strings['bclist']['ru'] = "Список разделов каталога";

	$PAGE_HEADER['ru'] = "Управление разделами каталога";
	$PAGE_HEADER['en'] = "Work's Catalog Sections";
	
	////////////////////////////////////////////////////////////////////////////
	// Module inits
	$glist = Array();
	
	$reqd = $this->sectinfo;
	
	/*
	$query = "SELECT * FROM $TABLE_CAT_SECTGROUPS ORDER BY sort_num";
	if( $res = mysql_query( $query ) )
	{
		while( $row = mysql_fetch_object( $res ) )
		{
			$gli = Array();
			$gli['id'] = $row->id;
			$gli['name'] = stripslashes($row->title);

			$glist[] = $gli;
		}
		mysql_free_result( $res );
	}
	
	$THIS_TABLE = $TABLE_CAT_CATALOG;
	$THIS_TABLE_LANG = $TABLE_CAT_CATALOG_LANGS;
	$THIS_TABLE_FILES = $TABLE_CAT_CATALOG_PICS;
	$THIS_TABLE_P2P = $TABLE_CAT_CATITEMS;	

	////////////////////////////////////////////////////////////////////////////
	// Request parameters
	$mode = GetParameter("mode", "");
	$action = GetParameter("action", "");

	$msg = "";

	$orgsect = GetParameter("orgsect", 0);
	$orgname = GetParameter("orgname", "");
	$orgdescr = GetParameter("orgdescr", "", false);
	$orgdescr0 = GetParameter("orgdescr0", "", false);
	$orgsort = GetParameter("orgsort", 0);
	$myfile = GetParameter("myfile", "");
	$orgvis = GetParameter("orgvis", 0);
	$orggroup = GetParameter("orggroup", 0);
	$orglayout = GetParameter("orglayout", 0);
	$orgslayout = GetParameter("orgslayout", 0);
	$page_url = GetParameter("page_url", "");
	$oldurl = GetParameter("oldurl", "");
	$orgfirst = GetParameter("orgfirst", 0);
	$urltype = GetParameter("urltype", 0);

	$orgmakefilt = GetParameter("orgmakefilt", 0);

	switch( $action )
	{
    	case "add":
			$orgsect = GetParameter("orgsect", 0);
			$orgname = GetParameter("orgname", "");
			$orgdescr = GetParameter("orgdescr", "", false);
			$orgdescr0 = GetParameter("orgdescr0", "", false);
			$orgsort = GetParameter("orgsort", 0);
			$myfile = GetParameter("myfile", "");
			$orgvis = GetParameter("orgvis", 0);
			$orggroup = GetParameter("orggroup", 0);
			$orglayout = GetParameter("orglayout", 0);
			$orgslayout = GetParameter("orgslayout", 0);
			$page_url = GetParameter("page_url", "");
			$orgfirst = GetParameter("orgfirst", 0);

			$orgmakefilt = GetParameter("orgmakefilt", 0);
			//$page_title = GetParameter("page_title", "");
			//$page_key = GetParameter("page_key", "");
			//$page_descr = GetParameter("page_descr", "");

			//if($page_url==""){
			//	$page_url=trim($orgname);
			//	$page_url=TranslitEncode($page_url);
			//	$page_url=strtolower($page_url);
			//}else{
			//	$page_url=trim($page_url);
			//	$page_url=TranslitEncode($page_url);
			//	$page_url=strtolower($page_url);
			//}

			$query = "INSERT INTO $THIS_TABLE ( parent_id, sort_num, visible, filename, product_layout, section_layout, url, menu_group_id,
				make_filter, show_first )
				VALUES ('$orgsect', '".$orgsort."', '$orgvis', '".addslashes($myfile)."', '".$orglayout."', '".$orgslayout."',
				'".$page_url."', '".$orggroup."', '$orgmakefilt', '$orgfirst')";
			if( mysql_query($query) )
			{
				$newsectid = mysql_insert_id();

				for( $i=0; $i<count($langs); $i++ )
				{
					if( !mysql_query( "INSERT INTO $THIS_TABLE_LANG ( sect_id, lang_id, name, descr, descr0 ) VALUES
						('$newsectid', '".$langs[$i]."', '".addslashes($orgname)."','".addslashes($orgdescr)."', '".addslashes($orgdescr0)."')" ) )
					{
					   echo mysql_error();
					}
				}
			}
			else
			{
				echo "<b>".mysql_error()."</b>";
			}
			break;


		case "delete":
			// Delete selected news
			$items_id = GetParameter("items_id", "");
			for($i = 0; $i < count($items_id); $i++)
			{
				if(!mysql_query("DELETE FROM $THIS_TABLE WHERE id=".$items_id[$i]." "))
				{
					echo "<b>".mysql_error()."</b>";
				}
				else
				{
					if( !mysql_query( "DELETE FROM $THIS_TABLE_LANG WHERE sect_id='".$items_id[$i]."'" ) )
					{
						echo mysql_error();
					}
				}
			}
			break;

		case "deleteitem":
			$item_id = GetParameter("item_id", "0");

			DeletAllSub($item_id, 0 ,'section');			
			break;

		case "edititem":
			$item_id = GetParameter("item_id", "");
			$mode = "edit";
			Breadcrumbs_Add( $PHP_SELF, $strings['bclist'][$lang] );
			break;

		case "update":
			$item_id = GetParameter("item_id", "");
			$orgsect = GetParameter("orgsect", 0);
			$orgname = GetParameter("orgname", "");
			$orgdescr = GetParameter("orgdescr", "", false);
			$orgdescr0 = GetParameter("orgdescr0", "", false);
			$orgsort = GetParameter("orgsort", 0);
			$myfile = GetParameter("myfile", "");
			$orgvis = GetParameter("orgvis", 0);
			$orggroup = GetParameter("orggroup", 0);
			$orglayout = GetParameter("orglayout", 0);
			$orgslayout = GetParameter("orgslayout", 0);
			$page_url = GetParameter("page_url","");
			$orgfirst = GetParameter("orgfirst", 0);
			$oldurl = GetParameter("oldurl", "");
			$urltype = GetParameter("urltype", 0);

			$orgmakefilt = GetParameter("orgmakefilt", 0);
			$orgmakepos = GetParameter("orgmakepos", $UHCMS_CAT_MAKEPOS);

			$bannerfile = GetParameter("bannerfile", "");
			$bannerlink = GetParameter("bannerlink", "");
			//$page_title = GetParameter("page_title","");
			//$page_key = GetParameter("page_key","");
			//$page_descr = GetParameter("page_descr","");
			//if($page_url==""){
			//	$page_url=trim($orgname);
			//	$page_url=TranslitEncode($page_url);
			//	$page_url=strtolower($page_url);
			//}else{
			//	$page_url=trim($page_url);
			//	$page_url=TranslitEncode($page_url);
			//	$page_url=strtolower($page_url);
			//}
			// check if the same URL is already set for other section
			$urlisused = false;
			if( $page_url != "" )
			{
				$query = "SELECT * FROM $TABLE_CAT_CATALOG WHERE url='".addslashes($page_url)."' AND id<>'".$item_id."' AND is_link=0";
				if( $res = mysql_query( $query ) )
				{
					while( $row = mysql_fetch_object( $res ) )
					{
						$urlisused = true;
					}
					mysql_free_result( $res );
				}
			}

			if( $urlisused )
			{
				$msg = "URL имя страницы, которое вы указали уже используется другим разделом. Нужно указать уникальное название.";
				$mode = "edit";
			}
			else if( $orgsect == $item_id )
			{
				$msg = "Вы не можете в качестве родительского каталога выбрать сам этот же каталог, который редактируете.";
				$mode = "edit";
			}
			else
			{

				if(!mysql_query("UPDATE $THIS_TABLE
	                        SET parent_id='$orgsect', sort_num='$orgsort', filename='".addslashes($myfile)."', product_layout='".$orglayout."',
	                        section_layout='".$orgslayout."', visible='".$orgvis."', url='".addslashes($page_url)."', menu_group_id='".$orggroup."',
	                        make_filter='$orgmakefilt', make_view='$orgmakepos', show_first='$orgfirst', is_link='$urltype',
	                        banner_file='".addslashes($bannerfile)."', banner_link='".addslashes($bannerlink)."',
	                        url_old='".addslashes($oldurl)."'
	                        WHERE id='".$item_id."'"))
				{
					echo "<b>".mysql_error()."</b>";
				}
				else
				{
	    			if( !mysql_query("UPDATE $THIS_TABLE_LANG SET name='".addslashes($orgname)."',
	    				descr='".addslashes($orgdescr)."', descr0='".addslashes($orgdescr0)."'
	            		WHERE sect_id='".$item_id."' AND lang_id='".$LangId."'" ) )
		            {
		            	echo mysql_error();
		            }
				}
			}
			break;

		case "list":
			$item_id = GetParameter("item_id", 0);
			$mode = "showproducts";
			break;

		case "deleteprods":
   			$prodids = GetParameter("prodids", null);
			$item_id = GetParameter("item_id", 0);
			$mode = "showproducts";
			for($i=0; $i<count($prodids); $i++)
			{
    			if( !mysql_query("DELETE FROM $TABLE_CAT_CATITEMS WHERE id='".$prodids[$i]."'") )
    			{
    				echo mysql_error();
    			}
			}
			break;
	}
	
	*/


	////////////////////////////////////////////////////////////////////////////
	// Include Top Header HTML Style
	//include "inc/header-inc.php";
	//
	////////////////////////////////////////////////////////////////////////////

    if( $this->viewMode == "edit" )
    {
    	/*
		$item_id = GetParameter("item_id", 0);
		if( $msg == "" )
		{
			$orgsect = 0;
			$orgname = "";
			$orgdescr = "";
			$orgdescr0 = "";
			$orgsort = 0;
			$myfile = "";
			$orgvis = 0;
			$orggroup = 0;
			$orglayout = 0;
			$orgslayout = 0;
			$page_url = "";
			$orgfirst = 0;
			$urltype = 0;

			$oldurl = "";

			$bannerfile = "";
			$bannerlink = "";

			$orgmakefilt = 0;
			$orgmakepos = $UHCMS_CAT_MAKEPOS;
			//$page_title = "";
			//$page_key = "";
			//$page_descr = "";

			if($res = mysql_query("SELECT s1.*, s2.*
					FROM $THIS_TABLE s1, $THIS_TABLE_LANG s2
					WHERE s1.id='$item_id' AND s1.id=s2.sect_id AND s2.lang_id='$LangId'"))
			{
				if($row = mysql_fetch_object($res))
				{
					$orgsect = $row->parent_id;
					$orgname = stripslashes($row->name);
					$orgdescr = stripslashes($row->descr);
					$orgdescr0 = stripslashes($row->descr0);
					$page_url = stripslashes($row->url);
					//$page_title = stripslashes($row->page_title);
					//$page_key = stripslashes($row->page_keywords);
					//$page_descr = stripslashes($row->page_descr);
					$orgsort = $row->sort_num;
					$orggroup = $row->menu_group_id;
					$myfile = stripslashes($row->filename);
					$orglayout = $row->product_layout;
					$orgslayout = $row->section_layout;
					$orgvis = $row->visible;
					$orgfirst = $row->show_first;
					$urltype = $row->is_link;

					$orgmakefilt = $row->make_filter;
					$orgmakepos = $row->make_view;

					$bannerfile = stripslashes($row->banner_file);
					$bannerlink = stripslashes($row->banner_link);

					$oldurl = stripslashes($row->url_old);
				}
				mysql_free_result($res);
			}
		}
		*/

		//echo "ID: $item_id<br />";
?>
	<div style="padding: 0px 0px 20px 0px; text-align: center;"><a href="<?=$this->Page_BuildUrl("sects", "");?>">Вернуться к перечню разделов</a></div>

	<h2>Редактировать</h2>
	<div class="dd-sep"></div>
	<?php
		if( $this->msg != "" )
		{
			echo '<div class="error" style="padding: 0px 0px 10px 0px; text-align: center;">'.$msg.'</div>';
		}
	?>
	<form name="catfrm" id="catfrm" action="<?=$this->Page_BuildUrl("sects", "save");?>" method="post">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="item_id" value="<?=$reqd['id'];?>" />
	<input type="hidden" name="smode" value="<?=$this->sGroup;?>">
	<table class="tblfrm w100">
    <tr class="even">
    	<td class="ff">Родительский раздел:</td>
    	<td>
    		<select name="orgsect">
    			<option value="0" selected>--- Корневой раздел ---</option>
<?php
		$this->drawSectCombo($reqd['orgsect']);
		
		//PrintWorkCatalog(0, $LangId, 1, "select", $orgsect);
?>
    		</select>
    	</td>
    </tr>
    <tr>
    	<td class="ff">В группе (только для 2го уровня):</td>
    	<td>
    		<select name="orggroup">
    			<option value="0">----- Без группы -----</option>
<?php
	for( $i=0; $i<count($glist); $i++ )
	{
		echo '<option value="'.$glist[$i]['id'].'"'.($reqd['orggroup'] == $glist[$i]['id'] ? " selected" : "").'>'.$glist[$i]['name'].'</option>';
	}
?>
    		<select>
    	</td>
    </tr>
	<tr class="even"><td class="ff">Название раздела:</td><td><input type="text" size="70" name="orgname" value="<?=$reqd['orgname'];?>" /></td></tr>
	<tr>
		<td class="ff">Url:</td>
		<td><input type="text" size="50" name="orgurl" value="<?=$reqd['orgurl'];?>" /><select name="urltype">
			<option value="0"<?=($reqd['urltype'] == 0 ? " selected" : "");?>>Раздел</option>
			<option value="1"<?=($reqd['urltype'] == 1 ? " selected" : "");?>>Ссылка</option>
			</select>  &nbsp;&nbsp;&nbsp; Старый URL <input type="text" name="oldurl" size="14" value="<?=$reqd['oldurl'];?>" />
		</td>
	</tr>
<?php
/*
	<tr><td class="ff">Title:</td><td class="fr"><input type="text" size="70" name="page_title" value="<?=$page_title;?>" /></td></tr>
	<tr><td class="ff">Keywords:</td><td class="fr"><textarea rows="2" cols="65" name="page_key"><?=$page_key;?></textarea></td></tr>
	<tr><td class="ff">Description:</td><td class="fr"><textarea rows="4" cols="65" name="page_descr"><?=$page_descr;?></textarea></td></tr>
*/
?>
	<tr class="even"><td class="ff">Описание:</td><td><textarea class="ckeditor" rows="15" cols="65" name="orgdescr"><?=$reqd['orgdescr'];?></textarea></td></tr>
	<tr><td class="ff">Доп. текст:</td><td><textarea class="ckeditor" rows="7" cols="65" name="orgdescr0"><?=$reqd['orgdescr0'];?></textarea></td></tr>
	<tr class="even"><td class="ff">Порядковый номер:</td><td><input type="text" size="30" name="orgsort" value="<?=$reqd['orgsort'];?>" /></td></tr>
	<tr>
    	<td class="ff">Показывать на сайте:</td>
    	<td>
    		<select name="orgvis">
    			<option value="0"<?=($reqd['orgvis'] == 0 ? ' selected="selected"' : '');?>>НЕТ</option>
    			<option value="1"<?=($reqd['orgvis'] == 1 ? ' selected="selected"' : '');?>>ДА</option>
    		<select>
    	</td>
    </tr>
    <tr class="even">
    	<td class="ff">Показывать на главной:</td>
    	<td>
    		<select name="orgfirst">
    			<option value="0"<?=($reqd['orgfirst'] == 0 ? ' selected="selected"' : '');?>>НЕТ</option>
    			<option value="1"<?=($reqd['orgfirst'] == 1 ? ' selected="selected"' : '');?>>ДА</option>
    		<select>
    	</td>
    </tr>    
<?php
/*
 * <tr>
    	<td class="ff">Показывать бренд в фильтре:</td>
    	<td>
    		<select name="orgmakefilt">
    			<option value="0"<?=($reqd['orgmakefilt'] == 0 ? ' selected="selected"' : '');?>>НЕТ</option>
    			<option value="1"<?=($reqd['orgmakefilt'] == 1 ? ' selected="selected"' : '');?>>ДА</option>
    		<select>
    	</td>
    </tr>
    <tr>
    	<td class="ff">Бренд в названии товара:</td>
    	<td>
    		<select name="orgmakepos">
    	<?php
    		for( $i=0; $i<count($CATBRAND_SHOW); $i++ )
    		{
    			echo '<option value="'.$i.'"'.( $i == $orgmakepos ? ' selected="selected"' : '').'>'.$CATBRAND_SHOW[$i].'</option>';
    		}
    	?>
    		</select>
    	</td>
    </tr>
	<tr>
    	<td class="ff">Тип вывода подсекций:</td>
    	<td class="fr">
    		<select name="orgslayout">
    			<option value="0" <?=($orgslayout == 0 ? " selected" : "");?>>Картинка слева</option>
    			<option value="1" <?=($orgslayout == 1 ? " selected" : "");?>>Картинка сверху</option>
    		<select>
    	</td>
    </tr>
	<tr>
    	<td class="ff">Тип вывода товаров:</td>
    	<td class="fr">
    		<select name="orglayout">
    			<option value="0" <?=($orglayout == 0 ? " selected" : "");?>>Иконками</option>
    			<option value="1" <?=($orglayout == 1 ? " selected" : "");?>>Таблицeй</option>
    			<option value="2" <?=($orglayout == 2 ? " selected" : "");?>>Иконки с описанием</option>
    		<select>
    	</td>
    </tr>
*/
?>
	<tr class="even"><td class="ff">Картинка на главную: </td><td><input type="text" name="myfile" style="width: 200px" value="<?=$reqd['myfile'];?>"><input type="button" value="Выбрать" onclick="MM_openBrWindow('<?=$this->Page_BuildUrl("fileman","", 'full=0&hide=1&target=self.opener.document.catfrm.myfile');?>','winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');"></td></tr>
	<tr><td class="ff">Картинка в раздел: </td><td><input type="text" name="myfile2" style="width: 200px" value="<?=$reqd['myfile2'];?>"><input type="button" value="Выбрать" onclick="MM_openBrWindow('<?=$this->Page_BuildUrl("fileman","", 'full=0&hide=1&target=self.opener.document.catfrm.myfile2');?>','winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');"></td></tr>
	<tr class="even"><td class="ff">Картинка в раздел акт.: </td><td><input type="text" name="myfile3" style="width: 200px" value="<?=$reqd['myfile3'];?>"><input type="button" value="Выбрать" onclick="MM_openBrWindow('<?=$this->Page_BuildUrl("fileman","", 'full=0&hide=1&target=self.opener.document.catfrm.myfile3');?>','winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');"></td></tr>
	<tr><td class="ff">Ссылка с банера: </td><td><input type="text" name="bannerlink" style="width: 200px" value="<?=$reqd['bannerlink'];?>"></td></tr>
	<tr class="even"><td class="ff">Банер в разделе: </td><td><input type="text" name="bannerfile" style="width: 200px" value="<?=$reqd['bannerfile'];?>"><input type="button" value="Выбрать" onclick="MM_openBrWindow('<?=$this->Page_BuildUrl("fileman","", 'full=0&hide=1&target=self.opener.document.catfrm.bannerfile');?>,'winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');"></td></tr>
	</table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-save.png" alt=" Сохранить изменения " /></div>
	</form>
<?php
	}
	else
	{
?>
	<div>
		<a href="<?=$this->Page_BuildUrl("sects", "helpsects");?>">Оказание помощи</a> | 
		<a href="<?=$this->Page_BuildUrl("sects", "");?>">Получение помощи</a>
	</div>

    <h2>Структура разделов в базе</h2>
    <form action="<?=$this->Page_BuildUrl("sects", "deleteall");?>" method=POST>
    <input type="hidden" name="action" value="deleteprods" />
    <table class="tbldat tbltopbrd">
	<tr>
		<th>ID</th>
		<th>№</th>
		<th>Раздел</th>
		<th>Группа</th>
		<th>URL</th>
		<th>Товаров</th>
		<th class="ls">Действия</th>
	</tr>
<?php
	if( $this->viewMode == "showproducts" )
	{
		echo '<input type="hidden" name="item_id" value="'.$item_id.'" />';
  		PrintWorkCatalog(0, $LangId, 0, "products", $item_id);

  		echo "<tr><td colspan=\"2\" align=\"center\"><input type=\"submit\" name=\"delprodbut\" value=\" Удалить отмеченные из раздела \" /></td></tr>";
	}
	else
	{
		$this->drawSectTable();
		//PrintWorkCatalog(0, $LangId, 0, "cattree");
	}
?>
    </table>
    </form>

    <br /><br />
    <h2>Добавить секцию</h2>
    <div class="dd-sep"></div>
	<form action="<?=$this->Page_BuildUrl("sects", "add");?>" name="catfrm" method="POST" >
	<input type="hidden" name="action" value="add">	
	<input type="hidden" name="smode" value="<?=$this->sGroup;?>">	
    <table class="tblfrm w100">
    <tr class="even">
    	<td class="ff">Раздел в который добавлять:</td>
    	<td>
    		<select name="orgsect">
    			<option value="0" selected>--- Корневой раздел ---</option>
<?php
			$this->drawSectCombo();
			//PrintWorkCatalog(0, $LangId, 1, "select");
?>
    		</select>
    	</td>
    </tr>
    <tr>
    	<td class="ff">В группе (только для 2го уровня):</td>
    	<td>
    		<select name="orggroup">
    			<option value="0">----- Без группы -----</option>
<?php
	for( $i=0; $i<count($glist); $i++ )
	{
		echo '<option value="'.$glist[$i]['id'].'"'.($orggroup == $glist[$i]['id'] ? " selected" : "").'>'.$glist[$i]['name'].'</option>';
	}
?>
    		<select>
    	</td>
    </tr>
	<tr class="even"><td class="ff">Название нового раздела:</td><td><input type="text" size="70" name="orgname"></td></tr>
	<tr><td class="ff">Url:</td><td><input type="text" size="70" name="orgurl" value="" /></td></tr>
<?php
/*
	<tr><td class="ff">Title:</td><td class="fr"><input type="text" size="70" name="page_title" value="" /></td></tr>
	<tr><td class="ff">Keywords:</td><td class="fr"><textarea rows="2" cols="65" name="page_key"></textarea></td></tr>
	<tr><td class="ff">Description:</td><td class="fr"><textarea rows="4" cols="65" name="page_descr"></textarea></td></tr>
*/
?>
	<tr class="even"><td class="ff">Описание:</td><td><textarea class="ckeditor" rows="15" cols="65" name="orgdescr"><?=$reqd['orgdescr'];?></textarea></td></tr>
	<tr><td class="ff">Доп. текст:</td><td><textarea class="ckeditor" rows="7" cols="65" name="orgdescr0"><?=$reqd['orgdescr0'];?></textarea></td></tr>
	<tr class="even"><td class="ff">Порядковый номер:</td><td><input type="text" size="30" name="orgsort" value="<?=$reqd['orgsort'];?>" /></td></tr>
    <tr>
    	<td class="ff">Показывать на сайте:</td>
    	<td>
    		<select name="orgvis">
    			<option value="0">НЕТ</option>
    			<option value="1" selected="selected">ДА</option>
    		<select>
    	</td>
    </tr>
    <tr class="even">
    	<td class="ff">Показывать на главной:</td>
    	<td>
    		<select name="orgfirst">
    			<option value="0">НЕТ</option>
    			<option value="1">ДА</option>
    		<select>
    	</td>
    </tr>    
<?php
/*
	<tr>
    	<td class="ff">Показывать бренд в фильтре:</td>
    	<td>
    		<select name="orgmakefilt">
    			<option value="0" selected="selected">НЕТ</option>
    			<option value="1">ДА</option>
    		<select>
    	</td>
    </tr>
    <tr>
    	<td class="ff">Тип вывода подсекций:</td>
    	<td class="fr">
    		<select name="orgslayout">
    			<option value="0">Картинка слева</option>
    			<option value="1">Картинка сверху</option>
    		<select>
    	</td>
    </tr>
    <tr>
    	<td class="ff">Тип вывода товаров:</td>
    	<td class="fr">
    		<select name="orglayout">
    			<option value="0">Иконками</option>
    			<option value="1">Таблицeй</option>
    			<option value="2">Иконки с описанием</option>
    		<select>
    	</td>
    </tr>
*/
?>
    <tr class="even"><td class="ff">Картинка: </td><td><input type="text" name="myfile" style="width: 200px"><input type="button" value="Выбрать" onclick="MM_openBrWindow('<?=$this->Page_BuildUrl("fileman","", 'full=0&hide=1&target=self.opener.document.catfrm.myfile');?>','winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');"></td></tr>
    </table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST?>admin/img/btn-add-new.png" alt="Добавить новый" /></div>
	</form>

<?php
    }

    //include "inc/footer-inc.php";

    //include "../inc/close-inc.php";
?>