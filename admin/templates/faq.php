<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();
	
	//$ntype = $this->viewGroup;	
	//$ntype_arr = $this->model->news->News_GroupList();
	
	$nreq = $this->faqinfo;
	
	$grlist = $this->faqgrlist;
	
	/*
	include "../inc/db-inc.php";
	include "../inc/connect-inc.php";

	include "../inc/utils-inc.php";
	include "inc/admin_catutils-inc.php";

    include "../inc/ses-inc.php";
    include "inc/authorize-inc.php";

    if( $UserId == 0 )
    {
    	header("Location: index.php");
    }

	////////////////////////////////////////////////////////////////////////////
	// Module functions
function checkDtFormat($dt)
{
	if( !(
		is_numeric(substr($dt, 0, 1)) &&
		is_numeric(substr($dt, 1, 1)) &&
		is_numeric(substr($dt, 3, 1)) &&
		is_numeric(substr($dt, 4, 1)) &&
		is_numeric(substr($dt, 6, 1)) &&
		is_numeric(substr($dt, 7, 1)) &&
		is_numeric(substr($dt, 8, 1)) &&
		is_numeric(substr($dt, 9, 1))
		) )
		return false;

	$starr = split("[.]", $dt);
	if( (count($starr) == 3) &&
		is_numeric($starr[1]) && is_numeric($starr[0]) && is_numeric($starr[2]) &&
	 	!checkdate( $starr[1], $starr[0], $starr[2] ) )
	{
		return false;
	}

	return true;
}
	*/

	////////////////////////////////////////////////////////////////////////////
	// Module defines
	$strings['tipedit']['en'] = "Edit This News";
   	$strings['tipdel']['en'] = "Delete This News";
   	$strings['hdrlist']['en'] = "News List";
   	$strings['hdradd']['en'] = "Add News Record";
   	$strings['hdredit']['en'] = "Edit News Record";
   	$strings['rowdate']['en'] = "News date";
   	$strings['rowtitle']['en'] = "Title";
   	$strings['rowfirst']['en'] = "Preview Page";
   	$strings['rowtext']['en'] = "News Text";
   	$strings['rowbrand']['en'] = "Company Source";
   	$strings['btnadd']['en'] = "Add";
   	$strings['btndel']['en'] = "Delete";
   	$strings['btnedit']['en'] = "Edit";
   	$strings['btnrefresh']['en'] = "Update";
   	$strings['nolist']['en'] = "No news in database";
   	$strings['rowcont']['en'] = "Content";
   	$strings['rowfunc']['en'] = "Functions";
   	$strings["deleteconfirm"]["en"] = "Delete this news record now?";
   	$strings['bclist']['en'] = "News list";

    $strings['tipedit']['ru'] = "Редактировать вопрос";
   	$strings['tipdel']['ru'] = "Удалить этот вопрос";
   	$strings['hdrlist']['ru'] = "Список вопросов и ответов";
   	$strings['hdradd']['ru'] = "Добавить вопрос";
   	$strings['hdredit']['ru'] = "Редакировать вопрос";
   	$strings['rowdate']['ru'] = "Дата";
   	$strings['rowtitle']['ru'] = "Заголовок";
   	$strings['rowfirst']['ru'] = "Отображать в анонсе";
   	$strings['rowtext']['ru'] = "Текст";
   	$strings['rowbrand']['ru'] = "Компания";
    $strings['btnadd']['ru'] = "Добавить";
   	$strings['btndel']['ru'] = "Удалить";
   	$strings['btnedit']['ru'] = "Редактировать";
   	$strings['btnrefresh']['ru'] = "Обновить";
   	$strings['nolist']['ru'] = "В базе нет вопросов и ответов";
    $strings['rowcont']['ru'] = "Содержание записей";
   	$strings['rowfunc']['ru'] = "Функции";
   	$strings["deleteconfirm"]["ru"] = "Вы действительно хотите удалить этот вопрос?";
   	$strings['bclist']['ru'] = "Список вопросов";

	

    if( $this->viewMode == "edit" )
    {		
?>
	<br /><br />
    <h2><?=$strings['hdredit'][$lang];?></h2>
	<div class="dd-sep"></div>
	<form name="advfrm" id="advfrm" action="<?=$this->page_BuildUrl("faq", "save");?>" method="post">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="item_id" value="<?=$nreq['id'];?>" />
	<table class="tblfrm w100">
	<tr>
		<th colspan="2">Поля для заполнения</th>
	</tr>
	<tr>
		<td class="ff">Группа:</td>
		<td><select name="group_id">			
	<?php
		//$this->drawSectCombo($nreq['sectid']);
		for( $i=0; $i<count($grlist); $i++ )
		{
			echo '<option value="'.$grlist[$i]['id'].'"'.($nreq['group_id'] == $grlist[$i]['id'] ? ' selected' : '').'>'.$grlist[$i]['name'].'</option>';
		}		
	?>
		</select></td>
	</tr>	
	<tr class="even">
		<td class="ff"><?=$strings['rowtitle'][$lang];?>: </td>
		<td><input type="text" size="70" name="newstitle" value="<?=$nreq['title'];?>" /></td>
	</tr>
	<tr>
		<td class="ff">Описание типа:</td>
    	<td><textarea class="ckeditor" name="newscont" cols="70" rows="10"><?=$nreq['content'];?></textarea></td>
	</tr>
	<tr class="even">
		<td class="ff">Порядковый номер: </td>
		<td><input type="text" size="2" name="newssort" value="<?=$nreq['sort_num'];?>" /></td>
	</tr>
	<?php 
	/*
	<tr class="even">
		<td class="ff">Картинка:</td>
		<td><input type="text" size="30" name="myfile" value="<?=$nreq['filename_src'];?>" /><input type="button" value=" Выбрать файл " onclick="javascript:MM_openBrWindow('cat_files.php?hide=1&lang=<?=$lang;?>&target=self.opener.document.advfrm.myfile','winfiles','width=<?=$FILEMAN_POPUP_W;?>,height=<?=$FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');" /></td>
	</tr>
	<tr>
		<td class="ff">Раздел каталога:</td>
		<td><select name="sectid">
			<option value="0">--- без раздела ---</option>
	<?php
		$this->drawSectCombo($nreq['sectid']);
		
		//$THIS_TABLE = $TABLE_CAT_CATALOG;
		//$THIS_TABLE_LANG = $TABLE_CAT_CATALOG_LANGS;
		//PrintWorkCatalog(0, $LangId, 0, "select", $sectid);
	?>
		</select></td>
	</tr>
	<tr class="even">
		<td class="ff"><?=$strings['rowfirst'][$lang];?>:</td>
    	<td><select name="newsfirst">
			<option value="0">НЕТ</option>
			<option value="1"<?=($nreq['first_page'] == 1 ? ' selected' : '');?>>ДА</option>
		</select></td>
	</tr>
	*/
	?>
	</table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-save.png" alt="Сохранить изменения" /></div>
	</form>

<?php
	}
	else
	{		
	
		//$news_total = $this->model->news->News_ItemsNum($ntype);

		//$pagesnum = ceil($news_total / $this->pn);
?>
	<h2><?=$strings['hdrlist'][$lang];?></h2>
	<?php
	/*
    <div class="dd">
		<div class="left">Тип публикации: &nbsp;
		<?php
			for( $i=0; $i<count($ntype_arr); $i++ )
			{
				if( $i > 0 )
					echo ' &nbsp;::&nbsp; ';

				if( $ntype == $i )
					echo '<b>'.$ntype_arr[$i].'</b>';
				else
					echo '<a href="'.$this->Page_BuildUrl("news", "", 'ntype='.$i).'">'.$ntype_arr[$i].'</a>';
			}
		?>
		</div>
		<div class="right">
			<div class="cpages">
		<?php
			echo '<a class="a-first" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi=1&pn='.$this->pn ).'"></a>
				<a class="a-prev" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi='.($this->pi > 1 ? ($this->pi-1) : 1).'&pn='.$this->pn).'"></a>';
			for( $i=1; $i<=$pagesnum; $i++ )
   			{
   				if( $i == $this->pi )
   					echo '<span>'.$i.'</span>';
   				else
        				echo '<a class="a-page" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi='.$i.'&pn='.$this->pn).'">'.$i.'</a>';
   			}
   			echo '<a class="a-next" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi='.( $this->pi < $pagesnum ? $this->pi+1 : $pagesnum ).'&pn='.$this->pn).'"></a>
				<a class="a-last" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi='.$pagesnum.'&pn='.$this->pn).'"></a>';
   		?>
			</div>
		</div>
		<div class="cen"> &nbsp; </div>
	</div>
	*/
   	?>
	<form action="<?=$this->page_BuildUrl("faq", "deleteall");?>" method=POST>
    <input type="hidden" name="action" value="delete" />
    <table class="tbldat">
	<tr>
		<th>&nbsp;</th>
		<th>Группа / вопрос</th>
		<th><?=$strings['rowcont'][$lang];?></th>
		<th>&nbsp;</th>
		<th class="ls">Действия</th>
	</tr>
<?php
		$found_items = 0;
		$its = $this->faqslist;
		
		$prev_group = 0;
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			$found_items++;
			
			$sect_name = '';
			
			if( $prev_group != $row['group_id'] )
			{
				echo '<tr>
					<td>&nbsp;</td>
					<td colspan="4" class="grname">
						'.$row['grname'].'
					</td>
				</tr>';
				$prev_group = $row['group_id'];
			}
			
			echo '<tr'.( $found_items % 2 == 1 ? ' class="even"' : '' ).'>
				<td><input type="checkbox" name="news_id[]" value="'.$row['id'].'" /></td>
				<td class="p-name">'.$row['title'].'<br /><span>['.$row['add_date'].']'.'</span></td>
				<td>'.$row['short'].'</td>
				<td>&nbsp;</td>
				<td class="ls">
					<a href="'.$this->Page_BuildUrl("faq", "edit", 'item_id='.$row['id']).'" title="'.$strings['tipedit'][$lang].'"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
					<a href="'.$this->Page_BuildUrl("faq", "delete", 'item_id='.$row['id']).'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>&nbsp;
				</td>
			</tr>';
		}
		

		if( $found_items == 0 )
		{
			echo '<tr><td colspan="5" class="ls">'.$strings['nolist'][$lang].'</td></tr>';
        }
    ?>
    </table>
<?php
	if( $found_items > 0 )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-del-selected.png" alt="Удалить выбранные" /></div>';
	}
?>
	</form>

    <br /><br />
    <h2><?=$strings['hdradd'][$lang];?></h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->Page_BuildUrl("faq", "add");?>" method="post">
	<input type="hidden" name="action" value="add" />
	<table class="tblfrm w100">
	<tr>
		<th colspan="2">Поля для заполнения</th>
	</tr>
	<tr>
		<td class="ff">Группа:</td>
		<td><select name="group_id">			
	<?php
		//$this->drawSectCombo($nreq['sectid']);
		for( $i=0; $i<count($grlist); $i++ )
		{
			echo '<option value="'.$grlist[$i]['id'].'"'.($nreq['group_id'] == $grlist[$i]['id'] ? ' selected' : '').'>'.$grlist[$i]['name'].'</option>';
		}		
	?>
		</select></td>
	</tr>	
	<tr class="even">
		<td class="ff"><?=$strings['rowtitle'][$lang];?>: </td>
		<td><input type="text" size="70" name="newstitle" value="<?=$nreq['title'];?>" /></td>
	</tr>
	<tr>
		<td class="ff">Текст ответа:</td>
    	<td><textarea class="ckeditor" name="newscont" cols="70" rows="10"><?=$nreq['content'];?></textarea></td>
	</tr>
	<tr class="even">
		<td class="ff">Порядковый номер: </td>
		<td><input type="text" size="2" name="newssort" value="<?=$nreq['sort_num'];?>" /></td>
	</tr>
	<?php 
	/*
	<tr class="even">
		<td class="ff">Картинка:</td>
		<td><input type="text" size="30" name="myfile" value="<?=$nreq['filename_src'];?>" /><input type="button" value=" Выбрать файл " onclick="javascript:MM_openBrWindow('cat_files.php?hide=1&lang=<?=$lang;?>&target=self.opener.document.catfrm.myfile','winfiles','width=<?=$FILEMAN_POPUP_W;?>,height=<?=$FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');" /></td>
	</tr>
	<tr>
		<td class="ff">Раздел каталога:</td>
		<td><select name="sectid">
			<option value="0">--- без раздела ---</option>
	<?php
		$this->drawSectCombo();		
	?>
		</select></td>
	</tr>
	<tr class="even">
		<td class="ff"><?=$strings['rowfirst'][$lang];?>:</td>
    	<td><select name="newsfirst">
			<option value="0">НЕТ</option>
			<option value="1">ДА</option>
		</select></td>
	</tr>
	*/
	?>
	</table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-make-new.png" alt="Создать новый вопрос" /></div>
	</form>

<?php
    }

    //include "inc/footer-inc.php";
	//
    //include "../inc/close-inc.php";
?>
