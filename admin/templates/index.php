<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;
	
	////////////////////////////////////////////////////////////////////////////
	//	Interface Language
    $lang = $PM->getViewLang(); //"ru";

    ////////////////////////////////////////////////////////////////////////////
    //	Display page layout
    if( $this->pageModel->ses->UserId == 0 )
    {		
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<title><?=( $lang == "en" ? "Website Control Panel: ".$this->cfg['NAME_EN']." - UH CMS" : "Центр управления сайтом: ".$this->cfg['NAME_RU']." - UH CMS" );?></title>
	<link rel="stylesheet" type="text/css" href="css/all.css" />
	<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="css/ie.css" />
	<![endif]-->
	<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ltie7.css" />
	<![endif]-->
</head>
<body class="indexp">
	<div id="page">
		<div id="header">
			<div id="logo">
				<a href="http://uhdesign.com.ua/" target="_blank"><img class="block" src="img/uh_logo.gif" alt="Центр управления сайтом - UH Design" width="199" height="44" /></a>
			</div>
			<div class="tright">
				<div class="tmenu">
					Языки: &nbsp;&nbsp;&nbsp;&nbsp;
					<a href="#" class="active">Рус</a>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="#">Eng</a>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="#">Укр</a>
				</div>
			</div>
			<div class="tcen">
				<div class="tname">
					<p><?=substr(WWWHOST, 7, strlen(WWWHOST)-8);?></p>
				</div>
			</div>
			<div class="both"></div>
		</div>
		<div id="main">
			<div class="logpage">
				<h1>Вход в систему управления</h1>
				<div id="logbox"<?=( $this->msg != "" ? ' style="border:3px solid #df0015"' : '');?>>
					<form action="<?=$this->Page_BuildUrl("");?>" method="post">
					<input type="hidden" name="action" value="makelogin" />
					<div class="errpan"><p><?=$this->msg;?></p></div>
					<div>
						<table>
						<tr>
							<td class="ta_right">Логин: </td>
							<td><input name="login" type="text" /></td>
						</tr>
						<tr>
							<td class="ta_right">Пароль: </td>
							<td><input name="passwd" type="password" /></td>
						</tr>
						</table>
					</div>
					<div class="entpan"><input type="image" src="img/btn-enter.gif" width="100" height="37" alt="Войти" /></div>
					</form>
				</div>
			</div>
		</div>
		<div id="footer">
			<div id="fleft"><a href="#">Справка</a> | <a href="#">Задать вопрос онлайн</a></div>
			<div id="fright">&copy; <?=date("Y", time());?> - Украинский хостинг</div>
		</div>
		<div class="both"></div>
	</div>
</body>
</html>
<?php
/*
<body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
	<tr height="70"><td colspan="2" valign="top" class="panel">
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td width="1"><img src="img/spacer.gif" width="1" height="70" alt="" /></td>
			<td width="180" align="right"><img src="<?=( $lang == "en" ? "img/adminpanel.gif" : "img/adminpanel_ru.gif");?>" width="150" height="38" alt="<?=( $lang == "en" ? "Admin Panel" : "Админ Панель");?>" border="0" /></td>
			<td>&nbsp;</td>
		</tr>
		</table>
	</td></tr>
	<tr><td>
		<table align="center" cellspacing="0" cellpadding="0" border="0" width="200">
		<tr><td>
			<form action="<?=$PHP_SELF;?>" method="POST">
			<input type="hidden" name="action" value="makelogin" />
			<div class="maindiv">
				<h3><?=( $lang == "en" ? "Sign In" : "Вход" );?></h3>

                <b><?=( $lang == "en" ? "Login" : "Логин" );?>:</b><br />
                <input type="text" name="login" value="" style="width: 160px;" /><br />
                <b><?=( $lang == "en" ? "Password" : "Пароль" );?>:</b><br />
				<input type="password" name="passwd" value="" style="width: 160px;" /><br /><br />
				<input type="submit" value=" <?=( $lang == "en" ? "Submit" : "Войти" );?> " />
				<?php
					if( $error_msg != "" )
						echo "<br /><br />$error_msg<br />";
				?>
			</div>
			</form>
		</td></tr>
		</table>
	</td></tr>
    <tr height="70"><td colspan="2" valign="bottom" class="panel">
        <table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<td width="1"><img src="img/spacer.gif" width="1" height="70" alt="" /></td>
			<td align="right"><img src="<?=( $lang == "en" ? "img/adminpanel.gif" : "img/adminpanel_ru.gif" );?>" width="150" height="38" alt="Admin Panel" border="0" /></td>
		</tr>
		</table>
	</td></tr>
	</table>
</body>
</html>
<?php
*/
	}
	else
	{
		//header("Location: main.php");
		//exit;
	}

	//include "../inc/close-inc.php";
?>