<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;    

	//include "inc/header-inc.php";

    //$lines = file("txt/intro_".$lang.".txt");
    //$intro_txt = join("<br />",$lines);


	echo "Вы вошли на административную панель сайта.";
    //echo $intro_txt;

    $mngroups = Array(
    	Array("name" => "", "pic" => ""),
    	Array("name" => "Управление сайтом",	"pic" => "icn/icn_control.png"),
    	Array("name" => "Разделы сайта",		"pic" => "icn/icn_sections.png"),
    	Array("name" => "Наполнение каталога",	"pic" => "icn/icn_catalog.png"),
    	Array("name" => "Конфигуратор каталога","pic" => "icn/icn_config.png"),
    	Array("name" => "Управления заказами",	"pic" => "icn/icn_orders.png"),
    	Array("name" => "Экспорт данных",		"pic" => "icn/icn_export.png")
    );
?>

	<table class="ind-menu">
<?php
	$menu_data = $PM->getAdminMenu();
	$menu_items2gr = $menu_data['menu_2groups'];

	$MNCOLS = 2;
	for($gi=1; $gi<count($mngroups); $gi++)
	{		if( $gi % $MNCOLS == 1 )
			echo '<tr>';
		echo '<td>
			<div class="ind-ico"><img src="img/'.$mngroups[$gi]['pic'].'" width="182" height="152" alt="" /></div>
			<div class="ind-info">
				<div class="ind-group">'.$mngroups[$gi]['name'].'</div>
				<div class="ind-links">
					<ul>';
		if( isset($menu_items2gr[$gi]) )
		{
			$mglinks = $menu_items2gr[$gi];

			for( $i=0; $i<count($mglinks); $i++ )
			{
				echo '<li><a href="'.$mglinks[$i]['link'].'">'.$mglinks[$i]['name'].'</a></li>';
			}
		}

		echo '</ul>
				</div>
			</div>
		</td>';

		if( $gi % $MNCOLS == 0 )
			echo '</tr>';
	}

	if( $gi % $MNCOLS == 0 )
		echo '</tr>';
?>
	</table>

<?php
	//include "inc/footer-inc.php";
	//include "../inc/close-inc.php";
?>