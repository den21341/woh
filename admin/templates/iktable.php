<form action="<?=WWWHOST?>admin/iktable/load/" method="post" target="_blank">
    
    <label for="sec">Выберете статус платежей:</label>
    <select name="sel" id="sel">
        <option value="success">Успешные платежи</option>
        <option value="waitAccept">Ожидает оплаты</option>
        <option value="canceled">Отменены</option>
    </select>

    <label for="val">Кол-во. дней:</label>
    <input type="number" name="val" id="val" />

    <label for="curr">Валюта:</label>
    <select name="curr" id="curr">
        <option value="all">Все</option>
        <option value="UAH">Гривны</option>
        <option value="RUB">Рубли</option>
        <option value="USD">Доллары</option>
        <option value="EUR">Евро</option>
    </select>

    <button type="submit">Загрузить</button>
</form>