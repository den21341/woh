<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	//$PAGE_HEADER['ru'] = "Редактировать Общие Настройки";
	//$PAGE_HEADER['en'] = "Edit Contacts";

	// Include Top Header HTML Style
	//include "inc/header-inc.php";

	/*
	$action = GetParameter("action", "");

	switch($action)
	{
		case "apply":
			$optid = GetParameter("optid", 0);
			$optval = GetParameter("optval", 1);
			if(!mysql_query("UPDATE $TABLE_PREFERENCES SET value='".$optval."' WHERE id='".$optid."'"))
			{
				echo mysql_error();
			}
			break;
	}

	$opts = Array();
	if($res = mysql_query("SELECT * FROM $TABLE_PREFERENCES"))
	{
		while($row=mysql_fetch_object($res))
		{
	    	$opts[$row->id] = $row->value;
	    }
	    mysql_free_result($res);
	}
	*/

	$opts = $this->prefs;
?>

	<br /><br />
    <h2>Установка базовых настроек</h2>
	<div class="dd-sep"></div>
<?php
	for( $i=0; $i<count($opts); $i++ )
	{
?>
	<form class="preference-frm" action="<?=$this->page_BuildUrl("preferences", "save");?>" method="POST">
    <input type="hidden" name="action" value="apply" />
    <input type="hidden" name="optid" value="<?=$opts[$i]['id'];?>" />
	<table class="tblfrm w100">
	<tr class="even">
		<td class="ff ffw130"><?=$opts[$i]['title'];?>: </td>
		<td><input type="text" name="optval" value="<?=$opts[$i]['value'];?>" /></td>
		<td><input type="submit" value=" Применить " /></td>
	</tr>
	</table>
	</form>
	<br />
<?php
	}
	
	/*
?>

	<form class="preference-frm" action="<?=$this->page_BuildUrl("preferences", "save");?>" method="POST">
    <input type="hidden" name="action" value="apply" />
    <input type="hidden" name="optid" value="1" />
	<table class="tblfrm w100">
	<tr class="even">
		<td class="ff ffw130">Курс валюты сайта: </td>
		<td><input type="text" name="optval" value="<?=$opts[1];?>" /></td>
		<td><input type="submit" value=" Применить " /></td>
	</tr>
	</table>
	</form>
	<br />

	<form class="preference-frm" action="<?=$this->page_BuildUrl("preferences", "save");?>" method="POST">
    <input type="hidden" name="action" value="apply" />
    <input type="hidden" name="optid" value="2" />
	<table class="tblfrm w100">
	<tr class="even">
		<td class="ff ffw130">Курс доллара: </td>
		<td><input type="text" name="optval" value="<?=$opts[2];?>" /></td>
		<td><input type="submit" value=" Применить " /></td>
	</tr>
	</table>
	</form>
	<br />

	<form class="preference-frm" action="<?=$this->page_BuildUrl("preferences", "save");?>" method="POST">
    <input type="hidden" name="action" value="apply" />
    <input type="hidden" name="optid" value="3" />
	<table class="tblfrm w100">
	<tr class="even">
		<td class="ff ffw130">Курс евро: </td>
		<td><input type="text" name="optval" value="<?=$opts[3];?>" /></td>
		<td><input type="submit" value=" Применить " /></td>
	</tr>
	</table>
	</form>
<?php
	*/
	
	//include "inc/footer-inc.php";
	//include "../inc/close-inc.php";
?>
