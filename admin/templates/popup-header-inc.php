<?php
	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();
	
	////////////////////////////////////////////////////////////////////////////
	// Meta data and navigation
	$P_TITLE = ( $PM->getViewLang() == "en" ? "Website Control Panel: ".$this->cfg['NAME_EN']." - UH CMS" : "Центр управления сайтом: ".$this->cfg['NAME_RU']." - UH CMS" );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	//if( $lang == "ru" )
	//	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1251\" />";
?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<title><?=$P_TITLE;?></title>
<link rel="stylesheet" type="text/css" href="<?=WWWHOST;?>admin/css/all.css" />
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?=WWWHOST;?>admin/css/ie.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="<?=WWWHOST;?>admin/css/ltie7.css" />
<![endif]-->

	<script language="javascript" src="<?=WWWHOST;?>admin/js/script_cat.js"></script>
</head>
<body>
	<div id="page">
		<div id="header">
			<div id="logo">
				<a href="http://uhdesign.com.ua/" target="_blank"><img class="block" src="<?=WWWHOST;?>admin/img/uh_logo.gif" alt="Центр управления сайтом - UH Design" width="199" height="44" /></a>
			</div>
			<div class="tright2">
				<div class="tmenu">
					<a class="exit" href="javascript:window.close();">Закрыть</a>
				</div>
			</div>
			<div class="both"></div>
		</div>
		<div id="main">
			<div class="both"></div>
			<div id="centc_popup">