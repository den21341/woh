<?php

$PM = $this->pageModel;

$LangId = $PM->getLangId();
$lang = $PM->getViewLang();



$UHCMS_MODNAME = "bonuswin_list";

$fields['name']['ru'] = 'Имя';
$fields['fname']['ru'] = 'Фамилия';
$fields['login']['ru'] = 'Логин';
$fields['passwd']['ru'] = 'Пароль';
$fields['address']['ru'] = 'Адрес';
$fields['city']['ru'] = 'Город';
$fields['country']['ru'] = 'Страна';
$fields['zip_code']['ru'] = 'Почтовый Индекс';
$fields['telephone']['ru'] = 'Тел.';
$fields['office_phone']['ru'] = 'Раб.';
$fields['cell_phone']['ru'] = 'Моб.';
$fields['email1']['ru'] = 'E-Mail';
$fields['email2']['ru'] = 'E-Mail';
$fields['email3']['ru'] = 'E-Mail';
$fields['web_url']['ru'] = 'Веб-страница';
$fields['groupid']['ru'] = 'Группа пользователей';
$fields['trader']['ru'] = 'Привязка к трейдеру';

$fields['name']['en'] = 'Name';
$fields['fname']['en'] = 'Sirname';
$fields['login']['en'] = 'Login';
$fields['passwd']['en'] = 'Password';
$fields['address']['en'] = 'Address';
$fields['city']['en'] = 'City';
$fields['country']['en'] = 'Country';
$fields['zip_code']['en'] = 'Zip';
$fields['telephone']['en'] = 'Telephone';
$fields['office_phone']['en'] = 'Office Phone';
$fields['cell_phone']['en'] = 'Cell Phone';
$fields['email1']['en'] = 'E-Mail 1';
$fields['email2']['en'] = 'E-Mail 2';
$fields['email3']['en'] = 'E-Mail 3';
$fields['web_url']['en'] = 'Web-url';
$fields['groupid']['en'] = 'User Group';
$fields['trader']['en'] = 'Trader Assign';

$strings['editprof']['en'] = "Edit project's profile";
$strings['newprof']['en'] = "Add user profile";
$strings['projlist']['en'] = "Projects List";
$strings['addbtn']['en'] = " Add New ";
$strings['applybtn']['en'] = " Apply ";
$strings['rowlogin']['en'] = "Login";
$strings['rowname']['en'] = "Name";
$strings['rowaddr']['en'] = "Address";
$strings['rowcontact']['en'] = "Contacts";
$strings['tipchangepwd']['en'] = "Change Password";
$strings['tipedituser']['en'] = "Edit User Profile";
$strings['tipdeluser']['en'] = "Delete User";
$strings['tipstatus']['en'] = "Change Status";

$strings['editprof']['ru'] = "Редактирование Запроса";
$strings['newprof']['ru'] = "Добавление Нового Пользователя";
$strings['projlist']['ru'] = "Список Предложений";
$strings['addbtn']['ru'] = " Добавить ";
$strings['applybtn']['ru'] = " Применить ";
$strings['rowlogin']['ru'] = "Пользователи";
$strings['rowname']['ru'] = "Ф.И.О.";
$strings['rowaddr']['ru'] = "Адрес";
$strings['rowcontact']['ru'] = "Контакты";
$strings['tipchangepwd']['ru'] = "Изменить Пароль";
$strings['tipedituser']['ru'] = "Редактировать Профиль";
$strings['tipdeluser']['ru'] = "Удалить Пользователя";
$strings['tipstatus']['ru'] = "Изменить Статус";

////////////////////////////////////////////////////////////////////////////

$citylist = Array();	//GetCityList($LangId);

////////////////////////////////////////////////////////////////////////////

if( $this->viewMode == "editproj" )
{
    $bonInfo = $this->boninfo[0];
    //print_r($bonInfo['premoderation']);
    //die();
}

echo $this->viewMode;

if( $this->viewMode == "editproj" )
{
    $che = ($bonInfo['premoderation']? 'checked' : 'unchecked' );

    ?>
    <br/><br/>
    <h2><?= ($this->viewMode == "editproj" ? $strings['editprof'][$lang] : $strings['newprof'][$lang]); ?></h2>
    <div class="dd-sep"></div>
    <form class="w600-frm"
          action="<?= ($this->viewMode == "editproj" ? $this->page_BuildUrl("bon", "saveproj") : $this->page_BuildUrl("bon", "")); ?>"
          method="post">
        <input type="hidden" name="action" value="<?= ($this->viewMode == "editproj" ? "applynew" : "add"); ?>"/>
        <?= ($this->viewMode == "editproj" ? '<input type="hidden" name="pid" value="' . $bonInfo['id'] . '" />' : ''); ?>
        <?= ($this->viewMode == "editproj" ? '<input type="hidden" name="pname" value="' . $bonInfo['name'] . '" />' : ''); ?>
        <?= ($this->viewMode == "editproj" ? '<input type="hidden" name="pfname" value="' . $bonInfo['fname'] . '" />' : ''); ?>
        <?= ($this->viewMode == "editproj" ? '<input type="hidden" name="pemail" value="' . $bonInfo['email'] . '" />' : ''); ?>
        <?= ($this->viewMode == "editproj" ? '<input type="hidden" name="pstatus" value="' . $bonInfo['status'] . '" />' : ''); ?>
        <table class="tblfrm">
            <tr>
                <th colspan="2">Редактирование бонуса №<?= $bonInfo['id'] ?></th>
            <tr>
                <td>Заголовок</td>
                <td><textarea type="text" name="ptitle" value="<?= $bonInfo['title'] ?>" cols="22"
                              rows="3"><?= $bonInfo['title'] ?></textarea></td>
            </tr>
            <tr>
                <td>Условие</td>
                <td><textarea type="text" name="pdescr" value="<?= $bonInfo['descr'] ?>" cols="22"
                              rows="4"><?= $bonInfo['descr'] ?></textarea></td>
            </tr>
            <tr>
                <td>Описание</td>
                <td><textarea type="text" name="pdescr0" value="<?= $bonInfo['descr0'] ?>" cols="22"
                              rows="10"><?= $bonInfo['descr0'] ?></textarea></td>
            </tr>
            <td>Кол-во бонусов</td>
            <td><input type="text" name="pamount" value="<?= $bonInfo['amount'] ?>"></td>
            </tr>
            </tr>
            <td>Использовано бонусов</td>
            <td><input type="text" name="pam_used" value="<?= $bonInfo['amount_used'] ?>"></td>
            </tr>
            </tr>
            <td>Старт</td>
            <td><input type="text" name="pstart" value="<?= $bonInfo['start_date'] ?>"></td>
            </tr>
            </tr>
            <td>Конец</td>
            <td><input type="text" name="pend" value="<?= $bonInfo['end_date'] ?>"></td>
            </tr>


            </tr>
            <td>Премодерация</td>
            <td><input id="ch" <?= $che ?> type="checkbox" name="premoder" value="<?= $bonInfo['premoderation'] ?>"></td>
            </tr>



        </table>
        <?php
        if ($this->viewMode == "editproj") {
            echo '<div class="frmbtn"><input type="image" src="' . WWWHOST . 'admin/img/btn-user-save.png" alt="Сохранить изменения" /></div>';
        } else {
            echo '<div class="frmbtn"><input type="image" src="' . WWWHOST . 'admin/img/btn-user-add.png" alt="Добавить нового пользователя" /></div>';
        }
        ?>
    </form>
    <?php

}
//var_dump($usergroups);
$its = $this->bonlist;
//print_r($its);
?>

<br /><br />
<h2><?=$strings['projlist'][$lang];?></h2>
<table class="tbldat">
    <tr>
        <th>Пользователь</th>
        <th>Email</th>
        <th>Остановлен</th>
        <th>Кол-во</th>
        <th>Использовано</th>
        <th>Заголовок</th>
        <th>Начало</th>
        <th>Конец</th>
        <th>Премодерация</th>
        <th class="ls">Действия</th>
    </tr>

    <tr>
        <?php for( $i=0; $i<count($its); $i++ ) { $row = $its[$i];
        $yn = ($row['premoderation']? '<span style="color: #42e235;">Да</span>' : '<span style="color: #dc5b65;">Нет</span>' );
        ?>

        <td><?=$row['name'].' '.$row['fname']?></td>
        <td><?=$row['email']?></td>
        <td><?=$row['archive'] ? '<span style="color: #dc5b65;">Да</span>' : '<span style="color: #42e235;">Нет</span>' ?></td>
        <td><?=$row['amount']?></td>
        <td><?=$row['amount_used']?></td>
        <td><?=$row['title']?></td>
        <td><?=$row['start_date']?></td>
        <td><?=$row['end_date']?></td>
        <td><?=$yn?></td>
        <td class="ls">
            <a href="<?=$this->page_BuildUrl( "bon", "status", 'pid='.$row['id'].'&archive='.($row['archive'] ? 0 : 1) )?>" title="Остановить бонус"><img src="<?=WWWHOST?>admin/img/a-usr-dis.png" width="22" height="22" alt="Остановить пользователя" /></a>&nbsp;
            <a href="<?=$this->page_BuildUrl( 'bon', 'editproj', 'pid='.$row['id'])?>" title="Редактировать"><img src="<?=WWWHOST?>admin/img/a-edit.png" width="22" height="22" alt="Редактировать" /></a>&nbsp;
            <!--<a onclick="return confirm('Вы хотите удалить проект. Действительно удалить проект?')" href="<?=$this->page_BuildUrl( 'bon', 'deletebonus', 'pid='.$row['id'] )?>" title="Удалить"><img src="<?=WWWHOST?>admin/img/a-del.png" width="22" height="22" alt="Удалить" /></a>-->
        </td>
    </tr>
    <?php } ?>
</table>

<script>
    var ch = '<?= $bonInfo['premoderation'] ?>';

    $('#ch').bind('click', function () {
        //alert('CLICK');
        if (ch == 1){
            $('#ch').val(0);
        } else {
            $('#ch').val(1);
        }
        //alert($('#ch').val('000000'););
    });
</script>
