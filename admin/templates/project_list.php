<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	//include "../inc/utils-inc.php";
	//include "../inc/countryutils-inc.php";
	
	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();
	
	$UHCMS_MODNAME = "project_list";

	$fields['name']['ru'] = 'Имя';
	$fields['fname']['ru'] = 'Фамилия';
	$fields['login']['ru'] = 'Логин';
	$fields['passwd']['ru'] = 'Пароль';
	$fields['address']['ru'] = 'Адрес';
	$fields['city']['ru'] = 'Город';
	$fields['country']['ru'] = 'Страна';
	$fields['zip_code']['ru'] = 'Почтовый Индекс';
	$fields['telephone']['ru'] = 'Тел.';
	$fields['office_phone']['ru'] = 'Раб.';
	$fields['cell_phone']['ru'] = 'Моб.';
	$fields['email1']['ru'] = 'E-Mail';
	$fields['email2']['ru'] = 'E-Mail';
	$fields['email3']['ru'] = 'E-Mail';
	$fields['web_url']['ru'] = 'Веб-страница';
	$fields['groupid']['ru'] = 'Группа пользователей';
	$fields['trader']['ru'] = 'Привязка к трейдеру';

	$fields['name']['en'] = 'Name';
	$fields['fname']['en'] = 'Sirname';
	$fields['login']['en'] = 'Login';
	$fields['passwd']['en'] = 'Password';
	$fields['address']['en'] = 'Address';
	$fields['city']['en'] = 'City';
	$fields['country']['en'] = 'Country';
	$fields['zip_code']['en'] = 'Zip';
	$fields['telephone']['en'] = 'Telephone';
	$fields['office_phone']['en'] = 'Office Phone';
	$fields['cell_phone']['en'] = 'Cell Phone';
	$fields['email1']['en'] = 'E-Mail 1';
	$fields['email2']['en'] = 'E-Mail 2';
	$fields['email3']['en'] = 'E-Mail 3';
	$fields['web_url']['en'] = 'Web-url';
	$fields['groupid']['en'] = 'User Group';
	$fields['trader']['en'] = 'Trader Assign';

	$strings['editprof']['en'] = "Edit project's profile";
	$strings['newprof']['en'] = "Add user profile";
	$strings['projlist']['en'] = "Projects List";
	$strings['addbtn']['en'] = " Add New ";
	$strings['applybtn']['en'] = " Apply ";
	$strings['rowlogin']['en'] = "Login";
	$strings['rowname']['en'] = "Name";
	$strings['rowaddr']['en'] = "Address";
	$strings['rowcontact']['en'] = "Contacts";
	$strings['tipchangepwd']['en'] = "Change Password";
	$strings['tipedituser']['en'] = "Edit User Profile";
	$strings['tipdeluser']['en'] = "Delete User";
	$strings['tipstatus']['en'] = "Change Status";

	$strings['editprof']['ru'] = "Редактирование Проекта";
	$strings['newprof']['ru'] = "Добавление Нового Пользователя";
	$strings['projlist']['ru'] = "Список Проектов";
	$strings['addbtn']['ru'] = " Добавить ";
	$strings['applybtn']['ru'] = " Применить ";
	$strings['rowlogin']['ru'] = "Пользователи";
	$strings['rowname']['ru'] = "Ф.И.О.";
	$strings['rowaddr']['ru'] = "Адрес";
	$strings['rowcontact']['ru'] = "Контакты";
	$strings['tipchangepwd']['ru'] = "Изменить Пароль";
	$strings['tipedituser']['ru'] = "Редактировать Профиль";
	$strings['tipdeluser']['ru'] = "Удалить Пользователя";
	$strings['tipstatus']['ru'] = "Изменить Статус";

	////////////////////////////////////////////////////////////////////////////
	
	$citylist = Array();	//GetCityList($LangId);

	////////////////////////////////////////////////////////////////////////////
	/*
	$action = GetParameter("action", "");
	$uid = GetParameter("uid", "");

	$uprof = Array();

	$uprof['name'] = GetParameter("fullname", "");
	$uprof['passwd'] = GetParameter("passwd", "");
	$uprof['login'] = GetParameter("login", "");
	$uprof['address'] = GetParameter("address", "");
	$uprof['city_id'] = GetParameter("city_id", 0);
	$uprof['country'] = GetParameter("country", "");
	$uprof['zip_code'] = GetParameter("zip", "");
	$uprof['telephone'] = GetParameter("phone1", "");
	$uprof['office_phone'] = GetParameter("phone2", "");
	$uprof['cell_phone'] = GetParameter("phone3", "");
	$uprof['email1'] = GetParameter("email1", "");
	$uprof['email2'] = GetParameter("email2", "");
	$uprof['email3'] = GetParameter("email3", "");
	$uprof['web_url'] = GetParameter("weburl", "");
	$uprof['groupid'] = GetParameter("userlevel", "0");
	$uprof['usertrader'] = GetParameter("usertrader", 0);
	
	switch( $action )
	{
		case "add":
			if( $UserGroup == $GROUP_ADMIN )
  			{
  				$newuid = makeUuid();
        		if( !mysql_query("INSERT INTO $TABLE_USERS
        							(login, passwd, activation_code, isactive, name, address, city_id, zip_code, telephone,
        							office_phone, cell_phone, email1, email2, email3, web_url, group_id)
	                        VALUES ('".addslashes($uprof['login'])."', PASSWORD('".addslashes($uprof['passwd'])."'), '$newuid', 1, '".addslashes($uprof['name'])."',
	                        '".addslashes($uprof['address'])."', '".addslashes($uprof['city_id'])."', '".addslashes($uprof['zip_code'])."',
	                        '".addslashes($uprof['telephone'])."', '".addslashes($uprof['office_phone'])."', '".addslashes($uprof['cell_phone'])."',
	                        '".addslashes($uprof['email1'])."', '".addslashes($uprof['email2'])."', '".addslashes($uprof['email3'])."',
	                        '".addslashes($uprof['web_url'])."', '".$uprof['groupid']."')"))
            	{
	                echo "<b>".mysql_error()."</b>";
	            }
  			}
			else
  			{
  				header("Location: forbidden.php");
  				exit;
  			}
			break;

		case "status":
  			if(isset($_GET['active']))		$active = $_GET['active'];
  			if(empty($uid))					$uid = $_GET['uid'];

			if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
			{
				echo mysql_error();
			}
  			break;

		case "delete":
			if(empty($uid))			$uid = $_GET['uid'];
			if( $uid == $UserId )	break;

			if(mysql_query("DELETE FROM $TABLE_USERS WHERE id='$uid' AND id<>1"))
			{
				// Make user records cleaning
			}
			else
			{
				echo mysql_error();
			}
  			break;

		case "applynew":
			if( !mysql_query("UPDATE $TABLE_USERS SET login='".addslashes($uprof['login'])."',
						name='".addslashes($uprof['name'])."', address='".addslashes($uprof['address'])."',
						city_id='".addslashes($uprof['city_id'])."',
        				zip_code='".addslashes($uprof['zip_code'])."', telephone='".addslashes($uprof['telephone'])."',
        				office_phone='".addslashes($uprof['office_phone'])."', cell_phone='".addslashes($uprof['cell_phone'])."',
        				email1='".addslashes($uprof['email1'])."', email2='".addslashes($uprof['email2'])."',
        				email3='".addslashes($uprof['email3'])."', web_url='".addslashes($uprof['web_url'])."',
        				group_id='".$uprof['groupid']."' WHERE id=".$_POST['uid']." "))
			{
				echo "<b>".mysql_error()."</b>";
			}

			$action = "editprofile";
			$uid = $_POST['uid'];
			break;
	}
	*/

	if( $this->viewMode == "editproj" )
	{
		$proj = $this->projinfo;		
	}
	
	echo $this->viewMode;
	
	if( $this->viewMode == "editproj" )
	{
?>
	<br /><br />
    <h2><?=($this->viewMode == "editproj" ? $strings['editprof'][$lang] : $strings['newprof'][$lang]);?></h2>
	<div class="dd-sep"></div>
	<form class="w600-frm" action="<?=( $this->viewMode == "editproj" ? $this->page_BuildUrl("projects", "saveproj") : $this->page_BuildUrl("projects", "") );?>" method="post">
	<input type="hidden" name="action" value="<?=($this->viewMode == "editproj" ? "applynew" : "add");?>" />
	<?=( $this->viewMode == "editproj" ? '<input type="hidden" name="pid" value="'.$proj['id'].'" />' : '' );?>
	<table class="tblfrm">
	<tr>
		<th colspan="2">Общие данные</th>
	</tr>
	<tr class="even">
    	<td class="ff">Раздел:</td>
    	<td>
    		<select name="psect">
    			<option value="0" selected>--- Корневой раздел ---</option>
<?php
		$this->drawSectCombo( ($proj['profile_id'] == PROJ_SENDHELP ? 1 : 0), $proj['sectid']);
?>
    		</select>
    	</td>
    </tr>
	<tr>
	    <td class="ff">Заголовок:</td>
	    <td><input name="ptit" type="text" size="70" value="<?=$proj['title2'];?>" /></td>
	</tr>
	<tr class="even">
    	<td class="ff">Активный:</td>
    	<td>
    		<select name="pactive">
    			<option value="0">Нет</option>
    			<option value="1"<?=( $proj['active'] == 1 ? ' selected' : '');?>>Да</option>
    		</select>
    	</td>
    </tr>
    <tr>
    	<td class="ff">Прошел модерацию:</td>
    	<td>
    		<select name="pmoderated">
    			<option value="0">Нет</option>
    			<option value="1"<?=( $proj['moderated'] == 1 ? ' selected' : '');?>>Да</option>
    		</select>
    	</td>
    </tr>
		<tr>
			<td class="ff">Оценка:</td>
			<td>
				<select name="padmrate">
					<?php for($i=0; $i<=10; ++$i) { ?>
						<option <?=($i == $proj['admin_rate']) ? 'selected' : ''?> value="<?=$i?>"><?=$i?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<td class="ff">Легкая помощь:</td>
			<td>
				<select name="phlptype">
					<option value="0">Нет</option>
					<option value="1"<?=( $proj['help_type'] == 1 ? ' selected' : '')?>>Да</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="ff">Вывод проекта:</td>
			<td>
				<select name="ploctype">
					<option value="0">Все</option>
					<option value="1"<?=( $proj['location_type'] == 1 ? ' selected' : '')?>>Страна</option> <!-- SHOW ONLY IN ITEM COUNTRY -->
					<option value="2"<?=( $proj['location_type'] == 2 ? ' selected' : '')?>>Регион</option> <!-- SHOW ONLY IN ITEM REGION OBL. -->
				</select>
			</td>
		</tr>
		<tr>
			<td class="ff">Прошел проверку:</td>
			<td>
				<?php if($proj['is_check']){ ?>
					<input type="checkbox" name="pcheck" class="chbox" checked>
				<?php } else { ?>
					<input type="checkbox" name="pcheck" class="chbox">
				<?php } ?>
			</td>
		</tr>
	<tr>
		<th colspan="2">Дополнительные сведения</th>
	</tr>
	<tr class="even">
	    <td class="ff">Размер помощи:</td>
	    <td><input class="fw" name="pamount" type="text" value="<?=$proj['amount'];?>" /></td>
	</tr>
	<tr>
	    <td class="ff">Описание:</td>
	    <td><textarea name="pdescr0" cols="70" rows="8"><?=$proj['descr0'];?></textarea></td>
	</tr>
	<tr class="even">
	    <td class="ff">Полное Описание:</td>
	    <td><textarea name="pdescr" cols="70" rows="14"><?=$proj['descr'];?></textarea></td>
	</tr>
	<?php
	/*
    <tr>
	    <td class="ff"><?=$fields['address'][$lang];?>:</td>
	    <td><input class="fw" name="address" type="text" value="<?=$uprof['address'];?>" /></td>
	</tr>
    <tr>
	    <td class="ff"><?=$fields['city'][$lang];?>:</td>
	    <td>
	    	<select name="city_id">
	<?php
		for($i=0; $i<count($citylist); $i++)
		{
        	echo "<option value=\"".$citylist[$i]['cityid']."\"".( $citylist[$i]['cityid'] == $uprof['city_id'] ? " selected " : "" ).">".$citylist[$i]['city']."</option>";
        }
	?>
	    	</select>
		</td>
	</tr>
    <tr class="even">
	    <td class="ff"><?=$fields['zip_code'][$lang];?>:</td>
	    <td><input name="zip" type="text" value="<?=$uprof['zip_code'];?>" /></td>
	</tr>
	*/
	?>	
	</table>
<?php
	if( $this->viewMode == "editproj" )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-user-save.png" alt="Сохранить изменения" /></div>';
	}
	else
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-user-add.png" alt="Добавить нового пользователя" /></div>';
	}
?>
	</form>
<?php
	} else if($this->viewMode == 'projdone') { ?>
		<table class="tblfrm">
			<tr>
				<td>id</td>
				<td>Проект</td>
				<td>Сумма сбора</td>
				<td>Собрано</td>
				<td>Выплачено</td>
				<td class="ls">Поставить выплату</td>
			</tr>
			<tr>
				<?php foreach ($this->projlist as $item) { ?>
				<td><?=$item['id']?></td>
				<td><a target="_blank" href="<?=WWWHOST.'proj/view/'.$item['id'].'/'?>"><?=$item['title2']?></a></td>
				<td><?=$item['amount']?></td>
				<td><?=$item['sum']?></td>
				<td><?=($item['paydone'] == PROJ_PAYED_DONE) ? '<span style="color: #0e9400">Да</span>' : 'Нет' ?></td>
				<td><a href="<?=$this->page_BuildUrl('projects','projdone').'?pyd='.($item['paydone'] == PROJ_PAYED_DONE ? 0 : 1).'&pid='.$item['id']?>"><button><?=($item['paydone'] == PROJ_PAYED_DONE) ? 'Отменить' : 'Да' ?></button></a></td>
			</tr>
			<?php } ?>
		</table>

	<?php
		} if($this->viewMode != 'projdone') {
	?>

    <!-- PART OF PAGE TO DISPLAY USER'S LIST -->
	<a href="<?= WWWHOST ?>admin/projects/projdone/">
		<button>Завершенные проекты</button>
	</a>
    <br /><br />
	<h2><?=$strings['projlist'][$lang]?></h2>
	<table class="tbldat">
		<tr>
			<th>Пользователь</th>
			<th>Группа</th>
			<th>Проект</th>
			<th>Нужно</th>
			<th>Помогли</th>
			<th>Дата</th>
			<th>Статус</th>
			<th>Активн.</th>
			<th>Модерац.</th>
			<!--<th>Проверка</th>-->
			<th>Инфо</th>
			<th class="ls">Действия</th>
		</tr>
    <?php
		$its = $this->projlist;
		
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];

			$user_city = "";
			$user_country = "";
			
			/*
			$query1 = "SELECT cl1.name as city, c2.name as country
					FROM $TABLE_CITY c1
					INNER JOIN $TABLE_CITY_LANG cl1 ON c1.id=cl1.city_id AND cl1.lang_id='$LangId'
					INNER JOIN $TABLE_COUNTRY_LANG c2 ON c1.country_id=c1.country_id AND c2.lang_id='$LangId'
					WHERE c1.id='".$row->city_id."'";
			if( $res1 = mysql_query( $query1 ) )
			{
				if( $row1 = mysql_fetch_object($res1) )
				{
					$user_city = stripslashes($row1->city);
					$user_country = stripslashes($row1->country);
				}
				mysql_free_result($res1);
			}
			*/

			$u_addr_str = "";
			//if( ($row['zip_code'] != "") || ($user_country != "") || ($user_city != "") )
			//{
			//	$u_addr_str = ',<br />'.stripslashes($row['zip_code']).' '.$user_country.', '.$user_city;
			//}

			$u_cont_str = "";
			//$u_cont_str .= ( trim(stripslashes($row['phone'])) != "" ?  '<span>'.$fields['telephone'][$lang].'</span> '.stripslashes($row['phone']).'<br />' : '' );
			//$u_cont_str .= ( trim(stripslashes($row['telephone'])) != "" ?  '<span>'.$fields['office_phone'][$lang].'</span> '.stripslashes($row['office_phone']).'<br />' : '' );
			//$u_cont_str .= ( trim(stripslashes($row['telephone'])) != "" ?  '<span>'.$fields['cell_phone'][$lang].'</span> '.stripslashes($row['cell_phone']).'<br />' : '' );
			//$u_cont_str .= ( trim(stripslashes($row['email'])) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row['email']).'">'.stripslashes($row['email']).'</a><br />' : '' );
			//$u_cont_str .= ( trim(stripslashes($row['email2'])) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row['email2']).'">'.stripslashes($row['email2']).'</a><br />' : '' );
			//$u_cont_str .= ( trim(stripslashes($row['email3'])) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row['email3']).'">'.stripslashes($row['email3']).'</a><br />' : '' );
			
			$pstatus_str = '';
			switch( $row['status'] ) {
				case PROJ_STATUS_STOP:	$pstatus_str = 'Останов.';	break;
				case PROJ_STATUS_RUN:	$pstatus_str = '<span style="color:#22FF22;">Работает</span>';	break;
				case PROJ_STATUS_ENDED:	$pstatus_str = 'Закончен';	break;
			}

			echo '<tr'.( ($i+1) % 2 == 1 ? ' class="even"' : '' ).'>
				<td class="p-name"><a target="_blank" href='.WWWHOST.'users/viewrev/'.$row['author_id'].'/'.' >'.(
					$row['account_type'] == USR_TYPE_PERS ? 
						stripslashes($row['name']).' '.stripslashes($row['fname']) :
						stripslashes($row['orgname']).'<br><span>'.stripslashes($row['name']).' '.stripslashes($row['fname'].'</span>')
				).'</a></td>
				<td>'.$row['account_group'].'</td>
				<td class="p-name"><a target="_blank" href="'.WWWHOST."proj/view/".$row['id'].'/'.'" >'.stripslashes($row['title2']).'</a></td>
				<td>'.( $row['amount'] ).'</td>
				<td>
					Всего: '.( $row['rnum_all']['num'] ).'<br>
					Подтверждено: '.( $row['rnum_conf']['num'] ).' на '.$row['rnum_conf']['sum'].'
				</td>
				<td>
					Доб.: '.$row['adddt'].'<br>
					Зап.: '.$row['stdt'].'<br>
					Зак.: '.$row['endt'].'<br>
				</td>
				<td>'.$pstatus_str.'</td>
				<td>'.( $row['active'] == 0 ? '<span style="color: red;">Нет</span>' : 'Да' ).'</td>
				<td>'.( $row['moderated'] == 0 ? '<span style="color: red;">Нет</span>' : 'Да' ).'</td>
			
				<td>'.
				'Оценка: <span style="color: #1F1A0E">'.($row['admin_rate']).'</span>'.'
					<br>'.($row['help_type'] ? '<span style="color: #1cd41c">Лег. помощь</span>' : '<span style="color: rgba(46, 4, 0, 0.21);" >Лег. помощь</span>').'</td>
				<td class="ls">
					<a href="'.$this->page_BuildUrl( "projects", "status", 'pid='.$row['id'].'&active='.($row['status'] == NULL ? 0 : 1) ).'" title="Остановить проект"><img src="'.WWWHOST.'admin/img/a-usr-dis.png" width="22" height="22" alt="Остановить пользователя" /></a>&nbsp;
					<a href="'.$this->page_BuildUrl( "projects", "editproj", 'pid='.$row['id'] ).'" title="Редактировать"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="Редактировать" /></a>&nbsp;
					<a onclick="return confirm(\'Вы хотите удалить проект. Действительно удалить проект?\')" href="'.$this->page_BuildUrl( "projects", "deleteproj", 'pid='.$row['id'] ).'" title="Удалить"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="Удалить" /></a>
				</td>
			</tr>';
			}
			?>
		</table>
		<?php
	}
?>
