<?php
    $items = $this->allPr;
?>
<a href="<?=$this->page_BuildUrl('promise','')?>"><button>Посмотреть все платежи</button></a>
<a href="<?=$this->page_BuildUrl('promise','proj')?>"><button>Посмотреть все проекты</button></a>
<a href="<?=$this->page_BuildUrl('promise','proj').'?type=50'?>"><button>Завершающиеся проекты(>=50%)</button></a>
<a href="<?=$this->page_BuildUrl('promise','proj').'?type=60'?>"><button>Завершенные проекты(>=60%)</button></a>
<hr>
<br>
<?php if($this->viewMode == 'byitem') { ?>
    <table class="tbldat">
        <tr>
            <th>id</th>
            <th>Проект</th>
            <th>Сумма сбора</th>
            <th>Собрал</th>
            <th>Из них обещания</th>
        </tr>
        <tr>
            <?php foreach ($this->its as $its) { ?>
                <th><?=$its['item_id']?></th>
                <th><a target="_blank" href="<?=WWWHOST.'proj/'.'view/'.$its['item_id']?>"><?=$its['title2']?></a></th>
                <th><?=$its['amount']?></th>
                <th><?=$its['req_amount']?></th>
                <th><?=($its['promise_sum'] ? '<a href="'.($this->page_BuildUrl('promise','proj').'?id='.$its['item_id']).'">'.$its['promise_sum'].'</a>' : "--" )?></th>
        </tr>
            <?php } ?>
    </table>
<?php } else if($this->viewMode == 'byuser') {
    if($this->subMode === 'msg') { ?>
        <form action="<?=$this->page_BuildUrl('promise','sendmess')?>" method="post" class="w600-frm">
            <input type="hidden" name="uid" value="<?=$this->uid?>" >
            <input type="hidden" name="pid" value="<?=$this->pid?>" >
            <label for="ms"><h3>Кому: <?=$this->uid?></h3></label>
            <textarea name="message" id="ms" cols="40" rows="5"></textarea><br>
            <button type="submit">Отправить</button>
        </form>
    <?php } ?>
    <table class="tbldat">
        <tr>
            <th>Пользователь</th>
            <th>Сумма обещаний</th>
            <th>Телефон</th>
            <th>Подтвержден</th>
            <th class="ls">Действия</th>
        </tr>
        <tr>
            <?php foreach ($this->its as $its) { ?>
            <td><?=$its['name'].' '.$its['fname']?></td>
            <td><?=$its['req_amount']?></td>
            <td><?=$its['phone'] ? $its['phone'] : '---' ?></td>
            <td><?=$its['is_active'] ? 'Да' : 'Нет'?></td>
            <td><a href="<?=$this->page_BuildUrl('promise', 'mess').'?uid='.$its['uid'].'&pid='.$its['item_id']?>">Сообщение</a></td>
        </tr>
            <?php } ?>
    </table>
<?php } else { ?>

<table class="tbldat">
    <tr>
        <th>id</th>
        <th>Проект</th>
        <th>Сумма обещаний</th>
        <th>Пользователь</th>
        <th>Телефон</th>
        <th>Подтвержден телефон</th>
        <th>Дата</th>
    </tr>

    <tr>
        <?php foreach ($items as $its) { ?>
            <td><?=$its['id']?></td>
            <td><a target="_blank" href="<?=WWWHOST.'proj/','view/'.$its['id']?>"><?=$its['title2']?></a></td>
            <td><?=$its['req_amount'].' '.$its['currname']?></td>
            <td><a href="<?=WWWHOST.'users/'.'viewrev/'.$its['uid'].'/'?>"><?=$its['name'].' '.$its['fname']?></a></td>
            <td><?=(isset($its['phone']) ? $its['phone'] : '------')?></td>
            <td><?=(isset($its['is_active']) ? 'Да' : 'Нет')?></td>
            <td><?=$its['add_date']?></td>
    </tr>
        <?php } ?>
</table>

<?php } ?>
