<?php
    $_uId = $this->usersId;
?>
<?php if($this->viewMode == 'ans') { ?>
    <table class="tbldat">
        <tr>
            <th>Варианты анкет</th>
        </tr>
        <tr>
            <td>
                <a href="<?=$this->Page_BuildUrl('testproj','answers').'?from=0&to=18'?>">Младше 18</a>
            </td>
        </tr>
        <tr>
            <td>
                <a href="<?=$this->Page_BuildUrl('testproj','answers').'?from=18&to=24'?>">18-24</a>
            </td>
        </tr>
        <tr>
            <td>
                <a href="<?=$this->Page_BuildUrl('testproj','answers').'?from=25&to=34'?>">25-34</a>
            </td>
        </tr>
        <tr>
            <td>
                <a href="<?=$this->Page_BuildUrl('testproj','answers').'?from=35&to=44'?>">35-44</a>
            </td>
        </tr>
        <tr>
            <td>
                <a href="<?=$this->Page_BuildUrl('testproj','answers').'?from=45&to=100'?>">45+</a>
            </td>
        </tr>
    </table>
<?php } else if ($this->viewMode == 'ansby') { ?>
    <table class="tbldat">
        <tr>
            <th>Пользователь</th>
            <th class="ls">Действия</th>
        </tr>
        <tr>
            <?php foreach ($this->ansList as $_uidList) { ?>
                <td><a href="<?=WWWHOST.'users/','viewrev/'.$_uidList['user_id'].'/'?>" target="_blank"><?=$_uidList['user_id']?></a></td>
                <td><a href="<?=$this->Page_BuildUrl('testproj','viewans').'?uid='.$_uidList['user_id']?>">Просмотреть анкету</a></td>
        </tr>
            <?php } ?>
    </table>
<?php } else if($this->viewMode == 'userAns') { ?>
    <h3>Анкета пользователя</h3>
    <table class="tbldat">
    <tr>
        <th>Вопрос</th>
        <th>Ответ</th>
    </tr>
    <tr>
<?php foreach ($this->uans as $uanswer) { ?>
        <td><?=$uanswer['qname']?></td>
        <td><?=$uanswer['answer']?></td>
    </tr>
<?php } ?>
    </table>

<?php } else if($this->viewMode == 'allans') { ?>
    <table class="tbldat">
        <tr>
                <th>Вопрос</th>
                <th>Вариант</th>
                <th>Сумма</th>
        </tr>
        <tr>
            <?php foreach ($this->allSum as $_allSum) { ?>
               <?php foreach ($_allSum as $qans) { ?>
                    <td><?=$qans['qname']?></td>
                    <td><?=$qans['answer']?></td>
                    <td><?=$qans['asum']?></td>
        </tr>
             <?php } ?>
           <?php } ?>
    </table>
<?php } ?>

<table class="tbldat">
    <tr>
        <th>Вариант</th>
    </tr>
    <tr>
        <td>
            <a href="<?=$this->Page_BuildUrl('testproj','answers')?>">Отдельные анкеты пользователей</a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="<?=$this->Page_BuildUrl('testproj','ansall')?>">Общий результат</a>
        </td>
    </tr>
</table>