<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();
	
	$UHCMS_MODNAME = "page_manager";

	$HTMLAREA=true;
    
	//include "../inc/utils-inc.php";    

	$strings["hdrsel"]["ru"] = "Скрипты добавленные в базу";
    $strings["hdradd"]["ru"] = "Добавить скрипт";
	$strings["hdredit"]["ru"] = "Редактировать скрипт";
    $strings["rowedit"]["ru"] = "Редактировать";
    $strings["rowsel"]["ru"] = "Выберите страницу";
    $strings["btnnext"]["ru"] = "Далее";
    $strings["rowtitle"]["ru"] = "Заголовок Страницы";
    $strings["rowheader"]["ru"] = "Верхний Контент";
    $strings["rowfooter"]["ru"] = "Нижний Контент";
    $strings["btnapply"]["ru"] = "Применить";
    $strings["btndel"]["ru"] = "Удалить выбранные";
    $strings["hdrname"]["ru"] = "Название";
    $strings["hdrcont"]["ru"] = "Контент";
    $strings["tipedit"]["ru"] = "Редактировать";
    $strings["tipdel"]["ru"] = "Удалить";    
	$strings["deleteconfirm"]["ru"] = "При удаление вся информация связанная с выбранной страницей будет удалена.\\r\\nУдалить?";
	$strings["nolist"]["ru"] = "В базе нет записей";

	$strings["hdrsel"]["en"] = "Select page for editing";
	$strings["hdradd"]["en"] = "Add page";
	$strings["hdredit"]["en"] = "Edit page";
	$strings["rowedit"]["en"] = "Edit page";
	$strings["rowsel"]["en"] = "Select page";
	$strings["btnnext"]["en"] = "Continue";
	$strings["rowtitle"]["en"] = "Page Title";
	$strings["rowheader"]["en"] = "Top text";
	$strings["rowfooter"]["en"] = "Bottom text";
	$strings["btnapply"]["en"] = "Apply";
	$strings["btndel"]["en"] = "Delete selected";
	$strings["hdrname"]["en"] = "Name";
	$strings["hdrcont"]["en"] = "Content";
	$strings["tipedit"]["en"] = "Edit item";
    $strings["tipdel"]["en"] = "Delete item";
    $strings["nolist"]["en"] = "No records in database";
	
	
	$msg = $this->error_msg;

    //require_once "inccat/functions.inc.php";

	/*
	$action = GetParameter( "action", "" );
	

	$content = GetParameter("content", "", false);
	$content=str_replace ("&gt;",">",$content);
	$content=str_replace ("&lt;","<",$content);

	$title = GetParameter("title", "");
	$header = GetParameter("header", "", false);

	$pfile = GetParameter("pfile", "");
	$ptitle = GetParameter("ptitle", "");
	$pmean = GetParameter("pmean", "");
	$pkey = GetParameter("pkey", "");
	$pdescr = GetParameter("pdescr", "");

	$psort = GetParameter("psort", "0");
	$menushow = GetParameter("menushow", 0);
	$pagetype = GetParameter("pagetype", 1);
	$parentid = GetParameter("parentid", 0);

	if( $psort == "" )
		$psort = 0;
	
	switch( $action )
	{
    	case "add":
			if( $pfile == "" )
			{
				$msg = "Страница не добавлена. Вы не указали имя скрипта.";
			}
			else if( $pmean == "" )
			{
				$msg = "Страница не добавлена. Вы не указали назначение скрипта.";
			}
			else
			{
				if( !mysql_query("INSERT INTO $TABLE_PAGES (parent_id, page_name, create_date, modify_date, show_in_menu,
					page_record_type, sort_num)
					VALUES ( $parentid, '".addslashes($pfile)."', NOW(), NOW(), $menushow, $pagetype, $psort )") )
				{
		        	$msg = mysql_error();
		        }
		        else
		        {
		        	$newpageid = mysql_insert_id();
		        	for($i=0; $i<count($langs); $i++)
		        	{		        		$query = "INSERT INTO $TABLE_PAGES_LANGS (item_id, lang_id, page_mean, page_title, page_keywords, page_descr,
		        			content, title, header) VALUES ($newpageid, ".$langs[$i].", '".addslashes($pmean)."',
		        			'".addslashes($ptitle)."', '".addslashes($pkey)."', '".addslashes($pdescr)."',
		        			'".addslashes($content)."', '".addslashes($title)."', '".addslashes($header)."' )";
			            if( !mysql_query( $query ) )
			            {
			            	echo mysql_error();
			            }		        	}

		        	$content = "";

					$title = "";
					$header = "";

					$pfile = "";
					$ptitle = "";
					$pmean = "";
					$pkey = "";
					$pdescr = "";
		        }
			}
			break;

		case "delete":

			$items_id = GetParameter("items_id", "");

			// Delete selected pages
			for($i = 0; $i < count($items_id); $i++)
			{
				//echo $items_id[$i];
				DeletAllSub($items_id[$i], 0 ,'page');				
			}
			break;

		case "deleteitem":

			$item_id = GetParameter("item_id", "0");
			DeletAllSub($item_id, 0 ,'page');			
    		break;

	}
	*/


	//---------------------------- Extract all pages ---------------------------

	if ( $msg != "" )
	{  		echo '<p class="error">'.$msg.'</p>';	}
?>
<script type="text/javascript" src="<?=WWWHOST;?>swfobject.js"></script>
<?php
	
	if( $this->viewMode == "" )
	{
?>

	<h2><?=$strings["hdrsel"][$lang];?></h2>
	<form action="<?=$this->Page_BuildUrl("pages","");?>" method=POST>
    <input type="hidden" name="action" value="delete" />	
	<table class="tbldat tbltopbrd t_black">
	<tr>
		<th>&nbsp;</th>
		<th>№</th>
		<th>Страница</th>
		<th>Скрипт</th>
		<th>Ссылка</th>
		<th>Фото</th>
		<th>Видео</th>
		<th>Дата изменения</th>
		<th class="ls">Действия</th>
	</tr>
<?php
		$found_items = $this->tablePagesList();	// Draw table with pages
	
		if( $found_items == 0 )
		{
			echo '<tr><td colspan="9">'.$strings['nolist'][$lang].'</td></tr>';
		}
		else
		{
			//echo "<tr><td align=\"center\" colspan=\"7\"><input type=\"submit\"  onclick='return confirm(\"".$strings['deleteconfirm'][$lang]."\")' name=\"delete_but\" value=\" ".$strings['btndel'][$lang]." \" /></td></tr>";
		}
?>
    </table>
    </form>
<?php
	}
	
	if( $this->viewMode == "editvideo" )
	{				
		$videos = $this->pgsModel->pageLib()->Page_VideoInfo($this->videoid);
		$video = $videos[0];
		?>
		<br />
		<center><a href="<?=$this->Page_BuildUrl("pages", "edit", "id=".$this->pageinfo['id']);?>#stvideoa">Вернуться к редактированию видео страницы</a></center>

		<h2>Редактировать информацию о видеоклипе</h2>
		<div class="dd-sep"></div>
		<form action="<?=$this->Page_BuildUrl("pages", "videosave");?>#stvideoa" method="POST" name="addfrm2" id="addfrm2" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?=$this->pageinfo['id'];?>" />
		<input type="hidden" name="videoid" value="<?=$video['id'];?>" />
		<input type="hidden" name="action" value="videoupdt" />
		<table class="tblfrm w100">
		<tr>
			<th colspan="2">Ифнормация в базе</th>
		</tr>
		<tr class="even">
			<td class="ff">Заголовок видео:</td>
			<td>
				<input type="text" name="phototitle" size="40" value="<?=$video['title'];?>" />
			</td>
		</tr>
		<tr>
			<td class="ff">Описание видео:</td>
			<td>
				<textarea name="photodescr" cols="50" rows="8"><?=$video['descr'];?></textarea>
			</td>
		</tr>
		<tr class="even">
			<td class="ff">Видео файл (*.flv):</td>
			<td>
				<input type="text" size="30" name="photofile" value="<?=$video['filename'];?>" /><input type="button" value=" Выбрать файл " onclick="javascript:MM_openBrWindow('cat_files.php?hide=1&lang=<?=$lang;?>&target=self.opener.document.addfrm2.photofile','winfiles','width=<?=$FILEMAN_POPUP_W;?>,height=<?=$FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');" />
			</td>
		</tr>
		<tr>
			<td class="ff">Или youtube код:</td>
			<td>
				<textarea cols="60" rows="6" name="photoyoutube"><?=$video['tube_code'];?></textarea>
			</td>
		</tr>
		<tr class="even">
			<td class="ff">Порядковый номер:</td>
			<td>
				<input type="text" name="photoind" size="2" value="<?=$video['sort_num'];?>" />
			</td>
		</tr>
		</table>
		<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-save.png" alt=" Сохранить изменения " /></div>
		</form>
<?php
	}
	if( $this->viewMode == "editphoto" )
	{
		$photos = $this->pgsModel->pageLib()->Page_PhotoInfo($this->photoid);
		$photo = $photos[0];
		/*
		$photo = Array();
		
		$query = "SELECT p1.*, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
		FROM $TABLE_PAGE_PHOTO p1
		INNER JOIN $TABLE_PAGE_PHOTO_LANGS p2 ON p1.id=p2.item_id AND p2.lang_id='$LangId'
		WHERE p1.id=$photoid";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object( $res ) )
			{
				$pi = Array();
		
				$pi['id'] = $row->id;
				$pi['alt'] = stripslashes($row->title);
				$pi['descr'] = stripslashes($row->descr);
				$pi['snum'] = $row->sort_num;
		
				$pi['pic'] = "../".stripslashes($row->filename);
				$pi['pic_w'] = $row->src_w;
				$pi['pic_h'] = $row->src_h;
		
				$pi['thumb'] = "../".stripslashes($row->filename_thumb);
				$pi['thumb_w'] = $row->thumb_w;
				$pi['thumb_h'] = $row->thumb_h;
		
				$pi['ico'] = "../".stripslashes($row->filename_ico);
				$pi['ico_w'] = $row->ico_w;
				$pi['ico_h'] = $row->ico_h;
		
				$pi['date'] = sprintf("%02d.%02d.%04d", $row->dd, $row->dm, $row->dy);
		
				$photo = $pi;
			}
			mysql_free_result( $res );
		}
		*/
		?>
		<br />
		<center><a href="<?=$this->Page_BuildUrl("pages", "edit", "id=".$this->pageinfo['id']);?>#stphotoa">Вернуться к редактированию фото страницы</a></center>

		<h2>Редактировать Фото Товара</h2>
		<div class="dd-sep"></div>
		<form action="<?=$this->Page_BuildUrl("pages", "photosave");?>#stphotoa" method="POST" name="addfrm" id="addfrm" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?=$this->pageinfo['id'];?>" />
		<input type="hidden" name="photoid" value="<?=$this->photoid;?>" />
		<input type="hidden" name="action" value="photoupd" />
		<table class="tblfrm w100">
		<tr>
			<th colspan="2">Текущее фото</th>
		</tr>
		<tr class="even">
			<td class="ff">Фото:</td>
			<td><img src="<?=$photo['ico'];?>" alt="" /></td>
		</tr>
		<tr>
			<th colspan="2">Изменить данные</th>
		</tr>
		<tr class="even">
			<td class="ff">Заголовок для фото:</td>
			<td><input type="text" name="phototitle" size="40" value="<?=$photo['title'];?>" /></td>
		</tr>
		<tr>
			<td class="ff">Описание фото:</td>
			<td><textarea name="photodescr" cols="50" rows="5"><?=$photo['descr'];?></textarea></td>
		</tr>
		<tr class="even">
			<td class="ff">Порядковый номер:</td>
			<td>
				<input type="text" name="photoind" size="2" maxlength="2" value="<?=$photo['sort_num'];?>" />
			</td>
		</tr>
		</table>
		<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-save.png" alt=" Сохранить изменения " /></div>
		</form>
		<br />
<?php
	}
	else
	{	

	$req = $this->pageinfo;
?>
	<br /><br />
	<h2><?=( $this->viewMode == "editpage" ? $strings["hdredit"][$lang] : $strings["hdradd"][$lang] );?></h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->page_BuildUrl("pages", ( $this->viewMode == "editpage" ? "save" : "add" ));?>" method="post">
	<input type="hidden" name="action" value="<?=( $this->viewMode == "editpage" ? "save" : "add" );?>" />
<?php
	if( $this->viewMode == "editpage" )
		echo '<input type="hidden" name="id" value="'.$req['id'].'" />';
?>
	<table class="tblfrm w100">
	<tr>
		<th colspan="2">Основные параметры</th>
	</tr>
	<tr class="even">
		<td class="ff">Куда вставлять: </td>
	    <td>
	    	<select name="parentid">
	    		<option value="0">--- Корневой раздел ---</option>
<?php
	$this->comboPagesList($req['parent_id']);
?>
	    	</select>
	    </td>
	</tr>
	<tr>
		<td class="ff">Имя файла: </td>
	    <td><input type="text" name="pfile" size="20" value="<?=$req['pfile'];?>" /> <span>Имя файла без расширения</span></td>
	</tr>
	<tr class="even">
		<td class="ff">Назначение: </td>
	    <td><input type="text" name="pmean" size="60" value="<?=$req['pmean'];?>" /></td>
	</tr>
	<tr>
		<td class="ff">Title: </td>
	    <td><input type="text" name="ptitle" class="fw" value="<?=$req['seo_title'];?>" /></td>
	</tr>
	<tr class="even">
		<td class="ff">Keywords: </td>
	    <td><input type="text" name="pkey" class="fw" value="<?=$req['seo_keyw'];?>" /></td>
	</tr>
	<tr>
		<td class="ff">Description: </td>
	    <td><textarea name="pdescr" class="fw" rows="3"><?=$req['seo_descr'];?></textarea></td>
	</tr>
	<tr>
		<th colspan="2">Контент страницы</th>
	</tr>
	<tr class="even">
		<td class="ff"><?=$strings["rowtitle"][$lang];?>: </td>
	    <td><input type="text" name="title" size="70" value="<?=$req['title'];?>" /></td>
	</tr>
	<tr>
		<td class="ff"><?=$strings["rowheader"][$lang];?>: </td>
	    <td><textarea class="ckeditor" name="header" cols="70" rows="10"><?=$req['header'];?></textarea></td>
	</tr>
	<tr class="even">
		<td class="ff"><?=$strings["rowfooter"][$lang];?>: </td>
	    <td><textarea class="ckeditor" name="content" cols="70" rows="10"><?=$req['content'];?></textarea></td>
	</tr>
    <tr>
		<th colspan="2">Настройки страницы</th>
	</tr>
	
	<tr class="even">
		<td class="ff">Показывать в меню:</td>
		<td>
			<select name="menushow">
				<option value="0"<?=($req['show_in_menu'] == 0 ? " selected" : "");?>>Нет</option>
				<option value="1"<?=($req['show_in_menu'] == 1 ? " selected" : "");?>>Верхнее меню</option>
				<option value="2"<?=($req['show_in_menu'] == 2 ? " selected" : "");?>>Левое меню</option>
				<option value="3"<?=($req['show_in_menu'] == 3 ? " selected" : "");?>>Только для зарегистрированных</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class="ff">Тип записи страницы:</td>
		<td>
			<select name="pagetype">
				<option value="0"<?=($req['page_record_type'] == 0 ? " selected" : "");?>>Не ссылка (заглавие подгруппы)</option>
				<option value="1"<?=($req['page_record_type'] == 1 ? " selected" : "");?>>Обычная страница</option>
				<option value="2"<?=($req['page_record_type'] == 2 ? " selected" : "");?>>Ссылка на подкаталог</option>
				<option value="3"<?=($req['page_record_type'] == 3 ? " selected" : "");?>>Страница из базы</option>
				<option value="4"<?=($req['page_record_type'] == 4 ? " selected" : "");?>>Прямая ссылка</option>
			</select>
		</td>
	</tr>
	<tr class="even">
		<td class="ff">Показывать HTML редактор:</td>
		<td>
			<select name="withedit">
				<option value="0"<?=($req['with_editor'] == 0 ? " selected" : "");?>>Нет</option>
				<option value="1"<?=($req['with_editor'] == 1 ? " selected" : "");?>>Да</option>
			</select>
		</td>
	</tr>
	<tr>
		<td class="ff">Порядковый номер:</td>
	    <td><input type="text" name="psort" size="2" value="<?=$req['sort_num'];?>" /></td>
	</tr>
	</table>
	<div class="frmbtn"><input type="image" src="<?=( $this->viewMode == "editpage" ? WWWHOST.'admin/img/btn-save.png' : WWWHOST.'admin/img/btn-page-add.png' );?>" alt="<?=$strings["btnapply"][$lang];?>" /></div>
	</form>
<?php

	if( $this->viewMode == "editpage" )
	{
?>
	<br />

	<div style="margin: 0 150px;">
<?php
		//$allres = GetResources($LangId);
		//$allres = Resources_Get($LangId);
		$allres = $this->pgsModel->pageBasic()->get_txtres()->Resources_List();
		
		//$pageres = $this->pgsModel->get_txtres()->Resources_ForPageList($req['id']);
		//$pageres = $this->pageinfo->Page_GetResource($req['id']);
		$pageres = $this->pgsModel->pageLib()->Page_GetResource($req['id']);

		/*
		$pageres = Array();
		$query = "SELECT pr1.id as assid, pr1.display_type, pr1.sort_num, r1.*, r2.content
			FROM $TABLE_PAGE_RESOURCES pr1
			INNER JOIN $TABLE_RESOURCE r1 ON pr1.item_id=r1.id
			INNER JOIN $TABLE_RESOURCE_LANGS r2 ON r1.id=r2.item_id AND r2.lang_id='$LangId'
			WHERE pr1.page_id='$id'
			ORDER BY pr1.display_type,pr1.sort_num";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object( $res ) )
			{
				$ri = Array();
				$ri['id'] = $row->id;
				$ri['assignid'] = $row->assid;
				$ri['disp'] = $row->display_type;
				$ri['sort'] = $row->sort_num;
				$ri['name'] = stripslashes($row->name);
				$ri['title'] = stripslashes($row->title);
				$ri['text'] = stripslashes($row->content);
				$pageres[] = $ri;
			}
			mysql_free_result( $res );
		}
		*/
?>
		<a name="stblka"></a>
		<h2>Текстовые блоки на странице</h2>
		<div class="dd-sep"></div>
		<table class="tbldat">
		<tr>
			<th>Где показывать</th>
			<th>№ сорт.</th>
			<th>Название блока</th>
			<th> &nbsp; </th>
		</tr>
<?php
		if( count($pageres) > 0 )
		{
			for($i=0; $i<count($pageres); $i++)
			{
				$disp_str = "";
				switch( $pageres[$i]['display_type'] )
				{
					case 1:
						$disp_str = "Левая колонка";
						break;
					case 2:
						$disp_str = "Правая колонка";
						break;
				}

				echo '<tr'.($i % 2 == 0 ? ' class="even"' : '').'>
					<td>'.$disp_str.'</td>
					<td>'.$pageres[$i]['sort_num'].'</td>
					<td>'.stripslashes($pageres[$i]['title']).'</td>
					<td class="ls">
						<a href="'.$this->Page_BuildUrl("pages", "resunassign", "action=resdel&id=".$req['id']."&resid=".$pageres[$i]['assignid']).'#stblka"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="Удалить блок с этой страницы" /></a>
					</td>
				</tr>';
			}
		}
		else
		{
			echo '<tr><td colspan="4" align="center">Нет прикрепленных текстовых блоков</td></tr>';
		}
?>
		</table>

		<h2>Добавить блок</h2>
		<div class="dd-sep"></div>
		<form action="<?=$this->Page_BuildUrl("pages", "resadd");?>#stblka" method="POST" name="addfrm" id="addfrm">
		<input type="hidden" name="id" value="<?=$req['id'];?>" />
		<input type="hidden" name="action" value="addres" />

		<table class="tblfrm w100">
		<tr>
			<th colspan="2">Привязать текстовый блок к странице</th>
		</tr>
		<tr class="even">
			<td class="ff">Где показывать:</td>
			<td>
				<select name="resdisp">
					<option value="1">Левая колонка</option>
					<option value="2">Правая колонка</option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="ff">Текстовый блок:</td>
			<td>
				<select name="resblock">
				<?php
					foreach($allres as $kres=>$vres)
					{
						echo '<option value="'.$vres['id'].'">'.$vres['title'].'</option>';
					}
				?>
				</select>
			</td>
		</tr>
		<tr class="even">
			<td class="ff">Порядковый номер:</td>
			<td><input type="text" name="resind" size="2" maxlength="2" /></td>
		</tr>
		</table>
		<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-add-new.png" alt=" Добавить блок " /></div>
		</form>
<?php
		$photos = $this->pgsModel->pageLib()->Page_Photo($req['id']);

		/*
		$photos = Array();

		$query = "SELECT p1.*, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM $TABLE_PAGE_PHOTO p1
			INNER JOIN $TABLE_PAGE_PHOTO_LANGS p2 ON p1.id=p2.item_id AND p2.lang_id='$LangId'
			WHERE p1.item_id=$id
			ORDER BY p1.sort_num,p1.add_date";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object( $res ) )
			{
				$pi = Array();

				$pi['id'] = $row->id;
				$pi['alt'] = stripslashes($row->title);
				$pi['snum'] = $row->sort_num;

				$pi['pic'] = "../".stripslashes($row->filename);
				$pi['pic_w'] = $row->src_w;
				$pi['pic_h'] = $row->src_h;

				$pi['thumb'] = "../".stripslashes($row->filename_thumb);
				$pi['thumb_w'] = $row->thumb_w;
				$pi['thumb_h'] = $row->thumb_h;

				$pi['ico'] = "../".stripslashes($row->filename_ico);
				$pi['ico_w'] = $row->ico_w;
				$pi['ico_h'] = $row->ico_h;

				$pi['date'] = sprintf("%02d.%02d.%04d", $row->dd, $row->dm, $row->dy);

				$photos[] = $pi;
			}
			mysql_free_result( $res );
		}
		*/
?>
		<a name="stphotoa"></a>
		<h2>Фотографии на странице</h2>
		<div class="dd-sep"></div>
		<table class="tblfrm">
<?php
		if( count($photos) > 0 )
		{
			$COLS = 3;
			for($i=0; $i<count($photos); $i++)
			{
				if( ($i+1) % $COLS == 1 )
					echo "<tr>";

				echo '<td class="picit">
					<div class="brd">
						<div style="padding-bottom: 5px;">
							<a href="'.$this->Page_BuildUrl("pages", "photodel", 'id='.$req['id'].'&action=photodel&photoid='.$photos[$i]['id']).'#stphotoa" onclick="return confirm(\'Вы действительно хотите удалить это фото?\')" title="Удалить фото"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="Удалить фото" /></a>&nbsp;
		     				<a href="'.$this->Page_BuildUrl("pages", "photoedit", 'id='.$req['id'].'&action=photoed&photoid='.$photos[$i]['id']).'" title="Редактировать фото"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="Редактировать фото" /></a>&nbsp;
		     			</div>
		     			<img src="'.$photos[$i]['ico'].'" width="'.$photos[$i]['ico_w'].'" height="'.$photos[$i]['ico_h'].'" alt="'.$photos[$i]['title'].'" />
		     			<div style="padding: 3px 0px 5px 0px;">
		     				<a href="'.$this->Page_BuildUrl("pages", "photominus", 'id='.$req['id'].'&action=minspic&photoid='.$photos[$i]['id']).'#stphotoa"><img src="'.WWWHOST.'admin/img/a-minus.png" alt="Уменьшить порядковый номер" /></a>
							&nbsp;
							№ сорт. '.$photos[$i]['sort_num'].'
							&nbsp;
							<a href="'.$this->Page_BuildUrl("pages", "photoplus", 'id='.$req['id'].'&action=pluspic&photoid='.$photos[$i]['id']).'#stphotoa"><img src="'.WWWHOST.'admin/img/a-plus.png" alt="Увеличить порядковый номер" /></a>
						</div>
	     			</div>
				</td>';

				if( ($i+1) % $COLS == 0 )
					echo "</tr>";
			}

			if( ($i+1) % $COLS != 0 )
			{
				echo "</tr>";
			}
		}
		else
		{
			echo "<tr><td align=\"center\">Нет фотографий у данной страницы</td></tr>";
		}
?>
		</table>
		<br />

		<h2>Добавить фотографию</h2>
		<div class="dd-sep"></div>
		<form action="<?=$this->Page_BuildUrl("pages", "photoadd");?>#stphotoa" method="POST" name="addfrm" id="addfrm" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?=$req['id'];?>" />
		<input type="hidden" name="action" value="addphoto" />
		<table class="tblfrm w100">
		<tr>
			<th colspan="2">Добавить фото в галерею</th>
		</tr>
		<tr class="even">
			<td class="ff">Заголовок для фото:</td>
			<td><input type="text" name="phototitle" size="40" /></td>
		</tr>
		<tr>
			<td class="ff">Описание фото:</td>
			<td><textarea name="photodescr" cols="50" rows="5"></textarea></td>
		</tr>
		<tr class="even">
			<td class="ff">Фото файл (*.jpg, *.png, *.gif):</td>
			<td><input type="file" name="photofile" /></td>
		</tr>
		<tr>
			<td class="ff">Порядковый номер:</td>
			<td><input type="text" name="photoind" size="2" maxlength="2" /></td>
		</tr>
		</table>
		<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-add-new.png" alt=" Добавить фото " /></div>
        <?php
        /*
		<table align="center" width="550" cellspacing="0" cellpadding="1" border="0" class="tableborder">
			<tr><td>
		        <table width="100%" cellspacing="1" cellpadding="1" border="0">
		        <tr>
					<td colspan="2" class="fh">Добавить фото в галерею</td>
				</tr>
		        <tr>
					<td class="ff">Заголовок для фото:</td>
					<td class="fr">
		                <input type="text" name="phototitle" size="40" />
					</td>
				</tr>
				<tr>
					<td class="ff">Описание фото:</td>
					<td class="fr">
		                <textarea name="photodescr" cols="50" rows="5"></textarea>
					</td>
				</tr>
				<tr>
					<td class="ff">Фото файл (*.jpg, *.png, *.gif):</td>
					<td class="fr">
		                <input type="file" name="photofile" />
					</td>
				</tr>
				<tr>
					<td class="ff">Порядковый номер:</td>
					<td class="fr">
		                <input type="text" name="photoind" size="2" />
					</td>
				</tr>
				<tr>
				<td colspan="2" class="fr" align="center"><input type="submit" name="addnewbut" value="Добавить" /></td>
				</tr>
				</table>
			</td></tr>
			</table>
		*/
		?>
		</form>
<?php
		$video = $this->pgsModel->pageLib()->Page_Video($req['id']);
		
		/*
		$video = Array();

		$query = "SELECT p1.*, p2.title, p2.descr, YEAR(p1.add_date) as dy, MONTH (p1.add_date) as dm, DAYOFMONTH(p1.add_date) as dd
			FROM $TABLE_PAGE_VIDEO p1
			INNER JOIN $TABLE_PAGE_VIDEO_LANGS p2 ON p1.id=p2.item_id AND p2.lang_id='$LangId'
			WHERE p1.item_id=$id
			ORDER BY p1.sort_num,p1.add_date";
		if( $res = mysql_query( $query ) )
		{
			while( $row = mysql_fetch_object( $res ) )
			{
				$pi = Array();

				$pi['id'] = $row->id;
				$pi['alt'] = stripslashes($row->title);
				$pi['snum'] = $row->sort_num;

				$pi['clip'] = stripslashes($row->filename);
				$pi['clip_w'] = $row->src_w;
				$pi['clip_h'] = $row->src_h;

				$pi['ico'] = stripslashes($row->filename_ico);
				$pi['ico_w'] = $row->ico_w;
				$pi['ico_h'] = $row->ico_h;

				$pi['tubecode'] = stripslashes($row->tube_code);

				$pi['date'] = sprintf("%02d.%02d.%04d", $row->dd, $row->dm, $row->dy);

				$video[] = $pi;
			}
			mysql_free_result( $res );
		}
		*/
?>
		<a name="stvideoa"></a>
		<h2>Видео на странице</h2>
		<div class="dd-sep"></div>
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
<?php
		if( count($video) > 0 )
		{
			for($i=0; $i<count($video); $i++)
			{
				echo "<tr>";
				echo "<td align=\"center\">";
				
     			if( trim($video[$i]['tube_code']) != "" )
				{
					echo '<div style="padding: 0px 0px 0px 0px;  text-align: center;">'.html_entity_decode(trim($video[$i]['tube_code']), ENT_COMPAT).'</div>';
				}
				else
				{
					//echo '<embed src="'.$FILE_DIR.$orglogo.'" style="width: 300px;" loop="false" autostart="false" volume="25" hidden="false"></embed>';
?>
		<div id='mediaspace<?=$i;?>'>This text will be replaced</div>

		<script type='text/javascript'>
		  var so<?=$i;?> = new SWFObject('../player.swf','ply','310','270','9','#ffffff');
		  so<?=$i;?>.addParam('allowfullscreen','true');
		  so<?=$i;?>.addParam('allowscriptaccess','always');
		  so<?=$i;?>.addParam('wmode','opaque');
		  so<?=$i;?>.addVariable('file','<?=(WWWHOST.FILE_DIR.$video[$i]['filename']);?>');
		  so<?=$i;?>.write('mediaspace<?=$i;?>');
		</script>
		<br />
<?php
				}

				//echo "<a href=\"$PHP_SELF?item_id=$item_id&action=photodel&photoid=".$photos[$i]['id']."\">Удалить</a><br />";
				echo '</td>
				'.( false ? '<td><img src="'.$video[$i]['filename_ico'].'" width="'.$video[$i]['ico_w'].'" height="'.$video[$i]['ico_h'].'" style="border: 1px solid #b0b0b0;" alt="'.$video[$i]['title'].'" /></td>' : '' ).'
				<td style="padding: 3px 0px 5px 10px;">
					<b>Название:</b><br />
					'.$video[$i]['title'].'<br /><br />
					№ сорт. '.$video[$i]['sort_num'].'<br /><br />
					<a href="'.$this->Page_BuildUrl("pages", "videodel", 'id='.$req['id'].'&action=videodel&videoid='.$video[$i]['id']).'#stvideoa" onclick="return confirm(\'Вы действительно хотите удалить это видео?\')" title="Удалить видео"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="Удалить видео" /></a>&nbsp;
     				<a href="'.$this->Page_BuildUrl("pages", "videoedit", 'id='.$req['id'].'&action=videoed&videoid='.$video[$i]['id']).'" title="Редактировать видео"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="Редактировать видео" /></a>&nbsp;
     			</td>
     			</tr>';
			}
		}
		else
		{
			echo "<tr><td colspan=\"2\" align=\"center\">Нет видеоклипов у данной страницы</td></tr>";
		}
?>
		</table>
		<br />
		
		<h2>Добавить видеоклип</h2>
		<div class="dd-sep"></div>
		<form action="<?=$this->Page_BuildUrl("pages", "videoadd");?>#stvideoa" method="POST" name="addfrm2" id="addfrm2" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?=$req['id'];?>" />
		<input type="hidden" name="action" value="addvideo" />
		<table class="tblfrm w100">
		<tr>
			<th colspan="2">Добавить видео</th>
		</tr>
		<tr class="even">
			<td class="ff">Заголовок видео:</td>
			<td><input type="text" name="phototitle" size="40" /></td>
		</tr>
		<tr>
			<td class="ff">Описание видео:</td>
			<td><textarea name="photodescr" cols="50" rows="8"></textarea></td>
		</tr>
		<tr class="even">
			<td class="ff">Видео файл (*.flv):</td>
			<td>
				<input type="text" size="30" name="photofile" /><input type="button" value=" Выбрать файл " onclick="javascript:MM_openBrWindow('<?=$this->Page_BuildUrl("fileman", "", 'full=0&hide=1&lang='.$lang.'&target=self.opener.document.addfrm2.photofile');?>','winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');" />
			</td>
		</tr>
		<tr>
			<td class="ff">Или youtube код:</td>
			<td><textarea cols="60" rows="6" name="photoyoutube"></textarea></td>
		</tr>
		<tr class="even">
			<td class="ff">Порядковый номер:</td>
			<td><input type="text" name="photoind" size="2" /></td>
		</tr>
		</table>
		<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-add-new.png" alt=" Добавить видео " /></div>
		</form>

	</div>

<?php
	}
	
	}


	//include ("inc/footer-inc.php");
	//include ("../inc/close-inc.php");
?>