<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	//include "../inc/utils-inc.php";
	//include "../inc/countryutils-inc.php";
	
	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();
	
	$UHCMS_MODNAME = "user_list";

	$fields['name']['ru'] = 'Ф.И.О.';
	$fields['login']['ru'] = 'Логин';
	$fields['passwd']['ru'] = 'Пароль';
	$fields['address']['ru'] = 'Адрес';
	$fields['city']['ru'] = 'Город';
	$fields['country']['ru'] = 'Страна';
	$fields['zip_code']['ru'] = 'Почтовый Индекс';
	$fields['telephone']['ru'] = 'Тел.';
	$fields['office_phone']['ru'] = 'Раб.';
	$fields['cell_phone']['ru'] = 'Моб.';
	$fields['email1']['ru'] = 'E-Mail';
	$fields['email2']['ru'] = 'E-Mail';
	$fields['email3']['ru'] = 'E-Mail';
	$fields['web_url']['ru'] = 'Веб-страница';
	$fields['groupid']['ru'] = 'Группа пользователей';
	$fields['trader']['ru'] = 'Привязка к трейдеру';

	$fields['name']['en'] = 'Full Name';
	$fields['login']['en'] = 'Login';
	$fields['passwd']['en'] = 'Password';
	$fields['address']['en'] = 'Address';
	$fields['city']['en'] = 'City';
	$fields['country']['en'] = 'Country';
	$fields['zip_code']['en'] = 'Zip';
	$fields['telephone']['en'] = 'Telephone';
	$fields['office_phone']['en'] = 'Office Phone';
	$fields['cell_phone']['en'] = 'Cell Phone';
	$fields['email1']['en'] = 'E-Mail 1';
	$fields['email2']['en'] = 'E-Mail 2';
	$fields['email3']['en'] = 'E-Mail 3';
	$fields['web_url']['en'] = 'Web-url';
	$fields['groupid']['en'] = 'User Group';
	$fields['trader']['en'] = 'Trader Assign';

	$strings['editprof']['en'] = "Edit user profile";
	$strings['newprof']['en'] = "Add user profile";
	$strings['userlist']['en'] = "User List";
	$strings['addbtn']['en'] = " Add New ";
	$strings['applybtn']['en'] = " Apply ";
	$strings['rowlogin']['en'] = "Login";
	$strings['rowname']['en'] = "Name";
	$strings['rowaddr']['en'] = "Address";
	$strings['rowcontact']['en'] = "Contacts";
	$strings['tipchangepwd']['en'] = "Change Password";
	$strings['tipedituser']['en'] = "Edit User Profile";
	$strings['tipdeluser']['en'] = "Delete User";
	$strings['tipstatus']['en'] = "Change Status";

	$strings['editprof']['ru'] = "Редактирование Профиля Пользователя";
	$strings['newprof']['ru'] = "Добавление Нового Пользователя";
	$strings['userlist']['ru'] = "Список Пользователей";
	$strings['addbtn']['ru'] = " Добавить ";
	$strings['applybtn']['ru'] = " Применить ";
	$strings['rowlogin']['ru'] = "Пользователи";
	$strings['rowname']['ru'] = "Ф.И.О.";
	$strings['rowaddr']['ru'] = "Адрес";
	$strings['rowcontact']['ru'] = "Контакты";
	$strings['tipchangepwd']['ru'] = "Изменить Пароль";
	$strings['tipedituser']['ru'] = "Редактировать Профиль";
	$strings['tipdeluser']['ru'] = "Удалить Пользователя";
	$strings['tipstatus']['ru'] = "Изменить Статус";

	////////////////////////////////////////////////////////////////////////////
	
	$citylist = Array();	//GetCityList($LangId);

	$usergroups = $this->userModel->get_groupList();
	

	////////////////////////////////////////////////////////////////////////////
	$action = GetParameter("action", "");
	$uid = GetParameter("uid", "");

	$uprof = Array();

	$uprof['name'] = GetParameter("fullname", "");
	$uprof['passwd'] = GetParameter("passwd", "");
	$uprof['login'] = GetParameter("login", "");
	$uprof['address'] = GetParameter("address", "");
	$uprof['city_id'] = GetParameter("city_id", 0);
	$uprof['country'] = GetParameter("country", "");
	$uprof['zip_code'] = GetParameter("zip", "");
	$uprof['telephone'] = GetParameter("phone1", "");
	$uprof['office_phone'] = GetParameter("phone2", "");
	$uprof['cell_phone'] = GetParameter("phone3", "");
	$uprof['email1'] = GetParameter("email1", "");
	$uprof['email2'] = GetParameter("email2", "");
	$uprof['email3'] = GetParameter("email3", "");
	$uprof['web_url'] = GetParameter("weburl", "");
	$uprof['groupid'] = GetParameter("userlevel", "0");
	$uprof['usertrader'] = GetParameter("usertrader", 0);

	/*
	switch( $action )
	{
		case "add":
			if( $UserGroup == $GROUP_ADMIN )
  			{
  				$newuid = makeUuid();
        		if( !mysql_query("INSERT INTO $TABLE_USERS
        							(login, passwd, activation_code, isactive, name, address, city_id, zip_code, telephone,
        							office_phone, cell_phone, email1, email2, email3, web_url, group_id)
	                        VALUES ('".addslashes($uprof['login'])."', PASSWORD('".addslashes($uprof['passwd'])."'), '$newuid', 1, '".addslashes($uprof['name'])."',
	                        '".addslashes($uprof['address'])."', '".addslashes($uprof['city_id'])."', '".addslashes($uprof['zip_code'])."',
	                        '".addslashes($uprof['telephone'])."', '".addslashes($uprof['office_phone'])."', '".addslashes($uprof['cell_phone'])."',
	                        '".addslashes($uprof['email1'])."', '".addslashes($uprof['email2'])."', '".addslashes($uprof['email3'])."',
	                        '".addslashes($uprof['web_url'])."', '".$uprof['groupid']."')"))
            	{
	                echo "<b>".mysql_error()."</b>";
	            }
  			}
			else
  			{
  				header("Location: forbidden.php");
  				exit;
  			}
			break;

		case "status":
  			if(isset($_GET['active']))		$active = $_GET['active'];
  			if(empty($uid))					$uid = $_GET['uid'];

			if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
			{
				echo mysql_error();
			}
  			break;

		case "delete":
			if(empty($uid))			$uid = $_GET['uid'];
			if( $uid == $UserId )	break;

			if(mysql_query("DELETE FROM $TABLE_USERS WHERE id='$uid' AND id<>1"))
			{
				// Make user records cleaning
			}
			else
			{
				echo mysql_error();
			}
  			break;

		case "applynew":
			if( !mysql_query("UPDATE $TABLE_USERS SET login='".addslashes($uprof['login'])."',
						name='".addslashes($uprof['name'])."', address='".addslashes($uprof['address'])."',
						city_id='".addslashes($uprof['city_id'])."',
        				zip_code='".addslashes($uprof['zip_code'])."', telephone='".addslashes($uprof['telephone'])."',
        				office_phone='".addslashes($uprof['office_phone'])."', cell_phone='".addslashes($uprof['cell_phone'])."',
        				email1='".addslashes($uprof['email1'])."', email2='".addslashes($uprof['email2'])."',
        				email3='".addslashes($uprof['email3'])."', web_url='".addslashes($uprof['web_url'])."',
        				group_id='".$uprof['groupid']."' WHERE id=".$_POST['uid']." "))
			{
				echo "<b>".mysql_error()."</b>";
			}

			$action = "editprofile";
			$uid = $_POST['uid'];
			break;
	}
	*/

	if( $this->viewMode == "edituser" )
	{
		if( $this->userModel->ses->UserGroup == GROUP_ADMIN )
		{
			$uprof = $this->userinfo;
		}
		
		/*
		Breadcrumbs_Add( $PHP_SELF, $strings['userlist'][$lang] );

		if( $UserGroup != $GROUP_ADMIN )
		{
			break;
		}

		$query = "SELECT g.id as g_id, g.group_name, u.id as id, u.login as login, u.isactive, u.name, u.address, u.city_id, u.zip_code,
			u.telephone, u.office_phone, u.cell_phone, u.email1, u.email2, u.email3, u.web_url
			FROM $TABLE_USERS u, $TABLE_USER_GROUPS g
			WHERE u.group_id=g.id AND u.id='".$uid."' ";
		if( $res = mysql_query($query) )
		{
			if( $row = mysql_fetch_object($res) )
			{
				$uprof['name'] = stripslashes($row->name);
				$uprof['login'] = stripslashes($row->login);
				$uprof['address'] = stripslashes($row->address);
				$uprof['city_id'] = $row->city_id;
				//$uprof['country'] = stripslashes($row->country);
				$uprof['zip_code'] = stripslashes($row->zip_code);
				$uprof['telephone'] = stripslashes($row->telephone);
				$uprof['cell_phone'] = stripslashes($row->cell_phone);
				$uprof['office_phone'] = stripslashes($row->office_phone);
				$uprof['email1'] = stripslashes($row->email1);
				$uprof['email2'] = stripslashes($row->email2);
				$uprof['email3'] = stripslashes($row->email3);
				$uprof['web_url'] = stripslashes($row->web_url);
				$uprof['groupid'] = $row->g_id;
			}
			mysql_free_result($res);
		}
		*/
	}

	// Include Top Header HTML Style
	//include "inc/header-inc.php";
	
	echo $this->viewMode;
?>
	<br /><br />
    <h2><?=($this->viewMode == "edituser" ? $strings['editprof'][$lang] : $strings['newprof'][$lang]);?></h2>
	<div class="dd-sep"></div>
	<form class="w600-frm" action="<?=( $this->viewMode == "edituser" ? $this->page_BuildUrl("users", "saveuser") : $this->page_BuildUrl("users", "adduser") );?>" method="post">
	<input type="hidden" name="action" value="<?=($this->viewMode == "edituser" ? "applynew" : "add");?>" />
	<?=( $this->viewMode == "edituser" ? '<input type="hidden" name="uid" value="'.$uid.'" />' : '' );?>
	<table class="tblfrm">
	<tr>
		<th colspan="2">Данные для авторизации</th>
	</tr>
	<tr class="even">
	    <td class="ff"><?=$fields['groupid'][$lang];?>:</td>
	    <td>
	    	<select name="userlevel">
	    	<?php
	    		for( $i = 0; $i < count($usergroups); $i++ )
	    		{
                	if($usergroups[$i]['id'] == GROUP_ADMIN)
                	{
                		if( $this->userModel->ses->UserGroup == GROUP_ADMIN )
                		{
	                    	if( $uprof['group_id'] == GROUP_ADMIN )    	echo "<option value=\"".$usergroups[$i]['id']."\" selected>".$usergroups[$i]['group_name']."</option>";
	                    	else                            			echo "<option value=\"".$usergroups[$i]['id']."\">".$usergroups[$i]['group_name']."</option>";
	                    }
	                }
	                else
	                {
                        if( $uprof['group_id'] == $usergroups[$i]['id'] )	echo "<option value=\"".$usergroups[$i]['id']."\" selected>".$usergroups[$i]['group_name']."</option>";
	                    else											echo "<option value=\"".$usergroups[$i]['id']."\">".$usergroups[$i]['group_name']."</option>";
                    }
	            }
	    	?>
	    	</select>
	    </td>
	</tr>
	<tr>
	    <td class="ff"><?=$fields['login'][$lang];?>:</td>
	    <td><input name="login" type="text" value="<?=$uprof['login'];?>" /></td>
	</tr>
<?php
	if($this->viewMode != "edituser")
	{
?>
    <tr class="even">
	    <td class="ff"><?=$fields['passwd'][$lang];?>:</td>
	    <td><input name="passwd" type="password" value="" /></td>
	</tr>
<?php
	}
?>
	<tr>
		<th colspan="2">Общие сведения</th>
	</tr>
	<tr class="even">
	    <td class="ff"><?=$fields['name'][$lang];?>:</td>
	    <td><input class="fw" name="fullname" type="text" value="<?=$uprof['name'];?>" /></td>
	</tr>
    <tr>
	    <td class="ff"><?=$fields['address'][$lang];?>:</td>
	    <td><input class="fw" name="address" type="text" value="<?=$uprof['address'];?>" /></td>
	</tr>
    <tr>
	    <td class="ff"><?=$fields['city'][$lang];?>:</td>
	    <td>
	    	<select name="city_id">
	<?php
		for($i=0; $i<count($citylist); $i++)
		{
        	echo "<option value=\"".$citylist[$i]['cityid']."\"".( $citylist[$i]['cityid'] == $uprof['city_id'] ? " selected " : "" ).">".$citylist[$i]['city']."</option>";
        }
	?>
	    	</select>
		</td>
	</tr>
    <tr class="even">
	    <td class="ff"><?=$fields['zip_code'][$lang];?>:</td>
	    <td><input name="zip" type="text" value="<?=$uprof['zip_code'];?>" /></td>
	</tr>
	<tr>
		<th colspan="2">Контактная информация</th>
	</tr>
    <tr>
	    <td class="ff"><?=$fields['telephone'][$lang];?>:</td>
	    <td><input name="phone1" type="text" value="<?=$uprof['telephone'];?>" /></td>
	</tr>
    <tr class="even">
	    <td class="ff"><?=$fields['office_phone'][$lang];?>:</td>
	    <td><input name="phone2" type="text" value="<?=$uprof['office_phone'];?>" /></td>
	</tr>
    <tr>
	    <td class="ff"><?=$fields['cell_phone'][$lang];?>:</td>
	    <td><input name="phone3" type="text" value="<?=$uprof['cell_phone'];?>" /></td>
	</tr>
    <tr class="even">
	    <td class="ff"><?=$fields['email1'][$lang];?>:</td>
	    <td><input name="email1" type="text" value="<?=$uprof['email1'];?>" /></td>
	</tr>
    <tr>
	    <td class="ff"><?=$fields['email2'][$lang];?>:</td>
	    <td><input name="email2" type="text" value="<?=$uprof['email2'];?>" /></td>
	</tr>
    <tr class="even">
	    <td class="ff"><?=$fields['email3'][$lang];?>:</td>
	    <td><input name="email3" type="text" value="<?=$uprof['email3'];?>" /></td>
	</tr>
    <tr>
	    <td class="ff"><?=$fields['web_url'][$lang];?>:</td>
	    <td><input name="weburl" type="text" value="<?=$uprof['web_url'];?>" /></td>
	</tr>
	</table>
<?php
	if( $this->viewMode == "edituser" )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-user-save.png" alt="Сохранить информацию о пользователе" /></div>';
	}
	else
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-user-add.png" alt="Добавить нового пользователя" /></div>';
	}
?>
	</form>


    <!-- PART OF PAGE TO DISPLAY USER'S LIST -->
    <br /><br />
	<h2><?=$strings['userlist'][$lang];?></h2>
	<table class="tbldat">
	<tr>
		<th>Пользователи</th>
		<th>Группа</th>
		<th>Ф.И.О.</th>
		<th>Адреса и контакты</th>
		<th class="ls">Действия</th>
	</tr>
    <?php
		$its = $this->userlist;
		
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			$user_city = "";
			$user_country = "";
			
			/*
			$query1 = "SELECT cl1.name as city, c2.name as country
					FROM $TABLE_CITY c1
					INNER JOIN $TABLE_CITY_LANG cl1 ON c1.id=cl1.city_id AND cl1.lang_id='$LangId'
					INNER JOIN $TABLE_COUNTRY_LANG c2 ON c1.country_id=c1.country_id AND c2.lang_id='$LangId'
					WHERE c1.id='".$row->city_id."'";
			if( $res1 = mysql_query( $query1 ) )
			{
				if( $row1 = mysql_fetch_object($res1) )
				{
					$user_city = stripslashes($row1->city);
					$user_country = stripslashes($row1->country);
				}
				mysql_free_result($res1);
			}
			*/

			$u_addr_str = "";
			if( ($row['zip_code'] != "") || ($user_country != "") || ($user_city != "") )
			{
				$u_addr_str = ',<br />'.stripslashes($row['zip_code']).' '.$user_country.', '.$user_city;
			}

			$u_cont_str = "";
			$u_cont_str .= ( trim(stripslashes($row['telephone'])) != "" ?  '<span>'.$fields['telephone'][$lang].'</span> '.stripslashes($row['telephone']).'<br />' : '' );
			$u_cont_str .= ( trim(stripslashes($row['telephone'])) != "" ?  '<span>'.$fields['office_phone'][$lang].'</span> '.stripslashes($row['office_phone']).'<br />' : '' );
			$u_cont_str .= ( trim(stripslashes($row['telephone'])) != "" ?  '<span>'.$fields['cell_phone'][$lang].'</span> '.stripslashes($row['cell_phone']).'<br />' : '' );
			$u_cont_str .= ( trim(stripslashes($row['email1'])) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row['email1']).'">'.stripslashes($row['email1']).'</a><br />' : '' );
			$u_cont_str .= ( trim(stripslashes($row['email2'])) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row['email2']).'">'.stripslashes($row['email2']).'</a><br />' : '' );
			$u_cont_str .= ( trim(stripslashes($row['email3'])) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row['email3']).'">'.stripslashes($row['email3']).'</a><br />' : '' );

			echo '<tr'.( ($i+1) % 2 == 1 ? ' class="even"' : '' ).'>
				<td class="u-act'.($row['isactive'] == NULL ? '-no' : '').'"><span>'.stripslashes($row['login']).'</span></td>
				<td>'.stripslashes($row['group_name']).'</td>
				<td class="p-name">'.stripslashes($row['name']).'</td>
				<td class="u-cont">
					<p class="u-addr">'.stripslashes($row['address']).$u_addr_str.'</p>
					<div class="u-inf">
						'.$u_cont_str.'
					</div>
					<div class="u-slide"><a class="a-slide" href="#"><span>Развернуть</span></a></div>
				</td>
				<td class="ls">
					<a href="'.$this->page_BuildUrl( "users", "status", 'uid='.$row['id'].'&active='.($row['isactive'] == NULL ? 0 : 1) ).'" title="Отключить пользователя"><img src="'.WWWHOST.'admin/img/a-usr-dis.png" width="22" height="22" alt="Отключить пользователя" /></a>&nbsp;
					<a href="javascript:NewWindow(\'change_pass.php?uid='.$row['id'].'&lang='.$lang.'\');" title="Сменить пароль"><img src="'.WWWHOST.'admin/img/a-chpass.png" width="22" height="22" alt="Сменить пароль" /></a>&nbsp;
					<a href="'.$this->page_BuildUrl( "users", "edituser", 'uid='.$row['id'] ).'" title="Редактировать"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="Редактировать" /></a>&nbsp;
					<a onclick="return confirm(\'Вы хотите удалить учетную запись пользователя. Удалить пользователя '.stripslashes($row['login']).'?\')" href="'.$this->page_BuildUrl( "users", "deleteuser", 'uid='.$row['id'] ).'" title="Удалить"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="Удалить" /></a>
				</td>
			</tr>';
		}
	
		/*
    	$cond = "";
    	if( $this->userModel->ses->UserGroup == GROUP_ADMIN )
    	{
            if( $this->userModel->ses->UserId != 1 )
            {
            	$cond = " AND u.id<>1 ";
			}
		}
		else
		{
			$cond = " AND u.id<>1 AND u.group_id<>".GROUP_ADMIN." ";
		}

		$found_items = 0;

		$query = "SELECT g.id as g_id, g.group_name, u.id as id, u.login as login, u.isactive, u.name, u.address, u.city_id, u.country, u.zip_code,
			u.telephone, u.office_phone, u.cell_phone, u.email1, u.email2, u.email3
			FROM $TABLE_USERS u, $TABLE_USER_GROUPS g
			WHERE u.group_id=g.id $cond
			ORDER BY g_id, login";
		if($res = mysql_query($query))
		{
            while( $row = mysql_fetch_object($res) )
            {
            	$found_items++;

                $user_city = "";
                $user_country = "";
                $query1 = "SELECT cl1.name as city, c2.name as country
                		FROM $TABLE_CITY c1
                		INNER JOIN $TABLE_CITY_LANG cl1 ON c1.id=cl1.city_id AND cl1.lang_id='$LangId'
                		INNER JOIN $TABLE_COUNTRY_LANG c2 ON c1.country_id=c1.country_id AND c2.lang_id='$LangId'
                		WHERE c1.id='".$row->city_id."'";
                if( $res1 = mysql_query( $query1 ) )
                {
                    if( $row1 = mysql_fetch_object($res1) )
                    {
                    	$user_city = stripslashes($row1->city);
                    	$user_country = stripslashes($row1->country);
                    }
                    mysql_free_result($res1);
                }

                $u_addr_str = "";
                if( ($row->zip_code != "") || ($user_country != "") || ($user_city != "") )
                {
                	$u_addr_str = ',<br />'.stripslashes($row->zip_code).' '.$user_country.', '.$user_city;
                }

                $u_cont_str = "";
                $u_cont_str .= ( trim(stripslashes($row->telephone)) != "" ?  '<span>'.$fields['telephone'][$lang].'</span> '.stripslashes($row->telephone).'<br />' : '' );
                $u_cont_str .= ( trim(stripslashes($row->telephone)) != "" ?  '<span>'.$fields['office_phone'][$lang].'</span> '.stripslashes($row->office_phone).'<br />' : '' );
                $u_cont_str .= ( trim(stripslashes($row->telephone)) != "" ?  '<span>'.$fields['cell_phone'][$lang].'</span> '.stripslashes($row->cell_phone).'<br />' : '' );
                $u_cont_str .= ( trim(stripslashes($row->email1)) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row->email1).'">'.stripslashes($row->email1).'</a><br />' : '' );
                $u_cont_str .= ( trim(stripslashes($row->email2)) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row->email2).'">'.stripslashes($row->email2).'</a><br />' : '' );
                $u_cont_str .= ( trim(stripslashes($row->email3)) != "" ?  '<span>E-mail: </span> <a href="mailto:'.stripslashes($row->email3).'">'.stripslashes($row->email3).'</a><br />' : '' );

                echo '<tr'.( $found_items % 2 == 1 ? ' class="even"' : '' ).'>
					<td class="u-act'.($row->isactive == NULL ? '-no' : '').'"><span>'.stripslashes($row->login).'</span></td>
					<td>'.stripslashes($row->group_name).'</td>
					<td class="p-name">'.stripslashes($row->name).'</td>
					<td class="u-cont">
						<p class="u-addr">'.stripslashes($row->address).$u_addr_str.'</p>
						<div class="u-inf">
							'.$u_cont_str.'
						</div>
						<div class="u-slide"><a class="a-slide" href="#"><span>Развернуть</span></a></div>
					</td>
					<td class="ls">
						<a href="'.$PHP_SELF.'?action=status&active='.($row->isactive == NULL ? 0 : 1).'&uid='.$row->id.'" title="Отключить пользователя"><img src="img/a-usr-dis.png" width="22" height="22" alt="Отключить пользователя" /></a>&nbsp;
						<a href="javascript:NewWindow(\'change_pass.php?uid='.$row->id.'&lang='.$lang.'\');" title="Сменить пароль"><img src="img/a-chpass.png" width="22" height="22" alt="Сменить пароль" /></a>&nbsp;
						<a href="'.$PHP_SELF.'?action=editprofile&uid='.$row->id.'" title="Редактировать"><img src="img/a-edit.png" width="22" height="22" alt="Редактировать" /></a>&nbsp;
						<a onclick="return confirm(\'Вы хотите удалить учетную запись пользователя. Удалить пользователя '.stripslashes($row->login).'?\')" href="'.$PHP_SELF.'?action=delete&uid='.$row->id.'" title="Удалить"><img src="img/a-del.png" width="22" height="22" alt="Удалить" /></a>
					</td>
				</tr>';				
            }
        	mysql_free_result($res);
        }
        else
        	echo "<b>".mysql_error()."</b>";
		*/
    ?>
	</table>

<?php
	//include "inc/footer-inc.php";
	//include "../inc/close-inc.php";
?>
