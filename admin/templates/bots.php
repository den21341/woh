<?php

if($this->viewMode == 'act') {
    foreach ($this->bot_list as $bots) { ?>
        <p><?=$bots['id'].' '.$bots['name']?></p>
    <?php } ?>
    <form action="<?=$this->page_BuildUrl('bots', 'actdone')?>" method="post">
        <input type="hidden" value="<?=$this->act_bot?>" name="actbot">
        <select name="bid" id="btsel">
            <?php foreach ($this->bot_list as $bots) {
                if ($bots['id'] == $this->act_bot) {
                } else { ?>
                    <option value="<?= $bots['id'] ?>"><?= $bots['name'] . ' ' . $bots['fname'] ?></option>
                    <?php
                }
            } ?>
        </select>
        <select name="pid" id="itsel">
            <option value="0">-- Выберете бота --</option>
        </select><br><br>
        <label for="mon">Введите сумму:</label>
        <input id="mon" type="number" name="mon">
        <br>
        <input type="submit" value="Подтвердить" class="btn">
    </form>
<?php } else if($this->viewMode == 'msg') {
    if($this->subMode == 'sendMsg') { ?>
        <form class="w600-frm" action="<?=$this->page_BuildUrl("bots", "sendmsgDone")?>" method="post">
            <input type="hidden" name="from" value="<?=$this->from_id?>">
            <input type="hidden" name="to" value="<?=$this->to_id?>">
            <input type="hidden" name="item" value="<?=$this->item?>">
            <table class="tblfrm">
                <tr>
                    <th colspan="2">Кому - <?=$this->name[0]['name'].' '.$this->name[0]['fname']?></th>
                <tr>
                    <td>Сообщение</td>
                    <td><input type="text" name="mess"></td>
                </tr>
                </tr>
            </table>
            <div class="frmbtn"><input type="image" src="<?=WWWHOST.'admin/img/btn-user-save.png'?>" alt="Сохранить" /></div>;
        </form>
    <?php } ?>
    <table class="tbldat">
        <tr>
            <td>id</td>
            <td>Проект/Личное</td>
            <td>От кого</td>
            <td>Сообщение</td>
            <td>Дата</td>
            <td class="ls">Действия</td>
        </tr>
        <tr>
            <?php foreach ($this->botmsg as $msg) { ?>
            <td><?=$msg['id']?></td>
            <td><?=($msg['item_id'] ? "<a href=".$this->page_BuildUrl('proj','view').$msg['item_id'].">".$msg['item_id'].'</a>' : 'Личное')?></td>
            <td><a href="<?=WWWHOST.'users/viewrev/'.$msg['from_id'].'/'?>" target="_blank"><?=$msg['name'].' '.$msg['fname']?></a></td>
            <td><?=$msg['message']?></td>
            <td><?=$msg['add_date']?></td>
            <td><a href="<?=$this->page_BuildUrl('bots','sendmsg','from='.$msg['to_id']).'&to='.$msg['from_id'].($msg['item_id'] ? '$item='.$msg['item_id'] : '')?>">Ответить</a></td>
        </tr>
        <?php } ?>
    </table>
<?php } else if($this->viewMode == 'comm') { ?>
    <table class="tbldat">
        <tr>
            <td>id</td>
            <td>Проект</td>
            <td>От кого</td>
            <td>Коммент</td>
            <td>Дата</td>
        </tr>
        <tr>
            <?php foreach ($this->botmsg as $msg) { ?>
            <td><?=$msg['id']?></td>
            <td><?="<a href=".$this->page_BuildUrl('proj','view').$msg['item_id'].">".$msg['item_id']?></td>
            <td><a href="<?=WWWHOST.'users/viewrev/'.$msg['author_id'].'/'?>" target="_blank"><?=$msg['name'].' '.$msg['fname']?></a></td>
            <td><?=$msg['content']?></td>
            <td><?=$msg['add_date']?></td>
        </tr>
        <?php } ?>
    </table>
<?php } else { ?>

    <form class="w600-frm" action="<?=$this->page_BuildUrl("bots", "setbot")?>" method="post">
        <table class="tblfrm">
            <tr>
                <th colspan="2">Добавление бота</th>
            <tr>
                <td>ID бота</td>
                <td><input type="text" name="bid"></td>
            </tr>
            </tr>
        </table>
        <div class="frmbtn"><input type="image" src="<?=WWWHOST.'admin/img/btn-user-save.png'?>" alt="Добавить нового пользователя" /></div>;
    </form>
    <table class="tbldat">
        <tr>
            <td>id</td>
            <td>Имя</td>
            <td class="ls">Действия</td>
        </tr>
        <tr>
            <?php foreach ($this->botlist as $item) { ?>
            <td><?=$item['id']?></td>
            <td><a href="<?=$this->page_BuildUrl('bots','act').'?uid='.$item['id']?>"><?=$item['name'].' '.$item['fname']?></a></td>
            <td>
                <a href="<?=$this->page_BuildUrl('bots','showbot','bid='.$item['id'])?>">Сообщения</a>
                <a href="<?=$this->page_BuildUrl('bots','showcomm','bid='.$item['id'])?>">Комментарии</a>
            </td>
        </tr>
        <?php } ?>
    </table>
<?php } ?>
<script>
    $(document).ready(function () {
        $('#btsel').on('change', function (e) {
            var optionSelected = $("option:selected", this).val();
            var post_req_str = "uid=" + optionSelected;

            $.ajax({
                type: "GET",
                url: reqajxhost + "admin/bots/loaditems/",
                data: post_req_str,
                dataType: "json",
                success: function (data) {
                    if(data.length > 0) {
                        $('#itsel').find('option').remove().end();

                        for(var i=0; i<data.length; ++i) {
                            $('#itsel').append(
                                $('<option></option>').val(data[i]['id']).html(data[i]['id']));
                        }
                    }
                }

            });
        });

        $('#btsel').trigger("change");
    });
</script>
