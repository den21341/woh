<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();
	
	$ntype = $this->viewGroup;
	
	$ntype_arr = $this->model->news->News_GroupList();
	
	$ntype_title = $this->model->news->News_GroupInfo($ntype);
	
	$nreq = $this->newsinfo;
	
	/*
	include "../inc/db-inc.php";
	include "../inc/connect-inc.php";

	include "../inc/utils-inc.php";
	include "inc/admin_catutils-inc.php";

    include "../inc/ses-inc.php";
    include "inc/authorize-inc.php";

    if( $UserId == 0 )
    {
    	header("Location: index.php");
    }

	////////////////////////////////////////////////////////////////////////////
	// Module functions
function checkDtFormat($dt)
{
	if( !(
		is_numeric(substr($dt, 0, 1)) &&
		is_numeric(substr($dt, 1, 1)) &&
		is_numeric(substr($dt, 3, 1)) &&
		is_numeric(substr($dt, 4, 1)) &&
		is_numeric(substr($dt, 6, 1)) &&
		is_numeric(substr($dt, 7, 1)) &&
		is_numeric(substr($dt, 8, 1)) &&
		is_numeric(substr($dt, 9, 1))
		) )
		return false;

	$starr = split("[.]", $dt);
	if( (count($starr) == 3) &&
		is_numeric($starr[1]) && is_numeric($starr[0]) && is_numeric($starr[2]) &&
	 	!checkdate( $starr[1], $starr[0], $starr[2] ) )
	{
		return false;
	}

	return true;
}
	*/

	////////////////////////////////////////////////////////////////////////////
	// Module defines
	$strings['tipedit']['en'] = "Edit This News";
   	$strings['tipdel']['en'] = "Delete This News";
   	$strings['hdrlist']['en'] = "News List";
   	$strings['hdradd']['en'] = "Add News Record";
   	$strings['hdredit']['en'] = "Edit News Record";
   	$strings['rowdate']['en'] = "News date";
   	$strings['rowtitle']['en'] = "Title";
   	$strings['rowfirst']['en'] = "Preview Page";
   	$strings['rowtext']['en'] = "News Text";
   	$strings['rowbrand']['en'] = "Company Source";
   	$strings['btnadd']['en'] = "Add";
   	$strings['btndel']['en'] = "Delete";
   	$strings['btnedit']['en'] = "Edit";
   	$strings['btnrefresh']['en'] = "Update";
   	$strings['nolist']['en'] = "No news in database";
   	$strings['rowcont']['en'] = "Content";
   	$strings['rowfunc']['en'] = "Functions";
   	$strings["deleteconfirm"]["en"] = "Delete this news record now?";
   	$strings['bclist']['en'] = "News list";

    $strings['tipedit']['ru'] = "Редактировать эту новость";
   	$strings['tipdel']['ru'] = "Удалить эту новость";
   	$strings['hdrlist']['ru'] = "Список - ";
   	$strings['hdradd']['ru'] = "Добавить Новость";
   	$strings['hdredit']['ru'] = "Редакировать Новость";
   	$strings['rowdate']['ru'] = "Дата";
   	$strings['rowtitle']['ru'] = "Заголовок";
   	$strings['rowfirst']['ru'] = "Отображать в анонсе";
   	$strings['rowtext']['ru'] = "Текст";
   	$strings['rowbrand']['ru'] = "Компания";
    $strings['btnadd']['ru'] = "Добавить";
   	$strings['btndel']['ru'] = "Удалить";
   	$strings['btnedit']['ru'] = "Редактировать";
   	$strings['btnrefresh']['ru'] = "Обновить";
   	$strings['nolist']['ru'] = "В базе нет новостей";
    $strings['rowcont']['ru'] = "Содержание записей";
   	$strings['rowfunc']['ru'] = "Функции";
   	$strings["deleteconfirm"]["ru"] = "Вы действительно хотите удалить эту новость?";
   	$strings['bclist']['ru'] = "Список публикаций";

	/*
	$PAGE_HEADER['ru'] = "Управление новостями и статьями";
	$PAGE_HEADER['en'] = "News Editing";

	$THIS_TABLE = $TABLE_NEWS;
	$THIS_TABLE_LANG = $TABLE_NEWS_LANGS;

	////////////////////////////////////////////////////////////////////////////
	// Request parameters
	$ntype_arr = Array("Новости", "Статьи", "Обзоры");

	$mode = GetParameter("mode", "");
	$action = GetParameter("action", "");
	$ntype = GetParameter("ntype", 0);

	$pi = GetParameter("pi", 0);
	$pn = GetParameter("pn", 50);

	$datest = GetParameter("datest", date("d.m.Y", time()));

	$sectid = GetParameter("sectid", 0);
	$newstitle = GetParameter("newstitle", "");
	$newscont = GetParameter("newscont", "", false);
	$newsfirst = GetParameter("newsfirst", 0);
	$myfile = GetParameter("myfile", "");

	switch( $action )
	{
    	case "add":
    		$sectid = GetParameter("sectid", 0);
    		$newstitle = GetParameter("newstitle", "");
    		$newscont = GetParameter("newscont", "", false);
    		$newsfirst = GetParameter("newsfirst", 0);
    		$myfile = GetParameter("myfile", "");

    		$query = "INSERT INTO $THIS_TABLE ( sect_id, dtime, ngroup, first_page, filename_src)
    			VALUES ( '$sectid', NOW(), '$ntype', '$newsfirst', '".addslashes($myfile)."')";
			if( mysql_query($query) )
			{
				$newid = mysql_insert_id();

                for( $i=0; $i<count($langs); $i++ )
                {
                	if( !mysql_query( "INSERT INTO $THIS_TABLE_LANG ( news_id, lang_id, title, content )
	                    VALUES ('$newid', '".$langs[$i]."', '".addslashes($newstitle)."', '".addslashes($newscont)."')" ) )
	                {
	                   echo mysql_error();
	                }
	            }
			}
			else
			{
				echo "<b>".mysql_error()."</b>";
			}
			break;

		case "delete":
			// Delete selected news
			$news_id = GetParameter("news_id", "0");
			for($i = 0; $i < count($news_id); $i++)
			{
    			if(!mysql_query("DELETE FROM $THIS_TABLE WHERE id=".$news_id[$i]." "))
				{
        			echo "<b>".mysql_error()."</b>";
    			}
    			else
    			{
    				if( !mysql_query( "DELETE FROM $THIS_TABLE_LANG WHERE news_id='".$news_id[$i]."'" ) )
                    {
                       echo mysql_error();
                    }
    			}
			}
			break;

		case "deleteitem":
			$item_id = GetParameter("item_id", "0");
            if(!mysql_query("DELETE FROM $THIS_TABLE WHERE id=".$item_id." "))
			{
        		echo "<b>".mysql_error()."</b>";
    		}
    		else
    		{
    			if( !mysql_query( "DELETE FROM $THIS_TABLE_LANG WHERE news_id='".$item_id."'" ) )
				{
					echo mysql_error();
				}
    		}
    		break;

    	case "edititem":
    		$mode = "edit";
    		Breadcrumbs_Add( $PHP_SELF, $strings['bclist'][$lang] );
    		break;

		case "update":
			$item_id = GetParameter("item_id", "0");
			$sectid = GetParameter("sectid", 0);
            $newstitle = GetParameter("newstitle", "");
    		$newscont = GetParameter("newscont", "", false);
    		$newsfirst = GetParameter("newsfirst", 0);
    		//$newsbrand = GetParameter("newsbrand", 0);
    		$myfile = GetParameter("myfile", "");

    		$db_datest = substr($datest, 6, 4)."-".substr($datest, 3, 2)."-".substr($datest, 0, 2)." 01:00:00";

			if(!mysql_query("UPDATE $THIS_TABLE SET sect_id='$sectid', first_page='$newsfirst', filename_src='".addslashes($myfile)."', dtime='$db_datest'
				WHERE id='".$item_id."'"))
			{
				echo "<b>".mysql_error()."</b>";
			}

			$query = "UPDATE $THIS_TABLE_LANG SET title='".addslashes($newstitle)."', content='".addslashes($newscont)."'
                        WHERE news_id='".$item_id."' AND lang_id='".$LangId."'";
			if( !mysql_query( $query ) )
			{
				echo mysql_error();
			}
			break;
	}

	////////////////////////////////////////////////////////////////////////////
	// Include Top Header HTML Style
	include "inc/header-inc.php";
	//
	////////////////////////////////////////////////////////////////////////////
	
	*/

    if( $this->viewMode == "edit" )
    {
		/*
		$item_id = GetParameter("item_id", "0");
		$sectid = 0;
		$newstitle = "";
		$newscont = "";
		$newsfirst = 0;
		$myfile = "";
		//$datest = date("d.m.Y", time());

		if($res = mysql_query("SELECT m1.*, m2.title, m2.content,
			DAYOFMONTH(m1.dtime) as dd, MONTH(m1.dtime) as dm, YEAR(m1.dtime) as dy
			FROM $THIS_TABLE m1, $THIS_TABLE_LANG m2
			WHERE m1.id='$item_id' AND m1.id=m2.news_id AND m2.lang_id='$LangId'"))
		{
			if($row = mysql_fetch_object($res))
			{
				$sectid = $row->sect_id;
				$newstitle = stripslashes($row->title);
				$newscont = stripslashes($row->content);
				$newsfirst = $row->first_page;
				$datest = sprintf("%02d.%02d.%04d", $row->dd, $row->dm, $row->dy);

				$myfile = stripslashes($row->filename_src);
			}
			mysql_free_result($res);
		}
		*/
?>
	<br /><br />
    <h2><?=$strings['hdredit'][$lang];?></h2>
	<div class="dd-sep"></div>
	<form name="advfrm" id="advfrm" action="<?=$this->page_BuildUrl("news", "save");?>" method="post">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="ntype" value="<?=$ntype;?>" />
	<input type="hidden" name="item_id" value="<?=$nreq['id'];?>" />
	<table class="tblfrm w100">
	<tr>
		<th colspan="2">Поля для заполнения</th>
	</tr>
	<tr><td class="ff"><?=$strings['rowdate'][$lang];?>:</td><td>
	<?php
		echo "<table class=\"calendtbl\"><tr>
		<td><input type=\"text\" id=\"datest\" name=\"datest\" size=\"10\" maxlength=\"10\" value=\"".$nreq['date']."\" /> &nbsp;</td>
		<td><a href=\"javascript:OpenPopup('calendar.php?target=self.opener.document.advfrm.datest', 190, 160)\"><img src=\"".WWWHOST."admin/img/a-calend.gif\" width=\"17\" height=\"22\" border=\"0\" alt=\"Выбрать дату\" /></a></td>
		</tr></table>";
	?>
	</td></tr>
	<tr class="even">
		<td class="ff"><?=$strings['rowtitle'][$lang];?>: </td>
		<td><input type="text" size="70" name="newstitle" value="<?=$nreq['title'];?>" /></td>
	</tr>
	<tr>
		<td class="ff">Описание типа:</td>
    	<td><textarea class="ckeditor" name="newscont" cols="70" rows="10"><?=$nreq['content'];?></textarea></td>
	</tr>
	<tr class="even">
		<td class="ff">Картинка:</td>
		<td><input type="text" size="30" name="myfile" value="<?=$nreq['filename_src'];?>" /><input type="button" value=" Выбрать файл " onclick="javascript:MM_openBrWindow('cat_files.php?hide=1&lang=<?=$lang;?>&target=self.opener.document.advfrm.myfile','winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');" /></td>
	</tr>
	<tr>
		<td class="ff">Раздел каталога:</td>
		<td><select name="sectid">
			<option value="0">--- без раздела ---</option>
	<?php
		$this->drawSectCombo($nreq['sectid']);
		
		//$THIS_TABLE = $TABLE_CAT_CATALOG;
		//$THIS_TABLE_LANG = $TABLE_CAT_CATALOG_LANGS;
		//PrintWorkCatalog(0, $LangId, 0, "select", $sectid);
	?>
		</select></td>
	</tr>
	<tr class="even">
		<td class="ff"><?=$strings['rowfirst'][$lang];?>:</td>
    	<td><select name="newsfirst">
			<option value="0">НЕТ</option>
			<option value="1"<?=($nreq['first_page'] == 1 ? ' selected' : '');?>>ДА</option>
		</select></td>
	</tr>
	</table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-save.png" alt="Сохранить изменения" /></div>
	</form>

<?php
	}
	else
	{
		/*
		$news_total = 0;
		$query = "SELECT count(*) as totnews FROM $THIS_TABLE WHERE ngroup='$ntype'";
		if( $res = mysql_query( $query ) )
		{
			if( $row = mysql_fetch_object( $res ) )
			{
				$news_total = $row->totnews;
			}
			mysql_free_result( $res );
		}
		*/
		
		$news_total = $this->model->news->News_ItemsNum($ntype);

		$pagesnum = ceil($news_total / $this->pn);
?>
	<h2><?=$strings['hdrlist'][$lang].$ntype_title?></h2>
    <div class="dd">
		<div class="left">Тип публикации: &nbsp;
		<?php
			for( $i=0; $i<count($ntype_arr); $i++ )
			{
				if( $i > 0 )
					echo ' &nbsp;::&nbsp; ';

				if( $ntype == $i )
					echo '<b>'.$ntype_arr[$i].'</b>';
				else
					echo '<a href="'.$this->Page_BuildUrl("news", "", 'ntype='.$i).'">'.$ntype_arr[$i].'</a>';
			}
		?>
		</div>
		<div class="right">
			<div class="cpages">
		<?php
			echo '<a class="a-first" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi=1&pn='.$this->pn ).'"></a>
				<a class="a-prev" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi='.($this->pi > 1 ? ($this->pi-1) : 1).'&pn='.$this->pn).'"></a>';
			for( $i=1; $i<=$pagesnum; $i++ )
   			{
   				if( $i == $this->pi )
   					echo '<span>'.$i.'</span>';
   				else
        				echo '<a class="a-page" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi='.$i.'&pn='.$this->pn).'">'.$i.'</a>';
   			}
   			echo '<a class="a-next" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi='.( $this->pi < $pagesnum ? $this->pi+1 : $pagesnum ).'&pn='.$this->pn).'"></a>
				<a class="a-last" href="'.$this->Page_BuildUrl("news", "", 'ntype='.$ntype.'&pi='.$pagesnum.'&pn='.$this->pn).'"></a>';
   		?>
			</div>
		</div>
		<div class="cen"> &nbsp; </div>
	</div>
	<form action="<?=$this->page_BuildUrl("news", "deleteall");?>" method=POST>
    <input type="hidden" name="action" value="delete" />
    <input type="hidden" name="ntype" value="<?=$ntype;?>" />
    <table class="tbldat">
	<tr>
		<th>&nbsp;</th>
		<th>Фото</th>
		<th><?=$strings['rowcont'][$lang];?></th>
		<th><?=$strings['rowfirst'][$lang];?></th>
		<th class="ls">Действия</th>
	</tr>
<?php
		$found_items = 0;
		$news = $this->newslist;
		
		for( $i=0; $i<count($news); $i++ )
		{
			$row = $news[$i];
			$found_items++;
			
			$sect_name = '';
			
			echo '<tr'.( $found_items % 2 == 1 ? ' class="even"' : '' ).'>
				<td><input type="checkbox" name="news_id[]" value="'.$row['id'].'" /></td>
				<td>'.( $row['filename_ico'] != "" ? "<img src=\"".FILE_DIR.stripslashes($row['filename_ico'])."\" alt=\"\" />" : "" ).'</td>
				<td class="p-name">'.$row['title'].'<br /><span>['.$row['dtime'].']'.($sect_name != "" ? ' - Раздел: '.$sect_name : '').'</span></td>
				<td>'.($row['first_page'] == 1 ? " <span style=\"font-weight: bold; color: red;\">Да</span> " : " Нет ").'</td>
				<td class="ls">
					<a href="'.$this->Page_BuildUrl("news", "edit", 'ntype='.$ntype.'&item_id='.$row['id']).'" title="'.$strings['tipedit'][$lang].'"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
					<a href="'.$this->Page_BuildUrl("news", "delete", 'ntype='.$ntype.'&item_id='.$row['id']).'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>&nbsp;
				</td>
			</tr>';
		}

		/*
    	$found_items = 0;
		if( $res = mysql_query("SELECT m1.*, m2.title, m2.content
			FROM $THIS_TABLE m1, $THIS_TABLE_LANG m2
			WHERE m1.ngroup='$ntype' AND m1.id=m2.news_id AND m2.lang_id='$LangId'
			ORDER BY m1.dtime DESC
			LIMIT ".($pi*$pn).",$pn") )
		{
			while($row=mysql_fetch_object($res))
			{
                $found_items++;

                $sect_name = '';
                if( $row->sect_id != 0 )
                {
                	$query1 = "SELECT * FROM $TABLE_CAT_CATALOG_LANGS WHERE sect_id='".$row->sect_id."' AND lang_id='$LangId'";
					if( $res1 = mysql_query( $query1 ) )
					{
						while( $row1 = mysql_fetch_object( $res1 ) )
						{
							$sect_name = stripslashes($row1->name);
						}
						mysql_free_result( $res1 );
					}
                }

                $str_text = strip_tags(stripslashes($row->content));
            	if( strlen($str_text) > 2000 )
	            {
	                $pos = strpos( $str_text, " ", 2000 );
	                if( $pos )
	                {
	                    $str_text = substr($str_text, 0, $pos)."...";
	                    //$str_text .= "<div class=\"linkpar\"><a href=\"$PHP_SELF?id=".$news[$i]['id']."\" class=\"greenlink\">Подробнее...</a></div>";
	                }
	            }

	            echo '<tr'.( $found_items % 2 == 1 ? ' class="even"' : '' ).'>
					<td><input type="checkbox" name="news_id[]" value="'.$row->id.'" /></td>
					<td>'.( $row->filename_ico != "" ? "<img src=\"".$FILE_DIR.stripslashes($row->filename_ico)."\" alt=\"\" />" : "" ).'</td>
					<td class="p-name">'.stripslashes($row->title).'<br /><span>['.$row->dtime.']'.($sect_name != "" ? ' - Раздел: '.$sect_name : '').'</span></td>
					<td>'.($row->first_page == 1 ? " <span style=\"font-weight: bold; color: red;\">Да</span> " : " Нет ").'</td>
					<td class="ls">
						<a href="'.$PHP_SELF.'?action=edititem&ntype='.$ntype.'&item_id='.$row->id.'" title="'.$strings['tipedit'][$lang].'"><img src="img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
						<a href="'.$PHP_SELF.'?action=deleteitem&ntype='.$ntype.'&item_id='.$row->id.'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>&nbsp;
					</td>
				</tr>';
			}
            mysql_free_result($res);
		}
		*/

		if( $found_items == 0 )
		{
			echo '<tr><td colspan="5" class="ls">'.$strings['nolist'][$lang].'</td></tr>';
        }
    ?>
    </table>
<?php
	if( $found_items > 0 )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-del-selected.png" alt="Удалить выбранные" /></div>';
	}
?>
	</form>

    <br /><br />
    <h2><?=$strings['hdradd'][$lang];?></h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->Page_BuildUrl("news", "add");?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="ntype" value="<?=$ntype;?>" />
	<table class="tblfrm w100">
	<tr>
		<th colspan="2">Поля для заполнения</th>
	</tr>
	<tr class="even">
		<td class="ff"><?=$strings['rowtitle'][$lang];?>: </td>
		<td><input type="text" size="70" name="newstitle" value="<?=$nreq['title'];?>" /></td>
	</tr>
	<tr>
		<td class="ff">Описание типа:</td>
    	<td><textarea class="ckeditor" name="newscont" cols="70" rows="10"><?=$nreq['content'];?></textarea></td>
	</tr>
	<tr class="even">
		<td class="ff">Картинка:</td>
		<!--<td><input type="text" size="30" name="myfile" value="<?=$nreq['filename_src'];?>" /><input type="button" value=" Выбрать файл " onclick="javascript:MM_openBrWindow('cat_files.php?hide=1&lang=<?=$lang;?>&target=self.opener.document.catfrm.myfile','winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');" /></td>-->
		<td><input type="file" name="pfile1"></td>
	</tr>
	<tr>
		<td class="ff">Раздел каталога:</td>
		<td><select name="sectid">
			<option value="0">--- без раздела ---</option>
	<?php
		$this->drawSectCombo();
		/*
		$THIS_TABLE = $TABLE_CAT_CATALOG;
		$THIS_TABLE_LANG = $TABLE_CAT_CATALOG_LANGS;
		PrintWorkCatalog(0, $LangId, 0, "select");
		*/
	?>
		</select></td>
	</tr>
	<tr class="even">
		<td class="ff"><?=$strings['rowfirst'][$lang];?>:</td>
    	<td><select name="newsfirst">
			<option value="0">НЕТ</option>
			<option value="1">ДА</option>
		</select></td>
	</tr>
	</table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-make-new.png" alt="Создать новый тип товара" /></div>
	</form>

<?php
    }

    //include "inc/footer-inc.php";
	//
    //include "../inc/close-inc.php";
?>
