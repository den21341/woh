<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();

	/*
	include "../inc/db-inc.php";
	include "../inc/connect-inc.php";

    include "../inc/utils-inc.php";
    include "../inc/catutils-inc.php";

    include "../inc/ses-inc.php";
    include "inc/authorize-inc.php";

    if( $UserId == 0 )
    {
    	header("Location: index.php");
    }
	*/

    ////////////////////////////////////////////////////////////////////////////
    // Module defines
	$strings['tipedit']['en'] = "Edit splash slides";
   	$strings['tipdel']['en'] = "Delete This Slide";
	$strings['nolist']['en'] = "In the database there is no slide";
	$strings['rowfunc']['en'] = "";
	$strings['deleteconfirm']["en"] = "Are you sure you want to delete this item?";
	$strings['bclist']['en'] = "Slide list";

	$strings['tipedit']['ru'] = "Редактировать слайд";
   	$strings['tipdel']['ru'] = "Удалить слайд";
	$strings['nolist']['ru'] = "В базе нет ни одного слайда";
	$strings['rowfunc']['ru'] = "";
	$strings['deleteconfirm']["ru"] = "Вы действительно хотите удалить этот элемент?";
	$strings['bclist']['ru'] = "Список слайдов";

	//$PAGE_HEADER['ru'] = "Управление слайдами";
	//$PAGE_HEADER['en'] = "Slide list";

	//$THIS_TABLE = $TABLE_SLIDES;

	$SLIDE_W = 980;
	$SLIDE_H = 246;

	////////////////////////////////////////////////////////////////////////////
	// Reuqest parameters
	/*
	$mode = GetParameter("mode", "");
	$action = GetParameter("action", "");

	switch( $action )
	{
    	case "add":
    		$orgname = GetParameter("orgname", "");
			$orgsort = GetParameter("orgsort", 0);
			$orgurl = GetParameter("orgurl", "");
			$orgdescr = GetParameter("orgdescr", "");
			$myfile = GetParameter("myfile", "");

    		$query = "INSERT INTO $THIS_TABLE ( lang_id, sort_num, filename, title, url, comment )
    			VALUES ('$LangId', '".$orgsort."', '".addslashes($myfile)."', '".addslashes($orgname)."', '".addslashes($orgurl)."', '".addslashes($orgdescr)."')";
			if( mysql_query($query) )
			{
				$newsectid = mysql_insert_id();
			}
			else
			{
				echo "<b>".mysql_error()."</b>";
			}
			break;

		case "delete":
			// Delete selected news
			$items_id = GetParameter("items_id", "");
			for($i = 0; $i < count($items_id); $i++)
			{
    			if(!mysql_query("DELETE FROM $THIS_TABLE WHERE id=".$items_id[$i]." "))
				{
        			echo "<b>".mysql_error()."</b>";
    			}
			}
			break;

		case "deleteitem":
			$item_id = GetParameter("item_id", "0");
            if(!mysql_query("DELETE FROM $THIS_TABLE WHERE id=".$item_id." "))
			{
        		echo "<b>".mysql_error()."</b>";
    		}
			break;

		case "edititem":
			$item_id = GetParameter("item_id", "");
			$mode = "edit";
			break;

		case "update":
			$item_id = GetParameter("item_id", "");
			$orgname = GetParameter("orgname", "");
			$orgsort = GetParameter("orgsort", 0);
			$orgurl = GetParameter("orgurl", "");
			$orgdescr = GetParameter("orgdescr", "");
			$myfile = GetParameter("myfile", "");

			if(!mysql_query("UPDATE $THIS_TABLE
                        SET sort_num='$orgsort', filename='".addslashes($myfile)."', title='".addslashes($orgname)."',
                        comment='".addslashes($orgdescr)."', url='".addslashes($orgurl)."'
                        WHERE id='".$item_id."'"))
			{
				echo "<b>".mysql_error()."</b>";
			}
			break;
	}

	if( $mode == "edit" )
	{
  		Breadcrumbs_Add( $PHP_SELF, $strings['bclist'][$lang] );
	}

	// Include Top Header HTML Style
	include "inc/header-inc.php";
	*/

    if( $this->viewMode == "edit" )
    {
		$req = $this->slideinfo;
		
		/*
		$item_id = GetParameter("item_id", 0);
		$orgname = "";
		$orgsort = 0;
		$myfile = "";
		$orgurl = "";
		$orgdescr = "";

		if($res = mysql_query("SELECT * FROM $THIS_TABLE WHERE id='$item_id'"))
		{
			if($row = mysql_fetch_object($res))
			{
				//$orgsect = $row->parent_id;
				$orgname = stripslashes($row->title);
				$orgsort = $row->sort_num;
				$myfile = stripslashes($row->filename);
				$orgurl = stripslashes($row->url);
				$orgdescr = stripslashes($row->comment);
			}
			mysql_free_result($res);
		}

		//echo "ID: $item_id<br />";
		*/
?>

	<br /><br />
    <h2>Редактировать</h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->page_BuildUrl("splashslides", "save");?>" method="post">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="id" value="<?=$req['id'];?>" />
	<table class="tblfrm w100">
	<tr><td colspan="2"><img src="<?=FILE_DIR.$req['filename'];?>" alt="" /></td></tr>
    <tr class="even"><td class="ff">Название:</td><td><input type="text" size="70" name="orgname" value="<?=$req['title'];?>" /></td></tr>
    <tr><td class="ff">Комментарий:</td><td><input type="text" size="70" name="orgdescr" value="<?=$req['comment'];?>" /></td></tr>
    <tr class="even"><td class="ff">Ссылка (URL):</td><td><input type="text" size="70" name="orgurl" value="<?=$req['url'];?>" /></td></tr>
    <tr><td class="ff">Порядковый номер:</td><td><input type="text" size="30" name="orgsort" value="<?=$req['sort_num'];?>" /></td></tr>
    <tr class="even"><td class="ff">Картинка (<?=$SLIDE_W;?> x <?=$SLIDE_H;?>): </td><td><input type="text" name="myfile" style="width: 200px" value="<?=$req['filename'];?>" /><input type="button" value="Выбрать" onclick="MM_openBrWindow('cat_files.php?hide=1&target=self.opener.document.catfrm.myfile','winfiles','width=<?=FILEMAN_POPUP_W;?>,height=<?=FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');"></td></tr>
    </table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-save.png" alt=" Сохранить изменения " /></div>
	</form>
<?php
	}
	else
	{
?>
	<h2>Список слайдов</h2>
    <form action="<?=$this->page_BuildUrl("splashslides", "deleteall");?>" method=POST>
    <input type="hidden" name="action" value="delete" />
    <table class="tbldat">
	<tr>
		<th>&nbsp;</th>
		<th>Слайд</th>
		<th class="ls">Действия</th>
	</tr>
<?php
		$its = $this->slidelist;
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			echo '<tr'.( ($i+1) % 2 == 1 ? ' class="even"' : '' ).'>
				<td><input type="checkbox" name="items_id[]" value="'.$row['id'].'" /></td>
				<td>'.( $row['filename'] != "" ? "<img src=\"".WWWHOST.FILE_DIR.stripslashes($row['filename'])."\" alt=\"\" />" : "" ).'
					<br /><b>'.stripslashes($row['title']).'</b> ('.stripslashes($row['comment']).')
					<br />'.stripslashes($row['url']).'
				</td>
				<td class="ls">
					<a href="'.$this->Page_BuildUrl("splashslides", "edit", 'action=edititem&id='.$row['id']).'" title="'.$strings['tipedit'][$lang].'"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
					<a href="'.$this->Page_BuildUrl("splashslides", "delete", 'action=deleteitem&id='.$row['id']).'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>
				</td>
			</tr>';
		}
		
		$found_items = count($its);
		
		/*
    	$found_items = 0;
		if( $res = mysql_query("SELECT * FROM $THIS_TABLE WHERE lang_id='$LangId' ORDER BY sort_num") )
		{
			while( $row = mysql_fetch_object($res) )
			{
                $found_items++;

                echo '<tr'.( $found_items % 2 == 1 ? ' class="even"' : '' ).'>
					<td><input type="checkbox" name="items_id[]" value="'.$row->id.'" /></td>
					<td>'.( $row->filename != "" ? "<img src=\"".$FILE_DIR.stripslashes($row->filename)."\" alt=\"\" />" : "" ).'
						<br /><b>'.stripslashes($row->title).'</b> ('.stripslashes($row->comment).')
                  		<br />'.stripslashes($row->url).'
     				</td>
					<td class="ls">
						<a href="'.$PHP_SELF.'?action=edititem&item_id='.$row->id.'" title="'.$strings['tipedit'][$lang].'"><img src="img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
						<a href="'.$PHP_SELF.'?action=deleteitem&item_id='.$row->id.'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>
					</td>
				</tr>';
			}
            mysql_free_result($res);
		}
		else
			echo mysql_error();
		*/

		if( $found_items == 0 )
		{
			echo '<tr><td colspan="6" class="ls">'.$strings['nolist'][$lang].'</td></tr>';
        }
?>
    </table>
<?php
	if( $found_items > 0 )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-del-selected.png" alt="Удалить выбранные" /></div>';
	}
?>
    </form>


	<br /><br />
    <h2>Добавить слайд</h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->page_BuildUrl("splashslides", "add");?>" method="post">
	<input type="hidden" name="action" value="add" />
	<table class="tblfrm w100">
    <tr class="even"><td class="ff">Название:</td><td><input type="text" size="70" name="orgname"></td></tr>
    <tr><td class="ff">Комментарий:</td><td><input type="text" size="70" name="orgdescr" /></td></tr>
    <tr class="even"><td class="ff">Ссылка (URL):</td><td><input type="text" size="70" name="orgurl" /></td></tr>
    <tr><td class="ff">Порядковый номер:</td><td><input type="text" size="30" name="orgsort"></td></tr>
    <tr class="even"><td class="ff">Картинка (<?=$SLIDE_W;?> x <?=$SLIDE_H;?>): </td><td><input type="text" name="myfile" style="width: 200px"><input type="button" value="Выбрать" onclick="MM_openBrWindow('cat_files.php?hide=1&target=self.opener.document.catfrm.myfile','winfiles','width=<?=$FILEMAN_POPUP_W;?>,height=<?=$FILEMAN_POPUP_H;?>,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');"></td></tr>
    </table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-add-new.png" alt="Добавить слайд" /></div>
	</form>
<?php
    }

    //include "inc/footer-inc.php";
    //include "../inc/close-inc.php";
?>
