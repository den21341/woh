<?php

$PM = $this->pageModel;

$LangId = $PM->getLangId();
$lang = $PM->getViewLang();

$UHCMS_MODNAME = "bonuswin_list";

$fields['name']['ru'] = 'Имя';
$fields['fname']['ru'] = 'Фамилия';
$fields['login']['ru'] = 'Логин';
$fields['passwd']['ru'] = 'Пароль';
$fields['address']['ru'] = 'Адрес';
$fields['city']['ru'] = 'Город';
$fields['country']['ru'] = 'Страна';
$fields['zip_code']['ru'] = 'Почтовый Индекс';
$fields['telephone']['ru'] = 'Тел.';
$fields['office_phone']['ru'] = 'Раб.';
$fields['cell_phone']['ru'] = 'Моб.';
$fields['email1']['ru'] = 'E-Mail';
$fields['email2']['ru'] = 'E-Mail';
$fields['email3']['ru'] = 'E-Mail';
$fields['web_url']['ru'] = 'Веб-страница';
$fields['groupid']['ru'] = 'Группа пользователей';
$fields['trader']['ru'] = 'Привязка к трейдеру';

$fields['name']['en'] = 'Name';
$fields['fname']['en'] = 'Sirname';
$fields['login']['en'] = 'Login';
$fields['passwd']['en'] = 'Password';
$fields['address']['en'] = 'Address';
$fields['city']['en'] = 'City';
$fields['country']['en'] = 'Country';
$fields['zip_code']['en'] = 'Zip';
$fields['telephone']['en'] = 'Telephone';
$fields['office_phone']['en'] = 'Office Phone';
$fields['cell_phone']['en'] = 'Cell Phone';
$fields['email1']['en'] = 'E-Mail 1';
$fields['email2']['en'] = 'E-Mail 2';
$fields['email3']['en'] = 'E-Mail 3';
$fields['web_url']['en'] = 'Web-url';
$fields['groupid']['en'] = 'User Group';
$fields['trader']['en'] = 'Trader Assign';

$strings['editprof']['en'] = "Edit project's profile";
$strings['newprof']['en'] = "Add user profile";
$strings['projlist']['en'] = "Projects List";
$strings['addbtn']['en'] = " Add New ";
$strings['applybtn']['en'] = " Apply ";
$strings['rowlogin']['en'] = "Login";
$strings['rowname']['en'] = "Name";
$strings['rowaddr']['en'] = "Address";
$strings['rowcontact']['en'] = "Contacts";
$strings['tipchangepwd']['en'] = "Change Password";
$strings['tipedituser']['en'] = "Edit User Profile";
$strings['tipdeluser']['en'] = "Delete User";
$strings['tipstatus']['en'] = "Change Status";

$strings['editprof']['ru'] = "Редактирование Запроса";
$strings['newprof']['ru'] = "Добавление Нового Пользователя";
$strings['projlist']['ru'] = "Список Предложений";
$strings['addbtn']['ru'] = " Добавить ";
$strings['applybtn']['ru'] = " Применить ";
$strings['rowlogin']['ru'] = "Пользователи";
$strings['rowname']['ru'] = "Ф.И.О.";
$strings['rowaddr']['ru'] = "Адрес";
$strings['rowcontact']['ru'] = "Контакты";
$strings['tipchangepwd']['ru'] = "Изменить Пароль";
$strings['tipedituser']['ru'] = "Редактировать Профиль";
$strings['tipdeluser']['ru'] = "Удалить Пользователя";
$strings['tipstatus']['ru'] = "Изменить Статус";


if(isset($this->res)) { ?>
<h3 style="color: #80cf68">Отправлено</h3>
<?php } ?>

<form class="w600-frm" action="<?=$this->page_BuildUrl("abox", "saveMoney")?>" method="post">
    <table class="tblfrm">
        <tr>
            <td>Введите id пользователя: </td>
            <td>Кол-во силы: </td>
        </tr>
        <tr>
            <td><input type="text" name="uid"></td>
            <td><input type="text" name="money"></td>
        </tr>
        <tr>
            <td>
                <input type="submit" name="submit" value="Отправить">
            </td>
        </tr>
    </table>
</form>
