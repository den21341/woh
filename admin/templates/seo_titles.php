<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();

	/*
	include "../inc/db-inc.php";
	include "../inc/connect-inc.php";

    include "../inc/utils-inc.php";
    include "../inc/catutils-inc.php";

    include "../inc/ses-inc.php";
    include "inc/authorize-inc.php";

    if( $UserId == 0 )
    {
    	header("Location: index.php");
    }
	*/

    ////////////////////////////////////////////////////////////////////////////
    // Module defines
	$strings['tipedit']['en'] = "Edit seo titles";
   	$strings['tipdel']['en'] = "Delete This Seo data";
	$strings['nolist']['en'] = "In the database there is no records";
	$strings['rowfunc']['en'] = "";
	$strings['deleteconfirm']["en"] = "Are you sure you want to delete this item?";
	$strings['bclist']['en'] = "Seo list";

	$strings['tipedit']['ru'] = "Редактировать SEO запись";
   	$strings['tipdel']['ru'] = "Удалить SEO запись";
	$strings['nolist']['ru'] = "В базе нет ни одной записи";
	$strings['rowfunc']['ru'] = "";
	$strings['deleteconfirm']["ru"] = "Вы действительно хотите удалить этот элемент?";
	$strings['bclist']['ru'] = "Список SEO записей";

	//$PAGE_HEADER['ru'] = "Управление слайдами";
	//$PAGE_HEADER['en'] = "Slide list";

	//$THIS_TABLE = $TABLE_SLIDES;

	$SLIDE_W = 980;
	$SLIDE_H = 246;

	////////////////////////////////////////////////////////////////////////////
	// Reuqest parameters
	/*
	$mode = GetParameter("mode", "");
	$action = GetParameter("action", "");

	if( $mode == "edit" )
	{
  		Breadcrumbs_Add( $PHP_SELF, $strings['bclist'][$lang] );
	}

	// Include Top Header HTML Style
	include "inc/header-inc.php";
	*/
	
	$req = $this->itinfo;

    if( $this->viewMode == "edit" )
    {		
		/*
		$item_id = GetParameter("item_id", 0);
		$orgname = "";
		$orgsort = 0;
		$myfile = "";
		$orgurl = "";
		$orgdescr = "";

		if($res = mysql_query("SELECT * FROM $THIS_TABLE WHERE id='$item_id'"))
		{
			if($row = mysql_fetch_object($res))
			{
				//$orgsect = $row->parent_id;
				$orgname = stripslashes($row->title);
				$orgsort = $row->sort_num;
				$myfile = stripslashes($row->filename);
				$orgurl = stripslashes($row->url);
				$orgdescr = stripslashes($row->comment);
			}
			mysql_free_result($res);
		}

		//echo "ID: $item_id<br />";
		*/
?>

	<br /><br />
    <h2>Редактировать</h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->page_BuildUrl("seo", "save");?>" method="post">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="id" value="<?=$req['id'];?>" />
	<table class="tblfrm w100">
    <tr class="even"><td class="ff">URL:</td><td><input type="text" size="50" name="orgurl" value="<?=$req['url'];?>" /></td></tr>
    <tr><td class="ff">Title:</td><td><input type="text" size="70" name="orgtitle" value="<?=$req['title'];?>" /></td></tr>
    <tr class="even"><td class="ff">Keywords:</td><td><textarea cols="70" rows="3" name="orgkeyw"><?=$req['keyw'];?></textarea></td></tr>
    <tr><td class="ff">Descr:</td><td><textarea cols="70" rows="4" name="orgdescr"><?=$req['descr'];?></textarea></td></tr>
    <tr><td class="ff">&nbsp;</td><td>&nbsp;</td></tr>    
	<tr class="even"><td class="ff">H1:</td><td><input type="text" name="orgh1" value="<?=$req['h1'];?>" /></td></tr>
    <tr><td class="ff">Текст 1:</td><td><textarea class="ckeditor" cols="70" rows="12" name="txt1"><?=$req['text1'];?></textarea></td></tr>
    <tr class="even"><td class="ff">Текст 2:</td><td><textarea class="ckeditor" cols="70" rows="12" name="txt2"><?=$req['text2'];?></textarea></td></tr>
    </table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-save.png" alt=" Сохранить изменения " /></div>
	</form>
<?php
	}
	else
	{
?>
	<h2>SEO записи</h2>
    <form action="<?=$this->page_BuildUrl("seo", "deleteall");?>" method=POST>
    <input type="hidden" name="action" value="delete" />
    <table class="tbldat">
	<tr>
		<th>&nbsp;</th>
		<th>URL</th>
		<th>Title</th>
		<th>Дата</th>
		<th class="ls">Действия</th>
	</tr>
<?php
		$its = $this->itslist;
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			echo '<tr'.( ($i+1) % 2 == 1 ? ' class="even"' : '' ).'>
				<td><input type="checkbox" name="items_id[]" value="'.$row['id'].'" /></td>
				<td class="name">'.stripslashes($row['url']).'</td>
				<td><b>'.stripslashes($row['page_title']).'</b></td>
				<td>'.$row['modify_date'].'</td>
				<td class="ls">
					<a href="'.$this->Page_BuildUrl("seo", "edit", 'action=edititem&id='.$row['id']).'" title="'.$strings['tipedit'][$lang].'"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
					<a href="'.$this->Page_BuildUrl("seo", "delete", 'action=deleteitem&id='.$row['id']).'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>
				</td>
			</tr>';
		}
		
		$found_items = count($its);		

		if( $found_items == 0 )
		{
			echo '<tr><td colspan="5" class="ls">'.$strings['nolist'][$lang].'</td></tr>';
        }
?>
    </table>
<?php
	if( $found_items > 0 )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-del-selected.png" alt="Удалить выбранные" /></div>';
	}
?>
    </form>


	<br /><br />
    <h2>Добавить SEO запись</h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->page_BuildUrl("seo", "add");?>" method="post">
	<input type="hidden" name="action" value="add" />
	<table class="tblfrm w100">
	<tr class="even"><td class="ff">URL:</td><td><input type="text" size="50" name="orgurl" value="<?=$req['url'];?>" /></td></tr>
    <tr><td class="ff">Title:</td><td><input type="text" size="70" name="orgtitle" value="<?=$req['title'];?>" /></td></tr>
    <tr class="even"><td class="ff">Keywords:</td><td><textarea cols="70" rows="3" name="orgkeyw"><?=$req['keyw'];?></textarea></td></tr>
    <tr><td class="ff">Descr:</td><td><textarea cols="70" rows="4" name="orgdescr"><?=$req['descr'];?></textarea></td></tr>
    <tr><td class="ff">&nbsp;</td><td>&nbsp;</td></tr>    
	<tr class="even"><td class="ff">H1:</td><td><input type="text" name="orgh1" value="<?=$req['h1'];?>" /></td></tr>
    <tr><td class="ff">Текст 1:</td><td><textarea class="ckeditor" cols="70" rows="12" name="txt1"><?=$req['text1'];?></textarea></td></tr>
    <tr class="even"><td class="ff">Текст 2:</td><td><textarea class="ckeditor" cols="70" rows="12" name="txt2"><?=$req['text2'];?></textarea></td></tr>    
    </table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-add-new.png" alt="Добавить SEO" /></div>
	</form>
<?php
    }

    //include "inc/footer-inc.php";
    //include "../inc/close-inc.php";
?>
