<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$MENU_HIDE=1;
	
	$PM = $this->pageModel;

	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();
	
	$uan = $this->uan;					// Use Absolute Name
	//$uan = GetParameter( "uan", 0 );
	
	$cur_dir = $this->cur_dir;
	$tmp_dir = $this->tmp_dir;	
	
	$full = ( $this->viewFull ? 1 : 0 );
	$mode = $this->viewMode;
		
	////////////////////////////////////////////////////////////////////////////
    //$strings['title']['ru'] = "Файловый Менеджер";
    $strings['close']['ru'] = "Закрыть Окно";    
    $strings['hdrload']['ru'] = "Загрузить файл на сервер в текущую папку";
    $strings['typelist']['ru'] = "Список разрешенных типов файлов";
    $strings['btnload']['ru'] = "Загрузить файл";
    $strings['delete']['ru'] = "Удалить";
    $strings['view']['ru'] = "Просмотреть";
    $strings['choose']['ru'] = "Выбрать";
    $strings['deleteall']['ru'] = "Удалить выбранные файлы";
    $strings['createfolder']['ru'] = "Создать папку";
    $strings['direrror']['ru'] = "Директория не существует";
    /*    
    $strings['msgupload']['ru'] = "Файл загружен на сервер";
    $strings['msgloaderr']['ru'] = "Не удалось загрузить файл на сервер";
    $strings['msgtypeerr']['ru'] = "Не удалось загрузить файл на сервер. Возможно вы пытаетесь загрузить файл с другим расширением.";
    $strings['msgdirdel']['ru'] = "Директория была удалена";
    $strings['msgdirfull']['ru'] = "Не удалось удалить директорию, возможно она не существует или в она не пустая.";
    $strings['msgfiledel']['ru'] = "Файл был удален";
    $strings['msgdelerr']['ru'] = "Не удалось удалить файл.";
    */


    //$strings['title']['en'] = "File Manager";
    $strings['close']['en'] = "Close Window";    
    $strings['hdrload']['en'] = "Upload file to server";
    $strings['typelist']['en'] = "List of allowed file types";
    $strings['btnload']['en'] = "Upload file";
    $strings['delete']['en'] = "Delete";
    $strings['view']['en'] = "View";
    $strings['choose']['en'] = "Choose";
    $strings['deleteall']['en'] = "Delete selected files";
    $strings['createfolder']['en'] = "Create folder";
    $strings['direrror']['en'] = "Folder is not valid";
    
    /*
    $strings['msgupload']['en'] = "File uploaded to server";
    $strings['msgloaderr']['en'] = "Error occure while loading file to server";
    $strings['msgtypeerr']['en'] = "Error occure while loading file to server. The extention of the file is incorrect.";
    $strings['msgdirdel']['en'] = "Folder was removed";
    $strings['msgdirfull']['en'] = "Can't remove directory, the directory is possibly not empty.";
    $strings['msgfiledel']['en'] = "File was removed.";
    $strings['msgdelerr']['en'] = "Can't remove file.";
    */

    //$PAGE_HEADER['ru'] = $strings['title']['ru'];
    //$PAGE_HEADER['en'] = $strings['title']['en'];

    ////////////////////////////////////////////////////////////////////////////

    $MEM_STFOLDER = "";
    
    $CKEditor = GetParameter('CKEditor', NULL);
    if (isset ($CKEditor))
    {
    	$funcNum = GetParameter('CKEditorFuncNum', NULL) ;
    	$lang = GetParameter('langCode',$lang) ;
    }

    //$full = GetParameter("full", false);
    if( !$full )
    {
    	$lang = isset($CKEditor)? "ru" : "en"; //GetParameter( "lang", "en" );
    }


    /*
    $ROOT_FILE_DIR = "../files/";
	$MAX_UPLOAD_SIZE=5000000;


	$str_array=Array("../","/..","./","/.");

	$dir = GetParameter("dir", "");
	if( $dir == "" )
	{
		if( isset($_COOKIE['memfmdir']) && ($_COOKIE['memfmdir'] != "") )
		{
			$dir = $_COOKIE['memfmdir'];
		}
	}
	$cur_dir=str_replace($str_array,"",$dir);

	if( is_dir($ROOT_FILE_DIR.$cur_dir) && ($dir!="") )
	{
		setcookie("memfmdir", $cur_dir, time()+3600*3);
	}
	
    if( $full )
    	require_once "inc/header-inc.php";
    else
		require_once "inc/popup-header-inc.php";


	$tmp_dir=$ROOT_FILE_DIR.$cur_dir;
	*/


	$ext_arr=Array("jpg","jpeg","gif","bmp","png","zip","rar","xls","csv","pdf","doc","mp3","mid","midi","jar");

    $target = GetParameter("target", "");

 	//echo $mode."<br>";
	
	// Форма загрузки файла на сервер
	if( $this->error_msg != "" )
	{
		echo '<p class="error">'.$this->error_msg.'</p>';
	}
	
		/*
		echo $strings['typelist'][$lang].": ";
		for( $i=0; $i<count($ext_arr); $i++ )
		{
			if($i>0)
            {
				echo " ";
			}
			echo ".".$ext_arr[$i];
		}
		*/
?>
	<form class="filemanager-frm" action="<?=$this->Page_BuildUrl("fileman", "loadfile" );?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="mode" value="<?=$mode;?>Add">
	<input type="hidden" name="dir" value="<?=( $cur_dir == "" ? './' : $cur_dir);?>">
	<input type="hidden" name="target" value="<?=$target;?>">
	<input type="hidden" name="lang" value="<?=$lang;?>">
	<input type="hidden" name="full" value="<?=$full;?>">
	<input type="hidden" name="uan" value="<?=$uan;?>">
	<?php
		if( $MENU_HIDE == 1 )
			echo "<input type=\"hidden\" name=\"hide\" value=\"1\">";
	?>
	<div><?=$strings['hdrload'][$lang];?></div>
	<table class="tblfrm w100">
	<tr class="even topbrd">
		<td><input type="file" name="myfile" /></td>
		<td class="ta_right"><input type="image" src="<?=WWWHOST;?>admin/img/fileman-btn-upload.png" width="141" height="25" alt="<?=$strings['btnload'][$lang];?>" /></td>
	</tr>
	</table>
	</form>
<?php
if (isset($CKEditor))
{
?>
<script type="text/javascript">
	function ckedit_int(func_num, file_name)
	{
		window.opener.CKEDITOR.tools.callFunction(func_num, file_name);
		self.close();
	}
</script>
<?php
}

function cmpFN($a, $b)
{
	return strcasecmp($a,$b);
}

		//echo getcwd()."<br>".$tmp_dir."<br>";

		if(is_dir($tmp_dir))
		{
			// Показать форму создания каталога
			echo '<form class="filemanager-frm" action="'.$this->Page_BuildUrl("fileman", "createdir" ).'" method="post">
			<input type="hidden" name="target" value="'.$target.'">
            <input type="hidden" name="dir" value="'.( $cur_dir == "" ? './' : $cur_dir).'">
            <input type="hidden" name="mode" value="'.$mode.'Create">
            <input type="hidden" name="lang" value="'.$lang.'">
            <input type="hidden" name="full" value="'.$full.'">
            <input type="hidden" name="uan" value="'.$uan.'">
            '.( $MENU_HIDE==1 ? "<input type=\"hidden\" name=\"hide\" value=\"1\">" : "" ).'
			<div>'.$strings['createfolder'][$lang].'</div>
			<table class="tblfrm w100">
			<tr class="even topbrd">
				<td><input style="width: 350px;" type="text" name="fname" /></td>
				<td class="ta_right"><input type="image" src="'.WWWHOST.'admin/img/fileman-btn-newdir.png" width="175" height="25" alt="'.$strings['createfolder'][$lang].'" /></td>
			</tr>
			</table>
			</form>
			';

			if($handle = opendir($tmp_dir))
			{
				/* Именно этот способ чтения элементов каталога является правильным. */
				$allfiles=Array();
				while(false !== ($file = readdir($handle)))
                {
					$allfiles[]=$file;
				}

                closedir($handle);

                // Sort files in dir
                usort($allfiles, "cmpFN");
                reset($allfiles);

				$path_parts = pathinfo($cur_dir);

				if(isset($path_parts["dirname"]) && $path_parts["dirname"]=="\\") $path_parts["dirname"]="";
				//echo $path_parts["dirname"]."<br>\n";
				//echo $path_parts["basename"]."<br>\n";
				//echo $path_parts["extension"]."<br><br>\n";
				
				//echo "<br>".WWWHOST." - ".FILE_DIR." - ".$cur_dir."<br>";

				echo '<br />
				<form class="filemanager-frm" action="'.$this->Page_BuildUrl("fileman", "deletefiles" ).'" method="post">
				<input type="hidden" name="dir" value="'.( $cur_dir == "" ? './' : $cur_dir).'" />
				<input type="hidden" name="mode" value="'.$mode.'Fdels" />
				<input type="hidden" name="target" value="'.$target.'" />
				<input type="hidden" name="lang" value="'.$lang.'" />
				<input type="hidden" name="full" value="'.$full.'" />
				<input type="hidden" name="uan" value="'.$uan.'" />
				'.( $MENU_HIDE==1 ? '<input type="hidden" name="hide" value="1" />' : '' ).'
				<div class="fileman-path">Текущая папка: <span>'.WWWHOST.FILE_DIR.$cur_dir.( false ? ' ('.($cur_dir?$cur_dir:"./").')' : '' ).'</span></div>
				<table class="tbldat tbltopbrd t_black">
				<tr>
					<th>&nbsp;</th>
					<th>Файлы и папки</th>
					<th class="ls">Действия</th>
				</tr>';

                //if($dir!="" && $dir!="./")
                if($cur_dir!="" && $cur_dir!="./")
                {
                	echo '<tr class="even">
						<td>&nbsp;</td>
						<td><p class="fm-dir"><a href="'.$this->Page_BuildUrl("fileman", "", 'full='.$full.'&'.(isset($CKEditor) ? 'CKEditor=1&CKEditorFuncNum='.$funcNum.'&' : '').'lang='.$lang.'&uan='.$uan.'&dir='.$path_parts["dirname"].'/&target='.$target.($MENU_HIDE==1 ? '&hide=1':'')).'">..</a></p></td>
						<td class="ls">&nbsp;</td>
					</tr>';
				}

				$ind = 0;
				for($i=0; $i<count($allfiles); $i++)
				{
					if( ($allfiles[$i]!=".") && ($allfiles[$i]!="..") )
					{
						if( is_dir($tmp_dir.$allfiles[$i]) )
						{
							echo '<tr'.($ind % 2 == 0 ? ' class="even"' : '').'>
								<td>&nbsp;</td>
								<td><p class="fm-dir"><a href="'.$this->Page_BuildUrl("fileman", "", 'full='.$full.'&'.(isset($CKEditor) ? 'CKEditor=1&CKEditorFuncNum='.$funcNum.'&' : '').'lang='.$lang.'&uan='.$uan.'&dir='.$cur_dir.$allfiles[$i].'/&target='.$target.($MENU_HIDE==1 ? '&hide=1':'') ).'">'.$allfiles[$i].'</a></p></td>
								<td class="ls">
									<a href="'.$this->Page_BuildUrl("fileman", "deletedir", 'full='.$full.'&'.(isset($CKEditor) ? 'CKEditor=1&CKEditorFuncNum='.$funcNum.'&' : '').'lang='.$lang.'&uan='.$uan.'&mode='.$mode.'Delete&dir='.($cur_dir == "" ? './' : $cur_dir).'&fname='.$allfiles[$i].'&target='.$target.($MENU_HIDE==1 ? '&hide=1':'')).'" title="'.$strings['delete'][$lang].'"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="'.$strings['delete'][$lang].'" /></a>
								</td>
							</tr>';
							$ind++;
						}
					}
				}

   				for($i=0;$i<count($allfiles);$i++)
   				{
					if(($allfiles[$i]!=".")&&($allfiles[$i]!=".."))
					{
						if(is_file($tmp_dir.$allfiles[$i]))
						{
							echo '<tr'.($ind % 2 == 0 ? ' class="even"' : '').'>
								<td><input type="checkbox" name="arr[]" value="'.$allfiles[$i].'" /></td>
								<td>';
							if(isset($target) && $target!="")
							{
								echo '<a href="javascript:FileCh(\''.($uan != 0 ? WWWHOST.FILE_DIR : "").$cur_dir.$allfiles[$i].'\','.$target.');" class="fm-choose" title="'.$strings['choose'][$lang].'"></a>';
							}
							if( isset($CKEditor)&& $CKEditor )
							{
								echo '<a href="javascript:ckedit_int('.$funcNum.',\'/'.FILE_DIR.$cur_dir.$allfiles[$i].'\');" class="fm-choose" title="'.$strings['choose'][$lang].'"></a>';
							}
							echo '<p class="fm-file">'.$allfiles[$i].'</p>
								</td>
								<td class="ls">
									<a href="'.$tmp_dir.$allfiles[$i].'" target="_blank" title="'.$strings['view'][$lang].'"><img src="'.WWWHOST.'admin/img/a-view.png" width="22" height="22" alt="'.$strings['view'][$lang].'" /></a>&nbsp;
									<a href="'.$this->Page_BuildUrl("fileman", "deletefile", 'full='.$full.'&'.(isset($CKEditor) ? 'CKEditor=1&CKEditorFuncNum='.$funcNum.'&' : '').'lang='.$lang.'&uan='.$uan.'&mode='.$mode.'FDelete&dir='.$cur_dir.'&fname='.$allfiles[$i].'&target='.$target.($MENU_HIDE==1 ? '&hide=1':'') ).'" title="'.$strings['delete'][$lang].'"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="'.$strings['delete'][$lang].'" /></a>
								</td>
							</tr>
							';
							$ind++;
						}
					}
				}

				echo '</table>
				<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/fileman-btn-delall.png" alt="'.$strings['deleteall'][$lang].'" /></div>
				</form>';
			}
		}
		else
		{
			echo '<p class="error">'.$strings['direrror'][$lang].'</p>';
		}

        /*
		if(!empty($target))
		{
			echo "<input type=\"button\" value=\"Закрыть окно\" onclick=\"window.close()\">";
		}
		*/

    //if( $full )
	//	include "inc/footer-inc.php";
	//else
	//	include "inc/popup-footer-inc.php";

	//include "../inc/close-inc.php";
?>
