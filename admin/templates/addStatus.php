<table class="tbldat">
    <tr>
        <th>id</th>
        <th>Профиль</th>
        <th><a href="<?=$this->page_BuildUrl('angel','sort_day')?>">Сила за день</a></th>
        <th><a href="<?=$this->page_BuildUrl('angel','sort_week')?>">Сила за неделю</a></th>
        <th>Ангел дня</th>
        <th>Ангел недели</th>
        <th class="ls">
        </th>
    </tr>
    <tr>
    <?php
        foreach ($this->userlist as $usr) { ?>
            <td><?=$usr['buyer_id']?></td>
            <td><a href="<?=WWWHOST.'users/viewrev/'.$usr['buyer_id'].'/'?>"><?=$usr['name'].' '.$usr['fname']?></td>
            <td><?=$usr['money_day']?></a></td>
            <td><?=$usr['money_week']?></td>
            <td><?= ($usr['angel_day']) ? '<span style="color: green">Да</span>' : 'Нет'?></td>
            <td><?= ($usr['angel_week']) ? '<span style="color: green">Да</span>' : 'Нет'?></td>
            <td class="ls">
                <a href="<?=$this->page_BuildUrl( "angel", "angDay",'uid='.$usr['buyer_id'].'&active='.($usr['angel_day'] == 1 ? 0 : 1) )?>">Ангел дня <img src="<?=WWWHOST.'img/u-angel-day.png'?>" alt="" height="20px" width="27px"></a>
                <a href="<?=$this->page_BuildUrl( "angel", "angWeek",'uid='.$usr['buyer_id'].'&active='.($usr['angel_week'] == 1 ? 0 : 1) )?>">Ангел недели <img src="<?=WWWHOST.'img/u-andel-week.png'?>" alt="" height="20px" width="27px"></a>
            </td>
        </tr>
        <?php } ?>
</table>
<br>
<h1>Пользователи, которые имеют статус</h1>
<table class="tbldat">
    <tr>
        <th>id</th>
        <th>Профиль</th>
        <th><a href="<?=$this->page_BuildUrl('angel','sort_day')?>">Сила за день</a></th>
        <th><a href="<?=$this->page_BuildUrl('angel','sort_week')?>">Сила за неделю</a></th>
        <th>Ангел дня</th>
        <th>Ангел недели</th>
        <th class="ls">
        </th>
    </tr>
    <tr>
    <?php
    foreach ($this->userStatuslist as $usr) { ?>
        <td><?=$usr['buyer_id']?></td>
        <td><a href="<?=WWWHOST.'users/viewrev/'.$usr['buyer_id'].'/'?>"><?=$usr['name'].' '.$usr['fname']?></td>
        <td><?=$usr['money_day']?></a></td>
        <td><?=$usr['money_week']?></td>
        <td><?= ($usr['angel_day']) ? '<span style="color: green">Да</span>' : 'Нет'?></td>
        <td><?= ($usr['angel_week']) ? '<span style="color: green">Да</span>' : 'Нет'?></td>
        <td class="ls">
            <a href="<?=$this->page_BuildUrl( "angel", "angDay",'uid='.$usr['buyer_id'].'&active='.($usr['angel_day'] == 1 ? 0 : 1) )?>">Ангел дня <img src="<?=WWWHOST.'img/u-angel-day.png'?>" alt="" height="20px" width="27px"></a>
            <a href="<?=$this->page_BuildUrl( "angel", "angWeek",'uid='.$usr['buyer_id'].'&active='.($usr['angel_week'] == 1 ? 0 : 1) )?>">Ангел недели <img src="<?=WWWHOST.'img/u-andel-week.png'?>" alt="" height="20px" width="27px"></a>
        </td>
        </tr>
    <?php } ?>
</table>
