<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	//include "../inc/utils-inc.php";
	//include "../inc/countryutils-inc.php";
	
	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();
	
	$UHCMS_MODNAME = "comments";

	$fields['name']['ru'] = 'Имя';
	$fields['fname']['ru'] = 'Фамилия';
	$fields['login']['ru'] = 'Логин';
	$fields['passwd']['ru'] = 'Пароль';
	$fields['address']['ru'] = 'Адрес';
	$fields['city']['ru'] = 'Город';
	$fields['country']['ru'] = 'Страна';
	$fields['zip_code']['ru'] = 'Почтовый Индекс';
	$fields['telephone']['ru'] = 'Тел.';
	$fields['office_phone']['ru'] = 'Раб.';
	$fields['cell_phone']['ru'] = 'Моб.';
	$fields['email1']['ru'] = 'E-Mail';
	$fields['email2']['ru'] = 'E-Mail';
	$fields['email3']['ru'] = 'E-Mail';
	$fields['web_url']['ru'] = 'Веб-страница';
	$fields['groupid']['ru'] = 'Группа пользователей';
	$fields['trader']['ru'] = 'Привязка к трейдеру';

	$fields['name']['en'] = 'Name';
	$fields['fname']['en'] = 'Sirname';
	$fields['login']['en'] = 'Login';
	$fields['passwd']['en'] = 'Password';
	$fields['address']['en'] = 'Address';
	$fields['city']['en'] = 'City';
	$fields['country']['en'] = 'Country';
	$fields['zip_code']['en'] = 'Zip';
	$fields['telephone']['en'] = 'Telephone';
	$fields['office_phone']['en'] = 'Office Phone';
	$fields['cell_phone']['en'] = 'Cell Phone';
	$fields['email1']['en'] = 'E-Mail 1';
	$fields['email2']['en'] = 'E-Mail 2';
	$fields['email3']['en'] = 'E-Mail 3';
	$fields['web_url']['en'] = 'Web-url';
	$fields['groupid']['en'] = 'User Group';
	$fields['trader']['en'] = 'Trader Assign';

	$strings['editprof']['en'] = "Edit project's profile";
	$strings['newprof']['en'] = "Add user profile";
	$strings['projlist']['en'] = "Projects List";
	$strings['addbtn']['en'] = " Add New ";
	$strings['applybtn']['en'] = " Apply ";
	$strings['rowlogin']['en'] = "Login";
	$strings['rowname']['en'] = "Name";
	$strings['rowaddr']['en'] = "Address";
	$strings['rowcontact']['en'] = "Contacts";
	$strings['tipchangepwd']['en'] = "Change Password";
	$strings['tipedituser']['en'] = "Edit User Profile";
	$strings['tipdeluser']['en'] = "Delete User";
	$strings['tipstatus']['en'] = "Change Status";

	$strings['editprof']['ru'] = "Редактирование Комментария";
	$strings['newprof']['ru'] = "Добавление Нового Пользователя";
	$strings['projlist']['ru'] = "Список Комментариев";
	$strings['addbtn']['ru'] = " Добавить ";
	$strings['applybtn']['ru'] = " Применить ";
	$strings['rowlogin']['ru'] = "Пользователи";
	$strings['rowname']['ru'] = "Ф.И.О.";
	$strings['rowaddr']['ru'] = "Адрес";
	$strings['rowcontact']['ru'] = "Контакты";
	$strings['tipchangepwd']['ru'] = "Изменить Пароль";
	$strings['tipedituser']['ru'] = "Редактировать Профиль";
	$strings['tipdeluser']['ru'] = "Удалить Пользователя";
	$strings['tipstatus']['ru'] = "Изменить Статус";

	////////////////////////////////////////////////////////////////////////////
	
	$citylist = Array();	//GetCityList($LangId);

	////////////////////////////////////////////////////////////////////////////
	/*
	$action = GetParameter("action", "");
	$uid = GetParameter("uid", "");

	$uprof = Array();

	$uprof['name'] = GetParameter("fullname", "");
	$uprof['passwd'] = GetParameter("passwd", "");
	$uprof['login'] = GetParameter("login", "");
	$uprof['address'] = GetParameter("address", "");
	$uprof['city_id'] = GetParameter("city_id", 0);
	$uprof['country'] = GetParameter("country", "");
	$uprof['zip_code'] = GetParameter("zip", "");
	$uprof['telephone'] = GetParameter("phone1", "");
	$uprof['office_phone'] = GetParameter("phone2", "");
	$uprof['cell_phone'] = GetParameter("phone3", "");
	$uprof['email1'] = GetParameter("email1", "");
	$uprof['email2'] = GetParameter("email2", "");
	$uprof['email3'] = GetParameter("email3", "");
	$uprof['web_url'] = GetParameter("weburl", "");
	$uprof['groupid'] = GetParameter("userlevel", "0");
	$uprof['usertrader'] = GetParameter("usertrader", 0);
	
	switch( $action )
	{
		case "add":
			if( $UserGroup == $GROUP_ADMIN )
  			{
  				$newuid = makeUuid();
        		if( !mysql_query("INSERT INTO $TABLE_USERS
        							(login, passwd, activation_code, isactive, name, address, city_id, zip_code, telephone,
        							office_phone, cell_phone, email1, email2, email3, web_url, group_id)
	                        VALUES ('".addslashes($uprof['login'])."', PASSWORD('".addslashes($uprof['passwd'])."'), '$newuid', 1, '".addslashes($uprof['name'])."',
	                        '".addslashes($uprof['address'])."', '".addslashes($uprof['city_id'])."', '".addslashes($uprof['zip_code'])."',
	                        '".addslashes($uprof['telephone'])."', '".addslashes($uprof['office_phone'])."', '".addslashes($uprof['cell_phone'])."',
	                        '".addslashes($uprof['email1'])."', '".addslashes($uprof['email2'])."', '".addslashes($uprof['email3'])."',
	                        '".addslashes($uprof['web_url'])."', '".$uprof['groupid']."')"))
            	{
	                echo "<b>".mysql_error()."</b>";
	            }
  			}
			else
  			{
  				header("Location: forbidden.php");
  				exit;
  			}
			break;

		case "status":
  			if(isset($_GET['active']))		$active = $_GET['active'];
  			if(empty($uid))					$uid = $_GET['uid'];

			if(!mysql_query("UPDATE $TABLE_USERS SET isactive=".(($active==0) ? "1" : "NULL")." WHERE id=$uid"))
			{
				echo mysql_error();
			}
  			break;

		case "delete":
			if(empty($uid))			$uid = $_GET['uid'];
			if( $uid == $UserId )	break;

			if(mysql_query("DELETE FROM $TABLE_USERS WHERE id='$uid' AND id<>1"))
			{
				// Make user records cleaning
			}
			else
			{
				echo mysql_error();
			}
  			break;

		case "applynew":
			if( !mysql_query("UPDATE $TABLE_USERS SET login='".addslashes($uprof['login'])."',
						name='".addslashes($uprof['name'])."', address='".addslashes($uprof['address'])."',
						city_id='".addslashes($uprof['city_id'])."',
        				zip_code='".addslashes($uprof['zip_code'])."', telephone='".addslashes($uprof['telephone'])."',
        				office_phone='".addslashes($uprof['office_phone'])."', cell_phone='".addslashes($uprof['cell_phone'])."',
        				email1='".addslashes($uprof['email1'])."', email2='".addslashes($uprof['email2'])."',
        				email3='".addslashes($uprof['email3'])."', web_url='".addslashes($uprof['web_url'])."',
        				group_id='".$uprof['groupid']."' WHERE id=".$_POST['uid']." "))
			{
				echo "<b>".mysql_error()."</b>";
			}

			$action = "editprofile";
			$uid = $_POST['uid'];
			break;
	}
	*/

	if( $this->viewMode == "editcomment" )
	{
		$reqi = $this->cominfo;		
	}
	
	echo $this->viewMode;
	
	if( $this->viewMode == "editcomment" )
	{
?>
	<br /><br />
    <h2><?=($this->viewMode == "editcomment" ? $strings['editprof'][$lang] : $strings['newprof'][$lang]);?></h2>
	<div class="dd-sep"></div>
	<form class="w600-frm" action="<?=( $this->viewMode == "editcomment" ? $this->page_BuildUrl("comments", "savecomment") : $this->page_BuildUrl("comments", "") );?>" method="post">
	<input type="hidden" name="action" value="<?=($this->viewMode == "editcomment" ? "applynew" : "add");?>" />
	<?=( $this->viewMode == "editcomment" ? '<input type="hidden" name="cid" value="'.$reqi['id'].'" />' : '' );?>
	<table class="tblfrm">
	<tr>
		<th colspan="2">Общие данные</th>
	</tr>
    <tr>
	    <td class="ff">Оценка:</td>
	    <td><select name="prate">
	<?php
		for( $i=1; $i<=10; $i++ )
		{
			echo '<option value="'.$i.'"'.($reqi['rate'] == $i ? ' selected' : '').'>'.$i.'</option>';
		}
	?>
	    </select></td>
	</tr>
	<tr class="even">
	    <td class="ff">Видимый:</td>
	    <td><select name="pfirst">
	    	<option value="0">НЕТ</option>
	    	<option value="1"<?=($reqi['visible'] == 1 ? ' selected' : '')?>>ДА</option>
	    </select></td>
	</tr>
	<tr>
		<th colspan="2">Дополнительные сведения</th>
	</tr>
	<tr class="even">
	    <td class="ff">Текст комментария:</td>
	    <td><textarea name="pdescr0" cols="70" rows="8"><?=$reqi['content'];?></textarea></td>
	</tr>
	</table>
<?php
	if( $this->viewMode == "editcomment" )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-user-save.png" alt="Сохранить изменения" /></div>';
	}
	else
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-user-add.png" alt="Добавить нового пользователя" /></div>';
	}
?>
	</form>
<?php
	}
	
	//var_dump($usergroups);
?>


    <!-- PART OF PAGE TO DISPLAY USER'S LIST -->
    <br /><br />
	<h2><?=$strings['projlist'][$lang];?></h2>
	<table class="tbldat">
	<tr>
		<th>Пользователь</th>
		<th>Группа</th>
		<th>Проект</th>
		<th>Видимый</th>
		<!--<th>Объем</th>-->
		<th>Дата</th>
		<th>Комментарий</th>
		<!--<th>Статус</th>-->
		<th class="ls">Действия</th>
	</tr>
    <?php
		$its = $this->commentlist;
		
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			$user_city = "";
			$user_country = "";					
			
			$pstatus_str = '';
			
			/*
			switch( $row['req_status'] )
			{
				case REQ_STATUS_NEW:		$pstatus_str = 'Новый';	break;
				case REQ_STATUS_DECLINE:	$pstatus_str = 'Отклон.';	break;
				case REQ_STATUS_CONFIRM:	$pstatus_str = '<span style="color:#22FF22;">Принят</span>';	break;
				case REQ_STATUS_CANCEL:		$pstatus_str = '<span style="color:#22FF22;">Отменен</span>';	break;
			}
			*/
			
			$pstatus_str = $row['rate'];

			echo '<tr'.( ($i+1) % 2 == 1 ? ' class="even"' : '' ).'>
				<td class="p-name">'.( 
					$row['account_type'] == USR_TYPE_PERS ? 
						stripslashes($row['name']).' '.stripslashes($row['fname']) :
						stripslashes($row['orgname']).'<br><span>'.stripslashes($row['name']).' '.stripslashes($row['fname'].'</span>')
				).'</td>
				<td>'.$row['account_group'].'</td>
				<td class="p-name">'.stripslashes($row['title2']).'</td>
				<td>'.( $row['visible'] == 1 ? '<span style="color:red;">Да</span>' : 'Нет' ).'</td>
				'.( false ? '<td>'.$row['req_amount'].'</td>' : " " ).'
				<td>
					'.$row['add_date'].'
				</td>
				<td>'.$row['content'].'</td>
				<td class="ls">
					'.( false ? '<a href="'.$this->page_BuildUrl( "comments", "status", 'rid='.$row['id'].'&active='.($row['status'] == NULL ? 0 : 1) ).'" title="Остановить проект"><img src="'.WWWHOST.'admin/img/a-usr-dis.png" width="22" height="22" alt="Остановить пользователя" /></a>&nbsp;' : '' ).'
					<a href="'.$this->page_BuildUrl( "comments", "editcomment", 'cid='.$row['id'] ).'" title="Редактировать"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="Редактировать" /></a>&nbsp;
					<a onclick="return confirm(\'Вы хотите удалить этот запрос. Действительно удалить запрос?\')" href="'.$this->page_BuildUrl( "comments", "deletecomment", 'cid='.$row['id'] ).'" title="Удалить"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="Удалить" /></a>
				</td>
			</tr>';
		}		
    ?>
	</table>

<?php
	//include "inc/footer-inc.php";
	//include "../inc/close-inc.php";
?>
