<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;
	
	$LangId = $PM->getLangId();


	//$FILE_DIR = "../files/";
	$arrMonths =  Array("January","February","March","April","May","June","July","August","September","October","November","December");
	//include "../inc/ses-inc.php";

	////////////////////////////////////////////////////////////////////////////
	//		Interface Language
    //$lang = "ru";
	/*
    $is_UTF8 = true;
    if( empty($_ENV) || empty($_ENV['LANG']) )
    {
		$is_UTF8 = false;
    }
    else
    {
    	if( strpos($_ENV['LANG'], "UTF-8") === false )
    	{
    		$is_UTF8 = false;
    	}
    }
    $is_UTF8 = false;
	*/

	////////////////////////////////////////////////////////////////////////////
	// Meta data and navigation
	$P_TITLE = ( $PM->getViewLang() == "en" ? "Website Control Panel: ".$this->cfg['NAME_EN']." - UH CMS" : "Центр управления сайтом: ".$this->cfg['NAME_RU']." - UH CMS" );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
	<title><?=$P_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="<?=WWWHOST;?>admin/css/all.css" />
	<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="<?=WWWHOST;?>admin/css/ie.css" />
	<![endif]-->
	<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="<?=WWWHOST;?>admin/css/ltie7.css" />
	<![endif]-->
	<link type="text/css" rel="stylesheet" href="<?=WWWHOST;?>css/all_print.css" />
<script type="text/javascript">
var reqajxhost = '<?=WWWHOST;?>';
</script>
	<script language="javascript" src="<?=WWWHOST;?>admin/js/utils.js"></script>
	<script language="javascript" src="<?=WWWHOST;?>admin/js/script_cat.js"></script>

	<script type="text/javascript" src="<?=WWWHOST;?>js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?=WWWHOST;?>ckeditor/ckeditor.js"></script>

	<script type="text/javascript" language="javascript" charset="windows-1251" src="<?=WWWHOST;?>admin/js/basic.js"></script>
	<script type="text/javascript" language="javascript" charset="windows-1251" src="<?=WWWHOST;?>admin/js/myajax.js"></script>
	<script type="text/javascript" language="javascript" charset="windows-1251" src="<?=WWWHOST;?>admin/js/config.js"></script>
<script type="text/javascript">
$(document).ready(function(){<?php
if( isset($UHCMS_MODNAME) )
{	if( $UHCMS_MODNAME == "user_list" )
	{	?>
	$(".u-slide a").bind("click", function(){
		$(this).toggleClass("a-collapse");
		$(this).find("span").html( $(this).attr("class") == "a-slide" ? "Развернуть" : "Свернуть" );
		if( $(this).attr("class") == "a-slide" )
		{
			$(this).parent().prev().slideUp('fast');
		}
		else
		{
			$(this).parent().prev().slideDown('fast');
		}
		return false;
	});
	<?php
	}
}
?>
	$(".tbldat tr").mouseenter( function(){
		$(this).toggleClass("highlight");
	});

	$(".tbldat tr").mouseleave( function(){
		$(this).toggleClass("highlight");
	});
});
</script>
</head>
<body>
	<div id="page">
		<div id="header">
			<div id="logo">
				<a href="http://uhdesign.com.ua/" target="_blank"><img class="block" src="<?=WWWHOST;?>admin/img/uh_logo.gif" alt="Центр управления сайтом - UH Design" width="199" height="44" /></a>
			</div>
			<div class="tright2">
				<div class="tmenu">
					Администратор &nbsp;<span class="usr"><?=( UhCmsAdminApp::getSesInstance()->UserName );?></span>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<a class="exit" href="<?=$this->page_BuildUrl("");?>?action=logout">Выйти</a>
				</div>
			</div>
			<div class="tright">
				<div class="tmenu">
			<?php
				$langs = $PM->getLangs();
				
				//var_dump($langs);				
				//if(count($langs)>1)
				//{
					echo 'Языки: ';
					for( $i=0; $i<count($langs); $i++ )
					{
						echo '&nbsp;&nbsp;&nbsp;&nbsp; <a href="'.$_SERVER['REQUEST_URI'].'?lang_id='.$langs[$i]['lang_code'].'" '.($langs[$i]['id'] == $LangId ? ' class="active"' : '').'>'.substr($langs[$i]['lang_name'], 0, 3).'</a>';
					}
				//}
			?>
				</div>
			</div>
			<div class="thome">
				<div>
					<a href="<?=WWWHOST;?>" target="_blank">Посмотреть сайт</a></p>
				</div>
			</div>
			<div class="both"></div>
		</div>
		<div id="main">
			<div class="both"></div>
			<div id="leftc">
		<?php
			$menuadm = $PM->loadAdminMenu();
			
			$menu_sections = $menuadm['menu_sections'];
			$menu_items = $menuadm['menu_items'];
			
			for($i = 0; $i < count($menu_sections); $i++)
			{
            	$section_name = $menu_sections[$i];
                $section_items = $menu_items[$i];

                echo '<div class="lblock">
					<div class="lblk-group">'.$section_name.'</div>
				';
                for($j = 0; $j < count($section_items); $j++)
                {
                	$item_name = $section_items[$j]["name"];
                	$item_link = $section_items[$j]["link"];

                    echo '<div><a href="'.( strncmp($item_link, "http://", 7) == 0 ? $item_link : WWWHOST."admin/".$item_link ).'" class="menulink">'.$item_name.'</a></div>';
                }

                echo '</div>';
            }
		?>
			</div>
			<div id="centc">
				<div id="breadcrumbs">
		<?php
			$BCROOTNAME = "Главная";
			$BCROORURL = $this->page_BuildUrl("");

			echo '<a href="'.$BCROORURL.'">'.$BCROOTNAME.'</a> › ';

			if( isset($this->breadcrumbs) && (count($this->breadcrumbs)>0) )
			{
				for( $i=0; $i<count($this->breadcrumbs); $i++ )
				{
					echo '<a href="'.$this->breadcrumbs[$i]['url'].'">'.$this->breadcrumbs[$i]['name'].'</a> › ';
				}
			}
		?>
				</div>
				<h1><?=$this->PAGE_HEADER[$PM->getViewLang()];?></h1>
