<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2011                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

	$PM = $this->pageModel;

	$LangId = $PM->getLangId();
	$lang = $PM->getViewLang();

    ////////////////////////////////////////////////////////////////////////////
    // Module defines
	$strings['tipedit']['en'] = "Edit splash slides";
   	$strings['tipdel']['en'] = "Delete This Slide";
	$strings['nolist']['en'] = "In the database there is no slide";
	$strings['rowfunc']['en'] = "";
	$strings['deleteconfirm']["en"] = "Are you sure you want to delete this item?";
	$strings['bclist']['en'] = "Slide list";

	$strings['tipedit']['ru'] = "Редактировать валюту";
   	$strings['tipdel']['ru'] = "Удалить валюту";
	$strings['nolist']['ru'] = "В базе нет ни одной валюты";
	$strings['rowfunc']['ru'] = "";
	$strings['deleteconfirm']["ru"] = "Вы действительно хотите удалить эту валюту?";
	$strings['bclist']['ru'] = "Список валют";

	////////////////////////////////////////////////////////////////////////////	

    if( $this->viewMode == "edit" )
    {
		$req = $this->curinfo;		
?>

	<br /><br />
    <h2>Редактировать</h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->page_BuildUrl("currency", "save");?>" method="post">
	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="id" value="<?=$req['id'];?>" />
	<table class="tblfrm w100">
    <tr class="even"><td class="ff">Название:</td><td><input type="text" size="70" name="orgname" value="<?=$req['name'];?>" /></td></tr>
    <tr><td class="ff">Код валюты:</td><td><input type="text" size="6" name="orgcode" value="<?=$req['cur_code'];?>" /></td></tr>
    <tr class="even"><td class="ff">Порядковый номер:</td><td><input type="text" size="30" name="orgsort" value="<?=$req['sort_num'];?>" /></td></tr>
    </table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-save.png" alt=" Сохранить изменения " /></div>
	</form>
<?php
	}
	else
	{
?>
	<h2>Список валют</h2>
    <form action="<?=$this->page_BuildUrl("currency", "deleteall");?>" method=POST>
    <input type="hidden" name="action" value="delete" />
    <table class="tbldat">
	<tr>
		<th>&nbsp;</th>
		<th>Название</th>
		<th>Код</th>
		<th class="ls">Действия</th>
	</tr>
<?php
		$its = $this->curlist;
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			echo '<tr'.( ($i+1) % 2 == 1 ? ' class="even"' : '' ).'>
				<td><input type="checkbox" name="items_id[]" value="'.$row['id'].'" /></td>
				<td class="p-name">'.stripslashes($row['name']).'</td>
				<td>'.stripslashes($row['cur_code']).'</td>
				<td class="ls">
					<a href="'.$this->Page_BuildUrl("currency", "edit", 'action=edititem&id='.$row['id']).'" title="'.$strings['tipedit'][$lang].'"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
					<a href="'.$this->Page_BuildUrl("currency", "delete", 'action=deleteitem&id='.$row['id']).'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>
				</td>
			</tr>';
		}
		
		$found_items = count($its);		

		if( $found_items == 0 )
		{
			echo '<tr><td colspan="4" class="ls">'.$strings['nolist'][$lang].'</td></tr>';
        }
?>
    </table>
<?php
	if( $found_items > 0 )
	{
		echo '<div class="frmbtn"><input type="image" src="'.WWWHOST.'admin/img/btn-del-selected.png" alt="Удалить выбранные" /></div>';
	}
?>
    </form>


	<br /><br />
    <h2>Добавить валюту</h2>
	<div class="dd-sep"></div>
	<form name="catfrm" id="catfrm" action="<?=$this->page_BuildUrl("currency", "add");?>" method="post">
	<input type="hidden" name="action" value="add" />
	<table class="tblfrm w100">
    <tr class="even"><td class="ff">Название:</td><td><input type="text" size="70" name="orgname"></td></tr>
    <tr><td class="ff">Код валюты:</td><td><input type="text" size="6" name="orgcode" /></td></tr>
    <tr class="even"><td class="ff">Порядковый номер:</td><td><input type="text" size="30" name="orgsort"></td></tr>
    </table>
	<div class="frmbtn"><input type="image" src="<?=WWWHOST;?>admin/img/btn-add-new.png" alt="Добавить слайд" /></div>
	</form>
<?php
    }
?>
