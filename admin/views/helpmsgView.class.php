<?php

class HelpmsgView extends AdmPageView {
    protected $model;
    public $viewMode;

    function __construct($config, $pModel, $catModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Сообщение от пользователей";
        $this->PAGE_HEADER['en'] = "User messages";
    }

    public function render_main() {
        $this->renderPage("helpmsg.php");
    }
}