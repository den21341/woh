<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class NewsView extends AdmPageView
{
	protected $model;
	protected $catUtilsView;
	private $msg;
	public $viewMode;
	public $viewGroup;
	protected $pi;
	protected $pn;
	
	function __construct($config, $pModel, $newsModel, $catModel, $pi, $pn)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $newsModel;
		$this->catUtilsView = $catModel;
		
		$this->viewMode = "";
		$this->viewGroup = 0;
		
		$this->pi = $pi;
		$this->pn = $pn;
		
		$this->PAGE_HEADER['ru'] = "Управление новостями и статьями";
		$this->PAGE_HEADER['en'] = "News Editing";
	}
	
	protected function drawSectCombo($sel_sect_id=0)
	{
		echo $this->catUtilsView->comboSects($sel_sect_id);
	}
	
	public function render_main()
	{			
		$this->renderPage("news.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "edit";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("news"), "Список новостей" );
		
		$this->renderPage("news.php");
	}
}
?>