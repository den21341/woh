<?php

class TestprojView extends AdmPageView {
    protected $model;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $catModel, $catViewModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;
        $this->catUtilsView = $catViewModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Опрос";
        $this->PAGE_HEADER['en'] = "Test";
    }

    public function render_main() {
        $this->renderPage("testproj.php");
    }
}