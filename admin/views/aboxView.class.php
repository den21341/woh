<?php

class AboxView extends AdmPageView {
    protected $model;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $catModel, $catViewModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;
        $this->catUtilsView = $catViewModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Добавление силы пользователям";
        $this->PAGE_HEADER['en'] = "Power Management";
    }

    public function render_main() {
        $this->renderPage("addPower.php");
    }
}