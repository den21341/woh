<?php

class BotsView extends AdmPageView {
    protected $model;
    public $viewMode;

    function __construct($config, $pModel, $catModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Управление ботами";
        $this->PAGE_HEADER['en'] = "Bots Management";
    }

    public function render_main() {
        $this->renderPage("bots.php");
    }
}