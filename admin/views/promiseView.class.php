<?php

class PromiseView extends AdmPageView {
    protected $model;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $catModel, $catViewModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;
        $this->catUtilsView = $catViewModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Обещания пользователей";
        $this->PAGE_HEADER['en'] = "Users promises";
    }

    public function render_main() {
        $this->renderPage("promise.php");
    }
}