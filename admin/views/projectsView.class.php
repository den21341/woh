<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ProjectsView extends AdmPageView
{
	protected $model;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $catModel, $catViewModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $catModel;		
		$this->catUtilsView = $catViewModel;
		
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Управление Проектами";
		$this->PAGE_HEADER['en'] = "Project Management";
	}
	
	public function render_main()
	{			
		$this->renderPage("project_list.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "editproj";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("projects"), "Список проектов" );
		
		$this->renderPage("project_list.php");
	}
	
	protected function drawSectCombo($sgroup, $sel_sect_id=0)
	{
		echo $this->catUtilsView->comboSects($sgroup, $sel_sect_id);
	}
}
?>