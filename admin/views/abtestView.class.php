<?php

class AbtestView extends AdmPageView {
    protected $model;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $catModel, $catViewModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;
        $this->catUtilsView = $catViewModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "A/B тестирование";
        $this->PAGE_HEADER['en'] = "A/B testing";
    }

    public function render_main() {
        $this->renderPage("abtest.php");
    }
}