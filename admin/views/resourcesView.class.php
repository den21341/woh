<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ResourcesView extends AdmPageView
{
	protected $resModel;
	private $msg;
	
	function __construct($config, $pModel, $resModel)
	{
		parent::__construct($config, $pModel);
		
		$this->resModel = $resModel;
		
		$this->PAGE_HEADER['ru'] = "Редактировать Текстовые Блоки Сайта";
		$this->PAGE_HEADER['en'] = "Edit Text Blocks";
	}
	
	public function render_main()
	{			
		$this->renderPage("page_res.php");
	}	
}
?>