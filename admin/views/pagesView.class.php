<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class PagesView extends AdmPageView
{
	protected $pgsModel;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $pgsModel)
	{
		parent::__construct($config, $pModel);
		
		$this->pgsModel = $pgsModel;
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Редактировать список скриптов";
		$this->PAGE_HEADER['en'] = "Page Headers Editing";
	}
	
	public function render_main()
	{			
		$this->renderPage("page_manager.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "editpage";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("pages"), "Список страниц" );
		
		$this->renderPage("page_manager.php");
	}
	
	public function render_editvideo($videoid)
	{
		$this->viewMode = "editvideo";
		$this->videoid = $videoid;
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("pages"), "Список страниц" );
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("pages", "edit", "id=".$this->pageinfo['id']), $this->pageinfo['pmean'] );
		
		$this->renderPage("page_manager.php");
	}
	
	public function render_editphoto($photoid)
	{
		$this->viewMode = "editphoto";
		$this->photoid = $photoid;
	
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("pages"), "Список страниц" );
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("pages", "edit", "id=".$this->pageinfo['id']), $this->pageinfo['pmean'] );
	
		$this->renderPage("page_manager.php");
	}
	
	protected function tablePagesList()
	{
		return $this->drawPagesLevelTable(0, 0);
	}
	
	protected function comboPagesList($selid)
	{
		return $this->drawPagesLevelCombo(0, 0, $selid);
	}
	
	protected function drawPagesLevelCombo($pid, $level, $selid=0)
	{
		$num_pages = 0;
		
		$its = $this->pgsModel->get_pageLevel($pid);
		
		for( $i=0; $i<count($its); $i++ )
		{			
			$row = $its[$i];			
			$num_pages++;
			
			$str_space = "";
    		for($j=0; $j<$level; $j++)
    			$str_space .= "&nbsp;&nbsp;&nbsp;";
			echo "<option value=\"".$row['id']."\"".($selid == $row['id'] ? " selected" : "").">".$str_space.stripslashes($row['page_mean'])."</option>";			
			
			$pres = $this->drawPagesLevelCombo($row['id'], $level+1, $selid);
			$num_pages += $pres;
		}
	}
	
	protected function drawPagesLevelTable($pid, $level)
	{
		//global $TABLE_PAGES, $TABLE_PAGES_LANGS, $LangId, $PHP_SELF, $strings, $lang;
		
		$strings["tipedit"]["ru"] = "Редактировать";
		$strings["tipdel"]["ru"] = "Удалить";    
		$strings["deleteconfirm"]["ru"] = "При удаление вся информация связанная с выбранной страницей будет удалена.\\r\\nУдалить?";
		
		$strings["tipedit"]["en"] = "Edit item";
		$strings["tipdel"]["en"] = "Delete item";
		
		$lang = $this->pageModel->getViewLang();

		$menu_type_arr = Array("Страницы, не отображающиеся в меню", "Страницы в главном меню", "Страницы в левом меню", "Страницы для зарегистрированных");

		$num_pages = 0;
		$show_in_menu = -1;
		
		$its = $this->pgsModel->get_pageLevel($pid);
		
		for( $i=0; $i<count($its); $i++ )
		{
			$row = $its[$i];
			
			if( ($show_in_menu != $row['show_in_menu']) && ($level == 0) )
			{
				$show_in_menu = $row['show_in_menu'];
				echo '<tr>
					<td colspan="9" class="tblh2">'.$menu_type_arr[$row['show_in_menu']].'</td>
				</tr>';
			}

			$num_pages++;

			/*
			$link_file = "";
			switch( $row['page_record_type'] )
			{
				case 1:	$link_file = stripslashes($row['page_name']).'.php';				break;
				case 2:	$link_file = stripslashes($row['page_name']).'/';					break;
				case 3:	$link_file = "info.php?page=".stripslashes($row['page_name']);	break;
				case 4: $link_file = stripslashes($row['page_name']);						break;
			}
			*/

			$vid_num = $this->pgsModel->pageLib()->Page_VideoNum($row['id']);
			$pic_num = $this->pgsModel->pageLib()->Page_PhotoNum($row['id']);

			$pagecls = "page-nomenu";
			$pagestyle = "";
			switch( $row['show_in_menu'] )
			{
				case 1:
					if( $level == 0 )
						$pagecls = "page-menu";
					else if( $level == 1 )
						$pagecls = "page-msub1";
					else
					{
						$pagecls = "page-msub2";
						if( $level > 2 )
						{
							$pagestyle = ' style="margin-left: '.(12+24*($level-1)).'px"';
						}
					}
					break;
			}

			echo '<tr'.( $level == 0 ? ' class="even"' : '' ).'>
				<td><input type="checkbox" name="items_id[]" value="'.$row['id'].'" /></td>
				<td>'.$row['sort_num'].'</td>
				<td><p class="'.$pagecls.'"'.$pagestyle.'>'.stripslashes($row['page_mean']).'</p></td>
				<td>'.stripslashes($row['page_name']).'</td>
				<td class="dgray">'.$row['link'].'</td>
				'.( $pic_num == 0 ? '<td class="dgray">—</td>' : '<td>'.$pic_num.' шт</td>' ).'
				'.( $vid_num == 0 ? '<td class="dgray">—</td>' : '<td>'.$vid_num.' шт</td>' ).'
				<td class="dt">'.$row['dd'].'<br /><span>'.$row['tm'].'</span></td>
				<td class="ls">
					<a href="'.$this->page_BuildUrl("pages", "edit", 'id='.$row['id']).'" title="'.$strings['tipedit'][$lang].'"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
					<a href="'.$this->page_BuildUrl("pages", "delete", 'id='.$row['id']).'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>
				</td>
			</tr>';
			
			$pres = $this->drawPagesLevelTable($row['id'], $level+1);
			$num_pages += $pres;
		}

		/*
		if( $res = mysql_query("SELECT p1.*, p2.page_mean, DATE_FORMAT(create_date, '%d.%m.%Y') as dd, DATE_FORMAT(create_date, '%H.%i') as tm
				FROM $TABLE_PAGES p1
				INNER JOIN $TABLE_PAGES_LANGS p2 ON p1.id=p2.item_id AND p2.lang_id='$LangId'
				WHERE p1.parent_id=$pid
				ORDER BY p1.show_in_menu, p1.sort_num") )
		{
			while( $row = mysql_fetch_object($res) )
			{
				if( ($show_in_menu != $row->show_in_menu) && ($level == 0) )
				{
					$show_in_menu = $row->show_in_menu;
					echo '<tr>
						<td colspan="9" class="tblh2">'.$menu_type_arr[$row->show_in_menu].'</td>
					</tr>';
				}

				$num_pages++;

				$link_file = "";
				switch( $row->page_record_type )
				{
					case 1:	$link_file = stripslashes($row->page_name).'.php';				break;
					case 2:	$link_file = stripslashes($row->page_name).'/';					break;
					case 3:	$link_file = "info.php?page=".stripslashes($row->page_name);	break;
					case 4: $link_file = stripslashes($row->page_name);						break;
				}

				$vid_num = Page_VideoNum($row->id);
				$pic_num = Page_PhotoNum($row->id);

				$pagecls = "page-nomenu";
				$pagestyle = "";
				switch( $row->show_in_menu )
				{
					case 1:
						if( $level == 0 )
							$pagecls = "page-menu";
						else if( $level == 1 )
							$pagecls = "page-msub1";
						else
						{
							$pagecls = "page-msub2";
							if( $level > 2 )
							{
								$pagestyle = ' style="margin-left: '.(12+24*($level-1)).'px"';
							}
						}
						break;
				}

				echo '<tr'.( $level == 0 ? ' class="even"' : '' ).'>
					<td><input type="checkbox" name="items_id[]" value="'.$row->id.'" /></td>
					<td>'.$row->sort_num.'</td>
					<td><p class="'.$pagecls.'"'.$pagestyle.'>'.stripslashes($row->page_mean).'</p></td>
					<td>'.stripslashes($row->page_name).'</td>
					<td class="dgray">'.$link_file.'</td>
					'.( $pic_num == 0 ? '<td class="dgray">—</td>' : '<td>'.$pic_num.' шт</td>' ).'
					'.( $vid_num == 0 ? '<td class="dgray">—</td>' : '<td>'.$vid_num.' шт</td>' ).'
					<td class="dt">'.$row->dd.'<br /><span>'.$row->tm.'</span></td>
					<td class="ls">
						<a href="page_basic.php?id='.$row->id.'" title="'.$strings['tipedit'][$lang].'"><img src="img/a-edit.png" width="22" height="22" alt="'.$strings['tipedit'][$lang].'" /></a>&nbsp;
						<a href="'.$PHP_SELF.'?action=deleteitem&item_id='.$row->id.'" onclick="return confirm(\''.$strings['deleteconfirm'][$lang].'\')" title="'.$strings['tipdel'][$lang].'"><img src="img/a-del.png" width="22" height="22" alt="'.$strings['tipdel'][$lang].'" /></a>
					</td>
				</tr>';				

				$pres = FillPageList($row->id, $level+1);
				$num_pages += $pres;
			}
			mysql_free_result($res);
		}
		*/

		return $num_pages;
	}
}
?>