<?php

class DespageView extends AdmPageView {
    protected $pgsModel;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $pgsModel) {
        parent::__construct($config, $pModel);

        $this->pgsModel = $pgsModel;
        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Редактировать страницы";
        $this->PAGE_HEADER['en'] = "Page editor";
    }

    public function render_main() {
        $this->renderPage("despage_manager.php");
    }

}