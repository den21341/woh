<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ExportDataView extends AdmPageView
{
	protected $model;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $sModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $sModel;
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Экспорт данных";
		$this->PAGE_HEADER['en'] = "Data export";
	}
	
	public function render_main()
	{			
		$this->renderPage("export.php");
	}
	
	public function render_clients_csv($its)
	{
		$this->viewMode = "csv";
		
		//$this->page_BreadcrumbsAdd( $this->page_BuildUrl("seo"), "Список SEO записей" );
		
		//$this->renderPage("seo_titles.php");
		
		$this->render_csv($its);
	}
	
	private function render_csv($its)
	{
		$csv = '';
		$first_line = true;
		
		for($i=0; $i<count($its); $i++)
		{				
			$hline = '';	
			$line = ''; 
			
			foreach($its[$i] as $k => $v)
			{
				if( $first_line )
				{
					$first_line = false;
					
					$hline .= '"'.str_replace("\"", "", $k).'";';
				}
				
				$line .= '"'.str_replace("\"", "", $v).'";';
			} 
			
			if( $hline != "" )
				$csv .= $hline."\r\n";
			
			$csv .= $line."\r\n";
		}
		
		header("Content-Type: text/csv; name=\"allclients_".date("dmY", time()).".csv\";");
		header("Content-Disposition: attachment; filename=\"allclients_".date("dmY", time()).".csv\";");
		
		echo $csv;
	}
}
?>