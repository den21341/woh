<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class UsersView extends AdmPageView
{
	protected $userModel;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $userModel)
	{
		parent::__construct($config, $pModel);
		
		$this->userModel = $userModel;
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Управление Пользователями";
		$this->PAGE_HEADER['en'] = "User Management";
	}
	
	public function render_main()
	{			
		$this->renderPage("user_list.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "edituser";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("users"), "Список пользователей" );
		
		$this->renderPage("user_list.php");
	}
}
?>