<?php

class AngelView extends AdmPageView {
    protected $model;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $catModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Добавление статуса пользователям";
        $this->PAGE_HEADER['en'] = "Angel status management";
    }

    public function render_main() {
        $this->renderPage("addStatus.php");
    }
}