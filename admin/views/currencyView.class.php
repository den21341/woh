<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CurrencyView extends AdmPageView
{
	protected $model;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $slideModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $slideModel;
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Управление валютами";
		$this->PAGE_HEADER['en'] = "Currency management";
	}
	
	public function render_main()
	{			
		$this->renderPage("currency.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "edit";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("currency"), "Список валют" );
		
		$this->renderPage("currency.php");
	}
}
?>