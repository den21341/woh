<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ClientsView extends AdmPageView
{
	protected $model;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $userModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $userModel;
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Управление Клиентами";
		$this->PAGE_HEADER['en'] = "Client Management";
	}
	
	public function render_main()
	{			
		$this->renderPage("client_list.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "edituser";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("users"), "Список пользователей" );
		
		$this->renderPage("client_list.php");
	}
}
?>