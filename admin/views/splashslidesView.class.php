<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class SplashslidesView extends AdmPageView
{
	protected $model;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $slideModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $slideModel;
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Управление слайдами";
		$this->PAGE_HEADER['en'] = "Splash slides management";
	}
	
	public function render_main()
	{			
		$this->renderPage("splashslides.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "edit";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("splashslides"), "Список слайдов" );
		
		$this->renderPage("splashslides.php");
	}
}
?>