<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class BonusProjView extends AdmPageView
{
    protected $model;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $catModel, $catViewModel)
    {
        parent::__construct($config, $pModel);

        $this->model = $catModel;
        $this->catUtilsView = $catViewModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Управление бонусами";
        $this->PAGE_HEADER['en'] = "Bonus Management";
    }

    public function render_main()
    {
        $this->renderPage("bonus_list.php");
    }

    public function render_editform()
    {
        $this->viewMode = "editproj";

        $this->page_BreadcrumbsAdd( $this->page_BuildUrl("bonusproj"), "Список бонусов" );

        $this->renderPage("bonus_list.php");
    }

    /*protected function drawSectCombo($sel_sect_id=0)
    {
        echo $this->catUtilsView->comboSects($sel_sect_id);
    }*/
}
?>