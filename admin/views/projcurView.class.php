<?php


class ProjcurView extends AdmPageView {
    protected $model;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $catModel, $catViewModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;
        $this->catUtilsView = $catViewModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Список проектов";
        $this->PAGE_HEADER['en'] = "Project list";
    }

    public function render_main() {
        $this->renderPage("projcurList.php");
    }
}