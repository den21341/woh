<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class MainView extends AdmPageView
{
	private $msg;
	
	public function renderLoginPage()
	{
		include $this->tpl_folder."index.php";
	}
	
	public function render_default($msg_text = "")
	{
		$this->msg = $msg_text;
		//$PM = Array();
		$this->renderLoginPage();
	}	
	
	public function render_main()
	{
		$this->PAGE_HEADER['ru'] = "Добро пожаловать";
		$this->PAGE_HEADER['en'] = "Welcome";
		
		$this->renderPage("main.php");
	}
}
?>