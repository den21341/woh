<?php

class IktableView extends AdmPageView {
    protected $model;
    private $msg;
    public $viewMode;

    function __construct($config, $pModel, $catModel, $catViewModel) {
        parent::__construct($config, $pModel);

        $this->model = $catModel;
        $this->catUtilsView = $catViewModel;

        $this->viewMode = "";

        $this->PAGE_HEADER['ru'] = "Выгрузка данных Интеркассы";
        $this->PAGE_HEADER['en'] = "Interkass table";
    }

    public function render_httpIk($pay_result = 'waitAccept', $pay_to_date = '0', $currency = 'UAH') {

        $uri = 'https://api.interkassa.com/v1/co-invoice';

        $ch = curl_init($uri);

        curl_setopt_array($ch, [
            CURLOPT_HTTPHEADER  => ['Authorization: Basic NTYxNTUzNzMzZDFlYWY2ZTRmOGI0NTY4OlpmMDJUZHJlTURoa1BITFIzeXNNRWZxMjdXTUozdXo5', 'Ik-Api-Account-Id : 561554103d1eaf39508b4569'],
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_VERBOSE => 1
        ]);

        $out = curl_exec($ch);
        curl_close($ch);

        $out = json_decode($out, true);

        if($out['status'] == 'error') {
            echo '<pre>';
            print_r($out);
            die();
        }

        $jout = $out['data'];

        $csv = '';
        $first_line = true;

        $type = '';

        //echo '<pre>';
        //print_r($jout); die();

        $hline = '';
        //$hline = '"Номер плат";"ID в нашей системе";"Валюта";"Сумма внесенного платежа";"Переведено";"Коммисия";"Статус";"Создан"';
        
        if(is_numeric($pay_to_date)) {

            $ld = new DateTime(date('Y-m-d'));
            $ld->modify('-'.$pay_to_date.' day');
            $ld = $ld->format('Y-m-d');

            foreach ($jout as $arr_jount) {
                $line = '';

               // echo $arr_jount['stateName'].'<br>';

                $d1 = new DateTime($arr_jount['created']);
                $d1 = $d1->format('Y-m-d');

                if($arr_jount['currencyCodeChar'] == $currency) {

                    if ($pay_to_date <= 0) {
                        if (strtotime($d1) == strtotime(date('Y-m-d'))) {
                            //echo 'true;';
                            if ($arr_jount['stateName'] == $pay_result) {
                                //echo 'true2';
                                $line .= '"' . str_replace("\"", "", $arr_jount['id']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['paymentNo']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['currencyCodeChar']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['coAmount']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['coRefund']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['ikFee']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['stateName']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['created']) . '";';
                                // echo $line; die();
                            }
                        }
                    } else {
                        if (strtotime($d1) >= strtotime($ld)) {
                            //echo 'true;';
                            if ($arr_jount['stateName'] == $pay_result) {
                                //echo 'true2';
                                $line .= '"' . str_replace("\"", "", $arr_jount['id']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['paymentNo']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['currencyCodeChar']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['coAmount']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['coRefund']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['ikFee']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['stateName']) . '";';
                                $line .= '"' . str_replace("\"", "", $arr_jount['created']) . '";';
                                // echo $line; die();
                            }
                        }
                    }
                }

                if ($hline != '')
                    $csv .= $hline . "\r\n";

                if ($line != '')
                    $csv .= $line . "\r\n";
            }
        }
        
        //die();

        header("Content-Type: text/csv; name=\"allclients_".date("dmY", time()).".csv\";");
        header("Content-Disposition: attachment; filename=\"allclients_".date("dmY", time()).".csv\";");

        echo $csv;

    }

    public function render_main() {
        $this->renderPage("iktable.php");
    }
}