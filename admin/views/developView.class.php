<?php

class DevelopView extends AdmPageView {
    protected $model;
    private $msg;

    function __construct($config, $pModel, $resModel)
    {
        parent::__construct($config, $pModel);

        $this->model = $resModel;

        $this->PAGE_HEADER['ru'] = "This is developer.debug panel, COMRAD :D";
        $this->PAGE_HEADER['en'] = "This is developer.debug panel, COMRAD :D";
    }

    public function render_main() {
        $this->renderPage("develop.php");
    }
}