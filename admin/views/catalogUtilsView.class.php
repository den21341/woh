<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CatalogUtilsView extends View
{
	protected $catalog;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $catModelObj)
	{
		parent::__construct($config);
		
		$this->catalog = $catModelObj;
		$this->viewMode = "";
	}
		
	public function getCatalogInst()
	{
		return $this->catalog;
	}
	
	public function comboSects($sgroup, $selid=0)
	{
		$html = $this->_comboSectsLevel($sgroup, 0, 0, $selid);
		
		return $html;
	}
	
	private function _comboSectsLevel($sgroup, $sid, $level, $selid=0)
	{
		$html = '';
		
		$sects = $this->catalog->Catalog_SectLevel($sgroup, $sid, false);
		for( $i=0; $i<count($sects); $i++ )
		{
			$html .= '<option value="'.$sects[$i]['id'].'"'.($level > 0 ? ' style="padding-left: '.($level*30 + 20).'px"' : '').' '.($selid == $sects[$i]['id'] ? ' selected' : '').'>'.stripslashes($sects[$i]['name']).'</option>';
			
			$html .= $this->_comboSectsLevel($sgroup, $sects[$i]['id'], $level+1, $selid);
		}
		
		return $html;
	}
		
}
?>