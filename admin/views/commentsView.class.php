<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class CommentsView extends AdmPageView
{
	protected $model;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $catModel, $catViewModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $catModel;		
		$this->catUtilsView = $catViewModel;
		
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Управление Комментариями";
		$this->PAGE_HEADER['en'] = "Project Comments Management";
	}
	
	public function render_main()
	{			
		$this->renderPage("comments.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "editcomment";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("requests"), "Список комментариев" );
		
		$this->renderPage("comments.php");
	}
	
	protected function drawSectCombo($sel_sect_id=0)
	{
		echo $this->catUtilsView->comboSects($sel_sect_id);
	}
}
?>