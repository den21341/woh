<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class SeoView extends AdmPageView
{
	protected $model;
	private $msg;
	public $viewMode;	
	
	function __construct($config, $pModel, $sModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $sModel;
		$this->viewMode = "";
		
		$this->PAGE_HEADER['ru'] = "Управление SEO";
		$this->PAGE_HEADER['en'] = "SEO data management";
	}
	
	public function render_main()
	{			
		$this->renderPage("seo_titles.php");
	}
	
	public function render_editform()
	{
		$this->viewMode = "edit";
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("seo"), "Список SEO записей" );
		
		$this->renderPage("seo_titles.php");
	}
}
?>