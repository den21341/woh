<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class FileManView extends AdmPageView
{
	protected $model;
	
	function __construct($config, $pModel, $fmModel, $viewfull, $cdir, $tdir)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $fmModel;
		
		$this->uan = 0;
		$this->cur_dir = $cdir;
		$this->tmp_dir = $tdir;
		
		$this->viewFull = $viewfull;
		$this->viewMode = "files";
		
		$this->PAGE_HEADER['ru'] = "Файловый менеджер";
		$this->PAGE_HEADER['en'] = "File Manager";
	}
	
	public function setErrorMsg($err_code)
	{		
		$strings['msgnotsel']['ru'] = "Вы не указали имя файла/папки";		
		$strings['msgnotexists']['ru'] = "Файл/папка с указанным именем не существует";
		$strings['msgexists']['ru'] = "Файл/папка с указанным именем уже существует";
		$strings['msgmkdir']['ru'] = "Не удалось создать директорию.";
		$strings['msgdirfull']['ru'] = "Не удалось удалить директорию, возможно она не пустая.";
		$strings['msgdelerr']['ru'] = "Не удалось удалить файл.";
		$strings['msgloaderr']['ru'] = "Не удалось загрузить файл на сервер";
		$strings['msgtypeerr']['ru'] = "Не удалось загрузить файл на сервер. Возможно вы пытаетесь загрузить файл с другим расширением.";

		$strings['msguploadok']['ru'] = "Файл загружен на сервер";
		$strings['msgdirdelok']['ru'] = "Директория была удалена";
		$strings['msgdirmkok']['ru'] = "Директория была создана.";
		$strings['msgfiledelok']['ru'] = "Файл был удален";		
				
		$strings['msgnotsel']['en'] = "The file|folder name is empty";		
		$strings['msgnotexists']['en'] = "File/folder with this name is unavailiable";
		$strings['msgexists']['en'] = "File|folder with this name is already exists";
		$strings['msgmkdir']['en'] = "Can't create new directory.";
		$strings['msgloaderr']['en'] = "Error occure while loading file to server";
		$strings['msgtypeerr']['en'] = "Error occure while loading file to server. The extention of the file is incorrect.";		
		$strings['msgdirfull']['en'] = "Can't remove directory, the directory is possibly not empty.";		
		$strings['msgdelerr']['en'] = "Can't remove file.";
		
		$strings['msguploadok']['en'] = "File uploaded to server";
		$strings['msgdirdelok']['en'] = "Folder was removed";
		$strings['msgdirmkok']['en'] = "Folder was created.";
		$strings['msgfiledelok']['en'] = "File was removed.";		
		
		$lang = $this->pageModel->getViewLang();
		
		switch($err_code)
		{
			// Error messages
			case FM_ERR_EMPTY_NAME:
				$this->error_msg = $strings['msgnotsel'][$lang];
				break;
				
			case FM_ERR_NOTEXIST:
				$this->error_msg = $strings['msgnotexists'][$lang];
				break;
				
			case FM_ERR_EXIST:
				$this->error_msg = $strings['msgexists'][$lang];
				break;
			
			case FM_ERR_MKDIR:
				$this->error_msg = $strings['msgmkdir'][$lang];
				break;
			
			case FM_ERR_RMDIR:
				$this->error_msg = $strings['msgdirfull'][$lang];
				break;
			
			case FM_ERR_UNLINK:
				$this->error_msg = $strings['msgdelerr'][$lang];
				break;
			
			case FM_ERR_LOADFILE:
				$this->error_msg = $strings['msgloaderr'][$lang];
				break;
				
			case FM_ERR_FILETYPE:
				$this->error_msg = $strings['msgtypeerr'][$lang];
				break;
				
			// Success messages
			case FM_OK_MKDIR:
				$this->error_msg = $strings['msgdirmkok'][$lang];
				break;
				
			case FM_OK_RMDIR:
				$this->error_msg = $strings['msgdirdelok'][$lang];
				break;
			
			case FM_OK_UNLINK:
				$this->error_msg = $strings['msgfiledelok'][$lang];
				break;
			
			case FM_OK_LOADFILE:
				$this->error_msg = $strings['msguploadok'][$lang];
				break;
		} 
	}
	
	public function render_main()
	{			
		//echo $this->tmp_dir."<br>";
		$this->renderPage("cat_files.php", ($this->viewFull == false));
	}	
}
?>