<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class ContactsView extends AdmPageView
{
	protected $model;
	private $msg;
	
	function __construct($config, $pModel, $resModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $resModel;
		
		$this->PAGE_HEADER['ru'] = "Редактировать Контактную Информацию";
		$this->PAGE_HEADER['en'] = "Edit Contacts";
	}
	
	public function render_main()
	{			
		$this->renderPage("page_cont.php");
	}	
}
?>