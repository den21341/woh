<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class SectsView extends AdmPageView
{
	protected $model;
	protected $catUtilsView;
	private $msg;
	public $viewMode;	
	
	public $sectinfo;
	
	function __construct($config, $pModel, $Model, $catModel)
	{
		parent::__construct($config, $pModel);
		
		$this->model = $Model;
		$this->catUtilsView = $catModel;
		$this->viewMode = "";
		$this->sGroup = 0;
		$this->sectinfo = null;
		
		$this->PAGE_HEADER['ru'] = "Управление разделами каталога";
		$this->PAGE_HEADER['en'] = "Catalog Sections";		
	}
	
	public function render_main($vmode="")
	{			
		if( $vmode == "help" )
		{
			$this->sGroup = 1;
		}
		$this->renderPage("cat_sect.php");
	}
	
	public function render_editform($vmode="")
	{
		$this->viewMode = "edit";
		
		if( $vmode == "help" )
		{
			$this->sGroup = 1;
		}
		
		$this->page_BreadcrumbsAdd( $this->page_BuildUrl("sects"), "Список разделов" );
		
		$this->renderPage("cat_sect.php");
	}
	
	protected function drawSectCombo($sel_sect_id=0)
	{
		echo $this->catUtilsView->comboSects($this->sGroup, $sel_sect_id);
	}
	
	protected function drawSectTable()
	{
		$catUtils = $this->catUtilsView->getCatalogInst();
		
		echo $this->_drawSectTableLevel($catUtils, $this->sGroup, 0, 0);
	}
	
	private function _drawSectTableLevel($catUtils, $sgroup, $sid, $level, $selid=0, $hidden = false)
	{
		$LEVEL_PAD_PX = 16;
		$html = '';
	
		$sects = $catUtils->Catalog_SectLevel($sgroup, $sid, true);
		for( $i=0; $i<count($sects); $i++ )
		{
			$row = $sects[$i];
			
			$menu_group = '';
			
			$html .= '<tr'.( $level == 0 ? ' class="even"' : '' ).'>
				<td>'.$row['id'].'</td>
				<td>'.$row['sort_num'].'</td>
				<td'.($level > 0 ? ' style="padding-left: '.(16+($level - 1)*$LEVEL_PAD_PX).'px"' : '').'>
					<p '.($level > 0 ? ' class="row-sub'.($level == 1 ? ' row-lev1' : '').'"' : ' class="row-top"').(( ($row['visible'] == 0) || $hidden ) ? ' style="color: #a0a0a0;"' : '').'>'.stripslashes($row['name']).'</p>
				</td>
				<td>'.$menu_group.'</td>
				<td>'.stripslashes($row['url']).'</td>
				<td>'.$row['totprods'].'</td>
				<td class="ls">
					<a href="'.$this->Page_BuildUrl("sects", "edit", 'item_id='.$row['id']).'" title="Редактировать '.str_replace("\"", "", stripslashes($row['name'])).'"><img src="'.WWWHOST.'admin/img/a-edit.png" width="22" height="22" alt="Редактировать '.str_replace("\"", "", stripslashes($row['name'])).'" /></a>&nbsp;
					<a href="'.$this->Page_BuildUrl("sects", "delete", 'item_id='.$row['id']).'" onclick="return confirm(\'Действительно хотите удалить раздел '.str_replace("\"", "", stripslashes($row['name'])).'?\')" title="Удалить"><img src="'.WWWHOST.'admin/img/a-del.png" width="22" height="22" alt="Удалить '.str_replace("\"", "", stripslashes($row['name'])).'" /></a>&nbsp;
				</td>
			</tr>';			
				
			$html .= $this->_drawSectTableLevel($catUtils, $sgroup, $sects[$i]['id'], $level+1, $selid, $hidden);
		}
	
		return $html;
	}
}
?>