function Disable(own,el,el2){
  el2.disabled=true;
  el2.style.backgroundColor='#EEEEEE';
  el.disabled=false;
  el.style.backgroundColor='#FFFFFF';
  return true;
}

function Delete(){
  if ( confirm( unescape("Вы действительно хотите продолжить?") ) ) {
    return true;
  }else{
    return false;
  }
}

function Delet(url){
  if ( confirm( unescape("Вы действительно хотите продолжить?") ) ) {
    location.href=url;
  }
}

function DeletS(url, spisok){
  if ( confirm( unescape("Вы действительно хотите продолжить удаление?\nПопутно будут удалены: ")+unescape(spisok) ) ) {
    location.href=url;
  }
}

function GoTo(url){
 location.href=url;
}

function OpenWin(url){
  newwindow = window.open(url,"EmailWindow","toolbar=no,status=no,resizable=yes,location=no,toolbar=no,menubar=no,width=400,height=400");
  newwindow.focus();
}

function ViewImage(url){
  newwindow = window.open(url,"ChangeWindow","toolbar=no,status=no,resizable=yes,location=no,toolbar=no,menubar=no,width=700,height=600");
  newwindow.focus();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  mywin = window.open(theURL,winName,features);
  mywin.focus();
}

function FileCh(filename,target){
  target.value=filename;
  self.opener.focus();
  window.close();
}

function fillProductBySect(sobj_id, pobj_id, sect_id, progress_id)
{
	var list_obj = uh_get_object( pobj_id );

	//var type_id = type_id_str;

  	if( AJAX != null )
	{
		//alert( 'AJAX is ok' );
		hideItem( pobj_id );
		showItem( progress_id );

		// Disable combos for select
		var objcombo = uh_get_object(sobj_id);
		objcombo.disabled = true;

		var process_fn = function(res)
		{
			//alert(' RESPONSE IN CALLBACK: ' + res);

			res = (res.split("\r\n")).join(" ");

			eval( 'var responce_data = ' + res );

			var prods = responce_data.prodlist;

			list_obj.options.length = 0;
			for( var i=0; i<prods.length; i++ )
			{
				try
				{
					var newopt = new Option( prods[i].name + " ( Цена: " + prods[i].cost + " грн.)", prods[i].id );
					list_obj.options[i] = newopt;
				}
				catch(err)
				{
					alert( "item: " + i + "; error: " + err.description );
				}
			}

   			hideItem( progress_id );
			showItemEx( pobj_id, "inline" );

			// Enable all combos for select
			var objcombo = uh_get_object(sobj_id);
			objcombo.disabled = false;
		};

		//showProgress();
		//hideItem(obj_id);

		//alert('rcom=uh_com_prodlistsect&sid=' + sect_id + '&langid_code=1');

		AJAX.SendRequest('GET', 'rcom=uh_com_prodlistsect&sid=' + sect_id + '&langid_code=1', process_fn, null);
	}
}

function fillProductByType(typeobj, ind, type_id_tpl, modobj_id, progress_id, mincombo, maxcombo)
{
	var typelist_obj = typeobj;
	var list_obj = uh_get_object( modobj_id );

	var type_id = typelist_obj.options[typelist_obj.selectedIndex].value;
	if( !type_id )
		return;

  	if( AJAX != null )
	{
		//alert( 'AJAX is ok' );
		showItem( progress_id );

		// Disable all combos for select
        for(var ic=mincombo; ic<=maxcombo; ic++)
        {
        	var objcombo = uh_get_object(type_id_tpl + ic);
        	objcombo.disabled = true;
        }

		var process_fn = function(res)
		{
			//alert(' RESPONSE IN CALLBACK: ' + res);
			res = (res.split("\r\n")).join(" ");

			eval( 'var responce_data = ' + res );

			var prods = responce_data.prodlist;

			var costarr = new Array(prods.length);

			list_obj.options.length = 0;

			var newopt = new Option( "-- Найдено товаров: " + prods.length + " --", "0:0" );
			list_obj.options[0] = newopt;

			for( var i=0; i<prods.length; i++ )
			{
				try
				{
					var newopt = new Option( prods[i].name+(prods[i].submod != "" ? ", "+prods[i].submod : ""), prods[i].id+":"+prods[i].submodid );
					list_obj.options[i+1] = newopt;
					costarr[i] = prods[i].cost;
				}
				catch(err)
				{
					alert( "item: " + i + "; error: " + err.description );
				}
			}

			prodcostarr[ind-1] = costarr;

   			hideItem( progress_id );

			// Enable all combos for select
	        for(var ic=mincombo; ic<=maxcombo; ic++)
	        {
	        	var objcombo = uh_get_object(type_id_tpl + ic);
	        	objcombo.disabled = false;
	        }
		};

		AJAX.SendRequest('GET', 'rcom=uh_com_prodlist&tid=' + type_id + '&lang_id=ru', process_fn, null);
	}
}

function fillProductCost(cost_obj_id, ind, selind, selval, recalc)
{
	var cost_obj = uh_get_object( cost_obj_id );
	if( cost_obj == null )
		return;

	if( selind == 0 )
	{
		// Not selected
		cost_obj.value = "0.00";
	}
	else
	{
		if( selval == 0 )
			cost_obj.value = "0.00";
		else
			cost_obj.value = prodcostarr[ind-1][selind-1];
	}

	if( recalc )
		fillCalcSum();
}

function fillCalcSum()
{
	var tot_num = 0;
	var tot_sum = 0;
	for(var i=1; i<=10; i++)
	{
		var pn_o = uh_get_object( "prodnum"+i );
		var pn_c = uh_get_object( "prodcost"+i );

		var pn_ov = parseInt(pn_o.value, 10);
		var pn_cv = parseInt(pn_c.value, 10);
		if( !isNaN(pn_ov) && !isNaN(pn_cv) )
		{
			tot_num += pn_ov;
			tot_sum += (pn_ov*pn_cv);
		}
	}

	var sum_o = uh_get_object( "costsum" );
	sum_o.value = tot_sum;
}

// Поиск товара по коду - форма редактирования заказа
function fillProductByCode(codobjid, ind, nameobjid, costobjid, progress_id, txtid, with_recalc)
{
	var txt_obj = uh_get_object( txtid );
	var code_obj = uh_get_object( codobjid );
	var list_obj = name_obj = uh_get_object( nameobjid );
	var cost_obj = uh_get_object( costobjid );
	var code_int = code_obj.value;
	//var code_int = parseInt(code_obj.value, 10);
	//if( isNaN(code_int) )
	//{
	//	alert('Код товара введен неправильно');
	//	return;
	//}

  	if( AJAX != null )
	{
		showItem( progress_id );

		var process_fn = function(res)
		{
			//alert(' RESPONSE IN CALLBACK: ' + res);
			res = (res.split("\r\n")).join(" ");
			eval( 'var responce_data = ' + res );

			//var prod = responce_data.prodinfo;
			var prods = responce_data.prodlist;

			var costarr = new Array(prods.length);

			list_obj.options.length = 0;

			var newopt = new Option( "-- Найдено товаров " + prods.length + " --", "0:0" );
			list_obj.options[0] = newopt;

			for( var i=0; i<prods.length; i++ )
			{
				try
				{
					var newopt = new Option( prods[i].name+(prods[i].submod != "" ? ", "+prods[i].submod : ""), prods[i].id+":"+prods[i].submodid );
					list_obj.options[i+1] = newopt;
					costarr[i] = prods[i].cost;
				}
				catch(err)
				{
					alert( "item: " + i + "; error: " + err.description );
				}
			}

			prodcostarr[ind-1] = costarr;

			/*
			try
			{
				if( prod.id != 0 )
				{
					name_obj.value = prod.name;
					cost_obj.value = prod.cost_g;
				}

				if( with_recalc )
					fillCalcSum();
			}
			catch(err)
			{
				alert( "item: " + prod.name + "; error: " + err.description );
			}
			*/

   			hideItem( progress_id );
		};

		//alert('rcom=uh_com_prodbycode&pcode=' + code_int + '&lang_id=1');
		AJAX.SendRequest('GET', 'rcom=uh_com_prodbycode&pcode=' + code_int + '&lang_id=ru', process_fn, null);
	}
}

// Поиск заказа по его номеру - Таблица подбора заказов
function findOrderByNum(codobjid, resobjid, progress_id)
{
	var code_obj = uh_get_object( codobjid );
	var res_obj = uh_get_object( resobjid );
	var code_int = parseInt(code_obj.value, 10);
	if( isNaN(code_int) )
	{
		alert('Номер заказа введен неправильно');
		return false;
	}

  	if( AJAX != null )
	{
		res_obj.innerHTML = '';
		showItem( progress_id );

		var process_fn = function(res)
		{
			//alert(' RESPONSE IN CALLBACK: ' + res);
			res = (res.split("\r\n")).join(" ");
			eval( 'var responce_data = ' + res );

			var order = responce_data.orderinfo;
			try
			{
				res_obj.innerHTML = ( order.id == 0 ? 'заказ не найден' : '<a href="orders.php?action=edit&item_id='+order.id+'" target="_blank">Заказ №'+order.ordnum+' от ' + order.dt + ' (на '+order.cost_g+'грн.)</a>' );
			}
			catch(err)
			{
				alert( "item: " + order.id + "; error: " + err.description );
			}

   			hideItem( progress_id );
		};
		AJAX.SendRequest('GET', 'rcom=uh_com_orderbycode&pcode=' + code_int + '&lang_id=ru', process_fn, null);
	}

	return false
}


//----------- ajax jq ----------------------
function uh_addsitegamma(prodid, submodel, col1, col2, col3, photoid, sortind, updtpanid, progress)
{
	if( submodel.value == "" )
	{
		alert( "Не указано название" );
		return 0;
	}

	//var post_req_str = "cmd=uh_com_prodaddsubm&prodid="+prodid+"&arttxt="+subart.value+"&modtxt="+submodel.value+"&picid="+photoid.options[photoid.selectedIndex].value+"&sortnum="+sortind.value;
	var post_req_str = "cmd=uh_com_siteaddgamma&prodid="+prodid+"&col1="+col1.value+"&col2="+col2.value+"&col3="+col3.value+"&modtxt="+submodel.value+"&picid="+photoid.options[photoid.selectedIndex].value+"&sortnum="+sortind.value;

	findrun = true;
	showItem( progress );

	$.ajax({
        type: "POST",
        url: "ajx/ajxad_jq.php",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	//alert( data );
        	hideItem( progress );
        	if( data != "" )
        	{
				$("#"+updtpanid).html(data);
        	}

        	findrun = false;
        },
        complete: function(){
        	hideItem( progress );
        	findrun = false;
        }
    });

	submodel.value = "";
	col1.value = "";
	col2.value = "";
	col3.value = "";
	photoid.selectedIndex = 0;
	sortind.value = "";

}

function uh_delsitegamma(prodid, submodid, updtpanid, progress)
{
	var post_req_str = "cmd=uh_com_sitedelgamma&prodid="+prodid+"&submodid="+submodid;

	findrun = true;
	showItem( progress );

	$.ajax({
        type: "POST",
        url: "ajx/ajxad_jq.php",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	//alert( data );
        	hideItem( progress );
        	if( data != "" )
        	{
				$("#"+updtpanid).html(data);
        	}

        	findrun = false;
        },
        complete: function(){
        	hideItem( progress );
        	findrun = false;
        }
    });
}


function uh_addprodsubm(prodid, submodel, subart, photoid, sortind, updtpanid, progress)
{
	if( submodel.value == "" )
	{
		alert( "Не указана модель товара" );
		return 0;
	}

	var post_req_str = "cmd=uh_com_prodaddsubm&prodid="+prodid+"&arttxt="+subart.value+"&modtxt="+submodel.value+"&picid="+photoid.options[photoid.selectedIndex].value+"&sortnum="+sortind.value;

	findrun = true;
	showItem( progress );

	$.ajax({
        type: "POST",
        url: "ajx/ajxad_jq.php",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	//alert( data );
        	hideItem( progress );
        	if( data != "" )
        	{
				$("#"+updtpanid).html(data);
        	}

        	findrun = false;
        },
        complete: function(){
        	hideItem( progress );
        	findrun = false;
        }
    });

	submodel.value = "";
	subart.value = "";
	photoid.selectedIndex = 0;
	sortind.value = "";

}

function uh_delprodsubm(prodid, submodid, updtpanid, progress)
{
	var post_req_str = "cmd=uh_com_proddelsubm&prodid="+prodid+"&submodid="+submodid;

	findrun = true;
	showItem( progress );

	$.ajax({
        type: "POST",
        url: "ajx/ajxad_jq.php",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	//alert( data );
        	hideItem( progress );
        	if( data != "" )
        	{
				$("#"+updtpanid).html(data);
        	}

        	findrun = false;
        },
        complete: function(){
        	hideItem( progress );
        	findrun = false;
        }
    });
}

function uh_pcartstat(selobj,cartid,prodid,orderid)
{
	var post_req_str = "cmd=uh_com_pcartstat&cartid="+cartid+"&prodid="+prodid+"&orderid="+orderid+"&newstat="+selobj.options[selobj.selectedIndex].value;

	$.ajax({
        type: "POST",
        url: "ajx/ajxad_jq.php",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	//alert( data );
        	if( data != "" )
        	{
				//$("#"+updtpanid).html(data);
        	}
        },
        error: function(){
        	alert("Не удалось сменить статус");
        },
        complete: function(){
			//
        }
    });
}

function uh_pcartstatd(chkobj,cartid,prodid,orderid)
{
	var post_req_str = "cmd=uh_com_pcartstatd&cartid="+cartid+"&prodid="+prodid+"&orderid="+orderid+"&donestat="+(chkobj.checked ? "1" : "0");

	$.ajax({
        type: "POST",
        url: "ajx/ajxad_jq.php",
        data: post_req_str,
        dataType: "text",
        success: function(data){
        	//alert( data );
        	if( data != "" )
        	{
				//$("#"+updtpanid).html(data);
        	}
        },
        error: function(){
        	alert("Не удалось сменить статус выполнения");
        },
        complete: function(){
			//
        }
    });
}
