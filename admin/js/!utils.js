function NewWindow(winname)
{
	var comwin = window.open(winname,'Memo','width=500,height=400,scrollbars=yes,resize=no,toolbar=no,status=no,menubar=no,location=no,directories=no');
    comwin.focus();
}

function checkDateInp( frm )
{
	var dst = frm.datest.value;
	var den = frm.dateen.value;

	if( (dst.charAt(2) != '.') || (dst.charAt(5) != '.') )
	{
		alert('��������� ���� ������� � ��������.');
		return false;
	}

	if( (den.charAt(2) != '.') || (den.charAt(5) != '.') )
	{
		alert('�������� ���� ������� � ��������.');
		return false;
	}

	// check if dates is in possible diapasone
	dst_d = dst.substring(0,2);
	dst_m = dst.substring(3,5);
	dst_y = dst.substring(6,10);

	dst_dn = parseInt(dst_d, 10);
	dst_mn = parseInt(dst_m, 10);
	dst_yn = parseInt(dst_y, 10);

	if( isNaN(dst_dn) )
	{
		alert('��������� ���� ������� � ��������. ���� �� �������� �������� ���������.');
		return false;
	}

	if( isNaN(dst_mn) )
	{
		alert('��������� ���� ������� � ��������. ����� �� �������� �������� ���������.');
		return false;
	}

	if( isNaN(dst_yn) )
	{
		alert('��������� ���� ������� � ��������. ��� �� �������� �������� ���������.');
		return false;
	}

	if( (dst_dn <= 0) || (dst_dn > 31) )
	{
		alert('��������� ���� ������� � ��������. ���� �� ����� ����� ��������� ��������.');
		return false;
	}

	if( (dst_mn <= 0) || (dst_mn > 12) )
	{
		alert('��������� ���� ������� � ��������. ����� �� ����� ����� ��������� ��������.');
		return false;
	}

	if( (dst_yn <= 1995) || (dst_yn > 2015) )
	{
		alert('��������� ���� ������� � ��������. ��� �� ����� ����� ��������� ��������.');
		return false;
	}

	// check if dates is in possible diapasone
	den_d = den.substring(0,2);
	den_m = den.substring(3,5);
	den_y = den.substring(6,10);

	den_dn = parseInt(den_d, 10);
	den_mn = parseInt(den_m, 10);
	den_yn = parseInt(den_y, 10);

	//alert( den_d + " : " + den_dn + " - " + den_m + " : " + den_mn + " - ");

	if( isNaN(den_dn) )
	{
		alert('�������� ���� ������� � ��������. ���� �� �������� �������� ���������.');
		return false;
	}

	if( isNaN(den_mn) )
	{
		alert('�������� ���� ������� � ��������. ����� �� �������� �������� ���������.');
		return false;
	}

	if( isNaN(den_yn) )
	{
		alert('�������� ���� ������� � ��������. ��� �� �������� �������� ���������.');
		return false;
	}

	if( (den_dn <= 0) || (den_dn > 31) )
	{
		alert('�������� ���� ������� � ��������. ���� �� ����� ����� ��������� ��������.');
		return false;
	}

	if( (den_mn <= 0) || (den_mn > 12) )
	{
		alert('�������� ���� ������� � ��������. ����� �� ����� ����� ��������� ��������.' + den_mn);
		return false;
	}

	if( (den_yn <= 1995) || (den_yn > 2015) )
	{
		alert('�������� ���� ������� � ��������. ��� �� ����� ����� ��������� ��������.');
		return false;
	}

	return true;
}

function OpenPopup(url, width, height){
  newwindow = window.open(url,"calendarwnd","toolbar=no,status=no,resizable=no,location=no,toolbar=no,menubar=no,width=" + width + ",height=" + height);
  newwindow.focus();
}

function DateChoose(filename,target){
  target.value=filename;
  self.opener.focus();
  window.close();
}
