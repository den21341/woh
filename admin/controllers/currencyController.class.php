<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Currency extends AdmPageController{
	
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
	
		$this->model = new CurrencyModel($this->getCfg(), $db, 1, $this->getPageModel()->getLangs());
		$this->pageView = new CurrencyView($this->getCfg(), $this->getPageModel(), $this->model);
	}
	
	protected function get_reqvars()
	{
		$data = Array();
		
		$data['name'] = $this->getReqParam("orgname", "");
		$data['cur_code'] = $this->getReqParam("orgcode", "");
		$data['sort_num'] = $this->getReqParam("orgsort", "0");
		if( $data['sort_num'] == "" )
			$data['sort_num'] = 0;
		
		return $data;
	}
	
	protected function init_reqvars()
	{
		$data = Array();
		$data['id'] = 0;
		
		$data['name'] = "";
		$data['cur_code'] = "";
		$data['sort_num'] = "";

		return $data;
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");		
				
		$this->pageView->curlist = $this->model->get_currencyList();	
		$this->pageView->curinfo = $this->init_reqvars();
		
		$this->pageView->render_main();		
	}

	public function action_edit()
	{		
		$id = $this->getReqParam("id", 0);
		$this->pageView->curinfo = $this->model->get_currencyInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_save()
	{
		$id = $this->getReqParam("id", 0);
		$req = $this->get_reqvars();
				
		$this->model->save_Currency($id, $req);
		
		$this->pageView->curlist = $this->model->get_currencyList();
		
		$this->pageView->render_main();
	}
	
	public function action_delete()
	{
		$id = $this->getReqParam("id", 0);
		$this->model->delete_Currency($id);
		
		$this->pageView->curlist = $this->model->get_currencyList();
		
		$this->pageView->render_main();			
	}
	
	public function action_deleteall()
	{
		$ids = $this->getReqParam("items_id", null);
		for( $i=0; $i<count($ids); $i++ )
			$this->model->delete_Currency($ids[$i]);
		
		$this->pageView->curlist = $this->model->get_currencyList();
		
		$this->pageView->render_main();			
	}
	
	public function action_add()
	{
		$req = $this->get_reqvars();
		
		$this->model->add_Currency( $req );
		
		$this->pageView->curlist = $this->model->get_currencyList();
		
		$this->pageView->render_main();
	}	
}
?>