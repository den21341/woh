<?php

class Develop extends AdmPageController {

    protected $model;

    function __construct($config, $db) {
        parent::__construct($config, $db);

        $fileModel = "models/" . strtolower(get_class()) . "Model.class.php";

        echo "Load model: " . $fileModel . "<br />";

        if (!file_exists($fileModel)) {
            die('404 Not Found - Model');
        }

        include_once $fileModel;

        $this->model = new DevelopModel($this->getCfg(), $db, 1);
        $this->pageView = new DevelopView($this->getCfg(), $this->getPageModel(), $this->model);
    }

    public function action_default() {
        $error_msg = "";
        //$action = $this->getReqParam("action", "");
        //if( $action == "makelogin" )
        //{
        //
        //}

        //$this->pageView->contacts = $this->model->get_ContList();

        $userIP = $this->getUserIP();
        $this->getUserLocation();

        $this->pageView->render_main();
    }

    private function getUserIP() {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = '';
        return $ipaddress;
    }

    private function getUserLocation() {
        $ip = $this->getUserIP();
        require_once('../geo/SxGeo.php');
        $SxGeo = new SxGeo('../geo/SxGeoCity.dat', SXGEO_BATCH | SXGEO_MEMORY);
        $region = $SxGeo->getCityFull($ip);
        $city = $SxGeo->get($ip);
        $this->pageView->reg = $region;
        $this->pageView->city = $city;
    }

    public function action_smssend() {
        include_once "smsc_api.php";

        if (isset($_POST["sendsms"])) {
            $r = send_sms($_POST["phone"], "Ваш код подтверждения: ".$this->ok_code($_POST["phone"]));

            if ($r[1] > 0)
                echo "Код подтверждения отправлен на номер ".$_POST["phone"];
        }

        if (isset($_POST["ok"])) {
            $oc = $this->ok_code($_POST["phone"]);

            if ($oc == $_POST["code"])
                echo "Номер активирован";
            else
                echo "Неверный код подтверждения";
        }
    }

   protected function ok_code($s) {
        return hexdec(substr(md5($s."<секретная строка>"), 7, 5));
    }
}