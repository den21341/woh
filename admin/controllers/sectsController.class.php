<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Sects extends AdmPageController{
	
	protected $model;
	protected $catalogModel;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);

		// Add ClatalogUtilsView Class to view
		include_once "views/catalogUtilsView.class.php";
				
		$this->catalogModel = new Catalog($db, 1);
		
		$catuViewObj = new CatalogUtilsView($config, $this->catalogModel);
	
		$this->model = new SectsModel($this->getCfg(), $db, 1, $this->getPageModel()->getLangs());
		$this->pageView = new SectsView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
		
		$this->pageView->sectinfo = $this->init_reqvars();
	}
	
	function init_reqvars()
	{
		$reqd = Array();

		$reqd['id'] = 0;
		$reqd['smode']		= 0;
		$reqd['orgsect']		= 0;
		$reqd['orggroup']		= 0;
		$reqd['orgname']		= "";
		$reqd['orgurl']			= "";
		$reqd['orgdescr']		= "";
		$reqd['orgdescr0']		= "";
		$reqd['orgsort']		= 0;
		$reqd['orgvis']			= 0;
		$reqd['orgfirst']		= 0;
		$reqd['myfile']			= "";
		$reqd['myfile2']		= "";
		$reqd['myfile3']		= "";
		
		$reqd['bannerfile']		= "";
		$reqd['bannerlink']		= "";
				
		return $reqd;		
	}
	
	function get_reqvars()
	{
		$reqd = Array();
		
		$reqd['smode']		= $this->getReqParam("smode", 0);;
		$reqd['id'] 			= $this->getReqParam("item_id", 0);
		$reqd['orgsect']		= $this->getReqParam("orgsect", 0);
		$reqd['orggroup']		= $this->getReqParam("orggroup", 0);
		$reqd['orgname']		= $this->getReqParam("orgname", "");
		$reqd['orgurl']			= $this->getReqParam("orgurl", "");
		$reqd['orgdescr']		= $this->getReqParam("orgdescr", "", false);
		$reqd['orgdescr0']		= $this->getReqParam("orgdescr", "", false);
		$reqd['orgsort']		= $this->getReqParam("orgsort", 0);
		$reqd['orgvis']			= $this->getReqParam("orgvis", 0);
		$reqd['orgfirst']		= $this->getReqParam("orgfirst", 0);
		$reqd['myfile']			= $this->getReqParam("myfile", "");
		$reqd['myfile2']		= $this->getReqParam("myfile2", "");
		$reqd['myfile3']		= $this->getReqParam("myfile3", "");
		
		$reqd['oldurl']			= $this->getReqParam("oldurl", "");
		$reqd['urltype']		= $this->getReqParam("urltype", 0);
		
		$reqd['bannerfile']		= $this->getReqParam("bannerfile");
		$reqd['bannerlink']		= $this->getReqParam("bannerlink");
		
		return $reqd;
	}
	
	function fill_reqvars()
	{
		$reqd = Array();
		
		$reqd['smode']		= $this->getReqParam("smode", 0);;
		$reqd['id'] 			= $this->getReqParam("item_id", 0);
		$reqd['orgsect']		= $this->getReqParam("orgsect", 0);
		$reqd['orggroup']		= $this->getReqParam("orggroup", 0);
		$reqd['orgname']		= $this->getReqParam("orgname", "");
		$reqd['orgurl']			= $this->getReqParam("orgurl", "");
		$reqd['orgdescr']		= $this->getReqParam("orgdescr", "", false);
		$reqd['orgdescr0']		= $this->getReqParam("orgdescr", "", false);
		$reqd['orgsort']		= $this->getReqParam("orgsort", 0);
		$reqd['orgvis']			= $this->getReqParam("orgvis", 0);
		$reqd['orgfirst']		= $this->getReqParam("orgfirst", 0);
		$reqd['myfile']			= $this->getReqParam("myfile", "");
		$reqd['myfile2']		= $this->getReqParam("myfile2", "");
		$reqd['myfile3']		= $this->getReqParam("myfile3", "");
		
		$reqd['oldurl']			= $this->getReqParam("oldurl", "");
		$reqd['urltype']		= $this->getReqParam("urltype", 0);
		
		$reqd['bannerfile']		= $this->getReqParam("bannerfile");
		$reqd['bannerlink']		= $this->getReqParam("bannerlink");
		
		return $reqd;
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");
		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		//$this->pageView->userlist = $this->userModel->get_userList();	
		
		$this->pageView->render_main();
	}
	
	public function action_helpsects()
	{
		$error_msg = "";
		$action = $this->getReqParam("action", "");
		
		$this->pageView->render_main("help");
	}

	public function action_edit()
	{		
		$id = $this->getReqParam("item_id", 0);
		$this->pageView->sectinfo = $this->model->get_SectInfo($id);
		//$this->pageView->userlist = $this->userModel->get_userList();	
		
		$this->pageView->render_editform( ($this->pageView->sectinfo['smode'] ? "help" : "") );
	}
	
	public function action_save()
	{
		$id = $this->getReqParam("item_id", 0);
		
		$reqd = $this->get_reqvars();		
		
		$this->model->save_Sect($id, $reqd);
		
		//$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main( ( $reqd['smode'] ? "help" : "") );
	}
	
	public function action_delete()
	{
		$id = $this->getReqParam("item_id", 0);
		
		$this->model->delete_Sect($id);
		
		//$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main();			
	}
	
	public function action_deleteall()
	{
		/*
		$id = $this->getReqParam("item_id", 0);
	
		$this->model->delete_Sect($id);
	
		//$this->pageView->userlist = $this->userModel->get_userList();		 
		 */
	
		$this->pageView->render_main();
	}
	
	public function action_add()
	{
		$reqd = $this->get_reqvars();
				
		$this->model->add_Sect( $reqd );
		
		//$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main( ( $reqd['smode'] ? "help" : "") );
	}
	
	public function action_status()
	{
		$uid = $this->getReqParam("uid", 0);
		$active = $this->getReqParam("active", 0);
		$this->userModel->set_Status($uid, $active);
		
		$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main();
	}
}
?>