<?php

class Abox extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->boxModel = new AboxModel($this->getCfg(), $db, 1);
        $this->pageView = new AboxView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->catLib = $this->model;
        //this->pageView->bonlist = $this->bonusModel->get_BonusList();

        $this->pageView->render_main();
    }
    
    public function action_saveMoney() {
        $uid = $this->getReqParam("uid", 0);
        $money = $this->getReqParam("money", 0);

        $res = $this->boxModel->setMoneyId($uid, $money);

        $this->pageView->res = $res;
        $this->pageView->render_main();
    }
}