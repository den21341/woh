<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Comments extends AdmPageController{
	
	protected $model;
	protected $comModel;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		/*
		echo getcwd()."<br>";
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
		*/

		echo "!!!<br>";
			
		///
		$this->model = new Catalog($db, 1);
		
		// Add ClatalogUtilsView Class to view
		include_once "views/catalogUtilsView.class.php";
		$catuViewObj = new CatalogUtilsView($config, $this->model);
		
		$this->comModel = new CommentsModel($this->getCfg(), $db, 1);
		$this->pageView = new CommentsView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");
		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->commentlist = $this->comModel->get_CommentList($this->model);
		
		$this->pageView->render_main();		
	}

	public function action_editcomment()
	{		
		$cid = $this->getReqParam("cid", 0);
		$this->pageView->cominfo = $this->model->Item_CommentInfo($cid);
		$this->pageView->commentlist = $this->comModel->get_CommentList($this->model);
		
		$this->pageView->render_editform();
	}
	
	public function action_savecomment()
	{
		$cid = $this->getReqParam("cid", 0);
		
		$proj = Array();

		$proj['id'] 		= $cid;
		$proj['pfirst']		= $this->getReqParam("pfirst", 0);
		$proj['prate']		= $this->getReqParam("prate", 0);
		//$proj['ptit']		= $this->getReqParam("ptit", "");
		$proj['pdescr0']	= $this->getReqParam("pdescr0", "");
		//$proj['pdescr']		= $this->getReqParam("pdescr", "");
		//$proj['pamount']	= $this->getReqParam("pamount", 0);
		//$proj['city_id']	= $this->getReqParam("city_id", 0);
		//$proj['country']	= $this->getReqParam("country", "");
		
		$this->comModel->save_Comment($cid, $proj);
		
		$this->pageView->commentlist = $this->comModel->get_CommentList($this->model);
		
		$this->pageView->render_main();
	}
	
	public function action_deletecomment()
	{
		$cid = $this->getReqParam("cid", 0);
		$this->comModel->delete_Comment($cid);
		
		$this->pageView->commentlist = $this->comModel->get_CommentList($this->model);
		
		$this->pageView->render_main();			
	}
	
		
	public function action_status()
	{
		$rid = $this->getReqParam("rid", 0);
		$active = $this->getReqParam("status", 0);
		//$this->projModel->set_ProjStatus($pid, $active);
		
		//$this->pageView->projlist = $this->projModel->get_ProjList($this->model);
		
		$this->pageView->render_main();
	}
}
?>