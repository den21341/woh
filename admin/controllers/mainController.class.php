<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Main extends AdmPageController{
	
	public function index(){
	}
	
	public function action_default()
	{					
		$this->pageView = new MainView($this->getCfg(), $this->getPageModel());		
		
		$error_msg = "";
		$action = $this->getReqParam("action", "");
		
		//echo "Action == ".$action."<br>";		
		
		if( $action == "makelogin" )
		{
			$ulogin = $this->getReqParam("login", "", false, true);
			$upass = $this->getReqParam("passwd", "", false, true);
			
			if( !UhCmsAdminApp::getSesInstance()->AuthMake($ulogin, $upass) )
			{
				$error_msg = "Логин или пароль указаны неправильно!";				
			}
		}
		else if( $action == "logout" )
		{
			UhCmsAdminApp::getSesInstance()->AuthClear();			
		}
		
		if( UhCmsAdminApp::getSesInstance()->UserId != 0 )
		{
			//$this->goToUrl( $this->pageView->Page_BuildUrl("main") );
			$this->pageView->render_main();
		}
		else
		{		
			$this->pageView->render_default($error_msg);
		}
	}	
}
?>