<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Contacts extends AdmPageController{
	
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
	
		$this->model = new ContactsModel($this->getCfg(), $db, 1);
		$this->pageView = new ContactsView($this->getCfg(), $this->getPageModel(), $this->model);
	}
	
	public function action_default()
	{									
		$error_msg = "";
		//$action = $this->getReqParam("action", "");		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->contacts = $this->model->get_ContList();	
		
		$this->pageView->render_main();		
	}	
	
	public function action_save()
	{
		$ids 		= $this->getReqParam("optids", null);
		$vals	= $this->getReqParam("tab_params", null);
		
		$this->model->save_Cont($ids, $vals);
		
		$this->pageView->contacts = $this->model->get_ContList();	
		
		$this->pageView->render_main();
	}
}
?>