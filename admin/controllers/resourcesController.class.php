<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Resources extends AdmPageController{
	
	protected $resModel;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
	
		$this->resModel = new ResourcesModel($this->getCfg(), $db, 1);
		$this->pageView = new ResourcesView($this->getCfg(), $this->getPageModel(), $this->resModel);
	}
	
	public function action_default()
	{									
		$error_msg = "";
		//$action = $this->getReqParam("action", "");		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->reslist = $this->resModel->get_ResourceList();	
		
		$this->pageView->render_main();		
	}	
	
	public function action_save()
	{
		$id 		= $this->getReqParam("id", 0);
		$content	= $this->getReqParam("content", "", false);
		
		$this->resModel->save_Resource($id, $content);
		
		$this->pageView->reslist = $this->resModel->get_ResourceList();	
		
		$this->pageView->render_main();
	}
}
?>