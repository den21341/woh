<?php
class Despage extends AdmPageController {

    protected $model;

    function __construct($config, $db) {
        parent::__construct($config, $db);
        $this->model = new DespageModel($this->getCfg(), $db, $this->getPageModel()->getLangId(), $this->getPageModel()->getLangs());
        $this->pageView = new DespageView($this->getCfg(), $this->getPageModel(), $this->model);

    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->pagelist = $this->model->getPagesDes();

        $this->pageView->render_main();
    }

    public function action_edit() {
        $id = $this->getReqParam("id", 0);

        $this->pageView->pagelistData = $this->model->getPagesDesSubData($id);
        $this->pageView->pagelist = $this->model->getPagesDes();
        $this->pageView->viewMode = 'edit';
        $this->pageView->lid = $id;

        $this->pageView->render_main();
    }

    public function action_save() {
        $data = $this->get_reqvars();
        $this->model->savePagesData($data);

        $this->pageView->pagelist = $this->model->getPagesDes();

        $this->pageView->render_main();
    }

    protected function get_reqvars()
    {
        $data['cobl'] = $this->getReqParam("cobl", 0);
        $data['paid'] = $this->getReqParam("paid", 0);

        for($i=1; $i<=$data['cobl']; $i++) {
            $data[$i] = $this->getReqParam('bl_'.$i, '');
        }

        return $data;
    }

}

?>