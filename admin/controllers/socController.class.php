<?php

class Soc extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->socModel = new SocModel($this->getCfg(), $db, 1);
        $this->pageView = new SocView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->Soc = $this->socModel->getAll();

        $this->pageView->catLib = $this->model;
        //this->pageView->bonlist = $this->bonusModel->get_BonusList();

        $this->pageView->render_main();
    }

    public function action_socCheck() {
        $uid = $this->getReqParam("uid", 0);
        $st = $this->getReqParam("active", 0);

        $this->socModel->setUserSocStatus($uid, $st);
        $this->pageView->Soc = $this->socModel->getAll();
        $this->pageView->render_main();
    }
}