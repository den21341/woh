<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Faq extends AdmPageController{
	
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		/*
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
		*/
		
		// Add ClatalogUtilsView Class to view
		include_once "views/catalogUtilsView.class.php";
		
		$this->catalogModel = new Catalog($db, 1);
		
		$catuViewObj = new CatalogUtilsView($config, $this->catalogModel);
	
		$this->model = new FaqModel($this->getCfg(), $db, 1, $this->getPageModel()->getLangs());
		$this->pageView = new FaqView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj, $this->getReqParam("pi", 0), $this->getReqParam("pn", 50));
		
		$this->pageView->faqgrinfo = $this->init_reqvars();
	}
	
	protected function get_reqvars()
	{
		$data = Array();
		
		$data['group_id'] = $this->getReqParam("group_id", "");
		$data['sect_id'] = $this->getReqParam("sect_id", 0);
		$data['name'] = $data['title'] = $this->getReqParam("newstitle", "");		
		$data['descr'] = $data['content'] = $this->getReqParam("newscont", "");
		
		$data['filename_src'] = $this->getReqParam("myfile", "");	
		$data['first_page'] = $this->getReqParam("newsfirst", "0");		
		$data['sort_num'] = $this->getReqParam("newssort", "0");
		if( $data['sort_num'] == "" )
			$data['sort_num'] = "0";	//$this->getReqParam("datest", "");
		
		$data['datest'] = "";
		
		return $data;
	}
	
	protected function init_reqvars()
	{
		$data = Array();
		$data['id'] = 0;
		$data['group_id'] = 0;
		$data['sect_id'] = 0;		
		
		$data['title'] = "";
		$data['url'] = "";
		$data['content'] = "";	
		$data['name'] = "";
		$data['descr'] = "";	
		
		$data['filename_src'] = "";		
		$data['first_page'] = 0;
		$data['sort_num'] = 0;
		
		$data['date'] = "";
		$data['datest'] = "";

		return $data;
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");		
		//if( $action == "makelogin" )
		//{
		//	
		//}
				
		$this->pageView->faqgrlist = $this->model->get_faqGroups();	
		//$this->pageView->newsinfo = $this->init_reqvars();
		
		$this->pageView->render_main();		
	}
	
	public function action_groupedit()
	{
		$id = $this->getReqParam("item_id", 0);
		
		$this->pageView->faqgrinfo = $this->model->get_faqGroupInfo($id);
		
		$this->pageView->render_editgroupform();
	}
	
	public function action_groupsave()
	{
		$id = $this->getReqParam("item_id", 0);
		$req = $this->get_reqvars();
		
		//var_dump($req);
		
		$this->model->save_faqGroup($id, $req);
		
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		
		$this->pageView->render_main();
	}
	
	public function action_groupdelete()
	{
		$id = $this->getReqParam("item_id", 0);
		$this->model->delete_faqGroup($id);
		
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		
		$this->pageView->render_main();
	}
	
	public function action_groupadd()
	{
		$req = $this->get_reqvars();
		
		$this->model->add_faqGroup( $req );
		
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		
		$this->pageView->render_main();
	}
	
	public function action_list()
	{
		$this->pageView->faqinfo = $this->init_reqvars();
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		$this->pageView->faqslist = $this->model->get_faqAll();
		
		$this->pageView->render_faqs();
	}

	public function action_edit() {
		$id = $this->getReqParam("item_id", 0);
						
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		$this->pageView->faqinfo = $this->model->get_faqInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_save() {
		$id = $this->getReqParam("item_id", 0);
		$req = $this->get_reqvars();

				
		$this->model->save_faq($id, $req);
		
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		$this->pageView->faqslist = $this->model->get_faqAll();
		
		$this->pageView->render_faqs();
	}
	
	public function action_delete() {
		$id = $this->getReqParam("item_id", 0);
		$this->model->delete_faq($id);
		
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		$this->pageView->faqslist = $this->model->get_faqAll();
		
		$this->pageView->render_faqs();			
	}
	
	public function action_deleteall()
	{
		$ids = $this->getReqParam("items_id", null);
		for( $i=0; $i<count($ids); $i++ )
			$this->model->delete_faq($ids[$i]);
		
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		$this->pageView->faqslist = $this->model->get_faqAll();
		
		$this->pageView->render_faqs();			
	}
	
	public function action_add()
	{
		$req = $this->get_reqvars();
		
		$this->model->add_faq( $req );
		
		$this->pageView->faqgrlist = $this->model->get_faqGroups();
		$this->pageView->faqslist = $this->model->get_faqAll();
		
		$this->pageView->render_faqs();
	}	
}
?>