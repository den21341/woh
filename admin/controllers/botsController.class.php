<?php

class Bots extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        $this->botModel = new BotsModel($this->getCfg(), $db, 1);
        $this->pageView = new BotsView($this->getCfg(), $this->getPageModel(), $this->model);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->catLib = $this->model;
        $this->pageView->botlist = $this->botModel->getBots();

        $this->pageView->render_main();
    }

    public function action_setbot() {
        $bid = $this->getReqParam('bid', '');

        $this->botModel->setBot($bid);

        $this->pageView->catLib = $this->model;
        $this->pageView->botlist = $this->botModel->getBots();

        $this->pageView->render_main();
    }

    public function action_showbot() {
        $bid = $this->getReqParam('bid', '');

        $this->pageView->botmsg = $this->botModel->getMsgs($bid);
        $this->pageView->viewMode = 'msg';

        $this->pageView->render_main();
    }

    public function action_showcomm() {
        $bid = $this->getReqParam('bid', '');

        $this->pageView->botmsg = $this->botModel->getComment($bid);
        $this->pageView->viewMode = 'comm';

        $this->pageView->render_main();
    }

    public function action_sendmsg() {
        $from_id = $this->getReqParam('from', '');
        $to_id = $this->getReqParam('to', '');
        $item = $this->getReqParam('item', '');


        $this->pageView->from_id = $from_id;
        $this->pageView->to_id = $to_id;
        $this->pageView->item = $item;
        $this->pageView->viewMode = 'msg';
        $this->pageView->subMode = 'sendMsg';

        $this->pageView->name = $this->botModel->getUsrName($to_id);
        $this->pageView->botmsg = $this->botModel->getMsgs($from_id);

        $this->pageView->render_main();
    }

    public function action_sendmsgDone() {
        $from_id = $this->getReqParam('from', '');
        $to_id = $this->getReqParam('to', '');
        $item = $this->getReqParam('item', '');
        $mess = $this->getReqParam('mess', '');

        $this->botModel->sendBotMsg($from_id, $to_id, $mess, ($item ? $item : 0));
        $this->pageView->botmsg = $this->botModel->getMsgs($from_id);
        $this->pageView->viewMode = 'msg';

        $this->pageView->render_main();

    }

    public function action_act() {
        $uid = $this->getReqParam('uid', 0);

        if($uid) {
            $this->pageView->bot_list = $this->botModel->getBots(1);
            $this->pageView->act_bot = $uid;

            $this->pageView->viewMode = 'act';
            //$bot_items = $this->botModel
            $this->pageView->render_main();
        }
    }

    public function action_loaditems() {
        $uid = $this->getReqParam('uid', 0);

        if($uid) {
            echo json_encode($this->botModel->getBotsItems($uid));
        }
    }

    public function action_actdone() {
        $act_bot = $this->getReqParam('actbot', 0);
        $sel_bot = $this->getReqParam('bid', 0);
        $pid_bot = $this->getReqParam('pid', 0);
        $mon = $this->getReqParam('mon', 0);

        $this->botModel->addHelpToItem($act_bot, $pid_bot, $mon);

        $this->pageView->bot_list = $this->botModel->getBots(1);
        $this->pageView->act_bot = $act_bot;

        $this->pageView->viewMode = 'act';
        //$bot_items = $this->botModel
        $this->pageView->render_main();
    }

}