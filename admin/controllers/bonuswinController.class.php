<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class BonusWin extends AdmPageController{
	
	protected $model;
	protected $bonusModel;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		/*
		echo getcwd()."<br>";
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
		*/

		//echo "!!!<br>";
			
		///
		$this->model = new Catalog($db, 1);
		
		// Add ClatalogUtilsView Class to view
		include_once "views/catalogUtilsView.class.php";
		$catuViewObj = new CatalogUtilsView($config, $this->model);
		
		$this->bonusModel = new BonusWinModel($this->getCfg(), $db, 1);
		$this->pageView = new BonusWinView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");
		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->catLib = $this->model;
		$this->pageView->winlist = $this->bonusModel->get_BonusWinList();
		
		$this->pageView->render_main();		
	}

	/*
	public function action_editreq()
	{		
		$rid = $this->getReqParam("rid", 0);
		$this->pageView->reqinfo = $this->model->Req_ReviewInfo($rid);
		$this->pageView->reqlist = $this->bonusModel->get_HelpReqList($this->model);
		
		$this->pageView->render_editform();
	}
	
	public function action_savereq()
	{
		$rid = $this->getReqParam("rid", 0);
		
		$proj = Array();

		$proj['id'] 		= $rid;
		$proj['pfirst']		= $this->getReqParam("pfirst", 0);
		$proj['prate']		= $this->getReqParam("prate", 0);
		//$proj['ptit']		= $this->getReqParam("ptit", "");
		$proj['pdescr0']	= $this->getReqParam("pdescr0", "");
		//$proj['pdescr']		= $this->getReqParam("pdescr", "");
		$proj['pamount']	= $this->getReqParam("pamount", 0);
		//$proj['city_id']	= $this->getReqParam("city_id", 0);
		//$proj['country']	= $this->getReqParam("country", "");
		
		$this->reqModel->save_Req($rid, $proj);
		
		$this->pageView->reqlist = $this->reqModel->get_HelpReqList($this->model);
		
		$this->pageView->render_main();
	}
	*/
	
	public function action_delete()
	{
		$id = $this->getReqParam("winid", 0);
		$this->bonusModel->delete_Win($id);
		
		//$this->pageView->reqlist = $this->reqModel->get_HelpReqList($this->model);
		$this->pageView->catLib = $this->model;
		$this->pageView->winlist = $this->bonusModel->get_BonusWinList();
		
		$this->pageView->render_main();			
	}	
}
?>