<?php

class Helpmsg extends AdmPageController {

    protected $model;
    protected $userModel;

    function __construct($config, $db) {
        parent::__construct($config, $db);

        $fileModel = "models/" . strtolower(get_class()) . "Model.class.php";

        echo "Load model: " . $fileModel . "<br />";

        if (!file_exists($fileModel)) {
            die('404 Not Found - Model');
        }

        include_once $fileModel;

        $this->model = new Catalog($db, 1);
        $this->helpmsgModel = new HelpmsgModel($this->getCfg(), $db, 1);
        $this->helpmsgView = new HelpmsgView($this->getCfg(), $this->getPageModel(), $this->model);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->helpmsgView->msglist = $this->helpmsgModel->getUsrHelpMsg();

        $this->helpmsgView->render_main();
    }
}