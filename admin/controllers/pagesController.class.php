<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Pages extends AdmPageController{
	
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		/*
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
		*/
	
		$this->model = new PagesModel($this->getCfg(), $db, $this->getPageModel()->getLangId(), $this->getPageModel()->getLangs());		
		$this->pageView = new PagesView($this->getCfg(), $this->getPageModel(), $this->model);
		$this->pageView->pageinfo = $this->init_reqvars();
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->render_main();		
	}
	
	protected function get_reqvars()
	{
		$data = Array();
		
		$data['pmean'] = $this->getReqParam("pmean", "");
		$data['pfile'] = $this->getReqParam("pfile", "");
		$data['pagename'] = $data['pfile'];
		
		$data['title'] = $this->getReqParam("title", "");
		$data['header'] = $this->getReqParam("header", "", false);
		$data['content'] = $this->getReqParam("content", "", false);
		$data['content']=str_replace ("&gt;",">",$data['content']);
		$data['content']=str_replace ("&lt;","<",$data['content']);		
		
		$data['seo_title'] = $this->getReqParam("ptitle", "");		
		$data['seo_keyw'] = $this->getReqParam("pkey", "");
		$data['seo_descr'] = $this->getReqParam("pdescr", "");

		$data['parent_id'] = $this->getReqParam("parentid", 0);
		$data['sort_num'] = $this->getReqParam("psort", "0");
		$data['show_in_menu'] = $this->getReqParam("menushow", 0);
		$data['page_record_type'] = $this->getReqParam("pagetype", 1);
		$data['with_editor'] = $this->getReqParam("withedit", 1);

		if( $data['sort_num'] == "" )
			$data['sort_num'] = 0;
		
		$data['page_ico'] = "";
		
		return $data;
	}
	
	protected function init_reqvars()
	{
		$data = Array();
		$data['id'] = 0;
		$data['pmean'] = "";
		$data['pfile'] = $data['pagename'] = "";
		
		$data['title'] = "";
		$data['header'] = "";
		$data['content'] = "";		
		
		$data['seo_title'] = "";		
		$data['seo_keyw'] = "";
		$data['seo_descr'] = "";

		$data['parent_id'] = 0;
		$data['sort_num'] = 0;
		$data['show_in_menu'] = 0;
		$data['page_record_type'] = 1;		
		$data['with_editor'] = 1;
		
		$data['page_ico'] = "";

		return $data;
	}

	public function action_edit()
	{		
		$id = $this->getReqParam("id", 0);
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_save()
	{
		$id = $this->getReqParam("id", 0);		
		$req = $this->get_reqvars();
		
		if( $req['parent_id'] == $id )
		{
			$this->pageView->error_msg = "Нельзя в качестве родительской указывать эту же страницу.";
			$this->pageView->pageinfo = $req;
			$this->pageView->render_editform();
		}
		else
		{
			$this->model->save_Page($id, $req);
			$this->pageView->render_main();
		}		
	}
	
	public function action_add()
	{
		$req = $this->get_reqvars();
		
		$this->model->add_Page( $req );
		
		//$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main();
	}
	
	public function action_delete()
	{
		$id = $this->getReqParam("id", 0);
		$this->model->delete_Page($id);
		
		//$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main();			
	}

	public function action_resunassign()
	{
		$id = $this->getReqParam("id", 0);
		$resid = $this->getReqParam("resid", 0);
		
		if( $resid != 0 )
			$this->model->resource_Del($resid);
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_resadd()
	{
		$id = $this->getReqParam("id", 0);
		
		$reqd = Array();
		$reqd['resdisp']	= $this->getReqParam("resdisp", 1);
		$reqd['resind']		= $this->getReqParam("resind", 0);
		$reqd['resblock']	= $this->getReqParam("resblock", 0);
		
		if($reqd['resblock'] != 0 )
		{			
			$this->model->resource_Add($id, $reqd);
		}
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_photoadd()
	{
		$id = $this->getReqParam("id", 0);
		
		$reqd = Array();
		$reqd['phototitle']	= $this->getReqParam("phototitle", "");
		$reqd['photoind']	= $this->getReqParam("photoind", 0);
		$reqd['photodescr']	= $this->getReqParam("photodescr", "");
		$photofile	= $_FILES['photofile'];
		
		if( isset($photofile['name']) && ($photofile['name'] != "") && ($photofile['tmp_name'] != "") )
		{
			$this->model->photo_Add($id, $photofile, $reqd);
		}
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_photodel()
	{
		$id = $this->getReqParam("id", 0);
		$photoid = $this->getReqParam("photoid", 0);
		
		$this->model->photo_Del($photoid);
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		$this->pageView->render_editform();
	}
	
	public function action_photominus()
	{
		$id = $this->getReqParam("id", 0);
		$photoid = $this->getReqParam("photoid", 0);
		
		$this->model->photo_Minus($photoid);
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);		
		$this->pageView->render_editform();
	}
	
	public function action_photoplus()
	{
		$id = $this->getReqParam("id", 0);
		$photoid = $this->getReqParam("photoid", 0);
		
		$this->model->photo_Plus($photoid);
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		$this->pageView->render_editform();
	}
	
	public function action_photoedit()
	{
		$id = $this->getReqParam("id", 0);
		$photoid = $this->getReqParam("photoid", 0);
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editphoto($photoid);
	}
	
	public function action_photosave()
	{
		$id = $this->getReqParam("id", 0);
		$photoid = $this->getReqParam("photoid", 0);
		
		$reqd = Array();
		$reqd['phototitle']	= $this->getReqParam("phototitle", "");
		$reqd['photoind']	= $this->getReqParam("photoind", 0);
		$reqd['photodescr']	= $this->getReqParam("photodescr", "");
		
		$this->model->photo_Save($photoid, $reqd);
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_videoadd()
	{
		$id = $this->getReqParam("id", 0);
		
		$reqd = Array();
		$reqd['phototitle']	= $this->getReqParam("phototitle", "");
		$reqd['photoind']	= $this->getReqParam("photoind", 0);
		$reqd['photodescr']	= $this->getReqParam("photodescr", "");
		$reqd['photofile']	= $this->getReqParam("photofile", "");
		$reqd['photoyoutube']	= $this->getReqParam("photoyoutube", "");
		
		if( (trim($reqd['photofile']) != "") || (trim($reqd['photoyoutube']) != "") )
		{			
			$this->model->video_Add($id, $reqd);
		}
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_videodel()
	{
		$id = $this->getReqParam("id", 0);
		$videoid = $this->getReqParam("videoid", 0);
		
		if( $videoid != 0 )
			$this->model->video_Del($videoid);
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_videoedit()
	{
		$id = $this->getReqParam("id", 0);
		$videoid = $this->getReqParam("videoid", 0);
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editvideo($videoid);
	}
	
	public function action_videosave()
	{
		$id = $this->getReqParam("id", 0);
		$videoid = $this->getReqParam("videoid", 0);
		
		$reqd = Array();
		$reqd['phototitle']	= $this->getReqParam("phototitle", "");
		$reqd['photoind']	= $this->getReqParam("photoind", 0);
		$reqd['photodescr']	= $this->getReqParam("photodescr", "");
		$reqd['photofile']	= $this->getReqParam("photofile", "");
		$reqd['photoyoutube']	= $this->getReqParam("photoyoutube", "");
		
		if( (trim($reqd['photofile']) != "") || (trim($reqd['photoyoutube']) != "") )
		{
			$this->model->video_Save($videoid, $reqd);
		}
		
		$this->pageView->pageinfo = $this->model->get_PageInfo($id);
		
		$this->pageView->render_editform();
	}
	
}
?>