<?php

class Promise extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->promModel = new PromiseModel($this->getCfg(), $db, 1);
        $this->pageView = new PromiseView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->catLib = $this->model;
        //this->pageView->bonlist = $this->bonusModel->get_BonusList();
        $this->pageView->allPr = $this->promModel->getPromiseAllItem();

        $this->pageView->render_main();
    }

    public function action_proj() {
        $pid = $this->getReqParam('id', 0);
        $type = $this->getReqParam('type', 0);
       
        if(!$pid) {
            $this->pageView->its = $this->promModel->getPromiseByAllItem();
            $this->pageView->viewMode = 'byitem';
        } else {
            $this->pageView->its = $this->promModel->getUserPromiseByItem($pid);
            $this->pageView->viewMode = 'byuser';
        }
        if($type) {
            $this->pageView->its = $this->promModel->getPromiseByAllItem($type);
            $this->pageView->viewMode = 'byitem';
        }

        $this->pageView->render_main();
    }

    public function action_mess() {
        $uid = $this->getReqParam('uid', 0);
        $pid = $this->getReqParam('pid', 0);
        $this->pageView->its = [];

        if($pid)
            $this->pageView->its = $this->promModel->getUserPromiseByItem($pid);


        $this->pageView->uid = $uid;
        $this->pageView->pid = $pid;
        $this->pageView->viewMode = 'byuser';
        $this->pageView->subMode = 'msg';

        $this->pageView->render_main();
    }

    public function action_sendmess() {
        $uid = $this->getReqParam('uid', 0);
        $mess = $this->getReqParam('message', '');
        $pid = $this->getReqParam('pid', 0);
        $this->pageView->its = [];

        if($pid)
            $this->pageView->its = $this->promModel->getUserPromiseByItem($pid);

        $this->promModel->sendBotMsg('481', $uid, $mess, 0);

        $this->pageView->uid = $uid;
        $this->pageView->pid = $pid;
        $this->pageView->viewMode = 'byuser';
        $this->pageView->subMode = '';

        $this->pageView->render_main();
    }

}