<?php

class Testproj extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->testModel = new TestprojModel($this->getCfg(), $db, 1);
        $this->pageView = new TestprojView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        //$this->pageView->catLib = $this->model;
        
        $this->pageView->usersId = $this->testModel->getUserId();
       // $this->pageView->ans = $this->testModel->getUserAns();

        $this->pageView->render_main();
    }

    public function action_answers() {
        $from = $this->getReqParam('from','');
        $to = $this->getReqParam('to','');

        $this->pageView->usersId = $this->testModel->getUserId();
        $this->pageView->ans = $this->testModel->getUserAns();

        if($from !== '' && $to !== ''){
            $this->pageView->ansList = $this->testModel->getUserAnsBy($from, $to);
            $this->pageView->viewMode = 'ansby';
        } else
            $this->pageView->viewMode = 'ans';

        $this->pageView->render_main();
    }

    public function action_viewans() {
        $uid = $this->getReqParam('uid','');

        if($uid) {
            $this->pageView->uans = $this->testModel->getUserAnsByOne($uid);
            $this->pageView->viewMode = 'userAns';
        }
        //$this->pageView->ansList = $this->testModel->getUserAnsBy($from, $to);
        $this->pageView->usersId = $this->testModel->getUserId();
        $this->pageView->ans = $this->testModel->getUserAns();
        $this->pageView->render_main();
    }

    public function action_ansall() {

        $this->pageView->allSum = $this->testModel->getSumAllAns();
        $this->pageView->viewMode = 'allans';
        
        $this->pageView->usersId = $this->testModel->getUserId();
        $this->pageView->ans = $this->testModel->getUserAns();
        $this->pageView->render_main();
    }
}