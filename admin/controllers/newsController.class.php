<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class News extends AdmPageController{
	
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		/*
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
		*/
		
		// Add ClatalogUtilsView Class to view
		include_once "views/catalogUtilsView.class.php";
		
		$this->catalogModel = new Catalog($db, 1);
		
		$catuViewObj = new CatalogUtilsView($config, $this->catalogModel);
	
		$this->model = new NewsModel($this->getCfg(), $db, 1, $this->getPageModel()->getLangs());
		$this->pageView = new NewsView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj, $this->getReqParam("pi", 0), $this->getReqParam("pn", 50));
		
		$this->pageView->newsinfo = $this->init_reqvars();
	}
	
	protected function get_reqvars()
	{
		$data = Array();
		
		$data['ngroup'] = $this->getReqParam("ntype", "");
		$data['sect_id'] = $this->getReqParam("sect_id", 0);
		$data['title'] = $this->getReqParam("newstitle", "");		
		$data['content'] = $this->getReqParam("newscont", "");
		
		$data['filename_src'] = $this->getReqParam("myfile", "");	
		$data['first_page'] = $this->getReqParam("newsfirst", "0");		
		$data['sort_num'] = $this->getReqParam("orgsort", "0");
		if( $data['sort_num'] == "" )
			$data['sort_num'] = $this->getReqParam("datest", "");
		
		$data['datest'] = "";
		
		return $data;
	}
	
	protected function init_reqvars()
	{
		$data = Array();
		$data['id'] = 0;
		$data['ngroup'] = 0;
		$data['sect_id'] = 0;		
		
		$data['title'] = "";
		$data['url'] = "";
		$data['content'] = "";		
		
		$data['filename_src'] = "";		
		$data['first_page'] = 0;
		$data['sort_num'] = 0;
		
		$data['date'] = "";
		$data['datest'] = "";

		return $data;
	}
	
	private function _news_prepare()
	{
		$ngroup = $this->getReqParam("ntype", 0);
		
		$this->pageView->viewGroup = $ngroup;
		
		$this->pageView->newslist = $this->model->get_newsList($ngroup);
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");		
		
				
		$this->_news_prepare();
		//$this->pageView->newsinfo = $this->init_reqvars();
		
		$this->pageView->render_main();		
	}

	public function action_edit()
	{		
		$id = $this->getReqParam("item_id", 0);
				
		$this->pageView->newsinfo = $this->model->get_newsInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_save()
	{
		$id = $this->getReqParam("item_id", 0);
		$req = $this->get_reqvars();
				
		$this->model->save_News($id, $req);
		
		//$this->pageView->newslist = $this->model->get_newsList();
		$this->_news_prepare();
		
		$this->pageView->render_main();
	}
	
	public function action_delete()
	{
		$id = $this->getReqParam("item_id", 0);
		$this->model->delete_News($id);
		
		//$this->pageView->newslist = $this->model->get_newsList();
		$this->_news_prepare();
		
		$this->pageView->render_main();			
	}
	
	public function action_deleteall()
	{
		$ids = $this->getReqParam("items_id", null);
		for( $i=0; $i<count($ids); $i++ )
			$this->model->delete_News($ids[$i]);
		
		//$this->pageView->newslist = $this->model->get_newsList();
		$this->_news_prepare();
		
		$this->pageView->render_main();			
	}
	
	public function action_add() {
		$req = $this->get_reqvars();

		$this->model->add_News($req, $_FILES);
		
		//$this->pageView->newslist = $this->model->get_newsList();
		$this->_news_prepare();
		
		$this->pageView->render_main();
	}	
}
?>