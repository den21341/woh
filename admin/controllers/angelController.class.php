<?php

class Angel extends AdmPageController
{

    protected $model;
    protected $userModel;

    function __construct($config, $db)
    {
        parent::__construct($config, $db);

        $fileModel = "models/" . strtolower(get_class()) . "Model.class.php";

        echo "Load model: " . $fileModel . "<br />";

        if (!file_exists($fileModel)) {
            die('404 Not Found - Model');
        }

        include_once $fileModel;

        $this->model = new Catalog($db, 1);
        $this->angModel = new AngelModel($this->getCfg(), $db, 1);
        $this->angView = new AngelView($this->getCfg(), $this->getPageModel(), $this->model);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->angView->userlist = $this->angModel->getUserMoneyList();
        $this->angView->userStatuslist = $this->angModel->getUserWithStatus();

        $this->angView->render_main();
    }

    public function action_angDay() {
        $uid = $this->getReqParam("uid", 0);
        $st = $this->getReqParam("active", 0);

        $this->angModel->setUserAngelStatus($uid, $st, 'day', 5);

        $this->angView->userlist = $this->angModel->getUserMoneyList();
        $this->angView->userStatuslist = $this->angModel->getUserWithStatus();

        $this->angView->render_main();
    }

    public function action_angWeek() {
        $uid = $this->getReqParam("uid", 0);
        $st = $this->getReqParam("active", 0);

        $this->angModel->setUserAngelStatus($uid, $st, 'week', 20);

        $this->angView->userlist = $this->angModel->getUserMoneyList();
        $this->angView->userStatuslist = $this->angModel->getUserWithStatus();

        $this->angView->render_main();
    }

    public function action_sort_day() {
        $action = $this->getReqParam("action", "");

        $this->angView->userlist = $this->angModel->getUserMoneyList();
        $this->angView->userStatuslist = $this->angModel->getUserWithStatus();

        $this->angView->render_main();
    }

    public  function action_sort_week() {
        $action = $this->getReqParam("action", "");

        $this->angView->userlist = $this->angModel->getUserMoneyList('money_week',20);
        $this->angView->userStatuslist = $this->angModel->getUserWithStatus();

        $this->angView->render_main();
    }
}