<?php

class Iktable extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->boxModel = new IktableModel($this->getCfg(), $db, 1);
        $this->pageView = new IktableView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->catLib = $this->model;
        //this->pageView->bonlist = $this->bonusModel->get_BonusList();

        $this->pageView->render_main();
    }

    public function action_load() {
        $pay_result = $this->getReqParam('sel', '');
        $pay_to_date = $this->getReqParam('val', '-');
        $currency = $this->getReqParam('curr', '');

        if($pay_result && $pay_to_date != '-' && is_numeric($pay_to_date) && $currency) {
            $this->pageView->render_httpIk($pay_result, $pay_to_date);
        }
    }
}