<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Preferences extends AdmPageController{
	
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
	
		$this->model = new PreferencesModel($this->getCfg(), $db, 1);
		$this->pageView = new PreferencesView($this->getCfg(), $this->getPageModel(), $this->model);
	}
	
	public function action_default()
	{									
		$error_msg = "";
		//$action = $this->getReqParam("action", "");		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->prefs = $this->model->get_PrefList();	
		
		$this->pageView->render_main();		
	}	
	
	public function action_save()
	{
		$id 		= $this->getReqParam("optid", 0);
		$content	= $this->getReqParam("optval", "");
		
		$this->model->save_Pref($id, $content);
		
		$this->pageView->prefs = $this->model->get_PrefList();	
		
		$this->pageView->render_main();
	}
}
?>