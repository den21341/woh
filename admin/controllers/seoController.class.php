<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Seo extends AdmPageController{
	
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
	
		$this->model = new SeoModel($this->getCfg(), $db, 1);
		$this->pageView = new SeoView($this->getCfg(), $this->getPageModel(), $this->model);
	}
	
	protected function get_reqvars()
	{
		$data = Array();
		
		$data['url'] = $this->getReqParam("orgurl", "");
		$data['h1'] = $this->getReqParam("orgh1", "");
		
		$data['title'] = $this->getReqParam("orgtitle", "");
		$data['keyw'] = $this->getReqParam("orgkeyw", "");
		$data['descr'] = $this->getReqParam("orgdescr", "");
		
		$data['text1'] = $this->getReqParam("txt1", "");		
		$data['text2'] = $this->getReqParam("txt2", "");		
		
		return $data;
	}
	
	protected function init_reqvars()
	{
		$data = Array();
		$data['id'] = 0;
		
		$data['url'] = "";
		$data['h1'] = "";
		
		$data['title'] = "";		
		$data['keyw'] = "";
		$data['descr'] = "";		
		
		$data['text1'] = "";		
		$data['text2'] = "";

		return $data;
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");		
		//if( $action == "makelogin" )
		//{
		//	
		//}
				
		$this->pageView->itslist = $this->model->get_seoList();	
		$this->pageView->itinfo = $this->init_reqvars();
		
		$this->pageView->render_main();		
	}

	public function action_edit()
	{		
		$id = $this->getReqParam("id", 0);
		$this->pageView->itinfo = $this->model->get_Seo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_save()
	{
		$id = $this->getReqParam("id", 0);
		$req = $this->get_reqvars();
				
		if( !$this->model->save_Seo($id, $req) )
			$this->pageView->itinfo = $req;
		else 
			$this->pageView->itinfo = $this->init_reqvars();
		
		$this->pageView->itslist = $this->model->get_seoList();
		
		$this->pageView->render_main();
	}
	
	public function action_delete()
	{
		$id = $this->getReqParam("id", 0);
		$this->model->delete_Seo($id);
				
		$this->pageView->itslist = $this->model->get_seoList();
		$this->pageView->itinfo = $this->init_reqvars();
		
		$this->pageView->render_main();			
	}
	
	public function action_deleteall()
	{
		$ids = $this->getReqParam("items_id", null);
		for( $i=0; $i<count($ids); $i++ )
			$this->model->delete_Seo($ids[$i]);
		
		$this->pageView->itslist = $this->model->get_seoList();
		$this->pageView->itinfo = $this->init_reqvars();
		
		$this->pageView->render_main();			
	}
	
	public function action_add()
	{
		$req = $this->get_reqvars();
		
		if( !$this->model->add_Seo( $req ) )
			$this->pageView->itinfo = $req;
		else
			$this->pageView->itinfo = $this->init_reqvars();
		
		$this->pageView->itslist = $this->model->get_seoList();
		
		$this->pageView->render_main();
	}	
}
?>