<?php

class Projcur extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->pcModel= new ProjcurModel($this->getCfg(), $db, 1);
        $this->pageView = new ProjcurView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->catLib = $this->model;
        $this->pageView->projlist = $this->pcModel->getProjCurrencyList();

        $this->pageView->render_main();
    }
}