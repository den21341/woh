<?php

class Popup extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->popModel = new PopupModel($this->getCfg(), $db, 1);
        $this->pageView = new PopupView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        $this->pageView->catLib = $this->model;
        $this->pageView->popups = $this->popModel->getCountPopup($this->pageView->name = $this->popModel->getPagePopup());
        //echo '<pre>';
        //print_r($this->pageView->popups);
        $this->pageView->render_main();
    }
}