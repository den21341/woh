<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Projects extends AdmPageController {
	
	protected $model;
	protected $projModel;
	
	function __construct($config, $db) {
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) ) {
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;		
			
		///
		$this->model = new Catalog($db, 1);
		
		// Add ClatalogUtilsView Class to view
		include_once "views/catalogUtilsView.class.php";
		$catuViewObj = new CatalogUtilsView($config, $this->model);
		
		$this->projModel = new ProjectsModel($this->getCfg(), $db, 1);
		$this->pageView = new ProjectsView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
	}
	
	public function action_default() {
		$error_msg = "";
		$action = $this->getReqParam("action", "");
		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->projlist = $this->projModel->get_ProjList($this->model);
		
		$this->pageView->render_main();		
	}

	public function action_editproj() {
		$pid = $this->getReqParam("pid", 0);
		$this->pageView->projinfo = $this->model->Item_Info($pid);
		$this->pageView->projlist = $this->projModel->get_ProjList($this->model);
		
		$this->pageView->render_editform();
	}
	
	public function action_saveproj() {
		$pid = $this->getReqParam("pid", 0);
		
		$proj = Array();

		$proj['id'] 		= $pid;
		$proj['psect']		= $this->getReqParam("psect", 0);
		$proj['ptit']		= $this->getReqParam("ptit", "");
		$proj['pdescr0']	= $this->getReqParam("pdescr0", "");
		$proj['pdescr']		= $this->getReqParam("pdescr", "");
		$proj['pamount']	= $this->getReqParam("pamount", 0);
		$proj['city_id']	= $this->getReqParam("city_id", 0);
		$proj['country']	= $this->getReqParam("country", "");
		$proj['pactive']	= $this->getReqParam("pactive", 0);
		$proj['pmoderated']	= $this->getReqParam("pmoderated", 0);
		$proj['pis_check']	= $this->getReqParam("pcheck", 0);
		$proj['padmrate']	= $this->getReqParam("padmrate", 0);
		$proj['phlptype']	= $this->getReqParam("phlptype", 0);
		$proj['ploctype']	= $this->getReqParam("ploctype", 0);

		$this->projModel->save_Proj($pid, $proj);
		
		$this->pageView->projlist = $this->projModel->get_ProjList($this->model);
		
		$this->pageView->render_main();
	}
	
	public function action_deleteproj() {
		$pid = $this->getReqParam("pid", 0);
		$this->projModel->delete_Proj($pid);
		
		$this->pageView->projlist = $this->projModel->get_ProjList($this->model);
		
		$this->pageView->render_main();			
	}
	
		
	public function action_status() {
		$pid = $this->getReqParam("pid", 0);
		$active = $this->getReqParam("status", 0);
		$this->projModel->set_ProjStatus($pid, $active);

		$this->pageView->projlist = $this->projModel->get_ProjList($this->model);

		$this->pageView->render_main();
	}

	public function action_save() {
		echo 'DONE!!!';
	}

	public function action_projdone() {
		$pyd = $this->getReqParam('pyd', '');
		$pid = $this->getReqParam('pid', 0);

		if($pyd != '' && $pid) {
			$this->projModel->set_PayedItem($pid, $pyd);
		}

		$this->pageView->projlist = $this->projModel->get_PayedItem();//$this->projModel->get_ProjList($this->model, -1, 0, PROJ_STATUS_ALL, 'payed', 'money');
		$this->pageView->viewMode = 'projdone';
		$this->pageView->render_main();
	}
}