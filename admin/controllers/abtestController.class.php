<?php

class Abtest extends AdmPageController {

    protected $model;
    protected $bonusModel;

    function __construct($config, $db) {

        parent::__construct($config, $db);

        $this->model = new Catalog($db, 1);

        // Add ClatalogUtilsView Class to view
        include_once "views/catalogUtilsView.class.php";
        $catuViewObj = new CatalogUtilsView($config, $this->model);

        $this->abModel = new AbtestModel($this->getCfg(), $db, 1);
        $this->pageView = new AbtestView($this->getCfg(), $this->getPageModel(), $this->model, $catuViewObj);
    }

    public function action_default() {
        $error_msg = "";
        $action = $this->getReqParam("action", "");

        //$this->pageView->abVal = $this->abModel->getABvalues();
        //$this->pageView->abList_t1 = $this->abModel->getABTestList();
        //$this->pageView->abList_t2 = $this->abModel->getABTestList(2);
        //$this->pageView->abListCount = $this->abModel->getABallByVal();
        $this->pageView->res = $this->abModel->getInfoSum();

        $this->pageView->render_main();
    }

    public function action_proj() {
        $pid = $this->getReqParam('pid', 0);

        $this->pageView->viewModeBe == 'proj';
        $this->pageView->res_byproj = $this->abModel->getInfoSumByProj($pid);
        $this->pageView->res = $this->abModel->getInfoSum();

        $this->pageView->render_main();
    }


    public function action_addnew() {
        $val = $this->getReqParam('cur', 0);

        $this->abModel->setABvalues($val);

        $this->pageView->abVal = $this->abModel->getABvalues();
        $this->pageView->abList_t1 = $this->abModel->getABTestList();
        $this->pageView->abList_t2 = $this->abModel->getABTestList(2);

        $this->pageView->render_main();
    }

    public function action_delval() {
        $val = $this->getReqParam('cur', 0);
        $this->abModel->delABvalues($val);

        $this->pageView->abVal = $this->abModel->getABvalues();
        $this->pageView->abList_t1 = $this->abModel->getABTestList();
        $this->pageView->abList_t2 = $this->abModel->getABTestList(2);

        $this->pageView->render_main();
    }//
}
