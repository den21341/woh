<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Splashslides extends AdmPageController{
	
	protected $model;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
	
		$this->model = new SplashslidesModel($this->getCfg(), $db, 1);
		$this->pageView = new SplashslidesView($this->getCfg(), $this->getPageModel(), $this->model);
	}
	
	protected function get_reqvars()
	{
		$data = Array();
		
		$data['title'] = $this->getReqParam("orgname", "");
		$data['url'] = $this->getReqParam("orgurl", "");
		$data['comment'] = $this->getReqParam("orgdescr", "");
		
		$data['filename'] = $this->getReqParam("myfile", "");		
		$data['sort_num'] = $this->getReqParam("orgsort", "0");
		if( $data['sort_num'] == "" )
			$data['sort_num'] = 0;
		
		return $data;
	}
	
	protected function init_reqvars()
	{
		$data = Array();
		$data['id'] = 0;
		
		$data['title'] = "";
		$data['url'] = "";
		$data['comment'] = "";		
		
		$data['filename'] = "";		
		$data['sort_num'] = "";

		return $data;
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");		
		//if( $action == "makelogin" )
		//{
		//	
		//}
				
		$this->pageView->slidelist = $this->model->get_slideList();	
		$this->pageView->slideinfo = $this->init_reqvars();
		
		$this->pageView->render_main();		
	}

	public function action_edit()
	{		
		$id = $this->getReqParam("id", 0);
		$this->pageView->slideinfo = $this->model->get_slideInfo($id);
		
		$this->pageView->render_editform();
	}
	
	public function action_save()
	{
		$id = $this->getReqParam("id", 0);
		$req = $this->get_reqvars();
				
		$this->model->save_Slide($id, $req);
		
		$this->pageView->slidelist = $this->model->get_slideList();
		
		$this->pageView->render_main();
	}
	
	public function action_delete()
	{
		$id = $this->getReqParam("id", 0);
		$this->model->delete_Slide($id);
		
		$this->pageView->slidelist = $this->model->get_slideList();
		
		$this->pageView->render_main();			
	}
	
	public function action_deleteall()
	{
		$ids = $this->getReqParam("items_id", null);
		for( $i=0; $i<count($ids); $i++ )
			$this->model->delete_Slide($ids[$i]);
		
		$this->pageView->slidelist = $this->model->get_slideList();
		
		$this->pageView->render_main();			
	}
	
	public function action_add()
	{
		$req = $this->get_reqvars();
		
		$this->model->add_Slide( $req );
		
		$this->pageView->slidelist = $this->model->get_slideList();
		
		$this->pageView->render_main();
	}	
}
?>