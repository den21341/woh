<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Users extends AdmPageController{
	
	protected $userModel;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
	
		$this->userModel = new UsersModel($this->getCfg(), $db, 1);
		$this->pageView = new UsersView($this->getCfg(), $this->getPageModel(), $this->userModel);
		
		$this->pageView->userinfo = $this->init_reqvars();
	}
	
	function init_reqvars()
	{
		$uprof = Array();

		$uprof['id'] = 0;
		$uprof['name']		= "";
		//$uprof['passwd']	= $this->getReqParam("passwd", "");
		$uprof['login']		= "";
		$uprof['address']	= "";
		$uprof['city_id']	= 0;
		$uprof['country']	= "";
		$uprof['zip_code']	= "";
		$uprof['telephone']	= "";
		$uprof['office_phone']= "";
		$uprof['cell_phone']= "";
		$uprof['email1']	= "";
		$uprof['email2']	= "";
		$uprof['email3']	= "";
		$uprof['web_url']	= "";
		$uprof['group_id']	= 0;
		
		return $uprof;		
	}
	
	function get_reqvars()
	{
		$uprof = Array();
		
		$uprof['id'] 		= $this->getReqParam("uid", 0);;
		$uprof['name']		= $this->getReqParam("fullname", "");
		//$uprof['passwd']	= $this->getReqParam("passwd", "");
		$uprof['login']		= $this->getReqParam("login", "");
		$uprof['address']	= $this->getReqParam("address", "");
		$uprof['city_id']	= $this->getReqParam("city_id", 0);
		$uprof['country']	= $this->getReqParam("country", "");
		$uprof['zip_code']	= $this->getReqParam("zip", "");
		$uprof['telephone']	= $this->getReqParam("phone1", "");
		$uprof['office_phone']= $this->getReqParam("phone2", "");
		$uprof['cell_phone']= $this->getReqParam("phone3", "");
		$uprof['email1']	= $this->getReqParam("email1", "");
		$uprof['email2']	= $this->getReqParam("email2", "");
		$uprof['email3']	= $this->getReqParam("email3", "");
		$uprof['web_url']	= $this->getReqParam("weburl", "");
		$uprof['group_id']	= $this->getReqParam("userlevel", "0");		
		
		return $uprof;		
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");
		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->userlist = $this->userModel->get_userList();	
		
		$this->pageView->render_main();		
	}

	public function action_edituser()
	{		
		$uid = $this->getReqParam("uid", 0);
		$this->pageView->userinfo = $this->userModel->get_userInfo($uid);
		$this->pageView->userlist = $this->userModel->get_userList();	
		
		$this->pageView->render_editform();
	}
	
	public function action_saveuser()
	{
		$uid = $this->getReqParam("uid", 0);
		
		$uprof = $this->get_reqvars();
		
		/*
		$uprof = Array();

		$uprof['name']		= $this->getReqParam("fullname", "");
		//$uprof['passwd']	= $this->getReqParam("passwd", "");
		$uprof['login']		= $this->getReqParam("login", "");
		$uprof['address']	= $this->getReqParam("address", "");
		$uprof['city_id']	= $this->getReqParam("city_id", 0);
		$uprof['country']	= $this->getReqParam("country", "");
		$uprof['zip_code']	= $this->getReqParam("zip", "");
		$uprof['telephone']	= $this->getReqParam("phone1", "");
		$uprof['office_phone']= $this->getReqParam("phone2", "");
		$uprof['cell_phone']= $this->getReqParam("phone3", "");
		$uprof['email1']	= $this->getReqParam("email1", "");
		$uprof['email2']	= $this->getReqParam("email2", "");
		$uprof['email3']	= $this->getReqParam("email3", "");
		$uprof['web_url']	= $this->getReqParam("weburl", "");
		$uprof['group_id']	= $this->getReqParam("userlevel", "0");
		$uprof['usertrader']= $this->getReqParam("usertrader", 0);
		*/
		
		$this->userModel->save_User($uid, $uprof);
		
		$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main();
	}
	
	public function action_deleteuser()
	{
		$uid = $this->getReqParam("uid", 0);
		$this->userModel->delete_User($uid);
		
		$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main();			
	}
	
	public function action_adduser()
	{
		$uprof = $uprof = $this->get_reqvars();
		/*
		$uprof = Array();

		$uprof['name']		= $this->getReqParam("fullname", "");
		$uprof['passwd']	= $this->getReqParam("passwd", "");
		$uprof['login']		= $this->getReqParam("login", "");
		$uprof['address']	= $this->getReqParam("address", "");
		$uprof['city_id']	= $this->getReqParam("city_id", 0);
		$uprof['country']	= $this->getReqParam("country", "");
		$uprof['zip_code']	= $this->getReqParam("zip", "");
		$uprof['telephone']	= $this->getReqParam("phone1", "");
		$uprof['office_phone']= $this->getReqParam("phone2", "");
		$uprof['cell_phone']= $this->getReqParam("phone3", "");
		$uprof['email1']	= $this->getReqParam("email1", "");
		$uprof['email2']	= $this->getReqParam("email2", "");
		$uprof['email3']	= $this->getReqParam("email3", "");
		$uprof['web_url']	= $this->getReqParam("weburl", "");
		$uprof['group_id']	= $this->getReqParam("userlevel", "0");
		$uprof['usertrader']= $this->getReqParam("usertrader", 0);
		*/
		
		$this->userModel->add_User( $uprof );
		
		$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main();
	}
	
	public function action_status()
	{
		$uid = $this->getReqParam("uid", 0);
		$active = $this->getReqParam("active", 0);
		$this->userModel->set_Status($uid, $active);
		
		$this->pageView->userlist = $this->userModel->get_userList();
		
		$this->pageView->render_main();
	}
}
?>