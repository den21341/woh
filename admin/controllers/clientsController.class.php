<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

class Clients extends AdmPageController{
	
	protected $model;
	protected $userModel;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
	
		$fileModel = "models/".strtolower(get_class())."Model.class.php";
		
		echo "Load model: ".$fileModel."<br />";
	
		if( !file_exists($fileModel) )
		{
			die('404 Not Found - Model');
		}
		
		include_once $fileModel;
	
		$this->model = new Catalog($db, 1);
		$this->userModel = new ClientsModel($this->getCfg(), $db, 1);
		$this->pageView = new ClientsView($this->getCfg(), $this->getPageModel(), $this->model);
	}
	
	public function action_default()
	{									
		$error_msg = "";
		$action = $this->getReqParam("action", "");
		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->userlist = $this->model->Buyer_List();	
		
		$this->pageView->render_main();		
	}

	public function action_edituser()
	{		
		$uid = $this->getReqParam("uid", 0);
		$this->pageView->userinfo = $this->model->Buyer_Info($uid);
		$this->pageView->userlist = $this->model->Buyer_List();
		
		$this->pageView->render_editform();
	}
	
	public function action_saveuser()
	{
		$uid = $this->getReqParam("uid", 0);
		
		$uprof = Array();

		$uprof['id'] 		= $uid;
		$uprof['name']		= $this->getReqParam("name", "");
		$uprof['fname']		= $this->getReqParam("fname", "");
		//$uprof['passwd']	= $this->getReqParam("passwd", "");
		$uprof['login']		= $this->getReqParam("login", "");
		$uprof['address']	= $this->getReqParam("address", "");
		$uprof['city_id']	= $this->getReqParam("city_id", 0);
		$uprof['country']	= $this->getReqParam("country", "");
		//$uprof['zip_code']	= $this->getReqParam("zip", "");
		$uprof['phone']		= $this->getReqParam("phone", "");
		//$uprof['office_phone']= $this->getReqParam("phone2", "");
		//$uprof['cell_phone']= $this->getReqParam("phone3", "");
		$uprof['email']		= $this->getReqParam("email", "");
		//$uprof['email2']	= $this->getReqParam("email2", "");
		//$uprof['email3']	= $this->getReqParam("email3", "");
		$uprof['web_url']	= $this->getReqParam("weburl", "");
		$uprof['account_type']	= $this->getReqParam("userlevel", "0");
		//$uprof['usertrader']= $this->getReqParam("usertrader", 0);
		
		$this->userModel->save_Buyer($uid, $uprof);
		
		$this->pageView->userlist = $this->model->Buyer_List();
		
		$this->pageView->render_main();
	}
	
	public function action_deleteuser()
	{
		$uid = $this->getReqParam("uid", 0);
		$this->userModel->delete_Buyer($uid);
		
		$this->pageView->userlist = $this->model->Buyer_List();
		
		$this->pageView->render_main();			
	}
	
		
	public function action_status()
	{
		$uid = $this->getReqParam("uid", 0);
		$active = $this->getReqParam("active", 0);
		$this->userModel->set_BuyerStatus($uid, $active);
		
		$this->pageView->userlist = $this->model->Buyer_List();
		
		$this->pageView->render_main();
	}
}
?>