<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("FM_LASTDIR_COOKIE", "memfmdir");
define("FM_LASTDIR_LIFETIME", 3600*3);

class FileMan extends AdmPageController{
	
	protected $model;
	protected $base_dir;
	protected $popupmode;
	
	function __construct($config, $db)
	{
		parent::__construct($config, $db);
		
		// set base directory where file manager operates
		$this->base_dir = "../".FILE_DIR;
		
		// Set current dir value from GET param or from COOKIE param				
		$dir = $this->getReqParam("dir", "");
		if( $dir == "" )
		{
			if( isset($_COOKIE[FM_LASTDIR_COOKIE]) && ($_COOKIE[FM_LASTDIR_COOKIE] != "") )
			{
				if( file_exists($this->base_dir.$_COOKIE[FM_LASTDIR_COOKIE]) )
					$dir = $_COOKIE[FM_LASTDIR_COOKIE];
				else 	
					setcookie("memfmdir", "./", time()+FM_LASTDIR_LIFETIME);
			}
		}
		
		// Clear all relative pointers in directory name
		$str_array=Array("../", "/..", "./", "/.");
		$cur_dir = str_replace($str_array,"",$dir);
		
		// Save current opened directory into cookie as LastOpened for future visits
		if( is_dir($this->base_dir.$cur_dir) && ($dir!="") )
		{
			setcookie(FM_LASTDIR_COOKIE, $cur_dir, time()+FM_LASTDIR_LIFETIME);
		}
		
		$tmp_dir = $this->base_dir.$cur_dir;
		
		//$this->showLog("<br>cur: ".$cur_dir."<br>tmp: ".$tmp_dir."<br>dir: ".$dir."<br>");
		
		// Full page mode		
		$full = $this->getReqParam("full", 1);			
		$this->popupmode = ($full == "1" ? false : true );
		
		//$this->showLog("<br>Full mode: ".$full.", Popup: ".$this->popupmode."<br>");
		
		// create model and view
		$this->model = new FileManModel($this->getCfg(), $db, 1, $tmp_dir);
		$this->pageView = new FileManView($this->getCfg(), $this->getPageModel(), $this->model, ( $full == "1" ? true : false ), $cur_dir, $tmp_dir);		
	}
	
	public function action_default()
	{									
		$error_msg = "";
		//$action = $this->getReqParam("action", "");		
		//if( $action == "makelogin" )
		//{
		//	
		//}
		
		$this->pageView->render_main();		
	}

	public function action_createdir()
	{
		$fname = $this->getReqParam("fname", "");
		
		if( !$this->model->dir_create($fname) )
		{
			// Error
		}
		$this->pageView->setErrorMsg($this->model->getLastErr());
		
		$this->pageView->viewMode = "files";		
		$this->pageView->render_main();
	}
	
	public function action_deletedir()
	{
		$fname = $this->getReqParam("fname", "");
		
		if( !$this->model->dir_delete($fname) )
		{
			// error
		}
		$this->pageView->setErrorMsg($this->model->getLastErr());
		
		$this->pageView->viewMode = "files";
		$this->pageView->render_main();		
	}
	
	public function action_loadfile()
	{
		if( (isset($_FILES['myfile'])) && ($_FILES['myfile']['name']!="") && ($_FILES['myfile']['tmp_name']!="") )
		{		
			if( !$this->model->file_load($_FILES['myfile']['name'], $_FILES['myfile']['tmp_name']) )
			{
				//$this->pageView->setErrorMsg($this->model->getLastErr());
			}
			$this->pageView->setErrorMsg($this->model->getLastErr());
		}
		
		$this->pageView->viewMode = "files";
		$this->pageView->render_main();
	}
	
	public function action_deletefile()
	{
		$fname = $this->getReqParam("fname", "");
	
		if( !$this->model->file_delete($fname) )
		{
			// error
		}
		$this->pageView->setErrorMsg($this->model->getLastErr());
	
		$this->pageView->viewMode = "files";
		$this->pageView->render_main();
	}
	
	public function action_deletefiles()
	{
		$fnamearr = $this->getReqParam("arr", null);
		
		$this->model->file_delete_arr($fnamearr);
		$this->pageView->setErrorMsg($this->model->getLastErr());
		
		$this->pageView->viewMode = "files";
		$this->pageView->render_main();		
	}	
}
?>