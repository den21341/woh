<?php
////////////////////////////////////////////////////////////////////////////////
// Developed By Ukrainian Hosting company, 2015                               //
// Alexandr Godunov                                                           //
//      Украинский Хостинг                                                    //
//      Годунов Александр                                                     //
//   Данный код запрещен для использования на других сайтах, которые          //
//   разрабатываются без участия компании "Украинский Хостинг"                //
////////////////////////////////////////////////////////////////////////////////

define("ADMIN_MODE", false);
define("TEST_MODE", false);

//if( isset($_SERVER['REQUEST_URI']) && ($_SERVER['REQUEST_URI'] == "/fbrun") )
//{
//	include "fb.php";
//	exit();
//}

// Load all settings
include "inc/db-inc.php";
//include "inc/connect-inc.php";
//include "inc/initcms-inc.php";

//phpinfo();

////////////////////////////////////////////////////////////////////////////////
// Start application
$uhcms_app = new UhCmsApp($UHCMS_CONFIG);
if( $uhcms_app )
	$uhcms_app->run();